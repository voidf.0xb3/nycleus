# Nycleus
Super awesome new programming language!

## Build
This project builds using [CMake](https://www.cmake.org/). It depends on the
following libraries, which must be present in the system and discoverable by
CMake:
* [Microsoft’s implementation of the Guidelines Support Library
  (GSL)](https://github.com/Microsoft/GSL)
* [Boost](https://www.boost.org/).ContainerHash
* [LLVM](https://llvm.org/) (obviously)

(In particular, GSL needs to be installed using CMake, so that it is properly
exported and can be imported here.)

It also uses [Python 3](https://www.python.org/) to run certain build scripts,
so it must also be installed and discoverable by CMake.

Finally, unit tests use the [Boost](https://www.boost.org/).Test framework, so
it should also be discoverable by CMake.

During configuration time, a number of Unicode Character Database files are
downloaded from the Unicode Consortium’s website. If downloads are not possible,
they can be placed in advance in the `ucd` subdirectory of the build directory.
Specifically, the following files from Unicode 15.0.0 are needed:
* [extracted/DerivedJoiningType.txt](https://www.unicode.org/Public/15.0.0/ucd/extracted/DerivedJoiningType.txt)
* [CaseFolding.txt](https://www.unicode.org/Public/15.0.0/ucd/CaseFolding.txt)
* [DerivedCoreProperties.txt](https://www.unicode.org/Public/15.0.0/ucd/DerivedCoreProperties.txt)
* [DerivedNormalizationProps.txt](https://www.unicode.org/Public/15.0.0/ucd/DerivedNormalizationProps.txt)
* [IndicSyllabicCategory.txt](https://www.unicode.org/Public/15.0.0/ucd/IndicSyllabicCategory.txt)
* [NormalizationTest.txt](https://www.unicode.org/Public/15.0.0/ucd/Scripts.txt)
* [PropList.txt](https://www.unicode.org/Public/15.0.0/ucd/PropList.txt)
* [Scripts.txt](https://www.unicode.org/Public/15.0.0/ucd/Scripts.txt)
* [UnicodeData.txt](https://www.unicode.org/Public/15.0.0/ucd/UnicodeData.txt)

## License
This project is copyrighted by voidf.0xb3 and is made available under the ISC
license, found in the `LICENSE` file.
