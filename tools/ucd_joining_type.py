import sys
from UTrieBuilder import UTrieBuilder
from ucd_utils import *

if len(sys.argv) != 5:
  raise ValueError("ucd_joining_type.py requires four arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
joining_type_cpp = sys.argv[3]
joining_type_hpp = sys.argv[4]

joining_type_values = {"U": 0, "R": 1, "L": 2, "D": 3, "C": 4, "T": 5}
joining_type = UTrieBuilder(4, joining_type_values["U"])
with open(source, encoding="utf-8") as f:
  for line in f:
    line = line.split("#", 1)[0].strip()
    if line == "":
      continue
    fields = [field.strip() for field in line.split(";")]
    if len(fields) != 2:
      raise ValueError(f"{source} has incorrect format")
    decode_to_utrie(fields[0], joining_type, joining_type_values[fields[1]])
utrie_to_cpp(
  base_dir + "/" + joining_type_cpp,
  base_dir + "/" + joining_type_hpp,
  joining_type_hpp,
  joining_type.build(),
  "joining_type_",
  "UNICODE_UCD_JOINING_TYPE_HPP_INCLUDED_OZC3LI4VMKX28TAA9ASI6218X",
  source
)
