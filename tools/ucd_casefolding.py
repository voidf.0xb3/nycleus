import sys
from kmp import KMPTable
from ucd_utils import *
from UTrieBuilder import UTrieBuilder

if len(sys.argv) != 5:
  raise ValueError("ucd_casefolding.py requires four arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
casefold_cpp = sys.argv[3]
casefold_hpp = sys.argv[4]

casefoldings = {}

with open(source, encoding="utf-8") as f:
  for line in f:
    line = line.split("#", 1)[0].strip()
    if line == "":
      continue
    fields = [field.strip() for field in line.split(";")]
    if len(fields) < 3:
      raise ValueError(f"{source} has incorrect format")

    if fields[1] != "C" and fields[1] != "F":
      continue

    codepoint = int(fields[0], 16)
    casefolding = [int(cp, 16) for cp in fields[2].split()]
    if len(casefolding) > 4:
      raise ValueError(f"{source}: casefolding longer than four code points")
    casefoldings[codepoint] = casefolding

def casefolding_is_short(cf):
  return len(cf) == 1 and (0 < cf[0] < 0xD800 or 0xF900 <= cf[0] < 0x10000)

def encode_casefolding(cf):
  cf_encoded = []
  for cp in cf:
    cf_encoded += utf16_encode(cp)
  return cf_encoded

long_casefoldings = [encode_casefolding(cf) for cf in casefoldings.values()
  if not casefolding_is_short(cf)]
kmp_tables = [KMPTable(cf) for cf in long_casefoldings]
long_mappings = [-1] * len(long_casefoldings)
long_serial = []
num_processed = 0

while num_processed < len(long_casefoldings):
  # Find the longest overlay. While we're at it, also find actual substrings and duplicates.
  overlay_len = 0
  overlay_idx = None
  for i in range(len(long_casefoldings)):
    if long_mappings[i] != -1:
      continue
    pos = kmp_tables[i].do_kmp(long_serial, long_casefoldings[i])
    if pos <= len(long_serial) - len(long_casefoldings[i]):
      # This is a substring of the existing serialization
      long_mappings[i] = pos
      num_processed += 1
    elif len(long_serial) - pos >= overlay_len:
      # We found a longer overlay.
      overlay_len = len(long_serial) - pos
      overlay_idx = i
  # If we didn't find *any* overlay, not even a zero-length one, we're done.
  if overlay_idx == None:
    break
  mapping = long_mappings[overlay_idx] = len(long_serial) - overlay_len
  if mapping >= (0xF900 - 0xD800) // 4:
    raise ValueError(f"{source}: long mapping limit exceeded")
  long_serial += long_casefoldings[overlay_idx][overlay_len:]
  num_processed += 1

# Turn the mappings list into a mappings dictionary
long_mappings = {str(long_casefoldings[i]): long_mappings[i]
  for i in range(len(long_mappings))}

casefold_trie = UTrieBuilder(16)
for cp, cf in casefoldings.items():
  if casefolding_is_short(cf):
    casefold_trie.set(cp, cf[0])
  else:
    casefold_trie.set(cp, 0xD800
      + ((long_mappings[str(encode_casefolding(cf))] << 2) | (len(cf) - 1)))

casefold_extra_hpp = (
  f"constexpr std::size_t casefold_long_size{{{len(long_serial)}}};\n"
  "extern char16_t const casefold_long[casefold_long_size];\n"
)

casefold_extra_cpp = "char16_t const unicode::ucd::casefold_long[]{"
index = 0
for unit in long_serial:
  if index != 0:
    casefold_extra_cpp += ","
  if index % 8 == 0:
    casefold_extra_cpp += "\n\t"
  else:
    casefold_extra_cpp += " "
  casefold_extra_cpp += f"0x{unit:04X}"
  index += 1
casefold_extra_cpp += "\n};\n"

utrie_to_cpp(
  base_dir + "/" + casefold_cpp,
  base_dir + "/" + casefold_hpp,
  casefold_hpp,
  casefold_trie.build(),
  "casefold_",
  "UNICODE_UCD_CASEFOLD_HPP_INCLUDED_D6TF29FVTSQWPAPY5I2LNI4M6",
  source,
  extra_hpp_includes = "#include <cstddef>\n",
  extra_cpp = casefold_extra_cpp,
  extra_hpp = casefold_extra_hpp
)
