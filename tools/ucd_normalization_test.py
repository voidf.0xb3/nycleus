import sys
from UTrieBuilder import UTrieBuilder
from ucd_utils import *

if len(sys.argv) != 5:
  raise ValueError("ucd_normalization_test.py requires four arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
data_cpp = sys.argv[3]
data_hpp = sys.argv[4]

class CPRunSet:
  """Represents a set of code points stored as a collection of non-overlapping runs of code
  points.

  This is a utility class, used to compactly store the code points not covered by
  single-code-point normalization tests.
  """

  def __init__(self):
    self.runs_ = [[0x0000, 0xD800], [0xE000, 0x110000]]

  def remove(self, cp):
    verify_codepoint(cp)

    # Find the range containing the code point
    start = 0
    end = len(self.runs_)
    while start < end:
      mid = start + (end - start) // 2
      run = self.runs_[mid]
      if cp < run[0]:
        end = mid
      elif cp >= run[1]:
        start = mid + 1
      else:
        break
    if start == end:
      # The code point has already been removed, so we don't need to do anything
      return

    if run == [cp, cp + 1]:
      del self.runs_[mid]
    elif cp == run[0]:
      self.runs_[mid][0] += 1
    elif cp == run[1] - 1:
      self.runs_[mid][1] -= 1
    else:
      self.runs_[mid:mid + 1] = [[run[0], cp], [cp + 1, run[1]]]

  def get_runs(self):
    return self.runs_

inputs = []
nfc = []
nfd = []
single_cps = CPRunSet()

with open(source, encoding="utf-8") as f:
  for line in f:
    line = line.split("#", 1)[0].strip()

    if line == "":
      continue

    if line.startswith("@"):
      if line[1:].startswith("Part"):
        part = int(line[5:])
      continue

    fields = [field.strip() for field in line.split(";")]
    if len(fields) < 5:
      raise ValueError(f"{source!s} has incorrect format")

    input_cps = [int(cp, 16) for cp in fields[0].split()]
    inputs.append(input_cps)
    nfc.append([int(cp, 16) for cp in fields[1].split()])
    nfd.append([int(cp, 16) for cp in fields[2].split()])

    if part == 1:
      if len(input_cps) != 1:
        raise ValueError(f"{source!s}: part 1 entry has non-length-1 test case")
      single_cps.remove(input_cps[0])

single_cps = single_cps.get_runs()

def write_cp(f, cp):
  """Writes a code point as a C++ string escape sequence."""
  if cp < 0x10000:
    f.write("\\u" + format(cp, "04X"))
  else:
    f.write("\\U" + format(cp, "08X"))

with open(base_dir + "/" + data_cpp, "w", encoding="utf-8") as f:
  f.write(gen_file_prefix(source) +
    f"#include \"{data_hpp}\"\n"
    "#include \"util/u8compat.hpp\"\n"
    "#include <gsl/gsl>\n"
    "#include <cstdint>\n"
    "#include <utility>\n"
    "\n"
    "using namespace util::u8compat_literals;\n"
    "\n"
    "gsl::czstring const unicode::ucd::normal_test_inputs[]{")
  first = True
  for s in inputs:
    if first:
      first = False
    else:
      f.write(",")
    f.write('\n\tu8"')
    for cp in s:
      write_cp(f, cp)
    f.write('"_ch')

  f.write("\n};\n\ngsl::czstring const unicode::ucd::normal_test_nfc[]{")
  first = True
  for s in nfc:
    if first:
      first = False
    else:
      f.write(",")
    f.write('\n\tu8"')
    for cp in s:
      write_cp(f, cp)
    f.write('"_ch')

  f.write("\n};\n\ngsl::czstring const unicode::ucd::normal_test_nfd[]{")
  first = True
  for s in nfd:
    if first:
      first = False
    else:
      f.write(",")
    f.write('\n\tu8"')
    for cp in s:
      write_cp(f, cp)
    f.write('"_ch')

  f.write("\n};\n\nstd::pair<util::uint32_t, util::uint32_t> const "
    "unicode::ucd::single_cps[]{")
  first = True
  for run in single_cps:
    if first:
      first = False
    else:
      f.write(",")
    f.write(f"\n\t{{0x{run[0]:04X}, 0x{run[1]:04X}}}")
  f.write('\n};\n')

with open(base_dir + "/" + data_hpp, "w", encoding="utf-8") as f:
  f.write(
    gen_file_prefix(source) +
    "#ifndef UCD_TESTS_DATA_HPP_INCLUDED_72OMA5XFQI5ZAAH2AFS6MCP26\n"
    "#define UCD_TESTS_DATA_HPP_INCLUDED_72OMA5XFQI5ZAAH2AFS6MCP26\n"
    "\n"
    "#include \"util/safe_int.hpp\"\n"
    "#include \"util/u8compat.hpp\"\n"
    "#include <gsl/gsl>\n"
    "#include <cstddef>\n"
    "#include <cstdint>\n"
    "#include <utility>\n"
    "\n"
    "namespace unicode::ucd {\n"
    "\n"
    f"constexpr util::size_t normal_test_size{{{len(inputs)}}};\n"
    "\n"
    "extern gsl::czstring const normal_test_inputs[normal_test_size.unwrap()];\n"
    "extern gsl::czstring const normal_test_nfc[normal_test_size.unwrap()];\n"
    "extern gsl::czstring const normal_test_nfd[normal_test_size.unwrap()];\n"
    "\n"
    f"constexpr util::size_t single_cps_size{{{len(single_cps)}}};\n"
    "\n"
    "extern std::pair<util::uint32_t, util::uint32_t> const "
      "single_cps[single_cps_size.unwrap()];\n"
    "\n"
    "} // unicode::ucd\n"
    "\n"
    "#endif\n"
  )
