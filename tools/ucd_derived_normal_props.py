import sys
from UTrieBuilder import UTrieBuilder
from ucd_utils import *

if len(sys.argv) != 9:
  raise ValueError("ucd_derived_normal_props.py requires eight arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
full_comp_excl_cpp = sys.argv[3]
full_comp_excl_hpp = sys.argv[4]
nfc_qc_cpp = sys.argv[5]
nfc_qc_hpp = sys.argv[6]
nfd_qc_cpp = sys.argv[7]
nfd_qc_hpp = sys.argv[8]

full_comp_excl = UTrieBuilder(1)
nfc_qc = UTrieBuilder(2)
nfd_qc = UTrieBuilder(1)

with open(source, encoding="utf-8") as f:
  for line in f:
    line = line.split("#", 1)[0].strip()
    if line == "":
      continue
    fields = [field.strip() for field in line.split(";")]
    if len(fields) < 2:
      raise ValueError(f"{source} has incorrect format")

    if fields[1] == "Full_Composition_Exclusion":
      if len(fields) != 2:
        raise ValueError(f"{source} has incorrect format")
      decode_to_utrie(fields[0], full_comp_excl, 1)

    elif fields[1] == "NFC_QC":
      if len(fields) != 3:
        raise ValueError(f"{source} has incorrect format")
      if fields[2] == "Y":
        decode_to_utrie(fields[0], nfc_qc, 0)
      elif fields[2] == "N":
        decode_to_utrie(fields[0], nfc_qc, 1)
      elif fields[2] == "M":
        decode_to_utrie(fields[0], nfc_qc, 2)
      else:
        raise ValueError(f"{source}: invalid value for NFC_QC")

    elif fields[1] == "NFD_QC":
      if len(fields) != 3:
        raise ValueError(f"{source} has incorrect format")
      if fields[2] == "Y":
        decode_to_utrie(fields[0], nfd_qc, 0)
      elif fields[2] == "N":
        decode_to_utrie(fields[0], nfd_qc, 1)
      else:
        raise ValueError(f"{source}: invalid value for NFD_QC")

utrie_to_cpp(
  base_dir + "/" + full_comp_excl_cpp,
  base_dir + "/" + full_comp_excl_hpp,
  full_comp_excl_hpp,
  full_comp_excl.build(),
  "full_comp_excl_",
  "UNICODE_UCD_FULL_COMP_EXCL_HPP_INCLUDED_17XSDYKZGUFEI4MD1ALVHB5P5",
  source
)

utrie_to_cpp(
  base_dir + "/" + nfc_qc_cpp,
  base_dir + "/" + nfc_qc_hpp,
  nfc_qc_hpp,
  nfc_qc.build(),
  "nfc_qc_",
  "UNICODE_UCD_NFC_QC_HPP_INCLUDED_39TGDS3Q9D3OPS1VJ2KBZO8WX",
  source
)

utrie_to_cpp(
  base_dir + "/" + nfd_qc_cpp,
  base_dir + "/" + nfd_qc_hpp,
  nfd_qc_hpp,
  nfd_qc.build(),
  "nfd_qc_",
  "UNICODE_UCD_NFC_QC_HPP_INCLUDED_ABZD0LIHCPFSNUTYHO7BQDGG4",
  source
)
