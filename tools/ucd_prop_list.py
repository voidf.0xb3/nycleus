import sys
from UTrieBuilder import UTrieBuilder
from ucd_utils import *

if len(sys.argv) != 5:
  raise ValueError("ucd_prop_list.py requires four arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
white_space_list_cpp = sys.argv[3]
white_space_list_hpp = sys.argv[4]

white_space_cps = []
with open(source, encoding="utf-8") as f:
  for line in f:
    line = line.split("#", 1)[0].strip()
    if line == "":
      continue
    fields = [field.strip() for field in line.split(";")]
    if len(fields) != 2:
      raise ValueError(f"{source} has incorrect format")
    if fields[1] == "White_Space":
      white_space_cps.extend(decode_cp_range(fields[0]))

white_space_list_hpp_code = (
  f"constexpr std::size_t white_space_cps_size{{{len(white_space_cps)}}};\n"
  "extern char32_t const white_space_cps[white_space_cps_size];\n"
)

white_space_list_cpp_code = "char32_t const unicode::ucd::white_space_cps[]{"
index = 0
for cp in white_space_cps:
  if index != 0:
    white_space_list_cpp_code += ","
  if index % 4 == 0:
    white_space_list_cpp_code += "\n\t"
  else:
    white_space_list_cpp_code += " "
  if cp < 0x10000:
    white_space_list_cpp_code += f"U'\\u{cp:04X}'"
  else:
    white_space_list_cpp_code += f"U'\\U{cp:08X}'"
  index += 1
white_space_list_cpp_code += "\n};\n"

gen_cpp_files(
  base_dir + "/" + white_space_list_cpp,
  base_dir + "/" + white_space_list_hpp,
  white_space_list_hpp,
  white_space_list_cpp_code,
  white_space_list_hpp_code,
  "UNICODE_UCD_WHITE_SPACE_LIST_HPP_INCLUDED_HPP_P7DT3ZFTRXCOLYZ0K0PPN066S",
  source,
  extra_hpp_includes = "#include <cstddef>\n"
)
