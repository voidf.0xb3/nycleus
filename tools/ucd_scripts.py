import sys
from UTrieBuilder import UTrieBuilder
from ucd_utils import *

if len(sys.argv) != 7:
  raise ValueError("ucd_scripts.py requires six arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
scripts_cpp = sys.argv[3]
scripts_hpp = sys.argv[4]
scripts_values_cpp = sys.argv[5]
scripts_values_hpp = sys.argv[6]

scripts_values = {
  "unknown": 0,
  "common": 1,
  "inherited": 2
}
next_script_value = 3

cps = {
  0: [],
  1: [],
  2: []
}
with open(source, encoding="utf-8") as f:
  for line in f:
    line = line.split("#", 1)[0].strip()
    if line == "":
      continue
    fields = [field.strip() for field in line.split(";")]
    if len(fields) != 2:
      raise ValueError(f"{source} has incorrect format")

    script_value = fields[1].lower()
    if script_value not in scripts_values:
      scripts_values[script_value] = next_script_value
      cps[next_script_value] = []
      next_script_value += 1

    cps[scripts_values[script_value]].append(fields[0])

if next_script_value <= 256:
  data_width = 8
elif next_script_value <= 65536:
  data_width = 16
else:
  data_width = 32

scripts = UTrieBuilder(data_width, scripts_values["unknown"])
for value, ranges in cps.items():
  for r in ranges:
    decode_to_utrie(r, scripts, scripts_values[script_value])

utrie_to_cpp(
  base_dir + "/" + scripts_cpp,
  base_dir + "/" + scripts_hpp,
  scripts_hpp,
  scripts.build(),
  "scripts_",
  "UNICODE_UCD_SCRIPTS_HPP_INCLUDED_BQWPZ7KKZQV8DW5ZRASVKU8RF",
  source
)

scripts_values_text = f"enum class script_t: std::uint{data_width}_t {{"
first = True
for script_value_str, script_value_int in scripts_values.items():
  if first:
    first = False
  else:
    scripts_values_text += ","
  scripts_values_text += f"\n\t{script_value_str} = {script_value_int}"
scripts_values_text += "\n};\n"

gen_cpp_files(
  base_dir + "/" + scripts_values_cpp,
  base_dir + "/" + scripts_values_hpp,
  scripts_values_hpp,
  "",
  scripts_values_text,
  "UNICODE_UCD_SCRIPTS_VALUES_HPP_INCLUDED_V2TTULELMXFRDRY94JIFO8S3P",
  source,
  extra_hpp_includes = "#include <cstdint>\n"
)
