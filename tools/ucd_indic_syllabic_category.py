import sys
from UTrieBuilder import UTrieBuilder
from ucd_utils import *

if len(sys.argv) != 5:
  raise ValueError("ucd_indic_syllabic_category.py requires four arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
indic_syllabic_category_cpp = sys.argv[3]
indic_syllabic_category_hpp = sys.argv[4]

indic_cat_values = {
  "Other": 0,
  "Bindu": 1,
  "Visarga": 2,
  "Avagraha": 3,
  "Nukta": 4,
  "Virama": 5,
  "Pure_Killer": 6,
  "Invisible_Stacker": 7,
  "Vowel_Independent": 8,
  "Vowel_Dependent": 9,
  "Vowel": 10,
  "Consonant_Placeholder": 11,
  "Consonant": 12,
  "Consonant_Dead": 13,
  "Consonant_With_Stacker": 14,
  "Consonant_Prefixed": 15,
  "Consonant_Preceding_Repha": 16,
  "Consonant_Initial_Postfixed": 17,
  "Consonant_Succeeding_Repha": 18,
  "Consonant_Subjoined": 19,
  "Consonant_Medial": 20,
  "Consonant_Final": 21,
  "Consonant_Head_Letter": 22,
  "Modifying_Letter": 23,
  "Tone_Letter": 24,
  "Tone_Mark": 25,
  "Gemination_Mark": 26,
  "Cantillation_Mark": 27,
  "Register_Shifter": 28,
  "Syllable_Modifier": 29,
  "Consonant_Killer": 20,
  "Non_Joiner": 31,
  "Joiner": 32,
  "Number_Joiner": 33,
  "Number": 34,
  "Brahmi_Joining_Number": 35
}
indic_cat = UTrieBuilder(8, indic_cat_values["Other"])
with open(source, encoding="utf-8") as f:
  for line in f:
    line = line.split("#", 1)[0].strip()
    if line == "":
      continue
    fields = [field.strip() for field in line.split(";")]
    if len(fields) != 2:
      raise ValueError(f"{source} has incorrect format")
    decode_to_utrie(fields[0], indic_cat, indic_cat_values[fields[1]])

utrie_to_cpp(
  base_dir + "/" + indic_syllabic_category_cpp,
  base_dir + "/" + indic_syllabic_category_hpp,
  indic_syllabic_category_hpp,
  indic_cat.build(),
  "indic_syllabic_category_",
  "UNICODE_UCD_INDIC_SYLLABIC_CATEGORY_HPP_INCLUDED_634YI6FRGO2ACE03XKEXAVLMM",
  source
)
