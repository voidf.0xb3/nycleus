class KMPTable:
  def __init__(self, str):
    """Constructs a table, based on the given string, that is used in the Knuth-Morris-Pratt
    (KMP) algorithm.

    The table will have the length equal to that of the string. The final element is not used,
    because we are not looking for multiple matches.
    """
    self.table_ = [-1] * len(str)
    # The longest suffix that matches up to this position
    suffix = 0
    for i in range(1, len(str)):
      if str[i] == str[suffix]:
        # If the current element continues the suffix, then extend it and copy the table value
        # from that suffix
        self.table_[i] = self.table_[suffix]
      else:
        # If the current element does not continue the suffix, then just write out the length
        # of the suffix (in hope that the element of the haystack will continue that suffix).
        self.table_[i] = suffix
        # Consider shorter suffixes to see if maybe the current element extends those instead.
        while suffix != -1 and str[i] != str[suffix]:
          suffix = self.table_[suffix]
      # If suffix is -1, this sets the default zero-length suffix. Otherwise, the current
      # character continues the suffix, so we can increase it.
      suffix += 1

  def do_kmp(self, haystack, needle):
    """Finds a substring or an overlay using a modified version of the KMP algorithm.

    Returns the index of the first character of the substring or overlay. If there is no match,
    returns the length of the haystack.

    Unlike the regular KMP, this version accepts a partial match at the end of the haystack.
    """
    # The current position in the haystack
    i = 0
    # The current position in the needle
    j = 0

    if len(haystack) == 0:
      return 0
    while True:
      if haystack[i] == needle[j]:
        i += 1
        j += 1
        if j == len(needle) or i == len(haystack):
          return i - j
      else:
        j = self.table_[j]
        if j == -1:
          j = 0
          i += 1
          if i == len(haystack):
            return i
