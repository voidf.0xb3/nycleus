import sys
from UTrieBuilder import UTrieBuilder
from ucd_utils import *

if len(sys.argv) != 9:
  raise ValueError("ucd_derived_core_props.py requires eight arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
xid_start_cpp = sys.argv[3]
xid_start_hpp = sys.argv[4]
xid_continue_cpp = sys.argv[5]
xid_continue_hpp = sys.argv[6]
default_ignorable_cpp = sys.argv[7]
default_ignorable_hpp = sys.argv[8]

xid_start = UTrieBuilder(1)
xid_continue = UTrieBuilder(1)
default_ignorable = UTrieBuilder(1)
with open(source, encoding="utf-8") as f:
  for line in f:
    line = line.split("#", 1)[0].strip()
    if line == "":
      continue
    fields = [field.strip() for field in line.split(";")]
    if len(fields) != 2:
      raise ValueError(f"{source} has incorrect format")
    if fields[1] == "XID_Start":
      decode_to_utrie(fields[0], xid_start, 1)
    elif fields[1] == "XID_Continue":
      decode_to_utrie(fields[0], xid_continue, 1)
    elif fields[1] == "Default_Ignorable_Code_Point":
      decode_to_utrie(fields[0], default_ignorable, 1)

utrie_to_cpp(
  base_dir + "/" + xid_start_cpp,
  base_dir + "/" + xid_start_hpp,
  xid_start_hpp,
  xid_start.build(),
  "xid_start_",
  "UNICODE_UCD_XID_START_HPP_INCLUDED_W4MVC10D9AHMEIYNKKURTQHLO",
  source
)

utrie_to_cpp(
  base_dir + "/" + xid_continue_cpp,
  base_dir + "/" + xid_continue_hpp,
  xid_continue_hpp,
  xid_continue.build(),
  "xid_continue_",
  "UNICODE_UCD_XID_CONTINUE_HPP_INCLUDED_X4OZZ299EGNN0MD109QEOFVPH",
  source
)

utrie_to_cpp(
  base_dir + "/" + default_ignorable_cpp,
  base_dir + "/" + default_ignorable_hpp,
  default_ignorable_hpp,
  default_ignorable.build(),
  "default_ignorable_",
  "UNICODE_UCD_DEFAULT_IGNORABLE_HPP_INCLUDED_I5261CMY1G6I0Z1E7GCEG5ZZ3",
  source
)
