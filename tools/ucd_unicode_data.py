import sys
from UTrieBuilder import UTrieBuilder
from ucd_utils import *

if len(sys.argv) != 19:
  raise ValueError("ucd_unicode_data.py requires eighteen arguments")

source = sys.argv[1]
base_dir = sys.argv[2]
gc_cpp = sys.argv[3]
gc_hpp = sys.argv[4]
ccc_cpp = sys.argv[5]
ccc_hpp = sys.argv[6]
decimal_value_cpp = sys.argv[7]
decimal_value_hpp = sys.argv[8]
decomp_cpp = sys.argv[9]
decomp_hpp = sys.argv[10]
comp_cpp = sys.argv[11]
comp_hpp = sys.argv[12]

non_starter_list_cpp = sys.argv[13]
non_starter_list_hpp = sys.argv[14]
decomposing_list_cpp = sys.argv[15]
decomposing_list_hpp = sys.argv[16]
decimal_list_cpp = sys.argv[17]
decimal_list_hpp = sys.argv[18]

gc_values = {
  "Lu": 1, "Ll": 2, "Lt": 3, "Lm": 4, "Lo": 5,
  "Mn": 6, "Mc": 7, "Me": 8,
  "Nd": 9, "Nl": 10, "No": 11,
  "Pc": 12, "Pd": 13, "Ps": 14, "Pe": 15, "Pi": 16, "Pf": 17, "Po": 18,
  "Sm": 19, "Sc": 20, "Sk": 21, "So": 22,
  "Zs": 23, "Zl": 24, "Zp": 25,
  "Cc": 26, "Cf": 27, "Cs": 28, "Co": 29, "Cn": 0
}
gc = UTrieBuilder(8, gc_values["Cn"])
ccc = UTrieBuilder(8, 0)
decimal_values = UTrieBuilder(4, 15)

# The list of all code points with CCC!=0.
non_starter_cps = []

# The list of all code points with Numeric_Type=Decimal (aka Gc=Nd) and Numeric_Value=0.
decimal_cps = []

# Stuff for constructing canonical decomposition data. See documentation for
# unicode::canon_decompose for an explanation.

# The dictionary mapping code points to their canonical decompositions.
decomp_dict = {}

# The dictionary mapping code points appearing at index 0 of length-2 decompositions to the
# indexes of their representations in `decomp0_serial` and also to sequential IDs for compact
# storage of compositions.
decomp0 = {}

# The array containing the actual code point values that may appear at index 0 of length-2
# decompositions.
decomp0_serial = []

# The maximum value appearing in the `decomp0` dictionary (for determining the width).
decomp0_max = 0

# The next sequential ID to be assigned to a code point in `decomp0`.
decomp0_next_seq = 1

# The dictionary mapping code points appearing at index 1 of length-2 decompositions to the
# indexes of their representations in `decomp1_serial` and also to sequential IDs for compact
# storage of compositions.
decomp1 = {}

# The array containing the actual code point values that may appear at index 1 of length-2
# decompositions.
decomp1_serial = []

# The maximum value appearing in the `decomp1` dictionary (for determining the width).
decomp1_max = 0

# The next sequential ID to be assigned to a code point in `decomp1`.
decomp1_next_seq = 1

###############################################################################################

with open(source, encoding="utf-8") as f:
  first = None
  for line in f:
    line = line.split("#", 1)[0].strip()
    if line == "":
      continue
    fields = [field.strip() for field in line.split(";")]
    if len(fields) < 7:
      raise ValueError(f"{source} has incorrect format")

    # Skip surrogate code points
    codepoint = int(fields[0], 16)
    if codepoint >= 0xD800 and codepoint <= 0xDFFF:
      continue

    # Process ranges encoded in a weird way
    if first != None:
      gc.set_range(first, codepoint, gc_values[fields[2]])
      ccc.set_range(first, codepoint, int(fields[3]))
      first = None
    elif fields[1].endswith("First>"):
      first = codepoint
    else:
      gc.set(codepoint, gc_values[fields[2]])

      this_ccc = int(fields[3])
      ccc.set(codepoint, this_ccc)
      if this_ccc != 0:
        non_starter_cps.append(codepoint)

      if fields[6] != "":
        value = int(fields[6])
        decimal_values.set(codepoint, value)
        if value == 0:
          decimal_cps.append(codepoint)

      # Process decompositions
      decomp = fields[5].strip()
      if decomp != "" and not decomp.startswith("<"):
        decomp = [int(cp, 16) for cp in decomp.split()]
        if len(decomp) > 3:
          raise ValueError(f"{source}: U+{codepoint:04X} has canonical "
            "decomposition longer than 3 characters")
        if len(decomp) == 2:
          if decomp[0] not in decomp0:
            decomp0_max = len(decomp0_serial)
            decomp0[decomp[0]] = len(decomp0_serial), decomp0_next_seq
            decomp0_serial += utf16_encode(decomp[0])
            decomp0_next_seq += 1
          if decomp[1] not in decomp1:
            decomp1_max = len(decomp1_serial)
            decomp1[decomp[1]] = len(decomp1_serial), decomp1_next_seq
            decomp1_serial += utf16_encode(decomp[1])
            decomp1_next_seq += 1
        decomp_dict[codepoint] = decomp

utrie_to_cpp(
  base_dir + "/" + gc_cpp,
  base_dir + "/" + gc_hpp,
  gc_hpp,
  gc.build(),
  "gc_",
  "UNICODE_UCD_GC_HPP_INCLUDED_Z3E1MBZ2YFDJ0QC92L2T88LSC",
  source
)

utrie_to_cpp(
  base_dir + "/" + ccc_cpp,
  base_dir + "/" + ccc_hpp,
  ccc_hpp,
  ccc.build(),
  "ccc_",
  "UNICODE_UCD_CCC_HPP_INCLUDED_VCLH929ZSCDPZIP3O5O6W1F1I",
  source
)

utrie_to_cpp(
  base_dir + "/" + decimal_value_cpp,
  base_dir + "/" + decimal_value_hpp,
  decimal_value_hpp,
  decimal_values.build(),
  "decimal_value_",
  "UNICODE_UCD_CCC_HPP_INCLUDED_8E89F2CC1ZCFF9OQ3RDWYSF6U",
  source
)

###############################################################################################

# Produce the files for retrieving canonical decompositions

# Determine the width of the encoding of a length-2 decomposition. (Ideally, it should be 16
# bits, but just in case...)
decomp0_width = 1
while decomp0_max >= (1 << decomp0_width):
  decomp0_width += 1
decomp1_width = 1
while decomp1_max >= (1 << decomp1_width):
  decomp1_width += 1
if decomp0_width + decomp1_width <= 16:
  decomp01_width = 16
elif decomp0_width + decomp1_width <= 32:
  decomp01_width = 32
else:
  decomp01_width = 64

# The serialization of all decomposition into the array.
decomp_serial = []
# The mapping of canonically decomposable code points to their indexes in the `decomp_serial`
# list (this mapping will eventually appear in the trie).
decomp_indexes = {}
# The maximum value in the `decomp_indexes` dictionary (for determining the width of the
# values).
decomp_index_max = 0

for codepoint, decomp in decomp_dict.items():
  decomp_index_max = decomp_indexes[codepoint] = len(decomp_serial)
  if len(decomp) == 2:
    decomp_value = (decomp1[decomp[1]][0] << decomp0_width) | decomp0[decomp[0]][0]
    if decomp01_width == 64:
      decomp_serial += [decomp_value & 0xFFFF, (decomp_value >> 16) & 0xFFFF,
        (decomp_value >> 32) & 0xFFFF, decomp_value >> 48]
    elif decomp01_width == 32:
      decomp_serial += [decomp_value & 0xFFFF, decomp_value >> 16]
    else:
      decomp_serial.append(decomp_value)
  else:
    for cp in decomp:
      decomp_serial += utf16_encode(cp)

if decomp_index_max < (1 << 2):
  decomp_trie_width = 4
elif decomp_index_max < (1 << 6):
  decomp_trie_width = 8
elif decomp_index_max < (1 << 14):
  decomp_trie_width = 16
else:
  decomp_trie_width = 32

decomp_trie = UTrieBuilder(decomp_trie_width)
for codepoint, index in decomp_indexes.items():
  decomp_trie.set(codepoint, (index << 2) | len(decomp_dict[codepoint]))

decomp_extra_hpp = (
  f"constexpr util::size_t decomp_index0_width{{{decomp0_width!s}}};\n"
  f"constexpr util::size_t decomp_index1_width{{{decomp1_width!s}}};\n"
  "\n"
  "extern util::uint16_t const decomp_array[];\n"
  "extern util::uint16_t const decomp_array0[];\n"
  "extern util::uint16_t const decomp_array1[];\n")

decomp_extra_cpp = "util::uint16_t const unicode::ucd::decomp_array[]{"
first = True
for i in range(len(decomp_serial)):
  if first:
    first = False
  else:
    decomp_extra_cpp += ","
  if i % 8 == 0:
    decomp_extra_cpp += "\n\t"
  else:
    decomp_extra_cpp += " "
  decomp_extra_cpp += f"0x{decomp_serial[i]:04X}"
decomp_extra_cpp += ("\n};\n\n"
  "util::uint16_t const unicode::ucd::decomp_array0[]{")
first = True
for i in range(len(decomp0_serial)):
  if first:
    first = False
  else:
    decomp_extra_cpp += ","
  if i % 8 == 0:
    decomp_extra_cpp += "\n\t"
  else:
    decomp_extra_cpp += " "
  decomp_extra_cpp += f"0x{decomp0_serial[i]:04X}"
decomp_extra_cpp += ("\n};\n\n"
  "util::uint16_t const unicode::ucd::decomp_array1[]{")
first = True
for i in range(len(decomp1_serial)):
  if first:
    first = False
  else:
    decomp_extra_cpp += ","
  if i % 8 == 0:
    decomp_extra_cpp += "\n\t"
  else:
    decomp_extra_cpp += " "
  decomp_extra_cpp += f"0x{decomp1_serial[i]:04X}"
decomp_extra_cpp += "\n};\n"

utrie_to_cpp(
  base_dir + "/" + decomp_cpp,
  base_dir + "/" + decomp_hpp,
  decomp_hpp,
  decomp_trie.build(),
  "decomp_",
  "UNICODE_UCD_DECOMP_HPP_INCLUDED_7E958ZUF930UJP1STLCOCKLDI",
  source,
  extra_hpp_includes = "#include <cstddef>\n",
  extra_cpp = decomp_extra_cpp,
  extra_hpp = decomp_extra_hpp
)

###############################################################################################

# Stuff for constructing canonical composition data (the inverse of length-2 canonical
# decompositions). See documentation for unicode::canon_compose for an explanation.

comp0_width = 1
while decomp0_next_seq > (1 << comp0_width):
  comp0_width <<= 1
comp0_trie = UTrieBuilder(comp0_width, 0)
for codepoint, value in decomp0.items():
  comp0_trie.set(codepoint, value[1])
comp0_cpp, comp0_hpp = utrie_to_cpp_code(comp0_trie.build(), "comp0_")

comp1_width = 1
while decomp1_next_seq > (1 << comp1_width):
  comp1_width <<= 1
comp1_trie = UTrieBuilder(comp1_width, 0)
for codepoint, value in decomp1.items():
  comp1_trie.set(codepoint, value[1])
comp1_cpp, comp1_hpp = utrie_to_cpp_code(comp1_trie.build(), "comp1_")

comp_serial = [0xDFFF] * (decomp0_next_seq - 1) * (decomp1_next_seq - 1)
comp_suppl = []
for codepoint, comp in decomp_dict.items():
  if len(comp) != 2:
    continue
  idx0 = decomp0[comp[0]][1]
  idx1 = decomp1[comp[1]][1]
  if codepoint < 0x10000:
    comp_value = codepoint
  else:
    comp_value = len(comp_suppl) + 0xD800
    comp_suppl.append(codepoint - 0x10000)
  comp_serial[(idx1 - 1) * (decomp0_next_seq - 1) + (idx0 - 1)] = comp_value
if len(comp_suppl) > 2047:
  raise ValueError(f"{source}: more that 2047 supplementary code points "
    "with a length-2 canonical decomposition are not supported")

comp_suppl_packed = []
for i in range(0, len(comp_suppl) - 1, 2):
  comp_suppl_packed += [comp_suppl[i] & 0xFF, (comp_suppl[i] >> 8) & 0xFF,
    (comp_suppl[i] >> 16) | ((comp_suppl[i + 1] & 0x0F) << 4),
    (comp_suppl[i + 1] >> 4) & 0xFF, comp_suppl[i + 1] >> 12]
if len(comp_suppl) % 2 == 1:
  comp_suppl_packed += [comp_suppl[-1] & 0xFF, (comp_suppl[-1] >> 8) & 0xFF,
    comp_suppl[-1] >> 16]

comp_trie0_cpp, comp_trie0_hpp = utrie_to_cpp_code(comp0_trie.build(),
  "comp0_")
comp_trie1_cpp, comp_trie1_hpp = utrie_to_cpp_code(comp1_trie.build(),
  "comp1_")

comp_hpp_code = (comp_trie0_hpp + "\n" + comp_trie1_hpp + "\n"
  f"constexpr util::size_t comp_array_mult = {decomp0_next_seq - 1!s};\n"
  "extern util::uint16_t const comp_array[];\n"
  "extern util::uint8_t const comp_suppl[];\n")
comp_cpp_code = (comp_trie0_cpp + "\n" + comp_trie1_cpp + "\n"
  "util::uint16_t const unicode::ucd::comp_array[]{")
first = True
for i in range(len(comp_serial)):
  if first:
    first = False
  else:
    comp_cpp_code += ","
  if i % 8 == 0:
    comp_cpp_code += "\n\t"
  else:
    comp_cpp_code += " "
  comp_cpp_code += f"0x{comp_serial[i]:04X}"
comp_cpp_code += "\n};\n\nutil::uint8_t const unicode::ucd::comp_suppl[]{"
first = True
for i in range(len(comp_suppl_packed)):
  if first:
    first = False
  else:
    comp_cpp_code += ","
  if i % 8 == 0:
    comp_cpp_code += "\n\t"
  else:
    comp_cpp_code += " "
  comp_cpp_code += f"0x{comp_suppl_packed[i]:02X}"
comp_cpp_code += "\n};\n"

gen_cpp_files(
  base_dir + "/" + comp_cpp,
  base_dir + "/" + comp_hpp,
  comp_hpp,
  comp_cpp_code,
  comp_hpp_code,
  "UNICODE_UCD_COMP_HPP_INCLUDED_HPP_BRHFUKM8OU3246E4RXVB7VG09",
  source,
  extra_hpp_includes = "#include <cstddef>\n"
)

###############################################################################################

# A full list of non-starter code points (with CCC != 0), for testing data.

non_starter_list_hpp_code = (
  f"constexpr util::size_t non_starter_cps_size{{{len(non_starter_cps)}}};\n"
  "extern std::uint32_t const non_starter_cps[non_starter_cps_size.unwrap()];\n"
)

non_starter_list_cpp_code = (
  "std::uint32_t const unicode::ucd::non_starter_cps[]{"
)
index = 0
for cp in non_starter_cps:
  if index != 0:
    non_starter_list_cpp_code += ","
  if index % 4 == 0:
    non_starter_list_cpp_code += "\n\t"
  else:
    non_starter_list_cpp_code += " "
  if cp < 0x10000:
    non_starter_list_cpp_code += f"U'\\u{cp:04X}'"
  else:
    non_starter_list_cpp_code += f"U'\\U{cp:08X}'"
  index += 1
non_starter_list_cpp_code += "\n};\n"

gen_cpp_files(
  base_dir + "/" + non_starter_list_cpp,
  base_dir + "/" + non_starter_list_hpp,
  non_starter_list_hpp,
  non_starter_list_cpp_code,
  non_starter_list_hpp_code,
  "UNICODE_UCD_NON_STARTER_LIST_HPP_INCLUDED_HPP_L70AQ7B9TWXEFX2826G4UOFP9",
  source,
  extra_hpp_includes = "#include <cstddef>\n"
)

###############################################################################################

# A full list of code points with non-trivial canonical decompositions, for testing data.

decomposing_list_hpp_code = (
  f"constexpr util::size_t decomposing_cps_size{{{len(decomp_dict)}}};\n"
  "extern std::uint32_t const decomposing_cps[decomposing_cps_size.unwrap()];\n"
)

decomposing_list_cpp_code = (
  "std::uint32_t const unicode::ucd::decomposing_cps[]{"
)
index = 0
for cp, decomp in decomp_dict.items():
  if index != 0:
    decomposing_list_cpp_code += ","
  if index % 4 == 0:
    decomposing_list_cpp_code += "\n\t"
  else:
    decomposing_list_cpp_code += " "
  if cp < 0x10000:
    decomposing_list_cpp_code += f"U'\\u{cp:04X}'"
  else:
    decomposing_list_cpp_code += f"U'\\U{cp:08X}'"
  index += 1
decomposing_list_cpp_code += "\n};\n"

gen_cpp_files(
  base_dir + "/" + decomposing_list_cpp,
  base_dir + "/" + decomposing_list_hpp,
  decomposing_list_hpp,
  decomposing_list_cpp_code,
  decomposing_list_hpp_code,
  "UNICODE_UCD_DECOMPOSING_LIST_HPP_INCLUDED_HPP_3LZ4XADHKTE3SS9F4AK5ZKK9W",
  source,
  extra_hpp_includes = "#include <cstddef>\n"
)

###############################################################################################

# A full list of code points with Numeric_Value=Decimal (aka Gc=Nd), for testing data.

decimal_list_hpp_code = (
  f"constexpr util::size_t decimal_cps_size{{{len(decimal_cps)}}};\n"
  "extern std::uint32_t const decimal_cps[decimal_cps_size.unwrap()];\n"
)

decimal_list_cpp_code = "std::uint32_t const unicode::ucd::decimal_cps[]{"
index = 0
for cp in decimal_cps:
  if index != 0:
    decimal_list_cpp_code += ","
  if index % 4 == 0:
    decimal_list_cpp_code += "\n\t"
  else:
    decimal_list_cpp_code += " "
  if cp < 0x10000:
    decimal_list_cpp_code += f"U'\\u{cp:04X}'"
  else:
    decimal_list_cpp_code += f"U'\\U{cp:08X}'"
  index += 1
decimal_list_cpp_code += "\n};\n"

gen_cpp_files(
  base_dir + "/" + decimal_list_cpp,
  base_dir + "/" + decimal_list_hpp,
  decimal_list_hpp,
  decimal_list_cpp_code,
  decimal_list_hpp_code,
  "UNICODE_UCD_DECIMAL_LIST_HPP_INCLUDED_HPP_21CDHHW4WQGEN0YW4PPFCAAOF",
  source
)
