from kmp import KMPTable
from ucd_utils import *

"""Contains utilities for building Unicode tries.

A Unicode trie is a data structure designed for fast and compact representation of a read-only
mapping of Unicode code points to arbitrary values. This is useful for representing the content
of the Unicode Character Database. See the unicode::utrie C++ class (in
`src/unicode/utrie.hpp`) for a description of Unicode tries as used here.
"""

# The number of least significant bits of the code point that are used as the index into the
# data block. (In case of one-level indirection, the first-level index is the rest of the code
# point's bits; in case of two-level indirection, it is determined by the INDEX2_WIDTH value.)
INDEX1_WIDTH = 5
# The mask selecting data first-level index bits from a code point that uses one-level
# indirection, or first- and second-level index bits from a code point that uses two-level
# indirection.
INDEX1_MASK = ~((1 << INDEX1_WIDTH) - 1)
# The number of data instances in a single data block.
DATA_BLOCK_SIZE = 1 << INDEX1_WIDTH
# The number of code points represented by a single data block.
DATA_BLOCK_CODEPOINTS = DATA_BLOCK_SIZE

# The number of bits of the code point, above those used to index into the data block, that are
# used to index into the first-level indirection block, in the case of two-level indexing. The
# second-level index is the rest of the code point's bits.
INDEX2_WIDTH = 6
# The mask selecting second-level index bits from a code point that uses two-level indirection.
INDEX2_MASK = ~((1 << (INDEX1_WIDTH + INDEX2_WIDTH)) - 1)
# The number of data block indexes in a first-level indirection block.
LVL1_BLOCK_SIZE = 1 << INDEX2_WIDTH
# The number of code points represented by a single first-level indirection block.
LVL1_BLOCK_CODEPOINTS = LVL1_BLOCK_SIZE * DATA_BLOCK_SIZE

# The mask selecting the bits of the codepoint containing the index into the data block.
DATA_INDEX_MASK = (1 << INDEX1_WIDTH) - 1

# The lowest surrogate code point.
SURROGATE_START = 0xD800
# The first code point after the surrogate range.
SURROGATE_END = 0xE000
# The number of surrogate code points.
SURROGATE_CODEPOINTS = SURROGATE_END - SURROGATE_START
# The number of data blocks that would contain surrogate code points.
SURROGATE_DATA_BLOCKS = SURROGATE_CODEPOINTS // DATA_BLOCK_CODEPOINTS

# The lowest code point where one-level indirection begins (below that, we use the code point
# as a direct index into the data array).
LVL1_THRESHOLD = 0x0100
# The number of values in the fixed (directly-indexed) data section.
DATA_FIXED_SIZE = LVL1_THRESHOLD
# The lowest code point where two-level indirection begins.
LVL2_THRESHOLD = 0x10000
# The number of indexes in the fixed (directly-indexed) first-level indirection section.
LVL1_FIXED_SIZE = (LVL2_THRESHOLD - LVL1_THRESHOLD
  - SURROGATE_CODEPOINTS) // DATA_BLOCK_SIZE

class UTrie:
  """Represents a built trie, as returned from UTrieBuilder.build().

  This is not intended to be used for retrieving the mapping from within Python, but rather for
  converting the trie data to a different format (e. g., C++ source code). Nevertheless, it can
  be queried.

  The fields `data_width`, `data`, `lvl1`, `lvl2`, `top`, and `initial_value` can be used to
  access the underlying data.
  """

  def __init__(self, data_width, data, lvl1, lvl2, top, initial_value):
    """Construct a Unicode trie. This should only be called from UTrieBuilder.build()."""
    self.data_width = data_width
    self.data = data
    self.lvl1 = lvl1
    self.lvl2 = lvl2
    self.top = top
    self.initial_value = initial_value

  def get(self, codepoint):
    """Retrieve the value associated with a particular code point."""
    verify_codepoint(codepoint)
    if codepoint >= self.top:
      return self.initial_value

    # Code point uses direct indexing.
    if codepoint < LVL1_THRESHOLD:
      return self.data[codepoint]

    # Code point uses one-level indirection.
    if codepoint < LVL2_THRESHOLD:
      shifted = codepoint - LVL1_THRESHOLD
      if codepoint >= SURROGATE_END:
        shifted -= SURROGATE_CODEPOINTS
      data_idx = self.lvl1[shifted >> INDEX1_WIDTH]
      return self.data[data_idx + (shifted & DATA_INDEX_MASK)]

    # Code point uses two-level indirection.
    shifted = codepoint - LVL2_THRESHOLD
    lvl1_idx = self.lvl2[shifted >> (INDEX1_WIDTH + INDEX2_WIDTH)]
    data_idx = self.lvl1[lvl1_idx + ((shifted & ~INDEX2_MASK) >> INDEX1_WIDTH)]
    return self.data[data_idx + (shifted & DATA_INDEX_MASK)]

class UTrieBuilder:
  """Gathers data for the trie and performs the trie compaction."""

  def verify_value_(self, value):
    """Check whether a value is valid. If not, raise an exception."""
    if value < 0:
      raise ValueError("UTrieBuilder: value must not be negative")
    if value >= (1 << self.data_width_):
      raise ValueError("UTrieBuilder: value does not fit in the specified data width")

  def __init__(self, data_width, initial_value = 0):
    """Construct the UTrieBuilder.

    `data_width` is the number of bits stored for each code point.
    `initial_value` is the value initially associated with each code point.
    """

    if data_width <= 0:
      raise ValueError("UTrieBuilder.__init__: data_width must be positive")
    self.data_width_ = data_width

    self.verify_value_(initial_value)
    self.initial_value_ = initial_value

    # The array containing data values for directly-indexing code points.
    self.data_fixed_ = [initial_value for i in range(LVL1_THRESHOLD)]
    # The data blocks for code points that use one- and two-level indirection.
    self.data_blocks_ = []
    # The reference counts for the data blocks. Note that this does not include references from
    # first-level indirection blocks whose reference count is zero.
    self.data_blocks_refcount_ = []

    # The array containing data block indexes for code points that use one-level indirection.
    self.lvl1_fixed_ = []
    # The first-level indirection blocks for code points that use two-level indirection.
    self.lvl1_blocks_ = []
    # The reference counts for the first-level indirection blocks.
    self.lvl1_blocks_refcount_ = []

    # The array containing indexes of first-level indirection block for code points that use
    # two-level indirection.
    self.lvl2_ = []

    # The first code point that is not covered by the trie. This code point and all code points
    # above it are mapped to the initial value.
    self.top_ = LVL1_THRESHOLD

  def ensure_top_(self, codepoint):
    """If necessary, increase self.top_ to be higher than the given code point."""
    if codepoint < self.top_:
      return
    if codepoint < LVL1_THRESHOLD:
      return

    new_data_block_idx = len(self.data_blocks_)
    self.data_blocks_.append([self.initial_value_] * DATA_BLOCK_SIZE)
    self.data_blocks_refcount_.append(0)

    if codepoint < LVL2_THRESHOLD:
      # Set new_top to the next highest data block boundary.
      new_top = (codepoint + DATA_BLOCK_CODEPOINTS) & INDEX1_MASK
    else:
      # For now, set new_top to the threshold.
      new_top = LVL2_THRESHOLD

    if new_top > self.top_:
      num_new = (new_top - self.top_) // DATA_BLOCK_CODEPOINTS
      if self.top_ < SURROGATE_START and new_top >= SURROGATE_END:
        num_new -= SURROGATE_DATA_BLOCKS
      self.lvl1_fixed_ += [new_data_block_idx] * num_new
      self.data_blocks_refcount_[new_data_block_idx] += num_new
      self.top_ = new_top

    # If we don't need to mess with second indexes, we're done.
    if codepoint < LVL2_THRESHOLD:
      return

    new_lvl1_block_idx = len(self.lvl1_blocks_)
    self.lvl1_blocks_.append([new_data_block_idx] * LVL1_BLOCK_SIZE)
    self.data_blocks_refcount_[new_data_block_idx] += LVL1_BLOCK_SIZE
    self.lvl1_blocks_refcount_.append(0)

    # Set new_top to the next highest first-level index block boundary.
    new_top = (codepoint + LVL1_BLOCK_CODEPOINTS) & INDEX2_MASK

    num_new = (new_top - self.top_) // LVL1_BLOCK_CODEPOINTS
    self.lvl2_ += [new_lvl1_block_idx] * num_new
    self.lvl1_blocks_refcount_[new_lvl1_block_idx] += num_new
    self.top_ = new_top

  def ensure_unique_block_(self, idx, is_lvl1):
    """Ensure that a block is only referenced once and therefore can be safely modified.

    `idx` is the index of the desired block in its corresponding array.
    `is_lvl1` is True if this is a first-level indirection block and false if this is a data
    block.

    Returns the index of the unique block (`idx` if the block was already used only once, or
    the index of the new block otherwise).
    """
    if is_lvl1:
      blocks = self.lvl1_blocks_
      refcounts = self.lvl1_blocks_refcount_
      block_size = LVL1_BLOCK_SIZE
    else:
      blocks = self.data_blocks_
      refcounts = self.data_blocks_refcount_
      block_size = DATA_BLOCK_SIZE

    if refcounts[idx] == 1:
      return idx

    new_block = [blocks[idx][i] for i in range(block_size)]
    # If this is a first-level indirection block, it contains indexes of data blocks. Since
    # we're duplicating it, we need to update the reference counts accordingly.
    if is_lvl1:
      for index in new_block:
        self.data_blocks_refcount_[index] += 1

    blocks.append(new_block)
    refcounts.append(1)
    refcounts[idx] -= 1
    return len(blocks) - 1

  def set(self, codepoint, value):
    """Update the value associated in the trie with the single code point."""
    verify_codepoint(codepoint)
    self.verify_value_(value)
    self.ensure_top_(codepoint)

    if codepoint < LVL1_THRESHOLD:
      data_array = self.data_fixed_
      data_idx = codepoint
    else:
      if codepoint < LVL2_THRESHOLD:
        if codepoint >= SURROGATE_END:
          codepoint -= SURROGATE_CODEPOINTS
        codepoint -= LVL1_THRESHOLD
        lvl1_array = self.lvl1_fixed_
        lvl1_idx = codepoint >> INDEX1_WIDTH
      else:
        codepoint -= LVL2_THRESHOLD
        lvl1_block_index = self.lvl2_[codepoint >> (INDEX1_WIDTH
          + INDEX2_WIDTH)] = self.ensure_unique_block_(self.lvl2_[
          codepoint >> (INDEX1_WIDTH + INDEX2_WIDTH)], is_lvl1 = True)
        lvl1_array = self.lvl1_blocks_[lvl1_block_index]
        lvl1_idx = (codepoint & ~INDEX2_MASK) >> INDEX1_WIDTH

      # If the data block containing the code point is in use elsewhere, we need to duplicate
      # it.
      data_block_index = lvl1_array[lvl1_idx] = self.ensure_unique_block_(
        lvl1_array[lvl1_idx], is_lvl1 = False)
      data_array = self.data_blocks_[data_block_index]
      data_idx = codepoint & DATA_INDEX_MASK

    data_array[data_idx] = value

  def set_range0_(self, array, start, end, value):
    """Update the values in a range within a directly-indexed array (either `self.data_fixed_`
    or a data block).

    `start` and `end` are code points shifted by the base of the array. `end` is exclusive.
    """
    for i in range(start, end):
      array[i] = value

  def set_range1_(self, array, start, end, value, new_data_block_idx = None):
    """Update the values in a range within an array with one-level indirection (either
    `self.lvl1_fixed_` or a first-level indirection block).

    `start` and `end` are code points shifted by the base of the array. `end` is exclusive.

    `new_data_block_idx` is the index of a data block that is filled with the target value, or
    None if no such block exists.

    Returns the index of a new block that is filled with the target value, or None if no such
    block had to be created. (If `new_data_block_idx` is not None, returns that.)
    """

    # Update the values of the leading part of the range that is not aligned on a data block
    # boundary, if such a part exists. This case also covers the situation when the entire
    # range is contained in a single data block.
    if start & DATA_INDEX_MASK != 0:
      idx = array[start >> INDEX1_WIDTH] = self.ensure_unique_block_(
        array[start >> INDEX1_WIDTH], is_lvl1 = False)
      start_shifted = start & DATA_INDEX_MASK
      end_shifted = end - (start - start_shifted)
      self.set_range0_(self.data_blocks_[idx],
        start_shifted, min(end_shifted, DATA_BLOCK_CODEPOINTS), value)
      start = (start + DATA_BLOCK_CODEPOINTS) & ~DATA_INDEX_MASK
      if start >= end:
        return new_data_block_idx

    # Update the values of full blocks contained in the range.
    lvl1_idx_start = start >> INDEX1_WIDTH
    lvl1_idx_end = end >> INDEX1_WIDTH
    if lvl1_idx_start < lvl1_idx_end:
      if new_data_block_idx == None:
        new_data_block_idx = len(self.data_blocks_)
        self.data_blocks_.append([value for i in range(DATA_BLOCK_SIZE)])
        self.data_blocks_refcount_.append(lvl1_idx_end - lvl1_idx_start)
      else:
        self.data_blocks_refcount_[new_data_block_idx] += (lvl1_idx_end
          - lvl1_idx_start)
      for i in range(lvl1_idx_start, lvl1_idx_end):
        self.data_blocks_refcount_[array[i]] -= 1
        array[i] = new_data_block_idx
    start += (lvl1_idx_end - lvl1_idx_start) * DATA_BLOCK_CODEPOINTS
    if start >= end:
      return new_data_block_idx

    # Update the values of the trailing part of the range that is not aligned on a data block
    # boundary. (If we got to this point, such a part exists.)
    idx = array[start >> INDEX1_WIDTH] = self.ensure_unique_block_(
      array[start >> INDEX1_WIDTH], is_lvl1 = False)
    start_shifted = 0
    end_shifted = end - (start - start_shifted)
    self.set_range0_(self.data_blocks_[idx],
      start_shifted, min(end_shifted, DATA_BLOCK_CODEPOINTS), value)
    return new_data_block_idx

  def set_range(self, start, end, value):
    """Update the values associated in the trie with the code points of a given range."""
    verify_codepoint(start)
    verify_codepoint(end)
    if start > end:
      raise ValueError("UTrieBuilder.set_range(): invalid range")
    if start < SURROGATE_START and end >= SURROGATE_END:
      raise ValueError("UTrieBuilder.set_range(): range includes surrogate code points")
    self.verify_value_(value)
    self.ensure_top_(end)

    # We want to work with bottom-inclusive, top-exclusive ranges, but the Unicode database
    # uses bottom- and top-inclusive ranges.
    end += 1

    # If the range contains directly-indexed code points, update them.
    if start < LVL1_THRESHOLD:
      self.set_range0_(self.data_fixed_, start, min(end, LVL1_THRESHOLD), value)
      start = LVL1_THRESHOLD
      if start >= end:
        return

    new_data_block_idx = None

    # If the range contains code points that use one-level indexing, update them.
    if start < LVL2_THRESHOLD:
      start_shifted = start - LVL1_THRESHOLD
      end_shifted = end - LVL1_THRESHOLD
      if start >= SURROGATE_END:
        start_shifted -= SURROGATE_CODEPOINTS
        end_shifted -= SURROGATE_CODEPOINTS
      new_data_block_idx = self.set_range1_(self.lvl1_fixed_,
        start_shifted, min(end_shifted, LVL1_FIXED_SIZE
        * DATA_BLOCK_CODEPOINTS), value)
      start = LVL2_THRESHOLD
      if start >= end:
        return

    # At this point, the range contains only code points that use two-level indirection.

    # Update the values of the leading part of the range that is not aligned on a first-level
    # indirection block boundary, if such a part exists. This case also covers the situation
    # when the entire range is contained in a single first-level indirection block.
    if start & ~INDEX2_MASK != 0:
      # This is very similar to the process used for code points that use only one level of
      # indirection.
      idx1 = self.lvl2_[(start - LVL2_THRESHOLD) >> (INDEX1_WIDTH
        + INDEX2_WIDTH)] = self.ensure_unique_block_(self.lvl2_[(start
        - LVL2_THRESHOLD) >> (INDEX1_WIDTH + INDEX2_WIDTH)], is_lvl1 = True)
      start_shifted = start & ~INDEX2_MASK
      end_shifted = end - (start - start_shifted)
      new_data_block_idx = self.set_range1_(self.lvl1_blocks_[idx1],
        start_shifted, min(end_shifted, LVL1_BLOCK_CODEPOINTS), value, new_data_block_idx)
      start = (start + LVL1_BLOCK_CODEPOINTS) & INDEX2_MASK
      if start >= end:
        return

    # We are now aligned on a first-level indirection block boundary. Update the values of full
    # first-level indirection blocks contained in the range, if any.
    lvl2_idx_start = (start - LVL2_THRESHOLD) >> (INDEX2_WIDTH + INDEX1_WIDTH)
    lvl2_idx_end = (end - LVL2_THRESHOLD) >> (INDEX2_WIDTH + INDEX1_WIDTH)
    if lvl2_idx_start < lvl2_idx_end:
      # If we haven't created a new data block yet, do so now.
      if new_data_block_idx == None:
        new_data_block_idx = len(self.data_blocks_)
        self.data_blocks_.append([value
          for i in range(DATA_BLOCK_SIZE)])
        self.data_blocks_refcount_.append(LVL1_BLOCK_SIZE)
      else:
        self.data_blocks_refcount_[new_data_block_idx] += LVL1_BLOCK_SIZE

      # Create a new first-level indirection block.
      new_lvl1_block_idx = len(self.lvl1_blocks_)
      self.lvl1_blocks_.append([new_data_block_idx for i in range(LVL1_BLOCK_SIZE)])
      self.lvl1_blocks_refcount_.append(lvl2_idx_end - lvl2_idx_start)

      # Attach the new block.
      for i in range(lvl2_idx_start, lvl2_idx_end):
        self.lvl1_blocks_refcount_[self.lvl2_[i]] -= 1
        if self.lvl1_blocks_refcount_[self.lvl2_[i]]:
          for idx in self.lvl1_blocks_[self.lvl2_[i]]:
            self.data_blocks_refcount_[idx] -= 1
        self.lvl2_[i] = new_lvl1_block_idx

    start += (lvl2_idx_end - lvl2_idx_start) * LVL1_BLOCK_CODEPOINTS
    if start >= end:
      return

    # Update the values of the trailing part of the range that is not aligned on a first-level
    # indirection block boundary. (If we got to this point, such a part definitely exists.)
    idx1 = self.lvl2_[(start - LVL2_THRESHOLD)
      >> (INDEX1_WIDTH + INDEX2_WIDTH)] = self.ensure_unique_block_(
      self.lvl2_[(start - LVL2_THRESHOLD) >> (INDEX1_WIDTH + INDEX2_WIDTH)], is_lvl1 = True)
    start_shifted = 0
    end_shifted = end - (start - start_shifted)
    self.set_range1_(self.lvl1_blocks_[idx1], start_shifted, end_shifted, value,
      new_data_block_idx)

    # We're done!

  def get(self, codepoint):
    """Retrieve the value associated with a particular code point."""
    verify_codepoint(codepoint)
    if codepoint >= self.top_:
      return self.initial_value_

    # Code point uses direct indexing.
    if codepoint < LVL1_THRESHOLD:
      return self.data_fixed_[codepoint]

    # Code point uses one-level indirection.
    if codepoint < LVL2_THRESHOLD:
      shifted = codepoint - LVL1_THRESHOLD
      if codepoint >= SURROGATE_END:
        shifted -= SURROGATE_CODEPOINTS
      data_block_idx = self.lvl1_fixed_[shifted >> INDEX1_WIDTH]
      return self.data_blocks_[data_block_idx][shifted & DATA_INDEX_MASK]

    # Code point uses two-level indirection.
    shifted = codepoint - LVL2_THRESHOLD
    lvl1_block_idx = self.lvl2_[shifted >> (INDEX1_WIDTH + INDEX2_WIDTH)]
    data_block_idx = self.lvl1_blocks_[lvl1_block_idx][(shifted
      & ~INDEX2_MASK) >> INDEX1_WIDTH]
    return self.data_blocks_[data_block_idx][shifted & DATA_INDEX_MASK]

  def build(self):
    """Return the built trie as a UTrie instance."""
    # If self.top_ is not 0x110000, this is a no-op, since getting the value for the top code
    # point gives the initial value. If self.top_ is 0x110000, this doesn't change the meaning
    # of the trie, but allows us to trim the table's end.
    self.initial_value_ = self.get(0x10FFFF)

    # Now remove blocks from the end while we can.
    while self.top_ > LVL2_THRESHOLD:
      can_trim = True
      # Read the top block to see if it can be trimmed.
      for lvl1_idx in self.lvl1_blocks_[self.lvl2_[-1]]:
        for value in self.data_blocks_[lvl1_idx]:
          if value != self.initial_value_:
            can_trim = False
            break
        if not can_trim:
          break
      if not can_trim:
        break
      # Now trim the block.
      self.lvl1_blocks_refcount_[self.lvl2_[-1]] -= 1
      if self.lvl1_blocks_refcount_[self.lvl2_[-1]] == 0:
        for idx in self.lvl1_blocks_[self.lvl2_[-1]]:
          self.data_blocks_refcount_[idx] -= 1
      self.lvl2_.pop()
      self.top_ -= LVL1_BLOCK_CODEPOINTS

    if self.top_ <= LVL2_THRESHOLD:
      while self.top_ > LVL1_THRESHOLD:
        can_trim = True
        # Read the top block to see if it can be trimmed.
        for value in self.data_blocks_[self.lvl1_fixed_[-1]]:
          if value != self.initial_value_:
            can_trim = False
            break
        if not can_trim:
          break
        # Now trim the block.
        self.data_blocks_refcount_[self.lvl2_[-1]] -= 1
        self.lvl1_fixed_.pop()
        self.top_ -= LVL1_BLOCK_CODEPOINTS

    # Serialize the data blocks. Begin by building the tables that are used in the KMP
    # (Knuth-Morris-Pratt) algorithm, because we will use a modified version of KMP to find
    # overlays.
    kmp = [KMPTable(block) for block in self.data_blocks_]

    # Use a greedy algorithm to find a short superstring of all data blocks.
    data_mapping = [-1] * len(self.data_blocks_)
    data_serial = [value for value in self.data_fixed_]
    num_processed = 0
    last_array = self.data_fixed_
    last_start = 0
    while num_processed < len(self.data_blocks_):
      # Find the longest overlay. While we're at it, also find actual substrings and
      # duplicates.
      overlay_len = 0
      overlay_idx = None
      for i in range(len(self.data_blocks_)):
        if data_mapping[i] != -1:
          continue
        pos = kmp[i].do_kmp(last_array, self.data_blocks_[i])
        if pos <= len(last_array) - DATA_BLOCK_SIZE:
          # This is a substring of the fixed section or a duplicate of a block.
          data_mapping[i] = last_start + pos
          num_processed += 1
        elif len(last_array) - pos >= overlay_len:
          # We found a longer overlay.
          overlay_len = len(last_array) - pos
          overlay_idx = i
      # If we didn't find *any* overlay, not even a zero-length one, we're done.
      if overlay_idx == None:
        break
      last_array = self.data_blocks_[overlay_idx]
      last_start = data_mapping[overlay_idx] = len(data_serial) - overlay_len
      data_serial += self.data_blocks_[overlay_idx][overlay_len:]
      num_processed += 1

    # In serialized form, first-level indirection blocks contain offsets into the data array,
    # so we need to work with those instead.
    lvl1_fixed = [data_mapping[idx] for idx in self.lvl1_fixed_]
    lvl1_blocks = [[data_mapping[idx] for idx in block]
      for block in self.lvl1_blocks_]

    # Now do the same things to serialize the first-level indirection blocks.
    kmp = [KMPTable(block) for block in lvl1_blocks]
    lvl1_mapping = [-1] * len(lvl1_blocks)
    lvl1_serial = [value for value in lvl1_fixed]
    num_processed = 0
    last_array = lvl1_fixed
    last_start = 0
    while num_processed < len(lvl1_blocks):
      # Find the longest overlay. While we're at it, also find actual substrings and
      # duplicates.
      overlay_len = 0
      overlay_idx = None
      for i in range(len(lvl1_blocks)):
        if lvl1_mapping[i] != -1:
          continue
        pos = kmp[i].do_kmp(last_array, lvl1_blocks[i])
        if pos <= len(last_array) - LVL1_BLOCK_SIZE:
          # This is a substring of the fixed section or a duplicate of a block.
          lvl1_mapping[i] = last_start + pos
          num_processed += 1
        elif len(last_array) - pos >= overlay_len:
          # We found a longer overlay.
          overlay_len = len(last_array) - pos
          overlay_idx = i
      # If we didn't find *any* overlay, not even a zero-length one, we're done.
      if overlay_idx == None:
        break
      last_array = lvl1_blocks[overlay_idx]
      last_start = lvl1_mapping[overlay_idx] = len(lvl1_serial) - overlay_len
      lvl1_serial += lvl1_blocks[overlay_idx][overlay_len:]
      num_processed += 1

    # Second-level indirection only has a fixed section, but it still needs to be mapped.
    lvl2_serial = [lvl1_mapping[idx] for idx in self.lvl2_]

    return UTrie(self.data_width_, data_serial, lvl1_serial, lvl2_serial,
      self.top_, self.initial_value_)
