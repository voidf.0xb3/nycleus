from datetime import datetime

def verify_codepoint(codepoint):
  """Check whether a code point is valid. If not, raise an exception."""
  if not ((codepoint >= 0 and codepoint < 0xD800)
      or (codepoint >= 0xE000 and codepoint < 0x110000)):
    raise ValueError("invalid codepoint value")

def utf16_encode(codepoint):
  """Encodes a code point into a list of two-byte code units using the UTF-16 encoding."""
  if codepoint < 0x10000:
    return [codepoint]
  codepoint -= 0x10000
  return [0xD800 | (codepoint >> 10), 0xDC00 | (codepoint & 0x3FF)]

def utrie_to_cpp_code(utrie, prefix):
  """Converts a UTrie into C++ code. Returns two strings: the first for inclusion into the
  source file, the second for inclusion into the header file.

  `prefix` is prefixed to the names of the globals comprising the file.
  """
  if utrie.data_width <= 8:
    if utrie.data_width == 1:
      if len(utrie.data) % 8 == 0:
        d = utrie.data
      else:
        d = utrie.data + [0] * (8 - len(utrie.data) % 8)
      data = [d[i] + (d[i + 1] << 1) + (d[i + 2] << 2) + (d[i + 3] << 3)
        + (d[i + 4] << 4) + (d[i + 5] << 5) + (d[i + 6] << 6) + (d[i + 7] << 7)
        for i in range(0, len(d), 8)]
      width = 1
    elif utrie.data_width == 2:
      if len(utrie.data) % 4 == 0:
        d = utrie.data
      else:
        d = utrie.data + [0] * (4 - len(utrie.data) % 4)
      data = [d[i] + (d[i + 1] << 2) + (d[i + 2] << 4) + (d[i + 3] << 6)
        for i in range(0, len(d), 4)]
      width = 2
    elif utrie.data_width <= 4:
      if len(utrie.data) % 2 == 0:
        d = utrie.data
      else:
        d = utrie.data + [0]
      data = [d[i] + (d[i + 1] << 4) for i in range(0, len(d), 2)]
      width = 4
    else:
      data = utrie.data
      width = 8
    data_type = "util::uint8_t"
    data_per_line = 8
  elif utrie.data_width <= 16:
    data = utrie.data
    width = 16
    data_type = "util::uint16_t"
    data_per_line = 8
  elif utrie.data_width <= 32:
    data = utrie.data
    width = 32
    data_type = "util::uint32_t"
    data_per_line = 4
  elif utrie.data_width <= 64:
    data = utrie.data
    width = 64
    data_type = "util::uint64_t"
    data_per_line = 2
  else:
    raise ValueError("utrie_to_cpp_code: data cannot be wider than 64 bits")

  if len(utrie.data) <= (1 << 8):
    lvl1_type = "util::uint8_t"
    lvl1_per_line = 8
  elif len(utrie.data) <= (1 << 16):
    lvl1_type = "util::uint16_t"
    lvl1_per_line = 8
  else:
    # The number of data values never exceeds the number of code points.
    lvl1_type = "util::uint32_t"
    lvl1_per_line = 4

  if len(utrie.lvl1) <= (1 << 8):
    lvl2_type = "util::uint8_t"
    lvl2_per_line = 8
  else:
    # The number of first-level indexes always fits in a 16-bit integer.
    lvl2_type = "util::uint16_t"
    lvl2_per_line = 8

  hpp = (f"constexpr util::uint8_t {prefix}data_width{{{width!s}}};\n"
    f"constexpr char32_t {prefix}top{{0x{utrie.top:04X}}};\n"
    f"constexpr {data_type} {prefix}initial_value{{{utrie.initial_value!s}}};\n"
    f"extern {data_type} const {prefix}data[];\n"
    f"extern {lvl1_type} const {prefix}lvl1[];\n"
    f"extern {lvl2_type} const {prefix}lvl2[];\n"
    f"using {prefix}lvl1_t = {lvl1_type};\n"
    f"using {prefix}lvl2_t = {lvl2_type};\n")

  cpp = f"{data_type} const unicode::ucd::{prefix}data[]{{"
  first = True
  for i in range(len(data)):
    if first:
      first = False
    else:
      cpp += ","
    if i % data_per_line == 0:
      cpp += "\n\t"
    else:
      cpp += " "
    cpp += str(data[i])
    if data_type == "util::uint64_t":
      cpp += "L"
  cpp += f"\n}};\n\n{lvl1_type} const unicode::ucd::{prefix}lvl1[]{{"
  if len(utrie.lvl1) == 0:
    cpp += "\n\t0"
  else:
    first = True
    for i in range(len(utrie.lvl1)):
      if first:
        first = False
      else:
        cpp += ","
      if i % lvl1_per_line == 0:
        cpp += "\n\t"
      else:
        cpp += " "
      cpp += str(utrie.lvl1[i])
  cpp += f"\n}};\n\n{lvl2_type} const unicode::ucd::{prefix}lvl2[]{{"
  if len(utrie.lvl2) == 0:
    cpp += "\n\t0"
  else:
    first = True
    for i in range(len(utrie.lvl2)):
      if first:
        first = False
      else:
        cpp += ","
      if i % lvl2_per_line == 0:
        cpp += "\n\t"
      else:
        cpp += " "
      cpp += str(utrie.lvl2[i])
  cpp += "\n};\n"

  return cpp, hpp

def gen_file_prefix(source):
  """Returns a string containing C++ comments to be emitted at the beginning of a generated
  file.

  `source` is the name of the UCD file containing the data that was used to generate the file.
  """

  return (
    f"// This file was auto-generated at {datetime.today().isoformat(' ')} from\n"
    f"// `{source}`\n"
    "\n"
  )

def gen_cpp_files(cpp_file, hpp_file, hpp_file_relative, cpp, hpp, header_guard,
    ucd_file, extra_hpp_includes = ""):
  """Produces a pair of generated C++ files, a source file and a header file, for the Unicode
  Character Database data.

  `prefix` is prefixed to the names of the globals comprising the file.
  `header_guard` is the C++ macro name used as the header guard.
  `ucd_file` is the name of the UCD file that serves as the origin of the data.
  `extra_hpp_includes` is an extra code chunk to be added to the header file's includes
  section.
  """

  with open(hpp_file, "w", encoding="utf-8") as f:
    f.write(
      gen_file_prefix(ucd_file) +
      f"#ifndef {header_guard}\n"
      f"#define {header_guard}\n"
      "\n"
      f"#include \"util/safe_int.hpp\"\n"
      f"{extra_hpp_includes}"
      "\n"
      "namespace unicode::ucd {\n"
      "\n"
      f"{hpp}"
      "\n"
      "} // unicode::ucd\n"
      "\n"
      "#endif\n"
    )

  with open(cpp_file, "w", encoding="utf-8") as f:
    f.write(
      gen_file_prefix(ucd_file) +
      "#include \"util/safe_int.hpp\"\n"
      f"#include \"{hpp_file_relative}\"\n"
      "\n"
      f"{cpp}"
    )

def utrie_to_cpp(cpp_file, hpp_file, hpp_file_relative, utrie, prefix,
    header_guard, ucd_file, extra_hpp_includes = "", extra_cpp = "",
    extra_hpp = ""):
  """Converts a UTrie into a pair of C++ files, a source file and a header file.

  `prefix` is prefixed to the names of the globals comprising the file.
  `header_guard` is the C++ macro name used as the header guard.
  `ucd_file` is the name of the UCD file that serves as the origin of the data.
  `extra_hpp_includes`, `extra_hpp` and `extra_cpp` are extra code chunks to be added to the
  header file (either in the includes section or within the `unicode::ucd` namespace scope) and
  the source file.
  """
  cpp, hpp = utrie_to_cpp_code(utrie, prefix)
  if extra_hpp != "":
    hpp += "\n" + extra_hpp
  if extra_cpp != "":
    cpp += "\n" + extra_cpp
  gen_cpp_files(cpp_file, hpp_file, hpp_file_relative, cpp, hpp, header_guard,
    ucd_file, extra_hpp_includes)

def decode_cp_range(txt):
  """Decodes `txt` from the Unicode Character Database as either a single code point or a range
  of code points and returns a Python range of all the code points contained in the specified
  range.
  """
  codepoints = [int(codepoint, 16) for codepoint in txt.split("..")]
  if len(codepoints) == 1:
    return range(codepoints[0], codepoints[0] + 1)
  elif len(codepoints) == 2:
    return range(codepoints[0], codepoints[1] + 1)
  else:
    raise ValueError("Invalid syntax for code point or code point range")

def decode_to_utrie(txt, utrie, value):
  """Decodes `txt` from the Unicode Character Database as either a single code point or a range
  of code points and updates `utrie` with the given `value`.

  Returns a Python range of all the code points decoded.
  """
  codepoints = [int(codepoint, 16) for codepoint in txt.split("..")]
  if len(codepoints) == 1:
    utrie.set(codepoints[0], value)
    return range(codepoints[0], codepoints[0] + 1)
  elif len(codepoints) == 2:
    utrie.set_range(codepoints[0], codepoints[1], value)
    return range(codepoints[0], codepoints[1] + 1)
  else:
    raise ValueError("Invalid syntax for code point or code point range")
