#ifndef DRIVER_HPP_INCLUDED_CP8XYL2PH1RGJTBISEIJ7TGD7
#define DRIVER_HPP_INCLUDED_CP8XYL2PH1RGJTBISEIJ7TGD7

#include "opt.hpp"

namespace nycleus_llvm {
  class target;
}

class diag_message_printer;

/** @returns Whether the invocation was successful.
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::bad_exception
 * @throws std::filesystem::filesystem_error
 * @throws std::system_error If a thread cannot be started.
 * @throws unicode::convert_exception
 * @throws std::ios_base::failure
 * @throws nycleus::target_error
 * @throws nycleus_llvm::codegen_error */
[[nodiscard]] bool run_compiler(
  options_config_t const& opts,
  diag_message_printer& diag_printer,
  nycleus_llvm::target& tgt
);

#endif
