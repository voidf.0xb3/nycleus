#ifndef NYCLEUS_SCOPE_HPP_INCLUDED_CDYZ3K2SHDXENZTH2NM8GZQNO
#define NYCLEUS_SCOPE_HPP_INCLUDED_CDYZ3K2SHDXENZTH2NM8GZQNO

#include "nycleus/utils.hpp"
#include "util/non_null_ptr.hpp"
#include "util/util.hpp"
#include <string>
#include <unordered_map>
#include <variant>

namespace nycleus {

struct class_type;
struct function;
struct local_var;

// Lookup results //////////////////////////////////////////////////////////////

struct class_field_lookup {
  class_type& cl;
  field_index_t index;

  [[nodiscard]] bool operator==(class_field_lookup other) const noexcept {
    return &cl == &other.cl && index == other.index;
  }
};

using lexical_lookup_result = std::variant<
  std::monostate,
  util::non_null_ptr<function>,
  util::non_null_ptr<local_var>,
  class_field_lookup,
  util::non_null_ptr<class_type>
>;

using class_lookup_result = std::variant<
  std::monostate,
  class_field_lookup,
  util::non_null_ptr<class_type>
>;

// Class scope /////////////////////////////////////////////////////////////////

/** @brief The scope of a class.
 *
 * A class scope can be used in two different ways: to look up symbols in the
 * class itself, such as when processing a member access expression, and as a
 * lexical scope, when processing class member definitions. In the latter but
 * not the former case, we need to fall back to the parent lexical scope.
 *
 * For class lookups, the scope object needs to live as long as the class
 * itself. For lexical lookups we need to connect to the parent scope, but that
 * only needs to live as long as we're processing the class definition and all
 * definitions nested inside it. Therefore, the class_scope object is stored
 * inside the class object and can only be used for class lookups; for lexical
 * lookups, it needs to be wrapped in a short-lived lexical_class_scope object.
 */
class class_scope {
public:
  using symbol = std::variant<
    class_field_lookup,
    util::non_null_ptr<class_type>
  >;

private:
  std::unordered_map<std::u8string, symbol> symbols_;

public:
  /** @brief Adds a new field to the symbol table.
   *
   * The parameter is moved from iff the symbol is actually added.
   *
   * @returns True if the symbol was added; false if a symbol with the same
   *          name already exists.
   * @throws std::bad_alloc */
  [[nodiscard]] bool add_symbol(std::u8string&&, symbol);

  [[nodiscard]] class_lookup_result lookup(std::u8string const&) const noexcept;
};

// Lexical scope ///////////////////////////////////////////////////////////////

/** @brief A mapping from symbol names to various entities.
 *
 * This is used for lookups within lexically enclosing scopes. Therefore, a
 * lexical_scope object only lives as long as we're processing the corresponding
 * lexical entity. */
class lexical_scope: util::hier_base {
private:
  lexical_scope const* parent_;

public:
  lexical_scope() noexcept = default;

  explicit lexical_scope(lexical_scope const* parent) noexcept:
    parent_{parent} {}

  virtual ~lexical_scope() noexcept = default;

  /** @brief Looks up the given name in the scope.
   *
   * If this scope does not have the name, the lookup is delegated to the parent
   * scope. If the lookup fails, returns std::monostate. */
  [[nodiscard]] lexical_lookup_result lookup(std::u8string const&)
    const noexcept;

private:
  [[nodiscard]] virtual lexical_lookup_result lookup_immediate(
    std::u8string const&
  ) const noexcept = 0;
};

// Lexical class scope /////////////////////////////////////////////////////////

/** @brief A lexical_scope wrapping a class_scope.
 *
 * This is used while processing a class definition to be able to perform
 * lookups in a class scope and fall back to an enclosing lexical scope. A
 * lexical_class_scope (and, therefore, its association with its enclosing
 * scope) only lives for the duration of processing a class definition, whereas
 * the underlying class_scope lives as long as the class itself. */
class lexical_class_scope final: public lexical_scope {
private:
  class_scope& cs_;

public:
  explicit lexical_class_scope(
    class_scope& cs,
    lexical_scope const* parent = nullptr
  ) noexcept: lexical_scope{parent}, cs_{cs} {}

private:
  [[nodiscard]] virtual lexical_lookup_result lookup_immediate(
    std::u8string const&
  ) const noexcept override;
};

// Simple scope ////////////////////////////////////////////////////////////////

/** @brief A scope which contains all its symbol mappings.
 *
 * Lookup consists simply of looking up the symbol in the supplied table. */
class simple_scope final: public lexical_scope {
public:
  using symbol = std::variant<
    util::non_null_ptr<function>,
    util::non_null_ptr<local_var>,
    class_field_lookup,
    util::non_null_ptr<class_type>
  >;

private:
  std::unordered_map<std::u8string, symbol> symbols_;

public:
  simple_scope() noexcept = default;

  explicit simple_scope(lexical_scope const* parent) noexcept:
    lexical_scope{parent} {}

  /** @brief Adds a new symbol to the symbol table.
   *
   * The parameter is moved from iff the symbol is actually added.
   *
   * @returns True if the symbol was added; false if a symbol with the same
   *          name already exists.
   * @throws std::bad_alloc */
  [[nodiscard]] bool add_symbol(std::u8string&&, symbol);

private:
  [[nodiscard]] virtual lexical_lookup_result lookup_immediate(
    std::u8string const&
  ) const noexcept override;
};

} // nycleus

#endif
