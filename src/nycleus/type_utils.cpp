#include "nycleus/type_utils.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/utils.hpp"
#include "util/non_null_ptr.hpp"
#include "util/ptr_with_flags.hpp"
#include <cassert>
#include <functional>
#include <memory>
#include <vector>

using namespace nycleus;

// Type registry ///////////////////////////////////////////////////////////////

int_type& type_registry::get_int_type(int_width_t width, bool is_signed) {
  assert(width > 0);
  assert(width <= max_int_width);

  auto& set{is_signed ? singed_int_types : unsinged_int_types};

  {
    auto locked{set.lock_shared()};
    auto const it{locked->find(width)};
    if(it != locked->cend()) {
      assert(*it);
      return **it;
    }
  }

  {
    auto locked{set.lock_unique()};
    auto [it, is_new] = locked->insert(
      std::make_unique<int_type>(width, is_signed));
    return **it;
  }
}

std::size_t type_registry::fn_types_hash::operator()(
    std::unique_ptr<fn_type> const& key) const noexcept {
  assert(key);
  return this->operator()<std::vector<util::non_null_ptr<type>> const&>(
    {key->param_types, key->return_type});
}

bool type_registry::fn_types_equal::operator()(
  std::unique_ptr<fn_type> const& a,
  std::unique_ptr<fn_type> const& b
) const noexcept {
  assert(a);
  assert(b);
  return operator()<std::vector<util::non_null_ptr<type>> const&>(
    {a->param_types, a->return_type}, b);
}

std::size_t type_registry::ptr_types_hash::operator()(
    util::ptr_with_flags<type, 1> t) const noexcept {
  return std::hash<util::ptr_with_flags<type, 1>>{}(t);
}

ptr_type& type_registry::get_ptr_type(
    util::ptr_with_flags<type, 1> pointee_type) {
  assert(pointee_type.get_ptr());

  {
    auto locked{ptr_types_.lock_shared()};
    auto const it{locked->find(pointee_type)};
    if(it != locked->cend()) {
      assert(*it);
      return **it;
    }
  }

  {
    auto locked{ptr_types_.lock_unique()};
    auto const [it, it_new]
      = locked->insert(std::make_unique<ptr_type>(pointee_type));
    return **it;
  }
}
