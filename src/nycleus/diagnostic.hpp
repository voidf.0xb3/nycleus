#ifndef NYCLEUS_DIAGNOSTIC_HPP_INCLUDED_HQOGKCFHV8WB16LZKPJBN8JAP
#define NYCLEUS_DIAGNOSTIC_HPP_INCLUDED_HQOGKCFHV8WB16LZKPJBN8JAP

#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/task_context.hpp"
#include "nycleus/utils.hpp"
#include "util/non_null_ptr.hpp"
#include "util/pp_seq.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <cassert>
#include <functional>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

namespace nycleus {
  struct class_type;
  struct function;
  struct local_var;
}

#define NYCLEUS_ERRORS(x, y, z) \
  z(bidi_code_in_string) y \
  x(comment_unmatched_bidi_code) y \
  x(empty_id) y \
  x(empty_ext) y \
  x(invalid_escape_code) y \
  x(num_literal_base_prefix_and_suffix) y \
  x(num_literal_invalid) y \
  x(num_literal_suffix_missing_width) y \
  x(num_literal_suffix_too_wide) y \
  x(num_literal_suffix_zero_width) y \
  x(unexpected_code_point) y \
  x(unexpected_zwj) y \
  x(unexpected_zwnj) y \
  x(unterminated_char) y \
  x(unterminated_comment) y \
  x(unterminated_string) y \
  \
  x(assign_semicolon_expected) y \
  x(bad_comparison_chain) y \
  x(bad_version) y \
  x(block_rbrace_expected) y \
  x(call_expr_arg_expected) y \
  x(class_def_body_expected) y \
  x(class_def_name_expected) y \
  x(class_def_rbrace_expected) y \
  x(def_or_directive_expected) y \
  x(expr_expected) y \
  x(expr_rparen_expected) y \
  x(fn_def_body_expected) y \
  x(fn_def_lparen_expected) y \
  x(fn_def_name_expected) y \
  x(fn_def_param_name_expected) y \
  x(fn_def_param_next_expected) y \
  x(fn_def_param_type_expected) y \
  x(fn_type_lparen_expected) y \
  x(fn_type_param_name_expected) y \
  x(fn_type_param_next_expected) y \
  x(fn_type_param_type_expected) y \
  x(fn_type_rparen_expected) y \
  x(int_type_too_wide) y \
  x(int_type_zero_width) y \
  x(member_expr_name_expected) y \
  x(type_name_expected) y \
  x(unexpected_version) y \
  x(var_def_init_expected) y \
  x(var_def_name_expected) y \
  x(var_def_semicolon_expected) y \
  x(var_def_type_expected) y \
  x(version_semicolon_expected) y \
  x(version_type_suffix) y \
  \
  x(addr_mut) y \
  x(assign_to_immut) y \
  x(assign_to_transient) y \
  x(call_expr_arg_count_mismatch) y \
  x(ct_int_div_by_zero) y \
  x(ct_int_mod_by_zero) y \
  x(ct_int_too_big) y \
  x(dep_cycle) y \
  x(duplicate_class_member) y \
  x(duplicate_global_def) y \
  x(duplicate_name_in_block) y \
  x(duplicate_param_name) y \
  x(int_literal_too_big) y \
  x(int_literal_does_not_fit_type) y \
  x(local_var_wrong_function) y \
  x(member_access_bad_name) y \
  x(name_is_not_type) y \
  x(name_is_not_value) y \
  x(name_undefined) y \
  x(too_many_nested_functions) y \
  x(uninitialized_global_var) y \
  \
  x(add_bad_types) y \
  x(add_void) y \
  x(addr_void) y \
  x(assign_bad_types) y \
  x(assign_from_void) y \
  x(assign_to_void) y \
  x(band_bad_types) y \
  x(band_void) y \
  x(bnot_bad_type) y \
  x(bnot_void) y \
  x(bor_bad_types) y \
  x(bor_void) y \
  x(bxor_bad_types) y \
  x(bxor_void) y \
  x(call_expr_arg_bad_type) y \
  x(call_expr_arg_void) y \
  x(call_expr_callee_bad_type) y \
  x(call_expr_callee_void) y \
  x(comparison_bad_types) y \
  x(comparison_void) y \
  x(deref_bad_type) y \
  x(deref_void) y \
  x(div_bad_types) y \
  x(div_void) y \
  x(fn_return_bad_type) y \
  x(fn_return_void) y \
  x(local_var_init_bad_type) y \
  x(local_var_init_void) y \
  x(logical_and_bad_types) y \
  x(logical_and_void) y \
  x(logical_or_bad_types) y \
  x(logical_or_void) y \
  x(logical_xor_bad_types) y \
  x(logical_xor_void) y \
  x(lrot_bad_types) y \
  x(lrot_void) y \
  x(lsh_bad_types) y \
  x(lsh_void) y \
  x(member_access_bad_type) y \
  x(member_access_void) y \
  x(mod_bad_types) y \
  x(mod_void) y \
  x(mul_bad_types) y \
  x(mul_void) y \
  x(neg_bad_type) y \
  x(neg_void) y \
  x(neq_bad_types) y \
  x(neq_void) y \
  x(not_bad_type) y \
  x(not_void) y \
  x(rrot_bad_types) y \
  x(rrot_void) y \
  x(rsh_bad_types) y \
  x(rsh_void) y \
  x(sub_bad_types) y \
  x(sub_void) y \
  \
  x(class_field_initializers_unsupported) y \
  x(fp_literals_unsupported) y \
  x(methods_unsupported) y \
  x(persistent_class_arg_unsupported) y \
  x(persistent_class_init_unsupported) y \
  x(persistent_class_return_unsupported) y \
  x(uninitialized_variables_unsupported)

#define NYCLEUS_WARNINGS(x, y, z) \
  z(string_newline)

namespace nycleus {

namespace diag {
  #define NYLCEUS_DIAG_FWD_DEF(x) struct x;
  PP_SEQ_FOR_EACH(NYCLEUS_ERRORS, NYLCEUS_DIAG_FWD_DEF)
  PP_SEQ_FOR_EACH(NYCLEUS_WARNINGS, NYLCEUS_DIAG_FWD_DEF)
  #undef NYCLEUS_DIAG_FWD_DEF
}

template<typename Visitor>
concept diagnostic_visitor =
  #define NYCLEUS_DIAG_IS_INVOCABLE(x) std::invocable<Visitor, diag::x&>
  PP_SEQ_APPLY(NYCLEUS_ERRORS, NYCLEUS_DIAG_IS_INVOCABLE, &&)
  && PP_SEQ_APPLY(NYCLEUS_WARNINGS, NYCLEUS_DIAG_IS_INVOCABLE, &&)
  #undef NYCLEUS_DIAG_IS_INVOCABLE
  && util::all_same_v<
    #define NYCLEUS_DIAG_INVOKE_RESULT(x) \
      std::invoke_result_t<Visitor&&, diag::x&>
    PP_SEQ_LIST(NYCLEUS_ERRORS, NYCLEUS_DIAG_INVOKE_RESULT),
    PP_SEQ_LIST(NYCLEUS_WARNINGS, NYCLEUS_DIAG_INVOKE_RESULT)
    #undef NYCLEUS_DIAG_INVOKE_RESULT
  >;

template<typename Visitor>
concept diagnostic_const_visitor =
  #define NYCLEUS_DIAG_IS_CONST_INVOCABLE(x) \
    std::invocable<Visitor, diag::x const&>
  PP_SEQ_APPLY(NYCLEUS_ERRORS, NYCLEUS_DIAG_IS_CONST_INVOCABLE, &&)
  && PP_SEQ_APPLY(NYCLEUS_WARNINGS, NYCLEUS_DIAG_IS_CONST_INVOCABLE, &&)
  #undef NYCLEUS_DIAG_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define NYCLEUS_DIAG_CONST_INVOKE_RESULT(x) \
      std::invoke_result_t<Visitor&&, diag::x const&>
    PP_SEQ_LIST(NYCLEUS_ERRORS, NYCLEUS_DIAG_CONST_INVOKE_RESULT),
    PP_SEQ_LIST(NYCLEUS_WARNINGS, NYCLEUS_DIAG_CONST_INVOKE_RESULT)
    #undef NYCLEUS_DIAG_CONST_INVOKE_RESULT
  >;

struct diagnostic {
  diagnostic() noexcept = default;
  virtual ~diagnostic() noexcept = 0;

protected:
  diagnostic(diagnostic const&) noexcept = default;
public:
  diagnostic(diagnostic&&) noexcept = delete;
  diagnostic& operator=(diagnostic const&) noexcept = delete;
  diagnostic& operator=(diagnostic&&) noexcept = delete;

  /** @throw std::bad_alloc */
  [[nodiscard]] std::unique_ptr<diagnostic> clone() const;

  enum class kind_t {
    #define NYCLEUS_DIAG_KIND(x) x
    PP_SEQ_LIST(NYCLEUS_ERRORS, NYCLEUS_DIAG_KIND),
    PP_SEQ_LIST(NYCLEUS_WARNINGS, NYCLEUS_DIAG_KIND)
    #undef NYCLEUS_DIAG_KIND
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<diagnostic_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&,
    diag::bidi_code_in_string&>;

private:
  template<diagnostic_visitor Visitor>
  static constexpr bool visit_noexcept{
    #define NYCLEUS_DIAG_INVOKE_NOEXCEPT(x) \
      noexcept(std::invoke(std::declval<Visitor&&>(), \
      std::declval<diag::x&>()))
    PP_SEQ_APPLY(NYCLEUS_ERRORS, NYCLEUS_DIAG_INVOKE_NOEXCEPT, &&)
    && PP_SEQ_APPLY(NYCLEUS_WARNINGS, NYCLEUS_DIAG_INVOKE_NOEXCEPT, &&)
    #undef NYCLEUS_DIAG_INVOKE_NOEXCEPT
  };

public:
  template<diagnostic_visitor Visitor>
  diagnostic::visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(diagnostic::visit_noexcept<Visitor>);

  template<diagnostic_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor)
      noexcept(visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<diagnostic_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&,
    diag::bidi_code_in_string const&>;

private:
  template<diagnostic_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define NYCLEUS_DIAG_CONST_INVOKE_NOEXCEPT(x) \
      noexcept(std::invoke(std::declval<Visitor&&>(), \
      std::declval<diag::x const&>()))
    PP_SEQ_APPLY(NYCLEUS_ERRORS, NYCLEUS_DIAG_CONST_INVOKE_NOEXCEPT, &&)
    && PP_SEQ_APPLY(NYCLEUS_WARNINGS, NYCLEUS_DIAG_CONST_INVOKE_NOEXCEPT, &&)
    #undef NYCLEUS_DIAG_CONST_INVOKE_NOEXCEPT
  };

public:
  template<diagnostic_const_visitor Visitor>
  diagnostic::const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(diagnostic::const_visit_noexcept<Visitor>);

  template<diagnostic_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

inline diagnostic::~diagnostic() noexcept = default;

struct error: diagnostic {};

struct warning: diagnostic {};

namespace diag {

/** @brief Base class for errors that carry source location information. */
struct error_with_location: error {
  source_location loc;

  explicit error_with_location(source_location l) noexcept: loc{std::move(l)} {}
};

/** @brief Base class for warnings that carry source location information. */
struct warning_with_location: warning {
  source_location loc;

  explicit warning_with_location(source_location l) noexcept:
    loc{std::move(l)} {}
};

// Lexer errors ////////////////////////////////////////////////////////////////

/** @brief Bidirectional formatting code inside a string or character literal.
 */
struct bidi_code_in_string final: error_with_location {
  enum class bidi_code_t {
    str_lre,
    str_rle,
    str_pdf,
    str_lro,
    str_rlo,
    str_lri,
    str_rli,
    str_fsi,
    str_pdi,
    char_lre,
    char_rle,
    char_pdf,
    char_lro,
    char_rlo,
    char_lri,
    char_rli,
    char_fsi,
    char_pdi
  };
  bidi_code_t code;

  explicit bidi_code_in_string(
    source_location l,
    bidi_code_t c
  ) noexcept: error_with_location{std::move(l)}, code{c} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bidi_code_in_string;
  }
};

/** @brief Unmatched bidirectional formatting code inside a comment. */
struct comment_unmatched_bidi_code final: error_with_location {
  enum class bidi_code_t { lre, rle, pdf, lro, rlo, lri, rli, fsi, pdi };
  bidi_code_t code;

  explicit comment_unmatched_bidi_code(
    source_location l,
    bidi_code_t c
  ) noexcept: error_with_location{std::move(l)}, code{c} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::comment_unmatched_bidi_code;
  }
};

/** @brief An empty identifier. */
struct empty_id final: error_with_location {
  explicit empty_id(
    source_location l
  ) noexcept: error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::empty_id;
  }
};

/** @brief An empty extension word. */
struct empty_ext final: error_with_location {
  explicit empty_ext(
    source_location l
  ) noexcept: error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::empty_ext;
  }
};

/** @brief A string or character literal has an invalid escape code. */
struct invalid_escape_code final: error_with_location {
  explicit invalid_escape_code(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::invalid_escape_code;
  }
};

/** @brief An integer literal has both a prefix and a suffix indicating the
 * literal's base. */
struct num_literal_base_prefix_and_suffix final: error_with_location {
  explicit num_literal_base_prefix_and_suffix(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_base_prefix_and_suffix;
  }
};

/** @brief An integer literal has invalid syntax. */
struct num_literal_invalid final: error_with_location {
  explicit num_literal_invalid(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_invalid;
  }
};

/** @brief A numeric literal's type suffix  */
struct num_literal_suffix_missing_width final: error_with_location {
  enum class prefix_letter_t { lower_s, lower_u, upper_s, upper_u };
  prefix_letter_t prefix_letter;

  explicit num_literal_suffix_missing_width(
    source_location l,
    prefix_letter_t pl
  ) noexcept: error_with_location{std::move(l)}, prefix_letter{pl} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_suffix_missing_width;
  }
};

/** @brief A numeric literal's type suffix specifies a width that is too big. */
struct num_literal_suffix_too_wide final: error_with_location {
  explicit num_literal_suffix_too_wide(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_suffix_too_wide;
  }
};

/** @brief A numeric literal's type suffix specifies a zero width. */
struct num_literal_suffix_zero_width final: error_with_location {
  explicit num_literal_suffix_zero_width(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_suffix_zero_width;
  }
};

/** @brief A source code contains an unexpected code point. */
struct unexpected_code_point final: error_with_location {
  char32_t code_point;

  explicit unexpected_code_point(
    source_location l,
    char32_t cp
  ) noexcept: error_with_location{std::move(l)}, code_point{cp} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unexpected_code_point;
  }
};

/** @brief An unexpected ZWJ (zero-width joiner) in a word. */
struct unexpected_zwj final: error_with_location {
  explicit unexpected_zwj(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unexpected_zwj;
  }
};

/** @brief An unexpected ZWNJ (zero-width non-joiner) in a word. */
struct unexpected_zwnj final: error_with_location {
  explicit unexpected_zwnj(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unexpected_zwnj;
  }
};

/** @brief An unterminated character literal. */
struct unterminated_char final: error_with_location {
  explicit unterminated_char(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unterminated_char;
  }
};

/** @brief An unterminated comment. */
struct unterminated_comment final: error_with_location {
  explicit unterminated_comment(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unterminated_comment;
  }
};

/** @brief An unterminated string or character literal. */
struct unterminated_string final: error_with_location {
  explicit unterminated_string(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unterminated_string;
  }
};

// Parser errors ///////////////////////////////////////////////////////////////

/** @brief Missing semicolon after an assignment statement. */
struct assign_semicolon_expected final: error_with_location {
  explicit assign_semicolon_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_semicolon_expected;
  }
};

/** @brief A comparison chain containing operations going in both directions. */
struct bad_comparison_chain final: error_with_location {
  /** @brief The location of a less-than operator. */
  source_location lt_loc;
  /** @brief The location of a greater-than operator. */
  source_location gt_loc;
  /** @brief Whether the less-than operator indicated above is strict. */
  bool lt_strict;
  /** @brief Whether the greater-than operator indicated above is strict. */
  bool gt_strict;
  /** @brief Whether the less-than operator occurs earlier in the chain than the
   * greater-than operator. */
  bool lt_before_gt;

  explicit bad_comparison_chain(
    source_location loc,
    source_location lt_loc_init,
    source_location gt_loc_init,
    bool lt_strict_init,
    bool gt_strict_init,
    bool lt_before_gt_init
  ) noexcept:
    error_with_location{std::move(loc)},
    lt_loc{std::move(lt_loc_init)},
    gt_loc{std::move(gt_loc_init)},
    lt_strict{lt_strict_init},
    gt_strict{gt_strict_init},
    lt_before_gt{lt_before_gt_init} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bad_comparison_chain;
  }
};

/** @brief Bad version number in the version directive. */
struct bad_version final: error_with_location {
  explicit bad_version(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bad_version;
  }
};

/** @brief Expected a right curly brace after a block expression. */
struct block_rbrace_expected final: error_with_location {
  explicit block_rbrace_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::block_rbrace_expected;
  }
};

/** @brief Expected an argument in a call expression. */
struct call_expr_arg_expected final: error_with_location {
  explicit call_expr_arg_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::call_expr_arg_expected;
  }
};

/** @brief Expected a body in a class definition. */
struct class_def_body_expected final: error_with_location {
  explicit class_def_body_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_def_body_expected;
  }
};

/** @brief Expected a name in a class definition. */
struct class_def_name_expected final: error_with_location {
  explicit class_def_name_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_def_name_expected;
  }
};

/** @brief Expected a right curly brace at the end of a class definition. */
struct class_def_rbrace_expected final: error_with_location {
  explicit class_def_rbrace_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_def_rbrace_expected;
  }
};

/** @brief Expected a top-level definition or directive. */
struct def_or_directive_expected final: error_with_location {
  explicit def_or_directive_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::def_or_directive_expected;
  }
};

/** @brief Expected an expression. */
struct expr_expected final: error_with_location {
  explicit expr_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::expr_expected;
  }
};

/** @brief Expected a right parenthesis after a parenthesized expression. */
struct expr_rparen_expected final: error_with_location {
  explicit expr_rparen_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::expr_rparen_expected;
  }
};

/** @brief Expected a body in a function definition. */
struct fn_def_body_expected final: error_with_location {
  explicit fn_def_body_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_body_expected;
  }
};

/** @brief Expected a left parenthesis in a function definition to start the
 * parameter list. */
struct fn_def_lparen_expected final: error_with_location {
  explicit fn_def_lparen_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_lparen_expected;
  }
};

/** @brief Expected a function name in a function defintion. */
struct fn_def_name_expected final: error_with_location {
  explicit fn_def_name_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_name_expected;
  }
};

/** @brief Expected a parameter name in a function definition. */
struct fn_def_param_name_expected final: error_with_location {
  explicit fn_def_param_name_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_param_name_expected;
  }
};

/** @brief Expected a next parameter in a function definition. */
struct fn_def_param_next_expected final: error_with_location {
  explicit fn_def_param_next_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_param_next_expected;
  }
};

/** @brief Expected a parameter type in a function definition. */
struct fn_def_param_type_expected final: error_with_location {
  explicit fn_def_param_type_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_param_type_expected;
  }
};

/** @brief Expected a left parenthesis to start the parameter list in a function
 * definition. */
struct fn_type_lparen_expected final: error_with_location {
  explicit fn_type_lparen_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_lparen_expected;
  }
};

/** @brief Expected a parameter name in a function definition. */
struct fn_type_param_name_expected final: error_with_location {
  explicit fn_type_param_name_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_param_name_expected;
  }
};

/** @brief Expected a parameter in a function definition. */
struct fn_type_param_next_expected final: error_with_location {
  explicit fn_type_param_next_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_param_next_expected;
  }
};

/** @brief Expected a parameter type in a function definition. */
struct fn_type_param_type_expected final: error_with_location {
  explicit fn_type_param_type_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_param_type_expected;
  }
};

/** @brief Expected a right parenthesis to end the parameter list in a function
 * definition. */
struct fn_type_rparen_expected final: error_with_location {
  explicit fn_type_rparen_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_rparen_expected;
  }
};

/** @brief An integer type name's width is too big. */
struct int_type_too_wide final: error_with_location {
  explicit int_type_too_wide(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_type_too_wide;
  }
};

/** @brief An integer type name's width is zero. */
struct int_type_zero_width final: error_with_location {
  explicit int_type_zero_width(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_type_zero_width;
  }
};

/** @brief Expected a member name as part of a member access operator. */
struct member_expr_name_expected final: error_with_location {
  explicit member_expr_name_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::member_expr_name_expected;
  }
};

/** @brief Expected a type name. */
struct type_name_expected final: error_with_location {
  explicit type_name_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::type_name_expected;
  }
};

/** @brief Unexpected version directive. */
struct unexpected_version final: error_with_location {
  explicit unexpected_version(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unexpected_version;
  }
};

/** @brief Expected a variable initializer (or the semicolon) as part of a
 * variable definition. */
struct var_def_init_expected final: error_with_location {
  explicit var_def_init_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def_init_expected;
  }
};

/** @brief Expected a variable name as part of a variable definition. */
struct var_def_name_expected final: error_with_location {
  explicit var_def_name_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def_name_expected;
  }
};

/** @brief Expected a semicolon at the end of a variable definition. */
struct var_def_semicolon_expected final: error_with_location {
  explicit var_def_semicolon_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def_semicolon_expected;
  }
};

/** @brief Expected a colon introducing a type name as part of a variable
 * definition. */
struct var_def_type_expected final: error_with_location {
  explicit var_def_type_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def_type_expected;
  }
};

/** @brief Expected a semicolon at the end of a version directive. */
struct version_semicolon_expected final: error_with_location {
  explicit version_semicolon_expected(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::version_semicolon_expected;
  }
};

/** @brief The version directive contained a numeric literal with a type suffix.
 */
struct version_type_suffix final: error_with_location {
  explicit version_type_suffix(source_location l) noexcept:
    error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::version_type_suffix;
  }
};

// Semantic errors /////////////////////////////////////////////////////////////

/** @brief Attempting to obtain a mutable pointer to an immutable operand. */
struct addr_mut final: error {
  diag_ptr<hlir::expr> expr;

  explicit addr_mut(diag_ptr<hlir::expr> e) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::addr_mut;
  }
};

/** @brief Assignment to an immutable target.
 *
 * The target must be present and have value semantics. */
struct assign_to_immut final: error {
  diag_ptr<hlir::assign_stmt> stmt;

  explicit assign_to_immut(
    diag_ptr<hlir::assign_stmt> s
  ) noexcept: stmt{std::move(s)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_to_immut;
  }
};

/** @brief Assignment to a transient target.
 *
 * The target must be present and have value semantics. */
struct assign_to_transient final: error {
  diag_ptr<hlir::assign_stmt> stmt;

  explicit assign_to_transient(
    diag_ptr<hlir::assign_stmt> s
  ) noexcept: stmt{std::move(s)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_to_transient;
  }
};

/** @brief Function called with a wrong number of arguments.
 *
 * The callee must be present and have value semantics with a function reference
 * type. */
struct call_expr_arg_count_mismatch final: error {
  diag_ptr<hlir::call_expr> expr;

  explicit call_expr_arg_count_mismatch(
    diag_ptr<hlir::call_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::call_expr_arg_count_mismatch;
  }
};

/** @brief Compile-time integer division by zero.
 *
 * The right operand must be present. */
struct ct_int_div_by_zero final: error {
  diag_ptr<hlir::div_expr> expr;

  explicit ct_int_div_by_zero(
    diag_ptr<hlir::div_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::ct_int_div_by_zero;
  }
};

/** @brief Compile-time integer remainder operation with a zero divisor.
 *
 * The right operand must be present. */
struct ct_int_mod_by_zero final: error {
  diag_ptr<hlir::mod_expr> expr;

  explicit ct_int_mod_by_zero(
    diag_ptr<hlir::mod_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::ct_int_mod_by_zero;
  }
};

/** @brief An expression with compile-time integer semantics has a value that
 * does not fit in any ordinary integer data type. */
struct ct_int_too_big final: error {
  diag_ptr<hlir::expr> expr;

  explicit ct_int_too_big(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::ct_int_too_big;
  }
};

/** @brief A dependency cycle. */
struct dep_cycle final: error {
  std::vector<task_dep_info> cycle;

  explicit dep_cycle(
    std::vector<task_dep_info> c
  ) noexcept: cycle{std::move(c)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::dep_cycle;
  }
};

/** @brief There are multiple class members defining the same name. */
struct duplicate_class_member final: error {
  std::u8string name;

  std::vector<source_location> locations;

  explicit duplicate_class_member(
    std::u8string n,
    std::vector<source_location> l
  ) noexcept: name{std::move(n)}, locations{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::duplicate_class_member;
  }
};

/** @brief There are multiple global definitions defining the same name. */
struct duplicate_global_def final: error {
  std::u8string name;

  std::vector<source_location> locations;

  explicit duplicate_global_def(
    std::u8string n,
    std::vector<source_location> l
  ) noexcept: name{std::move(n)}, locations{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::duplicate_global_def;
  }
};

/** @brief A name has been defined multiple times within the same block.
 *
 * The definition within the definition statement must be present. */
struct duplicate_name_in_block final: error {
  std::u8string name;
  std::vector<source_location> locations;

  explicit duplicate_name_in_block(
    std::u8string n,
    std::vector<source_location> l
  ) noexcept: name{std::move(n)}, locations{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::duplicate_name_in_block;
  }
};

/** @brief Duplicate parameter names in a function. */
struct duplicate_param_name final: error {
  std::u8string name;
  std::vector<source_location> locations;

  explicit duplicate_param_name(
    std::u8string n,
    std::vector<source_location> l
  ) noexcept: name{std::move(n)}, locations{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::duplicate_param_name;
  }
};

/** @brief An integer literal with a type suffix has a value that does not fit
 * in any ordinary integer data type (even one that is bigger than what is
 * requested by the type suffix). */
struct int_literal_too_big final: error {
  diag_ptr<hlir::int_literal> expr;

  explicit int_literal_too_big(
    diag_ptr<hlir::int_literal> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_literal_too_big;
  }
};

/** @brief An integer literal with a type suffix has a value that does not fit
 * in the requested data type, but does fit a bigger type. */
struct int_literal_does_not_fit_type final: error {
  diag_ptr<hlir::int_literal> expr;
  int_width_t width;

  explicit int_literal_does_not_fit_type(
    diag_ptr<hlir::int_literal> e,
    int_width_t w
  ) noexcept: expr{std::move(e)}, width{w} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_literal_does_not_fit_type;
  }
};

/** @brief A local variable of a function used in a different function. */
struct local_var_wrong_function final: error {
  diag_ptr<hlir::expr> expr;
  util::non_null_ptr<local_var const> var;
  util::non_null_ptr<function const> fn;

  explicit local_var_wrong_function(
    diag_ptr<hlir::expr> e,
    local_var const& v,
    function const& f
  ) noexcept: expr{std::move(e)}, var{&v}, fn{&f} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::local_var_wrong_function;
  }
};

/** @brief A member access operator specifies a member name that does not refer
 * to a member of the target class.
 *
 * The operand must be present and must have value semantics with a class type.
 */
struct member_access_bad_name final: error {
  diag_ptr<hlir::named_member_expr> expr;

  explicit member_access_bad_name(
    diag_ptr<hlir::named_member_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::member_access_bad_name;
  }
};

/** @brief A name does not refer to a type. */
struct name_is_not_type final: error {
  diag_ptr<hlir::named_type> name;

  explicit name_is_not_type(
    diag_ptr<hlir::named_type> n
  ) noexcept: name{std::move(n)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::name_is_not_type;
  }
};

/** @brief A name does not refer to a value. */
struct name_is_not_value final: error {
  diag_ptr<hlir::named_var_ref> name;

  explicit name_is_not_value(
    diag_ptr<hlir::named_var_ref> n
  ) noexcept: name{std::move(n)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::name_is_not_value;
  }
};

/** @brief A name that should refer to a value is undefined. */
struct name_undefined final: error {
  diag_ptr<hlir::named_var_ref> name;

  explicit name_undefined(
    diag_ptr<hlir::named_var_ref> n
  ) noexcept: name{std::move(n)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::name_undefined;
  }
};

/** @brief The limit of nested functions within a single global function has
 * been exceeded. */
struct too_many_nested_functions final: error_with_location {
  explicit too_many_nested_functions(
    source_location loc
  ) noexcept: error_with_location{std::move(loc)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::too_many_nested_functions;
  }
};

/** @brief A global variable is missing an initializer. */
struct uninitialized_global_var final: error_with_location {
  explicit uninitialized_global_var(
    source_location loc
  ) noexcept: error_with_location{std::move(loc)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::uninitialized_global_var;
  }
};

// Errors about types //////////////////////////////////////////////////////////

/** @brief Addition of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct add_bad_types final: error {
  diag_ptr<hlir::add_expr> expr;

  explicit add_bad_types(
    diag_ptr<hlir::add_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::add_bad_types;
  }
};

/** @brief Addition involving a void operand. */
struct add_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit add_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::add_void;
  }
};

/** @brief Taking the address a void operand. */
struct addr_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit addr_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::add_void;
  }
};

/** @brief Assignment of incompatible types.
 *
 * Both the target and the assigned value must be present and must have value
 * semantics. */
struct assign_bad_types final: error {
  diag_ptr<hlir::assign_stmt> stmt;

  explicit assign_bad_types(
    diag_ptr<hlir::assign_stmt> s
  ) noexcept: stmt{std::move(s)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_bad_types;
  }
};

/** @brief Assignment of a void value. */
struct assign_from_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit assign_from_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_from_void;
  }
};

/** @brief Assignment to a void target. */
struct assign_to_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit assign_to_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_to_void;
  }
};

/** @brief Bitwise AND of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct band_bad_types final: error {
  diag_ptr<hlir::band_expr> expr;

  explicit band_bad_types(
    diag_ptr<hlir::band_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::band_bad_types;
  }
};

/** @brief Bitwise AND involving a void operand. */
struct band_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit band_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::band_void;
  }
};

/** @brief Bitwise NOT of an operand of incompatible type.
 *
 * The operand must be present and must have value semantics. */
struct bnot_bad_type final: error {
  diag_ptr<hlir::bnot_expr> expr;

  explicit bnot_bad_type(
    diag_ptr<hlir::bnot_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bnot_bad_type;
  }
};

/** @brief Bitwise NOT involving a void operand. */
struct bnot_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit bnot_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bnot_void;
  }
};

/** @brief Bitwise OR of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct bor_bad_types final: error {
  diag_ptr<hlir::bor_expr> expr;

  explicit bor_bad_types(
    diag_ptr<hlir::bor_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bor_bad_types;
  }
};

/** @brief Bitwise OR involving a void operand. */
struct bor_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit bor_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bor_void;
  }
};

/** @brief Bitwise XOR of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct bxor_bad_types final: error {
  diag_ptr<hlir::bxor_expr> expr;

  explicit bxor_bad_types(
    diag_ptr<hlir::bxor_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bxor_bad_types;
  }
};

/** @brief Bitwise XOR involving a void operand. */
struct bxor_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit bxor_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bxor_void;
  }
};

/** @brief A call expression's argument has an incompatible type.
 *
 * The callee must be present and have value semantics with a function reference
 * type. Also, the argument must have value semantics. */
struct call_expr_arg_bad_type final: error {
  diag_ptr<hlir::call_expr> expr;
  util::non_null_ptr<type const> param_type;
  diag_ptr<hlir::expr> arg;

  explicit call_expr_arg_bad_type(
    diag_ptr<hlir::call_expr> e,
    type const& pt,
    diag_ptr<hlir::expr> a
  ) noexcept: expr{std::move(e)}, param_type{&pt}, arg{std::move(a)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::call_expr_arg_bad_type;
  }
};

/** @brief A call expression has a void argument. */
struct call_expr_arg_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit call_expr_arg_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::call_expr_arg_void;
  }
};

/** @brief A call expression's callee does not have a function reference type.
 *
 * The callee must be present and have value semantics. */
struct call_expr_callee_bad_type final: error {
  diag_ptr<hlir::call_expr> expr;

  explicit call_expr_callee_bad_type(
    diag_ptr<hlir::call_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::call_expr_arg_bad_type;
  }
};

/** @brief A call operation has a void callee. */
struct call_expr_callee_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit call_expr_callee_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::call_expr_arg_void;
  }
};

/** @brief Comparison of operands of incompatible types.
 *
 * The expressions must have value semantics. */
struct comparison_bad_types final: error_with_location {
  diag_ptr<hlir::expr> left;
  diag_ptr<hlir::expr> right;

  explicit comparison_bad_types(
    source_location loc,
    diag_ptr<hlir::expr> l,
    diag_ptr<hlir::expr> r
  ) noexcept:
    error_with_location{std::move(loc)},
    left{std::move(l)},
    right{std::move(r)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::comparison_bad_types;
  }
};

/** @brief Comparison involving a void operand. */
struct comparison_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit comparison_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::comparison_void;
  }
};

/** @brief Division of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct div_bad_types final: error {
  diag_ptr<hlir::div_expr> expr;

  explicit div_bad_types(
    diag_ptr<hlir::div_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::div_bad_types;
  }
};

/** @brief Division involving a void operand. */
struct div_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit div_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::div_void;
  }
};

/** @brief A function returns a value of an incompatible type.
 *
 * The function's body must be present and have value semantics. Also, the
 * function must have a non-void return type. */
struct fn_return_bad_type final: error_with_location {
  util::non_null_ptr<function const> fn;

  explicit fn_return_bad_type(
    source_location loc,
    function const& f
  ) noexcept: error_with_location{std::move(loc)}, fn{&f} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_return_bad_type;
  }
};

/** @brief A function has a body of void type even though it's declared with a
 * non-void return type.
 *
 * The function must have a non-void return type. */
struct fn_return_void final: error_with_location {
  util::non_null_ptr<function const> fn;

  explicit fn_return_void(
    source_location loc,
    function const& f
  ) noexcept: error_with_location{std::move(loc)}, fn{&f} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_return_void;
  }
};

/** @brief Dereferencing of an operand of incompatible type.
 *
 * The operand must be present and must have value semantics. */
struct deref_bad_type final: error {
  diag_ptr<hlir::deref_expr> expr;

  explicit deref_bad_type(
    diag_ptr<hlir::deref_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::deref_bad_type;
  }
};

/** @brief Dereferencing involving a void operand. */
struct deref_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit deref_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::deref_void;
  }
};

/** @brief Initialization of a local variable with an expression of an
 * incompatible type.
 *
 * The initializer must be present and have value semantics. */
struct local_var_init_bad_type final: error {
  diag_ptr<hlir::local_var_init_stmt> stmt;

  explicit local_var_init_bad_type(
    diag_ptr<hlir::local_var_init_stmt> s
  ) noexcept: stmt{std::move(s)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::local_var_init_bad_type;
  }
};

/** @brief Initialization of a local variable with a void initializer. */
struct local_var_init_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit local_var_init_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::local_var_init_void;
  }
};

/** @brief Logical AND of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct logical_and_bad_types final: error {
  diag_ptr<hlir::and_expr> expr;

  explicit logical_and_bad_types(
    diag_ptr<hlir::and_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::logical_and_bad_types;
  }
};

/** @brief Logical AND involving a void operand. */
struct logical_and_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit logical_and_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::logical_and_void;
  }
};

/** @brief Logical OR of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct logical_or_bad_types final: error {
  diag_ptr<hlir::or_expr> expr;

  explicit logical_or_bad_types(
    diag_ptr<hlir::or_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::logical_or_bad_types;
  }
};

/** @brief Logical OR involving a void operand. */
struct logical_or_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit logical_or_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::logical_or_void;
  }
};

/** @brief Logical XOR of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct logical_xor_bad_types final: error {
  diag_ptr<hlir::xor_expr> expr;

  explicit logical_xor_bad_types(
    diag_ptr<hlir::xor_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::logical_xor_bad_types;
  }
};

/** @brief Logical XOR involving a void operand. */
struct logical_xor_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit logical_xor_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::logical_xor_void;
  }
};

/** @brief Bitwise left rotation of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct lrot_bad_types final: error {
  diag_ptr<hlir::lrot_expr> expr;

  explicit lrot_bad_types(
    diag_ptr<hlir::lrot_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::lrot_bad_types;
  }
};

/** @brief Bitwise left rotation involving a void operand. */
struct lrot_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit lrot_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::lrot_void;
  }
};

/** @brief Bitwise left shift of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct lsh_bad_types final: error {
  diag_ptr<hlir::lsh_expr> expr;

  explicit lsh_bad_types(
    diag_ptr<hlir::lsh_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::lsh_bad_types;
  }
};

/** @brief Bitwise left shift involving a void operand. */
struct lsh_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit lsh_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::lsh_void;
  }
};

/** @brief Member access on an operand of a non-class type.
 *
 * The expression supplied is the operand, not the member access expression
 * itself. This is because we actually have two different node types for member
 * access (named_member_expr and indexed_member_expr). The expression must have
 * value semantics. */
struct member_access_bad_type final: error {
  diag_ptr<hlir::expr> expr;

  explicit member_access_bad_type(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::member_access_bad_type;
  }
};

/** @brief Member access involving a void operand. */
struct member_access_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit member_access_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::member_access_void;
  }
};

/** @brief Modulo operation of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct mod_bad_types final: error {
  diag_ptr<hlir::mod_expr> expr;

  explicit mod_bad_types(
    diag_ptr<hlir::mod_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::mod_bad_types;
  }
};

/** @brief Modulo operation involving a void operand. */
struct mod_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit mod_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::mod_void;
  }
};

/** @brief Multiplication of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct mul_bad_types final: error {
  diag_ptr<hlir::mul_expr> expr;

  explicit mul_bad_types(
    diag_ptr<hlir::mul_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::mul_bad_types;
  }
};

/** @brief Multiplication involving a void operand. */
struct mul_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit mul_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::mul_void;
  }
};

/** @brief Negation of an operand of incompatible type.
 *
 * The operand must be present and must have value semantics. */
struct neg_bad_type final: error {
  diag_ptr<hlir::neg_expr> expr;

  explicit neg_bad_type(
    diag_ptr<hlir::neg_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::neg_bad_type;
  }
};

/** @brief Negation involving a void operand. */
struct neg_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit neg_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::neg_void;
  }
};

/** @brief Not-equal operator applied to operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct neq_bad_types final: error {
  diag_ptr<hlir::neq_expr> expr;

  explicit neq_bad_types(
    diag_ptr<hlir::neq_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::neq_bad_types;
  }
};

/** @brief Not-equal operator involving a void operand. */
struct neq_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit neq_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::neq_void;
  }
};

/** @brief Logical NOT of an operand of incompatible type.
 *
 * The operand must be present and must have value semantics. */
struct not_bad_type final: error {
  diag_ptr<hlir::not_expr> expr;

  explicit not_bad_type(
    diag_ptr<hlir::not_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::not_bad_type;
  }
};

/** @brief Logical NOT involving a void operand. */
struct not_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit not_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::not_void;
  }
};

/** @brief Bitwise right rotation of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct rrot_bad_types final: error {
  diag_ptr<hlir::rrot_expr> expr;

  explicit rrot_bad_types(
    diag_ptr<hlir::rrot_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::rrot_bad_types;
  }
};

/** @brief Bitwise right rotation involving a void operand. */
struct rrot_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit rrot_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::rrot_void;
  }
};

/** @brief Bitwise right shift of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct rsh_bad_types final: error {
  diag_ptr<hlir::rsh_expr> expr;

  explicit rsh_bad_types(
    diag_ptr<hlir::rsh_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::rsh_bad_types;
  }
};

/** @brief Bitwise right shift involving a void operand. */
struct rsh_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit rsh_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::rsh_void;
  }
};

/** @brief Subtraction of operands of incompatible types.
 *
 * Both operands must be present and must have value semantics. */
struct sub_bad_types final: error {
  diag_ptr<hlir::sub_expr> expr;

  explicit sub_bad_types(
    diag_ptr<hlir::sub_expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::sub_bad_types;
  }
};

/** @brief Subtraction involving a void operand. */
struct sub_void final: error {
  diag_ptr<hlir::expr> expr;

  explicit sub_void(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::sub_void;
  }
};

// Errors about unsupported features ///////////////////////////////////////////

/** @brief Class field initializers are unsupported. */
struct class_field_initializers_unsupported final: error {
  diag_ptr<hlir::expr> init;

  explicit class_field_initializers_unsupported(
    diag_ptr<hlir::expr> i
  ) noexcept: init{std::move(i)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_field_initializers_unsupported;
  }
};

/** @brief Floating-point literals are unsupported. */
struct fp_literals_unsupported final: error_with_location {
  explicit fp_literals_unsupported(
    source_location loc
  ) noexcept: error_with_location{std::move(loc)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fp_literals_unsupported;
  }
};

/** @brief Methods are unsupported. */
struct methods_unsupported final: error {
  diag_ptr<hlir::fn_def> def;

  explicit methods_unsupported(
    diag_ptr<hlir::fn_def> d
  ) noexcept: def{std::move(d)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::methods_unsupported;
  }
};

/** @brief Persistent arguments of class type are unsupported. */
struct persistent_class_arg_unsupported final: error {
  diag_ptr<hlir::expr> expr;

  explicit persistent_class_arg_unsupported(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::persistent_class_arg_unsupported;
  }
};

/** @brief Persistent initializers of class type are unsupported. */
struct persistent_class_init_unsupported final: error {
  diag_ptr<hlir::expr> expr;

  explicit persistent_class_init_unsupported(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::persistent_class_init_unsupported;
  }
};

/** @brief Returning persistent expressions of class type are unsupported. */
struct persistent_class_return_unsupported final: error {
  diag_ptr<hlir::expr> expr;

  explicit persistent_class_return_unsupported(
    diag_ptr<hlir::expr> e
  ) noexcept: expr{std::move(e)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::persistent_class_return_unsupported;
  }
};

/** @brief Uninitialized variables are unsupported. */
struct uninitialized_variables_unsupported final: error {
  diag_ptr<hlir::var_def> def;

  explicit uninitialized_variables_unsupported(
    diag_ptr<hlir::var_def> d
  ) noexcept: def{std::move(d)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::uninitialized_variables_unsupported;
  }
};

// Warnings ////////////////////////////////////////////////////////////////////

/** @brief A warning about line breaks inside string or character literals. */
struct string_newline final: warning_with_location {
  enum class newline_code_t {
    str_lf, str_vt, str_ff, str_cr, str_nel, str_line_sep, str_para_sep,
    char_lf, char_vt, char_ff, char_cr, char_nel, char_line_sep, char_para_sep
  };
  newline_code_t newline_code;

  explicit string_newline(
    source_location loc,
    newline_code_t nc
  ) noexcept: warning_with_location{std::move(loc)}, newline_code{nc} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::string_newline;
  }
};

} // diag

template<diagnostic_visitor Visitor>
diagnostic::visit_result_t<Visitor> diagnostic::mutable_visit(Visitor&& visitor)
    noexcept(diagnostic::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define NYCLEUS_DIAG_VISIT(x) \
      case kind_t::x: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<diag::x&>(*this));
    PP_SEQ_FOR_EACH(NYCLEUS_ERRORS, NYCLEUS_DIAG_VISIT)
    PP_SEQ_FOR_EACH(NYCLEUS_WARNINGS, NYCLEUS_DIAG_VISIT)
    #undef NYCLEUS_DIAG_VISIT
  }
}

template<diagnostic_const_visitor Visitor>
diagnostic::const_visit_result_t<Visitor>
    diagnostic::const_visit(Visitor&& visitor) const
    noexcept(diagnostic::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define NYCLEUS_DIAG_CONST_VISIT(x) \
      case kind_t::x: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<diag::x const&>(*this));
    PP_SEQ_FOR_EACH(NYCLEUS_ERRORS, NYCLEUS_DIAG_CONST_VISIT)
    PP_SEQ_FOR_EACH(NYCLEUS_WARNINGS, NYCLEUS_DIAG_CONST_VISIT)
    #undef NYCLEUS_DIAG_CONST_VISIT
  }
}

/** @brief A copyable wrapper for std::unique_ptr<diagnostic> that performs a
 * copy of a diagnostic (via the concrete descendant's copy constructor) when
 * copied. */
class diagnostic_box {
private:
  std::unique_ptr<diagnostic> diag_;

public:
  diagnostic_box() noexcept = default;

  template<std::derived_from<diagnostic> T>
  diagnostic_box(std::unique_ptr<T> diag) noexcept: diag_{std::move(diag)} {}

  /** @throws std::bad_alloc */
  diagnostic_box(diagnostic_box const& other): diag_{other.diag_->clone()} {}

  diagnostic_box(diagnostic_box&& other) noexcept:
    diag_{std::move(other.diag_)} {}

  /** @throws std::bad_alloc */
  diagnostic_box& operator=(diagnostic_box const& other) {
    diag_ = other.diag_->clone();
    return *this;
  }

  diagnostic_box& operator=(diagnostic_box&& other) noexcept {
    diag_ = std::move(other.diag_);
    return *this;
  }

  ~diagnostic_box() noexcept = default;

  [[nodiscard]] std::unique_ptr<diagnostic> extract() && noexcept {
    return std::move(diag_);
  }

  [[nodiscard]] diagnostic& operator*() const noexcept {
    assert(has_value());
    return *diag_;
  }

  [[nodiscard]] diagnostic* operator->() const noexcept {
    assert(has_value());
    return diag_.get();
  }

  [[nodiscard]] diagnostic* get() const noexcept {
    return diag_.get();
  }

  [[nodiscard]] explicit operator bool() const noexcept {
    return diag_ != nullptr;
  }

  [[nodiscard]] bool has_value() const noexcept {
    return diag_ != nullptr;
  }
};

} // nycleus

#endif
