#include "nycleus/parser.hpp"
#include <gsl/gsl>

gsl::czstring nycleus::detail_::parser_fatal_error::what() const noexcept {
  return "nycleus::detail_::parser_fatal_error";
}
