#ifndef NYCLEUS_GLOBAL_NAME_HPP_INCLUDED_27CEOHPJQ16BPUTZUXE9G7YTU
#define NYCLEUS_GLOBAL_NAME_HPP_INCLUDED_27CEOHPJQ16BPUTZUXE9G7YTU

#include "util/format.hpp"
#include "util/ranges.hpp"
#include <cstddef>
#include <cstdint>
#include <functional>
#include <iterator>
#include <ranges>
#include <string>
#include <utility>
#include <variant>
#include <vector>

namespace nycleus {

/** @brief A context-independent name that can refer to any nameable entity in
 * a program.
 *
 * These names never occur in the source code, but are used for things such as
 * diagnostics and selecting low-level names. */
struct global_name {
  /** @brief The name of the global-scope containing entity. */
  std::u8string name;

  struct in_function {
    std::uint64_t index;
    std::u8string diag_name;

    friend bool operator==(
      in_function const& a,
      in_function const& b
    ) noexcept {
      // `diag_name` does not participate in comparisons
      return a.index == b.index;
    }
  };

  struct in_class {
    std::u8string name;

    friend bool operator==(in_class const&, in_class const&) noexcept = default;
  };

  using nested_part = std::variant<in_function, in_class>;

  /** @brief The path to a nested entity.
   *
   * Each component consists of: an index of the nested entity among all nested
   * entities of its parent; and a string indicating the entity's name. The
   * string is intended only for human consumption (e. g. in diagnostics) and is
   * not guaranteed to be unique among members of its parent; it also does not
   * participate in equality comparisons or hashes.
   *
   * The components are listed in order from outermost to innermost. */
  std::vector<nested_part> nested_parts{};

  /** @brief A flag to mark functions that serve as initializers of global
   * variables. */
  bool is_initializer{false};

  [[nodiscard]] bool operator==(global_name const& other) const noexcept;

  [[nodiscard]] std::size_t hash() const noexcept;

  template<std::output_iterator<char8_t> Out>
  Out format(Out out) const { // NOLINT(modernize-use-nodiscard)
    auto iend{nested_parts.cend()};
    for(;;) {
      auto const it{util::find_last_if(
        std::ranges::subrange{nested_parts.cbegin(), iend},
        [](nested_part const& part) noexcept {
          return std::holds_alternative<global_name::in_function>(part);
        }
      ).begin()};
      if(it == iend) {
        break;
      }
      out = util::format_to<u8"\"{}">(std::move(out),
        std::get_if<global_name::in_function>(&*iend)->diag_name);
      for(auto const& part: std::ranges::subrange{it + 1, iend}) {
        *out++ = u8':';
        auto const& name{std::get_if<global_name::in_class>(&part)->name};
        if(name.empty()) {
          out = util::format_to<u8"(no name)">(std::move(out));
        } else {
          out = util::format_to<u8"{}">(std::move(out), name);
        }
      }
      out = util::format_to<u8"\" within ">(std::move(out));
      iend = it;
    }

    if(is_initializer) {
      assert(iend == nested_parts.cbegin());
      out = util::format_to<u8"(initializer for ">(std::move(out));
    }
    out = util::format_to<u8"\"{}">(std::move(out), name);
    for(auto const& part:
        std::ranges::subrange{nested_parts.begin(), iend}) {
      out = util::format_to<u8":{}">(std::move(out),
        std::get_if<global_name::in_class>(&part)->name);
    }

    *out++ = u8'"';
    if(is_initializer) {
      *out++ = u8')';
    }

    return out;
  }
};

extern template util::output_counting_iterator global_name::format<
  util::output_counting_iterator
>(util::output_counting_iterator) const;
extern template std::back_insert_iterator<std::u8string> global_name::format<
  std::back_insert_iterator<std::u8string>
>(std::back_insert_iterator<std::u8string>) const;

} // nycleus

template<>
struct std::hash<::nycleus::global_name::in_function> {
  [[nodiscard]] ::std::size_t operator()(
    ::nycleus::global_name::in_function const& part
  ) const noexcept {
    // `diag_name` does not participate in hashes
    return ::std::hash<::std::uint64_t>{}(part.index);
  }
};

template<>
struct std::hash<::nycleus::global_name::in_class> {
  [[nodiscard]] ::std::size_t operator()(
    ::nycleus::global_name::in_class const& part
  ) const noexcept {
    return ::std::hash<::std::u8string>{}(part.name);
  }
};

template<>
struct std::hash<::nycleus::global_name> {
  [[nodiscard]] ::std::size_t operator()(::nycleus::global_name const& name)
      const noexcept {
    return name.hash();
  }
};

template<>
struct util::formatter<::nycleus::global_name, u8""> {
  struct type {
    static auto format(
      ::std::output_iterator<char8_t> auto out,
      ::nycleus::global_name const& name
    ) noexcept(noexcept(name.format(::std::move(out)))) {
      return name.format(::std::move(out));
    }
  };
};

#endif
