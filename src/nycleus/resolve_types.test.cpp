#include <boost/test/unit_test.hpp>

#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_logger.hpp"
#include "nycleus/diagnostic_tools.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/entity_bag.hpp"
#include "nycleus/global_name.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/resolve_types.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/type_utils.hpp"
#include "nycleus/utils.hpp"
#include "util/cow.hpp"
#include "util/non_null_ptr.hpp"
#include "util/stackful_coro.hpp"
#include "test_tools.hpp"
#include <concepts>
#include <filesystem>
#include <memory>
#include <optional>
#include <utility>
#include <vector>

namespace diag = nycleus::diag;
namespace hlir = nycleus::hlir;

namespace {

using src_loc = nycleus::source_location;
using nycleus::hlir_ptr;
using nycleus::diag_ptr;
using nycleus::detail_::resolve_type;
using nycleus::int_width_t;

template<nycleus::hlir_type T, typename... Args>
requires std::constructible_from<T, Args...>
[[nodiscard]] auto make(Args&&... args) {
  return nycleus::make_hlir<T>(std::forward<Args>(args)...);
}

} // (anonymous)

// The assertion macros are apparently implemented as loops, which confuses this
// clang-tidy pass
// NOLINTBEGIN(bugprone-use-after-move)

BOOST_AUTO_TEST_SUITE(test_nycleus)
BOOST_AUTO_TEST_SUITE(type_resolution)

BOOST_AUTO_TEST_CASE(int_type_unsigned) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  // u8
  auto const input{make<hlir::int_type>(src_loc{f, {1, 1}, {1, 3}},
    int_width_t{8},
    false
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_int_type(8, false));
}

BOOST_AUTO_TEST_CASE(int_type_signed) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  // s64
  auto const input{make<hlir::int_type>(src_loc{f, {1, 1}, {1, 4}},
    int_width_t{64},
    true
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_int_type(64, true));
}

BOOST_AUTO_TEST_CASE(bool_type) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  // bool
  auto const input{make<hlir::bool_type>(src_loc{f, {1, 1}, {1, 5}})};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_bool_type());
}

BOOST_AUTO_TEST_CASE(class_type) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  auto cl{std::make_unique<nycleus::class_type>()};
  auto& cl_ref{*cl};
  cl->name = nycleus::global_name{u8"A"};
  cl->core_loc = src_loc{f, {1, 7}, {1, 8}};
  bag.add(std::move(cl));

  // $A
  auto const input{make<hlir::named_type>(src_loc{f, {2, 1}, {2, 3}}, u8"A")};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &cl_ref);
}

BOOST_AUTO_TEST_CASE(ptr_type_const) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  auto cl{std::make_unique<nycleus::class_type>()};
  auto& cl_ref{*cl};
  cl->name = nycleus::global_name{u8"A"};
  cl->core_loc = src_loc{f, {1, 7}, {1, 8}};
  bag.add(std::move(cl));

  // &$A
  auto const input{make<hlir::ptr_type>(src_loc{f, {2, 1}, {2, 4}},
    make<hlir::named_type>(src_loc{f, {2, 2}, {2, 4}}, u8"A"),
    false
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_ptr_type({&cl_ref, false}));
}

BOOST_AUTO_TEST_CASE(ptr_type_mut) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  auto cl{std::make_unique<nycleus::class_type>()};
  auto& cl_ref{*cl};
  cl->name = nycleus::global_name{u8"A"};
  cl->core_loc = src_loc{f, {1, 7}, {1, 8}};
  bag.add(std::move(cl));

  // &mut $A
  auto const input{make<hlir::ptr_type>(src_loc{f, {2, 1}, {2, 8}},
    make<hlir::named_type>(src_loc{f, {2, 6}, {2, 8}}, u8"A"),
    true
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_ptr_type({&cl_ref, true}));
}

BOOST_AUTO_TEST_CASE(ptr_type_bad_pointee) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  // &#
  auto const input{make<hlir::ptr_type>(src_loc{f, {2, 1}, {2, 3}},
    nullptr,
    false
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(!result);
}

BOOST_AUTO_TEST_SUITE(fn_type)

BOOST_AUTO_TEST_CASE(no_params) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  auto cl{std::make_unique<nycleus::class_type>()};
  auto& cl_ref{*cl};
  cl->name = nycleus::global_name{u8"A"};
  cl->core_loc = src_loc{f, {1, 7}, {1, 8}};
  bag.add(std::move(cl));

  // fn((): $A)
  auto const input{make<hlir::fn_type>(src_loc{f, {2, 1}, {2, 11}},
    std::vector<hlir_ptr<hlir::type>>{},
    make<hlir::named_type>(src_loc{f, {2, 8}, {2, 10}}, u8"A")
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_fn_type(
    std::vector<util::non_null_ptr<nycleus::type>>{},
    &cl_ref
  ));
}

BOOST_AUTO_TEST_CASE(one_param) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  auto cl{std::make_unique<nycleus::class_type>()};
  auto& cl_ref{*cl};
  cl->name = nycleus::global_name{u8"A"};
  cl->core_loc = src_loc{f, {1, 7}, {1, 8}};
  bag.add(std::move(cl));

  // fn((:u64): $A)
  auto const input{make<hlir::fn_type>(src_loc{f, {2, 1}, {2, 15}},
    test::make_vector<hlir_ptr<hlir::type>>(
      make<hlir::int_type>(src_loc{f, {2, 6}, {2, 9}}, int_width_t{64}, false)
    ),
    make<hlir::named_type>(src_loc{f, {2, 12}, {2, 14}}, u8"A")
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_fn_type(
    std::vector<util::non_null_ptr<nycleus::type>>{
      util::non_null_ptr{&tr.get_int_type(64, false)}
    },
    &cl_ref
  ));
}

BOOST_AUTO_TEST_CASE(two_params) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  auto cl{std::make_unique<nycleus::class_type>()};
  auto& cl_ref{*cl};
  cl->name = nycleus::global_name{u8"A"};
  cl->core_loc = src_loc{f, {1, 7}, {1, 8}};
  bag.add(std::move(cl));

  // fn((:u64, :bool): $A)
  auto const input{make<hlir::fn_type>(src_loc{f, {2, 1}, {2, 22}},
    test::make_vector<hlir_ptr<hlir::type>>(
      make<hlir::int_type>(src_loc{f, {2, 6}, {2, 9}},
        int_width_t{64},
        false
      ),
      make<hlir::bool_type>(src_loc{f, {2, 12}, {2, 16}})
    ),
    make<hlir::named_type>(src_loc{f, {2, 19}, {2, 21}}, u8"A")
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_fn_type(
    std::vector<util::non_null_ptr<nycleus::type>>{
      util::non_null_ptr{&tr.get_int_type(64, false)},
      util::non_null_ptr{&tr.get_bool_type()}
    },
    &cl_ref
  ));
}

BOOST_AUTO_TEST_CASE(no_params_no_return) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  // fn()
  auto const input{make<hlir::fn_type>(src_loc{f, {1, 1}, {1, 5}},
    std::vector<hlir_ptr<hlir::type>>{},
    nullptr
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_fn_type(
    std::vector<util::non_null_ptr<nycleus::type>>{},
    nullptr
  ));
}

BOOST_AUTO_TEST_CASE(one_param_no_return) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  // fn(:u64)
  auto const input{make<hlir::fn_type>(src_loc{f, {1, 1}, {1, 9}},
    test::make_vector<hlir_ptr<hlir::type>>(
      make<hlir::int_type>(src_loc{f, {1, 5}, {1, 8}}, int_width_t{64}, false)
    ),
    nullptr
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_fn_type(
    std::vector<util::non_null_ptr<nycleus::type>>{
      util::non_null_ptr{&tr.get_int_type(64, false)}
    },
    nullptr
  ));
}

BOOST_AUTO_TEST_CASE(two_params_no_return) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  // fn(:u64, :bool)
  auto const input{make<hlir::fn_type>(src_loc{f, {1, 1}, {1, 16}},
    test::make_vector<hlir_ptr<hlir::type>>(
      make<hlir::int_type>(src_loc{f, {1, 5}, {1, 8}},
        int_width_t{64},
        false
      ),
      make<hlir::bool_type>(src_loc{f, {1, 11}, {1, 15}})
    ),
    nullptr
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(result == &tr.get_fn_type(
    std::vector<util::non_null_ptr<nycleus::type>>{
      util::non_null_ptr{&tr.get_int_type(64, false)},
      util::non_null_ptr{&tr.get_bool_type()}
    },
    nullptr
  ));
}

BOOST_AUTO_TEST_CASE(bad_param) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  auto cl{std::make_unique<nycleus::class_type>()};
  cl->name = nycleus::global_name{u8"A"};
  cl->core_loc = src_loc{f, {1, 7}, {1, 8}};
  bag.add(std::move(cl));

  // fn((:#): $A)
  auto const input{make<hlir::fn_type>(src_loc{f, {2, 1}, {2, 13}},
    test::make_vector<hlir_ptr<hlir::type>>(nullptr),
    make<hlir::named_type>(src_loc{f, {2, 10}, {2, 12}}, u8"A")
  )};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_TEST(!result);
}

BOOST_AUTO_TEST_SUITE_END() // fn_type

BOOST_AUTO_TEST_CASE(bad_name) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  // $A
  auto const input{make<hlir::named_type>(src_loc{f, {1, 1}, {1, 3}}, u8"A")};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::name_is_not_type>(diag_ptr{*input})
  )));

  BOOST_TEST(!result);
}

BOOST_AUTO_TEST_CASE(name_is_not_type) {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};

  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;
  nycleus::type_registry tr;

  auto var{std::make_unique<nycleus::global_var>()};
  var->name = nycleus::global_name{u8"A"};
  var->core_loc = src_loc{f, {1, 5}, {1, 6}};
  var->type = &tr.get_int_type(64, false);
  var->is_mutable = false;
  bag.add(std::move(var));

  // $A
  auto const input{make<hlir::named_type>(src_loc{f, {2, 1}, {2, 3}}, u8"A")};

  util::stackful_coro::processor<nycleus::type*> proc;
  proc.start(resolve_type, *input, dt, bag, tr);
  BOOST_REQUIRE(proc.done());
  nycleus::type const* const result{proc.get_result()};

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::name_is_not_type>(diag_ptr{*input})
  )));

  BOOST_TEST(!result);
}

BOOST_AUTO_TEST_SUITE_END() // type_resolution
BOOST_AUTO_TEST_SUITE_END() // test_nycleus

// NOLINTEND(bugprone-use-after-move)
