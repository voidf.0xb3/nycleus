#ifndef NYCLEUS_SEMANTICS_DECAY_HPP_INCLUDED_AJ9EZ6PNYGXQDPB0Q2FTBOTQU
#define NYCLEUS_SEMANTICS_DECAY_HPP_INCLUDED_AJ9EZ6PNYGXQDPB0Q2FTBOTQU

namespace nycleus {

namespace hlir {
  struct expr;
}
class diagnostic_tracker;
class type_registry;

/** @brief Decays certain semantics outside of special contexts.
 *
 * Certain semantics carry special meaning in certain contexts, but are
 * otherwise treated as certain other semantics. This is used to replace the
 * former semantics with the latter in those other situations.
 *
 * Currently this transforms compile-time integers into integer-typed value
 * semantics. */
void decay(hlir::expr&, diagnostic_tracker&, type_registry&);

} // nycleus

#endif
