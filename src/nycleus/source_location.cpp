#include "nycleus/source_location.hpp"
#include "unicode/encoding.hpp"

static_assert(unicode::encoding<nycleus::utf8_counted<char>>);
static_assert(unicode::encoding<nycleus::utf8_counted<char8_t>>);
