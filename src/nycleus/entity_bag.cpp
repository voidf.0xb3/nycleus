#include "nycleus/entity_bag.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_tracker.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/scope.hpp"
#include "nycleus/source_location.hpp"
#include "util/enumerate_view.hpp"
#include "util/format.hpp"
#include "util/non_null_ptr.hpp"
#include "util/nums.hpp"
#include "util/overflow.hpp"
#include "util/pair_projector.hpp"
#include "util/util.hpp"
#include "util/zip_view.hpp"
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <exception>
#include <memory>
#include <new>
#include <stdexcept>
#include <string>
#include <utility>
#include <variant>
#include <vector>

using namespace nycleus;

void entity_bag::add(std::unique_ptr<named_entity> entity) noexcept {
  assert(entity);
  entities_.emplace(std::move(entity));
}

named_entity* entity_bag::get(global_name const& name) const noexcept {
  auto const it{entities_.find(name)};
  if(it == entities_.cend()) {
    return nullptr;
  } else {
    return it->get();
  }
}

void entity_bag::report_duplicates(diagnostic_tracker& dt) {
  auto it{entities_.cbegin()};
  auto const iend{entities_.cend()};

  if(it == iend) {
    return;
  }

  auto it_run_start{it++};
  assert(*it_run_start);
  bool found_dupes{false};
  std::vector<source_location> locs;

  auto finish_run{[&] {
    if(found_dupes) {
      auto const& name{(*it_run_start)->name};
      assert(name.nested_parts.empty());
      assert(!name.is_initializer);
      dt.track<diag::duplicate_global_def>(name.name, std::move(locs));
    }
    found_dupes = false;
    locs.clear();
    it_run_start = it;
  }};

  while(it != iend) {
    assert(*it);
    if((*it)->name != (*it_run_start)->name) {
      finish_run();
    } else {
      if(!found_dupes) {
        locs.push_back((*it_run_start)->core_loc);
        found_dupes = true;
      }
      locs.push_back((*it)->core_loc);
    }
    ++it;
  }

  finish_run();
}

////////////////////////////////////////////////////////////////////////////////
// Processing //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

namespace {

struct type_processing_context {
  entity_bag& bag;
  diagnostic_tracker& dt;
  lexical_scope* scope;
};

struct processing_context {
  entity_bag& bag;
  diagnostic_tracker& dt;
  simple_scope& scope;
  function& fn;
  std::uint64_t& nested_index;
};

void process_type(
  hlir_ptr<hlir::type>&,
  type_processing_context
);

void process_type(
  type_ref&,
  type_processing_context
);

void process_expr(
  hlir_ptr<hlir::expr>&,
  processing_context
);

void process_stmt(
  hlir::stmt&,
  processing_context
);

void process_fn_def(
  std::unique_ptr<function>,
  hlir::fn_def&,
  global_name,
  entity_bag&,
  diagnostic_tracker&,
  simple_scope* parent_scope
);

void process_class_def(
  std::unique_ptr<class_type>,
  hlir::class_def&,
  global_name,
  entity_bag&,
  diagnostic_tracker&,
  lexical_scope* parent_scope
);

// Processing type names ///////////////////////////////////////////////////////

template<typename T>
concept type_ptr = util::one_of<T, hlir_ptr<hlir::type>, type_ref>;

void process_type_impl(
  hlir::int_type&,
  type_ptr auto&,
  type_processing_context
) noexcept {}

void process_type_impl(
  hlir::bool_type&,
  type_ptr auto&,
  type_processing_context
) noexcept {}

void process_type_impl(
  hlir::fn_type& type,
  type_ptr auto&,
  type_processing_context ctx
) {
  for(auto& param: type.param_types) {
    process_type(param, ctx);
  }
  process_type(type.return_type, ctx);
}

void process_type_impl(
  hlir::ptr_type& type,
  type_ptr auto&,
  type_processing_context ctx
) {
  process_type(type.pointee_type, ctx);
}

template<type_ptr Ptr>
void process_type_impl(
  hlir::named_type& type,
  Ptr& ptr,
  type_processing_context ctx
) {
  auto const lookup_result{[&]() noexcept -> lexical_lookup_result {
    if(ctx.scope) {
      return ctx.scope->lookup(type.name);
    } else {
      return std::monostate{};
    }
  }()};
  assert(!lookup_result.valueless_by_exception());

  std::visit(util::overloaded{
    [](std::monostate) {},

    [&](util::non_null_ptr<function>) {
      ctx.dt.track(std::make_unique<diag::name_is_not_type>(diag_ptr{type}));
      ptr = nullptr;
    },

    [&](util::non_null_ptr<local_var>) {
      ctx.dt.track(std::make_unique<diag::name_is_not_type>(diag_ptr{type}));
      ptr = nullptr;
    },

    [&](class_field_lookup) {
      ctx.dt.track(std::make_unique<diag::name_is_not_type>(diag_ptr{type}));
      ptr = nullptr;
    },

    [&](util::non_null_ptr<class_type> cl) {
      ptr = Ptr{nycleus::make_hlir<hlir::class_ref_type>(
        std::move(type.loc), *cl)};
    }
  }, lookup_result);
}

void process_type_impl(
  hlir::class_ref_type&,
  type_ptr auto&,
  type_processing_context
) noexcept {}

void process_type(
  hlir_ptr<hlir::type>& ptr,
  type_processing_context ctx
) {
  if(ptr) {
    ptr->mutable_visit([&](auto& type) {
      process_type_impl(type, ptr, ctx);
    });
  }
}

void process_type(
  type_ref& ptr,
  type_processing_context ctx
) {
  if(ptr) {
    ptr.assume_hlir().mutable_visit([&](auto& type) {
      process_type_impl(type, ptr, ctx);
    });
  }
}

// Processing expressions //////////////////////////////////////////////////////

void process_expr_impl(
  hlir::int_literal&,
  hlir_ptr<hlir::expr>&,
  processing_context
) noexcept {}

void process_expr_impl(
  hlir::bool_literal&,
  hlir_ptr<hlir::expr>&,
  processing_context
) noexcept {}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_expr_impl(
  hlir::named_var_ref& expr,
  hlir_ptr<hlir::expr>& ptr,
  processing_context ctx
) {
  auto const lookup_result{ctx.scope.lookup(expr.name)};
  assert(!lookup_result.valueless_by_exception());

  std::visit(util::overloaded{
    [](std::monostate) {},

    [&](util::non_null_ptr<function> fn) {
      ptr = nycleus::make_hlir<hlir::function_ref>(std::move(expr.loc), *fn);
    },

    [&](util::non_null_ptr<local_var> var) {
      ptr = nycleus::make_hlir<hlir::local_var_ref>(std::move(expr.loc), *var);
      if(var->fn != &ctx.fn) {
        ctx.dt.track<diag::local_var_wrong_function>(diag_ptr{*ptr},
          *var, ctx.fn);
      }
    },

    [&](class_field_lookup) {
      ctx.dt.track<diag::name_is_not_value>(diag_ptr{expr});
      ptr = nullptr;
    },

    [&](util::non_null_ptr<class_type>) {
      ctx.dt.track<diag::name_is_not_value>(diag_ptr{expr});
      ptr = nullptr;
    }
  }, lookup_result);
}

void process_expr_impl(
  hlir::global_var_ref&,
  hlir_ptr<hlir::expr>&,
  processing_context
) noexcept {}

void process_expr_impl(
  hlir::local_var_ref&,
  hlir_ptr<hlir::expr>&,
  processing_context
) noexcept {}

void process_expr_impl(
  hlir::function_ref&,
  hlir_ptr<hlir::expr>&,
  processing_context
) noexcept {}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_expr_impl(
  hlir::cmp_expr& expr,
  hlir_ptr<hlir::expr>&,
  processing_context ctx
) {
  process_expr(expr.first_op, ctx);
  for(auto& rel: expr.rels) {
    process_expr(rel.op, ctx);
  }
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_expr_impl(
  hlir::binary_op_expr& expr,
  hlir_ptr<hlir::expr>&,
  processing_context ctx
) {
  process_expr(expr.left, ctx);
  process_expr(expr.right, ctx);
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_expr_impl(
  hlir::unary_op_expr& expr,
  hlir_ptr<hlir::expr>&,
  processing_context ctx
) {
  process_expr(expr.op, ctx);
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_expr_impl(
  hlir::call_expr& expr,
  hlir_ptr<hlir::expr>&,
  processing_context ctx
) {
  process_expr(expr.callee, ctx);
  for(auto& arg: expr.args) {
    process_expr(arg.value, ctx);
  }
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_block(
  hlir::block& block,
  processing_context ctx
) {
  std::vector<util::non_null_ptr<local_var>> local_vars;

  struct nested_function {
    hlir_ptr<hlir::fn_def> def;
    std::u8string name;
    std::unique_ptr<function> fn;
  };
  std::vector<nested_function> nested_fns;

  struct local_class {
    hlir_ptr<hlir::class_def> def;
    std::u8string name;
    std::unique_ptr<class_type> cl;
  };
  std::vector<local_class> local_classes;

  using vec_size_t = std::vector<hlir_ptr<hlir::stmt>>::size_type;
  using vec_diff_t = std::vector<hlir_ptr<hlir::stmt>>::difference_type;
  vec_size_t stmt_index{0};
  while(stmt_index < block.stmts.size()) {
    auto& stmt_ptr{block.stmts[stmt_index]};
    auto* const def_stmt{dynamic_cast<hlir::def_stmt*>(stmt_ptr.get())};
    if(!def_stmt || !def_stmt->value) {
      ++stmt_index;
      continue;
    }

    switch(def_stmt->value->get_kind()) {
      case hlir::def::kind_t::fn_def: {
        nested_fns.push_back(nested_function{
          .def = std::move(def_stmt->value).static_downcast<hlir::fn_def>(),
          .name = std::move(def_stmt->name),
          .fn = std::make_unique<function>()
        });

        std::u8string symbol_name{nested_fns.back().name};
        if(!ctx.scope.add_symbol(std::move(symbol_name),
            util::non_null_ptr{nested_fns.back().fn.get()})) {
          ctx.dt.track<diag::duplicate_name_in_block>(std::move(symbol_name),
            std::vector{std::move(nested_fns.back().def->core_loc)});
        }

        block.stmts.erase(block.stmts.cbegin()
          + static_cast<vec_diff_t>(stmt_index));
        // Do not increment the index.

        break;
      }

      case hlir::def::kind_t::var_def: {
        auto& var_def{static_cast<hlir::var_def&>(*def_stmt->value)};

        ctx.fn.local_vars.push_back(std::make_unique<local_var>(
          def_stmt->name,
          ctx.fn,
          std::move(var_def.type),
          var_def.mutable_spec == hlir::var_def::mutable_spec_t::mut,
          false
        ));
        auto& var{*ctx.fn.local_vars.back()};
        local_vars.emplace_back(&var);
        if(!ctx.scope.add_symbol(std::move(def_stmt->name), util::non_null_ptr{&var})) {
          ctx.dt.track<diag::duplicate_name_in_block>(std::move(def_stmt->name),
            std::vector{std::move(var_def.core_loc)});
        }

        if(!var_def.init) {
          ctx.dt.track<diag::uninitialized_variables_unsupported>(
            diag_ptr{var_def});
        }

        stmt_ptr = nycleus::make_hlir<hlir::local_var_init_stmt>(var,
          std::move(var_def.init));
        ++stmt_index;

        break;
      }

      case hlir::def::kind_t::class_def: {
        local_classes.push_back(local_class{
          .def = std::move(def_stmt->value).static_downcast<hlir::class_def>(),
          .name = std::move(def_stmt->name),
          .cl = std::make_unique<class_type>()
        });

        std::u8string symbol_name{local_classes.back().name};
        if(!ctx.scope.add_symbol(std::move(symbol_name),
            util::non_null_ptr{local_classes.back().cl.get()})) {
          ctx.dt.track<diag::duplicate_name_in_block>(std::move(symbol_name),
            std::vector{std::move(local_classes.back().def->core_loc)});
        }

        block.stmts.erase(block.stmts.cbegin()
          + static_cast<vec_diff_t>(stmt_index));
        // Do not increment the index.

        break;
      }

      default:
        util::unreachable();
    }
  }

  for(auto& nested_fn: nested_fns) {
    global_name name{ctx.fn.name};
    name.nested_parts.emplace_back(global_name::in_function{ctx.nested_index++,
      std::move(nested_fn.name)});
    process_fn_def(std::move(nested_fn.fn), *nested_fn.def, std::move(name),
      ctx.bag, ctx.dt, &ctx.scope);
  }
  nested_fns.clear();

  for(auto const var: local_vars) {
    process_type(var->type, {ctx.bag, ctx.dt, &ctx.scope});
  }
  local_vars.clear();

  for(auto& local_cl: local_classes) {
    global_name name{ctx.fn.name};
    name.nested_parts.emplace_back(global_name::in_function{ctx.nested_index++,
      std::move(local_cl.name)});
    process_class_def(std::move(local_cl.cl), *local_cl.def, std::move(name),
      ctx.bag, ctx.dt, &ctx.scope);
  }
  local_classes.clear();

  for(auto& stmt: block.stmts) {
    process_stmt(*stmt, ctx);
  }

  if(block.trail) {
    process_expr(block.trail, ctx);
  }
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_expr_impl(
  hlir::block& expr,
  hlir_ptr<hlir::expr>&,
  processing_context ctx
) {
  simple_scope scope{&ctx.scope};
  process_block(expr, {ctx.bag, ctx.dt, scope, ctx.fn, ctx.nested_index});
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_expr(
  hlir_ptr<hlir::expr>& ptr,
  processing_context ctx
) {
  if(ptr) {
    ptr->mutable_visit([&](auto& expr) {
      process_expr_impl(expr, ptr, ctx);
    });
  }
}

// Processing statements ///////////////////////////////////////////////////////

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_stmt_impl(
  hlir::expr_stmt& stmt,
  processing_context ctx
) {
  process_expr(stmt.content, ctx);
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_stmt_impl(
  hlir::assign_stmt& stmt,
  processing_context ctx
) {
  process_expr(stmt.target, ctx);
  process_expr(stmt.value, ctx);
}

[[noreturn]] void process_stmt_impl(
  hlir::def_stmt&,
  processing_context
) noexcept {
  util::unreachable();
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_stmt_impl(
  hlir::local_var_init_stmt& stmt,
  processing_context ctx
) {
  process_expr(stmt.value, ctx);
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_stmt(
  hlir::stmt& stmt,
  processing_context ctx
) {
  stmt.mutable_visit([&](auto& stmt) {
    process_stmt_impl(stmt, ctx);
  });
}

// Processing definitions //////////////////////////////////////////////////////

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
void process_fn_def(
  std::unique_ptr<function> fn,
  hlir::fn_def& fn_def,
  global_name name,
  entity_bag& bag,
  diagnostic_tracker& dt,
  simple_scope* parent_scope
) {
  assert(fn);
  assert(!name.is_initializer);

  fn->name = std::move(name);

  fn->core_loc = std::move(fn_def.core_loc);

  using fn_size_t = std::vector<std::unique_ptr<local_var>>::size_type;
  try {
    fn->params.reserve(util::throwing_cast<fn_size_t>(fn_def.params.size()));
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }

  for(auto&& [index, param_def]: fn_def.params | util::enumerate) {
    std::u8string diag_name;
    if(param_def.name) {
      diag_name = *param_def.name;
    } else {
      diag_name = util::format<u8"(parameter {})">(index + 1);
    }

    fn->params.push_back(function::param{
      .core_loc = std::move(param_def.core_loc),
      .var = std::make_unique<local_var>(std::move(diag_name), *fn,
        type_ref{std::move(param_def.type)}, false, !param_def.name.has_value())
    });

    process_type(fn->params.back().var->type, {bag, dt, parent_scope});
  }

  if(fn_def.return_type) {
    fn->return_type = type_ref{std::move(fn_def.return_type)};
    process_type(fn->return_type, {bag, dt, parent_scope});
  }

  if(fn_def.body) {
    fn->body = std::move(fn_def.body);

    std::uint64_t nested_index{0};
    simple_scope scope{parent_scope};

    for(auto&& [param_def, param]: util::zip(fn_def.params, fn->params)) {
      if(param_def.name) {
        if(!scope.add_symbol(std::move(*param_def.name),
            util::non_null_ptr{param.var.get()})) {
          // TODO: Print other parameter locations
          dt.track<diag::duplicate_param_name>(*param_def.name,
            std::vector{param.core_loc});
        }
      }
    }

    process_block(*fn->body, {bag, dt, scope, *fn, nested_index});
  }

  bag.add(std::move(fn));
}

void process_global_var_def(
  std::unique_ptr<global_var> var,
  hlir::var_def& var_def,
  global_name name,
  entity_bag& bag,
  diagnostic_tracker& dt
) {
  assert(var);
  assert(name.nested_parts.empty());
  assert(!name.is_initializer);

  var->name = std::move(name);
  var->core_loc = std::move(var_def.core_loc);
  var->type = type_ref{std::move(var_def.type)};
  var->is_mutable = var_def.mutable_spec == hlir::var_def::mutable_spec_t::mut;

  process_type(var->type, {bag, dt, nullptr});

  if(var_def.init) {
    auto initializer{std::make_unique<function>()};

    initializer->name = var->name;
    initializer->name.is_initializer = true;
    initializer->core_loc = var_def.init->loc;

    initializer->body = nycleus::make_hlir<hlir::block>(var_def.init->loc);
    initializer->body->trail = std::move(var_def.init);

    var->initializer = initializer.get();
    initializer->initializee = var.get();

    std::uint64_t nested_index{0};
    simple_scope scope;
    process_block(*initializer->body,
      {bag, dt, scope, *initializer, nested_index});

    bag.add(std::move(initializer));
  } else {
    var->initializer = nullptr;
    dt.track(std::make_unique<diag::uninitialized_global_var>(var->core_loc));
  }

  bag.add(std::move(var));
}

void process_class_def(
  std::unique_ptr<class_type> cl,
  hlir::class_def& class_def,
  global_name name,
  entity_bag& bag,
  diagnostic_tracker& dt,
  lexical_scope* parent_scope
) {
  assert(cl);
  assert(!name.is_initializer);

  cl->name = std::move(name);
  cl->core_loc = std::move(class_def.core_loc);

  auto const field_count_raw{util::make_unsigned(
    std::ranges::count_if(class_def.members,
      [](hlir_ptr<hlir::def> const& member) noexcept -> bool {
        return dynamic_cast<hlir::var_def const*>(member.get())
          != nullptr;
      },
      util::pair_projector_second{}
    )
  )};

  auto const field_count{[&]() noexcept {
    try {
      return util::throwing_cast<field_index_t>(field_count_raw);
    } catch(std::overflow_error const&) {
      std::throw_with_nested(std::bad_alloc{});
    }
  }()};

  try {
    cl->fields.reserve(
      util::throwing_cast<std::vector<type*>::size_type>(field_count));
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }

  struct nested_class {
    hlir_ptr<hlir::class_def> def;
    std::u8string name;
    std::unique_ptr<class_type> cl;
  };
  std::vector<nested_class> nested_classes;
  try {
    nested_classes.reserve(
      util::throwing_cast<std::vector<nested_class>::size_type>(
        util::make_unsigned(std::ranges::count_if(class_def.members,
          [](hlir_ptr<hlir::def> const& member) noexcept -> bool {
            return dynamic_cast<hlir::class_def const*>(member.get())
              != nullptr;
          },
          util::pair_projector_second{}
        ))
      )
    );
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }

  for(auto& [name, member]: class_def.members) {
    if(!member) {
      continue;
    }

    switch(member->get_kind()) {
      case hlir::def::kind_t::var_def: {
        auto& var_def{static_cast<hlir::var_def&>(*member)};

        if(var_def.init) {
          dt.track(std::make_unique<diag::class_field_initializers_unsupported>(
            diag_ptr{*var_def.init}));
        }

        field_index_t index;
        if constexpr(sizeof(std::vector<type*>::size_type) > 4) {
          try {
            index = util::throwing_cast<field_index_t>(cl->fields.size());
          } catch(std::overflow_error const&) {
            std::throw_with_nested(std::bad_alloc{});
          }
        } else {
          index = cl->fields.size();
        }

        bool const is_new{cl->scope.add_symbol(std::u8string{name},
          class_field_lookup{*cl, index})};
        if(!is_new) {
          // TODO: List other duplicate class members
          dt.track(std::make_unique<diag::duplicate_class_member>(
            std::move(name), std::vector{member->core_loc}));
        }

        cl->fields.emplace_back(
          type_ref{std::move(var_def.type)},
          var_def.mutable_spec != hlir::var_def::mutable_spec_t::constant,
          std::move(name),
          std::move(member->core_loc)
        );
        break;
      }

      case hlir::def::kind_t::fn_def: {
        auto const& fn_def{static_cast<hlir::fn_def const&>(*member)};
        dt.track(std::make_unique<diag::methods_unsupported>(diag_ptr{fn_def}));
        break;
      }

      case hlir::def::kind_t::class_def: {
        nested_classes.push_back(nested_class{
          .def = std::move(member).static_downcast<hlir::class_def>(),
          .name = std::move(name),
          .cl = std::make_unique<class_type>()
        });

        std::u8string nested_name{nested_classes.back().name};
        bool const is_new{cl->scope.add_symbol(std::move(nested_name),
          util::non_null_ptr{nested_classes.back().cl.get()})};
        if(!is_new) {
          dt.track(std::make_unique<diag::duplicate_class_member>(
            std::move(nested_name), std::vector{member->core_loc}));
        }

        break;
      }

      default: util::unreachable();
    }
  }

  lexical_class_scope scope{cl->scope, parent_scope};

  for(auto& field: cl->fields) {
    process_type(field.type, {bag, dt, &scope});
  }

  for(auto& nested_class: nested_classes) {
    global_name name{cl->name};
    name.nested_parts.emplace_back(global_name::in_class{
      std::move(nested_class.name)});
    process_class_def(std::move(nested_class.cl), *nested_class.def,
      std::move(name), bag, dt, &scope);
  }

  bag.add(std::move(cl));
}

} // (anonymous)

void entity_bag::process_global_def(
  hlir_ptr<hlir::def> def,
  global_name name,
  diagnostic_tracker& dt
) {
  assert(def);
  assert(name.nested_parts.empty());
  assert(!name.is_initializer);

  def->mutable_visit(util::overloaded{
    [&](hlir::fn_def& fn_def) {
      process_fn_def(std::make_unique<function>(), fn_def, std::move(name),
        *this, dt, nullptr);
    },

    [&](hlir::var_def& var_def) {
      process_global_var_def(std::make_unique<global_var>(), var_def,
        std::move(name), *this, dt);
    },

    [&](hlir::class_def& class_def) {
      process_class_def(std::make_unique<class_type>(), class_def,
        std::move(name), *this, dt, nullptr);
    }
  });
}
