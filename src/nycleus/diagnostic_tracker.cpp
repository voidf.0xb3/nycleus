#include "nycleus/diagnostic_tracker.hpp"
#include "nycleus/diagnostic.hpp"
#include <memory>
#include <utility>

void nycleus::diagnostic_tracker::track(
  std::unique_ptr<nycleus::diagnostic> d
) {
  if(dynamic_cast<error const*>(d.get())) {
    has_errors_ = true;
  }
  this->do_track(std::move(d));
}
