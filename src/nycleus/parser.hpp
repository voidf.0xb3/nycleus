#ifndef NYCLEUS_PARSER_HPP_INCLUDED_7K4WL4A2PTRFSMXYYA82RIOQY
#define NYCLEUS_PARSER_HPP_INCLUDED_7K4WL4A2PTRFSMXYYA82RIOQY

#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_tracker.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/token.hpp"
#include "nycleus/utils.hpp"
#include "unicode/encoding.hpp"
#include "unicode/props.hpp"
#include "util/small_vector.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <cassert>
#include <concepts>
#include <exception>
#include <functional>
#include <ranges>
#include <string_view>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

namespace nycleus {

namespace detail_ {

/** @brief Thrown if the parser encounters an error that blocks it from
 * continuing the parse. */
class parser_fatal_error: public std::exception {
public:
  [[nodiscard]] virtual gsl::czstring what() const noexcept override;
};

/** @brief Determines whether the given expression needs to be followed by a
 * semicolon to form an expression statement.
 *
 * Used when parsing blocks to determine if a statement fits as a trailing
 * expression. */
[[nodiscard]] inline bool needs_semicolon(hlir::expr const& expr) noexcept {
  return expr.visit(util::overloaded{
    [&](hlir::block const&) noexcept { return false; },
    [&](auto const&) noexcept { return true; }
  });
}

// TODO: Error recovery in parser
// TODO: Better error messages about ambiguous operator precedence/associativity

template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<std::ranges::range_value_t<Range&&>, token>
class parser {
private:
  std::ranges::iterator_t<Range&&> it_;
  std::ranges::sentinel_t<Range&&> iend_;
  diagnostic_tracker& dt_;
  token tok_;
  file_pos last_;

  void next_token() {
    do {
      last_ = tok_.location.last;

      if(it_ == iend_) {
        tok_ = {tok::eof{}, {std::move(tok_.location.file), tok_.location.last,
          tok_.location.last}};
        return;
      }

      tok_ = *it_++;
      if(tok_.diagnostics) {
        for(diagnostic_box& diag: tok_.diagnostics->modify()) {
          dt_.track(std::move(diag).extract());
        }
        tok_.diagnostics.reset();
      }
    } while(std::holds_alternative<tok::comment>(tok_.tok));
  }

  /** @brief Tests whether a word token matches the given keyword. */
  [[nodiscard]] static bool is_keyword(
    tok::word const& tok,
    std::u8string_view pattern
  ) noexcept {
    return tok.sigil == tok::word::sigil_t::none && *tok.text == pattern;
  }

  /** @brief Tests whether a word token matches a keyword that starts a
   * definition. */
  [[nodiscard]] static bool is_def_keyword(tok::word const& tok) noexcept {
    return tok.sigil == tok::word::sigil_t::none
      && (*tok.text == u8"let"
      || *tok.text == u8"fn"
      || *tok.text == u8"class");
  }

public:
  explicit parser(Range&& range, diagnostic_tracker& dt) noexcept(
    noexcept(std::ranges::begin(range))
    && noexcept(std::ranges::end(range))
    && std::is_nothrow_move_constructible_v<
      std::ranges::iterator_t<Range&&>>
    && std::is_nothrow_move_constructible_v<
      std::ranges::sentinel_t<Range&&>>
  ):
    it_{std::ranges::begin(range)},
    iend_{std::ranges::end(range)},
    dt_{dt},
    tok_{tok::eof{}}
  {
    next_token();
  }

  parser(parser const&) = delete;
  parser(parser&&) = delete;
  parser& operator=(parser const&) = delete;
  parser& operator=(parser&&) = delete;
  ~parser() noexcept = default;

  /** @brief An implementation of the `parse` function which has access to the
   * parser state.
   *
   * Also throws whatever is thrown by the callback or by iterator operations.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  template<std::invocable<std::u8string&&, hlir_ptr<hlir::def>&&> Callback>
  void parse(Callback&& report_parse_result) {
    bool seen_stuff{false};
    while(!std::holds_alternative<tok::eof>(tok_.tok)) {
      std::visit(util::overloaded{
        [&](tok::word tok) {
          if(is_keyword(tok, u8"version")) {
            if(seen_stuff) {
              dt_.track(std::make_unique<diag::unexpected_version>(
                tok_.location));
              throw parser_fatal_error{};
            }

            next_token();
            if(!std::holds_alternative<tok::int_literal>(tok_.tok)) {
              dt_.track(std::make_unique<diag::bad_version>(tok_.location));
              throw parser_fatal_error{};
            }

            {
              tok::int_literal const lit{std::get<tok::int_literal>(tok_.tok)};

              if(lit.type_suffix) {
                dt_.track(std::make_unique<diag::version_type_suffix>(
                  tok_.location));
                throw parser_fatal_error{};
              }

              if(*lit.num != 0) {
                dt_.track(std::make_unique<diag::bad_version>(tok_.location));
                throw parser_fatal_error{};
              }
            }

            next_token();
            if(!std::holds_alternative<tok::semicolon>(tok_.tok)) {
              dt_.track(std::make_unique<diag::version_semicolon_expected>(
                tok_.location));
              throw parser_fatal_error{};
            }

            next_token();
          }

          else if(parser::is_def_keyword(tok)) {
            auto [name, def] = parse_def();
            assert(def);
            std::invoke(report_parse_result, std::move(name), std::move(def));
          }

          else {
            dt_.track(std::make_unique<diag::def_or_directive_expected>(
              tok_.location));
            throw parser_fatal_error{};
          }

          seen_stuff = true;
        },

        [&](tok::eof) noexcept { util::unreachable(); },

        [&](auto const&) {
          dt_.track(std::make_unique<diag::def_or_directive_expected>(
            tok_.location));
          throw parser_fatal_error{};
        }
      }, tok_.tok);
    }
  }

private:
  // Types /////////////////////////////////////////////////////////////////////

  /** @brief Parses a type name.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::type> parse_type() {
    if(std::holds_alternative<tok::logical_and>(tok_.tok)) {
      source_location first_level_loc{
        std::move(std::get_if<tok::logical_and>(&tok_.tok)->second_amp_loc)};
      source_location second_level_loc{std::move(tok_.location)};

      // Eat the operator
      next_token();

      // Eat the "mut" or "const", if any
      bool is_mutable{false};
      if(auto const* const w{std::get_if<tok::word>(&tok_.tok)};
          w && w->sigil == tok::word::sigil_t::none) {
        if(*w->text == u8"mut") {
          is_mutable = true;
          next_token();
        } else if(*w->text == u8"const") {
          next_token();
        }
      }

      // Eat the pointee type
      auto pointee{parse_type()};
      assert(pointee);

      first_level_loc.last = pointee->loc.last;
      second_level_loc.last = pointee->loc.last;
      auto first_level{nycleus::make_hlir<hlir::ptr_type>(
        std::move(first_level_loc), std::move(pointee), is_mutable)};
      return nycleus::make_hlir<hlir::ptr_type>(
        std::move(second_level_loc), std::move(first_level), false);
    }

    if(std::holds_alternative<tok::amp>(tok_.tok)) {
      source_location loc{std::move(tok_.location)};

      // Eat the operator
      next_token();

      // Eat the "mut" or "const", if any
      bool is_mutable{false};
      if(auto const* const w{std::get_if<tok::word>(&tok_.tok)};
          w && w->sigil == tok::word::sigil_t::none) {
        if(*w->text == u8"mut") {
          is_mutable = true;
          next_token();
        } else if(*w->text == u8"const") {
          next_token();
        }
      }

      // Eat the pointee type
      auto pointee{parse_type()};
      assert(pointee);

      loc.last = pointee->loc.last;
      return nycleus::make_hlir<hlir::ptr_type>(std::move(loc),
        std::move(pointee), is_mutable);
    }

    tok::word const* const w{std::get_if<tok::word>(&tok_.tok)};
    if(!w) {
      dt_.track(std::make_unique<diag::type_name_expected>(tok_.location));
      throw parser_fatal_error{};
    }

    if(w->sigil == tok::word::sigil_t::dollar) {
      source_location loc{std::move(tok_.location)};
      auto name{w->text};
      next_token();
      return nycleus::make_hlir<hlir::named_type>(std::move(loc),
        std::move(name.modify()));
    }

    if(w->sigil != tok::word::sigil_t::none) {
      dt_.track(std::make_unique<diag::type_name_expected>(tok_.location));
      throw parser_fatal_error{};
    }

    if(
      w->text->size() >= 2
      && (w->text->front() == u8's' || w->text->front() == u8'u')
      && std::ranges::all_of(
        *w->text | std::views::drop(1) | unicode::utf8_decode(),
        [](char32_t ch) noexcept {
          return unicode::decimal_value(ch) != unicode::no_decimal_value;
        }
      )
    ) {
      source_location loc{std::move(tok_.location)};
      int_width_t width{0};
      for(char32_t const ch: *w->text | std::views::drop(1)
          | unicode::utf8_decode()) {
        width = width * 10 + unicode::decimal_value(ch).unwrap();
        if(width > max_int_width) {
          dt_.track(std::make_unique<diag::int_type_too_wide>(tok_.location));
          width = max_int_width;
          break;
        }
      }
      if(width == 0) {
        dt_.track(std::make_unique<diag::int_type_zero_width>(tok_.location));
        width = 1;
      }
      bool const is_signed{w->text->front() == u8's'};
      next_token();
      return nycleus::make_hlir<hlir::int_type>(std::move(loc), width,
        is_signed);
    } else if(*w->text == u8"bool") {
      source_location loc{std::move(tok_.location)};
      next_token();
      return nycleus::make_hlir<hlir::bool_type>(std::move(loc));
    } else if(*w->text == u8"fn") {
      source_location loc{std::move(tok_.location)};
      next_token();
      return parse_fn_type_trail(std::move(loc));
    } else {
      dt_.track(std::make_unique<diag::type_name_expected>(tok_.location));
      throw parser_fatal_error{};
    }
  }

  /** @brief Parses the trailing part of a function type name (without the "fn"
   * keyword).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::fn_type> parse_fn_type_trail(
    source_location start_loc
  ) {
    file_pos last_pos;

    // Eat the left parenthesis
    if(!std::holds_alternative<tok::lparen>(tok_.tok)) {
      dt_.track(std::make_unique<diag::fn_type_lparen_expected>(tok_.location));
      throw parser_fatal_error{};
    }
    next_token();

    // Eat the second left parenthesis, if one exists
    bool extra_paren{false};
    if(std::holds_alternative<tok::lparen>(tok_.tok)) {
      extra_paren = true;
      next_token();
    }

    // Eat the parameters
    std::vector<hlir_ptr<hlir::type>> param_types;
    for(;;) {
      // Eat the right parenthesis, if any
      if(std::holds_alternative<tok::rparen>(tok_.tok)) {
        break;
      }

      // Eat the parameter name, if any
      bool seen_name{false};
      if(tok::word const* const w{std::get_if<tok::word>(&tok_.tok)};
          w && (w->sigil == tok::word::sigil_t::dollar
          || w->sigil == tok::word::sigil_t::none)) {
        seen_name = true;
        next_token();
      }

      // Eat the colon
      if(!std::holds_alternative<tok::colon>(tok_.tok)) {
        if(seen_name) {
          dt_.track(std::make_unique<diag::fn_type_param_type_expected>(
            tok_.location));
        } else {
          dt_.track(std::make_unique<diag::fn_type_param_name_expected>(
            tok_.location));
        }
        throw parser_fatal_error{};
      }
      next_token();

      // Eat the type name
      param_types.emplace_back(parse_type());
      assert(param_types.back());

      // Eat the right parenthesis or the comma
      if(std::holds_alternative<tok::rparen>(tok_.tok)) {
        break;
      }
      if(!std::holds_alternative<tok::comma>(tok_.tok)) {
        dt_.track(std::make_unique<diag::fn_type_param_next_expected>(
          tok_.location));
        throw parser_fatal_error{};
      }
      next_token();
    }
    if(!extra_paren) {
      last_pos = tok_.location.last;
    }
    next_token();

    hlir_ptr<hlir::type> return_type;
    if(extra_paren) {
      // Eat the return type, if any
      if(std::holds_alternative<tok::colon>(tok_.tok)) {
        next_token();
        return_type = parse_type();
        assert(return_type);
      }

      // Eat the right parenthesis
      if(!std::holds_alternative<tok::rparen>(tok_.tok)) {
        dt_.track(std::make_unique<diag::fn_type_rparen_expected>(
          tok_.location));
        throw parser_fatal_error{};
      }
      last_pos = tok_.location.last;
      next_token();
    }

    return nycleus::make_hlir<hlir::fn_type>(
      source_location{std::move(start_loc.file), start_loc.first, last_pos},
      std::move(param_types), std::move(return_type));
  }

  // Expressions ///////////////////////////////////////////////////////////////

  /** @brief Parses an expression.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_expr() {
    source_location loc{tok_.location};
    hlir_ptr<hlir::expr> result{parse_cmp_expr()};
    assert(result);

    std::visit(util::overloaded{
      [&](tok::logical_or) {
        result = this->parse_or_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::logical_and) {
        result = this->parse_or_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::logical_xor) {
        result = this->parse_xor_expr_trail(std::move(loc), std::move(result));
      },

      [](auto const&) noexcept {}
    }, tok_.tok);

    return result;
  }

  /** @brief Parses the trailing part of an expression containing logical OR
   * operators, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_or_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{
      this->parse_and_expr_trail(start_loc, std::move(left))};
    assert(result);

    for(;;) {
      if(!std::holds_alternative<tok::logical_or>(tok_.tok)) {
        break;
      }
      next_token();

      source_location and_expr_start{tok_.location};
      hlir_ptr<hlir::expr> right{this->parse_and_expr_trail(
        std::move(and_expr_start), parse_cmp_expr())};
      assert(right);

      result = nycleus::make_hlir<hlir::or_expr>(
        source_location{start_loc.file, start_loc.first, last_},
        std::move(result), std::move(right));
    }

    return result;
  }

  /** @brief Parses the trailing part of an expression containing logical AND
   * operators, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_and_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{std::move(left)};

    for(;;) {
      if(!std::holds_alternative<tok::logical_and>(tok_.tok)) {
        break;
      }
      next_token();

      hlir_ptr<hlir::expr> right{parse_cmp_expr()};
      assert(right);

      result = nycleus::make_hlir<hlir::and_expr>(
        source_location{start_loc.file, start_loc.first, last_},
        std::move(result), std::move(right));
    }

    return result;
  }

  /** @brief Parses the trailing part of an expression containing logical XOR
   * operators, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_xor_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{std::move(left)};

    for(;;) {
      if(!std::holds_alternative<tok::logical_xor>(tok_.tok)) {
        break;
      }
      next_token();

      hlir_ptr<hlir::expr> right{parse_cmp_expr()};
      assert(right);

      result = nycleus::make_hlir<hlir::xor_expr>(
        source_location{start_loc.file, start_loc.first, last_},
        std::move(result), std::move(right));
    }

    return result;
  }

  /** @brief Parses an expression containing comparison operators.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_cmp_expr() {
    source_location loc{tok_.location};
    hlir_ptr<hlir::expr> result{parse_arith_expr()};
    assert(result);

    std::visit(util::overloaded{
      [&](tok::eq) {
        result = this->parse_cmp_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::lt) {
        result = this->parse_cmp_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::leq) {
        result = this->parse_cmp_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::gt) {
        result = this->parse_cmp_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::geq) {
        result = this->parse_cmp_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::neq) {
        result = this->parse_neq_expr_trail(std::move(loc), std::move(result));
      },

      [](auto const&) noexcept {}
    }, tok_.tok);

    return result;
  }

  /** @brief Parses the trailing part of an expression containing comparison
   * binary operators, starting from the first operator (having already seen the
   * leftmost operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_cmp_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> first_op
  ) {
    using namespace std::string_view_literals;

    util::small_vector<hlir::cmp_expr::rel, 1> rels;

    for(;;) {
      auto add_rel{[&](hlir::cmp_expr::rel_kind kind) {
        source_location loc{std::move(tok_.location)};
        next_token();
        rels.emplace_back(kind, parse_arith_expr(), std::move(loc));
      }};

      if(std::holds_alternative<tok::eq>(tok_.tok)) {
        add_rel(hlir::cmp_expr::rel_kind::eq);
      } else if(std::holds_alternative<tok::lt>(tok_.tok)) {
        add_rel(hlir::cmp_expr::rel_kind::lt);
      } else if(std::holds_alternative<tok::leq>(tok_.tok)) {
        add_rel(hlir::cmp_expr::rel_kind::le);
      } else if(std::holds_alternative<tok::gt>(tok_.tok)) {
        add_rel(hlir::cmp_expr::rel_kind::gt);
      } else if(std::holds_alternative<tok::geq>(tok_.tok)) {
        add_rel(hlir::cmp_expr::rel_kind::ge);
      } else {
        break;
      }
    }

    assert(!rels.empty());

    auto const less_rel{std::ranges::find_if(rels,
      [](hlir::cmp_expr::rel const& rel) noexcept {
        return rel.kind == hlir::cmp_expr::rel_kind::lt
          || rel.kind == hlir::cmp_expr::rel_kind::le;
      })};
    auto const greater_rel{std::ranges::find_if(rels,
      [](hlir::cmp_expr::rel const& rel) noexcept {
        return rel.kind == hlir::cmp_expr::rel_kind::gt
          || rel.kind == hlir::cmp_expr::rel_kind::ge;
      })};

    source_location loc{start_loc.file, start_loc.first, last_};

    if(less_rel != rels.cend() && greater_rel != rels.cend()) {
      dt_.track(std::make_unique<diag::bad_comparison_chain>(
        loc,
        less_rel->loc, greater_rel->loc,
        less_rel->kind == hlir::cmp_expr::rel_kind::lt,
        greater_rel->kind == hlir::cmp_expr::rel_kind::gt,
        less_rel < greater_rel
      ));
    }

    return nycleus::make_hlir<hlir::cmp_expr>(std::move(loc),
      std::move(first_op), std::move(rels));
  }

  /** @brief Parses the trailing part of an expression containing an inequality
   * binary operator, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_neq_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    // Eat the "!="
    assert(std::holds_alternative<tok::neq>(tok_.tok));
    next_token();

    // Eat the right operand
    hlir_ptr<hlir::expr> right{parse_arith_expr()};

    return nycleus::make_hlir<hlir::neq_expr>(
      source_location{start_loc.file, start_loc.first, last_},
      std::move(left), std::move(right));
  }

  /** @brief Parses an expression containing arithmetic or bitwise operators.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_arith_expr() {
    source_location loc{tok_.location};
    hlir_ptr<hlir::expr> result{parse_prefix_unary_expr()};
    assert(result);

    std::visit(util::overloaded{
      [&](tok::plus) {
        result = this->parse_add_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::minus) {
        result = this->parse_add_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::star) {
        result = this->parse_add_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::slash) {
        result = this->parse_add_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::percent) {
        result = this->parse_add_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::pipe) {
        result = this->parse_bor_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::amp) {
        result = this->parse_bor_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::bitwise_xor) {
        result = this->parse_bxor_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::lshift) {
        result = this->parse_bsh_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::rshift) {
        result = this->parse_bsh_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::lrot) {
        result = this->parse_brot_expr_trail(std::move(loc), std::move(result));
      },

      [&](tok::rrot) {
        result = this->parse_brot_expr_trail(std::move(loc), std::move(result));
      },

      [](auto const&) noexcept {}
    }, tok_.tok);

    return result;
  }

  /** @brief Parses the trailing part of an expression containing additive
   * binary operators, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_add_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{
      this->parse_mul_expr_trail(start_loc, std::move(left))};
    assert(result);

    for(;;) {
      bool const done{std::visit(util::overloaded{
        [&](tok::plus) {
          next_token();

          source_location mul_expr_start{tok_.location};
          hlir_ptr<hlir::expr> right{this->parse_mul_expr_trail(
            std::move(mul_expr_start), parse_prefix_unary_expr())};
          assert(right);

          source_location loc{start_loc.file, start_loc.first, last_};
          result = nycleus::make_hlir<hlir::add_expr>(std::move(loc),
            std::move(result), std::move(right));
          return false;
        },

        [&](tok::minus) {
          next_token();

          source_location mul_expr_start{tok_.location};
          hlir_ptr<hlir::expr> right{this->parse_mul_expr_trail(
            std::move(mul_expr_start), parse_prefix_unary_expr())};
          assert(right);

          source_location loc{start_loc.file, start_loc.first, last_};
          result = nycleus::make_hlir<hlir::sub_expr>(std::move(loc),
            std::move(result), std::move(right));
          return false;
        },

        [](auto const&) noexcept { return true; }
      }, tok_.tok)};
      if(done) {
        break;
      }
    }

    return result;
  }

  /** @brief Parses the trailing part of an expression containing multiplicative
   * binary operators, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_mul_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{std::move(left)};

    for(;;) {
      bool const done{std::visit(util::overloaded{
        [&](tok::star) {
          next_token();

          hlir_ptr<hlir::expr> right{parse_prefix_unary_expr()};
          assert(right);

          source_location loc{start_loc.file, start_loc.first, last_};
          result = nycleus::make_hlir<hlir::mul_expr>(std::move(loc),
            std::move(result), std::move(right));
          return false;
        },

        [&](tok::slash) {
          next_token();

          hlir_ptr<hlir::expr> right{parse_prefix_unary_expr()};
          assert(right);

          source_location loc{start_loc.file, start_loc.first, last_};
          result = nycleus::make_hlir<hlir::div_expr>(std::move(loc),
            std::move(result), std::move(right));
          return false;
        },

        [&](tok::percent) {
          next_token();

          hlir_ptr<hlir::expr> right{parse_prefix_unary_expr()};
          assert(right);

          source_location loc{start_loc.file, start_loc.first, last_};
          result = nycleus::make_hlir<hlir::mod_expr>(std::move(loc),
            std::move(result), std::move(right));
          return false;
        },

        [](auto const&) noexcept { return true; }
      }, tok_.tok)};
      if(done) {
        break;
      }
    }

    return result;
  }

  /** @brief Parses the trailing part of an expression containing bitwise OR
   * operators, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_bor_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{
      this->parse_band_expr_trail(start_loc, std::move(left))};
    assert(result);

    for(;;) {
      bool const done{std::visit(util::overloaded{
        [&](tok::pipe) {
          next_token();

          source_location band_expr_start{tok_.location};
          hlir_ptr<hlir::expr> right{this->parse_band_expr_trail(
            std::move(band_expr_start), parse_prefix_unary_expr())};
          assert(right);

          source_location loc{start_loc.file, start_loc.first, last_};
          result = nycleus::make_hlir<hlir::bor_expr>(std::move(loc),
            std::move(result), std::move(right));
          return false;
        },

        [](auto const&) noexcept { return true; }
      }, tok_.tok)};
      if(done) {
        break;
      }
    }

    return result;
  }

  /** @brief Parses the trailing part of an expression containing bitwise AND
   * operators, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_band_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{std::move(left)};

    for(;;) {
      bool const done{std::visit(util::overloaded{
        [&](tok::amp) {
          next_token();

          hlir_ptr<hlir::expr> right{parse_prefix_unary_expr()};
          assert(right);

          source_location loc{start_loc.file, start_loc.first, last_};
          result = nycleus::make_hlir<hlir::band_expr>(std::move(loc),
            std::move(result), std::move(right));
          return false;
        },

        [](auto const&) noexcept { return true; }
      }, tok_.tok)};
      if(done) {
        break;
      }
    }

    return result;
  }

  /** @brief Parses the trailing part of an expression containing bitwise XOR
   * operators, starting from the operator (having already seen the left
   * operand).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_bxor_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{std::move(left)};

    for(;;) {
      bool const done{std::visit(util::overloaded{
        [&](tok::bitwise_xor) {
          next_token();

          hlir_ptr<hlir::expr> right{parse_prefix_unary_expr()};
          assert(right);

          source_location loc{start_loc.file, start_loc.first, last_};
          result = nycleus::make_hlir<hlir::bxor_expr>(std::move(loc),
            std::move(result), std::move(right));
          return false;
        },

        [](auto const&) noexcept { return true; }
      }, tok_.tok)};
      if(done) {
        break;
      }
    }

    return result;
  }

  /** @brief Parses the trailing part of an expression containing bitwise shift
   * operators, starting from the operator (having already seen the left
   * operand).
   *
   * The operator must be a bitwise shift operator, otherwise the behavior is
   * undefined.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_bsh_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{std::move(left)};

    auto const operator_token{tok_.tok};
    next_token();

    hlir_ptr<hlir::expr> right{parse_prefix_unary_expr()};
    assert(right);

    source_location loc{start_loc.file, start_loc.first, last_};

    std::visit(util::overloaded{
      [&](tok::lshift) {
        result = nycleus::make_hlir<hlir::lsh_expr>(std::move(loc),
          std::move(result), std::move(right));
      },

      [&](tok::rshift) {
        result = nycleus::make_hlir<hlir::rsh_expr>(std::move(loc),
          std::move(result), std::move(right));
      },

      [](auto) noexcept { util::unreachable(); }
    }, operator_token);

    return result;
  }

  /** @brief Parses the trailing part of an expression containing bitwise
   * rotation operators, starting from the operator (having already seen the
   * left operand).
   *
   * The operator must be a bitwise rotation operator, otherwise the behavior is
   * undefined.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_brot_expr_trail(
    source_location start_loc,
    hlir_ptr<hlir::expr> left
  ) {
    assert(left);
    hlir_ptr<hlir::expr> result{std::move(left)};

    auto const operator_token{tok_.tok};
    next_token();

    hlir_ptr<hlir::expr> right{parse_prefix_unary_expr()};
    assert(right);

    source_location loc{start_loc.file, start_loc.first, last_};

    std::visit(util::overloaded{
      [&](tok::lrot) {
        result = nycleus::make_hlir<hlir::lrot_expr>(std::move(loc),
          std::move(result), std::move(right));
      },

      [&](tok::rrot) {
        result = nycleus::make_hlir<hlir::rrot_expr>(std::move(loc),
          std::move(result), std::move(right));
      },

      [](auto) noexcept { util::unreachable(); }
    }, operator_token);

    return result;
  }

  /** @brief Parses an expression containing prefix unary operators.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_prefix_unary_expr() {
    return std::visit(util::overloaded{
      [&](tok::minus) -> hlir_ptr<hlir::expr> {
        source_location loc{tok_.location};
        next_token();

        hlir_ptr<hlir::expr> op{parse_prefix_unary_expr()};
        assert(op);
        loc.last = last_;
        return nycleus::make_hlir<hlir::neg_expr>(std::move(loc),
          std::move(op));
      },

      [&](tok::tilde) -> hlir_ptr<hlir::expr> {
        source_location loc{tok_.location};
        next_token();

        hlir_ptr<hlir::expr> op{parse_prefix_unary_expr()};
        assert(op);
        loc.last = last_;
        return nycleus::make_hlir<hlir::bnot_expr>(std::move(loc),
          std::move(op));
      },

      [&](tok::excl) -> hlir_ptr<hlir::expr> {
        source_location loc{tok_.location};
        next_token();

        hlir_ptr<hlir::expr> op{parse_prefix_unary_expr()};
        assert(op);
        loc.last = last_;
        return nycleus::make_hlir<hlir::not_expr>(std::move(loc),
          std::move(op));
      },

      [&](tok::amp) -> hlir_ptr<hlir::expr> {
        source_location loc{tok_.location};

        // Eat the operator
        next_token();

        // Eat the "const" or "mut", if any
        bool is_mutable{false};
        if(auto const* const w{std::get_if<tok::word>(&tok_.tok)};
            w && w->sigil == tok::word::sigil_t::none) {
          if(*w->text == u8"mut") {
            is_mutable = true;
            next_token();
          } else if(*w->text == u8"const") {
            next_token();
          }
        }

        // Eat the operand
        hlir_ptr<hlir::expr> op{parse_prefix_unary_expr()};
        assert(op);

        loc.last = last_;
        return nycleus::make_hlir<hlir::addr_expr>(std::move(loc),
          std::move(op), is_mutable);
      },

      [&](tok::logical_and& t) -> hlir_ptr<hlir::expr> {
        source_location first_level_loc{std::move(t.second_amp_loc)};
        source_location second_level_loc{tok_.location};

        // Eat the operator
        next_token();

        // Eat the "mut" or "const", if any
        bool is_mutable{false};
        if(auto const* const w{std::get_if<tok::word>(&tok_.tok)};
            w && w->sigil == tok::word::sigil_t::none) {
          if(*w->text == u8"mut") {
            is_mutable = true;
            next_token();
          } else if(*w->text == u8"const") {
            next_token();
          }
        }

        // Eat the operand
        hlir_ptr<hlir::expr> op{parse_prefix_unary_expr()};
        assert(op);

        first_level_loc.last = last_;
        second_level_loc.last = last_;
        auto first_level{nycleus::make_hlir<hlir::addr_expr>(
          std::move(first_level_loc), std::move(op), is_mutable)};
        return nycleus::make_hlir<hlir::addr_expr>(
          std::move(second_level_loc), std::move(first_level), false);
      },

      [&](tok::star) -> hlir_ptr<hlir::expr> {
        source_location loc{tok_.location};
        next_token();

        hlir_ptr<hlir::expr> op{parse_prefix_unary_expr()};
        assert(op);
        loc.last = last_;
        return nycleus::make_hlir<hlir::deref_expr>(std::move(loc),
          std::move(op));
      },

      [&](auto const&) { return parse_postfix_unary_expr(); }
    }, tok_.tok);
  }

  /** @brief Parses an expression containing postfix unary operators.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_postfix_unary_expr() {
    source_location new_loc{tok_.location.file, tok_.location.first, {}};
    hlir_ptr<hlir::expr> result{parse_atomic_expr()};
    assert(result);

    bool should_break;
    do {
      should_break = std::visit(util::overloaded{
        [&](tok::lparen) -> bool {
          // Eat the left parenthesis
          next_token();

          std::vector<hlir::call_expr::arg> args;
          for(;;) {
            // Eat the right parenthesis, if any
            if(std::holds_alternative<tok::rparen>(tok_.tok)) {
              new_loc.last = tok_.location.last;
              next_token();
              break;
            }

            // Eat the argument
            args.emplace_back(parse_expr());
            assert(args.back().value);

            // Eat the right parenthesis or the comma
            if(std::holds_alternative<tok::rparen>(tok_.tok)) {
              new_loc.last = tok_.location.last;
              next_token();
              break;
            }
            if(!std::holds_alternative<tok::comma>(tok_.tok)) {
              dt_.track(std::make_unique<diag::call_expr_arg_expected>(
                tok_.location));
              throw parser_fatal_error{};
            }
            next_token();
          }

          result = nycleus::make_hlir<hlir::call_expr>(new_loc,
            std::move(result), std::move(args));

          return false;
        },

        [&](tok::point) -> bool {
          // Eat the point
          next_token();

          // Eat the name
          auto const* const w{std::get_if<tok::word>(&tok_.tok)};
          if(!w || (w->sigil != tok::word::sigil_t::dollar
              && w->sigil != tok::word::sigil_t::none)) {
            dt_.track(std::make_unique<diag::member_expr_name_expected>(
              tok_.location));
            throw parser_fatal_error{};
          }

          new_loc.last = tok_.location.last;
          auto text{w->text};
          next_token();

          result = nycleus::make_hlir<hlir::named_member_expr>(new_loc,
            std::move(result), std::move(text.modify()));

          return false;
        },

        [&](auto const&) -> bool { return true; }
      }, tok_.tok);
    } while(!should_break);
    return result;
  }

  /** @brief Parses an expression of the lowest level (containing no operators).
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::expr> parse_atomic_expr() {
    return std::visit(util::overloaded{
      [&](tok::int_literal n) -> hlir_ptr<hlir::expr> {
        source_location loc{std::move(tok_.location)};
        next_token();
        return nycleus::make_hlir<hlir::int_literal>(std::move(loc),
          std::move(n.num.modify()), n.type_suffix);
      },

      [&](tok::word w) -> hlir_ptr<hlir::expr> {
        source_location loc{std::move(tok_.location)};
        if(w.sigil == tok::word::sigil_t::none) {
          if(*w.text == u8"true") {
            next_token();
            return nycleus::make_hlir<hlir::bool_literal>(std::move(loc), true);
          } else if(*w.text == u8"false") {
            next_token();
            return nycleus::make_hlir<hlir::bool_literal>(std::move(loc),
              false);
          } else {
            dt_.track(std::make_unique<diag::expr_expected>(tok_.location));
            throw parser_fatal_error{};
          }
        }

        if(w.sigil != tok::word::sigil_t::dollar) {
          dt_.track(std::make_unique<diag::expr_expected>(tok_.location));
          throw parser_fatal_error{};
        }
        next_token();
        return nycleus::make_hlir<hlir::named_var_ref>(std::move(loc), *w.text);
      },

      [&](tok::lparen) -> hlir_ptr<hlir::expr> {
        next_token();
        hlir_ptr<hlir::expr> result{parse_expr()};
        assert(result);
        if(!std::holds_alternative<tok::rparen>(tok_.tok)) {
          dt_.track(std::make_unique<diag::expr_rparen_expected>(
            tok_.location));
          throw parser_fatal_error{};
        }
        next_token();
        return result;
      },

      [&](tok::lbrace) -> hlir_ptr<hlir::expr> {
        source_location loc{tok_.location};
        next_token();
        return parse_block_trail(std::move(loc));
      },

      [&](auto const&) -> hlir_ptr<hlir::expr> {
        dt_.track(std::make_unique<diag::expr_expected>(tok_.location));
        throw parser_fatal_error{};
      }
    }, tok_.tok);
  }

  using stmt_or_expr = std::variant<hlir_ptr<hlir::stmt>,
    hlir_ptr<hlir::expr>>;

  /** @brief Parses the trailing part of a block (without the opening brace).
   * @param loc The source location of the opening brace.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] hlir_ptr<hlir::block> parse_block_trail(source_location loc) {
    std::vector<hlir_ptr<hlir::stmt>> vec;

    for(;;) {
      if(std::holds_alternative<tok::rbrace>(tok_.tok)) {
        loc.last = tok_.location.last;
        next_token();
        break;
      }

      stmt_or_expr next{parse_stmt()};
      assert(!next.valueless_by_exception());

      if(auto* const stmt{std::get_if<hlir_ptr<hlir::stmt>>(&next)}) {
        assert(*stmt);
        vec.push_back(std::move(*stmt));
      } else {
        auto& expr{std::get<hlir_ptr<hlir::expr>>(next)};
        assert(expr);

        if(!std::holds_alternative<tok::rbrace>(tok_.tok)) {
          dt_.track(std::make_unique<diag::block_rbrace_expected>(
            tok_.location));
          throw parser_fatal_error{};
        }
        loc.last = tok_.location.last;
        next_token();

        return nycleus::make_hlir<hlir::block>(std::move(loc), std::move(vec),
          std::move(expr));
      }
    }

    if(vec.empty()) {
      return nycleus::make_hlir<hlir::block>(std::move(loc),
        std::vector<hlir_ptr<hlir::stmt>>{}, nullptr);
    }

    auto const trail{dynamic_cast<hlir::expr_stmt*>(vec.back().get())};
    if(!trail) {
      return nycleus::make_hlir<hlir::block>(std::move(loc), std::move(vec),
        nullptr);
    }

    assert(trail->content);
    if(needs_semicolon(*trail->content)) {
      return nycleus::make_hlir<hlir::block>(std::move(loc), std::move(vec),
        nullptr);
    }

    hlir_ptr<hlir::expr> trail_expr{std::move(trail->content)};
    vec.pop_back();
    return nycleus::make_hlir<hlir::block>(std::move(loc), std::move(vec),
      std::move(trail_expr));
  }

  // Statements ////////////////////////////////////////////////////////////////

  /** @brief Parses either a statement or an expression.
   * @returns The parsed statement, or, failing that, the parsed expression.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] stmt_or_expr parse_stmt() {
    return std::visit(util::overloaded{
      [&](tok::lbrace) -> stmt_or_expr {
        source_location loc{tok_.location};
        next_token();
        return nycleus::make_hlir<hlir::expr_stmt>(
          parse_block_trail(std::move(loc)));
      },

      [&](tok::word w) -> stmt_or_expr {
        if(parser::is_def_keyword(w)) {
          auto [name, value] = parse_def();
          assert(value);
          return nycleus::make_hlir<hlir::def_stmt>(std::move(name),
            std::move(value));
        } else {
          return parse_maybe_assign();
        }
      },

      [&](auto const&) { return parse_maybe_assign(); }
    }, tok_.tok);
  }

  /** @brief Parses either an expression or an assigment statement.
   * @returns The parsed statement, or, failing that, the parsed expression.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] stmt_or_expr parse_maybe_assign() {
    hlir_ptr<hlir::expr> target{parse_expr()};
    assert(target);

    return std::visit(util::overloaded{
      [&](tok::semicolon) -> stmt_or_expr {
        next_token();
        return nycleus::make_hlir<hlir::expr_stmt>(std::move(target));
      },

      [&](tok::assign) -> stmt_or_expr {
        next_token();

        hlir_ptr<hlir::expr> value{parse_expr()};
        assert(value);

        if(!std::holds_alternative<tok::semicolon>(tok_.tok)) {
          dt_.track(std::make_unique<diag::assign_semicolon_expected>(
            tok_.location));
          throw parser_fatal_error{};
        }
        next_token();

        return nycleus::make_hlir<hlir::assign_stmt>(std::move(target),
          std::move(value));
      },

      [&](auto const&) -> stmt_or_expr {
        return std::move(target);
      }
    }, tok_.tok);
  }

  // Definitions ///////////////////////////////////////////////////////////////

  /** @brief Parses a definition.
   *
   * Must only be called if the next token is a keyword that starts a
   * definition.
   *
   * @returns The symbol name and its definition.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] std::pair<std::u8string, hlir_ptr<hlir::def>> parse_def() {
    assert(std::holds_alternative<tok::word>(tok_.tok));
    assert(parser::is_def_keyword(*std::get_if<tok::word>(&tok_.tok)));

    tok::word const& w{*std::get_if<tok::word>(&tok_.tok)};

    if(*w.text == u8"let") {
      next_token();
      return parse_var_def_trail();
    } else if(*w.text == u8"fn") {
      next_token();
      return parse_fn_def_trail();
    } else if(*w.text == u8"class") {
      next_token();
      return parse_class_def_trail();
    } else {
      util::unreachable();
    }
  }

  /** @brief Parses the trailing part of a variable definition (without the
   * "let" keyword).
   * @returns The variable name and its definition.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] std::pair<std::u8string, hlir_ptr<hlir::var_def>>
      parse_var_def_trail() {
    // Eat the variable name
    tok::word* const w{std::get_if<tok::word>(&tok_.tok)};

    if(!w) {
      dt_.track(std::make_unique<diag::var_def_name_expected>(tok_.location));
      throw parser_fatal_error{};
    }

    if(w->sigil != tok::word::sigil_t::dollar
        && w->sigil != tok::word::sigil_t::none) {
      dt_.track(std::make_unique<diag::var_def_name_expected>(tok_.location));
      throw parser_fatal_error{};
    }

    std::u8string name{std::move(w->text.modify())};
    source_location core_loc{std::move(tok_.location)};
    next_token();

    // Eat the "mut" or "const", if any
    auto mutable_spec{hlir::var_def::mutable_spec_t::none};
    if(auto const* const w{std::get_if<tok::word>(&tok_.tok)};
        w && w->sigil == tok::word::sigil_t::none) {
      if(*w->text == u8"mut") {
        mutable_spec = hlir::var_def::mutable_spec_t::mut;
        next_token();
      } else if(*w->text == u8"const") {
        mutable_spec = hlir::var_def::mutable_spec_t::constant;
        next_token();
      }
    }

    // Eat the colon
    if(!std::holds_alternative<tok::colon>(tok_.tok)) {
      dt_.track(std::make_unique<diag::var_def_type_expected>(tok_.location));
      throw parser_fatal_error{};
    }
    next_token();

    // Eat the variable type
    hlir_ptr<hlir::type> var_type{parse_type()};
    assert(var_type);

    hlir_ptr<hlir::expr> init;

    // Eat the assignment sign, if present
    if(std::holds_alternative<tok::assign>(tok_.tok)) {
      next_token();

      // Eat the initializer expression
      init = parse_expr();
      assert(init);
    }

    // Eat the semicolon
    if(!std::holds_alternative<tok::semicolon>(tok_.tok)) {
      if(init) {
        dt_.track(std::make_unique<diag::var_def_semicolon_expected>(
          tok_.location));
      } else {
        dt_.track(std::make_unique<diag::var_def_init_expected>(tok_.location));
      }
      throw parser_fatal_error{};
    }
    next_token();

    return {std::move(name), nycleus::make_hlir<hlir::var_def>(
      std::move(core_loc), std::move(var_type), std::move(init), mutable_spec)};
  }

  /** @brief Parses the trailing part of a function definition (without the "fn"
   * keyword).
   * @returns The function name and its definition.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] std::pair<std::u8string, hlir_ptr<hlir::fn_def>>
      parse_fn_def_trail() {
    // Eat the name
    tok::word* const w{std::get_if<tok::word>(&tok_.tok)};
    if(!w) {
      dt_.track(std::make_unique<diag::fn_def_name_expected>(tok_.location));
      throw parser_fatal_error{};
    }

    if(w->sigil != tok::word::sigil_t::dollar
        && w->sigil != tok::word::sigil_t::none) {
      dt_.track(std::make_unique<diag::fn_def_name_expected>(tok_.location));
      throw parser_fatal_error{};
    }

    std::u8string name{std::move(w->text.modify())};
    source_location core_loc{std::move(tok_.location)};
    std::vector<hlir::fn_def::param> params;
    next_token();

    // Eat the left parenthesis
    if(!std::holds_alternative<tok::lparen>(tok_.tok)) {
      dt_.track(std::make_unique<diag::fn_def_lparen_expected>(tok_.location));
      throw parser_fatal_error{};
    }
    next_token();

    // Eat the parameters
    for(;;) {
      // Eat the right parenthesis, if any
      if(std::holds_alternative<tok::rparen>(tok_.tok)) {
        next_token();
        break;
      }

      // Eat the parameter name, if any
      std::optional<std::u8string> name;
      source_location loc;
      if(tok::word const* const w{std::get_if<tok::word>(&tok_.tok)};
          w && (w->sigil == tok::word::sigil_t::dollar
          || w->sigil == tok::word::sigil_t::none)) {
        loc = tok_.location;
        auto cow_text{w->text};
        next_token();
        name = std::move(cow_text.modify());
      }

      // Eat the colon
      if(!std::holds_alternative<tok::colon>(tok_.tok)) {
        if(name.has_value()) {
          dt_.track(std::make_unique<diag::fn_def_param_type_expected>(
            tok_.location));
        } else {
          dt_.track(std::make_unique<diag::fn_def_param_name_expected>(
            tok_.location));
        }
        throw parser_fatal_error{};
      }
      if(!name.has_value()) {
        loc = tok_.location;
      }
      next_token();

      // Eat the type name
      hlir_ptr<hlir::type> type{parse_type()};
      assert(type);
      if(!name.has_value()) {
        loc.last = last_;
      }
      params.emplace_back(std::move(name), std::move(type), std::move(loc));

      // Eat the right parenthesis or the comma
      if(std::holds_alternative<tok::rparen>(tok_.tok)) {
        next_token();
        break;
      }
      if(!std::holds_alternative<tok::comma>(tok_.tok)) {
        dt_.track(std::make_unique<diag::fn_def_param_next_expected>(
          tok_.location));
        throw parser_fatal_error{};
      }
      next_token();
    }

    // Eat the return type, if any
    hlir_ptr<hlir::type> return_type;
    if(std::holds_alternative<tok::colon>(tok_.tok)) {
      next_token();
      return_type = parse_type();
      assert(return_type);
    }

    // Eat the left brace
    if(!std::holds_alternative<tok::lbrace>(tok_.tok)) {
      dt_.track(std::make_unique<diag::fn_def_body_expected>(tok_.location));
      throw parser_fatal_error{};
    }
    source_location loc{tok_.location};
    next_token();

    // Eat the function body
    return {std::move(name), nycleus::make_hlir<hlir::fn_def>(
      std::move(core_loc), std::move(params), std::move(return_type),
      parse_block_trail(std::move(loc)))};
  }

  /** @brief Parses the trailing part of a class definition (without the
   * "class" keyword).
   * @returns The class name and its definition.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws parser_fatal_error
   * @throws diagnostic_tracker_error */
  [[nodiscard]] std::pair<std::u8string, hlir_ptr<hlir::class_def>>
      parse_class_def_trail() {
    // Eat the name
    tok::word* const w{std::get_if<tok::word>(&tok_.tok)};
    if(!w || w->sigil != tok::word::sigil_t::dollar
          && w->sigil != tok::word::sigil_t::none) {
      dt_.track(std::make_unique<diag::class_def_name_expected>(tok_.location));
      throw parser_fatal_error{};
    }

    std::u8string name{std::move(w->text.modify())};
    source_location core_loc{std::move(tok_.location)};
    next_token();

    // Eat the left brace
    if(!std::holds_alternative<tok::lbrace>(tok_.tok)) {
      dt_.track(std::make_unique<diag::class_def_body_expected>(tok_.location));
      throw parser_fatal_error{};
    }
    next_token();

    // Eat the definitions
    std::vector<std::pair<std::u8string, hlir_ptr<hlir::def>>> members;
    for(;;) {
      tok::word const* const w{std::get_if<tok::word>(&tok_.tok)};
      if(!w || !parser::is_def_keyword(*w)) {
        break;
      }
      members.push_back(parse_def());
    }

    // Eat the right brace
    if(!std::holds_alternative<tok::rbrace>(tok_.tok)) {
      dt_.track(std::make_unique<diag::class_def_rbrace_expected>(
        tok_.location));
      throw parser_fatal_error{};
    }
    next_token();

    return {std::move(name), nycleus::make_hlir<hlir::class_def>(
      std::move(core_loc), std::move(members))};
  }
};

} // detail_

/** @brief Parses a file.
 *
 * The parsed definitions are passed to the given callback, which must accept
 * two arguments, which are an std::u8string and a hlir_ptr<hlir::def> rvalues.
 * Any exception thrown by the callback terminates the parse and is propagated
 * to the caller.
 *
 * The token range may end with a special EOF token. In that case, the parser
 * uses that token's source location information to determine the location of
 * the end of the file (for diagnostics). Note that the parser refuses to go
 * past the EOF token, so adding one in the middle of a range is not useful. The
 * EOF token may be absent, in which case the parser uses the end location of
 * the last token in the range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
template<
  std::ranges::input_range Range,
  std::invocable<std::u8string&&, hlir_ptr<hlir::def>&&> Callback
>
requires std::same_as<std::ranges::range_value_t<Range&&>, token>
void parse(
  Range&& range,
  Callback&& report_parse_result,
  diagnostic_tracker& dt
) {
  detail_::parser<Range> p{std::forward<Range>(range), dt};
  try {
    p.parse(std::forward<Callback>(report_parse_result));
  } catch(detail_::parser_fatal_error const&) {
    // ignore
  }
}

} // nycleus

#endif
