#include "nycleus/analyze.hpp"
#include "nycleus/conversion_utils.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_tracker.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/entity_bag.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/semantics.hpp"
#include "nycleus/semantics_decay.hpp"
#include "nycleus/target.hpp"
#include "nycleus/task_context.hpp"
#include "nycleus/type_utils.hpp"
#include "nycleus/utils.hpp"
#include "util/bigint.hpp"
#include "util/downcast.hpp"
#include "util/non_null_ptr.hpp"
#include "util/overflow.hpp"
#include "util/stackful_coro.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include "util/zip_view.hpp"
#include <algorithm>
#include <bit>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <optional>
#include <ranges>
#include <type_traits>
#include <utility>
#include <variant>

using namespace nycleus;
namespace coro = util::stackful_coro;

namespace {

struct analyze_context {
  function& fn;
  task_context taco;
  diagnostic_tracker& dt;
  entity_bag const& bag;
  target const& tgt;
  type_registry& tr;
};

[[nodiscard]] bool int_types_compatible(int_type& a, int_type& b) noexcept {
  if(a.is_signed == b.is_signed) {
    return true;
  }

  int_type& signed_type{a.is_signed ? a : b};
  int_type& unsigned_type{a.is_signed ? b : a};
  return signed_type.width > unsigned_type.width;
}

[[nodiscard]] int_type& common_int_type(
  int_type& a,
  int_type& b,
  type_registry& tr
) noexcept {
  assert(int_types_compatible(a, b));
  return tr.get_int_type(std::max(a.width, b.width),
    a.is_signed || b.is_signed);
}

[[nodiscard]] util::bigint& int_literal_value(
  hlir::expr& expr
) noexcept {
  hlir::expr* cur_expr{&expr};
  for(;;) {
    auto const block{dynamic_cast<hlir::block const*>(cur_expr)};
    if(!block) {
      break;
    }
    cur_expr = block->trail.get();
  }
  return util::downcast<hlir::int_literal&>(*cur_expr).value;
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr(
  coro::context,
  analyze_context,
  hlir_ptr<hlir::expr>&
);

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_stmt(
  coro::context,
  analyze_context,
  hlir::stmt&
);

// Analyzing expressions ///////////////////////////////////////////////////////

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::int_literal& expr
) {
  if(!expr.type_suffix) {
    expr.sem = ct_int_semantics{};
    co_return;
  }

  using width_t = std::common_type_t<std::size_t, int_width_t>;

  width_t const byte_width{expr.value.size()};
  width_t width{
    std::numeric_limits<width_t>::max() / 8 < byte_width
    ? std::numeric_limits<width_t>::max()
    : byte_width == 0 ? 1 : byte_width * 8
    - std::countl_zero(expr.value[byte_width - 1])};
  bool has_error{false};

  if(width > max_int_width) {
    a.dt.track<diag::int_literal_too_big>(diag_ptr{expr});
    has_error = true;
    width = max_int_width;
  }

  assert(expr.type_suffix->width > 0);
  assert(expr.type_suffix->width <= max_int_width);

  // To fit a non-negative literal into a signed integer type, we need one
  // extra bit for the sign.
  if(expr.type_suffix->is_signed) {
    ++width;
  }

  if(!has_error && width > expr.type_suffix->width) {
    a.dt.track<diag::int_literal_does_not_fit_type>(
      diag_ptr{expr}, util::asserted_cast<int_width_t>(width));
    has_error = true;
  }

  width = expr.type_suffix->width;

  if(has_error) {
    expr.sem = std::monostate{};
  } else {
    expr.sem = value_semantics{{
      &a.tr.get_int_type(util::asserted_cast<int_width_t>(width),
      expr.type_suffix ? expr.type_suffix->is_signed : false), false, true}};
  }
}

/** @throws std::bad_alloc
 * @throws std::bad_exception */
coro::result<> analyze_expr_impl(
  coro::context,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::bool_literal& expr
) {
  expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
  co_return;
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::named_var_ref& expr
) {
  named_entity* const entity{a.bag.get({std::move(expr.name)})};
  if(!entity) {
    a.dt.track<diag::name_undefined>(diag_ptr{expr});
    ptr = nullptr;
  } else {
    switch(entity->get_entity_kind()) {
      case named_entity::entity_kind_t::function:
        ptr = nycleus::make_hlir<hlir::function_ref>(std::move(expr.loc),
          util::downcast<function&>(*entity));
        co_return co_await c.tail_call([a, &ptr](coro::context c) {
          return analyze_expr(c, a, ptr);
        });

      case named_entity::entity_kind_t::global_var:
        ptr = nycleus::make_hlir<hlir::global_var_ref>(std::move(expr.loc),
          util::downcast<global_var&>(*entity));
        co_return co_await c.tail_call([a, &ptr](coro::context c) {
          return analyze_expr(c, a, ptr);
        });

      case named_entity::entity_kind_t::class_type:
        a.dt.track<diag::name_is_not_value>(diag_ptr{expr});
        ptr = nullptr;
        co_return;
    }
  }
}

/** @throws std::bad_alloc
 * @throws std::bad_exception */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::global_var_ref& expr
) {
  bool const success{co_await a.taco.depend(c,
    task_deps::analyzing_global_var_ref{diag_ptr{expr}})};
  expr.sem = value_semantics{{
    success && expr.target->type ? &expr.target->type.assume_other() : nullptr,
    true, expr.target->is_mutable}};
  co_return;
}

/** @throws std::bad_alloc
 * @throws std::bad_exception */
coro::result<> analyze_expr_impl(
  coro::context,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::local_var_ref& expr
) {
  // If the local belongs to a different function, we cannot access its type,
  // because the other function may not be resolved yet. But then this is an
  // error anyway.
  expr.sem = value_semantics{{expr.target->fn == &a.fn && expr.target->type
    ? &expr.target->type.assume_other() : nullptr,
    true, expr.target->is_mutable}};
  co_return;
}

/** @throws std::bad_alloc
 * @throws std::bad_exception */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::function_ref& expr
) {
  function& fn{*expr.target};

  if(!co_await a.taco.depend(c,
      task_deps::analyzing_function_ref{diag_ptr{expr}})) {
    expr.sem = value_semantics{{nullptr, false, true}};
    co_return;
  }

  if(!std::ranges::all_of(fn.params,
      [](function::param const& param) noexcept {
        assert(param.var);
        return param.var->type != nullptr;
      })) {
    expr.sem = value_semantics{{nullptr, false, true}};
    co_return;
  }

  expr.sem = value_semantics{{
    &a.tr.get_fn_type(
      fn.params | std::views::transform(
        [](function::param const& param) noexcept {
          assert(param.var);
          return util::non_null_ptr{&param.var->type.assume_other()};
        }),
      fn.return_type == nullptr ? nullptr : &fn.return_type.assume_other()
    ),
    false,
    true
  }};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::cmp_expr& expr
) {
  assert(!expr.rels.empty());

  co_await analyze_expr(c, a, expr.first_op);
  hlir::expr* last_op{expr.first_op.get()};
  if(last_op) {
    nycleus::decay(*last_op, a.dt, a.tr);
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {},

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        a.dt.track<diag::comparison_void>(diag_ptr{*last_op});
      },

      [](value_semantics) noexcept {}
    }, last_op->sem);
  }

  for(auto& rel: expr.rels) {
    co_await analyze_expr(c, a, rel.op);

    bool op_has_error{false};

    value_semantics right_sem;
    if(!rel.op) {
      op_has_error = true;
    } else {
      nycleus::decay(*rel.op, a.dt, a.tr);
      std::visit(util::overloaded{
        [&](std::monostate) noexcept {
          op_has_error = true;
        },

        [](ct_int_semantics) noexcept { util::unreachable(); },

        [&](void_semantics) {
          a.dt.track<diag::comparison_void>(diag_ptr{*rel.op});
          op_has_error = true;
        },

        [&](value_semantics vs) noexcept {
          right_sem = vs;
        }
      }, rel.op->sem);
    }

    value_semantics left_sem;
    if(!last_op || !std::holds_alternative<value_semantics>(last_op->sem)) {
      // We've already checked this, either on our last iteration or (for the
      // first operand) before the loop
      op_has_error = true;
    } else {
      left_sem = *std::get_if<value_semantics>(&last_op->sem);
    }

    if(op_has_error) {
      last_op = rel.op.get();
      continue;
    }

    util::overloaded equality_visitor{
      [&](int_type& l, int_type& r) noexcept {
        if(!int_types_compatible(l, r)) {
          return false;
        }

        int_type& common_int{common_int_type(l, r, a.tr)};
        rel.left_conversion = *nycleus::get_conversion(l, common_int);
        rel.right_conversion = *nycleus::get_conversion(r, common_int);
        return true;
      },

      [&](bool_type&, bool_type&) noexcept {
        rel.left_conversion = std::monostate{};
        rel.right_conversion = std::monostate{};
        return true;
      },

      [&](fn_type& l, fn_type& r) noexcept {
        if(&l != &r) {
          return false;
        }

        rel.left_conversion = std::monostate{};
        rel.right_conversion = std::monostate{};
        return true;
      },

      [&](ptr_type& l, ptr_type& r) noexcept {
        if(&l != &r) {
          return false;
        }

        rel.left_conversion = std::monostate{};
        rel.right_conversion = std::monostate{};
        return true;
      },

      [&](auto&, auto&) noexcept { return false; }
    };

    switch(rel.kind) {
      case hlir::cmp_expr::rel_kind::eq: {
        bool const success{left_sem.type->mutable_visit([&](auto& l) {
          return right_sem.type->mutable_visit([&](auto& r) {
            return equality_visitor(l, r);
          });
        })};

        if(!success) {
          a.dt.track<diag::comparison_bad_types>(rel.loc,
            diag_ptr{*last_op}, diag_ptr{*rel.op});
          rel.left_conversion = std::monostate{};
          rel.right_conversion = std::monostate{};
        }

        break;
      }

      case hlir::cmp_expr::rel_kind::lt:
      case hlir::cmp_expr::rel_kind::le:
      case hlir::cmp_expr::rel_kind::gt:
      case hlir::cmp_expr::rel_kind::ge: {
        auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())};
        auto right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};

        if(!left_int || !right_int
            || !int_types_compatible(*left_int, *right_int)) {
          a.dt.track<diag::comparison_bad_types>(rel.loc,
            diag_ptr{*last_op}, diag_ptr{*rel.op});
          rel.left_conversion = std::monostate{};
          rel.right_conversion = std::monostate{};
        } else {
          int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
          rel.left_conversion
            = *nycleus::get_conversion(*left_sem.type, common_int);
          rel.right_conversion
            = *nycleus::get_conversion(*right_sem.type, common_int);
        }

        break;
      }
    }

    last_op = rel.op.get();
  }

  expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
}

std::optional<std::pair<value_semantics, value_semantics>> analyze_binary_expr(
  hlir::binary_op_expr& expr
) noexcept {
  bool has_error{false};

  value_semantics left_sem;
  if(!expr.left) {
    has_error = true;
  } else {
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {
        has_error = true;
      },

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        has_error = true;
      },

      [&](value_semantics vs) noexcept {
        left_sem = vs;
      }
    }, expr.left->sem);
  }

  value_semantics right_sem;
  if(!expr.right) {
    has_error = true;
  } else {
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {
        has_error = true;
      },

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        has_error = true;
      },

      [&](value_semantics vs) noexcept {
        right_sem = vs;
      }
    }, expr.right->sem);
  }

  if(has_error || !left_sem.type || !right_sem.type) {
    expr.left_conversion = std::monostate{};
    expr.right_conversion = std::monostate{};
    expr.sem = std::monostate{};
    return std::nullopt;
  }
  return std::make_pair(left_sem, right_sem);
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::or_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::logical_or_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::logical_or_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  auto const left_conversion{nycleus::get_conversion(*left_sem.type,
    a.tr.get_bool_type())};
  auto const right_conversion{nycleus::get_conversion(*right_sem.type,
    a.tr.get_bool_type())};
  if(left_conversion && right_conversion) {
    expr.left_conversion = *left_conversion;
    expr.right_conversion = *right_conversion;
    expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
    co_return;
  }

  a.dt.track<diag::logical_or_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
  * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::and_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::logical_and_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::logical_and_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  auto const left_conversion{nycleus::get_conversion(*left_sem.type,
    a.tr.get_bool_type())};
  auto const right_conversion{nycleus::get_conversion(*right_sem.type,
    a.tr.get_bool_type())};
  if(left_conversion && right_conversion) {
    expr.left_conversion = *left_conversion;
    expr.right_conversion = *right_conversion;
    expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
    co_return;
  }

  a.dt.track<diag::logical_and_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::xor_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::logical_xor_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::logical_xor_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  auto const left_conversion{nycleus::get_conversion(*left_sem.type,
    a.tr.get_bool_type())};
  auto const right_conversion{nycleus::get_conversion(*right_sem.type,
    a.tr.get_bool_type())};
  if(left_conversion && right_conversion) {
    expr.left_conversion = *left_conversion;
    expr.right_conversion = *right_conversion;
    expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
    co_return;
  }

  a.dt.track<diag::logical_xor_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::neq_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::neq_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::neq_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  util::overloaded visitor{
    [&](int_type& l, int_type& r) {
      if(!int_types_compatible(l, r)) {
        return false;
      }
      int_type& common_int{common_int_type(l, r, a.tr)};
      expr.left_conversion = *nycleus::get_conversion(l, common_int);
      expr.right_conversion = *nycleus::get_conversion(r, common_int);
      expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
      return true;
    },

    [&](bool_type&, bool_type&) {
      expr.left_conversion = std::monostate{};
      expr.right_conversion = std::monostate{};
      expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
      return true;
    },

    [&](fn_type& l, fn_type& r) {
      if(&l != &r) {
        return false;
      }

      expr.left_conversion = std::monostate{};
      expr.right_conversion = std::monostate{};
      expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
      return true;
    },

    [&](ptr_type& l, ptr_type& r) {
      if(&l != &r) {
        return false;
      }

      expr.left_conversion = std::monostate{};
      expr.right_conversion = std::monostate{};
      expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
      return true;
    },

    [&](auto&, auto&) { return false; }
  };

  bool const success{left_sem.type->mutable_visit(
    [&, right_sem = right_sem](auto& l) {
      return right_sem.type->mutable_visit([&](auto& r) {
        return visitor(l, r);
      });
    })};
  if(success) {
    co_return;
  }

  a.dt.track<diag::neq_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::add_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left && expr.right
      && std::holds_alternative<ct_int_semantics>(expr.left->sem)
      && std::holds_alternative<ct_int_semantics>(expr.right->sem)) {
    auto& left_int{int_literal_value(*expr.left)};
    auto& right_int{int_literal_value(*expr.right)};
    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      std::move(left_int) + std::move(right_int))};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::add_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::add_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  if(
    auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())},
    right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};
    left_int && right_int && int_types_compatible(*left_int, *right_int)
  ) {
    int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
    expr.left_conversion = *nycleus::get_conversion(*left_int, common_int);
    expr.right_conversion = *nycleus::get_conversion(*right_int, common_int);
    expr.sem = value_semantics{{&common_int, false, true}};
    co_return;
  }

  a.dt.track<diag::add_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::sub_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left && expr.right
      && std::holds_alternative<ct_int_semantics>(expr.left->sem)
      && std::holds_alternative<ct_int_semantics>(expr.right->sem)) {
    auto& left_int{int_literal_value(*expr.left)};
    auto& right_int{int_literal_value(*expr.right)};
    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      std::move(left_int) - std::move(right_int))};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::sub_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::sub_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  if(
    auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())},
    right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};
    left_int && right_int && int_types_compatible(*left_int, *right_int)
  ) {
    int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
    expr.left_conversion = *nycleus::get_conversion(*left_int, common_int);
    expr.right_conversion = *nycleus::get_conversion(*right_int, common_int);
    expr.sem = value_semantics{{&common_int, false, true}};
    co_return;
  }

  a.dt.track<diag::sub_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::mul_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left && expr.right
      && std::holds_alternative<ct_int_semantics>(expr.left->sem)
      && std::holds_alternative<ct_int_semantics>(expr.right->sem)) {
    auto& left_int{int_literal_value(*expr.left)};
    auto& right_int{int_literal_value(*expr.right)};
    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      std::move(left_int) * std::move(right_int))};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::mul_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::mul_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  if(
    auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())},
    right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};
    left_int && right_int && int_types_compatible(*left_int, *right_int)
  ) {
    int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
    expr.left_conversion = *nycleus::get_conversion(*left_int, common_int);
    expr.right_conversion = *nycleus::get_conversion(*right_int, common_int);
    expr.sem = value_semantics{{&common_int, false, true}};
    co_return;
  }

  a.dt.track<diag::mul_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::div_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  // TODO: Warning on division/modulo by constant zero

  if(expr.left && expr.right
      && std::holds_alternative<ct_int_semantics>(expr.left->sem)
      && std::holds_alternative<ct_int_semantics>(expr.right->sem)) {
    auto& left_int{int_literal_value(*expr.left)};
    auto& right_int{int_literal_value(*expr.right)};

    if(right_int == 0) {
      a.dt.track<diag::ct_int_div_by_zero>(diag_ptr{expr});
      ptr = nullptr;
      co_return;
    }

    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      std::move(left_int) / right_int)};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::div_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::div_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  if(
    auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())},
    right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};
    left_int && right_int && int_types_compatible(*left_int, *right_int)
  ) {
    int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
    expr.left_conversion = *nycleus::get_conversion(*left_int, common_int);
    expr.right_conversion = *nycleus::get_conversion(*right_int, common_int);
    expr.sem = value_semantics{{&common_int, false, true}};
    co_return;
  }

  a.dt.track<diag::div_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::mod_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left && expr.right
      && std::holds_alternative<ct_int_semantics>(expr.left->sem)
      && std::holds_alternative<ct_int_semantics>(expr.right->sem)) {
    auto& left_int{int_literal_value(*expr.left)};
    auto& right_int{int_literal_value(*expr.right)};

    if(right_int == 0) {
      a.dt.track<diag::ct_int_mod_by_zero>(diag_ptr{expr});
      ptr = nullptr;
      co_return;
    }

    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      std::move(left_int) % right_int)};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::mod_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::mod_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  if(
    auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())},
    right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};
    left_int && right_int && int_types_compatible(*left_int, *right_int)
  ) {
    int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
    expr.left_conversion = *nycleus::get_conversion(*left_int, common_int);
    expr.right_conversion = *nycleus::get_conversion(*right_int, common_int);
    expr.sem = value_semantics{{&common_int, false, true}};
    co_return;
  }

  a.dt.track<diag::mod_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::bor_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left && expr.right
      && std::holds_alternative<ct_int_semantics>(expr.left->sem)
      && std::holds_alternative<ct_int_semantics>(expr.right->sem)) {
    auto& left_int{int_literal_value(*expr.left)};
    auto& right_int{int_literal_value(*expr.right)};
    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      std::move(left_int) | std::move(right_int))};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::bor_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::bor_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  if(
    auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())},
    right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};
    left_int && right_int && int_types_compatible(*left_int, *right_int)
  ) {
    int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
    expr.left_conversion = *nycleus::get_conversion(*left_int, common_int);
    expr.right_conversion = *nycleus::get_conversion(*right_int, common_int);
    expr.sem = value_semantics{{&common_int, false, true}};
    co_return;
  }

  a.dt.track<diag::bor_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::band_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left && expr.right
      && std::holds_alternative<ct_int_semantics>(expr.left->sem)
      && std::holds_alternative<ct_int_semantics>(expr.right->sem)) {
    auto& left_int{int_literal_value(*expr.left)};
    auto& right_int{int_literal_value(*expr.right)};
    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      std::move(left_int) & std::move(right_int))};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::band_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::band_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  if(
    auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())},
    right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};
    left_int && right_int && int_types_compatible(*left_int, *right_int)
  ) {
    int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
    expr.left_conversion = *nycleus::get_conversion(*left_int, common_int);
    expr.right_conversion = *nycleus::get_conversion(*right_int, common_int);
    expr.sem = value_semantics{{&common_int, false, true}};
    co_return;
  }

  a.dt.track<diag::band_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::bxor_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left && expr.right
      && std::holds_alternative<ct_int_semantics>(expr.left->sem)
      && std::holds_alternative<ct_int_semantics>(expr.right->sem)) {
    auto& left_int{int_literal_value(*expr.left)};
    auto& right_int{int_literal_value(*expr.right)};
    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      std::move(left_int) ^ std::move(right_int))};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::bxor_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::bxor_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  if(
    auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())},
    right_int{dynamic_cast<int_type*>(right_sem.type.get_ptr())};
    left_int && right_int && int_types_compatible(*left_int, *right_int)
  ) {
    int_type& common_int{common_int_type(*left_int, *right_int, a.tr)};
    expr.left_conversion = *nycleus::get_conversion(*left_int, common_int);
    expr.right_conversion = *nycleus::get_conversion(*right_int, common_int);
    expr.sem = value_semantics{{&common_int, false, true}};
    co_return;
  }

  a.dt.track<diag::bxor_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::lsh_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  // TODO: Compile-time bitwise shifts & rotations

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::lsh_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::lsh_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  int_type& native_int{a.tr.get_int_type(a.tgt.get_native_width(), false)};

  if(auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())}) {
    if(auto const conversion{nycleus::get_conversion(*right_sem.type,
        native_int)}) {
      expr.left_conversion = std::monostate{};
      expr.right_conversion = *conversion;
      expr.sem = value_semantics{{left_int, false, true}};
      co_return;
    }
  }

  a.dt.track<diag::lsh_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::rsh_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::rsh_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::rsh_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  int_type& native_int{a.tr.get_int_type(a.tgt.get_native_width(), false)};

  if(auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())}) {
    if(auto const conversion{nycleus::get_conversion(*right_sem.type,
        native_int)}) {
      expr.left_conversion = std::monostate{};
      expr.right_conversion = *conversion;
      expr.sem = value_semantics{{left_int, false, true}};
      co_return;
    }
  }

  a.dt.track<diag::rsh_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::lrot_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::lrot_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::lrot_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  int_type& native_int{a.tr.get_int_type(a.tgt.get_native_width(), false)};

  if(auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())}) {
    if(auto const conversion{nycleus::get_conversion(*right_sem.type,
        native_int)}) {
      expr.left_conversion = std::monostate{};
      expr.right_conversion = *conversion;
      expr.sem = value_semantics{{left_int, false, true}};
      co_return;
    }
  }

  a.dt.track<diag::lrot_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::rrot_expr& expr
) {
  co_await analyze_expr(c, a, expr.left);
  co_await analyze_expr(c, a, expr.right);

  if(expr.left) {
    nycleus::decay(*expr.left, a.dt, a.tr);
  }
  if(expr.right) {
    nycleus::decay(*expr.right, a.dt, a.tr);
  }

  auto const sems{analyze_binary_expr(expr)};
  if(!sems) {
    if(expr.left && std::holds_alternative<void_semantics>(expr.left->sem)) {
      a.dt.track<diag::rrot_void>(diag_ptr{*expr.left});
    }
    if(expr.right && std::holds_alternative<void_semantics>(expr.right->sem)) {
      a.dt.track<diag::rrot_void>(diag_ptr{*expr.right});
    }
    co_return;
  }
  auto const [left_sem, right_sem] = *sems;

  int_type& native_int{a.tr.get_int_type(a.tgt.get_native_width(), false)};

  if(auto left_int{dynamic_cast<int_type*>(left_sem.type.get_ptr())}) {
    if(auto const conversion{nycleus::get_conversion(*right_sem.type,
        native_int)}) {
      expr.left_conversion = std::monostate{};
      expr.right_conversion = *conversion;
      expr.sem = value_semantics{{left_int, false, true}};
      co_return;
    }
  }

  a.dt.track<diag::rrot_bad_types>(diag_ptr{expr});

  expr.left_conversion = std::monostate{};
  expr.right_conversion = std::monostate{};
  expr.sem = std::monostate{};
}

std::optional<value_semantics> analyze_unary_expr(
  hlir::unary_op_expr& expr
) noexcept {
  bool has_error{false};

  value_semantics op_sem;
  if(!expr.op) {
    has_error = true;
  } else {
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {
        has_error = true;
      },

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        has_error = true;
      },

      [&](value_semantics vs) noexcept {
        op_sem = vs;
      }
    }, expr.op->sem);
  }

  if(has_error || !op_sem.type) {
    expr.sem = std::monostate{};
    return std::nullopt;
  }
  return op_sem;
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::neg_expr& expr
) {
  co_await analyze_expr(c, a, expr.op);

  if(expr.op && std::holds_alternative<ct_int_semantics>(expr.op->sem)) {
    auto& op_int{int_literal_value(*expr.op)};
    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      -std::move(op_int))};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.op) {
    nycleus::decay(*expr.op, a.dt, a.tr);
  }

  auto const sems{analyze_unary_expr(expr)};
  if(!sems) {
    if(expr.op && std::holds_alternative<void_semantics>(expr.op->sem)) {
      a.dt.track<diag::neg_void>(diag_ptr{*expr.op});
    }
    co_return;
  }
  auto const op_sem{*sems};

  if(
    auto op_int{dynamic_cast<int_type*>(op_sem.type.get_ptr())};
    op_int && op_int->is_signed
  ) {
    expr.sem = value_semantics{{op_sem.type, false, true}};
    co_return;
  }

  a.dt.track<diag::neg_bad_type>(diag_ptr{expr});

  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::bnot_expr& expr
) {
  co_await analyze_expr(c, a, expr.op);

  if(expr.op && std::holds_alternative<ct_int_semantics>(expr.op->sem)) {
    auto& op_int{int_literal_value(*expr.op)};
    auto result{nycleus::make_hlir<hlir::int_literal>(expr.loc,
      ~std::move(op_int))};
    result->sem = ct_int_semantics{};
    ptr = std::move(result);
    co_return;
  }

  if(expr.op) {
    nycleus::decay(*expr.op, a.dt, a.tr);
  }

  auto const sems{analyze_unary_expr(expr)};
  if(!sems) {
    if(expr.op && std::holds_alternative<void_semantics>(expr.op->sem)) {
      a.dt.track<diag::bnot_void>(diag_ptr{*expr.op});
    }
    co_return;
  }
  auto const op_sem{*sems};

  if(op_sem.type->get_kind() == type::kind_t::int_type) {
    expr.sem = value_semantics{{op_sem.type, false, true}};
    co_return;
  }

  a.dt.track<diag::bnot_bad_type>(diag_ptr{expr});

  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::not_expr& expr
) {
  co_await analyze_expr(c, a, expr.op);

  if(expr.op) {
    nycleus::decay(*expr.op, a.dt, a.tr);
  }

  auto const sems{analyze_unary_expr(expr)};
  if(!sems) {
    if(expr.op && std::holds_alternative<void_semantics>(expr.op->sem)) {
      a.dt.track<diag::not_void>(diag_ptr{*expr.op});
    }
    co_return;
  }
  auto const op_sem{*sems};

  if(op_sem.type.get_ptr() == &a.tr.get_bool_type()) {
    expr.sem = value_semantics{{&a.tr.get_bool_type(), false, true}};
    co_return;
  }

  a.dt.track<diag::not_bad_type>(diag_ptr{expr});

  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::addr_expr& expr
) {
  co_await analyze_expr(c, a, expr.op);

  if(expr.op) {
    nycleus::decay(*expr.op, a.dt, a.tr);
  }

  value_semantics op_sem;
  bool has_error{false};
  if(!expr.op) {
    has_error = true;
  } else {
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {
        has_error = true;
      },

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        a.dt.track<diag::addr_void>(diag_ptr{*expr.op});
        has_error = true;
      },

      [&](value_semantics vs) noexcept {
        op_sem = vs;
      }
    }, expr.op->sem);
  }

  if(has_error || !op_sem.type) {
    expr.sem = std::monostate{};
    co_return;
  }

  if(expr.is_mutable && !op_sem.is_mutable()) {
    a.dt.track<diag::addr_mut>(diag_ptr{expr});
  }

  expr.sem = value_semantics{{
    &a.tr.get_ptr_type({op_sem.type.get_ptr(), expr.is_mutable}), false, true}};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::deref_expr& expr
) {
  co_await analyze_expr(c, a, expr.op);

  if(expr.op) {
    nycleus::decay(*expr.op, a.dt, a.tr);
  }

  auto const sems{analyze_unary_expr(expr)};
  if(!sems) {
    if(expr.op && std::holds_alternative<void_semantics>(expr.op->sem)) {
      a.dt.track<diag::deref_void>(diag_ptr{*expr.op});
    }
    co_return;
  }
  auto const op_sem{*sems};

  if(auto* const t{dynamic_cast<ptr_type*>(op_sem.type.get_ptr())}) {
    expr.sem = value_semantics{{t->pointee_type.get_ptr(), true,
      t->is_mutable()}};
    co_return;
  }

  a.dt.track<diag::deref_bad_type>(diag_ptr{expr});

  expr.sem = std::monostate{};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception */
coro::result<> analyze_member_expr(
  coro::context c,
  analyze_context a,
  hlir::indexed_member_expr& expr,
  class_type& cl,
  bool is_mutable
) noexcept {
  assert(expr.index < cl.fields.size());

  class_type::field_t const& field{cl.fields[expr.index]};

  if(!co_await a.taco.depend(c,
      task_deps::analyzing_member_expr{diag_ptr{expr}, util::non_null_ptr{&cl}})) {
    expr.sem = value_semantics{{nullptr, true, is_mutable && field.is_mutable}};
    co_return;
  }

  expr.sem = value_semantics{{
    field.type ? &field.type.assume_other() : nullptr,
    true,
    is_mutable && field.is_mutable
  }};
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr,
  hlir::named_member_expr& expr
) {
  co_await analyze_expr(c, a, expr.op);

  if(expr.op) {
    nycleus::decay(*expr.op, a.dt, a.tr);
  }

  auto const sems{analyze_unary_expr(expr)};
  if(!sems) {
    if(expr.op && std::holds_alternative<void_semantics>(expr.op->sem)) {
      a.dt.track<diag::member_access_void>(diag_ptr{*expr.op});
    }
    co_return;
  }
  auto const op_sem{*sems};

  auto* const op_type{dynamic_cast<class_type*>(op_sem.type.get_ptr())};
  if(!op_type) {
    a.dt.track<diag::member_access_bad_type>(diag_ptr{*expr.op});
    ptr = nullptr;
    co_return;
  }

  auto const lookup_result{op_type->scope.lookup(expr.name)};
  assert(!lookup_result.valueless_by_exception());

  switch(lookup_result.index()) {
    case util::variant_index_v<class_lookup_result, std::monostate>:
    case util::variant_index_v<class_lookup_result, util::non_null_ptr<class_type>>:
      a.dt.track<diag::member_access_bad_name>(diag_ptr{expr});
      ptr = nullptr;
      break;

    case util::variant_index_v<class_lookup_result, class_field_lookup>: {
      auto const field{*std::get_if<class_field_lookup>(&lookup_result)};

      if(&field.cl != op_type) {
        a.dt.track<diag::member_access_bad_name>(diag_ptr{expr});
        ptr = nullptr;
        co_return;
      }

      auto replacement{nycleus::make_hlir<hlir::indexed_member_expr>(
        std::move(expr.loc), std::move(expr.op), field.index)};
      co_await analyze_member_expr(c, a, *replacement, *op_type,
        op_sem.is_mutable());
      ptr = std::move(replacement);

      break;
    }

    default:
      util::unreachable();
  }
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::indexed_member_expr& expr
) {
  co_await analyze_expr(c, a, expr.op);

  if(expr.op) {
    nycleus::decay(*expr.op, a.dt, a.tr);
  }

  auto const sems{analyze_unary_expr(expr)};
  if(!sems) {
    if(expr.op && std::holds_alternative<void_semantics>(expr.op->sem)) {
      a.dt.track<diag::member_access_void>(diag_ptr{*expr.op});
    }
    co_return;
  }
  auto const op_sem{*sems};

  auto* const op_type{dynamic_cast<class_type*>(op_sem.type.get_ptr())};
  if(!op_type) {
    a.dt.track<diag::member_access_bad_type>(diag_ptr{*expr.op});
    expr.sem = std::monostate{};
    co_return;
  }

  co_await analyze_member_expr(c, a, expr, *op_type, op_sem.is_mutable());
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::call_expr& expr
) {
  co_await analyze_expr(c, a, expr.callee);
  if(expr.callee) {
    nycleus::decay(*expr.callee, a.dt, a.tr);
  }

  for(auto& arg: expr.args) {
    co_await analyze_expr(c, a, arg.value);
    if(arg.value) {
      nycleus::decay(*arg.value, a.dt, a.tr);
    }
  }

  fn_type* callee_type{nullptr};
  if(expr.callee) {
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {},

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        a.dt.track<diag::call_expr_callee_void>(diag_ptr{*expr.callee});
      },

      [&](value_semantics vs) {
        callee_type = dynamic_cast<fn_type*>(vs.type.get_ptr());
        if(!callee_type) {
          a.dt.track<diag::call_expr_callee_bad_type>(diag_ptr{expr});
        }
      }
    }, expr.callee->sem);
  }

  for(auto& arg: expr.args) {
    arg.conversion = std::monostate{};
    if(!arg.value) {
      continue;
    }
    if(std::holds_alternative<void_semantics>(arg.value->sem)) {
      a.dt.track<diag::call_expr_arg_void>(diag_ptr{*arg.value});
    }
  }

  if(!callee_type) {
    expr.sem = std::monostate{};
    co_return;
  }

  if(expr.args.size() != callee_type->param_types.size()) {
    a.dt.track<diag::call_expr_arg_count_mismatch>(diag_ptr{expr});
  }

  for(auto const& [arg, param_type]:
      util::zip(expr.args, callee_type->param_types)) {
    if(!arg.value) {
      continue;
    }
    auto const* const arg_sem{std::get_if<value_semantics>(&arg.value->sem)};
    if(!arg_sem || !arg_sem->type) {
      continue;
    }

    auto const conversion{nycleus::get_conversion(*arg_sem->type, *param_type)};

    if(!conversion) {
      a.dt.track<diag::call_expr_arg_bad_type>(diag_ptr{expr}, *param_type,
        diag_ptr{*arg.value});
    }

    arg.conversion = *conversion;

    if(dynamic_cast<class_type*>(arg_sem->type.get_ptr())
        && arg_sem->is_persistent()) {
      a.dt.track<diag::persistent_class_arg_unsupported>(diag_ptr{*arg.value});
    }
  }

  if(callee_type->return_type) {
    expr.sem = value_semantics{{callee_type->return_type, false, true}};
  } else {
    expr.sem = void_semantics{};
  }
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_block(
  coro::context c,
  analyze_context a,
  hlir::block& block
) {
  for(auto& stmt: block.stmts) {
    if(stmt) {
      co_await analyze_stmt(c, a, *stmt);
    }
  }
  if(block.trail) {
    co_await analyze_expr(c, a, block.trail);
  }

  if(block.trail) {
    block.sem = block.trail->sem;
  } else {
    block.sem = void_semantics{};
  }
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_expr_impl(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>&,
  hlir::block& expr
) {
  co_return co_await c.tail_call([a, &expr](coro::context c) {
    return analyze_block(c, a, expr);
  });
}

coro::result<> analyze_expr(
  coro::context c,
  analyze_context a,
  hlir_ptr<hlir::expr>& ptr
) {
  if(!ptr) {
    co_return;
  }

  co_return co_await c.tail_call([a, &ptr](coro::context c) {
    return ptr->mutable_visit([&c, a, &ptr](auto& expr) {
      return analyze_expr_impl(c, a, ptr, expr);
    });
  });
}

// Analyzing statements ////////////////////////////////////////////////////////

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_stmt_impl(
  coro::context c,
  analyze_context a,
  hlir::expr_stmt& stmt
) {
  co_await analyze_expr(c, a, stmt.content);
  if(stmt.content) {
    nycleus::decay(*stmt.content, a.dt, a.tr);
  }
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_stmt_impl(
  coro::context c,
  analyze_context a,
  hlir::assign_stmt& stmt
) {
  co_await analyze_expr(c, a, stmt.target);
  co_await analyze_expr(c, a, stmt.value);

  if(stmt.target) {
    nycleus::decay(*stmt.target, a.dt, a.tr);
  }
  if(stmt.value) {
    nycleus::decay(*stmt.value, a.dt, a.tr);
  }

  bool has_error{false};
  if(!stmt.value) {
    has_error = true;
  } else {
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {
        has_error = true;
      },

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        has_error = true;
        a.dt.track<diag::assign_from_void>(diag_ptr{*stmt.value});
      },

      [&](value_semantics) {}
    }, stmt.value->sem);
  }

  if(!stmt.target) {
    has_error = true;
  } else {
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {
        has_error = true;
      },

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        has_error = true;
        a.dt.track<diag::assign_to_void>(diag_ptr{*stmt.target});
      },

      [&](value_semantics vs) {
        if(!vs.is_mutable()) {
          has_error = true;
          a.dt.track<diag::assign_to_immut>(diag_ptr{stmt});
        }

        if(!vs.is_persistent()) {
          has_error = true;
          a.dt.track<diag::assign_to_transient>(diag_ptr{stmt});
        }
      }
    }, stmt.target->sem);
  }

  if(has_error) {
    stmt.conversion = std::monostate{};
    co_return;
  }

  auto const target_sem{*std::get_if<value_semantics>(&stmt.target->sem)};
  auto const value_sem{*std::get_if<value_semantics>(&stmt.value->sem)};

  bool const success{target_sem.type->visit(util::overloaded{
    [&](int_type const& t) noexcept {
      auto const conversion{nycleus::get_conversion(*value_sem.type, t)};
      if(conversion.has_value()) {
        stmt.conversion = *conversion;
        return true;
      } else {
        return false;
      }
    },

    [&](bool_type const& t) noexcept {
      auto const conversion{nycleus::get_conversion(*value_sem.type, t)};
      if(conversion.has_value()) {
        stmt.conversion = *conversion;
        return true;
      } else {
        return false;
      }
    },

    [&](fn_type const& t) noexcept {
      auto const conversion{nycleus::get_conversion(*value_sem.type, t)};
      if(conversion.has_value()) {
        stmt.conversion = *conversion;
        return true;
      } else {
        return false;
      }
    },

    [&](ptr_type const& t) noexcept {
      auto const conversion{nycleus::get_conversion(*value_sem.type, t)};
      if(conversion.has_value()) {
        stmt.conversion = *conversion;
        return true;
      } else {
        return false;
      }
    },

    [&](class_type const&) noexcept {
      return false;
    }
  })};

  if(!success) {
    a.dt.track<diag::assign_bad_types>(diag_ptr{stmt});
    stmt.conversion = std::monostate{};
  }
}

[[noreturn]] coro::result<> analyze_stmt_impl(
  coro::context,
  analyze_context,
  hlir::def_stmt&
) noexcept {
  util::unreachable();
}

/** @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
coro::result<> analyze_stmt_impl(
  coro::context c,
  analyze_context a,
  hlir::local_var_init_stmt& stmt
) {
  co_await analyze_expr(c, a, stmt.value);
  if(stmt.value) {
    nycleus::decay(*stmt.value, a.dt, a.tr);
  }

  bool has_error{false};
  if(!stmt.value) {
    has_error = true;
  } else {
    std::visit(util::overloaded{
      [&](std::monostate) noexcept {
        has_error = true;
      },

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        has_error = true;
        a.dt.track<diag::local_var_init_void>(diag_ptr{*stmt.value});
      },

      [](value_semantics) noexcept {}
    }, stmt.value->sem);
  }

  if(has_error) {
    stmt.conversion = std::monostate{};
    co_return;
  }
  auto const value_sem{*std::get_if<value_semantics>(&stmt.value->sem)};

  if(!stmt.target->type) {
    stmt.conversion = std::monostate{};
    co_return;
  }

  auto const conversion{nycleus::get_conversion(*value_sem.type,
    stmt.target->type.assume_other())};
  if(!conversion) {
    a.dt.track<diag::local_var_init_bad_type>(diag_ptr{stmt});
    stmt.conversion = std::monostate{};
  } else {
    stmt.conversion = *conversion;
    if(dynamic_cast<class_type*>(value_sem.type.get_ptr())
        && value_sem.is_persistent()) {
      a.dt.track<diag::persistent_class_init_unsupported>(
        diag_ptr{*stmt.value});
    }
  }
}

coro::result<> analyze_stmt(
  coro::context c,
  analyze_context a,
  hlir::stmt& stmt
) {
  co_return co_await c.tail_call([a, &stmt](coro::context c) {
    return stmt.mutable_visit([&c, a](auto& stmt) {
      return analyze_stmt_impl(c, a, stmt);
    });
  });
}

} // (anonymous)

// Analysis public API /////////////////////////////////////////////////////////

coro::result<> nycleus::analyze(
  coro::context c,
  function& fn,
  task_context taco,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  target const& tgt,
  type_registry& tr
) {
  assert(fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::resolved);

  if(!fn.body) {
    co_return;
  }
  co_await analyze_block(c, {fn, taco, dt, bag, tgt, tr}, *fn.body);
  nycleus::decay(*fn.body, dt, tr);

  if(fn.return_type) {
    std::visit(util::overloaded{
      [](std::monostate) noexcept {},

      [](ct_int_semantics) noexcept { util::unreachable(); },

      [&](void_semantics) {
        dt.track<diag::fn_return_void>(
          fn.body->trail ? fn.body->trail->loc : fn.body->loc, fn);
      },

      [&](value_semantics vs) {
        if(!vs.type) {
          return;
        }

        auto const conversion{nycleus::get_conversion(*vs.type,
          fn.return_type.assume_other())};
        if(!conversion) {
          dt.track<diag::fn_return_bad_type>(
            fn.body->trail ? fn.body->trail->loc : fn.body->loc, fn);
          return;
        }
        fn.body_conversion = *conversion;

        if(dynamic_cast<class_type const*>(vs.type.get_ptr())
            && vs.is_persistent()) {
          dt.track<diag::persistent_class_return_unsupported>(
            diag_ptr{fn.body->trail ? *fn.body->trail : *fn.body});
        }
      }
    }, fn.body->sem);
  }

  fn.processing_state.store(function::processing_state_t::analyzed,
    std::memory_order::release);
}
