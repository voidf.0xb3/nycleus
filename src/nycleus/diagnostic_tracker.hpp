#ifndef NYCLEUS_DIAGNOSTIC_TRACKER_HPP_INCLUDED_CZC98NLSBQV6H7PEZAO0FIDFY
#define NYCLEUS_DIAGNOSTIC_TRACKER_HPP_INCLUDED_CZC98NLSBQV6H7PEZAO0FIDFY

#include "nycleus/diagnostic.hpp"
#include "nycleus/source_location.hpp"
#include "util/guarded_var.hpp"
#include "util/u8compat.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <concepts>
#include <cstdint>
#include <exception>
#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

namespace nycleus {

/** @brief The abstract base class for diagnostic tracker exceptions. */
class diagnostic_tracker_error: public std::exception {
public:
  [[nodiscard]] virtual gsl::czstring what() const noexcept override = 0;
};

/** @brief An abstract base for a mechanism of recording and reporting
 * diagnostic messages.
 *
 * This is supposed to be thread-safe: its methods can be called without
 * external synchronization. */
class diagnostic_tracker: public util::hier_base {
private:
  bool has_errors_{false};

public:
  virtual ~diagnostic_tracker() noexcept = default;

  /** @brief Tracks a diagnostic message.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws diagnostic_tracker_error */
  void track(std::unique_ptr<diagnostic>);

  /** @brief A helper function to create a specific diagnostic object and track
   * it.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws diagnostic_tracker_error */
  template<std::derived_from<diagnostic> T, typename... Args>
  requires std::constructible_from<T, Args...>
  void track(Args&&... args) {
    this->track(std::make_unique<T>(std::forward<Args>(args)...));
  }

  /** @brief Determines whether any errors have been reported. */
  [[nodiscard]] bool has_errors() const noexcept {
    return has_errors_;
  }

private:
  /** @brief Overridden by the descendant to implement a specific diagnostic
   * tracking policy.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws diagnostic_tracker_error */
  virtual void do_track(std::unique_ptr<diagnostic>) = 0;
};

} // nycleus

#endif
