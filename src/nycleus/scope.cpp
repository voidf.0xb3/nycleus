#include "nycleus/scope.hpp"
#include <cassert>
#include <string>
#include <utility>
#include <variant>

// Class scope /////////////////////////////////////////////////////////////////

bool nycleus::class_scope::add_symbol(
  std::u8string&& name,
  nycleus::class_scope::symbol sym
) {
  assert(!sym.valueless_by_exception());
  return symbols_.try_emplace(std::move(name), sym).second;
}

nycleus::class_lookup_result nycleus::class_scope::lookup(
  std::u8string const& name
) const noexcept {
  auto const it{symbols_.find(name)};
  if(it == symbols_.cend()) {
    return std::monostate{};
  } else {
    return std::visit([](auto const& x) -> nycleus::class_lookup_result {
      return x;
    }, it->second);
  }
}

// Lexical scope ///////////////////////////////////////////////////////////////

nycleus::lexical_lookup_result nycleus::lexical_scope::lookup(
  std::u8string const& name
) const noexcept {
  auto result{this->lookup_immediate(name)};
  if(std::holds_alternative<std::monostate>(result) && parent_) {
    return parent_->lookup(name);
  }
  return result;
}

// Lexical class scope /////////////////////////////////////////////////////////

nycleus::lexical_lookup_result nycleus::lexical_class_scope::lookup_immediate(
  std::u8string const& name
) const noexcept {
  return std::visit([](auto const& x) -> nycleus::lexical_lookup_result {
    return x;
  }, cs_.lookup(name));
}

// Simple scope ////////////////////////////////////////////////////////////////

bool nycleus::simple_scope::add_symbol(
  std::u8string&& name,
  nycleus::simple_scope::symbol sym
) {
  assert(!sym.valueless_by_exception());
  return symbols_.try_emplace(std::move(name), sym).second;
}

nycleus::lexical_lookup_result nycleus::simple_scope::lookup_immediate(
  std::u8string const& name
) const noexcept {
  auto const it{symbols_.find(name)};
  if(it == symbols_.cend()) {
    return std::monostate{};
  } else {
    return std::visit([](auto const& x) -> nycleus::lexical_lookup_result {
      return x;
    }, it->second);
  }
}
