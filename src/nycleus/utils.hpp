#ifndef NYCLEUS_UTILS_HPP_INCLUDED_0PWYJJBRPU61IOKVAAQRV9ZR2
#define NYCLEUS_UTILS_HPP_INCLUDED_0PWYJJBRPU61IOKVAAQRV9ZR2

#include <algorithm>
#include <cstdint>
#include <limits>

namespace nycleus {

using field_index_t = std::uint32_t;

using int_width_t = std::uint32_t;

/** @brief A type suffix that may be present on an integer literal. */
struct int_type_suffix {
  /** @brief The width of the requested type.
   *
   * This must be positive and not greater than max_int_width. */
  int_width_t width;

  /** @brief Whether a signed type is requested. */
  bool is_signed;
};

/** @brief The maximum possible width of an integer type.
 *
 * This is set to 2<sup>23</sup>, which is the maximum width supported by LLVM,
 * or the largest value of the `unsigned` data type, which is used in LLVM's
 * APIs. */
constexpr int_width_t max_int_width{std::min(
  std::numeric_limits<unsigned>::max(), static_cast<int_width_t>(1) << 23)};

} // nycleus

#endif
