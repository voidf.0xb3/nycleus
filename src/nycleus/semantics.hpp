#ifndef NYCLEUS_SEMANTICS_HPP_INCLUDED_JGN8NXT4X3O8FTD1PW7KBFQ5C
#define NYCLEUS_SEMANTICS_HPP_INCLUDED_JGN8NXT4X3O8FTD1PW7KBFQ5C

#include "util/ptr_with_flags.hpp"
#include <variant>

namespace nycleus {

struct type;

/** @brief A description of void semantics. */
struct void_semantics {};

/** @brief A description of compile-time integer semantics.
 *
 * This structure is empty and does not contain the integer value, because it
 * can be determined from the HLIR node itself. Indeed, this semantics is only
 * ever allowed on integer literal nodes, which already contain the value, and
 * on block nodes, whose trailing expressions can be used to determine the
 * value. */
struct ct_int_semantics {};

/** @brief A description of the value semantics of an expression.
 *
 * If `type` is a null pointer, this indicates invalid semantics. (Such a value
 * can appear in the tree of an ill-formed program which we're processing to
 * find more diagnostics.) */
struct value_semantics {
  util::ptr_with_flags<nycleus::type, 2> type;

  [[nodiscard]] bool is_persistent() const noexcept {
    return type.get_flag<0>();
  }

  void set_persistent(bool value) noexcept {
    type.set_flag<0>(value);
  }

  [[nodiscard]] bool is_mutable() const noexcept {
    return type.get_flag<1>();
  }

  void set_mutable(bool value) noexcept {
    type.set_flag<1>(value);
  }
};

/** @brief A description of the semantics of an expression.
  *
  * The `std::monostate` alternative indicates semantics that is either unknown
  * or ill-defined. */
using semantics = std::variant<
  std::monostate,
  ct_int_semantics,
  void_semantics,
  value_semantics
>;

} // nycleus

#endif
