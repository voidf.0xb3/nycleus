#ifndef NYCLEUS_TASK_CONTEXT_HPP_INCLUDED_LEW9688DVZ2HVYY06EHZ6MMP9
#define NYCLEUS_TASK_CONTEXT_HPP_INCLUDED_LEW9688DVZ2HVYY06EHZ6MMP9

#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/utils.hpp"
#include "util/non_null_ptr.hpp"
#include "util/stackful_coro_fwd.hpp"
#include <boost/container_hash/hash.hpp>
#include <functional>
#include <optional>
#include <variant>

namespace nycleus {

struct class_type;
struct function;
struct global_var;
class task_runner;

// Task info ///////////////////////////////////////////////////////////////////

namespace tasks {

/** @brief A task that resolves the parameter and return types of a function. */
struct resolve_fn_task {
  util::non_null_ptr<function> fn;
  friend bool operator==(resolve_fn_task, resolve_fn_task) noexcept = default;
};

/** @brief A task that performs semantic analysis of a function. */
struct analyze_fn_task {
  util::non_null_ptr<function> fn;
  friend bool operator==(analyze_fn_task, analyze_fn_task) noexcept = default;
};

/** @brief A task that resolves the type of a global variable. */
struct resolve_global_var_task {
  util::non_null_ptr<global_var> var;
  friend bool operator==(resolve_global_var_task, resolve_global_var_task)
    noexcept = default;
};

/** @brief A task that resolves the field types of a class. */
struct resolve_class_task {
  util::non_null_ptr<class_type> cl;
  friend bool operator==(resolve_class_task, resolve_class_task)
    noexcept = default;
};

/** @brief A task that detects cycles in a class. */
struct class_cycles_task {
  util::non_null_ptr<class_type> cl;
  friend bool operator==(class_cycles_task, class_cycles_task)
    noexcept = default;
};

} // tasks

} // nycleus

template<>
struct std::hash<::nycleus::tasks::resolve_fn_task> {
  ::std::size_t operator()(::nycleus::tasks::resolve_fn_task t)
      const noexcept {
    return ::std::hash<::util::non_null_ptr<::nycleus::function>>{}(t.fn);
  }
};
template<>
struct std::hash<::nycleus::tasks::analyze_fn_task> {
  ::std::size_t operator()(::nycleus::tasks::analyze_fn_task t)
      const noexcept {
    return ::std::hash<::util::non_null_ptr<::nycleus::function>>{}(t.fn);
  }
};
template<>
struct std::hash<::nycleus::tasks::resolve_global_var_task> {
  ::std::size_t operator()(::nycleus::tasks::resolve_global_var_task t)
      const noexcept {
    return ::std::hash<::util::non_null_ptr<::nycleus::global_var>>{}(t.var);
  }
};
template<>
struct std::hash<::nycleus::tasks::resolve_class_task> {
  ::std::size_t operator()(::nycleus::tasks::resolve_class_task t)
      const noexcept {
    return ::std::hash<::util::non_null_ptr<::nycleus::class_type>>{}(t.cl);
  }
};
template<>
struct std::hash<::nycleus::tasks::class_cycles_task> {
  ::std::size_t operator()(::nycleus::tasks::class_cycles_task t)
      const noexcept {
    return ::std::hash<::util::non_null_ptr<::nycleus::class_type>>{}(t.cl);
  }
};

namespace nycleus {

/** @brief Information about a task to be executed by a task_runner. */
using task_info = std::variant<
  tasks::resolve_fn_task,
  tasks::analyze_fn_task,
  tasks::resolve_global_var_task,
  tasks::resolve_class_task,
  tasks::class_cycles_task
>;

// Task dependency info ////////////////////////////////////////////////////////

namespace task_deps {

/** @brief A function analysis task's dependency on a function resolution task
 * occurring when analyzing a function reference expression. */
struct analyzing_function_ref {
  diag_ptr<hlir::function_ref> expr;
  friend bool operator==(analyzing_function_ref, analyzing_function_ref)
    noexcept = default;
};

/** @brief A function analysis task's dependency on a global variable resolution
 * task occurring when analyzing a global variable reference expression. */
struct analyzing_global_var_ref {
  diag_ptr<hlir::global_var_ref> expr;
  friend bool operator==(analyzing_global_var_ref, analyzing_global_var_ref)
    noexcept = default;
};

/** @brief A function analysis task's dependency on a class resolution task
 * occurring when analyzing an indexed member access expression. */
struct analyzing_member_expr {
  diag_ptr<hlir::indexed_member_expr> expr;
  util::non_null_ptr<class_type> cl;
  friend bool operator==(analyzing_member_expr, analyzing_member_expr)
    noexcept = default;
};

/** @brief A class cycle detection task's dependency on another class cycle
 * detection task. */
struct class_cycle {
  util::non_null_ptr<class_type> cl;
  field_index_t index;
  friend bool operator==(class_cycle, class_cycle) noexcept = default;
};

} // task_deps

/** @brief Information about the manner in which a task dynamically depends on
 * another task.
 *
 * An instance of this type must be supplied when initiating a dynamic
 * dependency. */
using task_dyn_dep_info = std::variant<
  task_deps::analyzing_function_ref,
  task_deps::analyzing_global_var_ref,
  task_deps::analyzing_member_expr,
  task_deps::class_cycle
>;

namespace task_deps {

/** @brief A class cycle detection task's static dependency on the corresponding
 * class resolution task. */
struct class_cycles_prereq_resolve {
  util::non_null_ptr<class_type> cl;
  friend bool operator==(
    class_cycles_prereq_resolve,
    class_cycles_prereq_resolve
  ) noexcept = default;
};

} // task_deps

/** @brief Information about the manner in which a task depends, statically or
 * dynamically, on another task.
 *
 * When a dependency cycle is reported, the cycle log consists of these values.
 */
using task_dep_info = std::variant<
  task_deps::class_cycles_prereq_resolve,
  task_deps::analyzing_function_ref,
  task_deps::analyzing_global_var_ref,
  task_deps::analyzing_member_expr,
  task_deps::class_cycle
>;

} // nycleus

template<>
struct std::hash<::nycleus::task_deps::analyzing_function_ref> {
  ::std::size_t operator()(::nycleus::task_deps::analyzing_function_ref t)
      const noexcept {
    return ::std::hash<::nycleus::diag_ptr<::nycleus::hlir::function_ref>>{}(
      t.expr);
  }
};

template<>
struct std::hash<::nycleus::task_deps::analyzing_global_var_ref> {
  ::std::size_t operator()(::nycleus::task_deps::analyzing_global_var_ref t)
      const noexcept {
    return ::std::hash<::nycleus::diag_ptr<::nycleus::hlir::global_var_ref>>{}(
      t.expr);
  }
};

template<>
struct std::hash<::nycleus::task_deps::analyzing_member_expr> {
  ::std::size_t operator()(::nycleus::task_deps::analyzing_member_expr t)
      const noexcept {
    ::std::size_t result{::std::hash<::nycleus::diag_ptr<
      ::nycleus::hlir::indexed_member_expr>>{}(t.expr)};
    ::boost::hash_combine(result, ::std::hash<
      ::util::non_null_ptr<::nycleus::class_type>>{}(t.cl));
    return result;
  }
};

template<>
struct std::hash<::nycleus::task_deps::class_cycle> {
  ::std::size_t operator()(::nycleus::task_deps::class_cycle t)
      const noexcept {
    ::std::size_t result{::std::hash<
      ::util::non_null_ptr<::nycleus::class_type>>{}(t.cl)};
    ::boost::hash_combine(result,
      ::std::hash<::nycleus::field_index_t>{}(t.index));
    return result;
  }
};

template<>
struct std::hash<::nycleus::task_deps::class_cycles_prereq_resolve> {
  ::std::size_t operator()(::nycleus::task_deps::class_cycles_prereq_resolve t)
      const noexcept {
    return ::std::hash<::util::non_null_ptr<::nycleus::class_type>>{}(t.cl);
  }
};

namespace nycleus {

// Other ///////////////////////////////////////////////////////////////////////

/** @brief Given a task_dyn_dep_info, returns the same value as a task_dep_info.
 */
[[nodiscard]] task_dep_info get_task_dep_info(task_dyn_dep_info) noexcept;

/** @brief Given a task_dyn_dep_info, returns the dependency task as a
 * task_info. */
[[nodiscard]] task_info get_task_info(task_dyn_dep_info) noexcept;

/** @brief Given a task_dep_info, returns the dependency task as a task_info. */
[[nodiscard]] task_info get_task_info(task_dep_info) noexcept;

/** @brief Checks whether the given task has already been completed or if it
 * still needs to be executed. */
[[nodiscard]] bool is_done(task_info) noexcept;

/** @brief Passed into tasks executed by the task_runner to allow them to depend
 * on other tasks. */
class task_context {
private:
  std::optional<task_dyn_dep_info>& dep_;

  friend class task_runner;
  explicit task_context(std::optional<task_dyn_dep_info>& dep) noexcept:
    dep_{dep} {}

public:
  /** @brief Called to depend on the specified task.
   *
   * If the supplied task is not completed, suspend the current task and resumes
   * it when the given dependency task finishes.
   *
   * Returns true if the dependency was successfully satisfied. If the
   * dependency was broken due to being part of a cycle, returns false.
   *
   * @throws std::bad_alloc
   * @throws std::bad_exception */
  util::stackful_coro::result<bool> depend(
    util::stackful_coro::context,
    task_dyn_dep_info
  );
};

} // nycleus

#endif
