#ifndef NYCLEUS_TOKEN_HPP_INCLUDED_9IOSH07FQOOQWI5QS4QQ6EFYH
#define NYCLEUS_TOKEN_HPP_INCLUDED_9IOSH07FQOOQWI5QS4QQ6EFYH

#include "nycleus/diagnostic.hpp"
#include "nycleus/utils.hpp"
#include "nycleus/source_location.hpp"
#include "util/bigint.hpp"
#include "util/cow.hpp"
#include <cstdint>
#include <memory>
#include <optional>
#include <type_traits>
#include <variant>
#include <vector>

namespace nycleus {

namespace tok {
  struct word {
    util::cow<std::u8string> text;

    enum class sigil_t { none, dollar, underscore };
    sigil_t sigil{sigil_t::none};
  };

  struct int_literal {
    /** @brief The number encoded in the token.
     *
     * The value shall be non-negative. */
    util::cow<util::bigint> num;

    /** @brief The specified type suffix, if any. */
    std::optional<int_type_suffix> type_suffix{};
  };

  struct comment {
    util::cow<std::u8string> text;

    enum class kind_t {
      single_line, multiline_non_nestable, multiline_nestable
    };
    kind_t kind;
  };

  struct string_literal {
    util::cow<std::u8string> value;
  };

  struct char_literal {
    util::cow<std::u8string> value;
  };

  struct excl {};
  struct neq {};
  struct percent {};
  struct percent_assign {};
  struct amp {};
  struct logical_and {
    source_location first_amp_loc;
    source_location second_amp_loc;
  };
  struct amp_assign {};
  struct lparen {};
  struct rparen {};
  struct star {};
  struct star_assign {};
  struct plus {};
  struct incr {};
  struct plus_assign {};
  struct comma {};
  struct minus {};
  struct decr {};
  struct minus_assign {};
  struct arrow {};
  struct point {};
  struct double_point {};
  struct ellipsis {};
  struct slash {};
  struct slash_assign {};
  struct colon {};
  struct semicolon {};
  struct lt {};
  struct lrot {};
  struct lrot_assign {};
  struct lshift {};
  struct lshift_assign {};
  struct leq {};
  struct three_way_cmp {};
  struct assign {};
  struct eq {};
  struct fat_arrow {};
  struct gt {};
  struct rrot {};
  struct rrot_assign {};
  struct geq {};
  struct rshift {};
  struct rshift_assign {};
  struct question {};
  struct at {};
  struct lsquare {};
  struct rsquare {};
  struct bitwise_xor {};
  struct bitwise_xor_assign {};
  struct logical_xor {};
  struct lbrace {};
  struct pipe {};
  struct pipe_assign {};
  struct subst {};
  struct logical_or {};
  struct rbrace {};
  struct tilde {};

  /** @brief A special token kind which indicates a lexing error.
   *
   * The token should also contain a diagnostic, specifying the exact error. */
  struct error {};

  /** @brief A special token kind which indicates the end of the file. */
  struct eof {};
} // tok

/** @brief A token which may be produced by the lexer. */
struct token {
  using tok_type = std::variant<
    tok::word,
    tok::int_literal,
    tok::comment,
    tok::string_literal,
    tok::char_literal,

    tok::excl,
    tok::neq,
    tok::percent,
    tok::percent_assign,
    tok::amp,
    tok::logical_and,
    tok::amp_assign,
    tok::lparen,
    tok::rparen,
    tok::star,
    tok::star_assign,
    tok::plus,
    tok::incr,
    tok::plus_assign,
    tok::comma,
    tok::minus,
    tok::decr,
    tok::minus_assign,
    tok::arrow,
    tok::point,
    tok::double_point,
    tok::ellipsis,
    tok::slash,
    tok::slash_assign,
    tok::colon,
    tok::semicolon,
    tok::lt,
    tok::lrot,
    tok::lrot_assign,
    tok::lshift,
    tok::lshift_assign,
    tok::leq,
    tok::three_way_cmp,
    tok::assign,
    tok::eq,
    tok::fat_arrow,
    tok::gt,
    tok::rrot,
    tok::rrot_assign,
    tok::geq,
    tok::rshift,
    tok::rshift_assign,
    tok::question,
    tok::at,
    tok::lsquare,
    tok::rsquare,
    tok::bitwise_xor,
    tok::bitwise_xor_assign,
    tok::logical_xor,
    tok::lbrace,
    tok::pipe,
    tok::pipe_assign,
    tok::subst,
    tok::logical_or,
    tok::rbrace,
    tok::tilde,

    tok::error,
    tok::eof
  >;

  /** @brief The specific token kind. */
  tok_type tok;

  /** @brief The source location of the token. */
  source_location location{};

  /** @brief The diagnostics associated with the token. */
  std::optional<util::cow<std::vector<diagnostic_box>>> diagnostics{};
};

} // nycleus

#endif
