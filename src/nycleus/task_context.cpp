#include "nycleus/task_context.hpp"
#include "nycleus/entity.hpp"
#include "util/downcast.hpp"
#include "util/non_null_ptr.hpp"
#include "util/stackful_coro.hpp"
#include "util/util.hpp"
#include <atomic>
#include <cassert>
#include <utility>
#include <variant>

nycleus::task_info nycleus::get_task_info(
  nycleus::task_dyn_dep_info dep
) noexcept {
  return nycleus::get_task_info(nycleus::get_task_dep_info(std::move(dep)));
}

nycleus::task_info nycleus::get_task_info(
  nycleus::task_dep_info dep
) noexcept {
  assert(!dep.valueless_by_exception());
  return std::visit(util::overloaded{
    [](task_deps::class_cycles_prereq_resolve dep) noexcept -> task_info {
      return tasks::resolve_class_task{dep.cl};
    },

    [](task_deps::analyzing_function_ref dep) noexcept -> task_info {
      assert(dep.expr);
      return tasks::resolve_fn_task{dep.expr->target};
    },

    [](task_deps::analyzing_global_var_ref dep) noexcept -> task_info {
      assert(dep.expr);
      return tasks::resolve_global_var_task{dep.expr->target};
    },

    [](task_deps::analyzing_member_expr dep) noexcept -> task_info {
      assert(dep.expr);
      return tasks::resolve_class_task{dep.cl};
    },

    [](task_deps::class_cycle dep) noexcept -> task_info {
      assert(dep.index < dep.cl->fields.size());
      return tasks::class_cycles_task{util::non_null_ptr{&util::downcast<nycleus::class_type&>(
        dep.cl->fields[dep.index].type.assume_other())}};
    }
  }, std::move(dep));
}

nycleus::task_dep_info nycleus::get_task_dep_info(
  nycleus::task_dyn_dep_info dep
) noexcept {
  assert(!dep.valueless_by_exception());
  return std::visit([](auto dep) noexcept -> task_dep_info {
    return dep;
  }, dep);
}

bool nycleus::is_done(nycleus::task_info t) noexcept {
  assert(!t.valueless_by_exception());
  return std::visit(util::overloaded{
    [](tasks::resolve_fn_task t) noexcept {
      return t.fn->processing_state.load(std::memory_order::acquire)
        != function::processing_state_t::initial;
    },

    [](tasks::analyze_fn_task t) noexcept {
      return t.fn->processing_state.load(std::memory_order::acquire)
        == function::processing_state_t::analyzed;
    },

    [](tasks::resolve_global_var_task t) noexcept {
      return t.var->processing_state.load(std::memory_order::acquire)
        != global_var::processing_state_t::initial;
    },

    [](tasks::resolve_class_task t) noexcept {
      return t.cl->processing_state.load(std::memory_order::acquire)
        != class_type::processing_state_t::initial;
    },

    [](tasks::class_cycles_task t) noexcept {
      return t.cl->processing_state.load(std::memory_order::acquire)
        == class_type::processing_state_t::cycles;
    }
  }, t);
}

util::stackful_coro::result<bool> nycleus::task_context::depend(
  util::stackful_coro::context ctx,
  nycleus::task_dyn_dep_info dep
) {
  assert(!dep.valueless_by_exception());
  task_info const ti{nycleus::get_task_info(dep)};
  if(nycleus::is_done(ti)) {
    co_return true;
  }
  dep_ = std::move(dep);
  co_await ctx.suspend();
  co_return nycleus::is_done(ti);
}
