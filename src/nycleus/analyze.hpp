#ifndef NYCLEUS_ANALYZE_HPP_INCLUDED_RPMUSVQTAEURLI647LZBKWNMM
#define NYCLEUS_ANALYZE_HPP_INCLUDED_RPMUSVQTAEURLI647LZBKWNMM

#include "nycleus/task_context.hpp"
#include "util/stackful_coro_fwd.hpp"

namespace nycleus {

class diagnostic_tracker;
class entity_bag;
struct function;
class target;
class type_registry;

/** @brief A task, called by task_runner, to perform semantic analysis of the
 * given function's code.
 *
 * If this throws, the task may not be fully completed, but no incorrect
 * modifications will have been made.
 *
 * @throws std::bad_alloc
 * @throws std::bad_exception
 * @throws std::length_error
 * @throws target_error
 * @throws diagnostic_tracker_error */
util::stackful_coro::result<> analyze(
  util::stackful_coro::context,
  function&,
  task_context,
  diagnostic_tracker&,
  entity_bag const&,
  target const&,
  type_registry&
);

} // nycleus

#endif
