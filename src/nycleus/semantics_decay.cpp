
#include "nycleus/semantics_decay.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_tracker.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/utils.hpp"
#include "nycleus/type_utils.hpp"
#include "util/downcast.hpp"
#include "util/overflow.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <bit>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <ranges>
#include <type_traits>
#include <variant>

void nycleus::decay(
  nycleus::hlir::expr& expr,
  nycleus::diagnostic_tracker& dt,
  nycleus::type_registry& tr
) {
  if(auto* const block{dynamic_cast<hlir::block*>(&expr)};
      block && block->trail) {
    nycleus::decay(*block->trail, dt, tr);
    block->sem = block->trail->sem;
    return;
  }

  assert(!expr.sem.valueless_by_exception());
  std::visit(util::overloaded{
    [&](ct_int_semantics) {
      hlir::expr* cur_expr{&expr};
      for(;;) {
        hlir::block* block{dynamic_cast<hlir::block*>(cur_expr)};
        if(!block) {
          break;
        }
        cur_expr = block->trail.get();
      }

      hlir::int_literal& literal{util::downcast<hlir::int_literal&>(*cur_expr)};
      assert(!literal.type_suffix);

      using width_t = std::common_type_t<std::size_t, int_width_t>;

      width_t const byte_width{literal.value.size()};
      width_t width{
        std::numeric_limits<width_t>::max() / 8 < byte_width
        ? std::numeric_limits<width_t>::max()
        : byte_width == 0 ? 1 : byte_width * 8
        - std::countl_zero(literal.value[byte_width - 1])};

      // If the value is negative, the numer is represented as two's complement,
      // which is equivalent to subtracting one and then inverting all the bits.
      // After that happens, we want the most significant bit -- the sign bit --
      // to be one. Ordinarily this requires us to add one to the calculated
      // width. However, if the value is a power of two, then subtracting one
      // will reduce the bit width by one; after we add one, the bit width will
      // be the same as calculated above, so we leave it unchanged.
      if(literal.value.is_negative()) {
        assert(byte_width > 0);
        if(
          !std::has_single_bit(literal.value[byte_width - 1])
          || !std::ranges::all_of(
            std::views::iota(static_cast<width_t>(0), byte_width - 1)
            | std::views::transform([&](width_t i) noexcept {
              return literal.value[i];
            }),
            [](std::uint8_t x) noexcept { return x == 0; }
          )
        ) {
          ++width;
        }
      }

      if(width > max_int_width) {
        dt.track(std::make_unique<diag::ct_int_too_big>(diag_ptr{expr}));
        width = max_int_width;
        expr.sem = std::monostate{};
        return;
      }

      expr.sem = value_semantics{{
        &tr.get_int_type(util::asserted_cast<int_width_t>(width),
        literal.value.is_negative()), false, true}};
    },

    [](auto&&) noexcept {}
  }, expr.sem);
}
