#include "nycleus/task_runner.hpp"
#include "nycleus/analyze.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_tracker.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/entity_bag.hpp"
#include "nycleus/task_context.hpp"
#include "nycleus/resolve_types.hpp"
#include "util/downcast.hpp"
#include "util/enumerate_view.hpp"
#include "util/non_null_ptr.hpp"
#include "util/overflow.hpp"
#include "util/stackful_coro.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <atomic>
#include <cassert>
#include <exception>
#include <memory>
#include <ranges>
#include <stop_token>
#include <utility>
#include <variant>
#include <vector>

namespace {

/** @brief A task to perform detection of class cycles.
 *
 * This does nothing but depend on class cycle detection tasks for the contained
 * classes. If there is a cycle, the task_runner will detect and report it.
 *
 * @throws std::bad_alloc
 * @throws std::bad_exception */
util::stackful_coro::result<> detect_class_cycles(
  util::stackful_coro::context ctx,
  nycleus::class_type& cl,
  nycleus::task_context t
) {
  assert(cl.processing_state.load(std::memory_order::relaxed)
    == nycleus::class_type::processing_state_t::resolved);

  for(auto const& [index, field]: cl.fields | util::enumerate) {
    if(!field.type) {
      continue;
    }
    auto const member_cl{dynamic_cast<nycleus::class_type*>(
      &field.type.assume_other())};
    if(!member_cl) {
      continue;
    }

    // We don't really do anything with this information, so it's okay to just
    // discard the result. We just do this to give the task_runner an
    // opportunity to detect cycles.
    co_await t.depend(ctx, nycleus::task_deps::class_cycle{util::non_null_ptr{&cl},
      util::asserted_cast<nycleus::field_index_t>(index)});
  }

  cl.processing_state.store(nycleus::class_type::processing_state_t::cycles,
    std::memory_order::release);
}

} // (anonymous)

nycleus::task_runner::state_t nycleus::task_runner::make_state(
  nycleus::entity_bag const& bag
) noexcept {
  state_t state;

  state.statically_blocked_tasks = 0;

  for(auto& entity: bag.entities()) {
    switch(entity.get_entity_kind()) {
      case named_entity::entity_kind_t::function: {
        auto& fn{util::downcast<nycleus::function&>(entity)};
        if(!fn.initializee) {
          // A function that is not an initializer needs to have its types
          // resolved.
          state.resolve_fn_backlog.push_front(fn);
        }

        // A function will also need to be analyzed, including if it is an
        // initializer.
        ++state.statically_blocked_tasks;

        break;
      }

      case named_entity::entity_kind_t::global_var:
        // A global variable needs to have its type resolved.
        state.resolve_global_var_backlog.push_front(
          util::downcast<nycleus::global_var&>(entity));
        break;

      case named_entity::entity_kind_t::class_type:
        // A class needs to have its member types resolved.
        state.resolve_class_backlog.push_front(
          util::downcast<nycleus::class_type&>(entity));

        // Afterwards, it will need to go through cycle detection.
        ++state.statically_blocked_tasks;

        break;
    }
  }

  return state;
}

std::vector<nycleus::task_dep_info> nycleus::task_runner::find_cycle(
  nycleus::task_runner::state_t& state,
  nycleus::task_runner::this_task_set_t const& tasks
) {
  std::vector<task_dep_info> cycle;

  // Pick a blocked task to start. Since statically blocked tasks cannot form a
  // cycle, there must be at least one dynamically blocked task.
  assert(!state.blocked_tasks.empty());
  auto const& first_task{*state.blocked_tasks.begin()};
  assert(first_task);
  assert(first_task->dependency);
  cycle.push_back(nycleus::get_task_dep_info(*first_task->dependency));

  // The task that the last element in `cycle` is blocked by
  task_info obstructor{nycleus::get_task_info(*first_task->dependency)};

  for(;;) {
    // The manner in which `obstructor` is blocked on *its* dependency
    task_dep_info obstructor_dep_info{[&]() noexcept {
      auto const obstructor_task{tasks.find(obstructor)};
      if(obstructor_task == tasks.cend()) {
        // The obstructor is statically blocked
        return std::visit(util::overloaded{
          [](tasks::resolve_fn_task) noexcept -> task_dep_info {
            util::unreachable();
          },

          [](tasks::analyze_fn_task) noexcept -> task_dep_info {
            // The only time we depend on a function analysis task is when we
            // call it during compile-time execution. For that, we would have to
            // evaluate a function reference expression referring to the callee.
            // For that, we would have to first analyze that expression. That
            // requires adding a dependency on resolving the function.
            // Therefore, if we ever add a dependency on a function analysis
            // task, the function will already be resolved by that time. Thus,
            // the static dependency edge from a function analysis task to the
            // corresponding function type resolution task cannot ever be part
            // of a cycle.

            util::unreachable();
          },

          [](tasks::resolve_global_var_task) noexcept -> task_dep_info {
            util::unreachable();
          },

          [](tasks::resolve_class_task) noexcept -> task_dep_info {
            util::unreachable();
          },

          [](tasks::class_cycles_task t) noexcept -> task_dep_info {
            return task_deps::class_cycles_prereq_resolve{t.cl};
          }
        }, obstructor);
      } else {
        // The obstructor is dynamically blocked
        assert(*obstructor_task);
        assert((*obstructor_task)->dependency);
        return nycleus::get_task_dep_info(*(*obstructor_task)->dependency);
      }
    }()};

    // We will need to add `obstructor_dep_info` to `cycle`. But first we need
    // to find the obstructor's obstructor in the list. This is because we're
    // looking for a `task_dep_info` that depends on the same task as
    // `obstructor_dep_info`, and `obstructor_dep_info` must obviously be
    // excluded from this search.

    if(auto found_task{std::ranges::find(cycle,
        nycleus::get_task_info(obstructor_dep_info),
        [](task_dep_info d) { return nycleus::get_task_info(std::move(d)); })};
        found_task != cycle.cend()) {
      // The entry that we found must also be removed, since it corresponds to
      // the task *before* the one we're looking for.
      cycle.erase(cycle.cbegin(), found_task + 1);
      cycle.push_back(std::move(obstructor_dep_info));
      cycle.shrink_to_fit();
      return cycle;
    }

    obstructor = nycleus::get_task_info(obstructor_dep_info);
    cycle.push_back(std::move(obstructor_dep_info));
  }
}

void nycleus::task_runner::break_cycle(
  nycleus::task_runner::state_t& state,
  nycleus::diagnostic_tracker& dt
) {
  this_task_set_t tasks;
  try {
    tasks.reserve(util::throwing_cast<this_task_set_t::size_type>(
      state.blocked_tasks.size()));
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }
  for(auto const& t: state.blocked_tasks) {
    assert(t);
    tasks.insert(util::non_null_ptr{t.get()});
  }

  auto cycle{task_runner::find_cycle(state, tasks)};

  auto& task_to_unblock{[&]() noexcept -> task& {
    for(auto entry: cycle) {
      auto const task_to_unblock{tasks.find(
        nycleus::get_task_info(std::move(entry)))};
      if(task_to_unblock != tasks.cend()) {
        return **task_to_unblock;
      }
    }
    util::unreachable();
  }()};

  dt.track<diag::dep_cycle>(std::move(cycle));

  auto const [equal_first, equal_last] = state.blocked_tasks.equal_range(
    nycleus::get_task_info(*task_to_unblock.dependency));
  auto unblocked_task{
    std::move(state.blocked_tasks.extract(
      std::ranges::find(equal_first, equal_last,
        &task_to_unblock, &std::unique_ptr<task>::get)
    ).value())};
  assert(!unblocked_task->next);
  unblocked_task->next = std::move(state.ready_tasks);
  state.ready_tasks = std::move(unblocked_task);
}

nycleus::task_runner::task_runner(nycleus::entity_bag const& bag) noexcept:
  state_{task_runner::make_state(bag)} {}

void nycleus::task_runner::run(
  nycleus::diagnostic_tracker& dt,
  nycleus::entity_bag const& bag,
  nycleus::target const& tgt,
  nycleus::type_registry& tr,
  std::stop_token stop
) {
  // This is set to true whenever stop is requested. In order to work with
  // the condition variable, this can only be accessed while the state mutex is
  // locked.
  bool stopping{false};
  std::stop_callback stop_cb{stop, [&]() noexcept {
    {
      auto const guard{state_.lock()};
      stopping = true;
    }
    // Unfortunately, all workers wait on the same condition variable, so
    // there's no way to notify a specific thread; therefore, we notify all
    // threads.
    cv_.notify_all();
  }};

  // The task that we're about to execute.
  std::unique_ptr<task> next_task;
  // Whether the task has already been started. If true, we need to resume the
  // task. If false, we will need to start the task.
  bool started;

  // Whether we broke a cycle since the last time we notified waiting threads.
  //
  // Whenever a thread is out of work, but there are other running threads, it
  // waits until more work is available. Whenever we complete a task that was
  // blocking other tasks, we notify other threads.
  //
  // But when we break a cycle, this creates only one new task, which will be
  // immediately executed by the same thread that broke the cycle. Other threads
  // do not need to be woken up, since there will be no work for them anyway.
  //
  // However, this creates the possibility that the thread that broke the cycle
  // will finish all work without waking up other threads. The other threads
  // will continue waiting, even though there is no more work. To avoid this,
  // if we break a cycle, and then finish all work without notifying other
  // threads, we notify other threads before returning.
  bool cycle_broken{false};

  auto pull_one_task = [&](util::guarded_var<state_t>::guard& state) {
    for(;;) {
      if(stopping) {
        next_task.reset();
        return;
      } else if(state->ready_tasks) {
        next_task = std::move(state->ready_tasks);
        state->ready_tasks = std::move(next_task->next);
        started = true;
        return;
      } else if(!state->resolve_fn_backlog.empty()) {
        next_task = std::make_unique<task>(tasks::resolve_fn_task{
          util::non_null_ptr{&state->resolve_fn_backlog.front()}});
        state->resolve_fn_backlog.pop_front();
        started = false;
        return;
      } else if(!state->analyze_fn_backlog.empty()) {
        next_task = std::make_unique<task>(tasks::analyze_fn_task{
          util::non_null_ptr{&state->analyze_fn_backlog.front()}});
        state->analyze_fn_backlog.pop_front();
        started = false;
        return;
      } else if(!state->resolve_global_var_backlog.empty()) {
        next_task = std::make_unique<task>(tasks::resolve_global_var_task{
          util::non_null_ptr{&state->resolve_global_var_backlog.front()}});
        state->resolve_global_var_backlog.pop_front();
        started = false;
        return;
      } else if(!state->resolve_class_backlog.empty()) {
        next_task = std::make_unique<task>(tasks::resolve_class_task{
          util::non_null_ptr{&state->resolve_class_backlog.front()}});
        state->resolve_class_backlog.pop_front();
        started = false;
        return;
      } else if(!state->class_cycles_backlog.empty()) {
        next_task = std::make_unique<task>(tasks::class_cycles_task{
          util::non_null_ptr{&state->class_cycles_backlog.front()}});
        state->class_cycles_backlog.pop_front();
        started = false;
        return;
      } else if(state->blocked_tasks.empty()
          && state->statically_blocked_tasks == 0) {
        next_task.reset();
        assert(state->num_running_threads > 0);
        --state->num_running_threads;
        return;
      } else {
        --state->num_running_threads;

        if(state->num_running_threads == 0) {
          // If all threads are waiting, but there are still more tasks to run,
          // this means that there is a cycle. Have the last thread that ran out
          // of work find the cycle and break it.
          task_runner::break_cycle(*state, dt);
          cycle_broken = true;
        } else {
          cv_.wait(state.get_guard());
        }

        state->num_running_threads
          = util::asserted_add<std::uintptr_t>(state->num_running_threads, 1u);
      }
    }
  };

  {
    auto state{state_.lock()};
    state->num_running_threads
      = util::asserted_add<std::uintptr_t>(state->num_running_threads, 1u);
    pull_one_task(state);
  }

  while(next_task) {
    assert(!next_task->next);
    if(started) {
      next_task->proc.resume();
    } else {
      std::visit(util::overloaded{
        [&](tasks::resolve_fn_task t) {
          next_task->proc.start(resolve_fn, *t.fn, dt, bag, tr);
        },

        [&](tasks::analyze_fn_task t) {
          next_task->proc.start(analyze, *t.fn,
            task_context{next_task->dependency}, dt, bag, tgt, tr);
        },

        [&](tasks::resolve_global_var_task t) {
          next_task->proc.start(resolve_global_var, *t.var, dt, bag, tr);
        },

        [&](tasks::resolve_class_task t) {
          next_task->proc.start(resolve_class, *t.cl, dt, bag, tr);
        },

        [&](tasks::class_cycles_task t) {
          next_task->proc.start(detect_class_cycles, *t.cl,
            task_context{next_task->dependency});
        }
      }, next_task->this_task);
    }

    // Whether the completion of the current task unblocked some tasks that were
    // previously blocked. If true, we will need to notify waiting threads after
    // we've released the mutex.
    bool has_unblocked_tasks{false};

    {
      auto state{state_.lock()};

      if(next_task->proc.done()) {
        std::visit(util::overloaded{
          [&](tasks::resolve_fn_task t) noexcept {
            assert(t.fn->processing_state
              == function::processing_state_t::resolved);
            state->analyze_fn_backlog.push_front(*t.fn);
            assert(state->statically_blocked_tasks > 0);
            --state->statically_blocked_tasks;
            has_unblocked_tasks = true;
          },

          []([[maybe_unused]] tasks::analyze_fn_task t) noexcept {
            assert(t.fn->processing_state
              == function::processing_state_t::analyzed);
          },

          [&](tasks::resolve_global_var_task t) noexcept {
            assert(t.var->processing_state
              == global_var::processing_state_t::resolved);
            if(t.var->initializer) {
              assert(t.var->initializer->processing_state
                == function::processing_state_t::resolved);
              state->analyze_fn_backlog.push_front(*t.var->initializer);
              assert(state->statically_blocked_tasks > 0);
              --state->statically_blocked_tasks;
              has_unblocked_tasks = true;
            }
          },

          [&](tasks::resolve_class_task t) noexcept {
            assert(t.cl->processing_state
              == class_type::processing_state_t::resolved);
            state->class_cycles_backlog.push_front(*t.cl);
            assert(state->statically_blocked_tasks > 0);
            --state->statically_blocked_tasks;
            has_unblocked_tasks = true;
          },

          []([[maybe_unused]] tasks::class_cycles_task t) noexcept {
            assert(t.cl->processing_state
              == class_type::processing_state_t::cycles);
          }
        }, next_task->this_task);

        auto [blocked_it, blocked_iend]
          = state->blocked_tasks.equal_range(next_task->this_task);
        if(blocked_it != blocked_iend) {
          has_unblocked_tasks = true;
        }
        while(blocked_it != blocked_iend) {
          // We need to increment the iterator before we extract the element,
          // because iterators to erased elements get invalidated.
          auto const cur_it{blocked_it};
          ++blocked_it;

          auto dependent{
            std::move(state->blocked_tasks.extract(cur_it).value())};

          assert(dependent);
          assert(!dependent->next);
          dependent->next = std::move(state->ready_tasks);
          state->ready_tasks = std::move(dependent);
        }
      } else {
        assert(next_task->dependency);

        // We want to store the task in the blocked map in order for it to be
        // unblocked later, when the dependency entity is processed. We want to
        // avoid a scenario where a task is added to the blocked map after the
        // entity's dependent tasks are already woken up. In other words, if
        // this mutex lock is later (in the mutex lock order) that the one in
        // which this entity's dependents have been woken up, we do not want to
        // block the task.
        //
        // To that end, we test if the dependency is done while holding the
        // mutex. If the dependents have already been woken up, then the thread
        // that finished processing the entity has updated the dependency
        // entity's state before releasing (or even acquiring) the mutex, which
        // "synchronizes-with" us acquiring the mutex, so we are guaranteed to
        // see that write here. Therefore, if we see that the dependency is
        // done, we do not block the task, but immediately set to ready.
        //
        // Note that we also check the dependency's state in task_context,
        // before we even suspend the task at all. This is an optimization to
        // avoid suspending if the dependency has already been processed. But it
        // is not enough, since this does not prevent the bad scenario described
        // above.

        if(nycleus::is_done(nycleus::get_task_info(
            *next_task->dependency))) {
          next_task->next = std::move(state->ready_tasks);
          state->ready_tasks = std::move(next_task);
        } else {
          state->blocked_tasks.emplace(std::move(next_task));
        }
      }

      pull_one_task(state);
    }

    if(has_unblocked_tasks) {
      cv_.notify_all();
      cycle_broken = false;
    }
  }

  if(cycle_broken) {
    cv_.notify_all();
  }
}
