#include <boost/test/unit_test.hpp>

#include "nycleus/source_location.hpp"
#include "unicode/encoding.hpp"
#include "util/scope_guard.hpp"
#include "util/u8compat.hpp"
#include <algorithm>
#include <cstdint>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <optional>
#include <string>
#include <vector>
#include <utility>

using namespace util::u8compat_literals;

static_assert(unicode::decodable_encoding<nycleus::utf8_counted<char>,
  std::string::const_iterator, std::string::const_iterator,
  unicode::eh_replace<>>);
static_assert(unicode::decodable_encoding<nycleus::utf8_counted<char8_t>,
  std::u8string::const_iterator, std::u8string::const_iterator,
  unicode::eh_replace<>>);

namespace nycleus {

std::ostream& operator<<(std::ostream& os,
    std::pair<char32_t, nycleus::source_location> x) {
  os << u8"{U+"_ch;

  {
    std::ios state{nullptr};
    state.copyfmt(os);
    util::scope_guard const guard{[&]() { os.copyfmt(state); }};

    os << std::hex << std::setfill(u8'0'_ch) << std::uppercase << std::setw(4)
      << static_cast<std::uint32_t>(x.first);
  }

  return os
    << u8", {"_ch << x.second.first.line
    << u8':'_ch << x.second.first.col
    << u8"}, {"_ch << x.second.last.line
    << u8':'_ch << x.second.last.col << u8"}}"_ch;
}

} // nycleus

BOOST_AUTO_TEST_SUITE(test_nycleus)
BOOST_AUTO_TEST_SUITE(source_location)

BOOST_AUTO_TEST_CASE(utf8_counted) {
  std::u8string const input{u8"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F"
    u8"\U0001F3AF\n\U0001F52C\U0001F520\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"};
  std::vector<std::pair<char32_t, nycleus::source_location>> output;
  std::ranges::copy(input
    | unicode::decode(nycleus::utf8_counted{std::nullopt}, unicode::eh_throw{}),
    std::back_inserter(output));
  std::vector<std::pair<char32_t, nycleus::source_location>> const test{
    {U'\U0001F449', {std::nullopt, {1, 1}, {1, 5}}},
    {U'\U0001F3BB', {std::nullopt, {1, 5}, {1, 9}}},
    {U'\U0001F432', {std::nullopt, {1, 9}, {1, 13}}},
    {U'\u2699',     {std::nullopt, {1, 13}, {1, 16}}},
    {U'\uFE0F',     {std::nullopt, {1, 16}, {1, 19}}},
    {U'\U0001F3AF', {std::nullopt, {1, 19}, {1, 23}}},
    {U'\n',         {std::nullopt, {1, 23}, {1, 24}}},
    {U'\U0001F52C', {std::nullopt, {2, 1}, {2, 5}}},
    {U'\U0001F520', {std::nullopt, {2, 5}, {2, 9}}},
    {U'\u002D',     {std::nullopt, {2, 9}, {2, 10}}},
    {U'\u0038',     {std::nullopt, {2, 10}, {2, 11}}},
    {U'\u20E3',     {std::nullopt, {2, 11}, {2, 14}}},
    {U'\U0001F6E0', {std::nullopt, {2, 14}, {2, 18}}},
    {U'\U0001F4DF', {std::nullopt, {2, 18}, {2, 22}}}
  };
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(utf8_counted_terminators) {
  std::u8string const input{
    u8"LF\n"
    u8"CRLF\r\n"
    u8"CR\r"
    u8"vtab\v"
    u8"formfeed\f"
    u8"NEL\u0085"
    u8"line\u2028"
    u8"par\u2029"
    u8"end"
  };
  std::vector<std::pair<char32_t, nycleus::source_location>> output;
  std::ranges::copy(input
    | unicode::decode(nycleus::utf8_counted{std::nullopt}, unicode::eh_throw{}),
    std::back_inserter(output));
  std::vector<std::pair<char32_t, nycleus::source_location>> const test{
    {U'L',  {std::nullopt, {1, 1}, {1, 2}}},
    {U'F',  {std::nullopt, {1, 2}, {1, 3}}},
    {U'\n', {std::nullopt, {1, 3}, {1, 4}}},

    {U'C',  {std::nullopt, {2, 1}, {2, 2}}},
    {U'R',  {std::nullopt, {2, 2}, {2, 3}}},
    {U'L',  {std::nullopt, {2, 3}, {2, 4}}},
    {U'F',  {std::nullopt, {2, 4}, {2, 5}}},
    {U'\r', {std::nullopt, {2, 5}, {2, 6}}},
    {U'\n', {std::nullopt, {2, 6}, {2, 7}}},

    {U'C',  {std::nullopt, {3, 1}, {3, 2}}},
    {U'R',  {std::nullopt, {3, 2}, {3, 3}}},
    {U'\r', {std::nullopt, {3, 3}, {3, 4}}},

    {U'v',  {std::nullopt, {4, 1}, {4, 2}}},
    {U't',  {std::nullopt, {4, 2}, {4, 3}}},
    {U'a',  {std::nullopt, {4, 3}, {4, 4}}},
    {U'b',  {std::nullopt, {4, 4}, {4, 5}}},
    {U'\v', {std::nullopt, {4, 5}, {4, 6}}},

    {U'f',  {std::nullopt, {5, 1}, {5, 2}}},
    {U'o',  {std::nullopt, {5, 2}, {5, 3}}},
    {U'r',  {std::nullopt, {5, 3}, {5, 4}}},
    {U'm',  {std::nullopt, {5, 4}, {5, 5}}},
    {U'f',  {std::nullopt, {5, 5}, {5, 6}}},
    {U'e',  {std::nullopt, {5, 6}, {5, 7}}},
    {U'e',  {std::nullopt, {5, 7}, {5, 8}}},
    {U'd',  {std::nullopt, {5, 8}, {5, 9}}},
    {U'\f', {std::nullopt, {5, 9}, {5, 10}}},

    {U'N',      {std::nullopt, {6, 1}, {6, 2}}},
    {U'E',      {std::nullopt, {6, 2}, {6, 3}}},
    {U'L',      {std::nullopt, {6, 3}, {6, 4}}},
    {U'\u0085', {std::nullopt, {6, 4}, {6, 6}}},

    {U'l',      {std::nullopt, {7, 1}, {7, 2}}},
    {U'i',      {std::nullopt, {7, 2}, {7, 3}}},
    {U'n',      {std::nullopt, {7, 3}, {7, 4}}},
    {U'e',      {std::nullopt, {7, 4}, {7, 5}}},
    {U'\u2028', {std::nullopt, {7, 5}, {7, 8}}},

    {U'p',      {std::nullopt, {8, 1}, {8, 2}}},
    {U'a',      {std::nullopt, {8, 2}, {8, 3}}},
    {U'r',      {std::nullopt, {8, 3}, {8, 4}}},
    {U'\u2029', {std::nullopt, {8, 4}, {8, 7}}},

    {U'e', {std::nullopt, {9, 1}, {9, 2}}},
    {U'n', {std::nullopt, {9, 2}, {9, 3}}},
    {U'd', {std::nullopt, {9, 3}, {9, 4}}}
  };
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // source_location
BOOST_AUTO_TEST_SUITE_END() // test_nycleus
