#ifndef NYCLEUS_SOURCE_LOCATION_HPP_INCLUDED_X82ZOURXDJ3UMA65W36U8PE5A
#define NYCLEUS_SOURCE_LOCATION_HPP_INCLUDED_X82ZOURXDJ3UMA65W36U8PE5A

#include "unicode/encoding.hpp"
#include "util/cow.hpp"
#include "util/fs_path_utils.hpp"
#include "util/iterator_facade.hpp"
#include "util/non_null_ptr.hpp"
#include "util/unbounded_view.hpp"
#include <concepts>
#include <cstdint>
#include <filesystem>
#include <iterator>
#include <optional>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

namespace nycleus {

/** @brief Represents a position in a source file, without a specified name.
 *
 * The line and column number are one-based. Zero values can be used to indicate
 * absence of information.
 *
 * The integer type is 64 bits wide. This way, if a source file is so large that
 * it overflows the counters, then processing it at the rate of one character
 * per nanosecond (a very optimistic estimate) will take over 500 years. Since
 * it's highly probable that no-one will wait 500 years for a source file to
 * compile, it is safe to assume that overflow will never happen. */
struct file_pos {
  std::uint64_t line{0};
  std::uint64_t col{0};

  file_pos() = default;
  file_pos(std::uint64_t l, std::uint64_t c) noexcept: line{l}, col{c} {}

  [[nodiscard]] bool operator==(file_pos const& other) const noexcept {
    return line == other.line && col == other.col;
  }
};

/** @brief Reprensents a range of positions in a named source file.
 *
 * The optional holding the file name may be empty, indicating the absence of
 * information about the file name. */
struct source_location {
  std::optional<util::cow<std::filesystem::path>> file;
  file_pos first;
  file_pos last;

  source_location() = default;
  source_location(std::optional<util::cow<std::filesystem::path>> f,
    file_pos fst, file_pos lst) noexcept:
    file{std::move(f)}, first{fst}, last{lst} {}

  // The auto-generated operations are not noexcept, since std::optional's copy
  // is not noexcept. However, they can't actually throw exceptions, so we can
  // mark them noexcept.
  source_location(source_location const&) noexcept = default;
  source_location(source_location&&) noexcept = default;
  source_location& operator=(source_location const&) noexcept = default;
  source_location& operator=(source_location&&) noexcept = default;
  ~source_location() noexcept = default;

  [[nodiscard]] bool operator==(source_location const& other) const noexcept {
    return ((!file && !other.file)
      || (file && other.file && **file == **other.file))
      && first == other.first && last == other.last;
  }
};

/** @brief The decoded type of utf8_counted. */
using counted_char32_t = std::pair<char32_t, source_location>;

/** @brief An encoding type that decodes UTF-8 and attaches source location
 * information. */
template<unicode::utf8_code_unit Ch = char8_t>
class utf8_counted {
private:
  source_location loc_;
  bool after_cr_{false};

  /** @brief An iterator that counts the number of elements it produced.
   *
   * This is used to adjust the source location while decoding a code point. We
   * can't obtain this information from the decoded code point, since the error
   * handler can produce any code point, regardless of the number of code units
   * consumed on input. */
  template<std::input_iterator Base>
  requires std::same_as<std::iter_value_t<Base>, Ch>
  class counting_iterator_core {
  private:
    Base base_;
    util::non_null_ptr<std::uint8_t> count_;

    friend class utf8_counted;
    explicit counting_iterator_core(Base base, std::uint8_t& count)
      noexcept(std::is_nothrow_move_constructible_v<Base>):
      base_{std::move(base)}, count_{&count} {}
  public:
    counting_iterator_core() = default;

    [[nodiscard]] Base const& base() const& noexcept {
      return base_;
    }

    [[nodiscard]] Base base() && noexcept {
      return std::move(base_);
    }

  protected:
    static constexpr bool is_single_pass{true};

    [[nodiscard]] decltype(auto) dereference() const
        noexcept(noexcept(*base_)) {
      return *base_;
    }

    void increment() noexcept(noexcept(++base_)) {
      ++base_;
      ++*count_;
    }

    template<std::sentinel_for<Base> Sentinel>
    [[nodiscard]] bool equal_to(Sentinel const& other) const
        noexcept(noexcept(base_ == other)) {
      return base_ == other;
    }
  };

  template<std::input_iterator Base>
  requires std::same_as<std::iter_value_t<Base>, Ch>
  using counting_iterator = util::iterator_facade<counting_iterator_core<Base>>;

public:
  using encoded_type = Ch;
  using decoded_type = counted_char32_t;
  using optional_decoded_type = std::optional<counted_char32_t>;
  using eh_output_type = char32_t;
  using error_type = unicode::utf8_error;

  utf8_counted() = default;

  explicit utf8_counted(std::optional<util::cow<std::filesystem::path>> file)
    noexcept: loc_{std::move(file), {1, 1}, {1, 1}} {}

  template<
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel,
    unicode::error_handler<char32_t, unicode::utf8_error> EH
  >
  requires std::same_as<std::iter_value_t<Iterator>, Ch>
  counted_char32_t decode(Iterator& it, Sentinel iend, EH eh) {
    std::uint8_t len{0};
    char32_t const ch{[&]() {
      counting_iterator<Iterator> wrapped_it{it, len};
      char32_t const result{unicode::utf8<Ch>::decode(wrapped_it,
        std::move(iend), std::move(eh))};
      it = std::move(wrapped_it).base();
      return result;
    }()};

    if(after_cr_ && ch != U'\n') {
      ++loc_.last.line;
      loc_.last.col = 1;
    }
    loc_.first = loc_.last;
    loc_.last.col += len;

    counted_char32_t result{ch, loc_};

    if(ch == U'\n' || ch == U'\v' || ch == U'\f' || ch == U'\u0085'
        || ch == U'\u2028' || ch == U'\u2029') {
      ++loc_.last.line;
      loc_.last.col = 1;
    }
    after_cr_ = (ch == U'\r');

    return result;
  }

  [[nodiscard]] static std::optional<counted_char32_t> make_optional(
    counted_char32_t value
  ) noexcept {
    return std::optional{std::move(value)};
  }

  [[nodiscard]] static std::optional<counted_char32_t> make_empty_optional()
      noexcept {
    return std::nullopt;
  }

  [[nodiscard]] static std::optional<counted_char32_t> get_optional_value(
    std::optional<counted_char32_t> value
  ) noexcept {
    return value;
  }

  template<std::input_iterator Iterator>
  requires std::same_as<std::iter_value_t<Iterator>, Ch>
  void increment(Iterator& it) {
    this->decode(it, util::unreachable_sentinel<Iterator>{},
      unicode::eh_assume_valid{});
  }

  [[nodiscard]] bool operator==(utf8_counted const& other) const noexcept {
    return loc_ == other.loc_ && after_cr_ == other.after_cr_;
  }

  [[nodiscard]] source_location finish() && noexcept {
    if(after_cr_) {
      ++loc_.last.line;
      loc_.last.col = 1;
    }
    loc_.first = loc_.last;
    return loc_;
  }
};

} // nycleus

#endif
