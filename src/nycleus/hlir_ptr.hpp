#ifndef NYCLEUS_HLIR_PTR_HPP_INCLUDED_4WXDO7JAM1JHRL0JHIN8DEQRE
#define NYCLEUS_HLIR_PTR_HPP_INCLUDED_4WXDO7JAM1JHRL0JHIN8DEQRE

#include "util/ptr_with_flags.hpp"
#include <gsl/gsl>
#include <atomic>
#include <cassert>
#include <concepts>
#include <cstddef>
#include <functional>
#include <memory>
#include <type_traits>
#include <utility>

namespace nycleus {

/** @brief The base class for hlir_ptr and diag_ptr targets, which stores the
 * reference count. */
class hlir_base;

/** @brief An object that hlir_ptr and diag_ptr can point to. */
template<typename T>
concept hlir_type = std::derived_from<T, hlir_base>
  && std::has_virtual_destructor_v<T>;

/** @brief An owning pointer to a HLIR node.
 *
 * This is reference-counted pointer used by HLIR nodes to refer to their child
 * nodes, or otherwise by code that owns a HLIR tree. Because there can only be
 * one owner, this type is move-only.
 *
 * To create a HLIR node, it needs to be allocated via raw `new` and passed to
 * the hlir_ptr constructor, which takes ownership of it. To simplify this
 * process, the make_hlir function can be used. */
template<hlir_type T>
class hlir_ptr;

/** @brief A pointer that can either act as a `hlir_ptr` to the `Hlir` type or
 * an ordinary non-owning pointer to the `Other` type.
 *
 * Both pointees must have alignment requirements of at least 2 bytes, since the
 * least significant bit of the pointer is used to distinguish between these two
 * options. */
template<hlir_type Hlir, typename Other>
class maybe_hlir_ptr;

/** @brief A pointer to a HLIR node that is used within diagnostics.
 *
 * This is a reference-counted pointer is used within diagnostics to refer to a
 * HLIR node and extends the lifetime of the subtree for the lifetime of the
 * diagnostic. */
template<hlir_type T>
class diag_ptr;

class hlir_base {
private:
  mutable std::atomic_uintptr_t refs_;

  template<hlir_type>
  friend class nycleus::hlir_ptr;

  template<hlir_type>
  friend class nycleus::diag_ptr;

  template<hlir_type Hlir, typename Other>
  friend class nycleus::maybe_hlir_ptr;

  void increment_refs() const noexcept {
    refs_.fetch_add(1, std::memory_order::relaxed);
  }

  [[nodiscard]] bool decrement_refs() const noexcept {
    return refs_.fetch_sub(1, std::memory_order::acq_rel) == 1;
  }

protected:
  hlir_base() noexcept: refs_{1} {}
  ~hlir_base() noexcept = default;

public:
  hlir_base(hlir_base const&) = delete;
  hlir_base& operator=(hlir_base const&) = delete;
};

template<hlir_type T>
class diag_ptr {
private:
  gsl::owner<T const*> ptr_;

  template<hlir_type>
  friend class nycleus::diag_ptr;

  void destroy() noexcept(std::is_nothrow_destructible_v<T>) {
    if(ptr_ && ptr_->hlir_base::decrement_refs()) {
      delete ptr_;
    }
  }

public:
  diag_ptr() = default;

  diag_ptr(std::nullptr_t) noexcept: diag_ptr{} {}

  explicit diag_ptr(T const& ptr) noexcept: ptr_{&ptr} {
    ptr_->hlir_base::increment_refs();
  }

  diag_ptr(diag_ptr const& other) noexcept: ptr_{other.ptr_} {
    if(ptr_) {
      ptr_->hlir_base::increment_refs();
    }
  }

  diag_ptr(diag_ptr&& other) noexcept: ptr_{other.ptr_} {
    other.ptr_ = nullptr;
  }

  diag_ptr& operator=(diag_ptr const& other)
      noexcept(std::is_nothrow_destructible_v<T>) {
    if(this == &other) {
      return *this;
    }
    destroy();
    ptr_ = other.ptr_;
    if(ptr_) {
      ptr_->hlir_base::increment_refs();
    }
    return *this;
  }

  diag_ptr& operator=(diag_ptr&& other)
      noexcept(std::is_nothrow_destructible_v<T>) {
    if(this == &other) {
      return *this;
    }
    destroy();
    ptr_ = other.ptr_;
    other.ptr_ = nullptr;
    return *this;
  }

  ~diag_ptr() noexcept(std::is_nothrow_destructible_v<T>) {
    destroy();
  }

  template<hlir_type U>
  requires (!std::same_as<T, U>) && std::derived_from<U, T>
  diag_ptr(diag_ptr<U> const& other) noexcept: ptr_{other.ptr_} {
    if(ptr_) {
      ptr_->hlir_base::increment_refs();
    }
  }

  template<hlir_type U>
  requires (!std::same_as<T, U>) && std::derived_from<U, T>
  diag_ptr(diag_ptr<U>&& other) noexcept: ptr_{other.ptr_} {
    other.ptr_ = nullptr;
  }

  /** @brief Converts a pointer to a base class to a pointer to a derived class.
   *
   * The pointer must actually point to an object of the specified derived
   * class, otherwise the behavior is undefined. */
  template<std::derived_from<T> U>
  requires std::is_polymorphic_v<T>
  [[nodiscard]] diag_ptr<U> static_downcast() const& noexcept {
    assert(dynamic_cast<U const*>(get()));
    U& result{*static_cast<U const*>(ptr_)};
    ptr_->hlir_base::increment_refs();
    return diag_ptr<U>{result};
  }

  /** @brief Converts a pointer to a base class to a pointer to a derived class.
   *
   * The pointer must actually point to an object of the specified derived
   * class, otherwise the behavior is undefined. */
  template<std::derived_from<T> U>
  requires std::is_polymorphic_v<T>
  [[nodiscard]] diag_ptr<U> static_downcast() && noexcept {
    assert(dynamic_cast<U const*>(get()));
    U& result{*static_cast<U const*>(ptr_)};
    ptr_ = nullptr;
    return diag_ptr<U>{result};
  }

  [[nodiscard]] T const& operator*() const noexcept {
    assert(ptr_);
    return *ptr_;
  }

  [[nodiscard]] T const* operator->() const noexcept {
    assert(ptr_);
    return ptr_;
  }

  [[nodiscard]] T const* get() const noexcept {
    return ptr_;
  }

  [[nodiscard]] explicit operator bool() const noexcept {
    return ptr_ != nullptr;
  }

  [[nodiscard]] friend bool operator==(
    diag_ptr const& a,
    diag_ptr const& b
  ) noexcept {
    return a.ptr_ == b.ptr_;
  }
};

template<hlir_type T>
class hlir_ptr {
private:
  gsl::owner<T*> ptr_;

  template<hlir_type>
  friend class nycleus::hlir_ptr;

  template<hlir_type Hlir, typename Other>
  friend class nycleus::maybe_hlir_ptr;

  void destroy() {
    if(ptr_ && ptr_->hlir_base::decrement_refs()) {
      delete ptr_;
    }
  }

public:
  hlir_ptr() noexcept: ptr_{nullptr} {}

  hlir_ptr(std::nullptr_t) noexcept: hlir_ptr{} {}

  explicit hlir_ptr(T& ptr) noexcept: ptr_{&ptr} {
    assert(ptr_->hlir_base::refs_.load(std::memory_order::relaxed) == 1);
  }

  hlir_ptr(hlir_ptr const&) = delete;
  hlir_ptr& operator=(hlir_ptr const&) = delete;

  hlir_ptr(hlir_ptr&& other) noexcept: ptr_{other.ptr_} {
    other.ptr_ = nullptr;
  }

  hlir_ptr& operator=(hlir_ptr&& other) noexcept {
    if(this != &other) {
      destroy();
      ptr_ = other.ptr_;
      other.ptr_ = nullptr;
    }
    return *this;
  }

  ~hlir_ptr() noexcept {
    destroy();
  }

  template<hlir_type U>
  requires (!std::same_as<T, U>) && std::derived_from<U, T>
  hlir_ptr(hlir_ptr<U>&& other) noexcept: ptr_{other.ptr_} {
    other.ptr_ = nullptr;
  }

  /** @brief Converts a pointer to a base class to a pointer to a derived class.
   *
   * The pointer must actually point to an object of the specified derived
   * class, otherwise the behavior is undefined. */
  template<hlir_type U>
  requires (!std::same_as<T, U>) && std::derived_from<U, T>
  [[nodiscard]] hlir_ptr<U> static_downcast() && noexcept {
    assert(dynamic_cast<U*>(get()));
    U& result{*static_cast<U*>(ptr_)};
    ptr_ = nullptr;
    return hlir_ptr<U>{result};
  }

  [[nodiscard]] T& operator*() const noexcept {
    assert(ptr_);
    return *ptr_;
  }

  [[nodiscard]] T* operator->() const noexcept {
    assert(ptr_);
    return ptr_;
  }

  [[nodiscard]] T* get() const noexcept {
    return ptr_;
  }

  [[nodiscard]] explicit operator bool() const noexcept {
    return ptr_ != nullptr;
  }

  [[nodiscard]] friend bool operator==(
    hlir_ptr const& a,
    hlir_ptr const& b
  ) noexcept {
    return a.ptr_ == b.ptr_;
  }
};

/** @brief Creates a new HLIR node and returns a hlir_ptr to it.
 *
 * May also throw whatever is thrown by the node's construction.
 *
 * @throws std::bad_alloc */
template<hlir_type T, typename... Args>
requires std::constructible_from<T, Args...>
hlir_ptr<T> make_hlir(Args&&... args) {
  return hlir_ptr<T>{*::new T{std::forward<Args>(args)...}};
}

template<hlir_type Hlir, typename Other>
class maybe_hlir_ptr {
private:
  /** @brief A pointer to the pointee, together with a flag indicating whether
   * this is an owning `hlir_ptr`-like pointer.
   *
   * If the pointer is null, the flag must be clear. */
  util::ptr_with_flags<std::byte, 1> ptr_;

  void destroy() {
    if(is_hlir() && reinterpret_cast<Hlir*>(ptr_.get_ptr())
        ->hlir_base::decrement_refs()) {
      delete reinterpret_cast<Hlir*>(ptr_.get_ptr());
    }
  }

public:
  /** @brief Creates a null pointer. */
  maybe_hlir_ptr() noexcept: ptr_{nullptr, false} {}

  /** @brief Creates a null pointer. */
  maybe_hlir_ptr(std::nullptr_t) noexcept: ptr_{nullptr, false} {}

  /** @brief Creates an owning pointer to a HLIR node. */
  maybe_hlir_ptr(hlir_ptr<Hlir> p) noexcept:
      ptr_{reinterpret_cast<std::byte*>(p.ptr_), p.ptr_ != nullptr} {
    p.ptr_ = nullptr;
  }

  /** @brief Creates a non-owning pointer to `Other`. */
  maybe_hlir_ptr(Other* p) noexcept:
    ptr_{reinterpret_cast<std::byte*>(p), false} {}

  maybe_hlir_ptr(maybe_hlir_ptr const&) = delete;

  maybe_hlir_ptr& operator=(maybe_hlir_ptr const&) = delete;

  maybe_hlir_ptr(maybe_hlir_ptr&& other) noexcept: ptr_{other.ptr_} {
    other.ptr_ = nullptr;
  }

  maybe_hlir_ptr& operator=(maybe_hlir_ptr&& other) noexcept {
    if(this != &other) {
      destroy();
      ptr_ = other.ptr_;
      other.ptr_ = nullptr;
    }
    return *this;
  }

  ~maybe_hlir_ptr() noexcept {
    destroy();
  }

  /** @brief Checks if this is currently an owning `hlir_ptr`-like pointer. */
  [[nodiscard]] bool is_hlir() const noexcept {
    return ptr_.get_flag<0>();
  }

  /** @brief If this is an owning `hlir_ptr`-like pointer, returns the
   * underlying raw pointer; otherwise, returns a null pointer. */
  [[nodiscard]] Hlir* as_hlir() const noexcept {
    return is_hlir() ? reinterpret_cast<Hlir*>(ptr_.get_ptr()) : nullptr;
  }

  /** @brief If this is an owning `hlir_ptr`-like pointer, returns a reference
   * to the pointee; otherwise, the behavior is undefined. */
  [[nodiscard]] Hlir& assume_hlir() const noexcept {
    assert(is_hlir());
    return *reinterpret_cast<Hlir*>(ptr_.get_ptr());
  }

  /** @brief Checks if this is currently a non-owning pointer to `Other`. */
  [[nodiscard]] bool is_other() const noexcept {
    return !ptr_.get_flag<0>() && ptr_.get_ptr() != nullptr;
  }

  /** @brief If this is a non-owning pointer to `Other`, returns the underlying
   * raw pointer; otherwise, returns a null pointer. */
  [[nodiscard]] Other* as_other() const noexcept {
    return is_hlir() ? nullptr : reinterpret_cast<Other*>(ptr_.get_ptr());
  }

  /** @brief If this is a non-owning pointer to `Other`, returns a reference to
   * the pointee; otherwise, the behavior is undefined. */
  [[nodiscard]] Other& assume_other() const noexcept {
    assert(is_other());
    return *reinterpret_cast<Other*>(ptr_.get_ptr());
  }

  [[nodiscard]] friend bool operator==(
    maybe_hlir_ptr const& a,
    maybe_hlir_ptr const& b
  ) noexcept {
    return a.ptr_ == b.ptr_;
  }

  [[nodiscard]] explicit operator bool() const noexcept {
    return *this != nullptr;
  }
};

} // nycleus

template<::nycleus::hlir_type T>
struct std::hash<::nycleus::hlir_ptr<T>> {
  ::std::size_t operator()(::nycleus::hlir_ptr<T> const& ptr) const noexcept {
    return ::std::hash<T*>{}(ptr.get());
  }
};

template<::nycleus::hlir_type T>
struct std::hash<::nycleus::diag_ptr<T>> {
  ::std::size_t operator()(::nycleus::diag_ptr<T> const& ptr) const noexcept {
    return ::std::hash<T const*>{}(ptr.get());
  }
};

#endif
