#ifndef NYCLEUS_ENTITY_BAG_HPP_INCLUDED_KNK0W1XKLYA1UBT167Y4ESD27
#define NYCLEUS_ENTITY_BAG_HPP_INCLUDED_KNK0W1XKLYA1UBT167Y4ESD27

#include "nycleus/entity.hpp"
#include "nycleus/global_name.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "util/overflow.hpp"
#include <cassert>
#include <cstddef>
#include <exception>
#include <memory>
#include <new>
#include <ranges>
#include <stdexcept>
#include <unordered_set>

namespace nycleus {

class diagnostic_tracker;

/** @brief A container holding entities comprising a Nycleus program. */
class entity_bag {
private:
  struct named_entity_hash {
    [[nodiscard]] std::size_t operator()(
      std::unique_ptr<named_entity> const& entity
    ) const noexcept {
      assert(entity);
      return entity->name.hash();
    }

    [[nodiscard]] std::size_t operator()(
      global_name const& name
    ) const noexcept {
      return name.hash();
    }

    using is_transparent = void;
  };

  struct named_entity_equal {
    [[nodiscard]] bool operator()(
      std::unique_ptr<named_entity> const& a,
      std::unique_ptr<named_entity> const& b
    ) const noexcept {
      assert(a);
      assert(b);
      return a->name == b->name;
    }

    [[nodiscard]] bool operator()(
      std::unique_ptr<named_entity> const& a,
      global_name const& b
    ) const noexcept {
      assert(a);
      return a->name == b;
    }

    [[nodiscard]] bool operator()(
      global_name const& a,
      std::unique_ptr<named_entity> const& b
    ) const noexcept {
      assert(b);
      return a == b->name;
    }

    using is_transparent = void;
  };

  using set_type = std::unordered_multiset<std::unique_ptr<named_entity>,
    named_entity_hash, named_entity_equal>;

  set_type entities_;

  explicit entity_bag(set_type entities) noexcept:
    entities_{std::move(entities)} {}

public:
  entity_bag() noexcept = default;

  /** @brief Adds the given entity to the bag. */
  void add(std::unique_ptr<named_entity>) noexcept;

  /** @brief Processes the given definition HLIR, corresponding to a top-level
   * definition, and fills this bag with the resulting entities.
   *
   * This also processes definition nodes nested more deeply inside the tree and
   * puts the entities produced by them into this bag. After this function
   * returns, the processed HLIR trees will not contain any definition nodes.
   *
   * This also processes all named_var_ref nodes within this tree, resolving
   * those that can be resolved to entities defined within this tree. After this
   * function returns, the only named_var_ref nodes that remain will be the ones
   * that cannot be resolved in this manner, thus being presumed to refer to
   * top-level names.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws diagnostic_tracker_error */
  void process_global_def(
    hlir_ptr<hlir::def>,
    global_name,
    diagnostic_tracker&
  );

  /** @brief Given a range of entity bags, produces a new entity bag containing
   * all the entities from all the input bags.
   *
   * @throws std::bad_alloc */
  template<std::ranges::forward_range Range>
  requires std::same_as<std::ranges::range_value_t<Range>, entity_bag>
  static entity_bag merge(Range&& bags) {
    set_type entities;

    if constexpr(std::ranges::forward_range<Range>) {
      set_type::size_type num_entities{0};
      for(auto const& bag: bags) {
        try {
          num_entities = util::throwing_add(num_entities, bag.entities_.size());
        } catch(std::overflow_error const&) {
          std::throw_with_nested(std::bad_alloc{});
        }
      }
      entities.reserve(num_entities);
    }

    for(auto& bag: bags) {
      entities.merge(bag.entities_);
    }

    return entity_bag{std::move(entities)};
  }

  /** @brief Retrieves the entity with the given name.
   *
   * If there is no such entity, returns a null pointer.
   *
   * If there are multiple entities with the given name, returns an unspecified
   * one of them. */
  named_entity* get(global_name const&) const noexcept;

  /** @brief Finds entities with identical names and reports the appropriate
   * diagnostics.
   *
   * The only duplicates that are allowed to exist are top-level duplicates.
   * Nested duplicates should never be created in the first place.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws diagnostic_tracker_error */
  void report_duplicates(diagnostic_tracker&);

  /** @brief Returns a range of entities in the bag.
   *
   * This returns an unspecified sized forward range that produces lvalue
   * references to entities contained in the bag. Operations on the range do not
   * throw exceptions. */
  auto entities() const noexcept {
    return entities_ | std::views::transform(
      [](std::unique_ptr<named_entity> const& entity) -> named_entity& {
        assert(entity);
        return *entity;
      });
  }
};

} // nycleus

#endif
