// #include <boost/test/unit_test.hpp>

// #include "nycleus/diagnostic.hpp"
// #include "nycleus/diagnostic_logger.hpp"
// #include "nycleus/entity.hpp"
// #include "nycleus/source_location.hpp"
// #include "nycleus/type_utils.hpp"
// #include "util/downcast.hpp"
// #include "util/generator.hpp"
// #include <cassert>
// #include <algorithm>
// #include <span>

// namespace diag = nycleus::diag;

// namespace {

// using src_loc = nycleus::source_location;

// using cycle_entry = diag::class_cycle::entry;

// [[nodiscard]] bool cycles_eq(
//   std::span<cycle_entry const> a,
//   std::span<cycle_entry const> b
// ) {
//   assert(!a.empty());
//   assert(!b.empty());

//   if(a.size() != b.size()) {
//     return false;
//   }

//   auto start{std::ranges::find_if(a, [&](cycle_entry e) {
//     return e.cl == b[0].cl && e.field_index == b[0].field_index;
//   })};
//   if(start == a.end()) {
//     return false;
//   }

//   // TODO: views::concat
//   auto a_rearranged{[](
//     std::span<cycle_entry const> a,
//     std::span<cycle_entry const>::iterator start
//   ) -> util::generator<cycle_entry, cycle_entry> {
//     co_yield util::elements_of{std::ranges::subrange{start, a.end()}};
//     co_yield util::elements_of{std::ranges::subrange{a.begin(), start}};
//   }(a, std::move(start))};

//   return std::ranges::equal(a_rearranged, b, [](cycle_entry a, cycle_entry b) {
//     return a.cl == b.cl && a.field_index == b.field_index;
//   });
// }

// } // (anonymous)

// BOOST_AUTO_TEST_SUITE(test_nycleus)
// BOOST_AUTO_TEST_SUITE(class_cycles)

// BOOST_AUTO_TEST_CASE(one) {
//   nycleus::diagnostic_logger dt;
//   nycleus::type_registry tr;

//   nycleus::class_type cl;
//   cl.name = {u8"A"};
//   cl.fields.emplace_back(&tr.get_int_type(64, false), true, u8"x", src_loc{});

//   nycleus::detect_class_cycles(0, cl, dt);

//   BOOST_TEST(!cl.has_cycles());

//   BOOST_TEST(std::move(dt).extract_diags().empty());
// }

// BOOST_AUTO_TEST_CASE(tree) {
//   nycleus::diagnostic_logger dt;

//   nycleus::class_type cl[5];
//   cl[0].name = {u8"A"};
//   cl[0].fields.emplace_back(&cl[1], true, u8"x", src_loc{});
//   cl[1].name = {u8"B"};
//   cl[1].fields.emplace_back(&cl[2], true, u8"x", src_loc{});
//   cl[1].fields.emplace_back(&cl[3], true, u8"y", src_loc{});
//   cl[2].name = {u8"C"};
//   cl[3].name = {u8"D"};
//   cl[3].fields.emplace_back(&cl[4], true, u8"x", src_loc{});
//   cl[4].name = {u8"E"};

//   nycleus::detect_class_cycles(0, cl[0], dt);
//   nycleus::detect_class_cycles(0, cl[1], dt);
//   nycleus::detect_class_cycles(0, cl[2], dt);
//   nycleus::detect_class_cycles(0, cl[3], dt);
//   nycleus::detect_class_cycles(0, cl[4], dt);

//   BOOST_TEST(!cl[0].has_cycles());
//   BOOST_TEST(!cl[1].has_cycles());
//   BOOST_TEST(!cl[2].has_cycles());
//   BOOST_TEST(!cl[3].has_cycles());
//   BOOST_TEST(!cl[4].has_cycles());

//   BOOST_TEST(std::move(dt).extract_diags().empty());
// }

// BOOST_AUTO_TEST_CASE(self) {
//   nycleus::diagnostic_logger dt;

//   nycleus::class_type cl;
//   cl.name = {u8"A"};
//   cl.fields.emplace_back(&cl, true, u8"x", src_loc{});

//   nycleus::detect_class_cycles(0, cl, dt);

//   BOOST_TEST(cl.has_cycles());

//   auto const diags{std::move(dt).extract_diags()};
//   BOOST_REQUIRE(diags.size() == 1);
//   auto const& diag{util::downcast<diag::class_cycle const&>(*diags[0])};
//   BOOST_TEST(cycles_eq(diag.entries, std::vector{cycle_entry{&cl, 0}}));
// }

// BOOST_AUTO_TEST_CASE(cycle) {
//   nycleus::diagnostic_logger dt;

//   nycleus::class_type cl[6];
//   cl[0].name = {u8"A"};
//   cl[0].fields.emplace_back(&cl[1], true, u8"x", src_loc{});
//   cl[1].name = {u8"B"};
//   cl[1].fields.emplace_back(&cl[2], true, u8"x", src_loc{});
//   cl[2].name = {u8"C"};
//   cl[2].fields.emplace_back(&cl[3], true, u8"x", src_loc{});
//   cl[3].name = {u8"D"};
//   cl[3].fields.emplace_back(&cl[4], true, u8"x", src_loc{});
//   cl[4].name = {u8"E"};
//   cl[4].fields.emplace_back(&cl[5], true, u8"x", src_loc{});
//   cl[4].fields.emplace_back(&cl[2], true, u8"y", src_loc{});
//   cl[5].name = {u8"F"};

//   nycleus::detect_class_cycles(0, cl[0], dt);
//   nycleus::detect_class_cycles(0, cl[1], dt);
//   nycleus::detect_class_cycles(0, cl[2], dt);
//   nycleus::detect_class_cycles(0, cl[3], dt);
//   nycleus::detect_class_cycles(0, cl[4], dt);
//   nycleus::detect_class_cycles(0, cl[5], dt);

//   BOOST_TEST(cl[0].has_cycles());
//   BOOST_TEST(cl[1].has_cycles());
//   BOOST_TEST(cl[2].has_cycles());
//   BOOST_TEST(cl[3].has_cycles());
//   BOOST_TEST(cl[4].has_cycles());
//   BOOST_TEST(!cl[5].has_cycles());

//   auto const diags{std::move(dt).extract_diags()};
//   BOOST_REQUIRE(diags.size() == 1);
//   auto const& diag{util::downcast<diag::class_cycle const&>(*diags[0])};
//   BOOST_TEST(cycles_eq(diag.entries, std::vector{cycle_entry{&cl[4], 1},
//     cycle_entry{&cl[3], 0}, cycle_entry{&cl[2], 0}}));
// }

// BOOST_AUTO_TEST_CASE(multiple_cycles) {
//   nycleus::diagnostic_logger dt;

//   nycleus::class_type cl[4];
//   cl[0].name = {u8"A"};
//   cl[0].fields.emplace_back(&cl[1], true, u8"x", src_loc{});
//   cl[1].name = {u8"B"};
//   cl[1].fields.emplace_back(&cl[2], true, u8"x", src_loc{});
//   cl[2].name = {u8"C"};
//   cl[2].fields.emplace_back(&cl[3], true, u8"x", src_loc{});
//   cl[2].fields.emplace_back(&cl[0], true, u8"y", src_loc{});
//   cl[3].name = {u8"D"};
//   cl[3].fields.emplace_back(&cl[1], true, u8"x", src_loc{});

//   nycleus::detect_class_cycles(0, cl[0], dt);
//   nycleus::detect_class_cycles(0, cl[1], dt);
//   nycleus::detect_class_cycles(0, cl[2], dt);
//   nycleus::detect_class_cycles(0, cl[3], dt);

//   BOOST_TEST(cl[0].has_cycles());
//   BOOST_TEST(cl[1].has_cycles());
//   BOOST_TEST(cl[2].has_cycles());
//   BOOST_TEST(cl[3].has_cycles());

//   auto const diags{std::move(dt).extract_diags()};
//   std::vector const cycle1{cycle_entry{&cl[2], 1}, cycle_entry{&cl[1], 0},
//     cycle_entry{&cl[0], 0}};
//   std::vector const cycle2{cycle_entry{&cl[3], 0}, cycle_entry{&cl[2], 0},
//     cycle_entry{&cl[1], 0}};
//   BOOST_REQUIRE(diags.size() == 1 || diags.size() == 2);
//   if(diags.size() == 1) {
//     auto const& diag{util::downcast<diag::class_cycle const&>(*diags[0])};
//     BOOST_TEST((cycles_eq(diag.entries, cycle1)
//       || cycles_eq(diag.entries, cycle2)));
//   } else {
//     auto const& diag1{util::downcast<diag::class_cycle const&>(*diags[0])};
//     auto const& diag2{util::downcast<diag::class_cycle const&>(*diags[1])};
//     BOOST_TEST((cycles_eq(diag1.entries, cycle1)
//       && cycles_eq(diag2.entries, cycle2)
//       || cycles_eq(diag1.entries, cycle2)
//       && cycles_eq(diag2.entries, cycle1)));
//   }
// }

// BOOST_AUTO_TEST_SUITE_END() // class_cycles
// BOOST_AUTO_TEST_SUITE_END() // test_nycleus
