#include "nycleus/global_name.hpp"
#include "util/format.hpp"
#include <boost/container_hash/hash.hpp>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <iterator>
#include <utility>
#include <vector>

bool nycleus::global_name::operator==(nycleus::global_name const& other)
    const noexcept {
  return name == other.name
    && nested_parts == other.nested_parts
    && is_initializer == other.is_initializer;
}

std::size_t nycleus::global_name::hash() const noexcept {
  std::size_t result{0};

  boost::hash_combine(result, std::hash<std::u8string>{}(name));

  using nested_parts_size_t = std::vector<std::pair<std::uint64_t,
    std::u8string>>::size_type;
  boost::hash_combine(result,
    std::hash<nested_parts_size_t>{}(nested_parts.size()));

  for(auto const& part: nested_parts) {
    assert(!part.valueless_by_exception());
    boost::hash_combine(result, std::hash<nested_part>{}(part));
  }

  boost::hash_combine(result, std::hash<bool>{}(is_initializer));

  return result;
}

template util::output_counting_iterator nycleus::global_name::format<
  util::output_counting_iterator
>(util::output_counting_iterator) const;
template std::back_insert_iterator<std::u8string> nycleus::global_name::format<
  std::back_insert_iterator<std::u8string>
>(std::back_insert_iterator<std::u8string>) const;
