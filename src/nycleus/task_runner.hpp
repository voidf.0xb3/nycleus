#ifndef NYCLEUS_TASK_RUNNER_HPP_INCLUDED_AZOEVV8858WEN0FD32WSS8SAG
#define NYCLEUS_TASK_RUNNER_HPP_INCLUDED_AZOEVV8858WEN0FD32WSS8SAG

#include "nycleus/entity.hpp"
#include "nycleus/task_context.hpp"
#include "util/guarded_var.hpp"
#include "util/intrusive_list.hpp"
#include "util/non_null_ptr.hpp"
#include "util/stackful_coro.hpp"
#include <cassert>
#include <condition_variable>
#include <cstddef>
#include <memory>
#include <optional>
#include <stop_token>
#include <unordered_set>
#include <variant>
#include <vector>

namespace nycleus {

class diagnostic_tracker;
class entity_bag;
class target;
class type_registry;

/** @brief A mechanism to execute tasks with potentially complex dependencies
 * that may not be known until the tasks are started.
 *
 * Each task is a stackful coroutine that is given a task_context. If it
 * requires that another task be completed, it calls task_context::depend,
 * giving it a task_info describing the task to be executed. The coroutine must
 * not suspend in any other manner. */
class task_runner {
private:
  struct task {
    /** @brief The next task in the `ready_tasks` linked list. */
    std::unique_ptr<task> next;

    util::stackful_coro::processor<> proc;

    /** @brief The task blocking this task's execution.
     *
     * When task_context is used to block on a dependency, it remembers that
     * dependency here. The code that started the task will check here to see
     * what the task depends on. */
    std::optional<task_dyn_dep_info> dependency;

    /** @brief The task_info describing what this task does. */
    task_info this_task;

    explicit task(task_info kind) noexcept: this_task{kind} {}

    task(task const&) = delete;
    task& operator=(task const&) = delete;
    ~task() noexcept = default;
  };

  // TODO: Optimize multithreaded task scheduling

  struct state_t {
    // TODO: Prioritize processing tasks that block other tasks
    util::intrusive_list<function> resolve_fn_backlog;
    util::intrusive_list<function> analyze_fn_backlog;
    util::intrusive_list<global_var> resolve_global_var_backlog;
    util::intrusive_list<class_type> resolve_class_backlog;
    util::intrusive_list<class_type> class_cycles_backlog;

    /** @brief The number of tasks that cannot start because earlier tasks have
     * not completed yet.
     *
     * Each entity can have no more than one such task. Therefore, this number
     * cannot exceed the number of distinct memory addresses, so the uintptr_t
     * type is appropriate. */
    std::uintptr_t statically_blocked_tasks;

    struct dep_hash {
      std::size_t operator()(std::unique_ptr<task> const& t) const noexcept {
        assert(t);
        assert(t->dependency);
        return (*this)(nycleus::get_task_info(*t->dependency));
      }

      std::size_t operator()(task_info t) const noexcept {
        return std::hash<task_info>{}(std::move(t));
      }

      using is_transparent = void;
    };

    struct dep_eq {
      bool operator()(
        std::unique_ptr<task> const& a,
        std::unique_ptr<task> const& b
      ) const noexcept {
        assert(a);
        assert(a->dependency);
        assert(b);
        assert(b->dependency);
        return nycleus::get_task_info(*a->dependency)
          == nycleus::get_task_info(*b->dependency);
      }

      bool operator()(
        std::unique_ptr<task> const& a,
        task_info b
      ) const noexcept {
        assert(a);
        assert(a->dependency);
        return nycleus::get_task_info(*a->dependency) == b;
      }

      bool operator()(
        task_info a,
        std::unique_ptr<task> const& b
      ) const noexcept {
        assert(b);
        assert(b->dependency);
        return a == nycleus::get_task_info(*b->dependency);
      }

      using is_transparent = void;
    };

    /** @brief Tasks that have already started but cannot continue due to a
     * dependency on another task that has not completed yet.
     *
     * This is keyed by the task_info of the blocking dependency task. */
    std::unordered_multiset<std::unique_ptr<task>,
      dep_hash, dep_eq> blocked_tasks;

    /** @brief Tasks that were blocked on a dependency which has since
     * completed, allowing these tasks to resume.
     *
     * These tasks are organized in a linked list. This data member points to
     * the first task in the list; each subsequent task is stored in the `next`
     * data member of the previous one. */
    std::unique_ptr<task> ready_tasks;

    /** @brief The number of threads currently running tasks.
     *
     * This does not count threads that are waiting for more tasks to become
     * available. This is used to detect when all threads are blocked, so that
     * we can trigger cycle detection. */
    std::uintptr_t num_running_threads{0};
  };

  util::guarded_var<state_t> state_;

  std::condition_variable cv_;

  /** @brief Initializes the state with tasks pertaining to the entities in the
   * given bag. */
  [[nodiscard]] static state_t make_state(entity_bag const&) noexcept;

  struct this_task_hash {
    std::size_t operator()(util::non_null_ptr<task> t) const noexcept {
      return (*this)(t->this_task);
    }

    std::size_t operator()(task_info t) const noexcept {
      return std::hash<task_info>{}(t);
    }

    using is_transparent = void;
  };

  struct this_task_eq {
    bool operator()(
      util::non_null_ptr<task> a,
      util::non_null_ptr<task> b
    ) const noexcept {
      return a->this_task == b->this_task;
    }

    bool operator()(util::non_null_ptr<task> a, task_info b) const noexcept {
      return a->this_task == b;
    }

    bool operator()(task_info a, util::non_null_ptr<task> b) const noexcept {
      return a == b->this_task;
    }

    using is_transparent = void;
  };

  /** @brief The type for a set of non-owning pointers to tasks keyed by
   * task_info.
   *
   * This is used when breaking cycles. */
  using this_task_set_t = std::unordered_set<util::non_null_ptr<task>,
    this_task_hash, this_task_eq>;

  /** @brief Finds a dependency cycle in the given state and returns a vector
   * tracing the cycle.
   *
   * If there are no cycles, the behavior is undefined. If there are multiple
   * cycles, one of them chosen arbitrarily.
   *
   * The state is accepted by reference to indicate that the state mutex must be
   * held.
   *
   * The return value is a vector listing the dependencies comprising the cycle.
   * Each entry describes the manner in which a task in the cycle depends on its
   * dependency; the next entry describes that dependency's dependency on its
   * dependency; and so on. The vector is treated as cyclical: the first entry
   * is deemed as following the last one. The point in the cycle where the
   * vector starts is chosen arbitrarily.
   *
   * @throws std::bad_alloc */
  static std::vector<task_dep_info> find_cycle(
    state_t&,
    this_task_set_t const&
  );

  /** @brief Finds a dependency cycle in the give state, picks a task in the
   * cycle and unblocks it.
   *
   * This function finds a cycle, reports it via the diagnostic tracker, and
   * unblocks one task of the cycle.
   *
   * If there are no cycles, the behavior is undefined. If there are multiple
   * cycles, one is chosen arbitrarily. Also, the task to unblock is chosen
   * arbitrarily.
   *
   * The state is accepted by reference to indicate that the state mutex must be
   * held.
   *
   * @throws std::bad_alloc */
  static void break_cycle(state_t&, diagnostic_tracker&);

public:
  /** @brief Initializes the task runner to run all tasks pertaining to the
   * entities in the given bag. */
  explicit task_runner(entity_bag const&) noexcept;

  /** @brief Runs the tasks.
   *
   * This can be called from multiple threads to run tasks simultaneously.
   * Returns only when all tasks are completed.
   *
   * The supplied stop_token can be used to cancel the worker. Other worker
   * threads will continue running.
   *
   * If a task throws an exception, the exception will be rethrown out of this
   * function on whichever thread executed the offending task. The task will not
   * be considered completed and will not unblock its dependents, but will also
   * not be restared. This means that other threads will potentially need to be
   * cancelled to avoid a deadlock.
   *
   * @throws std::bad_alloc
   * @throws std::bad_exception
   * @throws std::length_error
   * @throws target_error
   * @throws diagnostic_tracker_error */
  void run(
    diagnostic_tracker&,
    entity_bag const&,
    target const&,
    type_registry&,
    std::stop_token
  );
};

} // nycleus

#endif
