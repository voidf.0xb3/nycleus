#ifndef NYCLEUS_DIAGNOSTIC_LOGGER_HPP_INCLUDED_CKX3AAIYNYCQNQ6PP744EBF5D
#define NYCLEUS_DIAGNOSTIC_LOGGER_HPP_INCLUDED_CKX3AAIYNYCQNQ6PP744EBF5D

#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_tracker.hpp"
#include "nycleus/source_location.hpp"
#include "util/guarded_var.hpp"
#include <gsl/gsl>
#include <cstdint>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

namespace nycleus {

/** @brief Thrown by `diagnostic_logger` if the error limit is exceeded. */
class too_many_errors final: public diagnostic_tracker_error {
public:
  [[nodiscard]] virtual gsl::czstring what() const noexcept override;
};

/** @brief A diagnostic tracker that remembers all diagnostics and can be later
 * used to retrieve them.
 *
 * It can be configured with an error limit, in which case it throws
 * `too_many_errors` when the number of diagnostics reaches that limit. The
 * limit can be set to zero to disable this feature. */
class diagnostic_logger final: public diagnostic_tracker {
private:
  struct body_t {
    std::vector<std::unique_ptr<diagnostic>> diags;
    std::uint64_t error_count;
    std::uint64_t const error_limit;

    explicit body_t(std::uint64_t error_limit) noexcept:
      error_count{0}, error_limit{error_limit} {}
  };

  util::guarded_var<body_t> body_;

public:
  diagnostic_logger() noexcept: body_{static_cast<std::uint64_t>(0)} {}

  explicit diagnostic_logger(std::uint64_t error_limit) noexcept:
    body_{error_limit} {}

  /** @brief Returns a vector of collected diagnostics.
   *
   * This requires an rvalue to indicate the fact that this can only be called
   * once. Afterwards, the diagnostic tracker cannot be used for anything and
   * must be discarded. In particular, extrernal synchronization is required to
   * ensure that all uses of the tracker happen before this call.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] std::vector<std::unique_ptr<diagnostic>> extract_diags() &&;

private:
  /** @throws std::bad_alloc
   * @throws too_many_errors */
  virtual void do_track(std::unique_ptr<diagnostic>) override;
};

} // nycleus

#endif
