#include "nycleus/entity.hpp"
#include "util/format.hpp"
#include <iterator>
#include <string>

template util::output_counting_iterator nycleus::detail_::format_type<
  util::output_counting_iterator
>(util::output_counting_iterator, type const&);
template std::back_insert_iterator<std::u8string> nycleus::detail_::format_type<
  std::back_insert_iterator<std::u8string>
>(std::back_insert_iterator<std::u8string>, nycleus::type const&);
