#include "nycleus/diagnostic.hpp"
#include <memory>

std::unique_ptr<nycleus::diagnostic> nycleus::diagnostic::clone() const {
  return this->const_visit([]<typename T>(T const& diag)
      -> std::unique_ptr<nycleus::diagnostic> {
    return std::make_unique<T>(diag);
  });
}
