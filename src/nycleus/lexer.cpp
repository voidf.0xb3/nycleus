#include "nycleus/lexer.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/token.hpp"
#include "nycleus/utils.hpp"
#include "unicode/basic.hpp"
#include "unicode/encoding.hpp"
#include "unicode/normal.hpp"
#include "unicode/props.hpp"
#include "util/bigint.hpp"
#include "util/cow.hpp"
#include "util/overflow.hpp"
#include "util/ranges.hpp"
#include "util/safe_int.hpp"
#include "util/unbounded_view.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <cassert>
#include <iterator>
#include <limits>
#include <optional>
#include <ranges>
#include <string>
#include <string_view>
#include <utility>
#include <variant>

bool nycleus::lexer::feed(char32_t ch, nycleus::source_location loc) {
  assert(!state_.valueless_by_exception());
  auto [ready, state] = std::visit([&](auto& s) {
    if constexpr(requires{ s.feed(ch, std::move(loc), *this); }) {
      return s.feed(ch, std::move(loc), *this);
    } else {
      return s.feed(ch, std::move(loc));
    }
  }, state_);
  if(state) {
    assert(!state->valueless_by_exception());
    state_ = std::move(*state);
  }
  return ready;
}

bool nycleus::lexer::feed_eof() {
  assert(!state_.valueless_by_exception());
  auto [ready, state] = std::visit([this](auto& s) {
    if constexpr(requires { s.feed_eof(*this); }) {
      return s.feed_eof(*this);
    } else {
      return s.feed_eof();
    }
  }, state_);
  if(state) {
    assert(!state->valueless_by_exception());
    state_ = std::move(*state);
  }
  return ready;
}

nycleus::token nycleus::lexer::get_token() {
  assert(!state_.valueless_by_exception());
  return std::visit([this](auto& s) {
    if constexpr(requires { s.get_token(*this); }) {
      return s.get_token(*this);
    } else {
      return s.get_token();
    }
  }, state_);
}

////////////////////////////////////////////////////////////////////////////////

bool nycleus::lexer::is_punct(char32_t ch) noexcept {
  return ch == U'!'
      || ch == U'%'
      || ch == U'&'
      || ch == U'('
      || ch == U')'
      || ch == U'*'
      || ch == U'+'
      || ch == U','
      || ch == U'-'
      || ch == U'.'
      || ch == U'/'
      || ch == U':'
      || ch == U';'
      || ch == U'<'
      || ch == U'='
      || ch == U'>'
      || ch == U'?'
      || ch == U'@'
      || ch == U'['
      || ch == U']'
      || ch == U'^'
      || ch == U'{'
      || ch == U'|'
      || ch == U'}'
      || ch == U'~';
}

bool nycleus::lexer::is_line_end(char32_t ch) noexcept {
  return ch == U'\n'
      || ch == U'\v'
      || ch == U'\f'
      || ch == U'\u0085'
      || ch == U'\u2028'
      || ch == U'\u2029';
}

////////////////////////////////////////////////////////////////////////////////

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_init::feed(char32_t ch,
    nycleus::source_location loc) {
  // Ignore whitespace.
  if(unicode::white_space(ch)) {
    return {false, std::nullopt};
  }

  // XID_Start characters and our special additions start words.
  if(ch == U'$' || ch == U'_' || unicode::xid_start(ch)) {
    state_in_word result{};
    result.loc = loc;

    switch(ch) {
      case U'$':
        result.sigil = tok::word::sigil_t::dollar;
        break;

      case U'_':
        result.sigil = tok::word::sigil_t::underscore;
        break;

      default:
        result.sigil = tok::word::sigil_t::none;
        result.buf.push_back(ch);
        break;
    }

    return {false, std::move(result)};
  }

  // Decimal digits start integer literals.
  if(util::uint8_t const value{unicode::decimal_value(ch)};
      value != unicode::no_decimal_value) {
    return {false, state_in_num{std::u32string{ch}, std::move(loc), value == 0
      ? state_in_num::substate_t::expects_x : state_in_num::substate_t::dec}};
  }

  // Hash signs start comments.
  if(ch == U'#') {
    return {false, state_in_comment_begin{std::move(loc)}};
  }

  // Double quotes start string literals.
  if(ch == U'"') {
    return {false, state_in_string{U"", loc, {}, {}, false}};
  }
  // Single quotes start string literals.
  if(ch == U'\'') {
    return {false, state_in_string{U"", loc, {}, {}, true}};
  }

  // Certain punctuation signs start punctuation tokens.
  if(is_punct(ch)) {
    return {false, get_punct_state(ch, std::move(loc))};
  }

  return {false, state_error{std::vector<diagnostic_box>{
    std::make_unique<diag::unexpected_code_point>(loc, ch)}, std::move(loc)}};
}

////////////////////////////////////////////////////////////////////////////////

namespace {

constexpr char32_t zwnj{U'\u200C'};
constexpr char32_t zwj{U'\u200D'};

} // (anonymous)

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_word::feed(char32_t ch,
    nycleus::source_location new_loc) {
  // XID_Continue characters, dollar signs, and ZW(N)J continue the word.
  if(ch == U'$' || ch == zwj || ch == zwnj || unicode::xid_continue(ch)) {
    assert((!loc.file && !new_loc.file)
      || (loc.file && new_loc.file && **loc.file == **new_loc.file));
    loc.last = new_loc.last;

    if(ch == zwj || ch == zwnj) {
      format_locs.emplace_back(buf.size(), new_loc);
    }

    buf.push_back(ch);
    return {false, std::nullopt};
  } else {
    // Any other character ends the word.
    return {true, std::nullopt};
  }
}

nycleus::token nycleus::lexer::state_in_word::get_token() {
  if(buf.empty()) {
    switch(sigil) {
      case tok::word::sigil_t::dollar:
        return token{tok::error{}, loc, std::optional{util::cow{
          std::vector<diagnostic_box>{
          std::make_unique<diag::empty_id>(loc)}}}};

      case tok::word::sigil_t::underscore:
        return token{tok::error{}, loc, std::optional{util::cow{
          std::vector<diagnostic_box>{
          std::make_unique<diag::empty_ext>(loc)}}}};

      case tok::word::sigil_t::none:
      default:
        util::unreachable();
    }
  }

  std::u8string value;
  std::vector<diagnostic_box> diags;

  if(format_locs.empty()) {
    std::ranges::copy(buf | unicode::nfc | unicode::utf8_encode(),
      std::back_inserter(value));
  } else {
    if(!unicode::is_nfc_qc(buf)) {
      // Normalize the buffer while remembering the locations of the ZW(N)Js
      // (note that they are NFC-stable, so we can just normalize the parts in
      // between)
      std::u32string normalized;
      normalized.reserve(buf.size());
      std::u32string::const_iterator cur_begin{buf.cbegin()};
      for(auto& [index, loc]: format_locs) {
        std::u32string::const_iterator const cur_end{buf.cbegin()
          + static_cast<std::ptrdiff_t>(index)};
        unicode::to_nfc(std::ranges::subrange{cur_begin, cur_end}, normalized);

        // `index` used to be the index of this code point in the old,
        // unnormalized, buffer. Change it to be the index of this code point in
        // the new, normalized, buffer.
        std::size_t const new_index{index};
        assert(buf[index] == zwj || buf[index] == zwnj);
        normalized += buf[index];
        index = new_index;

        cur_begin = cur_end + 1;
      }

      unicode::to_nfc(std::ranges::subrange{cur_begin, buf.cend()}, normalized);
      assert(normalized == unicode::to_nfc(buf));
      buf = std::move(normalized);
    }
    std::ranges::copy(buf | std::views::filter([](char32_t ch) noexcept {
      return ch != zwj && ch != zwnj;
    }) | unicode::nfc | unicode::utf8_encode(), std::back_inserter(value));

    // Now verify the context of all ZW(N)J occurences.

    for(auto [index, loc]: format_locs) {
      unicode::script_t context_script{unicode::script_t::unknown};

      // First, let's check for the prefix context from rules A2 and B. It's
      // going to be useful regardless of whether we're checking the context of
      // a ZWJ or a ZWNJ.

      // $L $M* $V $M₁* ZW(N)J ...
      // where:
      // $L = {gc=L}
      // $V = {ccc=virama}
      // $M = {gc=Mn}
      // $M₁ = {gc=Mn & ccc!=0}
      bool common_prefix_matched{false};

      // Whether, at the current position, we could be in the run of non-spacing
      // marks after the virama.
      bool after_virama{true};
      // Whether, at the current position, we could be in the run of non-spacing
      // marks before the virama.
      bool before_virama{false};

      std::u32string::const_iterator cur{buf.cbegin()
        + static_cast<std::ptrdiff_t>(index)};
      while(cur != buf.cbegin()) {
        --cur;

        unicode::script_t const script{unicode::script(*cur)};
        if(context_script == unicode::script_t::unknown) {
          if(script != unicode::script_t::unknown
              && script != unicode::script_t::common
              && script != unicode::script_t::inherited) {
            context_script = script;
          }
        } else if(script != context_script) {
          break;
        }

        unicode::gc_t const gc{unicode::gc(*cur)};
        unicode::ccc_t const ccc{unicode::ccc(*cur)};

        if(before_virama) {
          if(unicode::is_gc_group(gc, unicode::gc_group_t::l)) {
            common_prefix_matched = true;
            break;
          }
          before_virama = gc == unicode::gc_t::mn;
        }

        if(after_virama) {
          before_virama = before_virama || ccc == unicode::ccc_virama;
          after_virama = gc == unicode::gc_t::mn && ccc != 0;
        }

        if(!before_virama && !after_virama) {
          break;
        }
      }

      if(buf[index] == zwj) {
        // Rule B suffix context:
        // ... ZWJ (?!$D)
        // where:
        // $D = {Indic_Syllabic_Category=Vowel_Dependent}
        if(!common_prefix_matched || (index + 1 != buf.size()
            && unicode::indic_syllabic_category(buf[index + 1])
            == unicode::indic_syllabic_category_t::vowel_dependent)) {
          // Rule B violated.
          diags.emplace_back(std::make_unique<diag::unexpected_zwj>(loc));
        }
      } else {
        assert(buf[index] == zwnj);

        if(common_prefix_matched) {
          // Prefix part of Rule A2 is satisfied. Check the suffix part also:
          // ... ZWNJ $M₁* $L
          // where:
          // $L = {gc=L}
          // $M₁ = {gc=Mn & ccc!=0}

          auto next_letter{buf.cbegin()
            + static_cast<std::ptrdiff_t>(index + 1)};
          while(next_letter != buf.cend()) {
            if(unicode::gc(*next_letter) != unicode::gc_t::mn
                || unicode::ccc(*next_letter) == 0) {
              break;
            }

            unicode::script_t const script{unicode::script(*next_letter)};
            if(context_script == unicode::script_t::unknown) {
              if(script != unicode::script_t::unknown
                  && script != unicode::script_t::common
                  && script != unicode::script_t::inherited) {
                context_script = script;
              }
            } else if(script != context_script) {
              break;
            }

            ++next_letter;
          }

          if(next_letter != buf.cend()
              && unicode::is_gc_group(*next_letter, unicode::gc_group_t::l)
              && (context_script == unicode::script_t::unknown
              || unicode::script(*next_letter) == context_script)) {
            // Suffix part of Rule A2 satisfied.
            continue;
          }
        }

        // Rule A2 has not been satisfied. Check Rule A1 now.

        context_script = unicode::script_t::unknown;

        // Rule A1 prefix context:
        // $LJ $T* ZWNJ ...
        // where:
        // $LJ = {Joining_Type=Dual_Joining|Joining_Type=Left_Joining}
        // $T = {Joining_Type=Transparent}
        std::u32string::const_iterator prev_letter{buf.cbegin()
          + static_cast<std::ptrdiff_t>(index)};
        bool matched{false};
        while(prev_letter != buf.cbegin()) {
          --prev_letter;

          unicode::script_t const script{unicode::script(*prev_letter)};
          if(context_script == unicode::script_t::unknown) {
            if(script != unicode::script_t::unknown
                && script != unicode::script_t::common
                && script != unicode::script_t::inherited) {
              context_script = script;
            }
          } else if(script != context_script) {
            break;
          }

          auto const jt{unicode::joining_type(*prev_letter)};
          if(jt == unicode::joining_type_t::dual_joining
              || jt == unicode::joining_type_t::left_joining) {
            matched = true;
            break;
          }
          if(jt != unicode::joining_type_t::transparent) {
            break;
          }
        }
        if(!matched) {
          diags.emplace_back(std::make_unique<diag::unexpected_zwnj>(loc));
          continue;
        }

        // Rule A1 suffix context:
        // ... ZWNJ $T* $RJ
        // where:
        // $T = {Joining_Type=Transparent}
        // $RJ = {Joining_Type=Dual_Joining|Joining_Type=Right_Joining}
        std::u32string::const_iterator next_letter{buf.cbegin()
          + static_cast<std::ptrdiff_t>(index + 1)};
        matched = false;
        while(next_letter != buf.cend()) {
          unicode::script_t const script{unicode::script(*next_letter)};
          if(context_script == unicode::script_t::unknown) {
            if(script != unicode::script_t::unknown
                && script != unicode::script_t::common
                && script != unicode::script_t::inherited) {
              context_script = script;
            }
          } else if(script != context_script) {
            break;
          }

          auto const jt{unicode::joining_type(*next_letter)};
          if(jt == unicode::joining_type_t::dual_joining
              || jt == unicode::joining_type_t::right_joining) {
            matched = true;
            break;
          }
          if(jt != unicode::joining_type_t::transparent) {
            break;
          }

          ++next_letter;
        }
        if(!matched) {
          diags.emplace_back(std::make_unique<diag::unexpected_zwnj>(loc));
        }

        // Rule A1 satisfied.
      }
    }
  }

  value.shrink_to_fit();
  return token{tok::word{util::cow{std::move(value)}, sigil}, std::move(loc),
    diags.empty() ? std::nullopt : std::optional{util::cow{std::move(diags)}}};
}

////////////////////////////////////////////////////////////////////////////////

// TODO: Floating-point literals

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_num::feed(char32_t ch,
    nycleus::source_location new_loc) {
  if(
    ch != U'.'
    && ch != U'$'
    && !unicode::xid_continue(ch)
    && !(
      (ch == U'+' || ch == U'-')
      && (substate == substate_t::dec_exp || substate == substate_t::hex_exp)
    )
  ) {
    return {true, std::nullopt};
  }

  switch(substate) {
    case substate_t::expects_x:
      if(ch == U'X' || ch == U'x') {
        substate = substate_t::hex;
      } else if(ch != U'_') {
        substate = substate_t::dec;
      }
      break;

    case substate_t::dec:
    case substate_t::dec_exp:
      if(ch == U'E' || ch == U'e' || ch == U'P' || ch == U'p') {
        substate = substate_t::dec_exp;
      } else {
        substate = substate_t::dec;
      }
      break;

    case substate_t::hex:
    case substate_t::hex_exp:
      if(ch == U'P' || ch == U'p') {
        substate = substate_t::hex_exp;
      } else {
        substate = substate_t::hex;
      }
      break;
  }

  buf.push_back(ch);
  loc.last = new_loc.last;
  return {false, std::nullopt};
}

nycleus::token nycleus::lexer::state_in_num::get_token() {
  if(!unicode::is_nfc_qc(buf)) {
    buf = unicode::to_nfc(buf);
  }

  buf.erase(std::remove(buf.begin(), buf.end(), U'_'), buf.cend());

  std::u32string_view str{buf};

  std::vector<diagnostic_box> diags;
  auto make_error{[&](diagnostic_box d) {
    diags.emplace_back(std::move(d));
    return token{tok::error{}, loc, util::cow{std::move(diags)}};
  }};

  // Handle type suffixes

  if(
    str.size() >= 3
    && (
      str[str.size() - 3] == U'R'
      || str[str.size() - 3] == U'r'
      || !(
        unicode::decimal_value(str[0]) == 0
        && (str[1] == U'X' || str[1] == U'x')
      ) && (
        str[str.size() - 3] == U'F'
        || str[str.size() - 3] == U'f'
      )
    ) && (
      (
        unicode::decimal_value(str[str.size() - 2]) == 3
        && unicode::decimal_value(str[str.size() - 1]) == 2
      ) || (
        unicode::decimal_value(str[str.size() - 2]) == 6
        && unicode::decimal_value(str[str.size() - 1]) == 4
      )
    )
  ) {
    return make_error(std::make_unique<diag::fp_literals_unsupported>(loc));
  }

  std::optional<nycleus::int_type_suffix> type_suffix;
  if(auto last_non_digit{std::find_if(str.rbegin(), str.rend(),
      [](char32_t ch) noexcept { return unicode::decimal_value(ch)
      == unicode::no_decimal_value; })}; last_non_digit != str.rend()
      && (*last_non_digit == U'S' || *last_non_digit == U's'
      || *last_non_digit == U'U' || *last_non_digit == U'u')) {
    std::size_t const suffix_length{1
      + util::asserted_cast<std::size_t>(str.end() - last_non_digit.base())};
    if(last_non_digit == str.rbegin()) {
      diag::num_literal_suffix_missing_width::prefix_letter_t ch;
      switch(*last_non_digit) {
        using enum diag::num_literal_suffix_missing_width::prefix_letter_t;
        case u8's': ch = lower_s; break;
        case u8'u': ch = lower_u; break;
        case u8'S': ch = upper_s; break;
        case u8'U': ch = upper_u; break;
        default: util::unreachable();
      }
      diags.emplace_back(
        std::make_unique<diag::num_literal_suffix_missing_width>(loc, ch));
    } else {
      type_suffix.emplace();
      type_suffix->is_signed
        = *last_non_digit == U'S' || *last_non_digit == U's';

      type_suffix->width = 0;
      while(last_non_digit != str.rbegin()) {
        --last_non_digit;
        type_suffix->width = type_suffix->width * 10
          + unicode::decimal_value(*last_non_digit).unwrap();

        if(type_suffix->width > max_int_width) {
          diags.emplace_back(
            std::make_unique<diag::num_literal_suffix_too_wide>(loc));
          type_suffix->width = max_int_width;
          break;
        }
      }
      if(type_suffix && type_suffix->width == 0) {
        diags.emplace_back(
          std::make_unique<diag::num_literal_suffix_zero_width>(loc));
        type_suffix.reset();
      }
    }
    str.remove_suffix(suffix_length);
  }

  // Determine the base

  std::uint8_t base{10};

  {
    bool const has_hex_prefix{str.size() >= 2
      && unicode::decimal_value(str[0]) == 0
      && (str[1] == U'X' || str[1] == U'x')};
    bool const has_hex_suffix{!str.empty()
      && (str.back() == U'H' || str.back() == U'h')};

    if(has_hex_prefix) {
      if(has_hex_suffix) {
        return make_error(
          std::make_unique<diag::num_literal_base_prefix_and_suffix>(loc));
      } else {
        base = 16;
        str.remove_prefix(2);
      }
    } else if(has_hex_suffix) {
      base = 16;
      str.remove_suffix(1);
    }
  }

  if(base == 10) {
    bool const has_oct_prefix{str.size() >= 2
      && unicode::decimal_value(str[0]) == 0
      && (str[1] == U'O' || str[1] == U'o')};
    bool const has_oct_suffix{!str.empty()
      && (str.back() == U'O' || str.back() == U'o')};

    if(has_oct_prefix) {
      if(has_oct_suffix) {
        return make_error(
          std::make_unique<diag::num_literal_base_prefix_and_suffix>(loc));
      } else {
        base = 8;
        str.remove_prefix(2);
      }
    } else if(has_oct_suffix) {
      base = 8;
      str.remove_suffix(1);
    }
  }

  if(base == 10) {
    bool const has_bin_prefix{str.size() >= 2
      && unicode::decimal_value(str[0]) == 0
      && (str[1] == U'B' || str[1] == U'b')};
    bool const has_bin_suffix{!str.empty()
      && (str.back() == U'B' || str.back() == U'b')};

    if(has_bin_prefix) {
      if(has_bin_suffix) {
        return make_error(
          std::make_unique<diag::num_literal_base_prefix_and_suffix>(loc));
      } else {
        base = 2;
        str.remove_prefix(2);
      }
    } else if(has_bin_suffix) {
      base = 2;
      str.remove_suffix(1);
    }
  }

  // Extract the three parts (integral, fractional, exponential)

  auto is_digit{[&](char32_t ch) noexcept {
    if(base == 16 && (U'A' <= ch && ch <= U'F' || U'a' <= ch && ch <= U'f')) {
      return true;
    }

    util::uint8_t const value{unicode::decimal_value(ch)};
    if(value == unicode::no_decimal_value) {
      return false;
    }

    return value.unwrap() < base;
  }};

  std::u32string_view const integral_part{[&]() noexcept {
    std::size_t const integral_part_size{static_cast<std::size_t>(
      std::ranges::find_if(
        str,
        [&](char32_t ch) noexcept { return !is_digit(ch); }
      ) - str.cbegin())};
    std::u32string_view const integral_part{str.substr(0, integral_part_size)};
    str.remove_prefix(integral_part_size);
    return integral_part;
  }()};

  std::u32string_view const fractional_part{[&]() noexcept {
    if(str.empty() || str[0] != U'.') {
      return std::u32string_view{};
    }

    std::size_t const fractional_part_size{static_cast<std::size_t>(
      std::ranges::find_if(
        str | std::views::drop(1),
        [&](char32_t ch) noexcept { return !is_digit(ch); }
      ) - str.cbegin())};
    std::u32string_view const fractional_part{
      str.substr(0, fractional_part_size)};
    str.remove_prefix(fractional_part_size);
    return fractional_part;
  }()};

  std::u32string_view const exponential_part{[&]() noexcept {
    if(
      str.size() < 2
      || (base == 10 && str[0] != U'E' && str[0] != U'e')
      || (base != 10 && str[0] != U'P' && str[0] != U'p')
    ) {
      return std::u32string_view{};
    }

    std::size_t const exp_prefix_size{
      str[1] == U'+' || str[1] == U'-' ? 2U : 1U};

    std::size_t const exponential_part_size{static_cast<std::size_t>(
      std::ranges::find_if(
        str | std::views::drop(exp_prefix_size),
        [](char32_t ch) noexcept {
          return unicode::decimal_value(ch) == unicode::no_decimal_value;
        }
      ) - str.cbegin()
    )};
    if(exponential_part_size == exp_prefix_size) {
      return std::u32string_view{};
    }

    std::u32string_view const exponential_part{
      str.substr(0, exponential_part_size)};
    str.remove_prefix(exponential_part_size);
    return exponential_part;
  }()};

  // Determine semantics

  if(!str.empty() || integral_part.empty() && fractional_part.empty()) {
    return make_error(std::make_unique<diag::num_literal_invalid>(loc));
  }

  if(!fractional_part.empty() || !exponential_part.empty()) {
    return make_error(std::make_unique<diag::fp_literals_unsupported>(loc));
  }

  util::bigint num{util::bigint::from_string(integral_part
    | std::views::transform([](char32_t ch) {
      if(U'A' <= ch && ch <= U'F' || U'a' <= ch && ch <= U'f') {
        return ch;
      } else {
        return static_cast<char32_t>(U'0' + unicode::decimal_value(ch).unwrap());
      }
    }), base).first};

  return token{tok::int_literal{util::cow{std::move(num)}, type_suffix}, loc,
    util::cow{std::move(diags)}};
}

////////////////////////////////////////////////////////////////////////////////

void nycleus::lexer::comment_bidi_tracker::feed(
  char32_t ch,
  nycleus::source_location loc
) {
  auto is_isolate_code{[](open_code ch) noexcept {
    return ch == open_code::lri || ch == open_code::rli || ch == open_code::fsi;
  }};

  switch(ch) {
    case U'\u202A': // LEFT-TO-RIGHT EMBEDDING
    case U'\u202B': // RIGHT-TO-LEFT EMBEDDING
    case U'\u202D': // LEFT-TO-RIGHT OVERRIDE
    case U'\u202E': // RIGHT-TO-LEFT OVERRIDE
      if(open_codes_.empty() || !is_isolate_code(open_codes_.back().first)) {
        open_codes_.emplace_back(
          ch == U'\u202A' ? open_code::lre
          : ch == U'\u202B' ? open_code::rle
          : ch == U'\u202D' ? open_code::lro
          : open_code::rlo, std::move(loc));
      }
      break;

    case U'\u202C': // POP DIRECTIONAL FORMATTING
      if(open_codes_.empty()) {
        diags_.emplace_back(std::make_unique<diag::comment_unmatched_bidi_code>(
          loc, diag::comment_unmatched_bidi_code::bidi_code_t::pdf));
      } else if(!is_isolate_code(open_codes_.back().first)) {
        open_codes_.pop_back();
      }
      break;

    case U'\u2066': // LEFT-TO-RIGHT ISOLATE
    case U'\u2067': // RIGHT-TO-LEFT ISOLATE
    case U'\u2068': // FIRST STRONG ISOLATE
      open_codes_.emplace_back(
        ch == U'\u2066' ? open_code::lri
        : ch == U'\u2067' ? open_code::rli
        : open_code::fsi, std::move(loc));
      break;

    case U'\u2069': // POP DIRECTIONAL ISOLATE
      if(open_codes_.empty() || !is_isolate_code(open_codes_.back().first)) {
        diags_.emplace_back(std::make_unique<diag::comment_unmatched_bidi_code>(
          loc, diag::comment_unmatched_bidi_code::bidi_code_t::pdi));
      } else {
        open_codes_.pop_back();
      }
      break;

    case U'\u000A': // line feed
    case U'\u000B': // line tabulation
    case U'\u000C': // form feed
    case U'\u000D': // carriage return
    case U'\u0085': // next line
    case U'\u2028': // LINE SEPARATOR
    case U'\u2029': // PARAGRAPH SEPARATOR
      // If there's a carriage return followed by a line feed, the state will
      // be reset twice. That's fine: we're not interested here in counting
      // lines, just resetting the state whenever a line ends.
      end();
      break;
  }
}

void nycleus::lexer::comment_bidi_tracker::end() {
  if(open_codes_.empty()) {
    return;
  }
  if(diags_.max_size() - diags_.size() < open_codes_.size()) {
    throw std::bad_alloc{};
  }
  diags_.reserve(diags_.size() + open_codes_.size());
  for(auto& [ch, loc]: open_codes_ | std::views::reverse) {
    diag::comment_unmatched_bidi_code::bidi_code_t code;
    switch(ch) {
      using enum diag::comment_unmatched_bidi_code::bidi_code_t;
      case open_code::lre: code = lre; break;
      case open_code::rle: code = rle; break;
      case open_code::lro: code = lro; break;
      case open_code::rlo: code = rlo; break;
      case open_code::lri: code = lri; break;
      case open_code::rli: code = rli; break;
      case open_code::fsi: code = fsi; break;
    }
    diags_.emplace_back(std::make_unique<diag::comment_unmatched_bidi_code>(
      loc, code));
  }
  open_codes_.clear();
}

////////////////////////////////////////////////////////////////////////////////

// TODO: Warn if comments contain starting sequences of other comments

// TODO: Warn on exotic newlines in comments

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_comment_begin::feed(char32_t ch,
    nycleus::source_location new_loc, nycleus::lexer& l) {
  loc.last = new_loc.last;

  if(ch == U'*') {
    return {false, state_in_comment_ml{u8"#*", std::move(loc)}};
  }

  if(ch == U'+') {
    return {false, state_in_comment_mlnest{u8"#+", std::move(loc)}};
  }

  if(ch == U'\r') {
    return {false, state_in_comment_sl{u8"#\r", std::move(loc), {},
      state_in_comment_sl::substate_t::seen_cr}};
  }

  std::u8string text;
  comment_bidi_tracker bidi_tracker;
  if(l.keep_comments_) {
    text.push_back(u8'#');
    std::ranges::copy(std::views::single(ch) | unicode::utf8_encode(),
      std::back_inserter(text));
  }
  bidi_tracker.feed(ch, std::move(new_loc));
  return {false, state_in_comment_sl{std::move(text), std::move(loc),
    std::move(bidi_tracker),
    is_line_end(ch) ? state_in_comment_sl::substate_t::seen_end
    : state_in_comment_sl::substate_t::init}};
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_comment_begin::feed_eof(nycleus::lexer& l) {
  return {true, state_in_comment_sl{l.keep_comments_ ? u8"#" : u8"",
    std::move(loc), {}, state_in_comment_sl::substate_t::seen_end}};
}

////////////////////////////////////////////////////////////////////////////////

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_comment_sl::feed(char32_t ch,
    nycleus::source_location new_loc, nycleus::lexer& l) {
  if(
    substate == substate_t::seen_end
    || substate == substate_t::seen_cr && ch != U'\n'
  ) {
    return {true, std::nullopt};
  }

  if(l.keep_comments_) {
    std::ranges::copy(std::views::single(ch) | unicode::utf8_encode(),
      std::back_inserter(text));
  }
  loc.last = new_loc.last;
  bidi_tracker.feed(ch, std::move(new_loc));

  if(ch == U'\r') {
    substate = substate_t::seen_cr;
  } else if(is_line_end(ch)) {
    substate = substate_t::seen_end;
  }
  return {false, std::nullopt};
}

nycleus::token nycleus::lexer::state_in_comment_sl::get_token(
    nycleus::lexer& l) {
  std::u8string value;

  if(l.keep_comments_) {
    auto view{text | unicode::utf8_decode(unicode::eh_assume_valid{})};
    if(unicode::is_nfc_qc(view)) {
      value = std::move(text);
    } else {
      std::ranges::copy(view | unicode::nfc | unicode::utf8_encode(),
        std::back_inserter(value));
    }
  }

  bidi_tracker.end();
  auto diags = std::move(bidi_tracker).get_diags();
  return token{tok::comment{util::cow{std::move(value)},
    tok::comment::kind_t::single_line}, std::move(loc),
    diags.empty() ? std::nullopt
    : std::make_optional(util::cow{std::move(diags)})};
}

////////////////////////////////////////////////////////////////////////////////

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_comment_ml::feed(char32_t ch,
    nycleus::source_location new_loc, nycleus::lexer& l) {
  if(substate == substate_t::seen_end) {
    return {true, std::nullopt};
  }

  if(l.keep_comments_) {
    std::ranges::copy(std::views::single(ch) | unicode::utf8_encode(),
      std::back_inserter(text));
  }
  loc.last = new_loc.last;
  bidi_tracker.feed(ch, std::move(new_loc));

  if(ch == U'*') {
    substate = substate_t::seen_star;
  } else if(substate == substate_t::seen_star && ch == U'#') {
    substate = substate_t::seen_end;
  } else {
    substate = substate_t::init;
  }
  return {false, std::nullopt};
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_comment_ml::feed_eof() {
  if(substate == substate_t::seen_end) {
    return {true, std::nullopt};
  } else {
    bidi_tracker.end();
    auto diags{std::move(bidi_tracker).get_diags()};
    diags.emplace_back(std::make_unique<diag::unterminated_comment>(loc));
    return {true, state_error{std::move(diags), std::move(loc)}};
  }
}

nycleus::token nycleus::lexer::state_in_comment_ml::get_token(
    nycleus::lexer& l) {
  std::u8string value;

  if(l.keep_comments_) {
    auto view{text | unicode::utf8_decode(unicode::eh_assume_valid{})};
    if(unicode::is_nfc_qc(view)) {
      value = std::move(text);
    } else {
      std::ranges::copy(view | unicode::nfc | unicode::utf8_encode(),
        std::back_inserter(value));
    }
    value.shrink_to_fit();
  }

  bidi_tracker.end();
  auto diags = std::move(bidi_tracker).get_diags();
  return token{tok::comment{util::cow{std::move(value)},
    tok::comment::kind_t::multiline_non_nestable}, std::move(loc),
    diags.empty() ? std::nullopt
    : std::make_optional(util::cow{std::move(diags)})};
}

////////////////////////////////////////////////////////////////////////////////

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_comment_mlnest::feed(char32_t ch,
    nycleus::source_location new_loc, nycleus::lexer& l) {
  if(substate == substate_t::seen_end) {
    return {true, std::nullopt};
  }

  if(l.keep_comments_) {
    std::ranges::copy(std::views::single(ch) | unicode::utf8_encode(),
      std::back_inserter(text));
  }
  loc.last = new_loc.last;
  bidi_tracker.feed(ch, std::move(new_loc));

  switch(substate) {
    case substate_t::init:
      if(ch == U'+') {
        substate = substate_t::seen_plus;
      } else if(ch == U'#') {
        substate = substate_t::seen_hash;
      }
      break;

    case substate_t::seen_plus:
      if(ch == U'+') {
        substate = substate_t::seen_plus;
      } else if(ch == U'#') {
        if(nesting == 0) {
          substate = substate_t::seen_end;
        } else {
          --nesting;
          substate = substate_t::init;
        }
      } else {
        substate = substate_t::init;
      }
      break;

    case substate_t::seen_hash:
      if(ch == U'+') {
        assert(nesting < std::numeric_limits<std::uint64_t>::max());
        ++nesting;
        substate = substate_t::init;
      } else if(ch == U'#') {
        substate = substate_t::seen_hash;
      } else {
        substate = substate_t::init;
      }
      break;

    case substate_t::seen_end:
      util::unreachable();
  }

  return {false, std::nullopt};
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_comment_mlnest::feed_eof() {
  if(substate == substate_t::seen_end) {
    return {true, std::nullopt};
  } else {
    bidi_tracker.end();
    auto diags{std::move(bidi_tracker).get_diags()};
    diags.emplace_back(std::make_unique<diag::unterminated_comment>(loc));
    return {true, state_error{std::move(diags), std::move(loc)}};
  }
}

nycleus::token nycleus::lexer::state_in_comment_mlnest::get_token(
    nycleus::lexer& l) {
  std::u8string value;

  if(l.keep_comments_) {
    auto view{text | unicode::utf8_decode(unicode::eh_assume_valid{})};
    if(unicode::is_nfc_qc(view)) {
      value = std::move(text);
    } else {
      std::ranges::copy(view | unicode::nfc | unicode::utf8_encode(),
        std::back_inserter(value));
    }
    value.shrink_to_fit();
  }

  bidi_tracker.end();
  auto diags = std::move(bidi_tracker).get_diags();
  return token{tok::comment{util::cow{std::move(value)},
    tok::comment::kind_t::multiline_nestable}, std::move(loc),
    diags.empty() ? std::nullopt
    : std::make_optional(util::cow{std::move(diags)})};
}

////////////////////////////////////////////////////////////////////////////////

// TODO: Warn if a string or character literal is not normalized

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_string::feed(char32_t ch,
    nycleus::source_location new_loc) {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  loc.last = new_loc.last;

  {
    std::optional<diag::bidi_code_in_string::bidi_code_t> bad_code;
    switch(ch) {
      using enum diag::bidi_code_in_string::bidi_code_t;
      case U'\u202A':
        bad_code = is_char ? char_lre : str_lre;
        break;

      case U'\u202B':
        bad_code = is_char ? char_rle : str_rle;
        break;

      case U'\u202C':
        bad_code = is_char ? char_pdf : str_pdf;
        break;

      case U'\u202D':
        bad_code = is_char ? char_lro : str_lro;
        break;

      case U'\u202E':
        bad_code = is_char ? char_rlo : str_rlo;
        break;

      case U'\u2066':
        bad_code = is_char ? char_lri : str_lri;
        break;

      case U'\u2067':
        bad_code = is_char ? char_rli : str_rli;
        break;

      case U'\u2068':
        bad_code = is_char ? char_fsi : str_fsi;
        break;

      case U'\u2069':
        bad_code = is_char ? char_pdi : str_pdi;
        break;
    }

    if(bad_code) {
      diags.emplace_back(std::make_unique<diag::bidi_code_in_string>(
        new_loc, *bad_code));
    }
  }

  // TODO: Only warn once on a CRLF in strings
  {
    switch(ch) {
      using enum diag::string_newline::newline_code_t;

      case U'\n':
        diags.emplace_back(std::make_unique<diag::string_newline>(new_loc,
          is_char ? char_lf : str_lf));
        break;

      case U'\v':
        diags.emplace_back(std::make_unique<diag::string_newline>(new_loc,
          is_char ? char_vt : str_vt));
        break;

      case U'\f':
        diags.emplace_back(std::make_unique<diag::string_newline>(new_loc,
          is_char ? char_ff : str_ff));
        break;

      case U'\r':
        diags.emplace_back(std::make_unique<diag::string_newline>(new_loc,
          is_char ? char_cr : str_cr));
        break;

      case U'\u0085':
        diags.emplace_back(std::make_unique<diag::string_newline>(new_loc,
          is_char ? char_nel : str_nel));
        break;

      case U'\u2028':
        diags.emplace_back(std::make_unique<diag::string_newline>(new_loc,
          is_char ? char_line_sep : str_line_sep));
        break;

      case U'\u2029':
        diags.emplace_back(std::make_unique<diag::string_newline>(new_loc,
          is_char ? char_para_sep : str_para_sep));
        break;
    }
  }

  bool check_new_escape_code{true};
  if(escape_state == escape_state_pending) {
    check_new_escape_code = false;
    buf.push_back(ch);

    assert(!escape_codes.empty());
    auto& code_info{escape_codes.back()};
    code_info.code_loc.last = loc.last;
    code_info.code.push_back(ch);

    if(ch == U'u') {
      escape_state = 4;
    } else if(ch == U'U') {
      escape_state = 6;
    } else {
      escape_state = 0;
    }
  } else if(escape_state > 0) {
    if((U'A' <= ch && ch <= U'F') || (U'a' <= ch && ch <= U'f')
        || unicode::decimal_value(ch) != unicode::no_decimal_value) {
      check_new_escape_code = false;
      buf.push_back(ch);
      --escape_state;

      assert(!escape_codes.empty());
      auto& code_info{escape_codes.back()};
      code_info.code_loc.last = loc.last;
      code_info.code.push_back(ch);
    } else {
      escape_state = 0;
    }
  }

  if(check_new_escape_code) {
    if(ch == (is_char ? U'\'' : U'"')) {
      return {false, state_after_string{std::move(buf), loc,
        std::move(escape_codes), std::move(diags), is_char, true}};
    } else {
      buf.push_back(ch);
      if(ch == U'\\') {
        escape_state = escape_state_pending;
        escape_codes.push_back({new_loc, new_loc, U""});
      }
    }
  }

  return {false, std::nullopt};
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_in_string::feed_eof() noexcept {
  return {true, state_after_string{std::move(buf), loc,
    std::move(escape_codes), std::move(diags), is_char, false}};
}

////////////////////////////////////////////////////////////////////////////////

nycleus::token nycleus::lexer::state_after_string::get_token() {
  if(!is_terminated) {
    if(is_char) {
      diags.emplace_back(std::make_unique<diag::unterminated_char>(loc));
    } else {
      diags.emplace_back(std::make_unique<diag::unterminated_string>(loc));
    }
  }

  if(!unicode::is_nfc_qc(buf)) {
    buf = unicode::to_nfc(buf);
  }

  // Process escape codes. For each of them, we replace the code point after the
  // backslash with the code point that the escape code stands for. We also need
  // to remember the number of code points in the escape code after the
  // backslash; we reuse the buffer in the escape_code_info structure by setting
  // it to a one-code-point string where the only code point is the number in
  // question.
  {
    std::u32string::iterator next_escape_code{buf.begin()};

    auto reject_escape_code{[&](escape_code_info const& info) {
      diags.emplace_back(std::make_unique<diag::invalid_escape_code>(
        util::starts_with(std::ranges::subrange{next_escape_code, buf.cend()},
        info.code) ? info.code_loc : info.slash_loc));
    }};

    for(auto& info: escape_codes) {
      next_escape_code = std::ranges::find(
        util::unbounded_view{next_escape_code}, U'\\');
      ++next_escape_code;

      if(next_escape_code == buf.end()) {
        // This situation happens when there is an unterminated literal, and the
        // last code point of the literal -- and of the file itself -- is a
        // backslash.
        assert(&info == &escape_codes.back());

        reject_escape_code(info);

        // Since this degenerate escape code consists only of one code point --
        // the backslash -- the second loop below won't know how to process it.
        // Fortunately, it doesn't need to: we can just replace the backslash
        // with U+FFFD right now, and pretend that this was never an escape
        // code.
        *(next_escape_code - 1) = U'\uFFFD';
        escape_codes.pop_back();

        break;
      }

      char32_t const escape_code_kind{*next_escape_code};
      char32_t value;
      switch(escape_code_kind) {
        case U'"':  value = U'"';  break;
        case U'\'': value = U'\''; break;
        case U'\\': value = U'\\'; break;
        case U'a':  value = U'\a'; break;
        case U'b':  value = U'\b'; break;
        case U'e':  value = U'\u001B'; break;
        case U'f':  value = U'\f'; break;
        case U'n':  value = U'\n'; break;
        case U'r':  value = U'\r'; break;
        case U't':  value = U'\t'; break;
        case U'v':  value = U'\v'; break;

        case U'u':
        case U'U': {
          auto digit_it = next_escape_code + 1;
          std::uint8_t num_digits{static_cast<std::uint8_t>(
            escape_code_kind == U'u' ? 4 : 6)};
          std::uint32_t numeric_value{0};

          while(digit_it != buf.end()) {
            std::uint32_t digit_value;
            if(U'A' <= *digit_it && *digit_it <= U'F') {
              digit_value = 10 + *digit_it - U'A';
            } else if(U'a' <= *digit_it && *digit_it <= U'f') {
              digit_value = 10 + *digit_it - U'a';
            } else {
              digit_value = unicode::decimal_value(*digit_it).unwrap();
              if(digit_value == unicode::no_decimal_value.unwrap()) {
                break;
              }
            }

            numeric_value = (numeric_value << 4) | digit_value;
            ++digit_it;
            if(--num_digits == 0) {
              break;
            }
          }

          value = static_cast<char32_t>(numeric_value);
          if(num_digits != 0 || !unicode::is_scalar_value(value)) {
            reject_escape_code(info);
            value = U'\uFFFD';
          }
          *next_escape_code = value;

          info.code.clear();
          info.code.push_back(
            static_cast<char32_t>(digit_it - next_escape_code));
          next_escape_code = digit_it;

          continue;
        }

        default:
          reject_escape_code(info);
          value = U'\uFFFD';
          break;
      }

      *next_escape_code++ = value;
      info.code.clear();
      info.code.push_back(1);
    }
  }

  // Remove the extra code points in the escape codes. Every time we find a
  // backslash, which indicates the beginning of an escape code, we replace it
  // with the next code point, which is where we've previously stored the escape
  // code's value. Then we remove the rest of the code points of the escape
  // code.
  //
  // For example, if the original string contains the escape code `\u0041`,
  // which corresponds to the code point `A`, the loop above transformed it into
  // `\A0041`, and the loop below will transform it to just `A`.

  if(!escape_codes.empty()) {
    std::u32string::iterator dst;
    std::u32string::const_iterator src{buf.cbegin()};
    bool first{true};

    for(auto const& info: escape_codes) {
      auto const next_escape_code{std::ranges::find(
        util::unbounded_view{src}, U'\\')};
      if(first) {
        dst = buf.begin() + (next_escape_code - buf.cbegin());
        first = false;
      } else {
        dst = std::copy(src, next_escape_code, dst);
      }
      src = next_escape_code;

      assert(info.code.size() == 1);
      std::uint32_t const escape_code_size{
        static_cast<std::uint32_t>(info.code.front())};
      assert(1 <= escape_code_size && escape_code_size <= 7);

      ++src;
      *dst++ = *src;

      assert(buf.cend() - src >= escape_code_size);
      src += escape_code_size;
    }

    dst = std::copy(src, buf.cend(), dst);
    buf.erase(dst, buf.cend());
  }

  // Now generate the actual token.
  diags.shrink_to_fit();
  std::u8string value;
  std::ranges::copy(buf | unicode::utf8_encode(), std::back_inserter(value));
  value.shrink_to_fit();
  if(is_char) {
    return token{tok::char_literal{util::cow{std::move(value)}}, loc,
      diags.empty() ? std::nullopt
      : std::optional{util::cow{std::move(diags)}}};
  } else {
    return token{tok::string_literal{util::cow{std::move(value)}}, loc,
      diags.empty() ? std::nullopt
      : std::optional{util::cow{std::move(diags)}}
    };
  }
}

////////////////////////////////////////////////////////////////////////////////

nycleus::lexer::state_t nycleus::lexer::get_punct_state(char32_t ch,
    nycleus::source_location loc) noexcept {
  switch(ch) {
    case U'!': return make_punct_state<state_seen_excl>(std::move(loc));
    case U'%': return make_punct_state<state_seen_percent>(std::move(loc));
    case U'&': return make_punct_state<state_seen_amp>(std::move(loc));
    case U'(': return make_punct_state<state_seen_lparen>(std::move(loc));
    case U')': return make_punct_state<state_seen_rparen>(std::move(loc));
    case U'*': return make_punct_state<state_seen_star>(std::move(loc));
    case U'+': return make_punct_state<state_seen_plus>(std::move(loc));
    case U',': return make_punct_state<state_seen_comma>(std::move(loc));
    case U'-': return make_punct_state<state_seen_minus>(std::move(loc));
    case U'.': return make_punct_state<state_seen_point>(std::move(loc));
    case U'/': return make_punct_state<state_seen_slash>(std::move(loc));
    case U':': return make_punct_state<state_seen_colon>(std::move(loc));
    case U';': return make_punct_state<state_seen_semicolon>(std::move(loc));
    case U'<': return make_punct_state<state_seen_lt>(std::move(loc));
    case U'=': return make_punct_state<state_seen_assign>(std::move(loc));
    case U'>': return make_punct_state<state_seen_gt>(std::move(loc));
    case U'?': return make_punct_state<state_seen_question>(std::move(loc));
    case U'@': return make_punct_state<state_seen_at>(std::move(loc));
    case U'[': return make_punct_state<state_seen_lsquare>(std::move(loc));
    case U']': return make_punct_state<state_seen_rsquare>(std::move(loc));
    case U'^': return make_punct_state<state_seen_bitwise_xor>(std::move(loc));
    case U'{': return make_punct_state<state_seen_lbrace>(std::move(loc));
    case U'|': return make_punct_state<state_seen_pipe>(std::move(loc));
    case U'}': return make_punct_state<state_seen_rbrace>(std::move(loc));
    case U'~': return make_punct_state<state_seen_tilde>(std::move(loc));
    default:   return state_init{};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_excl::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'=') {
    return {false, make_punct_state<state_seen_neq>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_percent::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'=') {
    return {false, make_punct_state<state_seen_percent_assign>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_amp::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  switch(ch) {
    case U'&':
      return {false, state_seen_logical_and{
        state_punct_base<tok::logical_and>{{loc.file, loc.first, new_loc.last}},
        loc, new_loc}};
    case U'=':
      return {false, make_punct_state<state_seen_amp_assign>(
        {loc.file, loc.first, new_loc.last})};
    default:
      return {true, std::nullopt};
  }
}

nycleus::token nycleus::lexer::state_seen_logical_and::get_token() noexcept {
  return {tok::logical_and{std::move(first_amp_loc), std::move(second_amp_loc)},
    std::move(loc)};
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_star::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'=') {
    return {false, make_punct_state<state_seen_star_assign>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_plus::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  switch(ch) {
    case U'+':
      return {false, make_punct_state<state_seen_incr>(
        {loc.file, loc.first, new_loc.last})};
    case U'=':
      return {false, make_punct_state<state_seen_plus_assign>(
        {loc.file, loc.first, new_loc.last})};
    default:
      return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_minus::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  switch(ch) {
    case U'-':
      return {false, make_punct_state<state_seen_decr>(
        {loc.file, loc.first, new_loc.last})};
    case U'=':
      return {false, make_punct_state<state_seen_minus_assign>(
        {loc.file, loc.first, new_loc.last})};
    case U'>':
      return {false, make_punct_state<state_seen_arrow>(
        {loc.file, loc.first, new_loc.last})};
    default:
      return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_point::feed(char32_t ch,
    nycleus::source_location new_loc) {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'.') {
    return {false, make_punct_state<state_seen_double_point>(
      {loc.file, loc.first, new_loc.last})};
  } else if(unicode::decimal_value(ch) != unicode::no_decimal_value) {
    std::u32string buf{U"."};
    buf.push_back(ch);
    return {false, state_in_num{std::move(buf),
      {loc.file, loc.first, new_loc.last}, state_in_num::substate_t::dec}};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_double_point::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'.') {
    return {false, make_punct_state<state_seen_ellipsis>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_slash::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'=') {
    return {false, make_punct_state<state_seen_slash_assign>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_lt::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  switch(ch) {
    case U'/':
      return {false, make_punct_state<state_seen_lrot>(
        {loc.file, loc.first, new_loc.last})};
    case U'<':
      return {false, make_punct_state<state_seen_lshift>(
        {loc.file, loc.first, new_loc.last})};
    case U'=':
      return {false, make_punct_state<state_seen_leq>(
        {loc.file, loc.first, new_loc.last})};
    default:
      return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_lshift::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'=') {
    return {false, make_punct_state<state_seen_lshift_assign>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_lrot::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'=') {
    return {false, make_punct_state<state_seen_lrot_assign>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_leq::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'>') {
    return {false, make_punct_state<state_seen_three_way_cmp>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_assign::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  switch(ch) {
    case U'=':
      return {false, make_punct_state<state_seen_eq>(
        {loc.file, loc.first, new_loc.last})};
    case U'>':
      return {false, make_punct_state<state_seen_fat_arrow>(
        {loc.file, loc.first, new_loc.last})};
    default:
      return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_gt::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  switch(ch) {
    case U'/':
      return {false, make_punct_state<state_seen_rrot>(
        {loc.file, loc.first, new_loc.last})};
    case U'=':
      return {false, make_punct_state<state_seen_geq>(
        {loc.file, loc.first, new_loc.last})};
    case U'>':
      return {false, make_punct_state<state_seen_rshift>(
        {loc.file, loc.first, new_loc.last})};
    default:
      return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_rrot::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'=') {
    return {false, make_punct_state<state_seen_rrot_assign>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_rshift::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  if(ch == U'=') {
    return {false, make_punct_state<state_seen_rshift_assign>(
      {loc.file, loc.first, new_loc.last})};
  } else {
    return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_bitwise_xor::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  switch(ch) {
    case U'=':
      return {false, make_punct_state<state_seen_bitwise_xor_assign>(
        {loc.file, loc.first, new_loc.last})};
    case U'^':
      return {false, make_punct_state<state_seen_logical_xor>(
        {loc.file, loc.first, new_loc.last})};
    default:
      return {true, std::nullopt};
  }
}

std::pair<bool, std::optional<nycleus::lexer::state_t>>
    nycleus::lexer::state_seen_pipe::feed(char32_t ch,
    nycleus::source_location new_loc) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  switch(ch) {
    case U'=':
      return {false, make_punct_state<state_seen_pipe_assign>(
        {loc.file, loc.first, new_loc.last})};
    case U'>':
      return {false, make_punct_state<state_seen_subst>(
        {loc.file, loc.first, new_loc.last})};
    case U'|':
      return {false, make_punct_state<state_seen_logical_or>(
        {loc.file, loc.first, new_loc.last})};
    default:
      return {true, std::nullopt};
  }
}

////////////////////////////////////////////////////////////////////////////////

nycleus::token nycleus::lexer::state_error::get_token() {
  return token{tok::error{}, std::move(range), util::cow{std::move(diags)}};
}
