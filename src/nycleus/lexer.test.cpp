#include <boost/test/unit_test.hpp>

#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_tools.hpp"
#include "nycleus/lexer.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/token.hpp"
#include "nycleus/token_tools.hpp"
#include "unicode/encoding.hpp"
#include "util/bigint.hpp"
#include "util/cow.hpp"
#include "util/ranges.hpp"
#include "test_tools.hpp"
#include <algorithm>
#include <array>
#include <filesystem>
#include <iterator>
#include <optional>
#include <ranges>
#include <span>
#include <string>
#include <string_view>
#include <unordered_set>
#include <utility>
#include <variant>

using namespace std::string_literals;
using namespace util::bigint_literals;
namespace diag = nycleus::diag;
namespace nt = nycleus::tok;

namespace {

using t = nycleus::token;
using src_loc = nycleus::source_location;

static_assert(std::ranges::view<nycleus::lexer_view<
  unicode::decoded_view<
    std::views::all_t<std::u8string const&>,
    nycleus::utf8_counted<>,
    unicode::eh_throw<>
  >
>>);
static_assert(std::ranges::forward_range<nycleus::lexer_view<
  unicode::decoded_view<
    std::views::all_t<std::u8string const&>,
    nycleus::utf8_counted<>,
    unicode::eh_throw<>
  >
>>);
static_assert(std::same_as<std::ranges::range_value_t<nycleus::lexer_view<
  unicode::decoded_view<
    std::views::all_t<std::u8string const&>,
    nycleus::utf8_counted<>,
    unicode::eh_throw<>
  >
>>, t>);

struct lexer_fixture {
  std::optional<util::cow<std::filesystem::path>> f{
    util::cow{std::filesystem::path{u8"test.ny"}}};
};

void do_lexer_test(
  std::u8string_view str,
  std::optional<util::cow<std::filesystem::path>> f,
  std::span<nycleus::token const> test_tokens
) {
  std::vector<nycleus::token> tokens;
  std::ranges::copy(str
    | unicode::decode(nycleus::utf8_counted{std::move(f)}) | nycleus::lex(),
    std::back_inserter(tokens));
  BOOST_TEST(tokens == test_tokens, boost::test_tools::per_element());

  BOOST_TEST(std::ranges::all_of(tokens, [](t const& tt) noexcept {
    return !tt.diagnostics.has_value() || (*tt.diagnostics)->empty();
  }));
}

std::vector<nycleus::token> do_lexer_test_diags(
  std::u8string_view str,
  std::optional<util::cow<std::filesystem::path>> f,
  std::span<std::unique_ptr<nycleus::diagnostic> const> test_diags
) {
  std::vector<nycleus::token> tokens;
  std::ranges::copy(str
    | unicode::decode(nycleus::utf8_counted{std::move(f)}) | nycleus::lex(),
    std::back_inserter(tokens));

  auto const diags_set{tokens
    | std::views::filter([](t const& tt) noexcept {
      return tt.diagnostics.has_value();
    })
    | std::views::transform([](t const& tt) noexcept
        -> std::vector<nycleus::diagnostic_box> const& {
      return **tt.diagnostics;
    })
    | std::views::join
    | std::views::transform(&nycleus::diagnostic_box::operator*)
    | util::range_to<std::unordered_set<
      std::reference_wrapper<nycleus::diagnostic const>>>()};

  auto const test_diags_set{test_diags
    | std::views::transform(&std::unique_ptr<nycleus::diagnostic>::operator*)
    | util::range_to<std::unordered_set<
      std::reference_wrapper<nycleus::diagnostic const>>>()};

  BOOST_TEST(diags_set == test_diags_set);

  return tokens;
}

void do_lexer_test_diags(
  std::u8string_view str,
  std::optional<util::cow<std::filesystem::path>> f,
  std::span<std::unique_ptr<nycleus::diagnostic> const> test_diags,
  std::span<nycleus::token const> test_tokens
) {
  auto const tokens{do_lexer_test_diags(str, std::move(f), test_diags)};
  BOOST_TEST(tokens == test_tokens, boost::test_tools::per_element());
}

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_nycleus)
BOOST_FIXTURE_TEST_SUITE(lexer, lexer_fixture)

// Punctuation /////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(punctuation)

BOOST_AUTO_TEST_CASE(excl) {
  do_lexer_test(u8"!", f, std::array{
    t{nt::excl{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(neq) {
  do_lexer_test(u8"!=", f, std::array{
    t{nt::neq{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(percent) {
  do_lexer_test(u8"%", f, std::array{
    t{nt::percent{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(percent_assign) {
  do_lexer_test(u8"%=", f, std::array{
    t{nt::percent_assign{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(amp) {
  do_lexer_test(u8"&", f, std::array{
    t{nt::amp{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(logical_and) {
  do_lexer_test(u8"&&", f, std::array{
    t{nt::logical_and{{f, {1, 1}, {1, 3}}, {f, {1, 2}, {1, 3}}},
      {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(amp_assign) {
  do_lexer_test(u8"&=", f, std::array{
    t{nt::amp_assign{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(lparen) {
  do_lexer_test(u8"(", f, std::array{
    t{nt::lparen{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(rparen) {
  do_lexer_test(u8")", f, std::array{
    t{nt::rparen{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(star) {
  do_lexer_test(u8"*", f, std::array{
    t{nt::star{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(star_assign) {
  do_lexer_test(u8"*=", f, std::array{
    t{nt::star_assign{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(plus) {
  do_lexer_test(u8"+", f, std::array{
    t{nt::plus{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(incr) {
  do_lexer_test(u8"++", f, std::array{
    t{nt::incr{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(plus_assign) {
  do_lexer_test(u8"+=", f, std::array{
    t{nt::plus_assign{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(comma) {
  do_lexer_test(u8",", f, std::array{
    t{nt::comma{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(minus) {
  do_lexer_test(u8"-", f, std::array{
    t{nt::minus{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(decr) {
  do_lexer_test(u8"--", f, std::array{
    t{nt::decr{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(minus_assign) {
  do_lexer_test(u8"-=", f, std::array{
    t{nt::minus_assign{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(arrow) {
  do_lexer_test(u8"->", f, std::array{
    t{nt::arrow{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(point) {
  do_lexer_test(u8".", f, std::array{
    t{nt::point{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(double_point) {
  do_lexer_test(u8"..", f, std::array{
    t{nt::double_point{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(ellipsis) {
  do_lexer_test(u8"...", f, std::array{
    t{nt::ellipsis{}, {f, {1, 1}, {1, 4}}}
  });
}

BOOST_AUTO_TEST_CASE(slash) {
  do_lexer_test(u8"/", f, std::array{
    t{nt::slash{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(slash_assign) {
  do_lexer_test(u8"/=", f, std::array{
    t{nt::slash_assign{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(colon) {
  do_lexer_test(u8":", f, std::array{
    t{nt::colon{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(semicolon) {
  do_lexer_test(u8";", f, std::array{
    t{nt::semicolon{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(lt) {
  do_lexer_test(u8"<", f, std::array{
    t{nt::lt{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(lrot) {
  do_lexer_test(u8"</", f, std::array{
    t{nt::lrot{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(lrot_assign) {
  do_lexer_test(u8"</=", f, std::array{
    t{nt::lrot_assign{}, {f, {1, 1}, {1, 4}}}
  });
}

BOOST_AUTO_TEST_CASE(lshift) {
  do_lexer_test(u8"<<", f, std::array{
    t{nt::lshift{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(lshift_assign) {
  do_lexer_test(u8"<<=", f, std::array{
    t{nt::lshift_assign{}, {f, {1, 1}, {1, 4}}}
  });
}

BOOST_AUTO_TEST_CASE(leq) {
  do_lexer_test(u8"<=", f, std::array{
    t{nt::leq{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(three_way_cmp) {
  do_lexer_test(u8"<=>", f, std::array{
    t{nt::three_way_cmp{}, {f, {1, 1}, {1, 4}}}
  });
}

BOOST_AUTO_TEST_CASE(assign) {
  do_lexer_test(u8"=", f, std::array{
    t{nt::assign{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(eq) {
  do_lexer_test(u8"==", f, std::array{
    t{nt::eq{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(fat_arrow) {
  do_lexer_test(u8"=>", f, std::array{
    t{nt::fat_arrow{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(gt) {
  do_lexer_test(u8">", f, std::array{
    t{nt::gt{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(rrot) {
  do_lexer_test(u8">/", f, std::array{
    t{nt::rrot{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(rrot_assign) {
  do_lexer_test(u8">/=", f, std::array{
    t{nt::rrot_assign{}, {f, {1, 1}, {1, 4}}}
  });
}

BOOST_AUTO_TEST_CASE(geq) {
  do_lexer_test(u8">=", f, std::array{
    t{nt::geq{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(rshift) {
  do_lexer_test(u8">>", f, std::array{
    t{nt::rshift{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(rshift_assign) {
  do_lexer_test(u8">>=", f, std::array{
    t{nt::rshift_assign{}, {f, {1, 1}, {1, 4}}}
  });
}

BOOST_AUTO_TEST_CASE(question) {
  do_lexer_test(u8"?", f, std::array{
    t{nt::question{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(at) {
  do_lexer_test(u8"@", f, std::array{
    t{nt::at{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(lsquare) {
  do_lexer_test(u8"[", f, std::array{
    t{nt::lsquare{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(rsquare) {
  do_lexer_test(u8"]", f, std::array{
    t{nt::rsquare{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(bitwise_xor) {
  do_lexer_test(u8"^", f, std::array{
    t{nt::bitwise_xor{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(bitwise_xor_assign) {
  do_lexer_test(u8"^=", f, std::array{
    t{nt::bitwise_xor_assign{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(logical_xor) {
  do_lexer_test(u8"^^", f, std::array{
    t{nt::logical_xor{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(lbrace) {
  do_lexer_test(u8"{", f, std::array{
    t{nt::lbrace{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(pipe) {
  do_lexer_test(u8"|", f, std::array{
    t{nt::pipe{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(pipe_assign) {
  do_lexer_test(u8"|=", f, std::array{
    t{nt::pipe_assign{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(subst) {
  do_lexer_test(u8"|>", f, std::array{
    t{nt::subst{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(logical_or) {
  do_lexer_test(u8"||", f, std::array{
    t{nt::logical_or{}, {f, {1, 1}, {1, 3}}}
  });
}

BOOST_AUTO_TEST_CASE(rbrace) {
  do_lexer_test(u8"}", f, std::array{
    t{nt::rbrace{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(tilde) {
  do_lexer_test(u8"~", f, std::array{
    t{nt::tilde{}, {f, {1, 1}, {1, 2}}}
  });
}

BOOST_AUTO_TEST_CASE(maximal_munch) {
  do_lexer_test(u8".....>>>==", f, std::array{
    t{nt::ellipsis{}, {f, {1, 1}, {1, 4}}},
    t{nt::double_point{}, {f, {1, 4}, {1, 6}}},
    t{nt::rshift{}, {f, {1, 6}, {1, 8}}},
    t{nt::geq{}, {f, {1, 8}, {1, 10}}},
    t{nt::assign{}, {f, {1, 10}, {1, 11}}}
  });
}

BOOST_AUTO_TEST_SUITE_END()

// Words ///////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(words)

BOOST_AUTO_TEST_CASE(unprefixed) {
  do_lexer_test(u8"foo", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 4}}}
  });

  // "строка"
  do_lexer_test(u8"\u0441\u0442\u0440\u043E\u043A\u0430", f, std::array{
    t{nt::word{util::cow{u8"\u0441\u0442\u0440\u043E\u043A\u0430"s},
      nt::word::sigil_t::none}, {f, {1, 1}, {1, 13}}}
  });

  // "細繩"
  do_lexer_test(u8"\u7D30\u7E69", f, std::array{
    t{nt::word{util::cow{u8"\u7D30\u7E69"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 7}}}
  });

  // "𝕗𝕠𝕠"
  do_lexer_test(u8"\U0001D557\U0001D560\U0001D560", f, std::array{
    t{nt::word{util::cow{u8"\U0001D557\U0001D560\U0001D560"s},
      nt::word::sigil_t::none}, {f, {1, 1}, {1, 13}}}
  });
}

BOOST_AUTO_TEST_CASE(id) {
  do_lexer_test(u8"$foo", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::dollar},
      {f, {1, 1}, {1, 5}}}
  });

  // "$строка"
  do_lexer_test(u8"$\u0441\u0442\u0440\u043E\u043A\u0430", f, std::array{
    t{nt::word{util::cow{u8"\u0441\u0442\u0440\u043E\u043A\u0430"s},
      nt::word::sigil_t::dollar}, {f, {1, 1}, {1, 14}}}
  });

  // "$細繩"
  do_lexer_test(u8"$\u7D30\u7E69", f, std::array{
    t{nt::word{util::cow{u8"\u7D30\u7E69"s}, nt::word::sigil_t::dollar},
      {f, {1, 1}, {1, 8}}}
  });

  // "$𝕗𝕠𝕠"
  do_lexer_test(u8"$\U0001D557\U0001D560\U0001D560", f, std::array{
    t{nt::word{util::cow{u8"\U0001D557\U0001D560\U0001D560"s},
      nt::word::sigil_t::dollar}, {f, {1, 1}, {1, 14}}}
  });
}

BOOST_AUTO_TEST_CASE(ext) {
  do_lexer_test(u8"_foo", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::underscore},
      {f, {1, 1}, {1, 5}}}
  });

  // "_строка"
  do_lexer_test(u8"_\u0441\u0442\u0440\u043E\u043A\u0430", f, std::array{
    t{nt::word{util::cow{u8"\u0441\u0442\u0440\u043E\u043A\u0430"s},
      nt::word::sigil_t::underscore}, {f, {1, 1}, {1, 14}}}
  });

  // "_細繩"
  do_lexer_test(u8"_\u7D30\u7E69", f, std::array{
    t{nt::word{util::cow{u8"\u7D30\u7E69"s}, nt::word::sigil_t::underscore},
      {f, {1, 1}, {1, 8}}}
  });

  // "_𝕗𝕠𝕠"
  do_lexer_test(u8"_\U0001D557\U0001D560\U0001D560", f, std::array{
    t{nt::word{util::cow{u8"\U0001D557\U0001D560\U0001D560"s},
      nt::word::sigil_t::underscore}, {f, {1, 1}, {1, 14}}}
  });
}

BOOST_AUTO_TEST_CASE(internal_sigils) {
  do_lexer_test(u8"foo_bar$qux", f, std::array{
    t{nt::word{util::cow{u8"foo_bar$qux"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 12}}}
  });

  do_lexer_test(u8"$foo_bar$qux", f, std::array{
    t{nt::word{util::cow{u8"foo_bar$qux"s}, nt::word::sigil_t::dollar},
      {f, {1, 1}, {1, 13}}}
  });

  do_lexer_test(u8"_foo_bar$qux", f, std::array{
    t{nt::word{util::cow{u8"foo_bar$qux"s}, nt::word::sigil_t::underscore},
      {f, {1, 1}, {1, 13}}}
  });
}

BOOST_AUTO_TEST_CASE(empty_id) {
  do_lexer_test_diags(u8"$", f, test::nycleus::diags(
    std::make_unique<diag::empty_id>(src_loc{f, {1, 1}, {1, 2}})
  ));
}

BOOST_AUTO_TEST_CASE(empty_ext) {
  do_lexer_test_diags(u8"_", f, test::nycleus::diags(
    std::make_unique<diag::empty_ext>(src_loc{f, {1, 1}, {1, 2}})
  ));
}

BOOST_AUTO_TEST_CASE(zwnj) {
  do_lexer_test(u8"\u0646\u0627\u0645\u0647\u200C\u0627\u06CC", f, std::array{
    t{nt::word{util::cow{u8"\u0646\u0627\u0645\u0647\u0627\u06CC"s},
      nt::word::sigil_t::none}, {f, {1, 1}, {1, 16}}}
  });

  do_lexer_test(
    u8"\u0D26\u0D43\u0D15\u0D4D\u200C\u0D38\u0D3E\u0D15\u0D4D\u0D37\u0D3F",
    f,
    std::array{
      t{nt::word{util::cow{
        u8"\u0D26\u0D43\u0D15\u0D4D\u0D38\u0D3E\u0D15\u0D4D\u0D37\u0D3F"s},
        nt::word::sigil_t::none}, {f, {1, 1}, {1, 34}}}
    }
  );
}

BOOST_AUTO_TEST_CASE(zwj) {
  do_lexer_test(
    u8"\u0DC1\u0DCA\u200D\u0DBB\u0DD3\u0DBD\u0D82\u0D9A\u0DCF",
    f,
    std::array{
      t{nt::word{util::cow{
        u8"\u0DC1\u0DCA\u0DBB\u0DD3\u0DBD\u0D82\u0D9A\u0DCF"s},
        nt::word::sigil_t::none}, {f, {1, 1}, {1, 28}}}
    }
  );

  do_lexer_test(u8"\u0DC1\u0DCA\u200D", f, std::array{
    t{nt::word{util::cow{u8"\u0DC1\u0DCA"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(zwnj_fail) {
  do_lexer_test_diags(u8"\u0646\u0627\u0645\u0647\u200C", f,
    test::nycleus::diags(
      std::make_unique<diag::unexpected_zwnj>(src_loc{f, {1, 9}, {1, 12}})
    ), std::array{
      t{nt::word{util::cow{u8"\u0646\u0627\u0645\u0647"s},
        nt::word::sigil_t::none}, {f, {1, 1}, {1, 12}}}
    });

  do_lexer_test_diags(u8"\u0D26\u0D43\u0D15\u0D4D\u200C", f,
    test::nycleus::diags(
      std::make_unique<diag::unexpected_zwnj>(src_loc{f, {1, 13}, {1, 16}})
    ), std::array{
      t{nt::word{util::cow{u8"\u0D26\u0D43\u0D15\u0D4D"s},
        nt::word::sigil_t::none}, {f, {1, 1}, {1, 16}}}
    });

  do_lexer_test_diags(u8"\u0646\u0627\u0645\u0647\u200C ", f,
    test::nycleus::diags(
      std::make_unique<diag::unexpected_zwnj>(src_loc{f, {1, 9}, {1, 12}})
    ), std::array{
      t{nt::word{util::cow{u8"\u0646\u0627\u0645\u0647"s},
        nt::word::sigil_t::none}, {f, {1, 1}, {1, 12}}}
    });

  do_lexer_test_diags(u8"\u0D26\u0D43\u0D15\u0D4D\u200C ", f,
    test::nycleus::diags(
      std::make_unique<diag::unexpected_zwnj>(src_loc{f, {1, 13}, {1, 16}})
    ), std::array{
      t{nt::word{util::cow{u8"\u0D26\u0D43\u0D15\u0D4D"s},
        nt::word::sigil_t::none}, {f, {1, 1}, {1, 16}}}
    });
}

BOOST_AUTO_TEST_CASE(zwj_fail) {
  do_lexer_test_diags(u8"\u0DC1\u0DCA\u200D\u0DCF", f, test::nycleus::diags(
    std::make_unique<diag::unexpected_zwj>(src_loc{f, {1, 7}, {1, 10}})
  ), std::array{
    t{nt::word{util::cow{u8"\u0DC1\u0DCA\u0DCF"s},
      nt::word::sigil_t::none}, {f, {1, 1}, {1, 13}}}
  });
}

BOOST_AUTO_TEST_SUITE_END() // words

// Numeric literals ////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(numeric)

BOOST_AUTO_TEST_CASE(dec) {
  do_lexer_test(u8"1\U0001D7DF9", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 7}}}
  });
}

BOOST_AUTO_TEST_CASE(hex) {
  do_lexer_test(u8"\U0001D7D8x00BaDC0De1\U0001D7DA3b", f, std::array{
    t{nt::int_literal{util::cow{0xBADC0DE123B_big}}, {f, {1, 1}, {1, 22}}}
  });

  do_lexer_test(u8"0BaDC0De1\U0001D7DA3h", f, std::array{
    t{nt::int_literal{util::cow{0xBADC0DE123_big}}, {f, {1, 1}, {1, 16}}}
  });
}

BOOST_AUTO_TEST_CASE(oct) {
  do_lexer_test(u8"\U0001D7D8o001\U0001D7DA3", f, std::array{
    t{nt::int_literal{util::cow{0123_big}}, {f, {1, 1}, {1, 14}}}
  });

  do_lexer_test(u8"001\U0001D7DA3o", f, std::array{
    t{nt::int_literal{util::cow{0123_big}}, {f, {1, 1}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(bin) {
  do_lexer_test(u8"\U0001D7D8b101\U0001D7D90011", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 17}}}
  });

  do_lexer_test(u8"101\U0001D7D90011b", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 13}}}
  });
}

BOOST_AUTO_TEST_CASE(big) {
  do_lexer_test(
    u8"1234567890123456789012345678901234567890123456789012345678901234567890",
    f, std::array{t{nt::int_literal{util::cow{
      1234567890123456789012345678901234567890123456789012345678901234567890_big
    }}, {f, {1, 1}, {1, 71}}}});

  do_lexer_test(
    u8"0x123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF",
    f, std::array{t{nt::int_literal{util::cow{
      0x123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF_big
    }}, {f, {1, 1}, {1, 66}}}});

  do_lexer_test(
    u8"0o1234567012345670123456701234567012345670123456701234567012345670",
    f, std::array{t{nt::int_literal{util::cow{
      01234567012345670123456701234567012345670123456701234567012345670_big
    }}, {f, {1, 1}, {1, 67}}}});

  do_lexer_test(
    u8"0b011100100110001011100011001011001000011010110101001111010001",
    f, std::array{
      t{nt::int_literal{util::cow{0x7262E32C86B53D1_big}}, {f, {1, 1}, {1, 63}}}
    });
}

BOOST_AUTO_TEST_CASE(underscores) {
  do_lexer_test(u8"1_7__9", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 7}}}
  });

  do_lexer_test(u8"0__x_B3", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 8}}}
  });

  do_lexer_test(u8"10_1100__11_b", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 14}}}
  });

  do_lexer_test(u8"0_o26_3", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 8}}}
  });
}

BOOST_AUTO_TEST_CASE(prefix_and_suffix) {
  do_lexer_test_diags(u8"0x123h", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_base_prefix_and_suffix>(
      src_loc{f, {1, 1}, {1, 7}})
  ));

  do_lexer_test_diags(u8"0o123o", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_base_prefix_and_suffix>(
      src_loc{f, {1, 1}, {1, 7}})
  ));

  do_lexer_test_diags(u8"0b111b", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_base_prefix_and_suffix>(
      src_loc{f, {1, 1}, {1, 7}})
  ));
}

BOOST_AUTO_TEST_CASE(type_suffix) {
  do_lexer_test(u8"179u64", f, std::array{
    t{nt::int_literal{util::cow{179_big}, nycleus::int_type_suffix{64, false}},
      {f, {1, 1}, {1, 7}}}
  });

  do_lexer_test(u8"179s032", f, std::array{
    t{nt::int_literal{util::cow{179_big}, nycleus::int_type_suffix{32, true}},
      {f, {1, 1}, {1, 8}}}
  });

  do_lexer_test(u8"179U100", f, std::array{
    t{nt::int_literal{util::cow{179_big}, nycleus::int_type_suffix{100, false}},
      {f, {1, 1}, {1, 8}}}
  });

  do_lexer_test(u8"179S002", f, std::array{
    t{nt::int_literal{util::cow{179_big}, nycleus::int_type_suffix{2, true}},
      {f, {1, 1}, {1, 8}}}
  });
}

BOOST_AUTO_TEST_CASE(type_suffix_empty) {
  do_lexer_test_diags(u8"179u", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_suffix_missing_width>(
      src_loc{f, {1, 1}, {1, 5}},
      diag::num_literal_suffix_missing_width::prefix_letter_t::lower_u)
  ));

  do_lexer_test_diags(u8"179U", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_suffix_missing_width>(
      src_loc{f, {1, 1}, {1, 5}},
      diag::num_literal_suffix_missing_width::prefix_letter_t::upper_u)
  ));

  do_lexer_test_diags(u8"179s", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_suffix_missing_width>(
      src_loc{f, {1, 1}, {1, 5}},
      diag::num_literal_suffix_missing_width::prefix_letter_t::lower_s)
  ));

  do_lexer_test_diags(u8"179S", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_suffix_missing_width>(
      src_loc{f, {1, 1}, {1, 5}},
      diag::num_literal_suffix_missing_width::prefix_letter_t::upper_s)
  ));
}

BOOST_AUTO_TEST_CASE(type_suffix_zero) {
  do_lexer_test_diags(u8"179u0", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(
      src_loc{f, {1, 1}, {1, 6}})
  ));

  do_lexer_test_diags(u8"179U00", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(
      src_loc{f, {1, 1}, {1, 7}})
  ));

  do_lexer_test_diags(u8"179s000", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(
      src_loc{f, {1, 1}, {1, 8}})
  ));

  do_lexer_test_diags(u8"179S0000", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(
      src_loc{f, {1, 1}, {1, 9}})
  ));
}

BOOST_AUTO_TEST_CASE(type_suffix_big) {
  do_lexer_test_diags(u8"179u99999999999999999999999999999999", f,
    test::nycleus::diags(
      std::make_unique<diag::num_literal_suffix_too_wide>(
        src_loc{f, {1, 1}, {1, 37}})
    ));

  do_lexer_test_diags(u8"179U99999999999999999999999999999999", f,
    test::nycleus::diags(
      std::make_unique<diag::num_literal_suffix_too_wide>(
        src_loc{f, {1, 1}, {1, 37}})
    ));

  do_lexer_test_diags(u8"179s99999999999999999999999999999999", f,
    test::nycleus::diags(
      std::make_unique<diag::num_literal_suffix_too_wide>(
        src_loc{f, {1, 1}, {1, 37}})
    ));

  do_lexer_test_diags(u8"179S99999999999999999999999999999999", f,
    test::nycleus::diags(
      std::make_unique<diag::num_literal_suffix_too_wide>(
        src_loc{f, {1, 1}, {1, 37}})
    ));
}

BOOST_AUTO_TEST_CASE(invalid) {
  do_lexer_test_diags(u8"0x123.456hello", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 15}})
  ));

  do_lexer_test_diags(u8"123hello", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 9}})
  ));

  do_lexer_test_diags(u8".456hello", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 10}})
  ));

  do_lexer_test_diags(u8"123e+5hello", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 12}})
  ));

  do_lexer_test_diags(u8"123.456e+5hello", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 16}})
  ));

  do_lexer_test_diags(u8".456e+5hello", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 13}})
  ));

  do_lexer_test_diags(u8"0x123foo", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 9}})
  ));

  do_lexer_test_diags(u8"123$foo", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 8}})
  ));

  do_lexer_test_diags(u8"0x123$foo", f, test::nycleus::diags(
    std::make_unique<diag::num_literal_invalid>(
      src_loc{f, {1, 1}, {1, 10}})
  ));
}

BOOST_AUTO_TEST_SUITE_END() // numeric

// String & character literals /////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(strings)

BOOST_AUTO_TEST_CASE(string_literal) {
  do_lexer_test(u8"\"test\"", f, std::array{
    t{nt::string_literal{util::cow{u8"test"s}}, {f, {1, 1}, {1, 7}}}
  });
}

BOOST_AUTO_TEST_CASE(char_literal) {
  do_lexer_test(u8"\'test\'", f, std::array{
    t{nt::char_literal{util::cow{u8"test"s}}, {f, {1, 1}, {1, 7}}}
  });
}

BOOST_AUTO_TEST_CASE(string_literal_normalization) {
  do_lexer_test(u8"\"franc\u0327ais\"", f, std::array{
    t{nt::string_literal{util::cow{u8"fran\u00E7ais"s}}, {f, {1, 1}, {1, 13}}}
  });
}

BOOST_AUTO_TEST_CASE(char_literal_normalization) {
  do_lexer_test(u8"'c\u0327'", f, std::array{
    t{nt::char_literal{util::cow{u8"\u00E7"s}}, {f, {1, 1}, {1, 6}}}
  });
}

BOOST_AUTO_TEST_CASE(escape_codes) {
  do_lexer_test(
    u8"\"\\\"\\'\\\\\\a\\b\\e\\f\\n\\r\\t\\v\"\n'\\'' '\\n'", f,
    std::array{
      t{nt::string_literal{util::cow{u8"\"'\\\a\b\u001B\f\n\r\t\v"s}},
        {f, {1, 1}, {1, 25}}},
      t{nt::char_literal{util::cow{u8"'"s}}, {f, {2, 1}, {2, 5}}},
      t{nt::char_literal{util::cow{u8"\n"s}}, {f, {2, 6}, {2, 10}}}
    }
  );
}

BOOST_AUTO_TEST_CASE(unicode_escape_codes) {
  do_lexer_test(
    u8"\"\\u0041\\u0416\\u4E2D\"\n\"\\U000041\\U000416\\U004E2D\\U01F4A9\"", f,
    std::array{
      t{nt::string_literal{util::cow{u8"A\u0416\u4E2D"s}},
        {f, {1, 1}, {1, 21}}},
      t{nt::string_literal{util::cow{u8"A\u0416\u4E2D\U0001F4A9"s}},
        {f, {2, 1}, {2, 35}}}
    }
  );
}

BOOST_AUTO_TEST_CASE(invalid_escape_codes) {
  do_lexer_test_diags(u8"\"\\x\"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 2}, {1, 4}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"\uFFFD"s}}, {f, {1, 1}, {1, 5}}}
  });

  do_lexer_test_diags(u8"\"\\u12 \"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 2}, {1, 6}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"\uFFFD "s}}, {f, {1, 1}, {1, 8}}}
  });

  do_lexer_test_diags(u8"\"abc\\U12345\"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 5}, {1, 12}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"abc\uFFFD"s}}, {f, {1, 1}, {1, 13}}}
  });
}

BOOST_AUTO_TEST_CASE(invalid_escape_codes_unicode) {
  do_lexer_test_diags(u8"\"\\uD800\"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 2}, {1, 8}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"\uFFFD"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"\\U00D800\"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 2}, {1, 10}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"\uFFFD"s}}, {f, {1, 1}, {1, 11}}}
  });

  do_lexer_test_diags(u8"\"\\UFFFFFF\"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 2}, {1, 10}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"\uFFFD"s}}, {f, {1, 1}, {1, 11}}}
  });
}

BOOST_AUTO_TEST_CASE(escape_codes_normalization) {
  do_lexer_test_diags(u8"\"\\a\u0301\"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 2}, {1, 3}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"\uFFFD"s}}, {f, {1, 1}, {1, 7}}}
  });

  do_lexer_test_diags(u8"\"\\u\u0301\"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 2}, {1, 3}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"\uFFFD"s}}, {f, {1, 1}, {1, 7}}}
  });

  do_lexer_test_diags(u8"\"\\u123A\u0307\"", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 2}, {1, 3}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"\uFFFD\u0226"s}}, {f, {1, 1}, {1, 11}}}
  });
}

BOOST_AUTO_TEST_CASE(unterminated_string_literal) {
  do_lexer_test_diags(u8"\"abc", f, test::nycleus::diags(
    std::make_unique<diag::unterminated_string>(src_loc{f, {1, 1}, {1, 5}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"abc"s}}, {f, {1, 1}, {1, 5}}}
  });

  do_lexer_test_diags(u8"\"abc\\", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 5}, {1, 6}}),
    std::make_unique<diag::unterminated_string>(src_loc{f, {1, 1}, {1, 6}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"abc\uFFFD"s}}, {f, {1, 1}, {1, 6}}}
  });

  do_lexer_test_diags(u8"\"abc\\u12", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 5}, {1, 9}}),
    std::make_unique<diag::unterminated_string>(src_loc{f, {1, 1}, {1, 9}})
  ), std::array{
    t{nt::string_literal{util::cow{u8"abc\uFFFD"s}}, {f, {1, 1}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(unterminated_char_literal) {
  do_lexer_test_diags(u8"'abc", f, test::nycleus::diags(
    std::make_unique<diag::unterminated_char>(src_loc{f, {1, 1}, {1, 5}})
  ), std::array{
    t{nt::char_literal{util::cow{u8"abc"s}}, {f, {1, 1}, {1, 5}}}
  });

  do_lexer_test_diags(u8"'abc\\", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 5}, {1, 6}}),
    std::make_unique<diag::unterminated_char>(src_loc{f, {1, 1}, {1, 6}})
  ), std::array{
    t{nt::char_literal{util::cow{u8"abc\uFFFD"s}}, {f, {1, 1}, {1, 6}}}
  });

  do_lexer_test_diags(u8"'abc\\u12", f, test::nycleus::diags(
    std::make_unique<diag::invalid_escape_code>(src_loc{f, {1, 5}, {1, 9}}),
    std::make_unique<diag::unterminated_char>(src_loc{f, {1, 1}, {1, 9}})
  ), std::array{
    t{nt::char_literal{util::cow{u8"abc\uFFFD"s}}, {f, {1, 1}, {1, 9}}}
  });
}

// NOLINTBEGIN(misc-misleading-bidirectional)

BOOST_AUTO_TEST_CASE(bidi_code_in_string) {
  do_lexer_test_diags(u8"\"foo\u202A\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_lre)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u202A"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"foo\u202B\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_rle)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u202B"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"foo\u202C\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_pdf)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u202C"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"foo\u202D\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_lro)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u202D"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"foo\u202E\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_rlo)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u202E"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"foo\u2066\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_lri)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u2066"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"foo\u2067\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_rli)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u2067"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"foo\u2068\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_fsi)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u2068"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"\"foo\u2069\"", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::str_pdi)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u2069"s}}, {f, {1, 1}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(bidi_code_in_char) {
  do_lexer_test_diags(u8"'foo\u202A'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_lre)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u202A"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"'foo\u202B'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_rle)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u202B"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"'foo\u202C'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_pdf)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u202C"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"'foo\u202D'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_lro)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u202D"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"'foo\u202E'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_rlo)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u202E"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"'foo\u2066'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_lri)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u2066"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"'foo\u2067'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_rli)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u2067"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"'foo\u2068'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_fsi)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u2068"s}}, {f, {1, 1}, {1, 9}}}
  });

  do_lexer_test_diags(u8"'foo\u2069'", f, test::nycleus::diags(
    std::make_unique<diag::bidi_code_in_string>(src_loc{f, {1, 5}, {1, 8}},
      diag::bidi_code_in_string::bidi_code_t::char_pdi)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u2069"s}}, {f, {1, 1}, {1, 9}}}
  });
}

// NOLINTEND(misc-misleading-bidirectional)

BOOST_AUTO_TEST_CASE(newlines) {
  do_lexer_test_diags(u8"\"foo\nbar\"", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::str_lf)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\nbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"\"foo\vbar\"", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::str_vt)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\vbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"\"foo\fbar\"", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::str_ff)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\fbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"\"foo\rbar\"", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::str_cr)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\rbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"\"foo\r\nbar\"", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::str_cr),
    std::make_unique<diag::string_newline>(src_loc{f, {1, 6}, {1, 7}},
      diag::string_newline::newline_code_t::str_lf)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\r\nbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"\"foo\u0085bar\"", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 7}},
      diag::string_newline::newline_code_t::str_nel)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u0085bar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"\"foo\u2028bar\"", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 8}},
      diag::string_newline::newline_code_t::str_line_sep)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u2028bar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"\"foo\u2029bar\"", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 8}},
      diag::string_newline::newline_code_t::str_para_sep)
  ), std::array{
    t{nt::string_literal{util::cow{u8"foo\u2029bar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"'foo\nbar'", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::char_lf)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\nbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"'foo\vbar'", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::char_vt)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\vbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"'foo\fbar'", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::char_ff)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\fbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"'foo\rbar'", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::char_cr)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\rbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"'foo\r\nbar'", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 6}},
      diag::string_newline::newline_code_t::char_cr),
    std::make_unique<diag::string_newline>(src_loc{f, {1, 6}, {1, 7}},
      diag::string_newline::newline_code_t::char_lf)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\r\nbar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"'foo\u0085bar'", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 7}},
      diag::string_newline::newline_code_t::char_nel)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u0085bar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"'foo\u2028bar'", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 8}},
      diag::string_newline::newline_code_t::char_line_sep)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u2028bar"s}}, {f, {1, 1}, {2, 5}}}
  });

  do_lexer_test_diags(u8"'foo\u2029bar'", f, test::nycleus::diags(
    std::make_unique<diag::string_newline>(src_loc{f, {1, 5}, {1, 8}},
      diag::string_newline::newline_code_t::char_para_sep)
  ), std::array{
    t{nt::char_literal{util::cow{u8"foo\u2029bar"s}}, {f, {1, 1}, {2, 5}}}
  });
}

BOOST_AUTO_TEST_SUITE_END() // strings

// White space /////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(white_space) {
  do_lexer_test(u8"a  b", f, std::array{
    t{nt::word{util::cow{u8"a"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 2}}},
    t{nt::word{util::cow{u8"b"s}, nt::word::sigil_t::none},
      {f, {1, 4}, {1, 5}}}
  });

  do_lexer_test(u8"a \u2000 b", f, std::array{
    t{nt::word{util::cow{u8"a"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 2}}},
    t{nt::word{util::cow{u8"b"s}, nt::word::sigil_t::none},
      {f, {1, 7}, {1, 8}}}
  });

  do_lexer_test(u8"a \u2028 b", f, std::array{
    t{nt::word{util::cow{u8"a"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 2}}},
    t{nt::word{util::cow{u8"b"s}, nt::word::sigil_t::none},
      {f, {2, 2}, {2, 3}}}
  });
}

// Comments ////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(comments)

BOOST_AUTO_TEST_CASE(single_line) {
  do_lexer_test(u8"#comment\nfoo", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 10}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {2, 1}, {2, 4}}}
  });

  do_lexer_test(u8"#comment\r\nfoo", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 11}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {2, 1}, {2, 4}}}
  });

  do_lexer_test(u8"#comment\rfoo", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 10}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {2, 1}, {2, 4}}}
  });

  do_lexer_test(u8"#comment\vfoo", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 10}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {2, 1}, {2, 4}}}
  });

  do_lexer_test(u8"#comment\ffoo", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 10}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {2, 1}, {2, 4}}}
  });

  do_lexer_test(u8"#comment\u0085foo", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 11}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {2, 1}, {2, 4}}}
  });

  do_lexer_test(u8"#comment\u2028foo", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 12}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {2, 1}, {2, 4}}}
  });

  do_lexer_test(u8"#comment\u2029foo", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 12}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {2, 1}, {2, 4}}}
  });

  do_lexer_test(u8"#comment", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 1}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(multiline_non_nestable) {
  do_lexer_test(u8"#* This\ncomment #* doesn't *# nest", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {2, 22}}},
    t{nt::word{util::cow{u8"nest"s}}, {f, {2, 23}, {2, 27}}}
  });
}

BOOST_AUTO_TEST_CASE(multiline_nestable) {
  do_lexer_test(u8"#+ This\ncomment #+ actually +# nests +# foo", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_nestable}, {f, {1, 1}, {2, 32}}},
    t{nt::word{util::cow{u8"foo"s}}, {f, {2, 33}, {2, 36}}}
  });
}

BOOST_AUTO_TEST_CASE(multiline_non_nestable_unterminated) {
  do_lexer_test_diags(u8"#* unterminated"s, f, test::nycleus::diags(
    std::make_unique<diag::unterminated_comment>(src_loc{f, {1, 1}, {1, 16}})
  ));
}

BOOST_AUTO_TEST_CASE(multiline_nestable_unterminated) {
  do_lexer_test_diags(u8"#+ unterminated #+ comment +#"s, f,
    test::nycleus::diags(
      std::make_unique<diag::unterminated_comment>(src_loc{f, {1, 1}, {1, 30}})
    ));
}

BOOST_AUTO_TEST_SUITE(bidi)

// NOLINTBEGIN(misc-misleading-bidirectional)

BOOST_AUTO_TEST_CASE(embed_basic) {
  do_lexer_test(
    u8"#a\u202Ab\u202Cc\u202Bd\u202Ce\u202Df\u202Cg\u202Eh\u202C", f,
    std::array{
      t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
        {f, {1, 1}, {1, 34}}}
    });
}

BOOST_AUTO_TEST_CASE(embed_nested) {
  do_lexer_test(
    u8"#a\u202Ab\u202Bc\u202Dd\u202Ce\u202Cf\u202Eg\u202Ch\u202C", f,
    std::array{
      t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
        {f, {1, 1}, {1, 34}}}
    });
}

BOOST_AUTO_TEST_CASE(embed_missing_close) {
  do_lexer_test_diags(u8"#a\u202Ab\u202Bc\u202Cd", f, test::nycleus::diags(
    std::make_unique<diag::comment_unmatched_bidi_code>(
      src_loc{f, {1, 3}, {1, 6}},
      diag::comment_unmatched_bidi_code::bidi_code_t::lre)
  ), std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      src_loc{f, {1, 1}, {1, 15}}}
  });
}

BOOST_AUTO_TEST_CASE(embed_missing_open) {
  do_lexer_test_diags(u8"#a\u202Cb\u202Ac\u202Cd", f, test::nycleus::diags(
    std::make_unique<diag::comment_unmatched_bidi_code>(
      src_loc{f, {1, 3}, {1, 6}},
      diag::comment_unmatched_bidi_code::bidi_code_t::pdf)
  ), std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      src_loc{f, {1, 1}, {1, 15}}}
  });
}

BOOST_AUTO_TEST_CASE(embed_lines) {
  do_lexer_test(
    u8"#+a\u202Ab\u202Cd\ne\u202Bf\u202Cg+#", f,
    std::array{
      t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::multiline_nestable},
        {f, {1, 1}, {2, 12}}}
    }
  );
}

BOOST_AUTO_TEST_CASE(embed_lines_bad) {
  do_lexer_test_diags(u8"#+a\u202Ab\nc\u202Cd+#", f, test::nycleus::diags(
    std::make_unique<diag::comment_unmatched_bidi_code>(
      src_loc{f, {1, 4}, {1, 7}},
      diag::comment_unmatched_bidi_code::bidi_code_t::lre),
    std::make_unique<diag::comment_unmatched_bidi_code>(
      src_loc{f, {2, 2}, {2, 5}},
      diag::comment_unmatched_bidi_code::bidi_code_t::pdf)
  ), std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::multiline_nestable},
      src_loc{f, {1, 1}, {2, 8}}}
  });
}

BOOST_AUTO_TEST_CASE(isolate_basic) {
  do_lexer_test(
    u8"#a\u2066b\u2069c\u2067d\u2069e\u2068f\u2069g", f,
    std::array{
      t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
        {f, {1, 1}, {1, 27}}}
    });
}

BOOST_AUTO_TEST_CASE(isolate_nested) {
  do_lexer_test(
    u8"#a\u2066b\u2067c\u2069d\u2068e\u2069f\u2069g", f,
    std::array{
      t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
        {f, {1, 1}, {1, 27}}}
    });
}

BOOST_AUTO_TEST_CASE(isolate_missing_close) {
  do_lexer_test_diags(u8"#a\u2066b\u2067c\u2069d", f, test::nycleus::diags(
    std::make_unique<diag::comment_unmatched_bidi_code>(
      src_loc{f, {1, 3}, {1, 6}},
      diag::comment_unmatched_bidi_code::bidi_code_t::lri)
  ), std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      src_loc{f, {1, 1}, {1, 15}}}
  });
}

BOOST_AUTO_TEST_CASE(isolate_missing_open) {
  do_lexer_test_diags(u8"#a\u2069b\u2066c\u2069d", f, test::nycleus::diags(
    std::make_unique<diag::comment_unmatched_bidi_code>(
      src_loc{f, {1, 3}, {1, 6}},
      diag::comment_unmatched_bidi_code::bidi_code_t::pdi)
  ), std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      src_loc{f, {1, 1}, {1, 15}}}
  });
}

BOOST_AUTO_TEST_CASE(isolate_lines) {
  do_lexer_test(
    u8"#+a\u2066b\u2069d\ne\u2067f\u2069g+#", f,
    std::array{
      t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::multiline_nestable},
        {f, {1, 1}, {2, 12}}}
    });
}

BOOST_AUTO_TEST_CASE(isolate_lines_bad) {
  do_lexer_test_diags(u8"#+a\u2066b\nc\u2069d+#", f, test::nycleus::diags(
    std::make_unique<diag::comment_unmatched_bidi_code>(
      src_loc{f, {1, 4}, {1, 7}},
      diag::comment_unmatched_bidi_code::bidi_code_t::lri),
    std::make_unique<diag::comment_unmatched_bidi_code>(
      src_loc{f, {2, 2}, {2, 5}},
      diag::comment_unmatched_bidi_code::bidi_code_t::pdi)
  ), std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_nestable}, src_loc{f, {1, 1}, {2, 8}}}
  });
}

BOOST_AUTO_TEST_CASE(embed_inside_isolate) {
  do_lexer_test(
    u8"#\u2066\u2067\u202A\u202B\u2069\u202C\u2069", f,
    std::array{
      t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
        {f, {1, 1}, {1, 23}}}
    });
}

BOOST_AUTO_TEST_CASE(isolate_inside_embed) {
  do_lexer_test(
    u8"#\u202A\u202B\u2066\u202C\u2069\u202C\u202C", f,
    std::array{
      t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
        {f, {1, 1}, {1, 23}}}
    });
}

BOOST_AUTO_TEST_CASE(nested_comments) {
  do_lexer_test(u8"#+\u202A#+\u202C+#+#", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::multiline_nestable},
      {f, {1, 1}, {1, 15}}}
  });

  do_lexer_test(u8"#+#+\u2066+#\u2069+#", f, std::array{
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::multiline_nestable},
      {f, {1, 1}, {1, 15}}}
  });
}

// NOLINTEND(misc-misleading-bidirectional)

BOOST_AUTO_TEST_SUITE_END() // bidi

BOOST_AUTO_TEST_SUITE_END() // comments

// Invalid code points /////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(unexpected_code_point) {
  auto const tokens{do_lexer_test_diags(u8"let \u20BD", f, test::nycleus::diags(
    std::make_unique<diag::unexpected_code_point>(src_loc{f, {1, 5}, {1, 8}},
      U'\u20BD')
  ))};
  BOOST_TEST(util::starts_with(tokens, std::array{
    t{nt::word{util::cow{u8"let"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 4}}}
  }));
}

// Token combinations //////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(token_combo)

BOOST_AUTO_TEST_CASE(kw_string) {
  do_lexer_test(u8"foo\"bar\"", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 4}}},
    t{nt::string_literal{util::cow{u8"bar"s}},
      {f, {1, 4}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(kw_char) {
  do_lexer_test(u8"foo'bar'", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 4}}},
    t{nt::char_literal{util::cow{u8"bar"s}},
      {f, {1, 4}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(kw_punct) {
  do_lexer_test(u8"foo.", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 4}}},
    t{nt::point{}, {f, {1, 4}, {1, 5}}}
  });
}

BOOST_AUTO_TEST_CASE(kw_comment) {
  do_lexer_test(u8"foo#bar", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {1, 1}, {1, 4}}},
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 4}, {1, 8}}}
  });
}

BOOST_AUTO_TEST_CASE(id_string) {
  do_lexer_test(u8"$foo\"bar\"", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::dollar},
      {f, {1, 1}, {1, 5}}},
    t{nt::string_literal{util::cow{u8"bar"s}},
      {f, {1, 5}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(id_char) {
  do_lexer_test(u8"$foo'bar'", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::dollar},
      {f, {1, 1}, {1, 5}}},
    t{nt::char_literal{util::cow{u8"bar"s}},
      {f, {1, 5}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(id_punct) {
  do_lexer_test(u8"$foo.", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::dollar},
      {f, {1, 1}, {1, 5}}},
    t{nt::point{}, {f, {1, 5}, {1, 6}}}
  });
}

BOOST_AUTO_TEST_CASE(id_comment) {
  do_lexer_test(u8"$foo#bar", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::dollar},
      {f, {1, 1}, {1, 5}}},
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 5}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(ext_string) {
  do_lexer_test(u8"_foo\"bar\"", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::underscore},
      {f, {1, 1}, {1, 5}}},
    t{nt::string_literal{util::cow{u8"bar"s}},
      {f, {1, 5}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(ext_char) {
  do_lexer_test(u8"_foo'bar'", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::underscore},
      {f, {1, 1}, {1, 5}}},
    t{nt::char_literal{util::cow{u8"bar"s}},
      {f, {1, 5}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(ext_punct) {
  do_lexer_test(u8"_foo.", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::underscore},
      {f, {1, 1}, {1, 5}}},
    t{nt::point{}, {f, {1, 5}, {1, 6}}}
  });
}

BOOST_AUTO_TEST_CASE(ext_comment) {
  do_lexer_test(u8"_foo#bar", f, std::array{
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::underscore},
      {f, {1, 1}, {1, 5}}},
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 5}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(num_string) {
  do_lexer_test(u8"179\"bar\"", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 4}}},
    t{nt::string_literal{util::cow{u8"bar"s}},
      {f, {1, 4}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(num_char) {
  do_lexer_test(u8"179'bar'", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 4}}},
    t{nt::char_literal{util::cow{u8"bar"s}},
      {f, {1, 4}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(num_punct) {
  do_lexer_test(u8"179:", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 4}}},
    t{nt::colon{}, {f, {1, 4}, {1, 5}}}
  });
}

BOOST_AUTO_TEST_CASE(num_comment) {
  do_lexer_test(u8"179#bar", f, std::array{
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 1}, {1, 4}}},
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 4}, {1, 8}}}
  });
}

BOOST_AUTO_TEST_CASE(string_kw) {
  do_lexer_test(u8"\"foo\"bar", f, std::array{
    t{nt::string_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::none},
      {f, {1, 6}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(string_id) {
  do_lexer_test(u8"\"foo\"$bar", f, std::array{
    t{nt::string_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::dollar},
      {f, {1, 6}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(string_ext) {
  do_lexer_test(u8"\"foo\"_bar", f, std::array{
    t{nt::string_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::underscore},
      {f, {1, 6}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(string_string) {
  do_lexer_test(u8"\"foo\"\"bar\"", f, std::array{
    t{nt::string_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::string_literal{util::cow{u8"bar"s}}, {f, {1, 6}, {1, 11}}},
  });
}

BOOST_AUTO_TEST_CASE(string_char) {
  do_lexer_test(u8"\"foo\"'bar'", f, std::array{
    t{nt::string_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::char_literal{util::cow{u8"bar"s}}, {f, {1, 6}, {1, 11}}},
  });
}

BOOST_AUTO_TEST_CASE(string_punct) {
  do_lexer_test(u8"\"foo\".", f, std::array{
    t{nt::string_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::point{}, {f, {1, 6}, {1, 7}}}
  });
}

BOOST_AUTO_TEST_CASE(string_comment) {
  do_lexer_test(u8"\"foo\"#bar", f, std::array{
    t{nt::string_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 6}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(char_kw) {
  do_lexer_test(u8"'foo'bar", f, std::array{
    t{nt::char_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::none},
      {f, {1, 6}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(char_id) {
  do_lexer_test(u8"'foo'$bar", f, std::array{
    t{nt::char_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::dollar},
      {f, {1, 6}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(char_ext) {
  do_lexer_test(u8"'foo'_bar", f, std::array{
    t{nt::char_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::underscore},
      {f, {1, 6}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(char_string) {
  do_lexer_test(u8"'foo'\"bar\"", f, std::array{
    t{nt::char_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::string_literal{util::cow{u8"bar"s}}, {f, {1, 6}, {1, 11}}},
  });
}

BOOST_AUTO_TEST_CASE(char_char) {
  do_lexer_test(u8"'foo''bar'", f, std::array{
    t{nt::char_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::char_literal{util::cow{u8"bar"s}}, {f, {1, 6}, {1, 11}}},
  });
}

BOOST_AUTO_TEST_CASE(char_punct) {
  do_lexer_test(u8"'foo'.", f, std::array{
    t{nt::char_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::point{}, {f, {1, 6}, {1, 7}}}
  });
}

BOOST_AUTO_TEST_CASE(char_comment) {
  do_lexer_test(u8"'foo'#bar", f, std::array{
    t{nt::char_literal{util::cow{u8"foo"s}}, {f, {1, 1}, {1, 6}}},
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 6}, {1, 10}}}
  });
}

BOOST_AUTO_TEST_CASE(punct_kw) {
  do_lexer_test(u8".foo", f, std::array{
    t{nt::point{}, {f, {1, 1}, {1, 2}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::none},
      {f, {1, 2}, {1, 5}}}
  });
}

BOOST_AUTO_TEST_CASE(punct_id) {
  do_lexer_test(u8".$foo", f, std::array{
    t{nt::point{}, {f, {1, 1}, {1, 2}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::dollar},
      {f, {1, 2}, {1, 6}}}
  });
}

BOOST_AUTO_TEST_CASE(punct_ext) {
  do_lexer_test(u8"._foo", f, std::array{
    t{nt::point{}, {f, {1, 1}, {1, 2}}},
    t{nt::word{util::cow{u8"foo"s}, nt::word::sigil_t::underscore},
      {f, {1, 2}, {1, 6}}}
  });
}

BOOST_AUTO_TEST_CASE(punct_num) {
  do_lexer_test(u8":179", f, std::array{
    t{nt::colon{}, {f, {1, 1}, {1, 2}}},
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 2}, {1, 5}}}
  });
}

BOOST_AUTO_TEST_CASE(punct_string) {
  do_lexer_test(u8".\"foo\"", f, std::array{
    t{nt::point{}, {f, {1, 1}, {1, 2}}},
    t{nt::string_literal{util::cow{u8"foo"s}}, {f, {1, 2}, {1, 7}}}
  });
}

BOOST_AUTO_TEST_CASE(punct_char) {
  do_lexer_test(u8".'foo'", f, std::array{
    t{nt::point{}, {f, {1, 1}, {1, 2}}},
    t{nt::char_literal{util::cow{u8"foo"s}}, {f, {1, 2}, {1, 7}}}
  });
}

BOOST_AUTO_TEST_CASE(punct_comment) {
  do_lexer_test(u8".#foo", f, std::array{
    t{nt::point{}, {f, {1, 1}, {1, 2}}},
    t{nt::comment{util::cow{u8""s}, nt::comment::kind_t::single_line},
      {f, {1, 2}, {1, 6}}}
  });
}

BOOST_AUTO_TEST_CASE(comment_kw) {
  do_lexer_test(u8"#*foo*#bar", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {1, 8}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::none},
      {f, {1, 8}, {1, 11}}}
  });
}

BOOST_AUTO_TEST_CASE(comment_id) {
  do_lexer_test(u8"#*foo*#$bar", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {1, 8}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::dollar},
      {f, {1, 8}, {1, 12}}}
  });
}

BOOST_AUTO_TEST_CASE(comment_ext) {
  do_lexer_test(u8"#*foo*#_bar", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {1, 8}}},
    t{nt::word{util::cow{u8"bar"s}, nt::word::sigil_t::underscore},
      {f, {1, 8}, {1, 12}}}
  });
}

BOOST_AUTO_TEST_CASE(comment_num) {
  do_lexer_test(u8"#*foo*#179", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {1, 8}}},
    t{nt::int_literal{util::cow{179_big}}, {f, {1, 8}, {1, 11}}}
  });
}

BOOST_AUTO_TEST_CASE(comment_string) {
  do_lexer_test(u8"#*foo*#\"bar\"", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {1, 8}}},
    t{nt::string_literal{util::cow{u8"bar"s}}, {f, {1, 8}, {1, 13}}}
  });
}

BOOST_AUTO_TEST_CASE(comment_char) {
  do_lexer_test(u8"#*foo*#'bar'", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {1, 8}}},
    t{nt::char_literal{util::cow{u8"bar"s}}, {f, {1, 8}, {1, 13}}}
  });
}

BOOST_AUTO_TEST_CASE(comment_punct) {
  do_lexer_test(u8"#*foo*#.", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {1, 8}}},
    t{nt::point{}, {f, {1, 8}, {1, 9}}}
  });
}

BOOST_AUTO_TEST_CASE(comment_comment) {
  do_lexer_test(u8"#*foo*##bar", f, std::array{
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::multiline_non_nestable}, {f, {1, 1}, {1, 8}}},
    t{nt::comment{util::cow{u8""s},
      nt::comment::kind_t::single_line}, {f, {1, 8}, {1, 12}}}
  });
}

BOOST_AUTO_TEST_SUITE_END() // token_combo

BOOST_AUTO_TEST_SUITE_END() // lexer
BOOST_AUTO_TEST_SUITE_END() // test_nycleus
