#ifndef NYCLEUS_LEXER_NEW_HPP_INCLUDED_FCC9L3CJA7A7JRRHNXX2BD0DU
#define NYCLEUS_LEXER_NEW_HPP_INCLUDED_FCC9L3CJA7A7JRRHNXX2BD0DU

#include "nycleus/diagnostic.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/token.hpp"
#include "util/bigint.hpp"
#include "util/iterator_facade.hpp"
#include "util/range_adaptor_closure.hpp"
#include "util/util.hpp"
#include <cassert>
#include <concepts>
#include <iterator>
#include <optional>
#include <ranges>
#include <string>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

namespace nycleus {

/** @brief A class that consumes a sequence of code points into one token.
 *
 * A single instance of the lexer can produce only one token. To produce more
 * tokens, another instance has to be created.
 *
 * The protocol for interacting with the lexer is as follows:
 * 1. Feed the code points and their associated location information one by one
 *    via the `feed` function.
 * 2. If the end of the file is reached, inform the lexer via the `feed_eof`
 *    function.
 * 3. Both functions return a Boolean value to indicate whether a new token can
 *    be produced. While they return false, characters may be fed. Once true is
 *    returned, the next token should be retrieved by calling `get_token`.
 * 4. Afterwards, the object is useless and must be discarded. A new object may
 *    be created to produce more tokens.
 *
 * If `feed_eof` returns false, the sequence contained no tokens.
 *
 * If `feed` returns true, the code point fed into it on that invocation is not
 * part of the token. If lexing is continued, the same code point should be the
 * first one fed into the next object.
 *
 * In case of a lexing error, a special error token is returned. */
class lexer {
private:
  struct state_init;
  struct state_in_word;
  struct state_in_num;

  struct state_in_comment_begin;
  struct state_in_comment_sl;
  struct state_in_comment_ml;
  struct state_in_comment_mlnest;

  struct state_in_string;
  struct state_after_string;

  struct state_seen_excl;
  struct state_seen_neq;
  struct state_seen_percent;
  struct state_seen_percent_assign;
  struct state_seen_amp;
  struct state_seen_logical_and;
  struct state_seen_amp_assign;
  struct state_seen_lparen;
  struct state_seen_rparen;
  struct state_seen_star;
  struct state_seen_star_assign;
  struct state_seen_plus;
  struct state_seen_incr;
  struct state_seen_plus_assign;
  struct state_seen_comma;
  struct state_seen_minus;
  struct state_seen_decr;
  struct state_seen_minus_assign;
  struct state_seen_arrow;
  struct state_seen_point;
  struct state_seen_double_point;
  struct state_seen_ellipsis;
  struct state_seen_slash;
  struct state_seen_slash_assign;
  struct state_seen_colon;
  struct state_seen_semicolon;
  struct state_seen_lt;
  struct state_seen_lrot;
  struct state_seen_lrot_assign;
  struct state_seen_lshift;
  struct state_seen_lshift_assign;
  struct state_seen_leq;
  struct state_seen_three_way_cmp;
  struct state_seen_assign;
  struct state_seen_eq;
  struct state_seen_fat_arrow;
  struct state_seen_gt;
  struct state_seen_rrot;
  struct state_seen_rrot_assign;
  struct state_seen_geq;
  struct state_seen_rshift;
  struct state_seen_rshift_assign;
  struct state_seen_question;
  struct state_seen_at;
  struct state_seen_lsquare;
  struct state_seen_rsquare;
  struct state_seen_bitwise_xor;
  struct state_seen_bitwise_xor_assign;
  struct state_seen_logical_xor;
  struct state_seen_lbrace;
  struct state_seen_pipe;
  struct state_seen_pipe_assign;
  struct state_seen_subst;
  struct state_seen_logical_or;
  struct state_seen_rbrace;
  struct state_seen_tilde;

  struct state_error;

  using state_t = std::variant<
    state_init,
    state_in_word,
    state_in_num,

    state_in_comment_begin,
    state_in_comment_sl,
    state_in_comment_ml,
    state_in_comment_mlnest,

    state_in_string,
    state_after_string,

    state_seen_excl,
    state_seen_neq,
    state_seen_percent,
    state_seen_percent_assign,
    state_seen_amp,
    state_seen_logical_and,
    state_seen_amp_assign,
    state_seen_lparen,
    state_seen_rparen,
    state_seen_star,
    state_seen_star_assign,
    state_seen_plus,
    state_seen_incr,
    state_seen_plus_assign,
    state_seen_comma,
    state_seen_minus,
    state_seen_decr,
    state_seen_minus_assign,
    state_seen_arrow,
    state_seen_point,
    state_seen_double_point,
    state_seen_ellipsis,
    state_seen_slash,
    state_seen_slash_assign,
    state_seen_colon,
    state_seen_semicolon,
    state_seen_lt,
    state_seen_lrot,
    state_seen_lrot_assign,
    state_seen_lshift,
    state_seen_lshift_assign,
    state_seen_leq,
    state_seen_three_way_cmp,
    state_seen_assign,
    state_seen_eq,
    state_seen_fat_arrow,
    state_seen_gt,
    state_seen_rrot,
    state_seen_rrot_assign,
    state_seen_geq,
    state_seen_rshift,
    state_seen_rshift_assign,
    state_seen_question,
    state_seen_at,
    state_seen_lsquare,
    state_seen_rsquare,
    state_seen_bitwise_xor,
    state_seen_bitwise_xor_assign,
    state_seen_logical_xor,
    state_seen_lbrace,
    state_seen_pipe,
    state_seen_pipe_assign,
    state_seen_subst,
    state_seen_logical_or,
    state_seen_rbrace,
    state_seen_tilde,

    state_error
  >;

  struct state_init {
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location);
    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed_eof()
        noexcept {
      return {false, std::nullopt};
    }
    [[noreturn]] static token get_token() noexcept { util::unreachable(); }
  };

  struct state_in_word {
    /** @brief The sigil for the newly constructed word. */
    tok::word::sigil_t sigil;

    /** @brief A buffer holding the pre-normalization value of the word. */
    std::u32string buf;

    /** @brief The source range of code points comprising the word. */
    source_location loc;

    /** @brief The list of poisitions in the string where ZW(N)J code points
     * were found, along with their source locations. */
    std::vector<std::pair<std::size_t, source_location>> format_locs;

    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(
      char32_t,
      source_location
    );
    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed_eof() noexcept;
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] token get_token();
  };

  struct state_in_num {
    std::u32string buf;
    source_location loc;

    enum class substate_t { expects_x, dec, hex, dec_exp, hex_exp };
    substate_t substate;

    /** @throws std::bad_alloc */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(
      char32_t,
      source_location
    );
    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed_eof() noexcept;
    /** @throws std::bad_alloc */
    [[nodiscard]] token get_token();
  };

  /** @brief A utility that keeps track of bidirectional formatting codes inside
   * comments and produces diagnostics in case of invalid sequences. */
  class comment_bidi_tracker {
  private:
    enum class open_code: std::uint8_t { lre, rle, lro, rlo, lri, rli, fsi };

    std::vector<std::pair<open_code, source_location>> open_codes_;
    std::vector<diagnostic_box> diags_;

  public:
    comment_bidi_tracker() noexcept = default;

    /** @brief Updates the state upon seeing a new code point.
     * @throws std::bad_alloc
     * @throws std::length_error */
    void feed(char32_t, source_location);

    /** @brief Updates the state upon seeing the end of a comment.
     *
     * This processing actually needs to be done at the end of every line of the
     * comment, so this is called internally after every line-breaking code
     * point.
     *
     * @throws std::bad_alloc
     * @throws std::length_error */
    void end();

    /** @brief Called after processing the comment to retrieve the collected
     * diagnostics.
     *
     * After this is called, the tracker cannot be used anymore and must be
     * discarded. As a reminder of this fact, this can only be called on an
     * rvalue. */
    [[nodiscard]] std::vector<diagnostic_box> get_diags() && noexcept {
      return std::move(diags_);
    }
  };

  struct state_in_comment_begin {
    nycleus::source_location loc;

    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(
      char32_t,
      source_location,
      lexer&
    );
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed_eof(lexer&);
    [[noreturn]] static token get_token() noexcept { util::unreachable(); }
  };

  struct state_in_comment_sl {
    std::u8string text;
    nycleus::source_location loc;

    comment_bidi_tracker bidi_tracker{};

    enum class substate_t { init, seen_cr, seen_end };
    substate_t substate;

    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(
      char32_t,
      source_location,
      lexer&
    );
    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed_eof() noexcept;
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] token get_token(lexer&);
  };

  struct state_in_comment_ml {
    std::u8string text;
    nycleus::source_location loc;

    comment_bidi_tracker bidi_tracker{};

    enum class substate_t { init, seen_star, seen_end };
    substate_t substate{substate_t::init};

    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(
      char32_t,
      source_location,
      lexer&
    );
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed_eof();
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] token get_token(lexer&);
  };

  struct state_in_comment_mlnest {
    std::u8string text;
    nycleus::source_location loc;
    std::uint64_t nesting{0};

    comment_bidi_tracker bidi_tracker{};

    enum class substate_t { init, seen_plus, seen_hash, seen_end };
    substate_t substate{substate_t::init};

    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(
      char32_t,
      source_location,
      lexer&
    );
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed_eof();
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] token get_token(lexer&);
  };

  /** @brief Information about an escape code, used during string and character
   * literal processing. */
  struct escape_code_info {
    /** @brief The source location of the backslash code point of the escape
     * code.
     *
     * Used as the diagnostic location if the code point is changed by
     * normalization. */
    source_location slash_loc;

    /** @brief The source location of the entire escape code.
     *
     * Used as the diagnostic location if the code point is not changed by
     * normalization. */
    source_location code_loc;

    /** @brief The original (un-normalized) code points comprising the escape
     * code, not including the backslash.
     *
     * This is used to check, after normalization, whether the escape code has
     * been changed by the normalization.
     *
     * This field is also used to store the number of code points that need to
     * be erased from the string when replacing escape codes with their values.
     * See comments in state_after_string::get_token for more info. */
    std::u32string code;
  };

  struct state_in_string {
    std::u32string buf;
    source_location loc;

    /** @brief Information about the escape codes in this literal. */
    std::vector<escape_code_info> escape_codes{};

    /** @brief The literal's associated diagnostics. */
    std::vector<diagnostic_box> diags{};

    /** @brief Whether this is a character literal.
     *
     * If false, this is a string literal. */
    bool is_char;

    /** @brief The state of the escape code currently processed.
     *
     * - If this is zero, we are not in an escape code.
     * - If this is the special value `escape_state_pending`, we've just seen
     *   the backslash and are looking for the code point indicating the escape
     *   code's kind.
     * - If this is any other value, this is the number of remaining code points
     *   in the current Unicode escape code.
     *
     * This is used while initially building the literal, to construct the
     * source locations of the escape codes, for later use in diagnostics. This
     * is not used to actually parse escape code values, since escape codes can
     * change after normalization. */
    std::uint8_t escape_state{0};
    static constexpr std::uint8_t escape_state_pending{255};

    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(
      char32_t,
      source_location
    );
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed_eof() noexcept;
    [[noreturn]] static token get_token() noexcept { util::unreachable(); }
  };

  struct state_after_string {
    std::u32string buf;
    source_location loc;

    /** @brief Information about the escape codes in this literal. */
    std::vector<escape_code_info> escape_codes;

    /** @brief The literal's associated diagnostics. */
    std::vector<diagnostic_box> diags{};

    /** @brief Whether this is a character literal.
     *
     * If false, this is a string literal. */
    bool is_char;

    /** @brief Whether this literal has been terminated.
     *
     * If false, we've reached the end of the file without seeing the
     * terminator. */
    bool is_terminated;

    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed(char32_t,
        source_location) noexcept;
    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed_eof() noexcept;
    /** @throws std::bad_alloc
     * @throws std::length_error */
    token get_token();
  };

  template<typename Token>
  struct state_punct_base {
    source_location loc;

    using base = state_punct_base;
    explicit state_punct_base(source_location l) noexcept:
      loc{std::move(l)} {}

    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t, source_location)
      noexcept;
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed_eof() noexcept;
    [[nodiscard]] token get_token() noexcept {
      return {Token{}, std::move(loc)};
    }
  };

  template<typename State>
  [[nodiscard]] static State make_punct_state(source_location start) noexcept {
    return State{typename State::base{std::move(start)}};
  }

  struct state_seen_excl: state_punct_base<tok::excl> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_neq: state_punct_base<tok::neq> {};
  struct state_seen_percent: state_punct_base<tok::percent> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_percent_assign: state_punct_base<tok::percent_assign> {};
  struct state_seen_amp: state_punct_base<tok::amp> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };

  struct state_seen_logical_and: state_punct_base<tok::logical_and> {
    source_location first_amp_loc;
    source_location second_amp_loc;
    [[nodiscard]] token get_token() noexcept;
  };

  struct state_seen_amp_assign: state_punct_base<tok::amp_assign> {};
  struct state_seen_lparen: state_punct_base<tok::lparen> {};
  struct state_seen_rparen: state_punct_base<tok::rparen> {};
  struct state_seen_star: state_punct_base<tok::star> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_star_assign: state_punct_base<tok::star_assign> {};
  struct state_seen_plus: state_punct_base<tok::plus> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_incr: state_punct_base<tok::incr> {};
  struct state_seen_plus_assign: state_punct_base<tok::plus_assign> {};
  struct state_seen_comma: state_punct_base<tok::comma> {};
  struct state_seen_minus: state_punct_base<tok::minus> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_decr: state_punct_base<tok::decr> {};
  struct state_seen_minus_assign: state_punct_base<tok::minus_assign> {};
  struct state_seen_arrow: state_punct_base<tok::arrow> {};
  struct state_seen_point: state_punct_base<tok::point> {
    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location);
  };
  struct state_seen_double_point: state_punct_base<tok::double_point> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_ellipsis: state_punct_base<tok::ellipsis> {};
  struct state_seen_slash: state_punct_base<tok::slash> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_slash_assign: state_punct_base<tok::slash_assign> {};
  struct state_seen_colon: state_punct_base<tok::colon> {};
  struct state_seen_semicolon: state_punct_base<tok::semicolon> {};
  struct state_seen_lt: state_punct_base<tok::lt> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_lshift: state_punct_base<tok::lshift> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_lshift_assign: state_punct_base<tok::lshift_assign> {};
  struct state_seen_lrot: state_punct_base<tok::lrot> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_lrot_assign: state_punct_base<tok::lrot_assign> {};
  struct state_seen_leq: state_punct_base<tok::leq> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_three_way_cmp: state_punct_base<tok::three_way_cmp> {};
  struct state_seen_assign: state_punct_base<tok::assign> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_eq: state_punct_base<tok::eq> {};
  struct state_seen_fat_arrow: state_punct_base<tok::fat_arrow> {};
  struct state_seen_gt: state_punct_base<tok::gt> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t ch,
      source_location new_loc) noexcept;
  };
  struct state_seen_rrot: state_punct_base<tok::rrot> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_rrot_assign: state_punct_base<tok::rrot_assign> {};
  struct state_seen_geq: state_punct_base<tok::geq> {};
  struct state_seen_rshift: state_punct_base<tok::rshift> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_rshift_assign: state_punct_base<tok::rshift_assign> {};
  struct state_seen_question: state_punct_base<tok::question> {};
  struct state_seen_at: state_punct_base<tok::at> {};
  struct state_seen_lsquare: state_punct_base<tok::lsquare> {};
  struct state_seen_rsquare: state_punct_base<tok::rsquare> {};
  struct state_seen_bitwise_xor: state_punct_base<tok::bitwise_xor> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_bitwise_xor_assign:
    state_punct_base<tok::bitwise_xor_assign> {};
  struct state_seen_logical_xor: state_punct_base<tok::logical_xor> {};
  struct state_seen_lbrace: state_punct_base<tok::lbrace> {};
  struct state_seen_pipe: state_punct_base<tok::pipe> {
    [[nodiscard]] std::pair<bool, std::optional<state_t>> feed(char32_t,
      source_location) noexcept;
  };
  struct state_seen_pipe_assign:
    state_punct_base<tok::pipe_assign> {};
  struct state_seen_subst: state_punct_base<tok::subst> {};
  struct state_seen_logical_or: state_punct_base<tok::logical_or> {};
  struct state_seen_rbrace: state_punct_base<tok::rbrace> {};
  struct state_seen_tilde: state_punct_base<tok::tilde> {};

  struct state_error {
    std::vector<diagnostic_box> diags;
    source_location range;

    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed(char32_t,
        source_location) noexcept;
    [[nodiscard]] static std::pair<bool, std::optional<state_t>> feed_eof() noexcept;
    /** @throws std::bad_alloc */
    [[nodiscard]] token get_token();
  };

  state_t state_{state_init{}};

  /** @brief Whether comment tokens should contain comment text.
   *
   * If the comments are going to be immediately dismissed, there is no reason
   * to store their contents. */
  bool keep_comments_;

  /** @brief Whether the given code point begins a punctuation token. */
  [[nodiscard]] static bool is_punct(char32_t) noexcept;

  /** @brief Whether the given code point is followed by a mandatory line break.
   *
   * The carriage return (U+000D) code point is not included. */
  [[nodiscard]] static bool is_line_end(char32_t) noexcept;

  /** @brief Determine the state of the lexer after reading the given
   * punctuation code point.
   *
   * If the code point is not a punctuation code point, returns state_init. */
  [[nodiscard]] static state_t get_punct_state(
    char32_t,
    source_location
  ) noexcept;

public:
  explicit lexer(bool keep_comments) noexcept:
    keep_comments_{keep_comments} {}

  /** @brief Supplies the lexer with another code point.
   *
   * Cannot be called after another call to `feed` returned true, or after
   * `feed_eof` has been called.
   *
   * @returns Whether a new token can be produced.
   * @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] bool feed(char32_t, source_location);

  /** @brief Indicated to the lexer that the end of the code point sequence has
   * been reached.
   *
   * Cannot be called after another call to `feed` returned true, or after
   * `feed_eof` has been called.
   *
   * @returns Whether a new token can be produced.
   * @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] bool feed_eof();

  /** @brief Retrieves the token that has been produced.
   *
   * Can only be called once. Cannot be called until a call to either `feed` or
   * `feed_eof` has returned true.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] token get_token();
};

std::pair<bool, std::optional<lexer::state_t>>
inline lexer::state_in_word::feed_eof() noexcept {
  return {true, std::nullopt};
}

std::pair<bool, std::optional<lexer::state_t>>
inline lexer::state_in_num::feed_eof() noexcept {
  return {true, std::nullopt};
}

std::pair<bool, std::optional<lexer::state_t>>
inline lexer::state_in_comment_sl::feed_eof() noexcept {
  return {true, std::nullopt};
}

std::pair<bool, std::optional<lexer::state_t>>
inline lexer::state_after_string::feed(char32_t, source_location) noexcept {
  return {true, std::nullopt};
}

std::pair<bool, std::optional<lexer::state_t>>
inline lexer::state_after_string::feed_eof() noexcept {
  return {true, std::nullopt};
}

template<typename Token>
std::pair<bool, std::optional<lexer::state_t>>
lexer::state_punct_base<Token>::feed(
  char32_t,
  [[maybe_unused]] source_location new_loc
) noexcept {
  assert((!loc.file && !new_loc.file)
    || (loc.file && new_loc.file && **loc.file == **new_loc.file));
  return {true, std::nullopt};
}

template<typename Token>
std::pair<bool, std::optional<lexer::state_t>>
lexer::state_punct_base<Token>::feed_eof() noexcept {
  return {true, std::nullopt};
}

std::pair<bool, std::optional<lexer::state_t>>
inline lexer::state_error::feed(char32_t, source_location) noexcept {
  return {true, std::nullopt};
}

std::pair<bool, std::optional<lexer::state_t>>
inline lexer::state_error::feed_eof() noexcept {
  return {true, std::nullopt};
}

////////////////////////////////////////////////////////////////////////////////

/** @brief This view adapts an underlying range of pairs of code points and
 * source locations into a lexed range of tokens. */
template<std::ranges::view View>
requires std::ranges::input_range<View>
  && std::same_as<std::ranges::range_value_t<View>, counted_char32_t>
class lexer_view: public std::ranges::view_interface<lexer_view<View>> {
private:
  using base_iterator = std::ranges::iterator_t<View>;
  using base_sentinel = std::ranges::sentinel_t<View>;

  struct iterator_impl {
    base_iterator it;
    std::optional<token> tok;
    bool keep_comments;

    iterator_impl() = default;

    explicit iterator_impl(View& view, bool keep_comments):
        it{std::ranges::begin(view)}, tok{std::nullopt},
        keep_comments{keep_comments} {
      this->increment(std::ranges::end(view));
    }

    iterator_impl(iterator_impl const&) = default;
    iterator_impl(iterator_impl&&) = default;

    iterator_impl& operator=(iterator_impl const& other) {
      iterator_impl copy{other};
      this->swap(copy);
      return *this;
    }

    iterator_impl& operator=(iterator_impl&& other)
        noexcept(std::is_nothrow_move_assignable_v<base_iterator>
        || std::is_nothrow_move_constructible_v<base_iterator>
        && std::is_nothrow_swappable_v<base_iterator>) {
      if constexpr(std::is_nothrow_move_assignable_v<base_iterator>) {
        it = std::move(other.it);
        tok = std::move(other.tok);
        keep_comments = other.keep_comments;
      } else {
        iterator_impl copy{std::move(other)};
        this->swap(copy);
      }
      return *this;
    }

    ~iterator_impl() = default;

    void swap(iterator_impl& other)
        noexcept(std::is_nothrow_swappable_v<base_iterator>) {
      using std::swap;
      swap(it, other.it);
      swap(tok, other.tok);
      swap(keep_comments, other.keep_comments);
    }

    void increment(base_sentinel iend) {
      lexer l{keep_comments};
      while(it != iend) {
        auto [ch, loc] = *it;
        if(l.feed(ch, std::move(loc))) {
          tok = l.get_token();
          return;
        }
        ++it;
      }

      if(l.feed_eof()) {
        tok = l.get_token();
      } else {
        tok.reset();
      }
    }
  };

  class iterator_core {
  private:
    base_sentinel iend_;
    iterator_impl impl_;

    friend class lexer_view;
    explicit iterator_core(
      base_sentinel iend,
      iterator_impl impl
    ) noexcept(
      std::is_nothrow_constructible_v<base_iterator>
      && std::is_nothrow_constructible_v<base_sentinel>
    ):
      iend_{std::move(iend)},
      impl_{std::move(impl)} {}

  public:
    iterator_core() = default;
    iterator_core(iterator_core const&) = default;
    iterator_core(iterator_core&&) = default;

    iterator_core& operator=(iterator_core const& other) {
      iterator_core copy{other};
      this->swap(copy);
      return *this;
    }

    iterator_core& operator=(iterator_core&& other)
        noexcept(std::is_nothrow_move_assignable_v<base_sentinel>
        && std::is_nothrow_move_assignable_v<iterator_impl>
        || std::is_nothrow_move_constructible_v<base_sentinel>
        && std::is_nothrow_move_constructible_v<iterator_impl>
        && std::is_nothrow_swappable_v<base_sentinel>
        && std::is_nothrow_swappable_v<iterator_impl>) {
      if constexpr(std::is_nothrow_move_assignable_v<base_sentinel>
          && std::is_nothrow_move_assignable_v<iterator_impl>) {
        iend_ = std::move(other.iend_);
        impl_ = std::move(other.impl_);
      } else {
        iterator_core copy{std::move(other)};
        this->swap(copy);
      }
      return *this;
    }

    ~iterator_core() = default;

    void swap(iterator_core& other)
        noexcept(std::is_nothrow_swappable_v<base_sentinel>
        && std::is_nothrow_swappable_v<iterator_impl>) {
      using std::swap;
      swap(iend_, other.iend_);
      impl_.swap(other.impl_);
    }

  protected:
    static constexpr bool is_single_pass{!std::ranges::forward_range<View>};

    [[nodiscard]] token const& dereference() const noexcept {
      assert(impl_.tok.has_value());
      return *impl_.tok;
    }

    void increment() {
      impl_.increment(iend_);
    }

    /** @throws std::bad_alloc */
    [[nodiscard]] auto sp_postfix_increment() {
      class proxy {
      private:
        token tok_;

      public:
        explicit proxy(token tok) noexcept: tok_{std::move(tok)} {}

        [[nodiscard]] token operator*() && { return std::move(tok_); }
      };

      proxy result{dereference()};
      increment();
      return result;
    }

    [[nodiscard]] bool equal_to(iterator_core const& other) const
        noexcept(noexcept(impl_.it == other.impl_.it))
        requires std::sentinel_for<base_iterator, base_iterator> {
      return impl_.it == other.impl_.it && impl_.tok == other.impl_.tok;
    }

    [[nodiscard]] bool equal_to(std::default_sentinel_t) const noexcept {
      return !impl_.tok;
    }
  };

public:
  using iterator = util::iterator_facade<iterator_core>;

private:
  View view_;
  iterator_impl cache_;
  bool keep_comments_;

public:
  explicit lexer_view(View view, bool keep_comments = false)
    noexcept(std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{std::move(view)}, cache_{}, keep_comments_{keep_comments} {}

  lexer_view() = default;

  lexer_view(lexer_view const& other)
    noexcept(std::is_nothrow_copy_constructible_v<View>):
    view_{other.view_}, cache_{} {}

  lexer_view(lexer_view&& other)
      noexcept(std::is_nothrow_move_constructible_v<View>):
      view_{std::move(other.view_)}, cache_{} {
    other.cache_.tok.reset();
  }

  lexer_view& operator=(lexer_view const& other)
      noexcept(std::is_nothrow_copy_assignable_v<View>) {
    cache_.tok.reset();
    view_ = other.view_;
    return *this;
  }

  lexer_view& operator=(lexer_view&& other)
      noexcept(std::is_nothrow_move_assignable_v<View>) {
    cache_.tok.reset();
    other.cache_.tok.reset();
    view_ = std::move(other.view_);
    keep_comments_ = other.keep_comments_;
    return *this;
  }

  ~lexer_view() = default;

  [[nodiscard]] iterator begin() {
    if(!cache_.tok) {
      cache_ = iterator_impl{view_, keep_comments_};
    }
    iterator it{std::ranges::end(view_), cache_};
    return it;
  }

  [[nodiscard]] std::default_sentinel_t end() const noexcept {
    return std::default_sentinel;
  }

  [[nodiscard]] View& base() noexcept {
    return view_;
  }

  [[nodiscard]] View const& base() const noexcept {
    return view_;
  }
};

template<typename View>
void swap(
  typename lexer_view<View>::iterator_impl& a,
  typename lexer_view<View>::iterator_impl& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

template<typename View>
void swap(
  typename lexer_view<View>::iterator_core& a,
  typename lexer_view<View>::iterator_core& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

} // nycleus

template<typename View>
inline constexpr bool std::ranges::enable_borrowed_range<
  ::nycleus::lexer_view<View>>{::std::ranges::borrowed_range<View>};

namespace nycleus {

template<typename Range>
requires std::ranges::viewable_range<Range&&>
  && std::same_as<counted_char32_t, std::ranges::range_value_t<Range&&>>
[[nodiscard]] auto lex(Range&& range, bool keep_comments = false) noexcept(
  std::is_nothrow_move_constructible_v<std::views::all_t<Range&&>&>
  && noexcept(std::ranges::begin(std::declval<std::views::all_t<Range&&>&>()))
) {
  return lexer_view{std::views::all(std::forward<Range>(range)),
    keep_comments};
}

namespace detail_ {

class lex_adaptor: public util::range_adaptor_closure<lex_adaptor> {
private:
  bool keep_comments_;

public:
  explicit lex_adaptor(bool keep_comments) noexcept:
    keep_comments_{keep_comments} {}

  template<typename Range>
  requires std::ranges::viewable_range<Range&&>
    && std::same_as<counted_char32_t, std::ranges::range_value_t<Range&&>>
  [[nodiscard]] auto operator()(Range&& range)
      noexcept(std::is_nothrow_move_constructible_v<
      std::views::all_t<Range&&>&> && noexcept(std::ranges::begin(
      std::declval<std::views::all_t<Range&&>&>()))) {
    return lexer_view{std::views::all(std::forward<Range>(range)),
      keep_comments_};
  }
};

} // detail_

inline auto lex(bool keep_comments = false) noexcept {
  return detail_::lex_adaptor{keep_comments};
}

} // nycleus

#endif
