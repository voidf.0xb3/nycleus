#include "nycleus/diagnostic_logger.hpp"
#include "nycleus/diagnostic.hpp"
#include "util/u8compat.hpp"
#include <gsl/gsl>
#include <cassert>
#include <memory>
#include <utility>
#include <vector>

using namespace util::u8compat_literals;

gsl::czstring nycleus::too_many_errors::what() const noexcept {
  return u8"Nycleus: too many errors"_ch;
}

std::vector<std::unique_ptr<nycleus::diagnostic>>
    nycleus::diagnostic_logger::extract_diags() && {
  auto body{body_.unlocked()};
  std::vector<std::unique_ptr<diagnostic>> result{std::move(body->diags)};

  assert(body->error_limit == 0 || result.size() == body->error_count);
  return result;
}

void nycleus::diagnostic_logger::do_track(
  std::unique_ptr<nycleus::diagnostic> diag
) {
  auto body{body_.lock()};
  body->diags.push_back(std::move(diag));
  if(body->error_limit > 0 && ++body->error_count >= body->error_limit) {
    throw too_many_errors{};
  }
}
