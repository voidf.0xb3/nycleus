#include <boost/test/unit_test.hpp>

#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_logger.hpp"
#include "nycleus/diagnostic_tools.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/entity_bag.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/hlir_tools.hpp"
#include "nycleus/lexer.hpp"
#include "nycleus/parser.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/utils.hpp"
#include "unicode/encoding.hpp"
#include "util/cow.hpp"
#include "util/downcast.hpp"
#include "test_tools.hpp"
#include <atomic>
#include <concepts>
#include <filesystem>
#include <memory>
#include <optional>
#include <ranges>
#include <string>
#include <utility>

namespace nycleus {
  class diagnostic_tracker;
}

namespace diag = nycleus::diag;
namespace hlir = nycleus::hlir;

namespace {

using nycleus::diag_ptr;
using nycleus::hlir_ptr;
using nycleus::int_width_t;
using in_function = nycleus::global_name::in_function;
using in_class = nycleus::global_name::in_class;

struct entity_bag_fixture {
  std::optional<util::cow<std::filesystem::path>> f{"test.ny"};
  nycleus::diagnostic_logger dt;
  nycleus::entity_bag bag;

  void fill_bag(std::u8string text) {
    nycleus::parse(
      text | unicode::decode(nycleus::utf8_counted{std::move(f)},
        unicode::eh_throw{}) | nycleus::lex(),
      [&](std::u8string name, hlir_ptr<hlir::def> def) {
        bag.process_global_def(std::move(def), {std::move(name)}, dt);
      },
      dt
    );
  }
};

using src_loc = nycleus::source_location;

template<nycleus::hlir_type T, typename... Args>
requires std::constructible_from<T, Args...>
[[nodiscard]] auto make(Args&&... args) {
  return nycleus::make_hlir<T>(std::forward<Args>(args)...);
}

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_nycleus)
BOOST_FIXTURE_TEST_SUITE(entity_bag, entity_bag_fixture)

// Functions ///////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(function)

BOOST_AUTO_TEST_CASE(basic) {
  fill_bag(u8"fn foo(x: s64, y: bool): u8 {}");

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.params.size() == 2);

  BOOST_TEST((fn.params[0].core_loc == src_loc{f, {1, 8}, {1, 9}}));
  BOOST_REQUIRE(fn.params[0].var);
  auto const& param1_var{*fn.params[0].var};
  BOOST_TEST(param1_var.fn == &fn);
  BOOST_TEST(!param1_var.is_mutable);

  auto const param1_type{make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    true
  )};
  BOOST_TEST(param1_var.type.assume_hlir() == *param1_type);

  BOOST_TEST((fn.params[1].core_loc == src_loc{f, {1, 16}, {1, 17}}));
  BOOST_REQUIRE(fn.params[1].var);
  auto const& param2_var{*fn.params[1].var};
  BOOST_TEST(param2_var.fn == &fn);
  BOOST_TEST(!param2_var.is_mutable);

  auto const param2_type{make<hlir::bool_type>(src_loc{f, {1, 19}, {1, 23}})};
  BOOST_TEST(param2_var.type.assume_hlir() == *param2_type);

  auto const return_type{make<hlir::int_type>(src_loc{f, {1, 26}, {1, 28}},
    int_width_t{8},
    false
  )};
  BOOST_TEST(fn.return_type.assume_hlir() == *return_type);

  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make<hlir::block>(src_loc{f, {1, 29}, {1, 31}})};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(no_return) {
  fill_bag(u8"fn foo(x: s64, y: bool) {}");

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{{u8"foo"}}));
  BOOST_TEST((fn.core_loc == (src_loc{f, {1, 4}, {1, 7}})));

  BOOST_REQUIRE(fn.params.size() == 2);

  BOOST_TEST((fn.params[0].core_loc == (src_loc{f, {1, 8}, {1, 9}})));
  BOOST_REQUIRE(fn.params[0].var);
  auto const& param1_var{*fn.params[0].var};
  BOOST_TEST(param1_var.fn == &fn);
  BOOST_TEST(!param1_var.is_mutable);

  auto const param1_type{make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    true
  )};
  BOOST_TEST(param1_var.type.assume_hlir() == *param1_type);

  BOOST_TEST((fn.params[1].core_loc == (src_loc{f, {1, 16}, {1, 17}})));
  BOOST_REQUIRE(fn.params[1].var);
  auto const& param2_var{*fn.params[1].var};
  BOOST_TEST(param2_var.fn == &fn);
  BOOST_TEST(!param2_var.is_mutable);

  auto const param2_type{make<hlir::bool_type>(src_loc{f, {1, 19}, {1, 23}})};
  BOOST_TEST(param2_var.type.assume_hlir() == *param2_type);

  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make<hlir::block>(src_loc{f, {1, 25}, {1, 27}})};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(nameless_param) {
  fill_bag(u8"fn foo(:s64, y: bool) {}");

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{{u8"foo"}}));
  BOOST_TEST((fn.core_loc == (src_loc{f, {1, 4}, {1, 7}})));

  BOOST_REQUIRE(fn.params.size() == 2);

  BOOST_TEST((fn.params[0].core_loc == (src_loc{f, {1, 8}, {1, 12}})));
  BOOST_REQUIRE(fn.params[0].var);
  auto const& param1_var{*fn.params[0].var};
  BOOST_TEST(param1_var.fn == &fn);
  BOOST_TEST(!param1_var.is_mutable);

  auto const param1_type{make<hlir::int_type>(src_loc{f, {1, 9}, {1, 12}},
    int_width_t{64},
    true
  )};
  BOOST_TEST(param1_var.type.assume_hlir() == *param1_type);

  BOOST_TEST((fn.params[1].core_loc == (src_loc{f, {1, 14}, {1, 15}})));
  BOOST_REQUIRE(fn.params[1].var);
  auto const& param2_var{*fn.params[1].var};
  BOOST_TEST(param2_var.fn == &fn);
  BOOST_TEST(!param2_var.is_mutable);

  auto const param2_type{make<hlir::bool_type>(src_loc{f, {1, 17}, {1, 21}})};
  BOOST_TEST(param2_var.type.assume_hlir() == *param2_type);

  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make<hlir::block>(src_loc{f, {1, 23}, {1, 25}})};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_SUITE(params)

BOOST_AUTO_TEST_CASE(basic) {
  fill_bag(
    u8"fn foo(x: u64) {\n"
    u8"\t$x;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.params.size() == 1);

  BOOST_TEST((fn.params[0].core_loc == src_loc{f, {1, 8}, {1, 9}}));
  BOOST_REQUIRE(fn.params[0].var);
  auto& param_var{*fn.params[0].var};
  BOOST_TEST(param_var.fn == &fn);
  BOOST_TEST(!param_var.is_mutable);

  auto const param_type{make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param_var.type.assume_hlir() == *param_type);

  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make<hlir::block>(src_loc{f, {1, 16}, {3, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {2, 2}, {2, 4}},
        param_var
      ))
    ),
    nullptr
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(multiple) {
  fill_bag(
    u8"fn foo(x: u64, y: bool) {\n"
    u8"\t$x;\n"
    u8"\t$y;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.params.size() == 2);

  BOOST_TEST((fn.params[0].core_loc == src_loc{f, {1, 8}, {1, 9}}));
  BOOST_REQUIRE(fn.params[0].var);
  auto& param1_var{*fn.params[0].var};
  BOOST_TEST(param1_var.fn == &fn);
  BOOST_TEST(!param1_var.is_mutable);

  auto const param1_type{make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param1_var.type.assume_hlir() == *param1_type);

  BOOST_TEST((fn.params[1].core_loc == src_loc{f, {1, 16}, {1, 17}}));
  BOOST_REQUIRE(fn.params[1].var);
  auto& param2_var{*fn.params[1].var};
  BOOST_TEST(param2_var.fn == &fn);
  BOOST_TEST(!param2_var.is_mutable);

  auto const param2_type{make<hlir::bool_type>(src_loc{f, {1, 19}, {1, 23}})};
  BOOST_TEST(param2_var.type.assume_hlir() == *param2_type);

  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto body{make<hlir::block>(src_loc{f, {1, 25}, {4, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {2, 2}, {2, 4}},
        param1_var
      )),
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {3, 2}, {3, 4}},
        param2_var
      ))
    ),
    nullptr
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(inherit) {
  fill_bag(
    u8"fn foo(x: u64) {\n"
    u8"\t{\n"
    u8"\t\t$x;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.params.size() == 1);

  BOOST_TEST((fn.params[0].core_loc == src_loc{f, {1, 8}, {1, 9}}));
  BOOST_REQUIRE(fn.params[0].var);
  auto& param_var{*fn.params[0].var};
  BOOST_TEST(param_var.fn == &fn);
  BOOST_TEST(!param_var.is_mutable);

  auto const param_type{make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param_var.type.assume_hlir() == *param_type);

  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make<hlir::block>(src_loc{f, {1, 16}, {5, 2}},
    std::vector<hlir_ptr<hlir::stmt>>{},
    make<hlir::block>(src_loc{f, {2, 2}, {4, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::expr_stmt>(make<hlir::local_var_ref>(
          src_loc{f, {3, 3}, {3, 5}},
          param_var
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(duplicate) {
  fill_bag(u8"fn foo(x: u64, x: bool) {}");

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::duplicate_param_name>(u8"x",
      test::make_vector<src_loc>(src_loc{f, {1, 16}, {1, 17}}))
  )));

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.params.size() == 2);

  BOOST_TEST((fn.params[0].core_loc == src_loc{f, {1, 8}, {1, 9}}));
  BOOST_REQUIRE(fn.params[0].var);
  auto& param1_var{*fn.params[0].var};
  BOOST_TEST(param1_var.fn == &fn);
  BOOST_TEST(!param1_var.is_mutable);

  auto const param1_type{make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param1_var.type.assume_hlir() == *param1_type);

  BOOST_TEST((fn.params[1].core_loc == src_loc{f, {1, 16}, {1, 17}}));
  BOOST_REQUIRE(fn.params[1].var);
  auto& param2_var{*fn.params[1].var};
  BOOST_TEST(param2_var.fn == &fn);
  BOOST_TEST(!param2_var.is_mutable);

  auto const param2_type{make<hlir::bool_type>(src_loc{f, {1, 19}, {1, 23}})};
  BOOST_TEST(param2_var.type.assume_hlir() == *param2_type);

  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make<hlir::block>(src_loc{f, {1, 25}, {1, 27}})};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_SUITE_END() // params

BOOST_AUTO_TEST_SUITE(locals)

BOOST_AUTO_TEST_CASE(basic) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet x: u64 = 0;\n"
    u8"\t$x;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.local_vars.size() == 1);

  BOOST_REQUIRE(fn.local_vars[0].get());
  auto& local{*fn.local_vars[0]};
  BOOST_TEST(local.fn == &fn);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make_hlir<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {4, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local,
        make<hlir::int_literal>(src_loc{f, {2, 15}, {2, 16}}, 0)
      ),
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {3, 2}, {3, 4}},
        local
      ))
    ),
    nullptr
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(with_mut) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet x mut: u64 = 0;\n"
    u8"\t$x;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.local_vars.size() == 1);

  BOOST_REQUIRE(fn.local_vars[0].get());
  auto& local{*fn.local_vars[0]};
  BOOST_TEST(local.fn == &fn);
  BOOST_TEST(local.is_mutable);

  auto const local_type{make_hlir<hlir::int_type>(src_loc{f, {2, 13}, {2, 16}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {4, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local,
        make<hlir::int_literal>(src_loc{f, {2, 19}, {2, 20}}, 0)
      ),
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {3, 2}, {3, 4}},
        local
      ))
    ),
    nullptr
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(with_const) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet x const: u64 = 0;\n"
    u8"\t$x;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.local_vars.size() == 1);

  BOOST_REQUIRE(fn.local_vars[0].get());
  auto& local{*fn.local_vars[0]};
  BOOST_TEST(local.fn == &fn);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make_hlir<hlir::int_type>(src_loc{f, {2, 15}, {2, 18}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {4, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local,
        make<hlir::int_literal>(src_loc{f, {2, 21}, {2, 22}}, 0)
      ),
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {3, 2}, {3, 4}},
        local
      ))
    ),
    nullptr
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(inherit) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet x: u64 = 0;\n"
    u8"\t{\n"
    u8"\t\t$x;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.local_vars.size() == 1);

  BOOST_REQUIRE(fn.local_vars[0].get());
  auto& local{*fn.local_vars[0]};
  BOOST_TEST(local.fn == &fn);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make_hlir<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {6, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local,
        make<hlir::int_literal>(src_loc{f, {2, 15}, {2, 16}}, 0)
      )
    ),
    make<hlir::block>(src_loc{f, {3, 2}, {5, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::expr_stmt>(make<hlir::local_var_ref>(
          src_loc{f, {4, 3}, {4, 5}},
          local
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(shadowing) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet x: u64 = 0;\n"
    u8"\t$x;\n"
    u8"\t{\n"
    u8"\t\tlet x: s8 = 0;\n"
    u8"\t\t$x;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.local_vars.size() == 2);

  BOOST_REQUIRE(fn.local_vars[0].get());
  auto& local1{*fn.local_vars[0]};
  BOOST_TEST(local1.fn == &fn);
  BOOST_TEST(!local1.is_mutable);

  auto const local1_type{make_hlir<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local1.type.assume_hlir() == *local1_type);

  BOOST_REQUIRE(fn.local_vars[1].get());
  auto& local2{*fn.local_vars[1]};
  BOOST_TEST(local2.fn == &fn);
  BOOST_TEST(!local2.is_mutable);

  auto const local2_type{make_hlir<hlir::int_type>(src_loc{f, {5, 10}, {5, 12}},
    int_width_t{8},
    true
  )};
  BOOST_TEST(local2.type.assume_hlir() == *local2_type);

  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {8, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local1,
        make<hlir::int_literal>(src_loc{f, {2, 15}, {2, 16}}, 0)
      ),
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {3, 2}, {3, 4}},
        local1
      ))
    ),
    make<hlir::block>(src_loc{f, {4, 2}, {7, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::local_var_init_stmt>(
          local2,
          make<hlir::int_literal>(src_loc{f, {5, 15}, {5, 16}}, 0)
        ),
        make<hlir::expr_stmt>(make<hlir::local_var_ref>(
          src_loc{f, {6, 3}, {6, 5}},
          local2
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(shadowing_params) {
  fill_bag(
    u8"fn foo(x: u64) {\n"
    u8"\t$x;\n"
    u8"\t{\n"
    u8"\t\tlet x: s8 = 0;\n"
    u8"\t\t$x;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.params.size() == 1);

  BOOST_TEST((fn.params[0].core_loc == src_loc{f, {1, 8}, {1, 9}}));
  BOOST_REQUIRE(fn.params[0].var.get());
  auto& param{*fn.params[0].var};
  BOOST_TEST(param.fn == &fn);
  BOOST_TEST(!param.is_mutable);

  auto const param_type{make_hlir<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param.type.assume_hlir() == *param_type);

  BOOST_REQUIRE(fn.local_vars.size() == 1);

  BOOST_REQUIRE(fn.local_vars[0].get());
  auto& local{*fn.local_vars[0]};
  BOOST_TEST(local.fn == &fn);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make_hlir<hlir::int_type>(src_loc{f, {4, 10}, {4, 12}},
    int_width_t{8},
    true
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST(!fn.return_type);
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make_hlir<hlir::block>(src_loc{f, {1, 16}, {7, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {2, 2}, {2, 4}},
        param
      ))
    ),
    make<hlir::block>(src_loc{f, {3, 2}, {6, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::local_var_init_stmt>(
          local,
          make<hlir::int_literal>(src_loc{f, {4, 15}, {4, 16}}, 0)
        ),
        make<hlir::expr_stmt>(make<hlir::local_var_ref>(
          src_loc{f, {5, 3}, {5, 5}},
          local
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(duplicates) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet x: u64 = 0;\n"
    u8"\tlet x: s8 = 0;\n"
    u8"}"
  );

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::duplicate_name_in_block>(u8"x",
      std::vector{src_loc{f, {3, 6}, {3, 7}}})
  )));

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.local_vars.size() == 2);

  BOOST_REQUIRE(fn.local_vars[0].get());
  auto& local1{*fn.local_vars[0]};
  BOOST_TEST(local1.fn == &fn);
  BOOST_TEST(!local1.is_mutable);

  auto const local1_type{make_hlir<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local1.type.assume_hlir() == *local1_type);

  BOOST_REQUIRE(fn.local_vars[1].get());
  auto& local2{*fn.local_vars[1]};
  BOOST_TEST(local2.fn == &fn);
  BOOST_TEST(!local2.is_mutable);

  auto const local2_type{make_hlir<hlir::int_type>(src_loc{f, {3, 9}, {3, 11}},
    int_width_t{8},
    true
  )};
  BOOST_TEST(local2.type.assume_hlir() == *local2_type);

  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {4, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local1,
        make<hlir::int_literal>(src_loc{f, {2, 15}, {2, 16}}, 0)
      ),
      make<hlir::local_var_init_stmt>(
        local2,
        make<hlir::int_literal>(src_loc{f, {3, 14}, {3, 15}}, 0)
      )
    ),
    nullptr
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_CASE(duplicates_with_params) {
  fill_bag(
    u8"fn foo(x: u64) {\n"
    u8"\tlet x: s8 = 0;\n"
    u8"}"
  );

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::duplicate_name_in_block>(u8"x",
      std::vector{src_loc{f, {2, 6}, {2, 7}}})
  )));

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& fn{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};

  BOOST_TEST((fn.name == nycleus::global_name{u8"foo"}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 4}, {1, 7}}));

  BOOST_REQUIRE(fn.params.size() == 1);

  BOOST_TEST((fn.params[0].core_loc == src_loc{f, {1, 8}, {1, 9}}));
  BOOST_REQUIRE(fn.params[0].var.get());
  auto& param{*fn.params[0].var};
  BOOST_TEST(param.fn == &fn);
  BOOST_TEST(!param.is_mutable);

  auto const param_type{make_hlir<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param.type.assume_hlir() == *param_type);

  BOOST_REQUIRE(fn.local_vars.size() == 1);

  BOOST_REQUIRE(fn.local_vars[0].get());
  auto& local{*fn.local_vars[0]};
  BOOST_TEST(local.fn == &fn);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make_hlir<hlir::int_type>(src_loc{f, {2, 9}, {2, 11}},
    int_width_t{8},
    true
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST(!fn.return_type);
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!fn.initializee);

  auto const body{make_hlir<hlir::block>(src_loc{f, {1, 16}, {3, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local,
        make<hlir::int_literal>(src_loc{f, {2, 14}, {2, 15}}, 0)
      )
    ),
    nullptr
  )};
  BOOST_TEST(fn.body.get());
  BOOST_TEST(*fn.body == *body);
}

BOOST_AUTO_TEST_SUITE_END() // locals

BOOST_AUTO_TEST_SUITE(nested)

BOOST_AUTO_TEST_CASE(basic) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tfn bar() {}\n"
    u8"\t$bar;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.local_vars.empty());
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {4, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::expr_stmt>(make<hlir::function_ref>(src_loc{f, {3, 2}, {3, 6}},
        bar
      ))
    ),
    nullptr
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {2, 5}, {2, 8}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {2, 11}, {2, 13}})};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);
}

BOOST_AUTO_TEST_CASE(inherit) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tfn bar() {}\n"
    u8"\t{\n"
    u8"\t\t$bar;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.local_vars.empty());
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {6, 2}},
    std::vector<hlir_ptr<hlir::stmt>>{},
    make<hlir::block>(src_loc{f, {3, 2}, {5, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::expr_stmt>(make<hlir::function_ref>(
          src_loc{f, {4, 3}, {4, 7}},
          bar
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {2, 5}, {2, 8}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {2, 11}, {2, 13}})};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);
}

BOOST_AUTO_TEST_CASE(shadowing) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tfn bar() {}\n"
    u8"\t{\n"
    u8"\t\tfn bar() {}\n"
    u8"\t\t$bar;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 3);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar_outer{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};
  auto& bar_inner{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{1, u8""}}}))};

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.local_vars.empty());
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {7, 2}},
    std::vector<hlir_ptr<hlir::stmt>>{},
    make<hlir::block>(src_loc{f, {3, 2}, {6, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::expr_stmt>(make<hlir::function_ref>(
          src_loc{f, {5, 3}, {5, 7}},
          bar_inner
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar_outer.core_loc == src_loc{f, {2, 5}, {2, 8}}));
  BOOST_TEST(bar_outer.local_vars.empty());
  BOOST_TEST(bar_outer.params.empty());
  BOOST_TEST(!bar_outer.return_type);
  BOOST_TEST((bar_outer.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar_outer.initializee);

  auto const bar_outer_body{make_hlir<hlir::block>(
    src_loc{f, {2, 11}, {2, 13}})};
  BOOST_TEST(bar_outer.body.get());
  BOOST_TEST(*bar_outer.body == *bar_outer_body);

  BOOST_TEST((bar_inner.core_loc == src_loc{f, {4, 6}, {4, 9}}));
  BOOST_TEST(bar_inner.local_vars.empty());
  BOOST_TEST(bar_inner.params.empty());
  BOOST_TEST(!bar_inner.return_type);
  BOOST_TEST((bar_inner.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar_inner.initializee);

  auto const bar_inner_body{make_hlir<hlir::block>(
    src_loc{f, {4, 12}, {4, 14}})};
  BOOST_TEST(bar_inner.body.get());
  BOOST_TEST(*bar_inner.body == *bar_inner_body);
}

BOOST_AUTO_TEST_CASE(shadowing_locals) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet bar: u64 = 0;\n"
    u8"\t{\n"
    u8"\t\tfn bar() {}\n"
    u8"\t\t$bar;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_REQUIRE(foo.local_vars.size() == 1);
  BOOST_REQUIRE(foo.local_vars[0].get());
  auto& local{*foo.local_vars[0]};
  BOOST_TEST(local.fn == &foo);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make<hlir::int_type>(src_loc{f, {2, 11}, {2, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {7, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local,
        make<hlir::int_literal>(src_loc{f, {2, 17}, {2, 18}}, 0)
      )
    ),
    make<hlir::block>(src_loc{f, {3, 2}, {6, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::expr_stmt>(make<hlir::function_ref>(
          src_loc{f, {5, 3}, {5, 7}},
          bar
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {4, 6}, {4, 9}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {4, 12}, {4, 14}})};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);
}

BOOST_AUTO_TEST_CASE(shadowing_by_locals) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tfn bar() {}\n"
    u8"\t{\n"
    u8"\t\tlet bar: u64 = 0;\n"
    u8"\t\t$bar;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_REQUIRE(foo.local_vars.size() == 1);
  BOOST_REQUIRE(foo.local_vars[0].get());
  auto& local{*foo.local_vars[0]};
  BOOST_TEST(local.fn == &foo);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make<hlir::int_type>(src_loc{f, {4, 12}, {4, 15}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {7, 2}},
    std::vector<hlir_ptr<hlir::stmt>>{},
    make<hlir::block>(src_loc{f, {3, 2}, {6, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::local_var_init_stmt>(
          local,
          make<hlir::int_literal>(src_loc{f, {4, 18}, {4, 19}}, 0)
        ),
        make<hlir::expr_stmt>(make<hlir::local_var_ref>(
          src_loc{f, {5, 3}, {5, 7}},
          local
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {2, 5}, {2, 8}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {2, 11}, {2, 13}})};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);
}

BOOST_AUTO_TEST_CASE(shadowing_params) {
  fill_bag(
    u8"fn foo(bar: u64) {\n"
    u8"\t{\n"
    u8"\t\tfn bar() {}\n"
    u8"\t\t$bar;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_REQUIRE(foo.params.size() == 1);

  BOOST_TEST((foo.params[0].core_loc == src_loc{f, {1, 8}, {1, 11}}));
  BOOST_REQUIRE(foo.params[0].var.get());
  auto& param{*foo.params[0].var};
  BOOST_TEST(param.fn == &foo);
  BOOST_TEST(!param.is_mutable);

  auto const param_type{make<hlir::int_type>(src_loc{f, {1, 13}, {1, 16}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param.type.assume_hlir() == *param_type);

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.local_vars.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 18}, {6, 2}},
    std::vector<hlir_ptr<hlir::stmt>>{},
    make<hlir::block>(src_loc{f, {2, 2}, {5, 3}},
      test::make_vector<hlir_ptr<hlir::stmt>>(
        make<hlir::expr_stmt>(make<hlir::function_ref>(
          src_loc{f, {4, 3}, {4, 7}},
          bar
        ))
      ),
      nullptr
    )
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {3, 6}, {3, 9}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {3, 12}, {3, 14}})};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);
}

BOOST_AUTO_TEST_CASE(nested_from_nested) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tfn bar() {}\n"
    u8"\tfn qux() {\n"
    u8"\t\t$bar;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 3);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};
  auto& qux{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{1, u8""}}}))};

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.local_vars.empty());
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {6, 2}})};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {2, 5}, {2, 8}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {2, 11}, {2, 13}})};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);

  BOOST_TEST((qux.core_loc == src_loc{f, {3, 5}, {3, 8}}));
  BOOST_TEST(qux.local_vars.empty());
  BOOST_TEST(qux.params.empty());
  BOOST_TEST(!qux.return_type);
  BOOST_TEST((qux.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!qux.initializee);

  auto const qux_body{make_hlir<hlir::block>(src_loc{f, {3, 11}, {5, 3}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::expr_stmt>(make<hlir::function_ref>(src_loc{f, {4, 3}, {4, 7}},
        bar
      ))
    ),
    nullptr
  )};
  BOOST_TEST(qux.body.get());
  BOOST_TEST(*qux.body == *qux_body);
}

BOOST_AUTO_TEST_CASE(self_from_nested) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tfn bar() {\n"
    u8"\t\t$bar;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.local_vars.empty());
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {5, 2}})};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {2, 5}, {2, 8}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {2, 11}, {4, 3}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::expr_stmt>(make<hlir::function_ref>(src_loc{f, {3, 3}, {3, 7}},
        bar
      ))
    ),
    nullptr
  )};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);
}

BOOST_AUTO_TEST_CASE(locals_wrong_fn) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet x: u64 = 0;\n"
    u8"\tfn bar() {\n"
    u8"\t\t$x;\n"
    u8"\t}\n"
    u8"}"
  );

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_REQUIRE(foo.local_vars.size() == 1);

  BOOST_REQUIRE(foo.local_vars[0].get());
  auto& local{*foo.local_vars[0]};
  BOOST_TEST(local.fn == &foo);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make_hlir<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {6, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(
        local,
        make<hlir::int_literal>(src_loc{f, {2, 15}, {2, 16}}, 0)
      )
    ),
    nullptr
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {3, 5}, {3, 8}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto bar_body{make_hlir<hlir::block>(src_loc{f, {3, 11}, {5, 3}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::expr_stmt>(make<hlir::local_var_ref>(
        src_loc{f, {4, 3}, {4, 5}},
        local
      ))
    ),
    nullptr
  )};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::local_var_wrong_function>(diag_ptr<hlir::expr>{
      *util::downcast<hlir::expr_stmt&>(*bar.body->stmts[0]).content},
      local, bar)
  )));
}

BOOST_AUTO_TEST_CASE(duplicates) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tfn bar() {}\n"
    u8"\tfn bar() {}\n"
    u8"}"
  );

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::duplicate_name_in_block>(u8"bar",
      std::vector{src_loc{f, {3, 5}, {3, 8}}})
  )));

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 3);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar1{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};
  auto& bar2{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{1, u8""}}}))};

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.local_vars.empty());
  BOOST_TEST(foo.params.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {4, 2}})};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar1.core_loc == src_loc{f, {2, 5}, {2, 8}}));
  BOOST_TEST(bar1.local_vars.empty());
  BOOST_TEST(bar1.params.empty());
  BOOST_TEST(!bar1.return_type);
  BOOST_TEST((bar1.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar1.initializee);

  auto const bar1_body{make_hlir<hlir::block>(src_loc{f, {2, 11}, {2, 13}})};
  BOOST_TEST(bar1.body.get());
  BOOST_TEST(*bar1.body == *bar1_body);

  BOOST_TEST((bar2.core_loc == src_loc{f, {3, 5}, {3, 8}}));
  BOOST_TEST(bar2.local_vars.empty());
  BOOST_TEST(bar2.params.empty());
  BOOST_TEST(!bar2.return_type);
  BOOST_TEST((bar2.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar2.initializee);

  auto const bar2_body{make_hlir<hlir::block>(src_loc{f, {3, 11}, {3, 13}})};
  BOOST_TEST(bar2.body.get());
  BOOST_TEST(*bar2.body == *bar2_body);
}

BOOST_AUTO_TEST_CASE(duplicates_with_params) {
  fill_bag(
    u8"fn foo(bar: u64) {\n"
    u8"\tfn bar() {}\n"
    u8"}"
  );

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::duplicate_name_in_block>(u8"bar",
      std::vector{src_loc{f, {2, 5}, {2, 8}}})
  )));

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_REQUIRE(foo.params.size() == 1);

  BOOST_TEST((foo.params[0].core_loc == src_loc{f, {1, 8}, {1, 11}}));
  BOOST_REQUIRE(foo.params[0].var.get());
  auto const& param{*foo.params[0].var};
  BOOST_TEST(param.fn == &foo);
  BOOST_TEST(!param.is_mutable);

  auto const param_type{make<hlir::int_type>(src_loc{f, {1, 13}, {1, 16}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param.type.assume_hlir() == *param_type);

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(foo.local_vars.empty());
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 18}, {3, 2}})};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {2, 5}, {2, 8}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {2, 11}, {2, 13}})};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);
}

BOOST_AUTO_TEST_CASE(duplicates_with_locals) {
  fill_bag(
    u8"fn foo() {\n"
    u8"\tlet bar: u64 = 0;\n"
    u8"\tfn bar() {}\n"
    u8"}"
  );

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::duplicate_name_in_block>(u8"bar",
      std::vector{src_loc{f, {3, 5}, {3, 8}}})
  )));

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& bar{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_REQUIRE(foo.local_vars.size() == 1);

  BOOST_REQUIRE(foo.local_vars[0].get());
  auto& local{*foo.local_vars[0]};
  BOOST_TEST(local.fn == &foo);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make<hlir::int_type>(src_loc{f, {2, 11}, {2, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_TEST(foo.params.empty());
  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 10}, {4, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(local,
        make<hlir::int_literal>(src_loc{f, {2, 17}, {2, 18}}, 0)
      )
    ),
    nullptr
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((bar.core_loc == src_loc{f, {3, 5}, {3, 8}}));
  BOOST_TEST(bar.local_vars.empty());
  BOOST_TEST(bar.params.empty());
  BOOST_TEST(!bar.return_type);
  BOOST_TEST((bar.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!bar.initializee);

  auto const bar_body{make_hlir<hlir::block>(src_loc{f, {3, 11}, {3, 13}})};
  BOOST_TEST(bar.body.get());
  BOOST_TEST(*bar.body == *bar_body);
}

BOOST_AUTO_TEST_SUITE_END() // nested

BOOST_AUTO_TEST_CASE(global_refs) {
  fill_bag(
    u8"fn foo(x: u64) {\n"
    u8"\tlet y: u64 = 0;\n"
    u8"\tfn z() {\n"
    u8"\t\t$abc;\n"
    u8"\t}\n"
    u8"\t$def;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto& foo{*util::downcast<nycleus::function*>(bag.get({u8"foo"}))};
  auto& nested{*util::downcast<nycleus::function*>(
    bag.get({u8"foo", {in_function{0, u8""}}}))};

  BOOST_REQUIRE(foo.local_vars.size() == 1);

  BOOST_REQUIRE(foo.local_vars[0].get());
  auto& local{*foo.local_vars[0]};
  BOOST_TEST(local.fn == &foo);
  BOOST_TEST(!local.is_mutable);

  auto const local_type{make<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(local.type.assume_hlir() == *local_type);

  BOOST_REQUIRE(foo.params.size() == 1);

  BOOST_TEST((foo.params[0].core_loc == src_loc{f, {1, 8}, {1, 9}}));
  BOOST_REQUIRE(foo.params[0].var.get());
  auto& param{*foo.params[0].var};
  BOOST_TEST(param.fn == &foo);
  BOOST_TEST(!param.is_mutable);

  auto const param_type{make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(param.type.assume_hlir() == *param_type);

  BOOST_TEST((foo.core_loc == src_loc{f, {1, 4}, {1, 7}}));
  BOOST_TEST(!foo.return_type);
  BOOST_TEST((foo.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!foo.initializee);

  auto const foo_body{make_hlir<hlir::block>(src_loc{f, {1, 16}, {7, 2}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::local_var_init_stmt>(local,
        make<hlir::int_literal>(src_loc{f, {2, 15}, {2, 16}}, 0)
      ),
      make<hlir::expr_stmt>(make<hlir::named_var_ref>(
        src_loc{f, {6, 2}, {6, 6}},
        u8"def"
      ))
    ),
    nullptr
  )};
  BOOST_TEST(foo.body.get());
  BOOST_TEST(*foo.body == *foo_body);

  BOOST_TEST((nested.core_loc == src_loc{f, {3, 5}, {3, 6}}));
  BOOST_TEST(nested.local_vars.empty());
  BOOST_TEST(nested.params.empty());
  BOOST_TEST(!nested.return_type);
  BOOST_TEST((nested.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(!nested.initializee);

  auto const nested_body{make_hlir<hlir::block>(src_loc{f, {3, 9}, {5, 3}},
    test::make_vector<hlir_ptr<hlir::stmt>>(
      make<hlir::expr_stmt>(make<hlir::named_var_ref>(
        src_loc{f, {4, 3}, {4, 7}},
        u8"abc"
      ))
    ),
    nullptr
  )};
  BOOST_TEST(nested.body.get());
  BOOST_TEST(*nested.body == *nested_body);
}

BOOST_AUTO_TEST_SUITE_END() // function

// Global variables ////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(global_var)

BOOST_AUTO_TEST_CASE(basic) {
  fill_bag(u8"let x: u64 = 0;");

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto const& var{*util::downcast<nycleus::global_var*>(bag.get({u8"x"}))};
  auto const& fn{*util::downcast<nycleus::function*>(
    bag.get({u8"x", {}, true}))};

  BOOST_TEST((var.name == nycleus::global_name{{u8"x"}}));
  BOOST_TEST((var.core_loc == src_loc{f, {1, 5}, {1, 6}}));
  BOOST_TEST(!var.is_mutable);
  BOOST_TEST((var.processing_state.load(std::memory_order::relaxed)
    == nycleus::global_var::processing_state_t::initial));
  BOOST_TEST(var.initializer == &fn);

  auto const var_type{make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(var.type.assume_hlir() == *var_type);

  BOOST_TEST((fn.name == nycleus::global_name{u8"x", {}, true}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 14}, {1, 15}}));
  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(fn.initializee == &var);

  BOOST_REQUIRE(fn.body);
  auto& init_block{util::downcast<hlir::block const&>(*fn.body)};
  BOOST_TEST((init_block.loc == src_loc{f, {1, 14}, {1, 15}}));
  BOOST_TEST(init_block.stmts.empty());

  auto const init{make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0)};
  BOOST_TEST(init_block.trail.get());
  BOOST_TEST(*init_block.trail == *init);
}

BOOST_AUTO_TEST_CASE(no_init) {
  fill_bag(u8"let x: u64;");

  BOOST_TEST(test::nycleus::diags_eq(dt, test::nycleus::diags(
    std::make_unique<diag::uninitialized_global_var>(
      src_loc{f, {1, 5}, {1, 6}})
  )));

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& var{*util::downcast<nycleus::global_var*>(bag.get({u8"x"}))};

  BOOST_TEST((var.name == nycleus::global_name{{u8"x"}}));
  BOOST_TEST((var.core_loc == src_loc{f, {1, 5}, {1, 6}}));
  BOOST_TEST(!var.is_mutable);
  BOOST_TEST((var.processing_state.load(std::memory_order::relaxed)
    == nycleus::global_var::processing_state_t::initial));
  BOOST_TEST(!var.initializer);

  auto const var_type{make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(var.type.assume_hlir() == *var_type);
}

BOOST_AUTO_TEST_CASE(with_mut) {
  fill_bag(u8"let x mut: u64 = 0;");

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto const& var{*util::downcast<nycleus::global_var*>(bag.get({u8"x"}))};
  auto const& fn{*util::downcast<nycleus::function*>(
    bag.get({u8"x", {}, true}))};

  BOOST_TEST((var.name == nycleus::global_name{{u8"x"}}));
  BOOST_TEST((var.core_loc == src_loc{f, {1, 5}, {1, 6}}));
  BOOST_TEST(var.is_mutable);
  BOOST_TEST((var.processing_state.load(std::memory_order::relaxed)
    == nycleus::global_var::processing_state_t::initial));
  BOOST_TEST(var.initializer == &fn);

  auto const var_type{make<hlir::int_type>(src_loc{f, {1, 12}, {1, 15}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(var.type.assume_hlir() == *var_type);

  BOOST_TEST((fn.name == nycleus::global_name{u8"x", {}, true}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 18}, {1, 19}}));
  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(fn.initializee == &var);

  BOOST_REQUIRE(fn.body);
  auto& init_block{util::downcast<hlir::block const&>(*fn.body)};
  BOOST_TEST((init_block.loc == src_loc{f, {1, 18}, {1, 19}}));
  BOOST_TEST(init_block.stmts.empty());

  auto const init{make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)};
  BOOST_TEST(init_block.trail.get());
  BOOST_TEST(*init_block.trail == *init);
}

BOOST_AUTO_TEST_CASE(with_const) {
  fill_bag(u8"let x const: u64 = 0;");

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 2);

  auto const& var{*util::downcast<nycleus::global_var*>(bag.get({u8"x"}))};
  auto const& fn{*util::downcast<nycleus::function*>(
    bag.get({u8"x", {}, true}))};

  BOOST_TEST((var.name == nycleus::global_name{{u8"x"}}));
  BOOST_TEST((var.core_loc == src_loc{f, {1, 5}, {1, 6}}));
  BOOST_TEST(!var.is_mutable);
  BOOST_TEST((var.processing_state.load(std::memory_order::relaxed)
    == nycleus::global_var::processing_state_t::initial));
  BOOST_TEST(var.initializer == &fn);

  auto const var_type{make<hlir::int_type>(src_loc{f, {1, 14}, {1, 17}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(var.type.assume_hlir() == *var_type);

  BOOST_TEST((fn.name == nycleus::global_name{u8"x", {}, true}));
  BOOST_TEST((fn.core_loc == src_loc{f, {1, 20}, {1, 21}}));
  BOOST_TEST(fn.params.empty());
  BOOST_TEST(!fn.return_type);
  BOOST_TEST(fn.local_vars.empty());
  BOOST_TEST((fn.processing_state.load(std::memory_order::relaxed)
    == nycleus::function::processing_state_t::initial));
  BOOST_TEST(fn.initializee == &var);

  BOOST_REQUIRE(fn.body);
  auto& init_block{util::downcast<hlir::block const&>(*fn.body)};
  BOOST_TEST((init_block.loc == src_loc{f, {1, 20}, {1, 21}}));
  BOOST_TEST(init_block.stmts.empty());

  auto const init{make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)};
  BOOST_TEST(init_block.trail.get());
  BOOST_TEST(*init_block.trail == *init);
}

BOOST_AUTO_TEST_SUITE_END() // global_var

// Classes /////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(class_type)

BOOST_AUTO_TEST_CASE(empty) {
  fill_bag(u8"class A {}");

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& cl{*util::downcast<nycleus::class_type*>(bag.get({u8"A"}))};

  BOOST_TEST((cl.name == nycleus::global_name{u8"A"}));
  BOOST_TEST((cl.core_loc == src_loc{f, {1, 7}, {1, 8}}));
  BOOST_TEST(cl.fields.empty());
  BOOST_TEST((cl.processing_state.load(std::memory_order::relaxed)
    == nycleus::class_type::processing_state_t::initial));
}

BOOST_AUTO_TEST_CASE(one_field) {
  fill_bag(
    u8"class A {\n"
    u8"\tlet x: u64;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& cl{*util::downcast<nycleus::class_type*>(bag.get({u8"A"}))};

  BOOST_TEST((cl.name == nycleus::global_name{u8"A"}));
  BOOST_TEST((cl.core_loc == src_loc{f, {1, 7}, {1, 8}}));
  BOOST_TEST((cl.processing_state.load(std::memory_order::relaxed)
    == nycleus::class_type::processing_state_t::initial));

  BOOST_REQUIRE(cl.fields.size() == 1);
  auto& field{cl.fields[0]};
  BOOST_TEST(field.is_mutable);
  BOOST_TEST((field.core_loc == src_loc{f, {2, 6}, {2, 7}}));

  auto const field_type{make<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(field.type.assume_hlir() == *field_type);

  auto const lr{cl.scope.lookup(u8"x")};
  auto const* const field_lookup{std::get_if<nycleus::class_field_lookup>(&lr)};
  BOOST_REQUIRE(field_lookup);
  BOOST_TEST(&field_lookup->cl == &cl);
  BOOST_TEST(field_lookup->index == 0);
}

BOOST_AUTO_TEST_CASE(two_field) {
  fill_bag(
    u8"class A {\n"
    u8"\tlet x: u64;\n"
    u8"\tlet y: bool;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& cl{*util::downcast<nycleus::class_type*>(bag.get({u8"A"}))};

  BOOST_TEST((cl.name == nycleus::global_name{u8"A"}));
  BOOST_TEST((cl.core_loc == src_loc{f, {1, 7}, {1, 8}}));
  BOOST_TEST((cl.processing_state.load(std::memory_order::relaxed)
    == nycleus::class_type::processing_state_t::initial));

  BOOST_REQUIRE(cl.fields.size() == 2);

  auto const& field1{cl.fields[0]};
  BOOST_TEST(field1.is_mutable);
  BOOST_TEST((field1.core_loc == src_loc{f, {2, 6}, {2, 7}}));

  auto const field1_type{make<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(field1.type.assume_hlir() == *field1_type);

  auto const lr1{cl.scope.lookup(u8"x")};
  auto const* const field1_lookup{
    std::get_if<nycleus::class_field_lookup>(&lr1)};
  BOOST_REQUIRE(field1_lookup);
  BOOST_TEST(&field1_lookup->cl == &cl);
  BOOST_TEST(field1_lookup->index == 0);

  auto const& field2{cl.fields[1]};
  BOOST_TEST(field2.is_mutable);
  BOOST_TEST((field2.core_loc == src_loc{f, {3, 6}, {3, 7}}));

  auto const field2_type{make<hlir::bool_type>(src_loc{f, {3, 9}, {3, 13}})};
  BOOST_TEST(field2.type.assume_hlir() == *field2_type);

  auto const lr2{cl.scope.lookup(u8"y")};
  auto const* const field2_lookup{
    std::get_if<nycleus::class_field_lookup>(&lr2)};
  BOOST_REQUIRE(field2_lookup);
  BOOST_TEST(&field2_lookup->cl == &cl);
  BOOST_TEST(field2_lookup->index == 1);
}

BOOST_AUTO_TEST_CASE(field_mut_spec) {
  fill_bag(
    u8"class A {\n"
    u8"\tlet x mut: u64;\n"
    u8"\tlet y const: bool;\n"
    u8"}"
  );

  BOOST_TEST(std::move(dt).extract_diags().empty());

  BOOST_REQUIRE(std::ranges::distance(bag.entities()) == 1);
  auto const& cl{*util::downcast<nycleus::class_type*>(bag.get({u8"A"}))};

  BOOST_TEST((cl.name == nycleus::global_name{u8"A"}));
  BOOST_TEST((cl.core_loc == src_loc{f, {1, 7}, {1, 8}}));
  BOOST_TEST((cl.processing_state.load(std::memory_order::relaxed)
    == nycleus::class_type::processing_state_t::initial));

  BOOST_REQUIRE(cl.fields.size() == 2);

  auto const& field1{cl.fields[0]};
  BOOST_TEST(field1.is_mutable);
  BOOST_TEST((field1.core_loc == src_loc{f, {2, 6}, {2, 7}}));

  auto const field1_type{make<hlir::int_type>(src_loc{f, {2, 13}, {2, 16}},
    int_width_t{64},
    false
  )};
  BOOST_TEST(field1.type.assume_hlir() == *field1_type);

  auto const lr1{cl.scope.lookup(u8"x")};
  auto const* const field1_lookup{
    std::get_if<nycleus::class_field_lookup>(&lr1)};
  BOOST_REQUIRE(field1_lookup);
  BOOST_TEST(&field1_lookup->cl == &cl);
  BOOST_TEST(field1_lookup->index == 0);

  auto const& field2{cl.fields[1]};
  BOOST_TEST(!field2.is_mutable);
  BOOST_TEST((field2.core_loc == src_loc{f, {3, 6}, {3, 7}}));

  auto const field2_type{make<hlir::bool_type>(src_loc{f, {3, 15}, {3, 19}})};
  BOOST_TEST(field2.type.assume_hlir() == *field2_type);

  auto const lr2{cl.scope.lookup(u8"y")};
  auto const* const field2_lookup{
    std::get_if<nycleus::class_field_lookup>(&lr2)};
  BOOST_REQUIRE(field2_lookup);
  BOOST_TEST(&field2_lookup->cl == &cl);
  BOOST_TEST(field2_lookup->index == 1);
}

BOOST_AUTO_TEST_SUITE_END() // class_type

BOOST_AUTO_TEST_SUITE_END() // entity_bag
BOOST_AUTO_TEST_SUITE_END() // test_nycleus
