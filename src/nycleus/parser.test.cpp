#include <boost/test/unit_test.hpp>

#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_logger.hpp"
#include "nycleus/diagnostic_tools.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/hlir_tools.hpp"
#include "nycleus/lexer.hpp"
#include "nycleus/parser.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/utils.hpp"
#include "unicode/encoding.hpp"
#include "util/cow.hpp"
#include "util/ranges.hpp"
#include "test_tools.hpp"
#include <array>
#include <concepts>
#include <filesystem>
#include <functional>
#include <memory>
#include <optional>
#include <ranges>
#include <span>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

namespace diag = nycleus::diag;
namespace hlir = nycleus::hlir;
using namespace test::nycleus;

namespace {

using src_loc = nycleus::source_location;
using nycleus::hlir_ptr;
using nycleus::int_width_t;

struct parser_fixture {
  std::optional<util::cow<std::filesystem::path>> f{
    util::cow{std::filesystem::path{u8"test.ny"}}};
};

using sym = std::pair<std::u8string, hlir_ptr<hlir::def>>;

[[nodiscard]] auto syms(std::same_as<sym> auto... args) {
  std::unordered_map<std::u8string, hlir_ptr<hlir::def>> result;
  (result.emplace(std::move(args.first), std::move(args.second)), ...);
  return result;
}

template<nycleus::hlir_type T, typename... Args>
requires std::constructible_from<T, Args...>
[[nodiscard]] auto make(Args&&... args) {
  return nycleus::make_hlir<T>(std::forward<Args>(args)...);
}

void do_parser_test(
  std::u8string_view input,
  std::optional<util::cow<std::filesystem::path>> f,
  std::unordered_map<std::u8string, hlir_ptr<hlir::def>> test_symbols,
  std::span<std::unique_ptr<nycleus::diagnostic> const> test_diags = {}
) {
  nycleus::diagnostic_logger dt;

  std::unordered_map<std::u8string, hlir_ptr<hlir::def>> symbols;

  nycleus::parse(
    input | unicode::decode(nycleus::utf8_counted(std::move(f)))
      | nycleus::lex(),
    [&](std::u8string name, hlir_ptr<hlir::def> def) {
      auto const [it, is_new]
        = symbols.emplace(std::move(name), std::move(def));
      BOOST_CHECK(is_new);
    },
    dt
  );

  for(auto const& [name, def]: symbols) {
    BOOST_TEST(*def == *test_symbols.at(name));
    test_symbols.erase(name);
  }
  BOOST_TEST(test_symbols.empty());

  BOOST_TEST((test::nycleus::diags_eq(dt, test_diags)));
}

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_nycleus)
BOOST_FIXTURE_TEST_SUITE(parser, parser_fixture)

////////////////////////////////////////////////////////////////////////////////
// Version directive ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(version)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"version 0;", f,
    syms()
  );
}

BOOST_AUTO_TEST_CASE(hex) {
  do_parser_test(
    u8"version 0x0;", f,
    syms()
  );
}

BOOST_AUTO_TEST_CASE(unknown) {
  do_parser_test(
    u8"version 179;", f,
    syms(),
    diags(
      std::make_unique<diag::bad_version>(src_loc{f, {1, 9}, {1, 12}})
    )
  );
}

BOOST_AUTO_TEST_CASE(type_suffix) {
  do_parser_test(
    u8"version 0u64;", f,
    syms(),
    diags(
      std::make_unique<diag::version_type_suffix>(src_loc{f, {1, 9}, {1, 13}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_semicolon) {
  do_parser_test(
    u8"version 0 !", f,
    syms(),
    diags(
      std::make_unique<diag::version_semicolon_expected>(
        src_loc{f, {1, 11}, {1, 12}})
    )
  );

  do_parser_test(
    u8"version 0", f,
    syms(),
    diags(
      std::make_unique<diag::version_semicolon_expected>(
        src_loc{f, {1, 10}, {1, 10}})
    )
  );
}

BOOST_AUTO_TEST_CASE(is_not_num) {
  do_parser_test(
    u8"version x", f,
    syms(),
    diags(
      std::make_unique<diag::bad_version>(src_loc{f, {1, 9}, {1, 10}})
    )
  );
}

BOOST_AUTO_TEST_CASE(is_eof) {
  do_parser_test(
    u8"version", f,
    syms(),
    diags(
      std::make_unique<diag::bad_version>(src_loc{f, {1, 8}, {1, 8}})
    )
  );
}

BOOST_AUTO_TEST_CASE(and_more) {
  do_parser_test(
    u8"version 0;\n"
    u8"fn foo() {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {2, 4}, {2, 7}},
        std::vector<hlir::fn_def::param>{},
        make<hlir::block>(src_loc{f, {2, 10}, {2, 12}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(late) {
  do_parser_test(
    u8"fn foo() {}\n"
    u8"version 0;", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        std::vector<hlir::fn_def::param>{},
        make<hlir::block>(src_loc{f, {1, 10}, {1, 12}})
      )}
    ),
    diags(
      std::make_unique<diag::unexpected_version>(src_loc{f, {2, 1}, {2, 8}})
    )
  );
}

BOOST_AUTO_TEST_CASE(duplicate) {
  do_parser_test(
    u8"version 0;\n"
    u8"version 0;", f,
    syms(),
    diags(
      std::make_unique<diag::unexpected_version>(src_loc{f, {2, 1}, {2, 8}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // version

////////////////////////////////////////////////////////////////////////////////
// Definitions /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(defs)

// Function definitions ////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(fn_def)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"fn foo() {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        std::vector<hlir::fn_def::param>{},
        make<hlir::block>(src_loc{f, {1, 10}, {1, 12}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(one_param) {
  do_parser_test(
    u8"fn foo(x: u64) {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        test::make_vector<hlir::fn_def::param>(
          hlir::fn_def::param{
            u8"x",
            make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
              int_width_t{64},
              false
            ),
            src_loc{f, {1, 8}, {1, 9}}
          }
        ),
        make<hlir::block>(src_loc{f, {1, 16}, {1, 18}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(two_params) {
  do_parser_test(
    u8"fn foo(x: u64, y: s32) {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        test::make_vector<hlir::fn_def::param>(
          hlir::fn_def::param{
            u8"x",
            make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
              int_width_t{64},
              false
            ),
            src_loc{f, {1, 8}, {1, 9}}
          },
          hlir::fn_def::param{
            u8"y",
            make<hlir::int_type>(src_loc{f, {1, 19}, {1, 22}},
              int_width_t{32},
              true
            ),
            src_loc{f, {1, 16}, {1, 17}}
          }
        ),
        make<hlir::block>(src_loc{f, {1, 24}, {1, 26}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(param_and_comma) {
  do_parser_test(
    u8"fn foo(x: u64,) {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        test::make_vector<hlir::fn_def::param>(
          hlir::fn_def::param{
            u8"x",
            make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
              int_width_t{64},
              false
            ),
            src_loc{f, {1, 8}, {1, 9}}
          }
        ),
        make<hlir::block>(src_loc{f, {1, 17}, {1, 19}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(only_param_without_name) {
  do_parser_test(
    u8"fn foo(:u64) {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        test::make_vector<hlir::fn_def::param>(
          hlir::fn_def::param{
            std::nullopt,
            make<hlir::int_type>(src_loc{f, {1, 9}, {1, 12}},
              int_width_t{64},
              false
            ),
            src_loc{f, {1, 8}, {1, 12}}
          }
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {1, 16}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(first_param_without_name) {
  do_parser_test(
    u8"fn foo(:u64, x: s32) {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        test::make_vector<hlir::fn_def::param>(
          hlir::fn_def::param{
            std::nullopt,
            make<hlir::int_type>(src_loc{f, {1, 9}, {1, 12}},
              int_width_t{64},
              false
            ),
            src_loc{f, {1, 8}, {1, 12}}
          },
          hlir::fn_def::param{
            u8"x",
            make<hlir::int_type>(src_loc{f, {1, 17}, {1, 20}},
              int_width_t{32},
              true
            ),
            src_loc{f, {1, 14}, {1, 15}}
          }
        ),
        make<hlir::block>(src_loc{f, {1, 22}, {1, 24}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(last_param_without_name) {
  do_parser_test(
    u8"fn foo(x: u64, :s32) {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        test::make_vector<hlir::fn_def::param>(
          hlir::fn_def::param{
            u8"x",
            make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
              int_width_t{64},
              false
            ),
            src_loc{f, {1, 8}, {1, 9}}
          },
          hlir::fn_def::param{
            std::nullopt,
            make<hlir::int_type>(src_loc{f, {1, 17}, {1, 20}},
              int_width_t{32},
              true
            ),
            src_loc{f, {1, 16}, {1, 20}}
          }
        ),
        make<hlir::block>(src_loc{f, {1, 22}, {1, 24}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(no_params_but_comma) {
  do_parser_test(
    u8"fn foo(,) {}", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_param_name_expected>(
        src_loc{f, {1, 8}, {1, 9}})
    )
  );
}

BOOST_AUTO_TEST_CASE(return_type) {
  do_parser_test(
    u8"fn foo(x: u64): bool {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        test::make_vector<hlir::fn_def::param>(
          hlir::fn_def::param{
            u8"x",
            make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
              int_width_t{64},
              false
            ),
            src_loc{f, {1, 8}, {1, 9}}
          }
        ),
        make<hlir::bool_type>(src_loc{f, {1, 17}, {1, 21}}),
        make<hlir::block>(src_loc{f, {1, 22}, {1, 24}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(dollar_ids) {
  do_parser_test(
    u8"fn $foo($x: u64) {}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 8}},
        test::make_vector<hlir::fn_def::param>(
          hlir::fn_def::param{
            u8"x",
            make<hlir::int_type>(src_loc{f, {1, 13}, {1, 16}},
              int_width_t{64},
              false
            ),
            src_loc{f, {1, 9}, {1, 11}}
          }
        ),
        make<hlir::block>(src_loc{f, {1, 18}, {1, 20}})
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_name) {
  do_parser_test(
    u8"fn !", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_name_expected>(src_loc{f, {1, 4}, {1, 5}})
    )
  );

  do_parser_test(
    u8"fn", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_name_expected>(src_loc{f, {1, 3}, {1, 3}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_lparen) {
  do_parser_test(
    u8"fn foo!", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_lparen_expected>(src_loc{f, {1, 7}, {1, 8}})
    )
  );

  do_parser_test(
    u8"fn foo", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_lparen_expected>(src_loc{f, {1, 7}, {1, 7}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_name) {
  do_parser_test(
    u8"fn foo(!", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_param_name_expected>(
        src_loc{f, {1, 8}, {1, 9}})
    )
  );

  do_parser_test(
    u8"fn foo(", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_param_name_expected>(
        src_loc{f, {1, 8}, {1, 8}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_colon) {
  do_parser_test(
    u8"fn foo(x", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_param_type_expected>(
        src_loc{f, {1, 9}, {1, 9}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_type) {
  do_parser_test(
    u8"fn foo(x:", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 10}, {1, 10}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_comma) {
  do_parser_test(
    u8"fn foo(x: u64", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_param_next_expected>(
        src_loc{f, {1, 14}, {1, 14}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_body_without_return) {
  do_parser_test(
    u8"fn foo(x: u64)", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_body_expected>(src_loc{f, {1, 15}, {1, 15}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_return_type) {
  do_parser_test(
    u8"fn foo(x: u64):", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 16}, {1, 16}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_body_with_return) {
  do_parser_test(
    u8"fn foo(x: u64): u64", f,
    syms(),
    diags(
      std::make_unique<diag::fn_def_body_expected>(src_loc{f, {1, 20}, {1, 20}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // fn_def

// Variable definitions ////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(var_def)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(with_init) {
  do_parser_test(
    u8"let x: u64 = 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(with_mut) {
  do_parser_test(
    u8"let x mut: u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 12}, {1, 15}},
          int_width_t{64},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::mut
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(with_const) {
  do_parser_test(
    u8"let x const: u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 14}, {1, 17}},
          int_width_t{64},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::constant
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(dollar_id) {
  do_parser_test(
    u8"let $x: u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 7}},
        make<hlir::int_type>(src_loc{f, {1, 9}, {1, 12}},
          int_width_t{64},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_name) {
  do_parser_test(
    u8"let !", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_name_expected>(src_loc{f, {1, 5}, {1, 6}})
    )
  );

  do_parser_test(
    u8"let", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_name_expected>(src_loc{f, {1, 4}, {1, 4}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_colon) {
  do_parser_test(
    u8"let x!", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_type_expected>(src_loc{f, {1, 6}, {1, 7}})
    )
  );

  do_parser_test(
    u8"let x", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_type_expected>(src_loc{f, {1, 6}, {1, 6}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_type) {
  do_parser_test(
    u8"let x:!", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 7}, {1, 8}})
    )
  );

  do_parser_test(
    u8"let x:", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 7}, {1, 7}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_eq) {
  do_parser_test(
    u8"let x: u64 !", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_init_expected>(
        src_loc{f, {1, 12}, {1, 13}})
    )
  );

  do_parser_test(
    u8"let x: u64", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_init_expected>(
        src_loc{f, {1, 11}, {1, 11}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_init) {
  do_parser_test(
    u8"let x: u64 = @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 14}, {1, 15}})
    )
  );

  do_parser_test(
    u8"let x: u64 =", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 13}, {1, 13}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_semicolon) {
  do_parser_test(
    u8"let x: u64 = 0!", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 15}, {1, 16}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 15}, {1, 15}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // var_def

// Class definitions ///////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(class_def)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"class A {}", f,
    syms(
      sym{u8"A", make<hlir::class_def>(src_loc{f, {1, 7}, {1, 8}},
        std::vector<std::pair<std::u8string, hlir_ptr<hlir::def>>>{}
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(with_field) {
  do_parser_test(
    u8"class A {\n"
    u8"\tlet x: u64;\n"
    u8"}", f,
    syms(
      sym{u8"A", make<hlir::class_def>(src_loc{f, {1, 7}, {1, 8}},
        test::make_vector<sym>(
          sym{u8"x", make<hlir::var_def>(src_loc{f, {2, 6}, {2, 7}},
            make<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
              int_width_t{64},
              false
            ),
            nullptr,
            hlir::var_def::mutable_spec_t::none
          )}
        )
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(with_method) {
  do_parser_test(
    u8"class A {\n"
    u8"\tfn foo() {}\n"
    u8"}", f,
    syms(
      sym{u8"A", make<hlir::class_def>(src_loc{f, {1, 7}, {1, 8}},
        test::make_vector<sym>(
          sym{u8"foo", make<hlir::fn_def>(src_loc{f, {2, 5}, {2, 8}},
            std::vector<hlir::fn_def::param>{},
            make<hlir::block>(src_loc{f, {2, 11}, {2, 13}})
          )}
        )
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(with_nested_class) {
  do_parser_test(
    u8"class A {\n"
    u8"\tclass B {}\n"
    u8"}", f,
    syms(
      sym{u8"A", make<hlir::class_def>(src_loc{f, {1, 7}, {1, 8}},
        test::make_vector<sym>(
          sym{u8"B", make<hlir::class_def>(src_loc{f, {2, 8}, {2, 9}},
            std::vector<sym>{}
          )}
        )
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(dollar_id) {
  do_parser_test(
    u8"class $A {}", f,
    syms(
      sym{u8"A", make<hlir::class_def>(src_loc{f, {1, 7}, {1, 9}},
        std::vector<std::pair<std::u8string, hlir_ptr<hlir::def>>>{}
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_name) {
  do_parser_test(
    u8"class !", f,
    syms(),
    diags(
      std::make_unique<diag::class_def_name_expected>(
        src_loc{f, {1, 7}, {1, 8}})
    )
  );

  do_parser_test(
    u8"class", f,
    syms(),
    diags(
      std::make_unique<diag::class_def_name_expected>(
        src_loc{f, {1, 6}, {1, 6}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_body) {
  do_parser_test(
    u8"class A!", f,
    syms(),
    diags(
      std::make_unique<diag::class_def_body_expected>(
        src_loc{f, {1, 8}, {1, 9}})
    )
  );

  do_parser_test(
    u8"class A", f,
    syms(),
    diags(
      std::make_unique<diag::class_def_body_expected>(
        src_loc{f, {1, 8}, {1, 8}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_rbrace) {
  do_parser_test(
    u8"class A { !", f,
    syms(),
    diags(
      std::make_unique<diag::class_def_rbrace_expected>(
        src_loc{f, {1, 11}, {1, 12}})
    )
  );

  do_parser_test(
    u8"class A {", f,
    syms(),
    diags(
      std::make_unique<diag::class_def_rbrace_expected>(
        src_loc{f, {1, 10}, {1, 10}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // class_def

// Bad definitions /////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(bad) {
  do_parser_test(
    u8"0", f,
    syms(),
    diags(
      std::make_unique<diag::def_or_directive_expected>(
        src_loc{f, {1, 1}, {1, 2}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // defs

////////////////////////////////////////////////////////////////////////////////
// Types ///////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(types)

// Integer types ///////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(int_type)

BOOST_AUTO_TEST_CASE(basic_unsigned) {
  do_parser_test(
    u8"let x: u8;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 10}},
          int_width_t{8},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(basic_signed) {
  do_parser_test(
    u8"let x: s16;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{16},
          true
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(fractional) {
  do_parser_test(
    u8"let x: u14;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{14},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(big) {
  do_parser_test(
    u8"let x: u99999;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 14}},
          int_width_t{99999},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(too_big) {
  do_parser_test(
    u8"let x: u99999999999999999999999999999999999;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 44}},
          nycleus::max_int_width,
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    ),
    diags(
      std::make_unique<diag::int_type_too_wide>(src_loc{f, {1, 8}, {1, 44}})
    )
  );
}

BOOST_AUTO_TEST_CASE(leading_zeros) {
  do_parser_test(
    u8"let x: u00032;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 14}},
          int_width_t{32},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(zero_width) {
  do_parser_test(
    u8"let x: u0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 10}},
          int_width_t{1},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    ),
    diags(
      std::make_unique<diag::int_type_zero_width>(src_loc{f, {1, 8}, {1, 10}})
    )
  );

  do_parser_test(
    u8"let x: u000;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 12}},
          int_width_t{1},
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    ),
    diags(
      std::make_unique<diag::int_type_zero_width>(src_loc{f, {1, 8}, {1, 12}})
    )
  );
}

BOOST_AUTO_TEST_CASE(unicode) {
  do_parser_test(
    u8"let x: s\U0001D7DE\U0001D7DC;", f, // "let x: s𝟞𝟜;"
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 17}},
          int_width_t{64},
          true
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // int_type

// The Boolean type ////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(bool_type) {
  do_parser_test(
    u8"let x: bool;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::bool_type>(src_loc{f, {1, 8}, {1, 12}}),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

// Function reference types ////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(fn_type)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: fn();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 12}},
          std::vector<hlir_ptr<hlir::type>>{},
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(one_param) {
  do_parser_test(
    u8"let x: fn(x: u64);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 18}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 14}, {1, 17}},
              int_width_t{64},
              false
            )
          ),
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(two_params) {
  do_parser_test(
    u8"let x: fn(x: u64, y: s32);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 26}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 14}, {1, 17}},
              int_width_t{64},
              false
            ),
            make<hlir::int_type>(src_loc{f, {1, 22}, {1, 25}},
              int_width_t{32},
              true
            )
          ),
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(param_and_comma) {
  do_parser_test(
    u8"let x: fn(x: u64,);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 19}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 14}, {1, 17}},
              int_width_t{64},
              false
            )
          ),
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(only_param_without_name) {
  do_parser_test(
    u8"let x: fn(:u64);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 16}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 12}, {1, 15}},
              int_width_t{64},
              false
            )
          ),
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(first_param_without_name) {
  do_parser_test(
    u8"let x: fn(:u64, x: s32);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 24}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 12}, {1, 15}},
              int_width_t{64},
              false
            ),
            make<hlir::int_type>(src_loc{f, {1, 20}, {1, 23}},
              int_width_t{32},
              true
            )
          ),
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(last_param_without_name) {
  do_parser_test(
    u8"let x: fn(x: u64, :s32);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 24}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 14}, {1, 17}},
              int_width_t{64},
              false
            ),
            make<hlir::int_type>(src_loc{f, {1, 20}, {1, 23}},
              int_width_t{32},
              true
            )
          ),
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(no_params_but_comma) {
  do_parser_test(
    u8"let x: fn(,);", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_param_name_expected>(
        src_loc{f, {1, 11}, {1, 12}})
    )
  );
}

BOOST_AUTO_TEST_CASE(return_type) {
  do_parser_test(
    u8"let x: fn((x: u64): bool);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 26}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 15}, {1, 18}},
              int_width_t{64},
              false
            )
          ),
          make<hlir::bool_type>(src_loc{f, {1, 21}, {1, 25}})
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(extra_parens) {
  do_parser_test(
    u8"let x: fn((x: u64));", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 20}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 15}, {1, 18}},
              int_width_t{64},
              false
            )
          ),
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(dollar_ids) {
  do_parser_test(
    u8"let x: fn($x: u64);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::fn_type>(src_loc{f, {1, 8}, {1, 19}},
          test::make_vector<hlir_ptr<hlir::type>>(
            make<hlir::int_type>(src_loc{f, {1, 15}, {1, 18}},
              int_width_t{64},
              false
            )
          ),
          nullptr
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_lparen) {
  do_parser_test(
    u8"let x: fn!", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_lparen_expected>(
        src_loc{f, {1, 10}, {1, 11}})
    )
  );

  do_parser_test(
    u8"let x: fn", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_lparen_expected>(
        src_loc{f, {1, 10}, {1, 10}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_name) {
  do_parser_test(
    u8"let x: fn(!", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_param_name_expected>(
        src_loc{f, {1, 11}, {1, 12}})
    )
  );

  do_parser_test(
    u8"let x: fn(", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_param_name_expected>(
        src_loc{f, {1, 11}, {1, 11}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_colon) {
  do_parser_test(
    u8"let x: fn(x!", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_param_type_expected>(
        src_loc{f, {1, 12}, {1, 13}})
    )
  );

  do_parser_test(
    u8"let x: fn(x", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_param_type_expected>(
        src_loc{f, {1, 12}, {1, 12}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_type) {
  do_parser_test(
    u8"let x: fn(x: !", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 14}, {1, 15}})
    )
  );

  do_parser_test(
    u8"let x: fn(x:", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 13}, {1, 13}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_comma) {
  do_parser_test(
    u8"let x: fn(x: u64!", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_param_next_expected>(
        src_loc{f, {1, 17}, {1, 18}})
    )
  );

  do_parser_test(
    u8"let x: fn(x: u64", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_param_next_expected>(
        src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_return_colon) {
  do_parser_test(
    u8"let x: fn((x: u64)!", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_rparen_expected>(
        src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: fn((x: u64)", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_rparen_expected>(
        src_loc{f, {1, 19}, {1, 19}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_return_type) {
  do_parser_test(
    u8"let x: fn((x: u64):!", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: fn((x: u64):", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 20}, {1, 20}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_extra_rparen) {
  do_parser_test(
    u8"let x: fn((x: u64): u64!", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_rparen_expected>(
        src_loc{f, {1, 24}, {1, 25}})
    )
  );

  do_parser_test(
    u8"let x: fn((x: u64): u64", f,
    syms(),
    diags(
      std::make_unique<diag::fn_type_rparen_expected>(
        src_loc{f, {1, 24}, {1, 24}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // fn_type

// Pointer types ///////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(ptr_type)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: &u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 12}},
          make<hlir::int_type>(src_loc{f, {1, 9}, {1, 12}},
            int_width_t{64},
            false
          ),
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_mut) {
  do_parser_test(
    u8"let x: &mut u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 16}},
          make<hlir::int_type>(src_loc{f, {1, 13}, {1, 16}},
            int_width_t{64},
            false
          ),
          true
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_const) {
  do_parser_test(
    u8"let x: &const u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 18}},
          make<hlir::int_type>(src_loc{f, {1, 15}, {1, 18}},
            int_width_t{64},
            false
          ),
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_ptr) {
  do_parser_test(
    u8"let x: &&u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 13}},
          make<hlir::ptr_type>(src_loc{f, {1, 9}, {1, 13}},
            make<hlir::int_type>(src_loc{f, {1, 10}, {1, 13}},
              int_width_t{64},
              false
            ),
            false
          ),
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: & &u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 14}},
          make<hlir::ptr_type>(src_loc{f, {1, 10}, {1, 14}},
            make<hlir::int_type>(src_loc{f, {1, 11}, {1, 14}},
              int_width_t{64},
              false
            ),
            false
          ),
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_ptr_const) {
  do_parser_test(
    u8"let x: &&const u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 19}},
          make<hlir::ptr_type>(src_loc{f, {1, 9}, {1, 19}},
            make<hlir::int_type>(src_loc{f, {1, 16}, {1, 19}},
              int_width_t{64},
              false
            ),
            false
          ),
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: & &const u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 20}},
          make<hlir::ptr_type>(src_loc{f, {1, 10}, {1, 20}},
            make<hlir::int_type>(src_loc{f, {1, 17}, {1, 20}},
              int_width_t{64},
              false
            ),
            false
          ),
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_ptr_mut) {
  do_parser_test(
    u8"let x: &&mut u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 17}},
          make<hlir::ptr_type>(src_loc{f, {1, 9}, {1, 17}},
            make<hlir::int_type>(src_loc{f, {1, 14}, {1, 17}},
              int_width_t{64},
              false
            ),
            true
          ),
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: & &mut u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 18}},
          make<hlir::ptr_type>(src_loc{f, {1, 10}, {1, 18}},
            make<hlir::int_type>(src_loc{f, {1, 15}, {1, 18}},
              int_width_t{64},
              false
            ),
            true
          ),
          false
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_mut_ptr) {
  do_parser_test(
    u8"let x: &mut &u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 17}},
          make<hlir::ptr_type>(src_loc{f, {1, 13}, {1, 17}},
            make<hlir::int_type>(src_loc{f, {1, 14}, {1, 17}},
              int_width_t{64},
              false
            ),
            false
          ),
          true
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_mut_ptr_mut) {
  do_parser_test(
    u8"let x: &mut &mut u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::ptr_type>(src_loc{f, {1, 8}, {1, 21}},
          make<hlir::ptr_type>(src_loc{f, {1, 13}, {1, 21}},
            make<hlir::int_type>(src_loc{f, {1, 18}, {1, 21}},
              int_width_t{64},
              false
            ),
            true
          ),
          true
        ),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_pointee) {
  do_parser_test(
    u8"let x: &!", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 9}, {1, 10}})
    )
  );

  do_parser_test(
    u8"let x: &", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 9}, {1, 9}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_pointee_double) {
  do_parser_test(
    u8"let x: &&!", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 10}, {1, 11}})
    )
  );

  do_parser_test(
    u8"let x: &&", f,
    syms(),
    diags(
      std::make_unique<diag::type_name_expected>(src_loc{f, {1, 10}, {1, 10}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // ptr_type

// Named types /////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(named_type) {
  do_parser_test(
    u8"let x: $A;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::named_type>(src_loc{f, {1, 8}, {1, 10}}, u8"A"),
        nullptr,
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // types

////////////////////////////////////////////////////////////////////////////////
// Statements //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(stmts)

BOOST_AUTO_TEST_CASE(fn_def) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\tfn bar() {}\n"
    u8"}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        std::vector<hlir::fn_def::param>{},
        make<hlir::block>(src_loc{f, {1, 10}, {3, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::def_stmt>(
              u8"bar",
              make<hlir::fn_def>(src_loc{f, {2, 5}, {2, 8}},
                std::vector<hlir::fn_def::param>{},
                make<hlir::block>(src_loc{f, {2, 11}, {2, 13}})
              )
            )
          ),
          nullptr
        )
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(var_def) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\tlet x: u64;\n"
    u8"}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        std::vector<hlir::fn_def::param>{},
        make<hlir::block>(src_loc{f, {1, 10}, {3, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::def_stmt>(
              u8"x",
              make<hlir::var_def>(src_loc{f, {2, 6}, {2, 7}},
                make<hlir::int_type>(src_loc{f, {2, 9}, {2, 12}},
                  int_width_t{64},
                  false
                ),
                nullptr,
                hlir::var_def::mutable_spec_t::none
              )
            )
          ),
          nullptr
        )
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(class_def) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\tclass A {}\n"
    u8"}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        std::vector<hlir::fn_def::param>{},
        make<hlir::block>(src_loc{f, {1, 10}, {3, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::def_stmt>(
              u8"A",
              make<hlir::class_def>(src_loc{f, {2, 8}, {2, 9}},
                std::vector<std::pair<std::u8string, hlir_ptr<hlir::def>>>{}
              )
            )
          ),
          nullptr
        )
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(expr_stmt) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x;\n"
    u8"}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        std::vector<hlir::fn_def::param>{},
        make<hlir::block>(src_loc{f, {1, 10}, {3, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::expr_stmt>(
              make<hlir::named_var_ref>(src_loc{f, {2, 2}, {2, 4}}, u8"x")
            )
          ),
          nullptr
        )
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(assign_stmt) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x = $y;\n"
    u8"}", f,
    syms(
      sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
        std::vector<hlir::fn_def::param>{},
        make<hlir::block>(src_loc{f, {1, 10}, {3, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::assign_stmt>(
              make<hlir::named_var_ref>(src_loc{f, {2, 2}, {2, 4}}, u8"x"),
              make<hlir::named_var_ref>(src_loc{f, {2, 7}, {2, 9}}, u8"y")
            )
          ),
          nullptr
        )
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_semicolon_after_expr) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x @\n"
    u8"}", f,
    syms(),
    diags(
      std::make_unique<diag::block_rbrace_expected>(src_loc{f, {2, 5}, {2, 6}})
    )
  );

  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x", f,
    syms(),
    diags(
      std::make_unique<diag::block_rbrace_expected>(src_loc{f, {2, 4}, {2, 4}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_assign_value) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x = @\n"
    u8"}", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {2, 7}, {2, 8}})
    )
  );

  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x =", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {2, 6}, {2, 6}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_assign_semicolon) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x = $y @\n"
    u8"}", f,
    syms(),
    diags(
      std::make_unique<diag::assign_semicolon_expected>(
        src_loc{f, {2, 10}, {2, 11}})
    )
  );

  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x = $y", f,
    syms(),
    diags(
      std::make_unique<diag::assign_semicolon_expected>(
        src_loc{f, {2, 9}, {2, 9}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // stmts

////////////////////////////////////////////////////////////////////////////////
// Expressions /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(exprs)

BOOST_AUTO_TEST_CASE(int_literal) {
  do_parser_test(
    u8"let x: u64 = 179;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 17}},
          179,
          std::nullopt
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(int_literal_type_suffix) {
  do_parser_test(
    u8"let x: u64 = 179u64;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 20}},
          179,
          nycleus::int_type_suffix{64, false}
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bool_literal) {
  do_parser_test(
    u8"let x: bool = true;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::bool_type>(src_loc{f, {1, 8}, {1, 12}}),
        make<hlir::bool_literal>(src_loc{f, {1, 15}, {1, 19}}, true),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: bool = false;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::bool_type>(src_loc{f, {1, 8}, {1, 12}}),
        make<hlir::bool_literal>(src_loc{f, {1, 15}, {1, 20}}, false),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(named_var_ref) {
  do_parser_test(
    u8"let x: u64 = $y;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::named_var_ref>(src_loc{f, {1, 14}, {1, 16}}, u8"y"),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_SUITE(cmp_expr)

BOOST_AUTO_TEST_CASE(lt) {
  do_parser_test(
    u8"let x: u64 = 0 < 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::lt,
              make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
              src_loc{f, {1, 16}, {1, 17}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(gt) {
  do_parser_test(
    u8"let x: u64 = 0 > 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::gt,
              make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
              src_loc{f, {1, 16}, {1, 17}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(le) {
  do_parser_test(
    u8"let x: u64 = 0 <= 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::le,
              make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(ge) {
  do_parser_test(
    u8"let x: u64 = 0 >= 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::ge,
              make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(eq) {
  do_parser_test(
    u8"let x: u64 = 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(chain_lt) {
  do_parser_test(
    u8"let x: u64 = 0 < 0 <= 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 29}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::lt,
              make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
              src_loc{f, {1, 16}, {1, 17}}
            },
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::le,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            },
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 28}, {1, 29}}, 0),
              src_loc{f, {1, 25}, {1, 27}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(chain_gt) {
  do_parser_test(
    u8"let x: u64 = 0 > 0 == 0 >= 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 29}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::gt,
              make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
              src_loc{f, {1, 16}, {1, 17}}
            },
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            },
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::ge,
              make<hlir::int_literal>(src_loc{f, {1, 28}, {1, 29}}, 0),
              src_loc{f, {1, 25}, {1, 27}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(chain_bad) {
  do_parser_test(
    u8"let x: u64 = 0 < 0 >= 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::lt,
              make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
              src_loc{f, {1, 16}, {1, 17}}
            },
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::ge,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    ),
    diags(
      std::make_unique<diag::bad_comparison_chain>(
        src_loc{f, {1, 14}, {1, 24}},
        src_loc{f, {1, 16}, {1, 17}},
        src_loc{f, {1, 20}, {1, 22}},
        true,
        false,
        true
      )
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 > 0 <= 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::gt,
              make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
              src_loc{f, {1, 16}, {1, 17}}
            },
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::le,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    ),
    diags(
      std::make_unique<diag::bad_comparison_chain>(
        src_loc{f, {1, 14}, {1, 24}},
        src_loc{f, {1, 20}, {1, 22}},
        src_loc{f, {1, 16}, {1, 17}},
        false,
        true,
        false
      )
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_right) {
  do_parser_test(
    u8"let x: u64 = 0 < @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 <", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_right_chain) {
  do_parser_test(
    u8"let x: u64 = 0 < 0 <= @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 23}, {1, 24}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 < 0 <=", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 22}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // cmp_expr

BOOST_AUTO_TEST_CASE(or_expr) {
  do_parser_test(
    u8"let x: u64 = 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 || @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ||", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(and_expr) {
  do_parser_test(
    u8"let x: u64 = 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 && @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 &&", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_expr) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^^ @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^^", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_expr) {
  do_parser_test(
    u8"let x: u64 = 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 != @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 !=", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_expr) {
  do_parser_test(
    u8"let x: u64 = 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 + @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 +", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_expr) {
  do_parser_test(
    u8"let x: u64 = 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 - @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 -", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_expr) {
  do_parser_test(
    u8"let x: u64 = 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 * @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 *", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(div_expr) {
  do_parser_test(
    u8"let x: u64 = 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 /", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_expr) {
  do_parser_test(
    u8"let x: u64 = 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 %", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_expr) {
  do_parser_test(
    u8"let x: u64 = 0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 |", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(band_expr) {
  do_parser_test(
    u8"let x: u64 = 0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 &", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_expr) {
  do_parser_test(
    u8"let x: u64 = 0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_expr) {
  do_parser_test(
    u8"let x: u64 = 0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 <<", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_expr) {
  do_parser_test(
    u8"let x: u64 = 0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >>", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_expr) {
  do_parser_test(
    u8"let x: u64 = 0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_expr) {
  do_parser_test(
    u8"let x: u64 = 0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(neg_expr) {
  do_parser_test(
    u8"let x: u64 = -0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
          make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = -@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
    )
  );

  do_parser_test(
    u8"let x: u64 = -", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bnot_expr) {
  do_parser_test(
    u8"let x: u64 = ~0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
          make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = ~@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
    )
  );

  do_parser_test(
    u8"let x: u64 = ~", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
    )
  );
}

BOOST_AUTO_TEST_CASE(not_expr) {
  do_parser_test(
    u8"let x: u64 = !0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
          make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = !@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
    )
  );

  do_parser_test(
    u8"let x: u64 = !", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
    )
  );
}

BOOST_AUTO_TEST_SUITE(addr_expr)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: u64 = &0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
          make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
    )
  );

  do_parser_test(
    u8"let x: u64 = &", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
    )
  );
}

BOOST_AUTO_TEST_CASE(basic_mut) {
  do_parser_test(
    u8"let x: u64 = &mut 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
          true
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &mut @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = &mut", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_CASE(basic_const) {
  do_parser_test(
    u8"let x: u64 = &const 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &const @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 21}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = &const", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 20}, {1, 20}})
    )
  );
}

BOOST_AUTO_TEST_CASE(double_amp) {
  do_parser_test(
    u8"let x: u64 = &&0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
          make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
            make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
            false
          ),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 17}})
    )
  );

  do_parser_test(
    u8"let x: u64 = &&", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 16}})
    )
  );
}

BOOST_AUTO_TEST_CASE(double_amp_mut) {
  do_parser_test(
    u8"let x: u64 = &&mut 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
            true
          ),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&mut @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = &&mut", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 19}})
    )
  );
}

BOOST_AUTO_TEST_CASE(double_amp_const) {
  do_parser_test(
    u8"let x: u64 = &&const 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 23}},
            make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0),
            false
          ),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&const @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 22}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = &&const", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 21}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // addr_expr

BOOST_AUTO_TEST_CASE(deref_expr) {
  do_parser_test(
    u8"let x: u64 = *0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
          make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = *@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
    )
  );

  do_parser_test(
    u8"let x: u64 = *", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
    )
  );
}

BOOST_AUTO_TEST_CASE(named_member_expr) {
  do_parser_test(
    u8"let x: u64 = $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::named_member_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::named_var_ref>(src_loc{f, {1, 14}, {1, 16}}, u8"y"),
          u8"foo"
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = $y.$foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::named_member_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::named_var_ref>(src_loc{f, {1, 14}, {1, 16}}, u8"y"),
          u8"foo"
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = $y.@", f,
    syms(),
    diags(
      std::make_unique<diag::member_expr_name_expected>(
        src_loc{f, {1, 17}, {1, 18}})
    )
  );

  do_parser_test(
    u8"let x: u64 = $y.", f,
    syms(),
    diags(
      std::make_unique<diag::member_expr_name_expected>(
        src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_SUITE(call_expr)

BOOST_AUTO_TEST_CASE(no_args) {
  do_parser_test(
    u8"let x: u64 = 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::call_expr>(src_loc{f, {1, 14}, {1, 17}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          std::vector<hlir::call_expr::arg>{}
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(one_arg) {
  do_parser_test(
    u8"let x: u64 = 0(0);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::call_expr>(src_loc{f, {1, 14}, {1, 18}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_vector<hlir::call_expr::arg>(
            hlir::call_expr::arg{
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0)
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(two_args) {
  do_parser_test(
    u8"let x: u64 = 0(0, 0);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::call_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_vector<hlir::call_expr::arg>(
            hlir::call_expr::arg{
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0)
            },
            hlir::call_expr::arg{
              make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(arg_and_comma) {
  do_parser_test(
    u8"let x: u64 = 0(0,);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::call_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_vector<hlir::call_expr::arg>(
            hlir::call_expr::arg{
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0)
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(no_args_but_comma) {
  do_parser_test(
    u8"let x: u64 = 0(,);", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_first_arg) {
  do_parser_test(
    u8"let x: u64 = 0(@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 17}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0(", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 16}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_comma) {
  do_parser_test(
    u8"let x: u64 = 0(0@", f,
    syms(),
    diags(
      std::make_unique<diag::call_expr_arg_expected>(
        src_loc{f, {1, 17}, {1, 18}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0(0", f,
    syms(),
    diags(
      std::make_unique<diag::call_expr_arg_expected>(
        src_loc{f, {1, 17}, {1, 17}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_next_arg) {
  do_parser_test(
    u8"let x: u64 = 0(0, @", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0(0,", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // call_expr

BOOST_AUTO_TEST_SUITE(blocks)

BOOST_AUTO_TEST_CASE(empty) {
  do_parser_test(
    u8"let x: u64 = {};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {1, 16}}),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(one_stmt) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\tlet y: bool;\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {3, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::def_stmt>(u8"y",
              make<hlir::var_def>(src_loc{f, {2, 6}, {2, 7}},
                make<hlir::bool_type>(src_loc{f, {2, 9}, {2, 13}}),
                nullptr,
                hlir::var_def::mutable_spec_t::none
              )
            )
          ),
          nullptr
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t0;\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {3, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::expr_stmt>(
              make<hlir::int_literal>(src_loc{f, {2, 2}, {2, 3}}, 0)
            )
          ),
          nullptr
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(two_stmts) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\tlet y: bool;\n"
    u8"\tlet z: bool;\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {4, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::def_stmt>(u8"y",
              make<hlir::var_def>(src_loc{f, {2, 6}, {2, 7}},
                make<hlir::bool_type>(src_loc{f, {2, 9}, {2, 13}}),
                nullptr,
                hlir::var_def::mutable_spec_t::none
              )
            ),
            make<hlir::def_stmt>(u8"z",
              make<hlir::var_def>(src_loc{f, {3, 6}, {3, 7}},
                make<hlir::bool_type>(src_loc{f, {3, 9}, {3, 13}}),
                nullptr,
                hlir::var_def::mutable_spec_t::none
              )
            )
          ),
          nullptr
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t0;\n"
    u8"\t0;\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {4, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::expr_stmt>(
              make<hlir::int_literal>(src_loc{f, {2, 2}, {2, 3}}, 0)
            ),
            make<hlir::expr_stmt>(
              make<hlir::int_literal>(src_loc{f, {3, 2}, {3, 3}}, 0)
            )
          ),
          nullptr
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(nested_block) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t{}\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {3, 2}},
          std::vector<hlir_ptr<hlir::stmt>>{},
          make<hlir::block>(src_loc{f, {2, 2}, {2, 4}})
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(block_then_stmt) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t{}\n"
    u8"\tlet y: bool;\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {4, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::expr_stmt>(
              make<hlir::block>(src_loc{f, {2, 2}, {2, 4}})
            ),
            make<hlir::def_stmt>(
              u8"y",
              make<hlir::var_def>(src_loc{f, {3, 6}, {3, 7}},
                make<hlir::bool_type>(src_loc{f, {3, 9}, {3, 13}}),
                nullptr,
                hlir::var_def::mutable_spec_t::none
              )
            )
          ),
          nullptr
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(stmt_then_block) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\tlet y: bool;\n"
    u8"\t{}\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {4, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::def_stmt>(
              u8"y",
              make<hlir::var_def>(src_loc{f, {2, 6}, {2, 7}},
                make<hlir::bool_type>(src_loc{f, {2, 9}, {2, 13}}),
                nullptr,
                hlir::var_def::mutable_spec_t::none
              )
            )
          ),
          make<hlir::block>(src_loc{f, {3, 2}, {3, 4}})
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(two_nested_blocks) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t{}\n"
    u8"\t{}\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {4, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::expr_stmt>(
              make<hlir::block>(src_loc{f, {2, 2}, {2, 4}})
            )
          ),
          make<hlir::block>(src_loc{f, {3, 2}, {3, 4}})
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(block_as_stmt) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t{}\n"
    u8"\t-0;\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {4, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::expr_stmt>(
              make<hlir::block>(src_loc{f, {2, 2}, {2, 4}})
            ),
            make<hlir::expr_stmt>(
              make<hlir::neg_expr>(src_loc{f, {3, 2}, {3, 4}},
                make<hlir::int_literal>(src_loc{f, {3, 3}, {3, 4}}, 0)
              )
            )
          ),
          nullptr
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(block_as_expr) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t({}) - 0;\n"
    u8"};", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::block>(src_loc{f, {1, 14}, {3, 2}},
          test::make_vector<hlir_ptr<hlir::stmt>>(
            make<hlir::expr_stmt>(
              make<hlir::sub_expr>(src_loc{f, {2, 2}, {2, 10}},
                make<hlir::block>(src_loc{f, {2, 3}, {2, 5}}),
                make<hlir::int_literal>(src_loc{f, {2, 9}, {2, 10}}, 0)
              )
            )
          ),
          nullptr
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // blocks

BOOST_AUTO_TEST_SUITE(parens)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: u64 = (0);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(double_parens) {
  do_parser_test(
    u8"let x: u64 = ((0));", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(as_operands) {
  do_parser_test(
    u8"let x: u64 = (0) + (0);", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_expr) {
  do_parser_test(
    u8"let x: u64 = (@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
    )
  );

  do_parser_test(
    u8"let x: u64 = (", f,
    syms(),
    diags(
      std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_rparen) {
  do_parser_test(
    u8"let x: u64 = (0@", f,
    syms(),
    diags(
      std::make_unique<diag::expr_rparen_expected>(src_loc{f, {1, 16}, {1, 17}})
    )
  );

  do_parser_test(
    u8"let x: u64 = (0", f,
    syms(),
    diags(
      std::make_unique<diag::expr_rparen_expected>(src_loc{f, {1, 16}, {1, 16}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // parens

BOOST_AUTO_TEST_SUITE_END() // exprs

BOOST_AUTO_TEST_SUITE(associativity)

BOOST_AUTO_TEST_CASE(or_expr) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_expr) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_expr) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_expr) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 != 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_expr) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_expr) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_expr) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_expr) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_expr) {
  do_parser_test(
    u8"let x: u64 = 0 % 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_expr) {
  do_parser_test(
    u8"let x: u64 = 0 | 0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(band_expr) {
  do_parser_test(
    u8"let x: u64 = 0 & 0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_expr) {
  do_parser_test(
    u8"let x: u64 = 0 ^ 0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_expr) {
  do_parser_test(
    u8"let x: u64 = 0 << 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_expr) {
  do_parser_test(
    u8"let x: u64 = 0 >> 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_expr) {
  do_parser_test(
    u8"let x: u64 = 0 </ 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_expr) {
  do_parser_test(
    u8"let x: u64 = 0 >/ 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // associativity

BOOST_AUTO_TEST_SUITE(precedence)

BOOST_AUTO_TEST_CASE(cmp_or) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            test::make_small_vector<hlir::cmp_expr::rel, 1>(
              hlir::cmp_expr::rel{
                hlir::cmp_expr::rel_kind::eq,
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                src_loc{f, {1, 16}, {1, 18}}
              }
            )
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 || 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::cmp_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            test::make_small_vector<hlir::cmp_expr::rel, 1>(
              hlir::cmp_expr::rel{
                hlir::cmp_expr::rel_kind::eq,
                make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
                src_loc{f, {1, 21}, {1, 23}}
              }
            )
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_and) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            test::make_small_vector<hlir::cmp_expr::rel, 1>(
              hlir::cmp_expr::rel{
                hlir::cmp_expr::rel_kind::eq,
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                src_loc{f, {1, 16}, {1, 18}}
              }
            )
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 && 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::cmp_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            test::make_small_vector<hlir::cmp_expr::rel, 1>(
              hlir::cmp_expr::rel{
                hlir::cmp_expr::rel_kind::eq,
                make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
                src_loc{f, {1, 21}, {1, 23}}
              }
            )
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_xor) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            test::make_small_vector<hlir::cmp_expr::rel, 1>(
              hlir::cmp_expr::rel{
                hlir::cmp_expr::rel_kind::eq,
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                src_loc{f, {1, 16}, {1, 18}}
              }
            )
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::cmp_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            test::make_small_vector<hlir::cmp_expr::rel, 1>(
              hlir::cmp_expr::rel{
                hlir::cmp_expr::rel_kind::eq,
                make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
                src_loc{f, {1, 21}, {1, 23}}
              }
            )
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_neq) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 != 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 != 0 == 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_add) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 + 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_sub) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 - 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_mul) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 * 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_div) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_mod) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_bor) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_band) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
              src_loc{f, {1, 20}, {1, 22}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
              src_loc{f, {1, 21}, {1, 23}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
              src_loc{f, {1, 21}, {1, 23}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
              src_loc{f, {1, 21}, {1, 23}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 == 0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
                make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
                make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
              src_loc{f, {1, 21}, {1, 23}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_neg) {
  do_parser_test(
    u8"let x: u64 = -0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
              src_loc{f, {1, 17}, {1, 19}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
              src_loc{f, {1, 17}, {1, 19}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_not) {
  do_parser_test(
    u8"let x: u64 = !0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
              src_loc{f, {1, 17}, {1, 19}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_addr) {
  do_parser_test(
    u8"let x: u64 = &0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
              src_loc{f, {1, 17}, {1, 19}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0),
              src_loc{f, {1, 18}, {1, 20}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_deref) {
  do_parser_test(
    u8"let x: u64 = *0 == 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
              src_loc{f, {1, 17}, {1, 19}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_member) {
  do_parser_test(
    u8"let x: u64 = 0 == $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
                make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
                u8"foo"
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(cmp_call) {
  do_parser_test(
    u8"let x: u64 = 0 == $y();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          test::make_small_vector<hlir::cmp_expr::rel, 1>(
            hlir::cmp_expr::rel{
              hlir::cmp_expr::rel_kind::eq,
              make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 23}},
                make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
                std::vector<hlir::call_expr::arg>{}
              ),
              src_loc{f, {1, 16}, {1, 18}}
            }
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_and) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::and_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 && 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_xor) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 ^^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 || 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(or_neq) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::neq_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 != 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_add) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 + 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_sub) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 - 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_mul) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 * 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_div) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_mod) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_bor) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_band) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 || 0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_neg) {
  do_parser_test(
    u8"let x: u64 = -0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_not) {
  do_parser_test(
    u8"let x: u64 = !0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_addr) {
  do_parser_test(
    u8"let x: u64 = &0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_deref) {
  do_parser_test(
    u8"let x: u64 = *0 || 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_member) {
  do_parser_test(
    u8"let x: u64 = 0 || $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(or_call) {
  do_parser_test(
    u8"let x: u64 = 0 || 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_xor) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 ^^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 && 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(and_neq) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::neq_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 != 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_add) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 + 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_sub) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 - 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_mul) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 * 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_div) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_mod) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_bor) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_band) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 && 0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_neg) {
  do_parser_test(
    u8"let x: u64 = -0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_not) {
  do_parser_test(
    u8"let x: u64 = !0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_addr) {
  do_parser_test(
    u8"let x: u64 = &0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_deref) {
  do_parser_test(
    u8"let x: u64 = *0 && 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_member) {
  do_parser_test(
    u8"let x: u64 = 0 && $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(and_call) {
  do_parser_test(
    u8"let x: u64 = 0 && 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_neq) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::neq_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 != 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_add) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 + 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_sub) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 - 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_mul) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 * 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_div) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_mod) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_bor) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_band) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_neg) {
  do_parser_test(
    u8"let x: u64 = -0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_not) {
  do_parser_test(
    u8"let x: u64 = !0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_addr) {
  do_parser_test(
    u8"let x: u64 = &0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_deref) {
  do_parser_test(
    u8"let x: u64 = *0 ^^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_member) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(xor_call) {
  do_parser_test(
    u8"let x: u64 = 0 ^^ 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_add) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 + 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_sub) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 - 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_mul) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 * 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_div) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_mod) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_bor) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_band) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 != 0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_neg) {
  do_parser_test(
    u8"let x: u64 = -0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_not) {
  do_parser_test(
    u8"let x: u64 = !0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_addr) {
  do_parser_test(
    u8"let x: u64 = &0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_deref) {
  do_parser_test(
    u8"let x: u64 = *0 != 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_member) {
  do_parser_test(
    u8"let x: u64 = 0 != $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neq_call) {
  do_parser_test(
    u8"let x: u64 = 0 != 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_sub) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 - 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_mul) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mul_expr>(src_loc{f, {1, 18}, {1, 23}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 * 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_div) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::div_expr>(src_loc{f, {1, 18}, {1, 23}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_mod) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mod_expr>(src_loc{f, {1, 18}, {1, 23}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_bor) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 + 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_band) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 + 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 + 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 + 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 + 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 + 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 + 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 + 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(add_neg) {
  do_parser_test(
    u8"let x: u64 = -0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_not) {
  do_parser_test(
    u8"let x: u64 = !0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_addr) {
  do_parser_test(
    u8"let x: u64 = &0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_deref) {
  do_parser_test(
    u8"let x: u64 = *0 + 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_member) {
  do_parser_test(
    u8"let x: u64 = 0 + $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
            make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(add_call) {
  do_parser_test(
    u8"let x: u64 = 0 + 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_mul) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mul_expr>(src_loc{f, {1, 18}, {1, 23}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 * 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_div) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::div_expr>(src_loc{f, {1, 18}, {1, 23}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_mod) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::mod_expr>(src_loc{f, {1, 18}, {1, 23}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_bor) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 - 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_band) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 - 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 - 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 - 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 - 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 - 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 - 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 - 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_neg) {
  do_parser_test(
    u8"let x: u64 = -0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_not) {
  do_parser_test(
    u8"let x: u64 = !0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_addr) {
  do_parser_test(
    u8"let x: u64 = &0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_deref) {
  do_parser_test(
    u8"let x: u64 = *0 - 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_member) {
  do_parser_test(
    u8"let x: u64 = 0 - $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
            make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(sub_call) {
  do_parser_test(
    u8"let x: u64 = 0 - 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_div) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 / 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_mod) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_bor) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 * 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_band) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 * 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 * 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 * 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 * 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 * 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 * 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 * 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_neg) {
  do_parser_test(
    u8"let x: u64 = -0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_not) {
  do_parser_test(
    u8"let x: u64 = !0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_addr) {
  do_parser_test(
    u8"let x: u64 = &0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_deref) {
  do_parser_test(
    u8"let x: u64 = *0 * 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_member) {
  do_parser_test(
    u8"let x: u64 = 0 * $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
            make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mul_call) {
  do_parser_test(
    u8"let x: u64 = 0 * 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_mod) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 % 0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_bor) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 / 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(div_band) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 / 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(div_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 / 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(div_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 / 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(div_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 / 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(div_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 / 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(div_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 / 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 / 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(div_neg) {
  do_parser_test(
    u8"let x: u64 = -0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_not) {
  do_parser_test(
    u8"let x: u64 = !0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_addr) {
  do_parser_test(
    u8"let x: u64 = &0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_deref) {
  do_parser_test(
    u8"let x: u64 = *0 / 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_member) {
  do_parser_test(
    u8"let x: u64 = 0 / $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
            make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(div_call) {
  do_parser_test(
    u8"let x: u64 = 0 / 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_bor) {
  do_parser_test(
    u8"let x: u64 = 0 % 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 | 0 % 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_band) {
  do_parser_test(
    u8"let x: u64 = 0 % 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 % 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 % 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 % 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 % 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 % 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 % 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 % 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 % 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 % 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 % 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 % 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_neg) {
  do_parser_test(
    u8"let x: u64 = -0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_not) {
  do_parser_test(
    u8"let x: u64 = !0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_addr) {
  do_parser_test(
    u8"let x: u64 = &0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_deref) {
  do_parser_test(
    u8"let x: u64 = *0 % 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_member) {
  do_parser_test(
    u8"let x: u64 = 0 % $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
            make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(mod_call) {
  do_parser_test(
    u8"let x: u64 = 0 % 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_band) {
  do_parser_test(
    u8"let x: u64 = 0 | 0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::band_expr>(src_loc{f, {1, 18}, {1, 23}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 & 0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 23}},
          make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
            make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 | 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 | 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 | 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 | 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 | 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 | 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_neg) {
  do_parser_test(
    u8"let x: u64 = -0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_not) {
  do_parser_test(
    u8"let x: u64 = !0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_addr) {
  do_parser_test(
    u8"let x: u64 = &0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_deref) {
  do_parser_test(
    u8"let x: u64 = *0 | 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_member) {
  do_parser_test(
    u8"let x: u64 = 0 | $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
            make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bor_call) {
  do_parser_test(
    u8"let x: u64 = 0 | 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(band_bxor) {
  do_parser_test(
    u8"let x: u64 = 0 & 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 ^ 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 21}})
    )
  );
}

BOOST_AUTO_TEST_CASE(band_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 & 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(band_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 & 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(band_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 & 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(band_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 & 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 & 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(band_neg) {
  do_parser_test(
    u8"let x: u64 = -0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(band_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(band_not) {
  do_parser_test(
    u8"let x: u64 = !0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(band_addr) {
  do_parser_test(
    u8"let x: u64 = &0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(band_deref) {
  do_parser_test(
    u8"let x: u64 = *0 & 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(band_member) {
  do_parser_test(
    u8"let x: u64 = 0 & $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
            make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(band_call) {
  do_parser_test(
    u8"let x: u64 = 0 & 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_lsh) {
  do_parser_test(
    u8"let x: u64 = 0 ^ 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 << 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 ^ 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 ^ 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 ^ 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 20}, {1, 22}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 ^ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 22}})
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_neg) {
  do_parser_test(
    u8"let x: u64 = -0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_not) {
  do_parser_test(
    u8"let x: u64 = !0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_addr) {
  do_parser_test(
    u8"let x: u64 = &0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_deref) {
  do_parser_test(
    u8"let x: u64 = *0 ^ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_member) {
  do_parser_test(
    u8"let x: u64 = 0 ^ $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 24}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
            make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bxor_call) {
  do_parser_test(
    u8"let x: u64 = 0 ^ 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
            make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_rsh) {
  do_parser_test(
    u8"let x: u64 = 0 << 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >> 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 << 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 << 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 << 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_neg) {
  do_parser_test(
    u8"let x: u64 = -0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_not) {
  do_parser_test(
    u8"let x: u64 = !0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_addr) {
  do_parser_test(
    u8"let x: u64 = &0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_deref) {
  do_parser_test(
    u8"let x: u64 = *0 << 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_member) {
  do_parser_test(
    u8"let x: u64 = 0 << $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lsh_call) {
  do_parser_test(
    u8"let x: u64 = 0 << 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_lrot) {
  do_parser_test(
    u8"let x: u64 = 0 >> 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 </ 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 >> 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 >> 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_neg) {
  do_parser_test(
    u8"let x: u64 = -0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_not) {
  do_parser_test(
    u8"let x: u64 = !0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_addr) {
  do_parser_test(
    u8"let x: u64 = &0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_deref) {
  do_parser_test(
    u8"let x: u64 = *0 >> 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_member) {
  do_parser_test(
    u8"let x: u64 = 0 >> $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rsh_call) {
  do_parser_test(
    u8"let x: u64 = 0 >> 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_rrot) {
  do_parser_test(
    u8"let x: u64 = 0 </ 0 >/ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );

  do_parser_test(
    u8"let x: u64 = 0 >/ 0 </ 0;", f,
    syms(),
    diags(
      std::make_unique<diag::var_def_semicolon_expected>(
        src_loc{f, {1, 21}, {1, 23}})
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_neg) {
  do_parser_test(
    u8"let x: u64 = -0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_not) {
  do_parser_test(
    u8"let x: u64 = !0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_addr) {
  do_parser_test(
    u8"let x: u64 = &0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_deref) {
  do_parser_test(
    u8"let x: u64 = *0 </ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_member) {
  do_parser_test(
    u8"let x: u64 = 0 </ $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(lrot_call) {
  do_parser_test(
    u8"let x: u64 = 0 </ 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_neg) {
  do_parser_test(
    u8"let x: u64 = -0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_bnot) {
  do_parser_test(
    u8"let x: u64 = ~0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_not) {
  do_parser_test(
    u8"let x: u64 = !0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_addr) {
  do_parser_test(
    u8"let x: u64 = &0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
            make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              false
            ),
            false
          ),
          make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_deref) {
  do_parser_test(
    u8"let x: u64 = *0 >/ 0;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
          ),
          make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_member) {
  do_parser_test(
    u8"let x: u64 = 0 >/ $y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 25}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
            make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(rrot_call) {
  do_parser_test(
    u8"let x: u64 = 0 >/ 0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
          make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
            make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neg_member) {
  do_parser_test(
    u8"let x: u64 = -$y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
            make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(neg_call) {
  do_parser_test(
    u8"let x: u64 = -0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 18}},
          make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bnot_member) {
  do_parser_test(
    u8"let x: u64 = ~$y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
            make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(bnot_call) {
  do_parser_test(
    u8"let x: u64 = ~0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 18}},
          make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(not_member) {
  do_parser_test(
    u8"let x: u64 = !$y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
            make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(not_call) {
  do_parser_test(
    u8"let x: u64 = !0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 18}},
          make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(addr_member) {
  do_parser_test(
    u8"let x: u64 = &$y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
            make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
            u8"foo"
          ),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&$y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 22}},
          make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 22}},
            make<hlir::named_member_expr>(src_loc{f, {1, 16}, {1, 22}},
              make<hlir::named_var_ref>(src_loc{f, {1, 16}, {1, 18}}, u8"y"),
              u8"foo"
            ),
            false
          ),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(addr_call) {
  do_parser_test(
    u8"let x: u64 = &0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 18}},
          make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            std::vector<hlir::call_expr::arg>{}
          ),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );

  do_parser_test(
    u8"let x: u64 = &&0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 19}},
          make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 19}},
            make<hlir::call_expr>(src_loc{f, {1, 16}, {1, 19}},
              make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
              std::vector<hlir::call_expr::arg>{}
            ),
            false
          ),
          false
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(deref_member) {
  do_parser_test(
    u8"let x: u64 = *$y.foo;", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 21}},
          make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
            make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
            u8"foo"
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_CASE(deref_call) {
  do_parser_test(
    u8"let x: u64 = *0();", f,
    syms(
      sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
        make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
          int_width_t{64},
          false
        ),
        make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 18}},
          make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
            make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
            std::vector<hlir::call_expr::arg>{}
          )
        ),
        hlir::var_def::mutable_spec_t::none
      )}
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // precedence

BOOST_AUTO_TEST_SUITE_END() // parser
BOOST_AUTO_TEST_SUITE_END() // test_nycleus
