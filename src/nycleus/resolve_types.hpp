#ifndef NYCLEUS_RESOLVE_TYPES_HPP_INCLUDED_IQ464OMV2RTMPQDNQR1SW270P
#define NYCLEUS_RESOLVE_TYPES_HPP_INCLUDED_IQ464OMV2RTMPQDNQR1SW270P

#include "util/stackful_coro_fwd.hpp"

namespace nycleus {

namespace hlir {
  struct type;
}
class diagnostic_tracker;
struct class_type;
class entity_bag;
struct function;
struct global_var;
struct type;
class type_registry;

namespace detail_ {

/** @brief Determines the type named by the given type name.
 *
 * This is an internal implementation detail of type resolution tasks.
 *
 * @returns The resolved type, or a null pointer if the type name does not name
 *          a type.
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::bad_exception
 * @throws diagnostic_tracker_error */
util::stackful_coro::result<type*> resolve_type(
  util::stackful_coro::context,
  hlir::type const&,
  diagnostic_tracker&,
  entity_bag const&,
  type_registry&
);

} // detail_

/** @brief A task, called by task_runner, to resolve the types of a function's
 * parameters and local variables, as well as its return type.
 *
 * If this throws, the task may not be fully completed, but no incorrect
 * modifications will have been made.
 *
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::bad_exception
 * @throws diagnostic_tracker_error */
util::stackful_coro::result<> resolve_fn(
  util::stackful_coro::context,
  function&,
  diagnostic_tracker&,
  entity_bag const&,
  type_registry&
);

/** @brief A task, called by task_runner, to resolve the type of a global
 * variable.
 *
 * If this throws, the task may not be fully completed, but no incorrect
 * modifications will have been made.
 *
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::bad_exception
 * @throws diagnostic_tracker_error */
util::stackful_coro::result<> resolve_global_var(
  util::stackful_coro::context,
  global_var&,
  diagnostic_tracker&,
  entity_bag const&,
  type_registry&
);

/** @brief A task, called by task_runner, to resolve the types of a class's
 * field.
 *
 * If this throws, the task may not be fully completed, but no incorrect
 * modifications will have been made.
 *
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::bad_exception
 * @throws diagnostic_tracker_error */
util::stackful_coro::result<> resolve_class(
  util::stackful_coro::context ctx,
  class_type& cl,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  type_registry& tr
);

} // nycleus

#endif
