#ifndef NYCLEUS_DIAGNOSTIC_PRINT_HPP_INCLUDED_PTK2HLJ9Z3PJJAZYW6BZMJN7R
#define NYCLEUS_DIAGNOSTIC_PRINT_HPP_INCLUDED_PTK2HLJ9Z3PJJAZYW6BZMJN7R

#include "nycleus/diagnostic.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/global_name.hpp"
#include "nycleus/semantics.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/type_utils.hpp"
#include "util/downcast.hpp"
#include "util/format.hpp"
#include <algorithm>
#include <atomic>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <ranges>
#include <string>
#include <string_view>
#include <utility>
#include <variant>

namespace nycleus {

/** @brief Kinds of diagnostic messages that can be produced as a result of
 * printing diagnostics. */
enum class diag_message_kind { error, warning, notice };

/** @brief A diagnostic message handler is supplied to the diagnostic printing
 * routine to consume the produced diagnostic messages. */
template<typename T>
concept diag_message_handler
  = requires(T& t, source_location loc, diag_message_kind kind) {
    { t.message_start(loc, kind) } -> std::output_iterator<char8_t>;
    t.message_end();
  };

namespace detail_ {

void print_diagnostic_sv(
  diag_message_handler auto& handler,
  source_location loc,
  diag_message_kind kind,
  std::u8string_view sv
) {
  std::ranges::copy(sv, handler.message_start(std::move(loc), kind));
  handler.message_end();
}

// Printing lexer errors ///////////////////////////////////////////////////////

void print_diagnostic_impl(
  diag::bidi_code_in_string e,
  diag_message_handler auto& handler
) {
  auto out{handler.message_start(std::move(e.loc), diag_message_kind::error)};

  using namespace std::string_view_literals;
  using enum diag::bidi_code_in_string::bidi_code_t;

  switch(e.code) {
    case str_lre:
    case char_lre:
      std::ranges::copy(u8"U+202A LEFT-TO-RIGHT EMBEDDING"sv, out);
      break;

    case str_rle:
    case char_rle:
      std::ranges::copy(u8"U+202B RIGHT-TO-LEFT EMBEDDING"sv, out);
      break;

    case str_pdf:
    case char_pdf:
      std::ranges::copy(u8"U+202C POP DIRECTIONAL FORMATTING"sv, out);
      break;

    case str_lro:
    case char_lro:
      std::ranges::copy(u8"U+202D LEFT-TO-RIGHT OVERRIDE"sv, out);
      break;

    case str_rlo:
    case char_rlo:
      std::ranges::copy(u8"U+202E RIGHT-TO-LEFT OVERRIDE"sv, out);
      break;

    case str_lri:
    case char_lri:
      std::ranges::copy(u8"U+2066 LEFT-TO-RIGHT ISOLATE"sv, out);
      break;

    case str_rli:
    case char_rli:
      std::ranges::copy(u8"U+2067 RIGHT-TO-LEFT ISOLATE"sv, out);
      break;

    case str_fsi:
    case char_fsi:
      std::ranges::copy(u8"U+2068 FIRST STRONG ISOLATE"sv, out);
      break;

    case str_pdi:
    case char_pdi:
      std::ranges::copy(u8"U+2069 POP DIRECTIONAL ISOLATE"sv, out);
      break;
  }

  std::ranges::copy(u8" in "sv, out);

  switch(e.code) {
    case str_lre:
    case str_rle:
    case str_pdf:
    case str_lro:
    case str_rlo:
    case str_lri:
    case str_rli:
    case str_fsi:
    case str_pdi:
      std::ranges::copy(u8"string"sv, out);
      break;

    case char_lre:
    case char_rle:
    case char_pdf:
    case char_lro:
    case char_rlo:
    case char_lri:
    case char_rli:
    case char_fsi:
    case char_pdi:
      std::ranges::copy(u8"character"sv, out);
      break;
  }

  std::ranges::copy(u8" literal is not allowed"sv, out);
  handler.message_end();
}

void print_diagnostic_impl(
  diag::comment_unmatched_bidi_code e,
  diag_message_handler auto& handler
) {
  auto out{handler.message_start(std::move(e.loc), diag_message_kind::error)};

  using namespace std::string_view_literals;
  using enum diag::comment_unmatched_bidi_code::bidi_code_t;

  std::ranges::copy(u8"unmatched "sv, out);

  switch(e.code) {
    case lre:
      std::ranges::copy(u8"U+202A LEFT-TO-RIGHT EMBEDDING"sv, out);
      break;

    case rle:
      std::ranges::copy(u8"U+202B RIGHT-TO-LEFT EMBEDDING"sv, out);
      break;

    case pdf:
      std::ranges::copy(u8"U+202C POP DIRECTIONAL FORMATTING"sv, out);
      break;

    case lro:
      std::ranges::copy(u8"U+202D LEFT-TO-RIGHT OVERRIDE"sv, out);
      break;

    case rlo:
      std::ranges::copy(u8"U+202E RIGHT-TO-LEFT OVERRIDE"sv, out);
      break;

    case lri:
      std::ranges::copy(u8"U+2066 LEFT-TO-RIGHT ISOLATE"sv, out);
      break;

    case rli:
      std::ranges::copy(u8"U+2067 RIGHT-TO-LEFT ISOLATE"sv, out);
      break;

    case fsi:
      std::ranges::copy(u8"U+2068 FIRST STRONG ISOLATE"sv, out);
      break;

    case pdi:
      std::ranges::copy(u8"U+2068 POP DIRECTIONAL ISOLATE"sv, out);
      break;
  }

  std::ranges::copy(u8" in comment", out);
  handler.message_end();
}

void print_diagnostic_impl(
  diag::empty_id e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error, u8"\"$\" is not a valid identifier");
}

void print_diagnostic_impl(
  diag::empty_ext e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error, u8"\"_\" is not a valid extension word");
}

void print_diagnostic_impl(
  diag::invalid_escape_code e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error, u8"invalid escape code");
}

void print_diagnostic_impl(
  diag::num_literal_base_prefix_and_suffix e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"base prefix and suffix cannot be present at the same time");
}

void print_diagnostic_impl(
  diag::num_literal_invalid e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"invalid numeric literal");
}

void print_diagnostic_impl(
  diag::num_literal_suffix_missing_width e,
  diag_message_handler auto& handler
) {
  auto out{handler.message_start(std::move(e.loc), diag_message_kind::error)};

  using namespace std::string_view_literals;
  using enum diag::num_literal_suffix_missing_width::prefix_letter_t;

  std::ranges::copy(u8"expected a type width specifier after \""sv, out);
  switch(e.prefix_letter) {
    case lower_s: std::ranges::copy(u8"s"sv, out); break;
    case lower_u: std::ranges::copy(u8"u"sv, out); break;
    case upper_s: std::ranges::copy(u8"S"sv, out); break;
    case upper_u: std::ranges::copy(u8"U"sv, out); break;
  }
  std::ranges::copy(u8"\""sv, out);
  handler.message_end();
}

void print_diagnostic_impl(
  diag::num_literal_suffix_too_wide e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"integer type width is too big");
}

void print_diagnostic_impl(
  diag::num_literal_suffix_zero_width e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"integer type width is zero");
}

void print_diagnostic_impl(
  diag::unexpected_code_point e,
  diag_message_handler auto& handler
) {
  auto out{handler.message_start(std::move(e.loc), diag_message_kind::error)};

  using namespace std::string_view_literals;

  std::ranges::copy(u8"unexpected code point: U+"sv, out);

  char8_t str[6] = {u8'0', u8'0', u8'0', u8'0'};
  std::size_t i{0};
  std::uint32_t value{static_cast<std::uint32_t>(e.code_point)};
  while(value > 0) {
    std::uint32_t const next_digit{value % 16};
    if(next_digit == 10) {
      str[i++] = u8'A';
    } else if(next_digit == 11) {
      str[i++] = u8'B';
    } else if(next_digit == 12) {
      str[i++] = u8'C';
    } else if(next_digit == 13) {
      str[i++] = u8'D';
    } else if(next_digit == 14) {
      str[i++] = u8'E';
    } else if(next_digit == 15) {
      str[i++] = u8'F';
    } else {
      str[i++] = u8'0' + next_digit;
    }
    value /= 16;
    assert(i <= 6);
  }

  for(std::size_t const i: std::views::iota(static_cast<std::size_t>(0),
      std::max(static_cast<std::size_t>(4), i)) | std::views::reverse) {
    *out++ = str[i];
  }

  handler.message_end();
}

void print_diagnostic_impl(
  diag::unexpected_zwj e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"unexpected U+200D ZERO WIDTH JOINER inside a word");
}

void print_diagnostic_impl(
  diag::unexpected_zwnj e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"unexpected U+200C ZERO WIDTH NON-JOINER inside a word");
}

void print_diagnostic_impl(
  diag::unterminated_char e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error, u8"unterminated character literal");
}

void print_diagnostic_impl(
  diag::unterminated_comment e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"unterminated multiline comment");
}

void print_diagnostic_impl(
  diag::unterminated_string e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error, u8"unterminated string literal");
}

// Printing parser errors //////////////////////////////////////////////////////

void print_diagnostic_impl(
  diag::assign_semicolon_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"\";\" expected");
}

void print_diagnostic_impl(
  diag::bad_comparison_chain e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error, u8"comparison operator chain contains operators "
    u8"going in different directions");

  using namespace std::string_view_literals;

  auto print_lt_notice{[&]() {
    auto out{handler.message_start(std::move(e.lt_loc),
      diag_message_kind::notice)};
    *out++ = u8'\"';
    *out++ = u8'<';
    if(!e.lt_strict) {
      *out++ = u8'=';
    }
    std::ranges::copy(u8"\" here"sv, out);
    handler.message_end();
  }};

  auto print_gt_notice{[&]() {
    auto out{handler.message_start(std::move(e.gt_loc),
      diag_message_kind::notice)};
    *out++ = u8'\"';
    *out++ = u8'>';
    if(!e.gt_strict) {
      *out++ = u8'=';
    }
    std::ranges::copy(u8"\" here"sv, out);
    handler.message_end();
  }};

  if(e.lt_before_gt) {
    print_lt_notice();
    print_gt_notice();
  } else {
    print_gt_notice();
    print_lt_notice();
  }
}

void print_diagnostic_impl(
  diag::bad_version e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"version must be \"0\"");
}

void print_diagnostic_impl(
  diag::block_rbrace_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"';' or '}' expected");
}

void print_diagnostic_impl(
  diag::call_expr_arg_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"',' or ')' expected");
}

void print_diagnostic_impl(
  diag::class_def_body_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"'{' expected");
}

void print_diagnostic_impl(
  diag::class_def_name_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"class name expected");
}

void print_diagnostic_impl(
  diag::class_def_rbrace_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"'}' expected");
}

void print_diagnostic_impl(
  diag::def_or_directive_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"expected a directive or a definition");
}

void print_diagnostic_impl(
  diag::expr_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"expression expected");
}

void print_diagnostic_impl(
  diag::expr_rparen_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"')' expected");
}

void print_diagnostic_impl(
  diag::fn_def_body_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"'{' expected");
}

void print_diagnostic_impl(
  diag::fn_def_lparen_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"'(' expected");
}

void print_diagnostic_impl(
  diag::fn_def_name_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"function name expected");
}

void print_diagnostic_impl(
  diag::fn_def_param_name_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"parameter name or ':' expected");
}

void print_diagnostic_impl(
  diag::fn_def_param_next_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"',' or ')' expected");
}

void print_diagnostic_impl(
  diag::fn_def_param_type_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"':' expected");
}

void print_diagnostic_impl(
  diag::fn_type_lparen_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"'(' expected");
}

void print_diagnostic_impl(
  diag::fn_type_param_name_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"parameter name or ':' expected");
}

void print_diagnostic_impl(
  diag::fn_type_param_next_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"',' or ')' expected");
}

void print_diagnostic_impl(
  diag::fn_type_param_type_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"':' expected");
}

void print_diagnostic_impl(
  diag::fn_type_rparen_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"')' expected");
}

void print_diagnostic_impl(
  diag::int_type_too_wide e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"integer type width is too big");
}

void print_diagnostic_impl(
  diag::int_type_zero_width e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"integer type width is zero");
}

void print_diagnostic_impl(
  diag::member_expr_name_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"member name expected");
}

void print_diagnostic_impl(
  diag::type_name_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"type name expected");
}

void print_diagnostic_impl(
  diag::unexpected_version e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"version directive must be at the beginning of the file");
}

void print_diagnostic_impl(
  diag::var_def_init_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"'=' or ';' expected");
}

void print_diagnostic_impl(
  diag::var_def_name_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"variable name expected");
}

void print_diagnostic_impl(
  diag::var_def_semicolon_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"';' expected");
}

void print_diagnostic_impl(
  diag::var_def_type_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"':' expected");
}

void print_diagnostic_impl(
  diag::version_semicolon_expected e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"';' expected");
}

void print_diagnostic_impl(
  diag::version_type_suffix e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"version number cannot have a type suffix");
}

// Printing semantic errors ////////////////////////////////////////////////////

void print_diagnostic_impl(
  diag::addr_mut e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"cannot obtain a mutable pointer to an immutable expression");
}

void print_diagnostic_impl(
  diag::assign_to_immut e,
  diag_message_handler auto& handler
) {
  assert(e.stmt);

  assert(e.stmt->target);
  [[maybe_unused]] auto const* const target_sem{
    std::get_if<value_semantics>(&e.stmt->target->sem)};
  assert(target_sem);

  detail_::print_diagnostic_sv(handler, e.stmt->target->loc,
    diag_message_kind::error,
    u8"assignment target must be a mutable expression");
}

void print_diagnostic_impl(
  diag::assign_to_transient e,
  diag_message_handler auto& handler
) {
  assert(e.stmt);

  assert(e.stmt->target);
  [[maybe_unused]] auto const* const target_sem{
    std::get_if<value_semantics>(&e.stmt->target->sem)};
  assert(target_sem);

  detail_::print_diagnostic_sv(handler, e.stmt->target->loc,
    diag_message_kind::error,
    u8"assignment target must be a persistent expression");
}

void print_diagnostic_impl(
  diag::call_expr_arg_count_mismatch e,
  diag_message_handler auto& handler
) {
  assert(e.expr);

  assert(e.expr->callee);
  auto const* callee_sem{std::get_if<value_semantics>(&e.expr->callee->sem)};
  assert(callee_sem);
  auto const& callee_type{util::downcast<fn_type const&>(*callee_sem->type)};

  if(auto const* fn_ref{dynamic_cast<hlir::function_ref const*>(
      e.expr->callee.get())}) {
    util::format_to<
      u8"function {} called with wrong number of arguments: "
      u8"{} expected, {} provided"
    >(
      handler.message_start(e.expr->loc, diag_message_kind::error),
      fn_ref->target->name,
      callee_type.param_types.size(),
      e.expr->args.size()
    );
    handler.message_end();
  } else {
    util::format_to<
      u8"function call with wrong number of arguments: {} expected, {} provided"
    >(
      handler.message_start(e.expr->loc, diag_message_kind::error),
      callee_type.param_types.size(),
      e.expr->args.size()
    );
    handler.message_end();
  }
}

void print_diagnostic_impl(
  diag::ct_int_div_by_zero e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  assert(e.expr->right);
  detail_::print_diagnostic_sv(handler, e.expr->right->loc,
    diag_message_kind::error, u8"compile-time integer division by zero");
}

void print_diagnostic_impl(
  diag::ct_int_mod_by_zero e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  assert(e.expr->right);
  detail_::print_diagnostic_sv(handler, e.expr->right->loc,
    diag_message_kind::error,
    u8"compile-time integer remainder operation with a zero divisor");
}

void print_diagnostic_impl(
  diag::ct_int_too_big e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"compile-time integer expression is too big");
}

void print_diagnostic_impl(
  diag::dep_cycle e,
  diag_message_handler auto& handler
) {
  assert(!e.cycle.empty());

  using msg_t = std::pair<std::u8string, source_location>;
  auto make_message{[](task_dep_info d) -> msg_t {
    return std::visit(util::overloaded{
      [](task_deps::class_cycles_prereq_resolve) -> msg_t {
        // This doesn't happen anyway (yet)
        return {u8"class_cycles_prereq_resolve", {}};
      },

      [](task_deps::analyzing_function_ref d) -> msg_t {
        return {
          util::format<u8"function {} mentioned here">(d.expr->target->name),
          d.expr->loc
        };
      },

      [](task_deps::analyzing_global_var_ref d) -> msg_t {
        return {
          util::format<u8"global variable {} mentioned here">(
            d.expr->target->name),
          d.expr->loc
        };
      },

      [](task_deps::analyzing_member_expr d) -> msg_t {
        return {
          util::format<u8"accessing field of class {} here">(d.cl->name),
          d.expr->loc
        };
      },

      [](task_deps::class_cycle d) -> msg_t {
        assert(d.index < d.cl->fields.size());
        return {
          util::format<u8"class {} has field \"{}\" of type {}">(
            d.cl->name,
            d.cl->fields[d.index].diag_name,
            d.cl->fields[d.index].type.assume_other()
          ),
          d.cl->fields[d.index].core_loc
        };
      }
    }, std::move(d));
  }};

  {
    msg_t msg{make_message(std::move(e.cycle.front()))};
    auto out{handler.message_start(std::move(msg.second),
      diag_message_kind::error)};
    out = std::ranges::copy(std::u8string_view{u8"cycle detected: "},
      std::move(out)).out;
    std::ranges::copy(msg.first, std::move(out));
    handler.message_end();
  }

  if(e.cycle.size() > 2) {
    for(auto& entry:
        std::ranges::subrange{e.cycle.begin() + 1, e.cycle.end() - 1}) {
      msg_t msg{make_message(std::move(entry))};
      detail_::print_diagnostic_sv(handler, std::move(msg.second),
        diag_message_kind::notice, msg.first);
    }
  }

  if(e.cycle.size() > 1) {
    msg_t msg{make_message(std::move(e.cycle.back()))};
    auto out{handler.message_start(std::move(msg.second),
      diag_message_kind::notice)};
    out = std::ranges::copy(msg.first, std::move(out)).out;
    std::ranges::copy(std::u8string_view{u8", completing the cycle"},
      std::move(out));
    handler.message_end();
  }
}

void print_diagnostic_impl(
  diag::duplicate_class_member e,
  diag_message_handler auto& handler
) {
  assert(!e.locations.empty());

  util::format_to<u8"redefinition of class member name: \"{}\"">(
    handler.message_start(e.locations.front(), diag_message_kind::error),
    e.name
  );
  handler.message_end();

  for(auto const& loc: e.locations | std::views::drop(1)) {
    detail_::print_diagnostic_sv(handler, loc, diag_message_kind::notice,
      u8"another definition here");
  }
}

void print_diagnostic_impl(
  diag::duplicate_global_def e,
  diag_message_handler auto& handler
) {
  assert(!e.locations.empty());

  util::format_to<u8"redefinition of \"{}\"">(
    handler.message_start(e.locations.front(), diag_message_kind::error),
    e.name
  );
  handler.message_end();

  for(auto const& loc: e.locations | std::views::drop(1)) {
    detail_::print_diagnostic_sv(handler, loc, diag_message_kind::notice,
      u8"another definition here");
  }
}

void print_diagnostic_impl(
  diag::duplicate_name_in_block e,
  diag_message_handler auto& handler
) {
  assert(!e.locations.empty());

  util::format_to<u8"redefinition of \"{}\" in the same block">(
    handler.message_start(e.locations.front(), diag_message_kind::error),
    e.name
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::duplicate_param_name e,
  diag_message_handler auto& handler
) {
  assert(!e.locations.empty());

  util::format_to<u8"duplicate parameter name: \"{}\"">(
    handler.message_start(e.locations.front(), diag_message_kind::error),
    e.name
  );
  handler.message_end();

  for(auto const& loc: e.locations | std::views::drop(1)) {
    detail_::print_diagnostic_sv(handler, loc, diag_message_kind::notice,
      u8"another definition here");
  }
}

void print_diagnostic_impl(
  diag::int_literal_too_big e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"integer literal is too big");
}

void print_diagnostic_impl(
  diag::int_literal_does_not_fit_type e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  util::format_to<
    u8"integer literal does not fit within the requested type; at least {} "
    u8"bits needed"
  >(
    handler.message_start(e.expr->loc, diag_message_kind::error),
    e.width
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::local_var_wrong_function e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  util::format_to<u8"local variable {} of function {} used in function {}">(
    handler.message_start(e.expr->loc, diag_message_kind::error),
    e.var->diag_name,
    e.var->fn->name,
    e.fn->name
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::member_access_bad_name e,
  diag_message_handler auto& handler
) {
  assert(e.expr);

  assert(e.expr->op);
  auto const* const op_sem{std::get_if<value_semantics>(&e.expr->op->sem)};
  assert(op_sem);
  auto const& op_type{util::downcast<class_type const&>(*op_sem->type)};

  util::format_to<u8"the name \"{}\" does not refer to a field of class {}">(
    handler.message_start(e.expr->loc, diag_message_kind::error),
    e.expr->name,
    op_type.name
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::name_is_not_type e,
  diag_message_handler auto& handler
) {
  assert(e.name);
  util::format_to<u8"the name \"{}\" does not refer to a type">(
    handler.message_start(e.name->loc, diag_message_kind::error),
    e.name->name
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::name_is_not_value e,
  diag_message_handler auto& handler
) {
  assert(e.name);
  util::format_to<u8"the name \"{}\" does not refer to a value">(
    handler.message_start(e.name->loc, diag_message_kind::error),
    e.name->name
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::name_undefined e,
  diag_message_handler auto& handler
) {
  assert(e.name);
  util::format_to<u8"undefined name \"{}\"">(
    handler.message_start(e.name->loc, diag_message_kind::error),
    e.name->name
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::too_many_nested_functions e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, e.loc, diag_message_kind::error,
    u8"too many nested functions");
}

void print_diagnostic_impl(
  diag::uninitialized_global_var e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, e.loc, diag_message_kind::error,
    u8"a global variable must be initialized");
}

// Printing errors about types /////////////////////////////////////////////////

void print_binary_expr_bad_types(
  auto e,
  diag_message_handler auto& handler,
  std::u8string_view op_name
) {
  assert(e.expr);

  assert(e.expr->left);
  auto const* const left_sem
    = std::get_if<value_semantics>(&e.expr->left->sem);
  assert(left_sem);
  auto const& left_type{*left_sem->type};

  assert(e.expr->right);
  auto const* const right_sem
    = std::get_if<value_semantics>(&e.expr->right->sem);
  assert(right_sem);
  auto const& right_type{*right_sem->type};

  util::format_to<u8"{} of operands of incompatible types: {} and {}">(
    handler.message_start(e.expr->loc, diag_message_kind::error),
    op_name,
    left_type,
    right_type
  );
  handler.message_end();
}

void print_unary_expr_bad_type(
  auto e,
  diag_message_handler auto& handler,
  std::u8string_view op_name
) {
  assert(e.expr);

  assert(e.expr->op);
  auto const* const op_sem = std::get_if<value_semantics>(&e.expr->op->sem);
  assert(op_sem);
  auto const& op_type{*op_sem->type};

  util::format_to<u8"{} of an operand of an incompatible type: {}">(
    handler.message_start(e.expr->loc, diag_message_kind::error),
    op_name,
    op_type
  );
  handler.message_end();
}

void print_expr_void(
  auto e,
  diag_message_handler auto& handler,
  std::u8string_view op_name
) {
  assert(e.expr);
  util::format_to<u8"{} involving a void operand">(
    handler.message_start(e.expr->loc, diag_message_kind::error),
    op_name
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::add_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"addition");
}

void print_diagnostic_impl(
  diag::add_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"addition");
}

void print_diagnostic_impl(
  diag::addr_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"address operation");
}

void print_diagnostic_impl(
  diag::assign_bad_types e,
  diag_message_handler auto& handler
) {
  assert(e.stmt);

  assert(e.stmt->target);
  auto const* const target_sem
    = std::get_if<value_semantics>(&e.stmt->target->sem);
  assert(target_sem);
  auto const& target_type{*target_sem->type};

  assert(e.stmt->value);
  auto const* const value_sem
    = std::get_if<value_semantics>(&e.stmt->value->sem);
  assert(value_sem);
  auto const& value_type{*value_sem->type};

  // TODO: Print location of assignment statement
  util::format_to<u8"assignment of a value of type {} to a target of type {}">(
    handler.message_start(e.stmt->target->loc, diag_message_kind::error),
    value_type, target_type
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::assign_from_void e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"assignment of a void expression");
}

void print_diagnostic_impl(
  diag::assign_to_void e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"assignment to a void expression");
}

void print_diagnostic_impl(
  diag::band_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"bitwise AND");
}

void print_diagnostic_impl(
  diag::band_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"bitwise AND");
}

void print_diagnostic_impl(
  diag::bnot_bad_type e,
  diag_message_handler auto& handler
) {
  detail_::print_unary_expr_bad_type(e, handler, u8"bitwise NOT");
}

void print_diagnostic_impl(
  diag::bnot_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"bitwise NOT");
}

void print_diagnostic_impl(
  diag::bor_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"bitwise OR");
}

void print_diagnostic_impl(
  diag::bor_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"bitwise OR");
}

void print_diagnostic_impl(
  diag::bxor_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"bitwise XOR");
}

void print_diagnostic_impl(
  diag::bxor_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"biwise XOR");
}

void print_diagnostic_impl(
  diag::call_expr_arg_bad_type e,
  diag_message_handler auto& handler
) {
  assert(e.expr);

  assert(e.expr->callee);
  [[maybe_unused]] auto const* callee_sem{
    std::get_if<value_semantics>(&e.expr->callee->sem)};
  assert(callee_sem);
  assert(dynamic_cast<fn_type const*>(callee_sem->type.get_ptr()));

  assert(e.arg);
  auto const* arg_sem{std::get_if<value_semantics>(&e.arg->sem)};
  assert(arg_sem);
  auto const& arg_type{*arg_sem->type};

  if(auto const* fn_ref{dynamic_cast<hlir::function_ref const*>(
      e.expr->callee.get())}) {
    util::format_to<
      u8"invalid argument type for calling function {}: "
      u8"{} expected, {} provided"
    >(
      handler.message_start(e.expr->loc, diag_message_kind::error),
      fn_ref->target->name,
      *e.param_type,
      arg_type
    );
    handler.message_end();
  } else {
    util::format_to<
      u8"invalid argument type for call: {} expected, {} provided"
    >(
      handler.message_start(e.expr->loc, diag_message_kind::error),
      *e.param_type,
      arg_type
    );
    handler.message_end();
  }
}

void print_diagnostic_impl(
  diag::call_expr_arg_void e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"void expression supplied as function argument");
}

void print_diagnostic_impl(
  diag::call_expr_callee_bad_type e,
  diag_message_handler auto& handler
) {
  assert(e.expr);

  assert(e.expr->callee);
  auto const* callee_sem{std::get_if<value_semantics>(&e.expr->callee->sem)};
  assert(callee_sem);
  auto const& callee_type{*callee_sem->type};

  util::format_to<
    u8"call expression's callee operand must be a function reference ({} given)"
  >(
    handler.message_start(e.expr->callee->loc, diag_message_kind::error),
    callee_type
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::call_expr_callee_void e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"call expression has a void callee");
}

void print_diagnostic_impl(
  diag::comparison_bad_types e,
  diag_message_handler auto& handler
) {
  assert(e.left);
  auto const* const left_sem = std::get_if<value_semantics>(&e.left->sem);
  assert(left_sem);
  auto const& left_type{*left_sem->type};

  assert(e.right);
  auto const* const right_sem = std::get_if<value_semantics>(&e.right->sem);
  assert(right_sem);
  auto const& right_type{*right_sem->type};

  util::format_to<u8"comparison of operands of incompatible types: {} and {}">(
    handler.message_start(e.loc, diag_message_kind::error),
    left_type,
    right_type
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::comparison_void e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"comparison involving a void operand");
}

void print_diagnostic_impl(
  diag::deref_bad_type e,
  diag_message_handler auto& handler
) {
  detail_::print_unary_expr_bad_type(e, handler, u8"dereferencing");
}

void print_diagnostic_impl(
  diag::deref_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"dereferencing");
}

void print_diagnostic_impl(
  diag::div_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"division");
}

void print_diagnostic_impl(
  diag::div_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"division");
}

void print_diagnostic_impl(
  diag::fn_return_bad_type e,
  diag_message_handler auto& handler
) {
  assert(e.fn->body);
  auto const* const body_sem{std::get_if<value_semantics>(&e.fn->body->sem)};
  assert(body_sem);
  auto const& body_type{*body_sem->type};

  assert(e.fn->return_type);

  util::format_to<
    u8"function {} returns a value of an incompatible type: "
    u8"{} declared, {} returned"
  >(
    handler.message_start(e.loc, diag_message_kind::error),
    e.fn->name,
    e.fn->return_type.assume_other(),
    body_type
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::fn_return_void e,
  diag_message_handler auto& handler
) {
  assert(e.fn->return_type);

  util::format_to<u8"function {} is declared to return {}, but returns void">(
    handler.message_start(e.loc, diag_message_kind::error),
    e.fn->name,
    e.fn->return_type.assume_other()
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::local_var_init_bad_type e,
  diag_message_handler auto& handler
) {
  assert(e.stmt);

  assert(e.stmt->value);
  auto const* const init_sem{std::get_if<value_semantics>(&e.stmt->value->sem)};
  assert(init_sem);
  auto const& init_type{*init_sem->type};

  // TODO: Print location of the variable initialization statement
  util::format_to<
    u8"cannot initialize variable {} of type {} with an initializer expression "
    u8"of type {}"
  >(
    handler.message_start(e.stmt->value->loc, diag_message_kind::error),
    e.stmt->target->diag_name,
    e.stmt->target->type.assume_other(),
    init_type
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::local_var_init_void e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  detail_::print_diagnostic_sv(handler, e.expr->loc, diag_message_kind::error,
    u8"cannot initialize a variable with a void expression");
}

void print_diagnostic_impl(
  diag::logical_and_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"logical AND");
}

void print_diagnostic_impl(
  diag::logical_and_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"logical AND");
}

void print_diagnostic_impl(
  diag::logical_or_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"logical OR");
}

void print_diagnostic_impl(
  diag::logical_or_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"logical OR");
}

void print_diagnostic_impl(
  diag::logical_xor_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"logical XOR");
}

void print_diagnostic_impl(
  diag::logical_xor_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"logical XOR");
}

void print_diagnostic_impl(
  diag::lrot_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"bitwise left rotation");
}

void print_diagnostic_impl(
  diag::lrot_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"bitwise left rotation");
}

void print_diagnostic_impl(
  diag::lsh_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"bitwise left shift");
}

void print_diagnostic_impl(
  diag::lsh_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"bitwise left shift");
}

void print_diagnostic_impl(
  diag::mod_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"remainder operation");
}

void print_diagnostic_impl(
  diag::member_access_bad_type e,
  diag_message_handler auto& handler
) {
  assert(e.expr);
  auto const* const sem = std::get_if<value_semantics>(&e.expr->sem);
  assert(sem);
  auto const& expr_type{*sem->type};

  util::format_to<
    u8"member access on an operand that is not of class type: {}"
  >(
    handler.message_start(e.expr->loc, diag_message_kind::error),
    expr_type
  );
  handler.message_end();
}

void print_diagnostic_impl(
  diag::member_access_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"member access");
}

void print_diagnostic_impl(
  diag::mod_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"remainder operation");
}

void print_diagnostic_impl(
  diag::mul_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"multiplication");
}

void print_diagnostic_impl(
  diag::mul_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"multiplication");
}

void print_diagnostic_impl(
  diag::neg_bad_type e,
  diag_message_handler auto& handler
) {
  detail_::print_unary_expr_bad_type(e, handler, u8"negation");
}

void print_diagnostic_impl(
  diag::neg_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"negation");
}

void print_diagnostic_impl(
  diag::neq_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"comparison");
}

void print_diagnostic_impl(
  diag::neq_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"comparison");
}

void print_diagnostic_impl(
  diag::not_bad_type e,
  diag_message_handler auto& handler
) {
  detail_::print_unary_expr_bad_type(e, handler, u8"logical NOT");
}

void print_diagnostic_impl(
  diag::not_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"logical NOT");
}

void print_diagnostic_impl(
  diag::rrot_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"bitwise right rotation");
}

void print_diagnostic_impl(
  diag::rrot_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"bitwise right rotation");
}

void print_diagnostic_impl(
  diag::rsh_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"bitwise right shift");
}

void print_diagnostic_impl(
  diag::rsh_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"bitwise right shift");
}

void print_diagnostic_impl(
  diag::sub_bad_types e,
  diag_message_handler auto& handler
) {
  detail_::print_binary_expr_bad_types(e, handler, u8"subtraction");
}

void print_diagnostic_impl(
  diag::sub_void e,
  diag_message_handler auto& handler
) {
  detail_::print_expr_void(e, handler, u8"subtraction");
}

// Printing errors about unsupported features //////////////////////////////////

void print_diagnostic_impl(
  diag::class_field_initializers_unsupported e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, e.init->loc,
    diag_message_kind::error,
    u8"class field initializers are not supported yet");
}

void print_diagnostic_impl(
  diag::fp_literals_unsupported e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, std::move(e.loc),
    diag_message_kind::error,
    u8"floating-point literals are not supported yet");
}

void print_diagnostic_impl(
  diag::methods_unsupported e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, e.def->core_loc,
    diag_message_kind::error,
    u8"methods are not supported yet");
}

void print_diagnostic_impl(
  diag::persistent_class_arg_unsupported e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, e.expr->loc,
    diag_message_kind::error,
    u8"persistent arguments of class type are not supported yet");
}

void print_diagnostic_impl(
  diag::persistent_class_init_unsupported e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, e.expr->loc,
    diag_message_kind::error,
    u8"using persistent expressions to initialize variables of class type "
    u8"is not supported yet");
}

void print_diagnostic_impl(
  diag::persistent_class_return_unsupported e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, e.expr->loc,
    diag_message_kind::error,
    u8"returning persistent expressions of class type is not supported yet");
}

void print_diagnostic_impl(
  diag::uninitialized_variables_unsupported e,
  diag_message_handler auto& handler
) {
  detail_::print_diagnostic_sv(handler, e.def->core_loc,
    diag_message_kind::error,
    u8"uninitialized variables are not supported yet");
}

// Printing warnings ///////////////////////////////////////////////////////////

void print_diagnostic_impl(
  diag::string_newline w,
  diag_message_handler auto& handler
) {
  auto out{handler.message_start(std::move(w.loc), diag_message_kind::warning)};

  using enum nycleus::diag::string_newline::newline_code_t;
  using namespace std::string_view_literals;

  switch(w.newline_code) {
    case str_lf:
    case char_lf:
      std::ranges::copy(u8"U+000A (line feed)"sv, out);
      break;

    case str_vt:
    case char_vt:
      std::ranges::copy(u8"U+000B (line tabulation)"sv, out);
      break;

    case str_ff:
    case char_ff:
      std::ranges::copy(u8"U+000C (form feed)"sv, out);
      break;

    case str_cr:
    case char_cr:
      std::ranges::copy(u8"U+000D (carriage return)"sv, out);
      break;

    case str_nel:
    case char_nel:
      std::ranges::copy(u8"U+0085 (next line)"sv, out);
      break;

    case str_line_sep:
    case char_line_sep:
      std::ranges::copy(u8"U+2028 LINE SEPARATOR"sv, out);
      break;

    case str_para_sep:
    case char_para_sep:
      std::ranges::copy(u8"U+2029 PARAGRAPH SEPARATOR"sv, out);
      break;
  }

  std::ranges::copy(u8" in a "sv, out);

  switch(w.newline_code) {
    case str_lf:
    case str_vt:
    case str_ff:
    case str_cr:
    case str_nel:
    case str_line_sep:
    case str_para_sep:
      std::ranges::copy(u8"string"sv, out);
      break;
    case char_lf:
    case char_vt:
    case char_ff:
    case char_cr:
    case char_nel:
    case char_line_sep:
    case char_para_sep:
      std::ranges::copy(u8"character"sv, out);
      break;
  }

  std::ranges::copy(u8" literal should be avoided"sv, out);
  handler.message_end();
}

} // detail_

// Public API //////////////////////////////////////////////////////////////////

/** @brief Converts a diagnostic into a sequence of human-readable messages.
 *
 * The messages are reported via the supplied message handler. For each message,
 * the `message_start` member function is called, being given the message's
 * location and kind (error, warning, or notice). The function returns an output
 * iterator, which is fed a sequence of `char8_t`s representing the message
 * text. After the entire message is fed, `message_end` is called. This is
 * repeated for each message. Each diagnostic turns into exactly one error or
 * warning message, depending on the diagnostic kind, followed by zero or more
 * notice messages.
 *
 * This function throws whatever is thrown by the handler's member functions or
 * operations on the output iterator. */
void print_diagnostic(
  diagnostic const& diag,
  diag_message_handler auto& handler
) {
  diag.const_visit([&](auto const& d) {
    detail_::print_diagnostic_impl(d, handler);
  });
}

} // nycleus

#endif
