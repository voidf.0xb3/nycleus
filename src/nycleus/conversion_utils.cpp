#include "nycleus/conversion_utils.hpp"
#include "nycleus/conversion.hpp"
#include "nycleus/entity.hpp"
#include <optional>

using namespace nycleus;

namespace {

[[nodiscard]] std::nullopt_t get_conversion_impl(auto const&, auto const&)
    noexcept {
  return std::nullopt;
}

[[nodiscard]] std::optional<conversion_t> get_conversion_impl(
  int_type const& from,
  int_type const& to
) noexcept {
  if(from.width >= to.width) {
    return std::nullopt;
  }

  if(from.is_signed) {
    if(to.is_signed) {
      return signed_int_extend{to.width};
    } else {
      return std::nullopt;
    }
  } else {
    if(to.is_signed) {
      return unsigned_to_signed{to.width};
    } else {
      return unsigned_int_extend{to.width};
    }
  }
}

[[nodiscard]] std::optional<ptr_mutability_conversion> get_conversion_impl(
  ptr_type const& from,
  ptr_type const& to
) noexcept {
  if(!from.is_mutable() && to.is_mutable()) {
    return std::nullopt;
  }

  if(from.pointee_type.get_ptr() == to.pointee_type.get_ptr()) {
    // If the pointee types are the same, and the pointer types are different,
    // then the mutabilities are different. The possibility of converting
    // immutable to mutable has already been excluded, so this must be a
    // conversion from mutable to immutable.
    return ptr_mutability_conversion{0};
  }

  if(to.is_mutable()) {
    // Removing mutability on more interior pointer types is only allowed if
    // the exterior pointer types on the destination type are not mutable. If
    // they are mutable, we cannot convert.
    return std::nullopt;
  }

  if(
    from.pointee_type->get_kind() != type::kind_t::ptr_type
    || to.pointee_type->get_kind() != type::kind_t::ptr_type
  ) {
    return std::nullopt;
  }

  auto pointee_conversion{get_conversion_impl(
    static_cast<ptr_type const&>(*from.pointee_type),
    static_cast<ptr_type const&>(*to.pointee_type)
  )};

  if(pointee_conversion && from.is_mutable() && !to.is_mutable()) {
    ++pointee_conversion->extra_levels;
  }

  return pointee_conversion;
}

} // (anonymous)

std::optional<conversion_t> nycleus::get_conversion(
  type const& from,
  type const& to
) noexcept {
  if(&from == &to) {
    return std::monostate{};
  }

  using result_t = std::optional<conversion_t>;
  return from.visit([&to](auto const& from) noexcept -> result_t {
    return to.visit([&from](auto const& to) noexcept -> result_t {
      return get_conversion_impl(from, to);
    });
  });
}
