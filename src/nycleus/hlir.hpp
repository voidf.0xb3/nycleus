#ifndef NYLCEUS_HLIR_HPP_INCLUDED_PUULJTOUFHK6LYMISC8MSVMLF
#define NYLCEUS_HLIR_HPP_INCLUDED_PUULJTOUFHK6LYMISC8MSVMLF

#include "nycleus/conversion.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/semantics.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/utils.hpp"
#include "util/bigint.hpp"
#include "util/pp_seq.hpp"
#include "util/non_null_ptr.hpp"
#include "util/small_vector.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <cassert>
#include <concepts>
#include <memory>
#include <optional>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

namespace nycleus {

struct function;
struct global_var;
struct local_var;
struct type;
struct class_type;

/** @brief This namespace contains structures encoding Nycleus high-level
 * intermediate representation (HLIR).
 *
 * HLIR structures do not encapsulate anything (they are simple data holders),
 * but they are arranged in a hierarchy (which implements a visitor pattern).
 * Some of them have restrictions (invariants) on their contents, which are
 * documented for each particular structure, but, being simple data holders,
 * they cannot enforce those restrictions the way proper classes do. It is the
 * responsibility of code that generates HILR to ensure that the restrictions
 * are satisfied. The appearance of invalid HLIR during the execution of a
 * compiler is always a bug.
 *
 * Some members contain computed data. Such data is uniquely determined by other
 * data in the tree and is, strictly speaking, redundant. It is cached in its
 * computed form to avoid recomputation. This means that the values of computed
 * members do not affect HLIR semantics, but some operations require, as a
 * precondition, that computed data be correct, as determined by said semantics.
 *
 * Some nodes have conditions for well-formedness. When these conditions are
 * violated, the HLIR tree represents an ill-formed program. It is perfectly
 * acceptable to operate on ill-formed HLIR for the purpose of discovering as
 * many diagnostics as possible. This is not to be confused with validity
 * requirements, as stated above, which must not be violated even on ill-formed
 * inputs. */
namespace hlir {

// Types ///////////////////////////////////////////////////////////////////////

#define HLIR_TYPES(x, y, z) \
  z(int_type) y \
  x(bool_type) y \
  x(fn_type) y \
  x(ptr_type) y \
  x(named_type) y \
  x(class_ref_type)

#define HLIR_TYPE_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(HLIR_TYPES, HLIR_TYPE_FWD_DEF)
#undef HLIR_TYPE_FWD_DEF

template<typename Visitor>
concept type_visitor =
  #define HLIR_TYPE_IS_INVOCABLE(kind) std::invocable<Visitor, kind&>
  PP_SEQ_APPLY(HLIR_TYPES, HLIR_TYPE_IS_INVOCABLE, &&)
  #undef HLIR_TYPE_IS_INVOCABLE
  && util::all_same_v<
    #define HLIR_TYPE_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind&>
    PP_SEQ_LIST(HLIR_TYPES, HLIR_TYPE_INVOKE_RESULT)
    #undef HLIR_TYPE_INVOKE_RESULT
  >;

template<typename Visitor>
concept type_const_visitor =
  #define HLIR_TYPE_IS_CONST_INVOCABLE(kind) \
    std::invocable<Visitor, kind const&>
  PP_SEQ_APPLY(HLIR_TYPES, HLIR_TYPE_IS_CONST_INVOCABLE, &&)
  #undef HLIR_TYPE_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define HLIR_TYPE_CONST_INVOKE_RESULT(kind) \
      std::invoke_result_t<Visitor&&, kind const&>
    PP_SEQ_LIST(HLIR_TYPES, HLIR_TYPE_CONST_INVOKE_RESULT)
    #undef HLIR_TYPE_CONST_INVOKE_RESULT
  >;

/** @brief The name of a data type. */
struct type: hlir_base, util::hier_base {
  source_location loc;

  explicit type(source_location loc) noexcept: loc{std::move(loc)} {}
  virtual ~type() noexcept = default;

  enum class kind_t {
    #define HLIR_TYPE_ENUM(kind) kind
    PP_SEQ_LIST(HLIR_TYPES, HLIR_TYPE_ENUM)
    #undef HLIR_TYPE_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<type_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_TYPES)&>;

private:
  template<type_visitor Visitor>
  static constexpr bool visit_noexcept{
    #define HLIR_TYPE_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind&>()))
    PP_SEQ_APPLY(HLIR_TYPES, HLIR_TYPE_INVOKE_NOEXCEPT, &&)
    #undef HLIR_TYPE_INVOKE_NOEXCEPT
  };

public:
  template<type_visitor Visitor>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(type::visit_noexcept<Visitor>);

  template<type_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor)
      noexcept(type::visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<type_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&,
    PP_SEQ_FIRST(HLIR_TYPES) const&>;

private:
  template<type_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define HLIR_TYPE_CONST_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), \
      std::declval<kind const&>()))
    PP_SEQ_APPLY(HLIR_TYPES, HLIR_TYPE_CONST_INVOKE_NOEXCEPT, &&)
    #undef HLIR_TYPE_CONST_INVOKE_NOEXCEPT
  };

public:
  template<type_const_visitor Visitor>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(type::const_visit_noexcept<Visitor>);

  template<type_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(type::const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief The name of an integer data type.
 *
 * Requirement: the width must be positive and must not exceed max_int_width. */
struct int_type final: type {
  int_width_t width;
  bool is_signed;

  explicit int_type(
    source_location loc,
    int_width_t width,
    bool is_signed
  ) noexcept: type{std::move(loc)}, width{width}, is_signed{is_signed} {
    assert(0 < width);
    assert(width <= max_int_width);
  }

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_type;
  }
};

/** @brief The name of the Boolean data type. */
struct bool_type final: type {
  explicit bool_type(source_location loc) noexcept: type{std::move(loc)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bool_type;
  }
};

/** @brief The name of a function data type.
 *
 * If any of the parameter type pointers is null, this HLIR is ill-formed. */
struct fn_type final: type {
  std::vector<hlir_ptr<type>> param_types;
  hlir_ptr<type> return_type;

  explicit fn_type(
    source_location loc,
    std::vector<hlir_ptr<type>> param_types,
    hlir_ptr<type> return_type
  ) noexcept:
    type{std::move(loc)},
    param_types{std::move(param_types)},
    return_type{std::move(return_type)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type;
  }
};

/** @brief The name of a pointer type.
 *
 * If the pointee type pointer is null, this HLIR is ill-formed. */
struct ptr_type final: type {
  hlir_ptr<type> pointee_type;
  bool is_mutable;

  explicit ptr_type(
    source_location loc,
    hlir_ptr<type> pointee_type,
    bool is_mutable
  ) noexcept:
    type{std::move(loc)},
    pointee_type{std::move(pointee_type)},
    is_mutable{is_mutable} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::ptr_type;
  }
};

/** @brief A data type referred to by name. */
struct named_type final: type {
  std::u8string name;

  explicit named_type(
    source_location loc,
    std::u8string name
  ) noexcept: type{std::move(loc)}, name{std::move(name)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::named_type;
  }
};

/** @brief A type name referring to a class. */
struct class_ref_type final: type {
  util::non_null_ptr<class_type> cl;

  explicit class_ref_type(
    source_location loc,
    class_type& cl
  ) noexcept: type{std::move(loc)}, cl{&cl} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_ref_type;
  }
};

template<type_visitor Visitor>
type::visit_result_t<Visitor> type::mutable_visit(Visitor&& visitor)
    noexcept(type::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_TYPE_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(HLIR_TYPES, HLIR_TYPE_VISIT)
    #undef HLIR_TYPE_VISIT
  }
}

template<type_const_visitor Visitor>
type::const_visit_result_t<Visitor> type::const_visit(Visitor&& visitor) const
    noexcept(type::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_TYPE_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(HLIR_TYPES, HLIR_TYPE_CONST_VISIT)
    #undef HLIR_TYPE_CONST_VISIT
  }
}

#undef HLIR_TYPES

// Expressions /////////////////////////////////////////////////////////////////

#define HLIR_EXPRS(x, y, z) \
  z(int_literal) y \
  x(bool_literal) y \
  x(named_var_ref) y \
  x(global_var_ref) y \
  x(local_var_ref) y \
  x(function_ref) y \
  x(cmp_expr) y \
  x(or_expr) y \
  x(and_expr) y \
  x(xor_expr) y \
  x(neq_expr) y \
  x(add_expr) y \
  x(sub_expr) y \
  x(mul_expr) y \
  x(div_expr) y \
  x(mod_expr) y \
  x(bor_expr) y \
  x(band_expr) y \
  x(bxor_expr) y \
  x(lsh_expr) y \
  x(rsh_expr) y \
  x(lrot_expr) y \
  x(rrot_expr) y \
  x(neg_expr) y \
  x(bnot_expr) y \
  x(not_expr) y \
  x(addr_expr) y \
  x(deref_expr) y \
  x(named_member_expr) y \
  x(indexed_member_expr) y \
  x(call_expr) y \
  x(block)

#define HLIR_EXPR_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(HLIR_EXPRS, HLIR_EXPR_FWD_DEF)
#undef HLIR_EXPR_FWD_DEF

template<typename Visitor>
concept expr_visitor =
  #define HLIR_EXPR_IS_INVOCABLE(kind) std::invocable<Visitor, kind&>
  PP_SEQ_APPLY(HLIR_EXPRS, HLIR_EXPR_IS_INVOCABLE, &&)
  #undef HLIR_EXPR_IS_INVOCABLE
  && util::all_same_v<
    #define HLIR_EXPR_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind&>
    PP_SEQ_LIST(HLIR_EXPRS, HLIR_EXPR_INVOKE_RESULT)
    #undef HLIR_EXPR_INVOKE_RESULT
  >;

template<typename Visitor>
concept expr_const_visitor =
  #define HLIR_EXPR_IS_CONST_INVOCABLE(kind) \
    std::invocable<Visitor, kind const&>
  PP_SEQ_APPLY(HLIR_EXPRS, HLIR_EXPR_IS_CONST_INVOCABLE, &&)
  #undef HLIR_EXPR_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define HLIR_EXPR_CONST_INVOKE_RESULT(kind) \
      std::invoke_result_t<Visitor&&, kind const&>
    PP_SEQ_LIST(HLIR_EXPRS, HLIR_EXPR_CONST_INVOKE_RESULT)
    #undef HLIR_EXPR_CONST_INVOKE_RESULT
  >;

/** @brief The base class for HLIR expressions.
 *
 * The `semantics` member contains computed data. */
struct expr: hlir_base, util::hier_base {
  source_location loc;
  semantics sem;

  explicit expr(source_location loc) noexcept: loc{std::move(loc)} {}

  virtual ~expr() noexcept = default;

  enum class kind_t {
    #define HLIR_EXPR_ENUM(kind) kind
    PP_SEQ_LIST(HLIR_EXPRS, HLIR_EXPR_ENUM)
    #undef HLIR_EXPR_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<expr_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_EXPRS)&>;

private:
  template<expr_visitor Visitor>
  static constexpr bool visit_noexcept {
    #define HLIR_EXPR_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind&>()))
    PP_SEQ_APPLY(HLIR_EXPRS, HLIR_EXPR_INVOKE_NOEXCEPT, &&)
    #undef HLIR_EXPR_INVOKE_NOEXCEPT
  };

public:
  template<expr_visitor Visitor>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(expr::visit_noexcept<Visitor>);

  template<expr_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor)
      noexcept(expr::visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<expr_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_EXPRS) const&>;

private:
  template<expr_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define HLIR_EXPR_CONST_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), \
      std::declval<kind const&>()))
    PP_SEQ_APPLY(HLIR_EXPRS, HLIR_EXPR_CONST_INVOKE_NOEXCEPT, &&)
    #undef HLIR_EXPR_CONST_INVOKE_NOEXCEPT
  };

public:
  template<expr_const_visitor Visitor>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(expr::const_visit_noexcept<Visitor>);

  template<expr_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(expr::const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief An integer literal.
 *
 * If there is a type suffix, no requirement is imposed that the value must
 * actually fit within the requested type. If it does not, this is treated as
 * a semantic error (similarly to, say, addition of incompatible types). */
struct int_literal final: expr {
  util::bigint value;

  std::optional<int_type_suffix> type_suffix;

  explicit int_literal(
    source_location loc,
    util::bigint value,
    std::optional<int_type_suffix> type_suffix = std::nullopt
  ) noexcept:
    expr{std::move(loc)},
    value{std::move(value)},
    type_suffix{type_suffix}
  {
    assert(!type_suffix || type_suffix->width > 0);
    assert(!type_suffix || type_suffix->width < max_int_width);
  }

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_literal;
  }
};

/** @brief A Boolean literal. */
struct bool_literal final: expr {
  bool value;

  explicit bool_literal(source_location loc, bool value) noexcept:
    expr{std::move(loc)}, value{value} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bool_literal;
  }
};

/** @brief A reference to a variable by name. */
struct named_var_ref final: expr {
  std::u8string name;

  explicit named_var_ref(
    source_location loc,
    std::u8string name
  ) noexcept: expr{std::move(loc)}, name{std::move(name)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::named_var_ref;
  }
};

/** @brief A reference to a global variable. */
struct global_var_ref final: expr {
  util::non_null_ptr<global_var> target;

  explicit global_var_ref(
    source_location loc,
    global_var& target
  ) noexcept:
    expr{std::move(loc)},
    target{&target} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::global_var_ref;
  }
};

/** @brief A reference to a local variable.
 *
 * If the variable does not belong to the same function as the one containing
 * this node, the HLIR is ill-formed. */
struct local_var_ref final: expr {
  util::non_null_ptr<local_var> target;

  explicit local_var_ref(
    source_location loc,
    local_var& target
  ) noexcept:
    expr{std::move(loc)},
    target{&target} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::local_var_ref;
  }
};

/** @brief A reference to a function. */
struct function_ref final: expr {
  util::non_null_ptr<function> target;

  explicit function_ref(
    source_location loc,
    function& target
  ) noexcept:
    expr{std::move(loc)},
    target{&target} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::function_ref;
  }
};

/** @brief A chain of comparison operators.
 *
 * This is represented as the first operand, followed by a list of `rel`
 * objects, each of which corresponds to an operator and contains the operator
 * kind, the operator's source location, and the operator's right operand (which
 * also serves as the next operator's left operand).
 *
 * Requirement: There must be at least one operator. */
struct cmp_expr final: expr {
  enum class rel_kind { eq, lt, le, gt, ge };

  struct rel {
    rel_kind kind;
    hlir_ptr<expr> op;
    source_location loc;

    conversion_t left_conversion;
    conversion_t right_conversion;

    explicit rel(
      rel_kind kind,
      hlir_ptr<expr> op,
      source_location loc
    ) noexcept:
      kind{kind},
      op{std::move(op)},
      loc{std::move(loc)} {}
  };

  hlir_ptr<expr> first_op;
  util::small_vector<rel, 1> rels;

  explicit cmp_expr(
    source_location loc,
    hlir_ptr<expr> first_op,
    util::small_vector<rel, 1> rels
  ) noexcept:
    expr{std::move(loc)},
    first_op{std::move(first_op)},
    rels{std::move(rels)}
  {
    assert(!this->rels.empty());
  }

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::cmp_expr;
  }
};

/** @brief The base class for binary operators.
 *
 * If either of the operand pointers are null, this HLIR is ill-formed. */
struct binary_op_expr: expr {
  hlir_ptr<expr> left;
  hlir_ptr<expr> right;

  conversion_t left_conversion;
  conversion_t right_conversion;

  explicit binary_op_expr(
    source_location loc,
    hlir_ptr<expr> left,
    hlir_ptr<expr> right
  ) noexcept:
    expr{std::move(loc)},
    left{std::move(left)},
    right{std::move(right)} {}
};

#define HLIR_EXPR_BINARY(name) struct name final: binary_op_expr { \
  using binary_op_expr::binary_op_expr; \
  \
  [[nodiscard]] virtual kind_t get_kind() const noexcept override { \
    return kind_t::name; \
  } \
}

// Specific binary operators:
HLIR_EXPR_BINARY(or_expr);   // ||
HLIR_EXPR_BINARY(and_expr);  // &&
HLIR_EXPR_BINARY(xor_expr);  // ^^
HLIR_EXPR_BINARY(neq_expr);  // !=
HLIR_EXPR_BINARY(add_expr);  // +
HLIR_EXPR_BINARY(sub_expr);  // -
HLIR_EXPR_BINARY(mul_expr);  // *
HLIR_EXPR_BINARY(div_expr);  // /
HLIR_EXPR_BINARY(mod_expr);  // %
HLIR_EXPR_BINARY(bor_expr);  // |
HLIR_EXPR_BINARY(band_expr); // &
HLIR_EXPR_BINARY(bxor_expr); // ^
HLIR_EXPR_BINARY(lsh_expr);  // <<
HLIR_EXPR_BINARY(rsh_expr);  // >>
HLIR_EXPR_BINARY(lrot_expr); // </
HLIR_EXPR_BINARY(rrot_expr); // >/

#undef HLIR_EXPR_BINARY

/** @brief The base class for unary operators.
 *
 * If the operand pointer is null, this HLIR is ill-formed. */
struct unary_op_expr: expr {
  hlir_ptr<expr> op;

  explicit unary_op_expr(
    source_location loc,
    hlir_ptr<expr> op
  ) noexcept:
    expr{std::move(loc)},
    op{std::move(op)} {}
};

#define HLIR_EXPR_UNARY(name) struct name final: unary_op_expr { \
  using unary_op_expr::unary_op_expr; \
  \
  [[nodiscard]] virtual kind_t get_kind() const noexcept override { \
    return kind_t::name; \
  } \
}

// Specific unary operators:
HLIR_EXPR_UNARY(neg_expr);   // -
HLIR_EXPR_UNARY(bnot_expr);  // ~
HLIR_EXPR_UNARY(not_expr);   // !
HLIR_EXPR_UNARY(deref_expr); // *

#undef HLIR_EXPR_UNARY

/** @brief An address operation. */
struct addr_expr final: unary_op_expr {
  bool is_mutable;

  explicit addr_expr(
    source_location loc,
    hlir_ptr<expr> op,
    bool is_mutable
  ) noexcept:
    unary_op_expr{std::move(loc), std::move(op)},
    is_mutable{is_mutable} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::addr_expr;
  }
};

/** @brief A member access operator that specifies the member by name. */
struct named_member_expr final: unary_op_expr {
  std::u8string name;

  explicit named_member_expr(
    source_location loc,
    hlir_ptr<expr> op,
    std::u8string name
  ) noexcept:
    unary_op_expr{std::move(loc), std::move(op)},
    name{std::move(name)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::named_member_expr;
  }
};

/** @brief A member access operator that specifies the member by name. */
struct indexed_member_expr final: unary_op_expr {
  field_index_t index;

  explicit indexed_member_expr(
    source_location loc,
    hlir_ptr<expr> op,
    field_index_t index
  ) noexcept:
    unary_op_expr{std::move(loc), std::move(op)},
    index{index} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::indexed_member_expr;
  }
};

/** @brief A call expression.
 *
 * If the callee pointer or any of the argument pointers are null, this HLIR is
 * ill-formed. */
struct call_expr final: expr {
  struct arg {
    hlir_ptr<expr> value;

    conversion_t conversion;

    explicit arg(hlir_ptr<expr> value) noexcept:
      value{std::move(value)} {}
  };

  hlir_ptr<expr> callee;
  std::vector<arg> args;

  explicit call_expr(
    source_location loc,
    hlir_ptr<expr> callee,
    std::vector<arg> args
  ) noexcept:
    expr{std::move(loc)},
    callee{std::move(callee)},
    args{std::move(args)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::call_expr;
  }
};

// block will be defined after stmt

// Statements //////////////////////////////////////////////////////////////////

#define HLIR_STMTS(x, y, z) \
  z(expr_stmt) y \
  x(assign_stmt) y \
  x(def_stmt) y \
  x(local_var_init_stmt)

#define HLIR_STMT_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(HLIR_STMTS, HLIR_STMT_FWD_DEF)
#undef HLIR_STMT_FWD_DEF

template<typename Visitor>
concept stmt_visitor =
  #define HLIR_STMT_IS_INVOCABLE(kind) std::invocable<Visitor, kind&>
  PP_SEQ_APPLY(HLIR_STMTS, HLIR_STMT_IS_INVOCABLE, &&)
  #undef HLIR_STMT_IS_INVOCABLE
  && util::all_same_v<
    #define HLIR_STMT_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind&>
    PP_SEQ_LIST(HLIR_STMTS, HLIR_STMT_INVOKE_RESULT)
    #undef HLIR_STMT_INVOKE_RESULT
  >;

template<typename Visitor>
concept stmt_const_visitor =
  #define HLIR_STMT_IS_CONST_INVOCABLE(kind) \
    std::invocable<Visitor, kind const&>
  PP_SEQ_APPLY(HLIR_STMTS, HLIR_STMT_IS_CONST_INVOCABLE, &&)
  #undef HLIR_STMT_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define HLIR_STMT_CONST_INVOKE_RESULT(kind) \
      std::invoke_result_t<Visitor&&, kind const&>
    PP_SEQ_LIST(HLIR_STMTS, HLIR_STMT_CONST_INVOKE_RESULT)
    #undef HLIR_STMT_CONST_INVOKE_RESULT
  >;

/** @brief The base class for HLIR statements. */
struct stmt: hlir_base, util::hier_base {
  virtual ~stmt() noexcept = default;

  enum class kind_t {
    #define HLIR_STMT_ENUM(kind) kind
    PP_SEQ_LIST(HLIR_STMTS, HLIR_STMT_ENUM)
    #undef HLIR_STMT_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<stmt_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_STMTS)&>;

private:
  template<typename Visitor>
  static constexpr bool visit_noexcept{
    #define HLIR_STMT_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind&>()))
    PP_SEQ_APPLY(HLIR_STMTS, HLIR_STMT_INVOKE_NOEXCEPT, &&)
    #undef HLIR_STMT_INVOKE_NOEXCEPT
  };

public:
  template<stmt_visitor Visitor>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(stmt::visit_noexcept<Visitor>);

  template<stmt_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor)
      noexcept(stmt::visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<stmt_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_STMTS) const&>;

private:
  template<stmt_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define HLIR_STMT_CONST_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), \
      std::declval<kind const&>()))
    PP_SEQ_APPLY(HLIR_STMTS, HLIR_STMT_CONST_INVOKE_NOEXCEPT, &&)
    #undef HLIR_STMT_CONST_INVOKE_NOEXCEPT
  };

public:
  template<stmt_const_visitor Visitor>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(stmt::const_visit_noexcept<Visitor>);

  template<stmt_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(stmt::const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief A block of statements.
 *
 * If any of the member statement pointers are null, this HLIR is ill-formed.
 *
 * The trailing expression pointer may be null to indicate the absence of the
 * trailing expression. */
struct block final: expr {
  std::vector<hlir_ptr<stmt>> stmts;
  hlir_ptr<expr> trail;

  explicit block(source_location loc) noexcept: expr{std::move(loc)}, stmts{},
    trail{nullptr} {}

  explicit block(
    source_location loc,
    std::vector<hlir_ptr<stmt>> stmts,
    hlir_ptr<expr> trail
  ) noexcept:
    expr{std::move(loc)},
    stmts{std::move(stmts)},
    trail{std::move(trail)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::block;
  }
};

template<expr_visitor Visitor>
expr::visit_result_t<Visitor> expr::mutable_visit(Visitor&& visitor)
    noexcept(expr::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_EXPR_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(HLIR_EXPRS, HLIR_EXPR_VISIT)
    #undef HLIR_EXPR_VISIT
  }
}

template<expr_const_visitor Visitor>
expr::const_visit_result_t<Visitor> expr::const_visit(Visitor&& visitor) const
    noexcept(expr::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_EXPR_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(HLIR_EXPRS, HLIR_EXPR_CONST_VISIT)
    #undef HLIR_EXPR_CONST_VISIT
  }
}

#undef HLIR_EXPRS

/** @brief An expression evaluation statement.
 *
 * If the expression pointer is null, this HLIR is ill-formed. */
struct expr_stmt final: stmt {
  hlir_ptr<expr> content;

  explicit expr_stmt(hlir_ptr<expr> content) noexcept:
    content{std::move(content)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::expr_stmt;
  }
};

/** @brief An assignment to a variable.
 *
 * If either the target or the value pointer is null, this HLIR is ill-formed.
 */
struct assign_stmt final: stmt {
  hlir_ptr<expr> target;
  hlir_ptr<expr> value;

  conversion_t conversion;

  explicit assign_stmt(
    hlir_ptr<expr> target,
    hlir_ptr<expr> value
  ) noexcept:
    target{std::move(target)},
    value{std::move(value)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_stmt;
  }
};

// def_stmt will be defined after def

/** @brief The initialization of a local variable.
 *
 * If the value pointer is null or the target variable does not belong to the
 * function containing this node, this HLIR is ill-formed. */
struct local_var_init_stmt final: stmt {
  util::non_null_ptr<local_var> target;
  hlir_ptr<expr> value;

  conversion_t conversion;

  explicit local_var_init_stmt(
    local_var& target,
    hlir_ptr<expr> value
  ) noexcept:
    target{&target},
    value{std::move(value)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::local_var_init_stmt;
  }
};

// Definitions /////////////////////////////////////////////////////////////////

#define HLIR_DEFS(x, y, z) \
  z(fn_def) y \
  x(var_def) y \
  x(class_def)

#define HLIR_DEF_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(HLIR_DEFS, HLIR_DEF_FWD_DEF)
#undef HLIR_DEF_FWD_DEF

template<typename Visitor>
concept def_visitor =
  #define HLIR_DEF_IS_INVOCABLE(kind) std::invocable<Visitor, kind&>
  PP_SEQ_APPLY(HLIR_DEFS, HLIR_DEF_IS_INVOCABLE, &&)
  #undef HLIR_DEF_IS_INVOCABLE
  && util::all_same_v<
    #define HLIR_DEF_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind&>
    PP_SEQ_LIST(HLIR_DEFS, HLIR_DEF_INVOKE_RESULT)
    #undef HLIR_DEF_INVOKE_RESULT
  >;

template<typename Visitor>
concept def_const_visitor =
  #define HLIR_DEF_IS_CONST_INVOCABLE(kind) \
    std::invocable<Visitor, kind const&>
  PP_SEQ_APPLY(HLIR_DEFS, HLIR_DEF_IS_CONST_INVOCABLE, &&)
  #undef HLIR_DEF_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define HLIR_DEF_CONST_INVOKE_RESULT(kind) \
      std::invoke_result_t<Visitor&&, kind const&>
    PP_SEQ_LIST(HLIR_DEFS, HLIR_DEF_CONST_INVOKE_RESULT)
    #undef HLIR_DEF_CONST_INVOKE_RESULT
  >;

/** @brief A definition.
 *
 * Definitions do not remember their names; those are provided externally. */
struct def: hlir_base, util::hier_base {
  /** @brief The source location to be used to refer to the definition in
   * diagnostics.
   *
   * This is expected to be the place where the definition's name is introduced.
   */
  source_location core_loc;

  explicit def(source_location core_loc) noexcept:
    core_loc{std::move(core_loc)} {}
  virtual ~def() noexcept = default;

  enum class kind_t {
    #define HLIR_DEF_ENUM(kind) kind
    PP_SEQ_LIST(HLIR_DEFS, HLIR_DEF_ENUM)
    #undef HLIR_DEF_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<def_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_DEFS)&>;

private:
  template<def_visitor Visitor>
  static constexpr bool visit_noexcept{
    #define HLIR_DEF_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind&>()))
    PP_SEQ_APPLY(HLIR_DEFS, HLIR_DEF_INVOKE_NOEXCEPT, &&)
    #undef HLIR_DEF_INVOKE_NOEXCEPT
  };

public:
  template<def_visitor Visitor>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(def::visit_noexcept<Visitor>);

  template<def_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor)
      noexcept(def::visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<def_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_DEFS) const&>;

private:
  template<def_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define HLIR_DEF_CONST_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), \
      std::declval<kind const&>()))
    PP_SEQ_APPLY(HLIR_DEFS, HLIR_DEF_CONST_INVOKE_NOEXCEPT, &&)
    #undef HLIR_DEF_CONST_INVOKE_NOEXCEPT
  };

public:
  template<def_const_visitor Visitor>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(def::const_visit_noexcept<Visitor>);

  template<def_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(def::const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief A statement containing a definition.
 *
 * If the definition pointer is null, this HLIR is ill-formed. */
struct def_stmt final: stmt {
  std::u8string name;
  hlir_ptr<def> value;

  explicit def_stmt(
    std::u8string name,
    hlir_ptr<def> value
  ) noexcept:
    name{std::move(name)},
    value{std::move(value)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::def_stmt;
  }
};

template<stmt_visitor Visitor>
stmt::visit_result_t<Visitor> stmt::mutable_visit(Visitor&& visitor)
    noexcept(stmt::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_STMT_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(HLIR_STMTS, HLIR_STMT_VISIT)
    #undef HLIR_STMT_VISIT
  }
}

template<stmt_const_visitor Visitor>
stmt::const_visit_result_t<Visitor> stmt::const_visit(Visitor&& visitor) const
    noexcept(stmt::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_STMT_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(HLIR_STMTS, HLIR_STMT_CONST_VISIT)
    #undef HLIR_STMT_CONST_VISIT
  }
}

#undef HLIR_STMTS

/** @brief A function definition.
 *
 * If the pointer to the function's body is null or if any of the pointers to
 * the function's parameter type names are null, this HLIR is ill-formed.
 *
 * The return type pointer may be null to indicate the absence of a return type.
 */
struct fn_def final: def {
  struct param {
    std::optional<std::u8string> name;
    hlir_ptr<hlir::type> type;

    /** @brief The source location to be used to refer to the parameter in
     * diagnostics.
     *
     * This is expected to be the place where the parameter's name is
     * introduced. */
    source_location core_loc;

    explicit param(
      std::optional<std::u8string> name,
      hlir_ptr<hlir::type> type,
      source_location core_loc
    ) noexcept:
      name{std::move(name)},
      type{std::move(type)},
      core_loc{std::move(core_loc)} {}
  };

  std::vector<param> params;
  hlir_ptr<type> return_type;
  hlir_ptr<block> body;

  explicit fn_def(
    source_location core_loc,
    std::vector<param> params,
    hlir_ptr<block> body
  ) noexcept:
    def{std::move(core_loc)},
    params{std::move(params)},
    return_type{nullptr},
    body{std::move(body)} {}

  explicit fn_def(
    source_location core_loc,
    std::vector<param> params,
    hlir_ptr<type> return_type,
    hlir_ptr<block> body
  ) noexcept:
    def{std::move(core_loc)},
    params{std::move(params)},
    return_type{std::move(return_type)},
    body{std::move(body)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def;
  }
};

/** @brief A variable definition.
 *
 * If the pointer to the type name is null, this HLIR is ill-formed.
 *
 * The pointer to the initializer expression may be null to indicate the absence
 * of an initializer. */
struct var_def final: def {
  hlir_ptr<hlir::type> type;
  hlir_ptr<expr> init;

  enum class mutable_spec_t { none, mut, constant };

  mutable_spec_t mutable_spec;

  explicit var_def(
    source_location core_loc,
    hlir_ptr<hlir::type> type,
    hlir_ptr<expr> init,
    mutable_spec_t mutable_spec
  ) noexcept:
    def{std::move(core_loc)},
    type{std::move(type)},
    init{std::move(init)},
    mutable_spec{mutable_spec} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def;
  }
};

/** @brief A class definition.
 *
 * If any member pointers are null, this HLIR is ill-formed. */
struct class_def final: def {
  std::vector<std::pair<std::u8string, hlir_ptr<def>>> members;

  explicit class_def(
    source_location core_loc,
    std::vector<std::pair<std::u8string, hlir_ptr<def>>> members
  ) noexcept: def{std::move(core_loc)}, members{std::move(members)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_def;
  }
};

template<def_visitor Visitor>
def::visit_result_t<Visitor> def::mutable_visit(Visitor&& visitor)
    noexcept(def::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_DEF_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(HLIR_DEFS, HLIR_DEF_VISIT)
    #undef HLIR_DEF_VISIT
  }
}

template<def_const_visitor Visitor>
def::const_visit_result_t<Visitor> def::const_visit(Visitor&& visitor) const
    noexcept(def::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_DEF_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), \
          static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(HLIR_DEFS, HLIR_DEF_CONST_VISIT)
    #undef HLIR_DEF_CONST_VISIT
  }
}

#undef HLIR_DEF_kinds

} // hlir

} // nycleus

#endif
