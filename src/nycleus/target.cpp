#include "nycleus/target.hpp"
#include <gsl/gsl>

gsl::czstring nycleus::target_error::what() const noexcept {
  return "nycleus::target_error";
}
