#include "nycleus/resolve_types.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_tracker.hpp"
#include "nycleus/entity_bag.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/type_utils.hpp"
#include "util/downcast.hpp"
#include "util/enumerate_view.hpp"
#include "util/non_null_ptr.hpp"
#include "util/stackful_coro.hpp"
#include <atomic>
#include <cassert>
#include <limits>
#include <memory>
#include <new>
#include <utility>
#include <vector>

using namespace nycleus;
namespace coro = util::stackful_coro;

namespace {

/** @throws std::bad_alloc
 * @throws std::bad_exception */
coro::result<type*> resolve_type_impl(
  coro::context,
  hlir::int_type const& name,
  diagnostic_tracker&,
  entity_bag const&,
  type_registry& tr
) {
  assert(name.width > 0);
  assert(name.width <= max_int_width);
  co_return &tr.get_int_type(name.width, name.is_signed);
}

/** @throws std::bad_alloc
 * @throws std::bad_exception */
coro::result<type*> resolve_type_impl(
  coro::context,
  hlir::bool_type const&,
  diagnostic_tracker&,
  entity_bag const&,
  type_registry& tr
) {
  co_return &tr.get_bool_type();
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::bad_exception
 * @throws diagnostic_tracker_error */
coro::result<type*> resolve_type_impl(
  coro::context ctx,
  hlir::fn_type const& name,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  type_registry& tr
) {
  std::vector<util::non_null_ptr<type>> param_types;
  if(std::numeric_limits<std::vector<util::non_null_ptr<type>>::size_type>::max()
      < name.param_types.size()) {
    throw std::bad_alloc{};
  }
  param_types.reserve(name.param_types.size());
  for(auto const& param_name: name.param_types) {
    if(!param_name) {
      co_return nullptr;
    }
    type* param{co_await detail_::resolve_type(ctx, *param_name, dt, bag, tr)};
    if(!param) {
      co_return nullptr;
    }
    param_types.emplace_back(param);
  }

  type* return_type{nullptr};
  if(name.return_type) {
    return_type
      = co_await detail_::resolve_type(ctx, *name.return_type, dt, bag, tr);
    if(!return_type) {
      co_return nullptr;
    }
  }

  co_return &tr.get_fn_type(std::move(param_types), return_type);
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::bad_exception
 * @throws diagnostic_tracker_error */
coro::result<type*> resolve_type_impl(
  coro::context ctx,
  hlir::ptr_type const& name,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  type_registry& tr
) {
  if(!name.pointee_type) {
    co_return nullptr;
  }
  type* pointee{
    co_await detail_::resolve_type(ctx, *name.pointee_type, dt, bag, tr)};
  if(!pointee) {
    co_return nullptr;
  }
  co_return &tr.get_ptr_type({pointee, name.is_mutable});
}

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::bad_exception
 * @throws diagnostic_tracker_error */
coro::result<type*> resolve_type_impl(
  coro::context,
  hlir::named_type const& name,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  type_registry&
) {
  auto* const entity{bag.get({name.name})};
  if(!entity) {
    dt.track(std::make_unique<diag::name_is_not_type>(diag_ptr{name}));
    co_return nullptr;
  }

  switch(entity->get_entity_kind()) {
    case named_entity::entity_kind_t::class_type:
      co_return util::downcast<class_type*>(entity);

    default:
      dt.track(std::make_unique<diag::name_is_not_type>(diag_ptr{name}));
      co_return nullptr;
  }
}

coro::result<type*> resolve_type_impl(
  coro::context,
  hlir::class_ref_type const& name,
  diagnostic_tracker&,
  entity_bag const&,
  type_registry&
) {
  co_return name.cl;
}

} // (anonymous)

coro::result<type*> detail_::resolve_type(
  coro::context ctx,
  hlir::type const& name,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  type_registry& tr
) {
  co_return co_await ctx.tail_call(
      [&name, &dt, &bag, &tr](coro::context ctx) {
      return name.visit([&](auto const& name) {
        return resolve_type_impl(ctx, name, dt, bag, tr);
      });
    });
}

////////////////////////////////////////////////////////////////////////////////

coro::result<> nycleus::resolve_fn(
  coro::context ctx,
  function& fn,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  type_registry& tr
) {
  assert(!fn.initializee);
  assert(!fn.name.is_initializer);

  assert(fn.processing_state.load(std::memory_order::relaxed)
    == function::processing_state_t::initial);

  for(auto&& [index, param]: fn.params | util::enumerate) {
    assert(param.var);
    auto const* const name{param.var->type.as_hlir()};
    if(!name) {
      continue;
    }

    type* const resolved_type{
      co_await detail_::resolve_type(ctx, *name, dt, bag, tr)};
    param.var->type = resolved_type;

    if(param.var->name_is_generated && resolved_type) {
      param.var->diag_name = util::format<u8"(parameter {} of type {})">(
        index + 1, *resolved_type);
    }
  }

  if(auto const* const name{fn.return_type.as_hlir()}) {
    fn.return_type = co_await detail_::resolve_type(ctx, *name, dt, bag, tr);
  }

  for(auto& local: fn.local_vars) {
    assert(local);
    auto const* const name{local->type.as_hlir()};
    if(!name) {
      continue;
    }

    local->type = co_await detail_::resolve_type(ctx, *name, dt, bag, tr);
  }

  fn.processing_state.store(function::processing_state_t::resolved,
    std::memory_order::release);
}

coro::result<> nycleus::resolve_global_var(
  coro::context ctx,
  global_var& var,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  type_registry& tr
) {
  assert(var.processing_state.load(std::memory_order::relaxed)
    == global_var::processing_state_t::initial);

  if(auto const* const name{var.type.as_hlir()}) {
    auto* const resolved_type{
      co_await detail_::resolve_type(ctx, *name, dt, bag, tr)};
    var.type = resolved_type;
    if(var.initializer) {
      assert(var.initializer->initializee == &var);
      var.initializer->return_type = resolved_type;
    }
  }

  var.processing_state.store(global_var::processing_state_t::resolved,
    std::memory_order::release);
  if(var.initializer) {
    var.initializer->processing_state.store(
      function::processing_state_t::resolved, std::memory_order::release);
  }
}

coro::result<> nycleus::resolve_class(
  coro::context ctx,
  class_type& cl,
  diagnostic_tracker& dt,
  entity_bag const& bag,
  type_registry& tr
) {
  assert(cl.processing_state.load(std::memory_order::relaxed)
    == class_type::processing_state_t::initial);

  for(auto& field: cl.fields) {
    if(auto const* const name{field.type.as_hlir()}) {
      auto* const resolved_type{
        co_await detail_::resolve_type(ctx, *name, dt, bag, tr)};
      field.type = resolved_type;
    }
  }

  cl.processing_state.store(class_type::processing_state_t::resolved,
    std::memory_order::release);
}

