#ifndef NYCLEUS_TYPE_UTILS_HPP_INCLUDED_O7LIVS1KLP13BBXSO95K0FR0S
#define NYCLEUS_TYPE_UTILS_HPP_INCLUDED_O7LIVS1KLP13BBXSO95K0FR0S

#include "nycleus/entity.hpp"
#include "nycleus/utils.hpp"
#include "util/non_null_ptr.hpp"
#include "util/ptr_with_flags.hpp"
#include "util/ranges.hpp"
#include "util/shared_guarded_var.hpp"
#include <boost/container_hash/hash.hpp>
#include <algorithm>
#include <cassert>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <memory>
#include <ranges>
#include <unordered_set>
#include <utility>
#include <vector>

namespace nycleus {

// Type registry ///////////////////////////////////////////////////////////////

/** @brief A container for various data types to ensure their deduplication.
 *
 * This can be safely accessed concurrently. */
class type_registry {
private:
  struct int_types_hash {
    [[nodiscard]] std::size_t operator()(int_width_t key) const {
      return std::hash<int_width_t>{}(key);
    }

    [[nodiscard]] std::size_t operator()(std::unique_ptr<int_type> const& key)
        const {
      assert(key);
      return std::hash<int_width_t>{}(key->width);
    }

    using is_transparent = void;
  };

  struct int_types_equal {
    [[nodiscard]] bool operator()(
      int_width_t a,
      std::unique_ptr<int_type> const& b
    ) const {
      assert(b);
      return a == b->width;
    }

    [[nodiscard]] bool operator()(
      std::unique_ptr<int_type> const& a,
      int_width_t b
    ) const {
      assert(a);
      return this->operator()(b, a);
    }

    [[nodiscard]] bool operator()(
      std::unique_ptr<int_type> const& a,
      std::unique_ptr<int_type> const& b
    ) const {
      assert(a);
      assert(b);
      return a->width == b->width;
    }

    using is_transparent = void;
  };

  util::shared_guarded_var<std::unordered_set<
    std::unique_ptr<int_type>,
    int_types_hash,
    int_types_equal
  >> singed_int_types;

  util::shared_guarded_var<std::unordered_set<
    std::unique_ptr<int_type>,
    int_types_hash,
    int_types_equal
  >> unsinged_int_types;

public:
  /** @brief Returns the integer type with the given width and signedness.
   * @throws std::bad_alloc */
  [[nodiscard]] int_type& get_int_type(int_width_t width, bool is_signed);

private:
  bool_type bool_type_;

public:
  /** @brief Returns the Boolean type. */
  [[nodiscard]] bool_type& get_bool_type() noexcept {
    return bool_type_;
  }

private:
  struct fn_types_hash {
    template<std::ranges::forward_range Range>
    requires std::same_as<std::ranges::range_value_t<Range>, util::non_null_ptr<type>>
    [[nodiscard]] std::size_t operator()(
      std::pair<Range, type*> const& key
    ) const {
      std::size_t result{0};
      for(util::non_null_ptr<type> const param_type: key.first) {
        boost::hash_combine(result, std::hash<type*>{}(param_type.get()));
      }
      boost::hash_combine(result, std::hash<type*>{}(key.second));
      return result;
    }

    [[nodiscard]] std::size_t operator()(
      std::unique_ptr<fn_type> const& key
    ) const noexcept;

    using is_transparent = void;
  };

  struct fn_types_equal {
    template<std::ranges::forward_range Range>
    requires std::same_as<std::ranges::range_value_t<Range>, util::non_null_ptr<type>>
    [[nodiscard]] bool operator()(
      std::pair<Range, type*> const& a,
      std::unique_ptr<fn_type> const& b
    ) const {
      assert(b);
      return std::ranges::equal(a.first, b->param_types)
        && a.second == b->return_type;
    }

    template<std::ranges::forward_range Range>
    requires std::same_as<std::ranges::range_value_t<Range>, util::non_null_ptr<type>>
    [[nodiscard]] bool operator()(
      std::unique_ptr<fn_type> const& a,
      std::pair<Range, type*> const& b
    ) const {
      assert(a);
      return this->operator()(b, a);
    }

    [[nodiscard]] bool operator()(
      std::unique_ptr<fn_type> const&,
      std::unique_ptr<fn_type> const&
    ) const noexcept;

    using is_transparent = void;
  };

  util::shared_guarded_var<std::unordered_set<
    std::unique_ptr<fn_type>,
    fn_types_hash,
    fn_types_equal
  >> fn_types;

public:
  /** @brief Returns a function reference type with the given parameter types
   * and the given return type.
   *
   * A function reference type without a return type can be obtained by passing
   * a null pointer as a return type.
   *
   * If the first parameter type is std::vector<util::non_null_ptr<type>>&&
   * and no such function reference type has been created yet, the vector will
   * be moved into the newly created type.
   *
   * @throws std::bad_alloc */
  template<typename Range>
  requires std::ranges::forward_range<Range&&>
    && std::same_as<std::ranges::range_value_t<Range&&>, util::non_null_ptr<type>>
  [[nodiscard]] fn_type& get_fn_type(Range&& param_types, type* return_type) {
    {
      auto locked{fn_types.lock_shared()};
      auto const it{locked->find(std::pair<Range&&, type*>{
        std::forward<Range>(param_types), return_type})};
      if(it != locked->cend()) {
        assert(*it);
        return **it;
      }
    }

    {
      auto locked{fn_types.lock_unique()};
      if constexpr(std::same_as<Range&&, std::vector<util::non_null_ptr<type>>&&>) {
        auto [it, is_new] = locked->insert(std::make_unique<fn_type>(
          std::forward<Range>(param_types), return_type));
        return **it;
      } else {
        auto const [it, is_new] = locked->insert(std::make_unique<fn_type>(
          param_types | util::range_to<std::vector>(), return_type));
        return **it;
      }
    }
  }

private:
  struct ptr_types_hash {
    [[nodiscard]] std::size_t operator()(
      std::unique_ptr<ptr_type> const& t
    ) const noexcept {
      return this->operator()(t->pointee_type);
    }

    [[nodiscard]] std::size_t operator()(util::ptr_with_flags<type, 1>)
      const noexcept;

    using is_transparent = void;
  };

  struct ptr_types_equal {
    [[nodiscard]] bool operator()(
      std::unique_ptr<ptr_type> const& a,
      std::unique_ptr<ptr_type> const& b
    ) const noexcept {
      assert(a);
      assert(b);
      return a->pointee_type == b->pointee_type;
    }

    [[nodiscard]] bool operator()(
      util::ptr_with_flags<type, 1> a,
      std::unique_ptr<ptr_type> const& b
    ) const noexcept {
      assert(a.get_ptr());
      assert(b);
      return a == b->pointee_type;
    }

    [[nodiscard]] bool operator()(
      std::unique_ptr<ptr_type> const& a,
      util::ptr_with_flags<type, 1> b
    ) const noexcept {
      assert(a);
      assert(b.get_ptr());
      return a->pointee_type == b;
    }

    using is_transparent = void;
  };

  util::shared_guarded_var<std::unordered_set<
    std::unique_ptr<ptr_type>,
    ptr_types_hash,
    ptr_types_equal
  >> ptr_types_;

public:
  /** @brief Returns a pointer type with the given pointee type and mutability
   * flag.
   * @throws std::bad_alloc */
  [[nodiscard]] ptr_type& get_ptr_type(
    util::ptr_with_flags<type, 1> pointee_type
  );
};

} // nycleus

#endif
