#ifndef NYCLEUS_ENTITY_HPP_INCLUDED_1XZ7WVR38PK1FDKQBEX3Q2DWQ
#define NYCLEUS_ENTITY_HPP_INCLUDED_1XZ7WVR38PK1FDKQBEX3Q2DWQ

#include "nycleus/conversion.hpp"
#include "nycleus/global_name.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/scope.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/utils.hpp"
#include "util/format.hpp"
#include "util/intrusive_list.hpp"
#include "util/non_null_ptr.hpp"
#include "util/nums.hpp"
#include "util/pp_seq.hpp"
#include "util/ptr_with_flags.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <atomic>
#include <cassert>
#include <concepts>
#include <functional>
#include <iterator>
#include <memory>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

namespace nycleus {

struct function;
struct global_var;

/** @brief The base class for entities that have global names.
 *
 * These are the entities that can be stored in an entity_bag. */
struct named_entity: util::hier_base {
  enum class entity_kind_t { function, global_var, class_type };
  [[nodiscard]] virtual entity_kind_t get_entity_kind() const noexcept = 0;

  virtual ~named_entity() noexcept = default;

  global_name name;

  /** @brief The source location to be used to refer to the entity in
   * diagnostics.
   *
   * This is expected to be the place where the entity's name is introduced. */
  source_location core_loc;
};

// Types: general //////////////////////////////////////////////////////////////

#define NYCLEUS_TYPES(x, y, z) \
  z(int_type) y \
  x(bool_type) y \
  x(fn_type) y \
  x(ptr_type) y \
  x(class_type)

#define NYCLEUS_TYPE_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(NYCLEUS_TYPES, NYCLEUS_TYPE_FWD_DEF)
#undef NYCLEUS_TYPE_FWD_DEF

struct type: util::hier_base {
  virtual ~type() noexcept = default;

  enum class kind_t {
    #define NYCLEUS_TYPE_ENUM(kind) kind
    PP_SEQ_LIST(NYCLEUS_TYPES, NYCLEUS_TYPE_ENUM)
    #undef NYCLEUS_TYPE_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<typename Visitor, typename = void>
  struct is_visitor: std::false_type {};
  template<typename Visitor>
  requires
    #define NYCLEUS_TYPE_IS_INVOCABLE(kind) std::invocable<Visitor&, kind&>
    PP_SEQ_APPLY(NYCLEUS_TYPES, NYCLEUS_TYPE_IS_INVOCABLE, &&)
    #undef NYCLEUS_TYPE_IS_INVOCABLE
  struct is_visitor<Visitor>: util::all_same<
    #define NYCLEUS_TYPE_INVOKE_RESULT(kind) \
      std::invoke_result_t<Visitor&, kind&>
    PP_SEQ_LIST(NYCLEUS_TYPES, NYCLEUS_TYPE_INVOKE_RESULT)
    #undef NYCLEUS_TYPE_INVOKE_RESULT
  > {};
  template<typename Visitor>
  static constexpr bool is_visitor_v{is_visitor<Visitor>::value};
  template<typename Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&,
    PP_SEQ_FIRST(NYCLEUS_TYPES)&>;

private:
  template<typename Visitor>
  static constexpr bool visit_noexcept_impl() noexcept {
    if constexpr(is_visitor_v<Visitor>) {
      #define NYCLEUS_TYPE_INVOKE_NOEXCEPT(kind) \
        noexcept(std::invoke(std::declval<Visitor&>(), std::declval<kind&>()))
      return PP_SEQ_APPLY(NYCLEUS_TYPES, NYCLEUS_TYPE_INVOKE_NOEXCEPT, &&);
      #undef NYCLEUS_TYPE_INVOKE_NOEXCEPT
    } else {
      return false;
    }
  }

public:
  template<typename Visitor>
  requires type::is_visitor_v<Visitor&&>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(visit_noexcept_impl<Visitor&&>());

  template<typename Visitor>
  requires type::is_visitor_v<Visitor&&>
  visit_result_t<Visitor> visit(Visitor&& visitor)
      noexcept(visit_noexcept_impl<Visitor&&>()) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<typename Visitor, typename = void>
  struct is_const_visitor: std::false_type {};
  template<typename Visitor>
  requires
    #define NYCLEUS_TYPE_IS_CONST_INVOCABLE(kind) \
      std::invocable<Visitor&, kind const&>
    PP_SEQ_APPLY(NYCLEUS_TYPES, NYCLEUS_TYPE_IS_CONST_INVOCABLE, &&)
    #undef NYCLEUS_TYPE_IS_CONST_INVOCABLE
  struct is_const_visitor<Visitor>: util::all_same<
    #define NYCLEUS_TYPE_CONST_INVOKE_RESULT(kind) \
      std::invoke_result_t<Visitor&, kind const&>
    PP_SEQ_LIST(NYCLEUS_TYPES, NYCLEUS_TYPE_CONST_INVOKE_RESULT)
    #undef NYCLEUS_TYPE_CONST_INVOKE_RESULT
  > {};
  template<typename Visitor>
  static constexpr bool is_const_visitor_v{is_const_visitor<Visitor>::value};
  template<typename Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&,
    PP_SEQ_FIRST(NYCLEUS_TYPES) const&>;

private:
  template<typename Visitor>
  static constexpr bool const_visit_noexcept_impl() noexcept {
    if constexpr(is_const_visitor_v<Visitor>) {
      #define NYCLEUS_TYPE_CONST_INVOKE_NOEXCEPT(kind) \
        noexcept(std::invoke(std::declval<Visitor&>(), \
        std::declval<kind const&>()))
      return PP_SEQ_APPLY(NYCLEUS_TYPES,
        NYCLEUS_TYPE_CONST_INVOKE_NOEXCEPT, &&);
      #undef NYCLEUS_TYPE_CONST_INVOKE_NOEXCEPT
    } else {
      return false;
    }
  }

public:
  template<typename Visitor>
  requires type::is_const_visitor_v<Visitor&&>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(const_visit_noexcept_impl<Visitor&&>());

  template<typename Visitor>
  requires type::is_const_visitor_v<Visitor&&>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(const_visit_noexcept_impl<Visitor&&>()) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief Either an owning pointer to a HLIR tree representing a type name, or
 * a non-owning pointer to a concrete type. */
using type_ref = maybe_hlir_ptr<hlir::type, type>;

// Types: concrete /////////////////////////////////////////////////////////////

/** @brief An integer type.
 *
 * An instance of this type should only be obtained via
 * type_registry::get_int_type.
 *
 * The width must not exceed max_int_width. */
struct int_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_type;
  }

  int_width_t const width;
  bool const is_signed;

  explicit int_type(int_width_t w, bool s) noexcept: width{w}, is_signed{s} {
    assert(0 < w);
    assert(w <= max_int_width);
  }
};

/** @brief The Boolean type. */
struct bool_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bool_type;
  }
};

/** @brief A function reference type.
 *
 * An instance of this type should only be obtained via
 * type_registry::get_fn_type. */
struct fn_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type;
  }

  std::vector<util::non_null_ptr<type>> const param_types;
  type* const return_type;

  explicit fn_type(std::vector<util::non_null_ptr<type>> p, type* r)
    noexcept: param_types{std::move(p)}, return_type{r} {}
};

/** @brief A pointer type.
 *
 * An instance of this type should only be obtained via
 * type_registry::get_ptr_type. */
struct ptr_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::ptr_type;
  }

  util::ptr_with_flags<type, 1> const pointee_type;

  [[nodiscard]] bool is_mutable() const noexcept {
    return pointee_type.get_flag<0>();
  }

  explicit ptr_type(util::ptr_with_flags<type, 1> pointee_type) noexcept:
      pointee_type{pointee_type} {
    assert(pointee_type.get_ptr());
  }
};

/** @brief A Nycleus user-defined class type.
 *
 * If the class's body contains null pointers to types, the class is invalid.
 * It is the product of an ill-formed input and can only be used for the purpose
 * of discovering more diagnostics. It must never be used as input to code
 * generation. */
struct class_type final: type, named_entity, util::intrusive_list_hook {
  [[nodiscard]] virtual entity_kind_t get_entity_kind() const noexcept
      override {
    return entity_kind_t::class_type;
  }

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_type;
  }

  struct field_t {
    /** @brief The field's type.
     *
     * If this is null, the field's type is invalid and the program is
     * ill-formed. */
    type_ref type;

    bool is_mutable;

    /** @brief The field's human-readable name.
     *
     * This is only used for diagnostics and is not usable for any other
     * purposes. */
    std::u8string diag_name;

    /** @brief The source location to use when referring to the field in
     * diagnostics.
     *
     * This is going to be the location of the field's name. */
    source_location core_loc;

    explicit field_t(
      type_ref t,
      bool is_mut,
      std::u8string dn,
      source_location loc
    ) noexcept:
      type{std::move(t)},
      is_mutable{is_mut},
      diag_name{std::move(dn)},
      core_loc{std::move(loc)} {}
  };

  std::vector<field_t> fields;

  class_scope scope;

  enum class processing_state_t { initial, resolved, cycles };
  static_assert(std::atomic<processing_state_t>::is_always_lock_free);
  std::atomic<processing_state_t> processing_state;
};

template<typename Visitor>
requires type::is_visitor_v<Visitor&&>
type::visit_result_t<Visitor> type::mutable_visit(Visitor&& visitor)
    noexcept(visit_noexcept_impl<Visitor&&>()) {
  switch(get_kind()) {
    #define NYCLEUS_TYPE_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(visitor, static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(NYCLEUS_TYPES, NYCLEUS_TYPE_VISIT)
    #undef NYCLEUS_TYPE_VISIT
  }
}

template<typename Visitor>
requires type::is_const_visitor_v<Visitor&&>
type::const_visit_result_t<Visitor> type::const_visit(Visitor&& visitor) const
    noexcept(const_visit_noexcept_impl<Visitor&&>()) {
  switch(get_kind()) {
    #define NYCLEUS_TYPE_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(visitor, static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(NYCLEUS_TYPES, NYCLEUS_TYPE_CONST_VISIT)
    #undef NYCLEUS_TYPE_CONST_VISIT
  }
}

#undef NYCLEUS_TYPES

// Other entities //////////////////////////////////////////////////////////////

/** @brief Represents a local variable (including a parameter) of a function. */
struct local_var {
  /** @brief A human-readable name of the variable for use in diagnostics.
   *
   * This is intended purely for human consumption. Therefore, there are no
   * formal semantic requirements on this name, such as uniqueness. */
  std::u8string diag_name;

  /** @brief The function that the variable belongs to. */
  util::non_null_ptr<function> fn;

  /** @brief The variable's type or the type's name.
   *
   * If this is null, the type is invalid and the program is ill-formed. */
  type_ref type;

  /** @brief Whether the variable is mutable. */
  bool is_mutable;

  /** @brief Whether the `diag_name` is autogenerated.
   *
   * If false, the `diag_name` is based on the user-supplied name. If true, the
   * `diag_name` is generated entirely by the compiler. */
  bool name_is_generated;

  explicit local_var(
    std::u8string diag_name,
    function& fn,
    type_ref t,
    bool is_mut,
    bool name_is_gen
  ) noexcept:
    diag_name{std::move(diag_name)},
    fn{&fn},
    type{std::move(t)},
    is_mutable{is_mut},
    name_is_generated{name_is_gen} {}

  local_var(local_var const&) = delete;
  local_var& operator=(local_var const&) = delete;
  ~local_var() noexcept = default;
};

/** @brief A Nycleus function. */
struct function final: named_entity, util::intrusive_list_hook {
  [[nodiscard]] virtual entity_kind_t get_entity_kind() const noexcept
      override {
    return entity_kind_t::function;
  }

  struct param {
    /** @brief The source location to be used to refer to the parameter in
     * diagnostics.
     *
     * This is expected to be the place where the parameter's name is
     * introduced. */
    source_location core_loc;

    /** @brief The local variable object representing this parameter. */
    std::unique_ptr<local_var> var;
  };

  /** @brief The function's parameters. */
  std::vector<param> params;

  /** @brief The function's return type, or a null pointer if the function
   * returns nothing. */
  type_ref return_type;

  /** @brief The function's local variables. */
  std::vector<std::unique_ptr<local_var>> local_vars;

  /** @brief The function's body. */
  hlir_ptr<hlir::block> body;

  /** @brief The implicit conversion appplied to the function body's result to
   * obtain the returned value. */
  conversion_t body_conversion;

  /** @brief The global variable initialized by this function, if any. */
  global_var* initializee{nullptr};

  enum class processing_state_t { initial, resolved, analyzed };
  static_assert(std::atomic<processing_state_t>::is_always_lock_free);
  std::atomic<processing_state_t> processing_state;
};

/** @brief A Nycleus global variable. */
struct global_var final: named_entity, util::intrusive_list_hook {
  [[nodiscard]] virtual entity_kind_t get_entity_kind() const noexcept
      override {
    return entity_kind_t::global_var;
  }

  /** @brief The variable's type.
   *
   * If this is null, the variable's type is invalid and the program is
   * ill-formed. */
  type_ref type;

  bool is_mutable;

  /** @brief The function that initializes this variable. */
  function* initializer;

  enum class processing_state_t { initial, resolved };
  static_assert(std::atomic<processing_state_t>::is_always_lock_free);
  std::atomic<processing_state_t> processing_state;
};

// Type formatting /////////////////////////////////////////////////////////////

namespace detail_ {

template<std::output_iterator<char8_t> Out>
Out format_type(Out out, type const& t) noexcept(noexcept(*out++ = char8_t{})) {
  t.visit(util::overloaded{
    [&](int_type const& t) {
      *out++ = t.is_signed ? u8's' : u8'u';
      for(char8_t const ch: util::to_u8string(t.width)) {
        *out++ = ch;
      }
    },

    [&](bool_type const&) {
      out = util::format_to<u8"bool">(std::move(out));
    },

    [&](fn_type const& t) {
      out = util::format_to<u8"fn(">(std::move(out));
      if(t.return_type) {
        *out++ = u8'(';
      }

      bool first{true};
      for(auto const param: t.param_types) {
        if(first) {
          first = false;
        } else {
          out = util::format_to<u8", ">(std::move(out));
        }
        *out++ = u8':';
        out = util::format_to<u8"{}">(std::move(out), *param);
      }

      *out++ = u8')';
      if(t.return_type) {
        out = util::format_to<u8": {}">(std::move(out), *t.return_type);
        *out++ = u8')';
      }
    },

    [&](ptr_type const& t) {
      *out++ = u8'&';
      if(t.is_mutable()) {
        out = util::format_to<u8"mut ">(std::move(out));
      }
      out = util::format_to<u8"{}">(std::move(out), *t.pointee_type);
    },

    [&](class_type const& t) {
      out = util::format_to<u8"{}">(std::move(out), t.name);
    }
  });
  return out;
}

extern template util::output_counting_iterator format_type<
  util::output_counting_iterator
>(util::output_counting_iterator, type const&);
extern template std::back_insert_iterator<std::u8string> format_type<
  std::back_insert_iterator<std::u8string>
>(std::back_insert_iterator<std::u8string>, type const&);

} // detail_

} // nycleus

template<>
struct util::formatter<::nycleus::type, u8""> {
  struct type {
    static auto format(
      ::std::output_iterator<char8_t> auto out,
      ::nycleus::type const& t
    ) noexcept(noexcept(*out++ = char8_t{})) {
      return ::nycleus::detail_::format_type(::std::move(out), t);
    }
  };
};

#endif
