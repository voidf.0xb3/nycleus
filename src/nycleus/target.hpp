#ifndef NYCLEUS_TARGET_HPP_INCLUDED_80D2VLPS0IG5A9VZDKD2NGU4I
#define NYCLEUS_TARGET_HPP_INCLUDED_80D2VLPS0IG5A9VZDKD2NGU4I

#include "nycleus/utils.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <exception>

namespace nycleus {

/** @brief Thrown on target-specific errors. */
class target_error: public std::exception {
public:
  [[nodiscard]] virtual gsl::czstring what() const noexcept override;
};

/** @brief A platform for which compilation is performed. */
class target: public util::hier_base {
public:
  virtual ~target() noexcept = default;

  /** @brief Get the "native" bit width of integer types, that is to say, the
   * integer types big enough to store pointers or object sizes.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws target_error */
  [[nodiscard]] int_width_t get_native_width() const {
    return do_get_native_width();
  }

private:
  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws target_error */
  [[nodiscard]] virtual int_width_t do_get_native_width() const = 0;
};

} // nycleus

#endif
