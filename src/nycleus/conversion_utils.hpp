#ifndef NYCLEUS_CONVERSION_UTILS_HPP_INCLUDED_2JITDOM4F4FFYXL4ILNK9RV3O
#define NYCLEUS_CONVERSION_UTILS_HPP_INCLUDED_2JITDOM4F4FFYXL4ILNK9RV3O

#include "conversion.hpp"
#include <optional>

namespace nycleus {

struct type;

/** @brief Determines the appropriate implicit conversion between the two given
 * types.
 *
 * If no conversion is possible, an empty value is returned. */
[[nodiscard]] std::optional<conversion_t> get_conversion(
  type const& from,
  type const& to
) noexcept;

} // nycleus

#endif
