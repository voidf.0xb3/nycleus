#ifndef NYCLEUS_CONVERSION_HPP_INCLUDED_MMXJVP1I82MGWN1VIXLZOG8OL
#define NYCLEUS_CONVERSION_HPP_INCLUDED_MMXJVP1I82MGWN1VIXLZOG8OL

#include "nycleus/utils.hpp"
#include <variant>

namespace nycleus {

struct type;

/** @brief A conversion between unsigned integer types.
 *
 * The `from` width must be strictly less than the `to` width, and both must not
 * exceed `max_int_width`. */
struct unsigned_int_extend {
  int_width_t to;

  [[nodiscard]] bool operator==(unsigned_int_extend other) const noexcept {
    return to == other.to;
  }
};

/** @brief A conversion between signed integer types.
 *
 * The `from` width must be strictly less than the `to` width, and both must not
 * exceed `max_int_width`. */
struct signed_int_extend {
  int_width_t to;

  [[nodiscard]] bool operator==(signed_int_extend other) const noexcept {
    return to == other.to;
  }
};

/** @brief A conversion from an unsigned integer type to a signed one.
 *
 * The `from` width must be strictly less than the `to` width, and both must not
 * exceed `max_int_width`. */
struct unsigned_to_signed {
  int_width_t to;

  [[nodiscard]] bool operator==(unsigned_to_signed other) const noexcept {
    return to == other.to;
  }
};

/** @brief A conversion dropping pointer mutability qualifiers.
 *
 * Given a pointee type wrapped in one or more possibly mutable pointers, this
 * drops the mutability qualifiers from a certain number of exterior pointer
 * levels. The number stored in this object is one less than the number of
 * qualifiers actually being removed, since we need to remove at least one.
 *
 * Example: converting `&&mut &mut &&mut &u8` to `&&&&&mut &u8` drops two
 * qualifiers, so `extra_levels` will be set to 1. */
struct ptr_mutability_conversion {
  std::uintptr_t extra_levels;

  [[nodiscard]] bool operator==(ptr_mutability_conversion other) const
      noexcept {
    return extra_levels == other.extra_levels;
  }
};

/** @brief An implicit conversion between two types.
 *
 * The std::monostate alternative indicates an identity conversion. That is to
 * say, it indicates that both types are the same, so no conversion is
 * necessary. */
using conversion_t = std::variant<
  std::monostate,
  unsigned_int_extend,
  signed_int_extend,
  unsigned_to_signed,
  ptr_mutability_conversion
>;

} // nycleus

#endif
