#include "opt.hpp"
#include "unicode/windows.hpp"
#include "util/nums.hpp"
#include "util/u8compat.hpp"
#include <llvm/TargetParser/Host.h>
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <limits>
#include <ostream>
#include <stdexcept>
#include <string_view>
#include <thread>

#ifdef _WIN32
  #include "unicode/encoding.hpp"
#endif

using namespace util::u8compat_literals;

namespace {

class pad {
private:
  std::u8string_view str;
  std::size_t width;

public:
  consteval explicit pad(std::u8string_view str, std::size_t width):
      str{str}, width{width} {
    if(str.size() >= width) {
      // This will never actually be thrown, since this is a consteval function.
      // This only serves to trigger a compile-time error.
      throw std::overflow_error{""};
    }
  }

  friend std::ostream& operator<<(std::ostream& os, pad p) {
    os << util::as_char(p.str);
    assert(p.str.size() < p.width);
    std::ranges::fill_n(std::ostream_iterator<char>{os},
      static_cast<std::ptrdiff_t>(p.width - p.str.size()), u8' '_ch);
    return os;
  }
};

} // (anonymous)

/** @throws std::ios_base::failure */
void print_help(std::basic_string_view<unicode::native_char> program_name) {
  unicode::init_unicode_output();

  std::cout <<
    u8"Nycleus compiler\n"
    u8"\n"
    u8"USAGE: "_ch;
  #ifdef _WIN32
    std::ranges::copy(program_name | unicode::utf16_decode()
      | unicode::utf8_encode<char>(),
      std::ostreambuf_iterator<char>{std::cout});
  #else
    std::cout << program_name;
  #endif
  std::cout << u8" <options> <inputs>...\n"
    u8"\n"
    u8"OPTIONS:\n"_ch;

  constexpr std::size_t w{30};

  std::cout << pad{u8"--help, -?", w}
    << u8"Displays this help message.\n"_ch;

  std::cout << pad{u8"--list-targets", w}
    << u8"Lists all available compilation targets.\n"_ch;

  std::cout << pad{u8"--target=<target>", w}
    << u8"The target triple to specify the platform targeted by the compiler. "
    u8"Defaults to the platform the compiler is running on, which is \""_ch
    << llvm::sys::getDefaultTargetTriple()
    << u8"\".\n"_ch;

  std::cout << pad{u8"--jobs=<n>, -j<n>", w}
    << u8"The number of threads to execute simultaneously. Defaults to "_ch
    << util::as_char(util::to_u8string(default_num_jobs())) << u8".\n"_ch;

  {
    constexpr std::size_t vw{9};
    std::cout << pad{u8"--diagnostic-format=<format>", w}
      << u8"The format of outputting diagnostics.\n"_ch
      << pad{u8"", w} << pad{u8"human", vw}
        << u8"The diagnostics are printed in a human-readable format. There "
        u8"is no expectation that the diagnostics will be machine-readable. "
        u8"See also the --color-daignostics option. This is the default.\n"_ch
      << pad{u8"", w} << pad{u8"machine", vw} << u8"Not available.\n"_ch
      << pad{u8"", w} << pad{u8"json", vw}
        << u8"The diagnostics are printed in a machine-readable JSON "
        u8"format.\n"_ch
      << pad{u8"", w} << pad{u8"json-raw", vw} << u8"Not available.\n"_ch;
  }

  {
    constexpr std::size_t vw{5};
    std::cout << pad{u8"--color-diagnostics[=<value>]", w}
      << u8"Whether to print the diagnostic messages in color. Only relevant "
        u8"if the human-readable (default) diagnostic format is selected.\n"_ch
      << pad{u8"", w} << pad{u8"on", vw}
        << u8"Print the diagnostics in color. This is the value used if the "
        u8"option is not given a value explicitly.\n"_ch
      << pad{u8"", w} << pad{u8"off", vw}
        << u8"Do not print the diagnostics in color.\n"_ch
      << pad{u8"", w} << pad{u8"auto", vw}
        << u8"Print the diagnostics in color if the output goes to a terminal, "
        u8"and do not print in color otherwise. This is the default.\n"_ch;
  }

  std::cout << pad{u8"--error-limit=<n>", w}
    << u8"The number of errors that can occur before the compilation is "
    u8"terminated. Defaults to 20. Set to 0 to disable the limit.\n"_ch;

  std::cout << pad{u8"--temp=<path>", w}
    << u8"The location for intermediate result files.\n"_ch;
}

std::uintptr_t default_num_jobs() noexcept {
  unsigned const hw_concurrency{std::thread::hardware_concurrency()};
  if(hw_concurrency == 0) {
    return 1;
  }
  if(hw_concurrency > std::numeric_limits<std::uintptr_t>::max()) {
    return std::numeric_limits<std::uintptr_t>::max();
  }
  return hw_concurrency;
}
