#include "nycleus_llvm/common.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/global_name.hpp"
#include "nycleus_llvm/target.hpp"
#include "util/non_null_ptr.hpp"
#include "util/nums.hpp"
#include "util/overflow.hpp"
#include "util/util.hpp"
#include "util/u8compat.hpp"
#include <atomic>
#include <llvm/IR/Attributes.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalValue.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Value.h>
#include <llvm/Target/TargetMachine.h>
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <exception>
#include <memory>
#include <new>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

using namespace nycleus;

namespace nycleus {
  struct local_var;
}

namespace nycleus_llvm {
  class target;
}

using namespace util::u8compat_literals;

// Utilities ///////////////////////////////////////////////////////////////////

bool nycleus_llvm::is_trivial(type const& t) noexcept {
  return dynamic_cast<class_type const*>(&t) == nullptr;
}

std::string nycleus_llvm::make_llvm_id(global_name const& name,
    std::u8string_view prefix) {
  assert(std::ranges::find(name.name, u8'"') == name.name.cend());

  std::string result;

  try {
    std::size_t result_size{util::throwing_add<std::size_t>(prefix.size(),
      name.name.size(), name.nested_parts.size())};
    for(auto const& part: name.nested_parts) {
      std::visit(util::overloaded{
        [&](global_name::in_function const& p) {
          result_size = util::throwing_add<std::size_t>(result_size,
            util::num_decimal_digits(p.index));
        },

        [&](global_name::in_class const& p) {
          result_size = util::throwing_add<std::size_t>(result_size,
            util::throwing_cast<std::size_t>(p.name.size()));
        }
      }, part);
    }
    result.reserve(result_size);
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }

  result.append(util::as_char(prefix));
  result.append(util::as_char(name.name));
  for(auto const& part: name.nested_parts) {
    result.push_back(u8'.'_ch);
    std::visit(util::overloaded{
      [&](global_name::in_function const& p) {
        result.append(util::as_char(util::to_u8string(p.index)));
      },

      [&](global_name::in_class const& p) {
        result.append(util::as_char(p.name));
      }
    }, part);
  }
  return result;
}

// Context constructor /////////////////////////////////////////////////////////

nycleus_llvm::context::context(
  llvm::LLVMContext& llvm_ctx,
  nycleus_llvm::target& tgt
): mod_{std::make_unique<llvm::Module>("", llvm_ctx)}, builder_{llvm_ctx},
    tgt_{tgt} {
  mod_->setDataLayout(tgt.get_llvm_machine().createDataLayout());
  mod_->setTargetTriple(tgt.get_llvm_machine().getTargetTriple().str());
}

// Locals //////////////////////////////////////////////////////////////////////

void nycleus_llvm::context::add_local(
  local_var const& var,
  llvm::Value& llvm_value
) {
  [[maybe_unused]] auto const [it, added]
    = local_vars_.emplace(&var, &llvm_value);
  assert(added);
}

llvm::Value& nycleus_llvm::context::get_local(local_var const& var)
    const noexcept {
  auto const it{local_vars_.find(util::non_null_ptr{&var})};
  assert(it != local_vars_.cend());
  return *it->second;
}

// Temporaries /////////////////////////////////////////////////////////////////

void nycleus_llvm::context::push_temps() {
  temps_.emplace();
}

llvm::Value& nycleus_llvm::context::add_temp(nycleus::type& t) {
  assert(!temps_.empty());

  llvm::Type& llvm_type{this->get_type(t)};
  llvm::Value& value{*builder_.CreateAlloca(&llvm_type)};

  [[maybe_unused]] auto const [it, added] = temps_.top().emplace(&value, &t);
  assert(added);

  builder_.CreateLifetimeStart(&value, builder_.getInt64(mod_->getDataLayout()
    .getTypeStoreSize(&llvm_type).getFixedValue()));

  return value;
}

void nycleus_llvm::context::drop_temp(llvm::Value& value) {
  assert(!temps_.empty());

  auto const it{temps_.top().find(util::non_null_ptr{&value})};
  if(it == temps_.top().cend()) {
    return;
  }

  builder_.CreateLifetimeEnd(&value, builder_.getInt64(
    mod_->getDataLayout().getTypeStoreSize(&this->get_type(*it->second))
    .getFixedValue()));

  temps_.top().erase(it);
}

void nycleus_llvm::context::pop_temps() {
  assert(!temps_.empty());

  for(auto const [value, t]: temps_.top()) {
    builder_.CreateLifetimeEnd(value, builder_.getInt64(
      mod_->getDataLayout().getTypeStoreSize(&this->get_type(*t))
      .getFixedValue()));
  }

  temps_.pop();
}

// Converting Nycleus objects to LLVM objects //////////////////////////////////

llvm::Function& nycleus_llvm::context::get_function(function const& fn) {
  if(auto const it{functions_.find(util::non_null_ptr{&fn})}; it != functions_.cend()) {
    return *it->second;
  }

  bool const is_initializer{fn.name.is_initializer
    && fn.name.nested_parts.empty()};

  nycleus::type const* const ny_return_type{fn.return_type == nullptr
    ? nullptr : &fn.return_type.assume_other()};
  llvm::Type* const return_type{fn.return_type && !is_initializer
    && nycleus_llvm::is_trivial(fn.return_type.assume_other())
    ? &this->get_type(*ny_return_type) : builder_.getVoidTy()};

  std::vector<llvm::Type*> param_types;
  try {
    using param_size_t = std::vector<llvm::Type*>::size_type;
    param_size_t num_params{util::throwing_cast<param_size_t>(
      fn.params.size())};
    if(fn.return_type && !is_initializer
        && !nycleus_llvm::is_trivial(*ny_return_type)) {
      num_params = util::throwing_add<param_size_t>(num_params, 1u);
    }
    param_types.reserve(num_params);
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }

  if(fn.return_type && !is_initializer
      && !nycleus_llvm::is_trivial(*ny_return_type)) {
    param_types.push_back(builder_.getPtrTy());
  }
  for(auto const& param: fn.params) {
    assert(param.var);
    assert(param.var->type);
    param_types.push_back(
      nycleus_llvm::is_trivial(param.var->type.assume_other())
      ? &this->get_type(param.var->type.assume_other()) : builder_.getPtrTy());
  }

  auto* const result{llvm::Function::Create(
    llvm::FunctionType::get(return_type, param_types, false),
    llvm::GlobalValue::ExternalLinkage,
    nycleus_llvm::make_llvm_id(fn.name, is_initializer ? u8"_NYI" : u8"_NYF"),
    *mod_
  )};

  result->setCallingConv(llvm::CallingConv::Fast);
  result->setDSOLocal(true);

  result->addFnAttr(llvm::Attribute::NoUnwind);
  if(fn.return_type && !is_initializer) {
    if(nycleus_llvm::is_trivial(*ny_return_type)) {
      result->addRetAttr(llvm::Attribute::NoUndef);
    } else {
      llvm::AttrBuilder attr_builder{llvm()};
      attr_builder.addAttribute(llvm::Attribute::NonNull);
      attr_builder.addAttribute(llvm::Attribute::NoUndef);
      attr_builder.addAttribute(llvm::Attribute::NoAlias);
      attr_builder.addAttribute(llvm::Attribute::get(llvm(),
        llvm::Attribute::StructRet, &this->get_type(*ny_return_type)));
      result->addParamAttrs(0, attr_builder);
    }
  }

  unsigned const first_arg_index{fn.return_type && !is_initializer
    && !nycleus_llvm::is_trivial(*ny_return_type) ? 1u : 0u};
  for(unsigned const i: std::views::iota(first_arg_index, result->arg_size())) {
    llvm::AttrBuilder attr_builder{llvm()};
    attr_builder.addAttribute(llvm::Attribute::NoUndef);
    if(result->getArg(i)->getType()->isPointerTy()) {
      attr_builder.addAttribute(llvm::Attribute::NonNull);
    }
    result->addParamAttrs(i, attr_builder);
  }

  [[maybe_unused]] auto const [it, added] = functions_.emplace(&fn, result);
  assert(added);
  return *result;
}

llvm::GlobalVariable& nycleus_llvm::context::get_global_var(
    global_var const& var) {
  if(auto const it{global_vars_.find(util::non_null_ptr{&var})}; it != global_vars_.cend()) {
    return *it->second;
  }

  assert(var.type);
  auto* const result{new llvm::GlobalVariable{
    *mod_,
    &this->get_type(var.type.assume_other()),
    false,
    llvm::GlobalValue::ExternalLinkage,
    nullptr,
    make_llvm_id(var.name, u8"_NYV")
  }};

  [[maybe_unused]] auto const [it, added] = global_vars_.emplace(&var, result);
  assert(added);
  return *result;
}

llvm::Type& nycleus_llvm::context::get_type(type const& t) {
  return t.visit(util::overloaded{
    [this](int_type const& t) -> llvm::Type& {
      return *builder_.getIntNTy(util::asserted_cast<unsigned>(t.width));
    },

    [this](bool_type const&) -> llvm::Type& {
      return *builder_.getInt1Ty();
    },

    [this](fn_type const&) -> llvm::Type& {
      return *builder_.getPtrTy();
    },

    [this](ptr_type const&) -> llvm::Type& {
      return *builder_.getPtrTy();
    },

    [this](class_type const& t) -> llvm::Type& {
      assert(t.processing_state.load(std::memory_order::relaxed)
        == class_type::processing_state_t::cycles);

      if(auto const it{classes_.find(util::non_null_ptr{&t})}; it != classes_.cend()) {
        return *it->second;
      }

      auto* const result{llvm::StructType::create(llvm(),
        nycleus_llvm::make_llvm_id(t.name, u8"class."))};
      [[maybe_unused]] auto const [it, added] = classes_.emplace(&t, result);
      assert(added);

      std::vector<llvm::Type*> elements;
      using el_size_t = std::vector<llvm::Type*>::size_type;
      if(t.fields.empty()) {
        elements.push_back(llvm::Type::getInt8Ty(llvm()));
      } else {
        try {
          elements.reserve(util::throwing_cast<el_size_t>(t.fields.size()));
        } catch(std::overflow_error const&) {
          std::throw_with_nested(std::bad_alloc{});
        }
        for(auto const& field: t.fields) {
          assert(field.type);
          elements.push_back(&this->get_type(field.type.assume_other()));
        }
      }

      result->setBody(elements);
      return *result;
    }
  });
}
