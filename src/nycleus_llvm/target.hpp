#ifndef NYCLEUS_LLVM_TARGET_HPP_INCLUDED_J3WU6BNG6TGL4IUXSYRX3CMB6
#define NYCLEUS_LLVM_TARGET_HPP_INCLUDED_J3WU6BNG6TGL4IUXSYRX3CMB6

#include "nycleus/utils.hpp"
#include "nycleus/target.hpp"
#include <llvm/Target/TargetMachine.h>
#include <cassert>
#include <memory>
#include <utility>

namespace nycleus_llvm {

/** @brief A target suitable for compilation via LLVM. */
class target final: public nycleus::target {
private:
  std::unique_ptr<llvm::TargetMachine> llvm_machine_;

public:
  explicit target(std::unique_ptr<llvm::TargetMachine> llvm_machine) noexcept:
      llvm_machine_{std::move(llvm_machine)} {
    assert(llvm_machine_);
  }

  [[nodiscard]] llvm::TargetMachine& get_llvm_machine() const noexcept {
    assert(llvm_machine_);
    return *llvm_machine_;
  }

private:
  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws nycleus::target_error */
  [[nodiscard]] virtual nycleus::int_width_t do_get_native_width()
    const override;
};

} // nycleus_llvm

#endif
