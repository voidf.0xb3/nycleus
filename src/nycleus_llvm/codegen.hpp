#ifndef NYCLEUS_LLVM_CODEGEN_HPP_INCLUDED_JV145GWQTP7FO5LIX8FWS8J8U
#define NYCLEUS_LLVM_CODEGEN_HPP_INCLUDED_JV145GWQTP7FO5LIX8FWS8J8U

#include "util/u8compat.hpp"
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <gsl/gsl>
#include <exception>
#include <memory>

namespace nycleus {
  struct function;
  struct global_name;
  struct global_var;
}

namespace nycleus_llvm {

class target;

/** @brief Thrown on code generation error. */
class codegen_error: public std::exception {
public:
  [[nodiscard]] virtual gsl::czstring what() const noexcept override;
};

/** @brief Produces the LLVM module for the given function.
 *
 * The function's HLIR and all mentioned entities must be well-formed. All
 * entity types must have been resolved and all semantics computed. There must
 * be no named_var_ref or named_member_expr nodes; that is to say, all names
 * must have been resolved to the actual entities they refer to. There must be
 * no class cycles.
 *
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws codegen_error */
[[nodiscard]] std::unique_ptr<llvm::Module> codegen_function(
  llvm::LLVMContext&,
  target&,
  nycleus::function const&
);

/** @brief Produces the LLVM module for the given global variable.
 *
 * The variable and all mentioned entities must be well-formed. All entity types
 * must have been resolved and all semantics computed. There must be no
 * named_var_ref or named_member_expr nodes; that is to say, all names must have
 * been resolved to the actual entities they refer to. There must be no class
 * cycles.
 *
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws codegen_error */
[[nodiscard]] std::unique_ptr<llvm::Module> codegen_global_var(
  llvm::LLVMContext&,
  target&,
  nycleus::global_var const&
);

} // nycleus_llvm

#endif
