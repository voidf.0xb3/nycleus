#include "nycleus_llvm/target.hpp"
#include "nycleus/utils.hpp"
#include "nycleus/target.hpp"
#include "util/overflow.hpp"
#include <cassert>
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <new>

nycleus::int_width_t nycleus_llvm::target::do_get_native_width() const {
  try {
    assert(llvm_machine_);

    unsigned const result{llvm_machine_->createDataLayout().getPointerSizeInBits()};
    assert(result <= nycleus::max_int_width);
    return util::asserted_cast<nycleus::int_width_t>(result);
  } catch(std::bad_alloc const&) {
    throw;
  } catch(std::length_error const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(nycleus::target_error{});
  }
}
