#include "nycleus_llvm/codegen.hpp"
#include "nycleus/conversion.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/utils.hpp"
#include "nycleus/semantics.hpp"
#include "nycleus_llvm/common.hpp"
#include "nycleus_llvm/target.hpp"
#include "nycleus_llvm/transform.hpp"
#include "util/bigint.hpp"
#include "util/downcast.hpp"
#include "util/overflow.hpp"
#include "util/u8compat.hpp"
#include "util/unbounded_view.hpp"
#include "util/util.hpp"
#include "util/zip_view.hpp"
#include <gsl/gsl>
#include <llvm/ADT/APInt.h>
#include <llvm/IR/Attributes.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalValue.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/InstrTypes.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Value.h>
#include <algorithm>
#include <atomic>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <exception>
#include <memory>
#include <new>
#include <ranges>
#include <utility>
#include <variant>
#include <vector>

#ifndef NDEBUG
  #include <llvm/IR/Verifier.h>
  #include <llvm/Support/raw_os_ostream.h>
  #include <iostream>
#endif

using namespace nycleus;
using namespace nycleus_llvm;
using namespace util::u8compat_literals;

gsl::czstring nycleus_llvm::codegen_error::what() const noexcept {
  return "nycleus_llvm::codegen_error";
}

namespace {

/** @brief Emits code for the given expression.
 *
 * The return value and the treatment of the `dest` parameter depend on the
 * expression's semantics:
 * - For an expression with void semantics, a null pointer is returned, and the
 *   destination parameter is expected to be a null pointer.
 * - For a persistent expression, the returned value is a pointer to the storage
 *   containing the resulting value, and the destination parameter is expected
 *   to be a null pointer.
 * - For a transient expression of a trivial type, the value that is the result
 *   of evaluating the expression is returned, and the destination parameter is
 *   expected to be a null pointer.
 * - For a transient expression of a non-trivial type, the destination parameter
 *   is expected to be a pointer to the storage for the value, and the emitted
 *   code stores the value into the storage. If the destination parameter is a
 *   null pointer, then this creates a new temporary and uses that for storage.
 *   The return value is a pointer to the storage; that is to say, if the
 *   destination parameter is not a null pointer, then that parameter is
 *   returned, and otherwise, a pointer to the newly created temporary is
 *   returned. */
llvm::Value* emit_code_for_expr(context&, hlir::expr const&,
  llvm::Value* dest = nullptr);

/** @brief Emits code for the given statement. */
void emit_code_for_stmt(context&, hlir::stmt const&);

// Expressions /////////////////////////////////////////////////////////////////

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::int_literal const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(std::holds_alternative<value_semantics>(expr.sem));
  assert(std::get_if<value_semantics>(&expr.sem)->type);
  nycleus::int_width_t const width{static_cast<int_type const&>(
    *std::get_if<value_semantics>(&expr.sem)->type).width};

  if(expr.value == 0) {
    return llvm::ConstantInt::get(ctx->getIntNTy(util::asserted_cast<unsigned>(width)), 0);
  }

  std::vector<std::uint64_t> data;
  std::size_t const byte_size{expr.value.size()};
  std::size_t const word_size{byte_size / 8 + (byte_size % 8 == 0 ? 0 : 1)};
  try {
    using data_size_t = std::vector<std::uint64_t>::size_type;
    data.reserve(util::throwing_cast<data_size_t>(word_size));
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }

  // TODO: Create llvm::APInt from util::bigint via memcpy

  util::bigint::lnz_t const value_lnz{expr.value.lowest_non_zero()};
  for(std::size_t const i:
      std::views::iota(static_cast<std::size_t>(0), byte_size)) {
    std::size_t const offset_into_el{8 * (i % 8)};
    if(expr.value.is_negative()) {
      if(i % 8 == 0) {
        data.push_back(~static_cast<std::uint64_t>(0));
      }
      data.back() &= ~(static_cast<std::uint64_t>(0xFF) << offset_into_el);
    } else {
      if(i % 8 == 0) {
        data.push_back(0);
      }
    }

    data.back()
      |= static_cast<std::uint64_t>(expr.value.twos_compl_at(i, value_lnz))
      << offset_into_el;
  }

  return llvm::ConstantInt::get(
    ctx->getIntNTy(util::asserted_cast<unsigned>(width)),
    llvm::APInt{width, data}
  );
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::bool_literal const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);
  return ctx->getInt1(expr.value);
}

[[noreturn]] llvm::Value* emit_code_for_expr_impl(
  context&,
  hlir::named_var_ref const&,
  llvm::Value*
) noexcept {
  util::unreachable();
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::global_var_ref const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);
  return &ctx.get_global_var(*expr.target);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::local_var_ref const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);
  return &ctx.get_local(*expr.target);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::function_ref const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);
  return &ctx.get_function(*expr.target);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::cmp_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);
  assert(!expr.rels.empty());

  assert(expr.first_op);
  llvm::Value* last_op{emit_code_for_expr(ctx, *expr.first_op)};
  auto last_sem{*std::get_if<value_semantics>(&expr.first_op->sem)};

  auto emit_cmp{[&](hlir::cmp_expr::rel const& rel) -> llvm::Value& {
    assert(rel.op);
    llvm::Value* left{last_op};
    llvm::Value* right{emit_code_for_expr(ctx, *rel.op)};

    left = &nycleus_llvm::emit_transform(ctx, *left,
      last_sem, rel.left_conversion);

    last_op = right;
    last_sem = *std::get_if<value_semantics>(&rel.op->sem);

    right = &nycleus_llvm::emit_transform(ctx, *right,
      last_sem, rel.right_conversion);

    bool const is_signed{
      static_cast<int_type const&>(*last_sem.type).is_signed
      || std::holds_alternative<unsigned_to_signed>(rel.right_conversion)};

    llvm::CmpInst::Predicate llvm_rel;
    switch(rel.kind) {
      case hlir::cmp_expr::rel_kind::eq:
        llvm_rel = llvm::CmpInst::ICMP_EQ;
        break;

      case hlir::cmp_expr::rel_kind::lt:
        llvm_rel = is_signed ? llvm::CmpInst::ICMP_SLT
          : llvm::CmpInst::ICMP_ULT;
        break;

      case hlir::cmp_expr::rel_kind::le:
        llvm_rel = is_signed ? llvm::CmpInst::ICMP_SLE
          : llvm::CmpInst::ICMP_ULE;
        break;

      case hlir::cmp_expr::rel_kind::gt:
        llvm_rel = is_signed ? llvm::CmpInst::ICMP_SGT
          : llvm::CmpInst::ICMP_UGT;
        break;

      case hlir::cmp_expr::rel_kind::ge:
        llvm_rel = is_signed ? llvm::CmpInst::ICMP_SGE
          : llvm::CmpInst::ICMP_UGE;
        break;
    }
    llvm::Value& result{*ctx->CreateICmp(llvm_rel, left, right)};

    return result;
  }};

  llvm::Value* result{&emit_cmp(expr.rels.front())};

  // TODO: Comparison chain short-circuiting
  for(auto const& rel: expr.rels | std::views::drop(1)) {
    result = ctx->CreateAnd(result, &emit_cmp(rel));
  }

  return result;
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::or_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  // TODO: Logical OR short-circuiting
  return ctx->CreateOr(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::and_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  // TODO: Logical AND short-circuiting
  return ctx->CreateAnd(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::xor_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  return ctx->CreateXor(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::neq_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  return ctx->CreateICmpNE(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::add_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  return ctx->CreateAdd(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::sub_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  return ctx->CreateSub(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::mul_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  return ctx->CreateMul(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::div_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  assert(std::holds_alternative<value_semantics>(expr.sem));
  assert(std::get_if<value_semantics>(&expr.sem)->type);
  int_type const& type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.sem)->type)};
  if(type.is_signed) {
    return ctx->CreateSDiv(left, right);
  } else {
    return ctx->CreateUDiv(left, right);
  }
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::mod_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  assert(std::holds_alternative<value_semantics>(expr.sem));
  assert(std::get_if<value_semantics>(&expr.sem)->type);
  int_type const& type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.sem)->type)};
  if(type.is_signed) {
    return ctx->CreateSRem(left, right);
  } else {
    return ctx->CreateURem(left, right);
  }
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::bor_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  return ctx->CreateOr(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::band_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  return ctx->CreateAnd(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::bxor_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  return ctx->CreateXor(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::lsh_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  assert(std::holds_alternative<value_semantics>(expr.right->sem));
  assert(std::get_if<value_semantics>(&expr.right->sem)->type);
  int_type const& right_type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.right->sem)->type)};

  assert(std::holds_alternative<value_semantics>(expr.sem));
  assert(std::get_if<value_semantics>(&expr.sem)->type);
  int_type const& result_type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.sem)->type)};

  if(right_type.width < result_type.width) {
    right = ctx->CreateZExt(right, ctx->getIntNTy(result_type.width));
  } else if(right_type.width > result_type.width) {
    right = ctx->CreateTrunc(right, ctx->getIntNTy(result_type.width));
  }

  return ctx->CreateShl(left, right);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::rsh_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  assert(std::holds_alternative<value_semantics>(expr.right->sem));
  assert(std::get_if<value_semantics>(&expr.right->sem)->type);
  int_type const& right_type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.right->sem)->type)};

  assert(std::holds_alternative<value_semantics>(expr.sem));
  assert(std::get_if<value_semantics>(&expr.sem)->type);
  int_type const& result_type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.sem)->type)};

  if(right_type.width < result_type.width) {
    right = ctx->CreateZExt(right, ctx->getIntNTy(result_type.width));
  } else if(right_type.width > result_type.width) {
    right = ctx->CreateTrunc(right, ctx->getIntNTy(result_type.width));
  }

  if(result_type.is_signed) {
    return ctx->CreateAShr(left, right);
  } else {
    return ctx->CreateLShr(left, right);
  }
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::lrot_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  assert(std::holds_alternative<value_semantics>(expr.right->sem));
  assert(std::get_if<value_semantics>(&expr.right->sem)->type);
  int_type const& right_type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.right->sem)->type)};

  assert(std::holds_alternative<value_semantics>(expr.sem));
  assert(std::get_if<value_semantics>(&expr.sem)->type);
  int_type const& result_type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.sem)->type)};

  if(right_type.width < result_type.width) {
    right = ctx->CreateZExt(right, ctx->getIntNTy(result_type.width));
  } else if(right_type.width > result_type.width) {
    right = ctx->CreateTrunc(right, ctx->getIntNTy(result_type.width));
  }

  llvm::Value& shl{*ctx->CreateShl(left, right)};
  llvm::Value& shr{*ctx->CreateLShr(left,
    ctx->CreateSub(ctx->getIntN(result_type.width, result_type.width), right))};

  return ctx->CreateOr(&shl, &shr);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::rrot_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.left);
  assert(expr.right);
  llvm::Value* left{emit_code_for_expr(ctx, *expr.left)};
  llvm::Value* right{emit_code_for_expr(ctx, *expr.right)};

  left = &nycleus_llvm::emit_transform(ctx, *left,
    *std::get_if<value_semantics>(&expr.left->sem), expr.left_conversion);
  right = &nycleus_llvm::emit_transform(ctx, *right,
    *std::get_if<value_semantics>(&expr.right->sem), expr.right_conversion);

  assert(std::holds_alternative<value_semantics>(expr.right->sem));
  assert(std::get_if<value_semantics>(&expr.right->sem)->type);
  int_type const& right_type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.right->sem)->type)};

  assert(std::holds_alternative<value_semantics>(expr.sem));
  assert(std::get_if<value_semantics>(&expr.sem)->type);
  int_type const& result_type{util::downcast<int_type const&>(
    *std::get_if<value_semantics>(&expr.sem)->type)};

  if(right_type.width < result_type.width) {
    right = ctx->CreateZExt(right, ctx->getIntNTy(result_type.width));
  } else if(right_type.width > result_type.width) {
    right = ctx->CreateTrunc(right, ctx->getIntNTy(result_type.width));
  }

  llvm::Value& shr{*ctx->CreateLShr(left, right)};
  llvm::Value& shl{*ctx->CreateShl(left,
    ctx->CreateSub(ctx->getIntN(result_type.width, result_type.width), right))};

  return ctx->CreateOr(&shr, &shl);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::neg_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.op);
  llvm::Value* op{emit_code_for_expr(ctx, *expr.op)};

  op = &nycleus_llvm::emit_transform(ctx, *op,
    *std::get_if<value_semantics>(&expr.op->sem), std::monostate{});
  return ctx->CreateNeg(op);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::bnot_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.op);
  llvm::Value* op{emit_code_for_expr(ctx, *expr.op)};

  op = &nycleus_llvm::emit_transform(ctx, *op,
    *std::get_if<value_semantics>(&expr.op->sem), std::monostate{});
  return ctx->CreateNot(op);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::not_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.op);
  llvm::Value* op{emit_code_for_expr(ctx, *expr.op)};

  op = &nycleus_llvm::emit_transform(ctx, *op,
    *std::get_if<value_semantics>(&expr.op->sem), std::monostate{});
  return ctx->CreateNot(op);
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::addr_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.op);
  llvm::Value& op{*emit_code_for_expr(ctx, *expr.op)};

  auto const op_sem{*std::get_if<value_semantics>(&expr.op->sem)};
  if(op_sem.is_persistent() || !nycleus_llvm::is_trivial(*op_sem.type)) {
    // These are carried as pointers anyway, so we just return the pointer
    return &op;
  } else {
    // Logically (in the abstract machine), this is a temporary, but physically,
    // the value is passed directly as itself. So we need to store it in an
    // actual temporary.
    llvm::Value& result{ctx.add_temp(*op_sem.type)};
    ctx->CreateStore(&op, &result);
    return &result;
  }
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::deref_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.op);
  llvm::Value* op{emit_code_for_expr(ctx, *expr.op)};

  op = &nycleus_llvm::emit_transform(ctx, *op,
    *std::get_if<value_semantics>(&expr.op->sem), std::monostate{});
  return op;
}

[[noreturn]] llvm::Value* emit_code_for_expr_impl(
  context&,
  hlir::named_member_expr const&,
  llvm::Value*
) noexcept {
  util::unreachable();
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::indexed_member_expr const& expr,
  [[maybe_unused]] llvm::Value* dest
) {
  assert(!dest);

  assert(expr.op);
  assert(std::holds_alternative<value_semantics>(expr.op->sem));
  auto const sem{*std::get_if<value_semantics>(&expr.op->sem)};

  assert(sem.type);
  assert(!nycleus_llvm::is_trivial(*sem.type));
  llvm::Type& op_type{ctx.get_type(*sem.type)};

  llvm::Value& op{*emit_code_for_expr(ctx, *expr.op)};

  return ctx->CreateInBoundsGEP(&op_type, &op,
    {ctx->getInt32(0), ctx->getInt32(expr.index)});
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::call_expr const& expr,
  llvm::Value* dest
) {
  assert(expr.callee);
  llvm::Value* callee{emit_code_for_expr(ctx, *expr.callee)};

  assert(std::holds_alternative<value_semantics>(expr.callee->sem));
  auto const callee_sem{*std::get_if<value_semantics>(&expr.callee->sem)};

  assert(callee_sem.type);
  auto const& callee_type{util::downcast<fn_type const&>(*callee_sem.type)};
  bool const has_extra_return_param{callee_type.return_type != nullptr
    && !is_trivial(*callee_type.return_type)};

  std::vector<llvm::Value*> args;
  try {
    using arg_size_t = std::vector<llvm::Value*>::size_type;
    arg_size_t num_args{util::throwing_cast<arg_size_t>(expr.args.size())};
    if(has_extra_return_param) {
      num_args = util::throwing_add<arg_size_t>(num_args, 1u);
    }
    args.reserve(num_args);
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }
  if(has_extra_return_param) {
    args.push_back(dest);
  }
  for(auto const& arg: expr.args) {
    assert(arg.value);
    args.push_back(emit_code_for_expr(ctx, *arg.value));
  }

  callee = &nycleus_llvm::emit_transform(ctx, *callee, callee_sem,
    std::monostate{});
  for(auto&& [arg_value, arg_hlir]: util::zip(args
      | std::views::drop(has_extra_return_param ? 1 : 0),
      util::unbounded_view{expr.args.cbegin()})) {
    arg_value = &nycleus_llvm::emit_transform(ctx, *arg_value,
      *std::get_if<value_semantics>(&arg_hlir.value->sem), arg_hlir.conversion);
  }

  auto& llvm_callee_type{[&]() -> llvm::FunctionType& {
    llvm::Type* const return_type{
      callee_type.return_type && is_trivial(*callee_type.return_type)
      ? &ctx.get_type(*callee_type.return_type) : ctx->getVoidTy()};

    std::vector<llvm::Type*> param_types;
    try {
      using param_size_t = std::vector<llvm::Type*>::size_type;
      param_size_t num_params{util::throwing_cast<param_size_t>(
        callee_type.param_types.size())};
      if(has_extra_return_param) {
        num_params = util::throwing_add<param_size_t>(num_params, 1u);
      }
      param_types.reserve(num_params);
    } catch(std::overflow_error const&) {
      std::throw_with_nested(std::bad_alloc{});
    }

    if(has_extra_return_param) {
      param_types.push_back(ctx->getPtrTy());
    }
    for(auto const param_type: callee_type.param_types) {
      assert(param_type);
      param_types.push_back(is_trivial(*param_type)
        ? &ctx.get_type(*param_type) : ctx->getPtrTy());
    }

    return *llvm::FunctionType::get(return_type, param_types, false);
  }()};

  if(has_extra_return_param) {
    if(!dest) {
      args.front() = dest = &ctx.add_temp(*callee_type.return_type);
    } else if(dest != ctx.get_fn_dest()) {
      ctx->CreateLifetimeStart(dest, ctx->getInt64(
        ctx.get_module().getDataLayout().getTypeStoreSize(
        &ctx.get_type(*callee_type.return_type)).getFixedValue()));
    }
  }

  llvm::CallInst& result{*ctx->CreateCall(&llvm_callee_type, callee, args)};
  result.setCallingConv(llvm::CallingConv::Fast);

  llvm::AttributeList attrs;

  attrs = attrs.addFnAttribute(ctx.llvm(), llvm::Attribute::NoUnwind);
  if(callee_type.return_type) {
    if(is_trivial(*callee_type.return_type)) {
      llvm::AttrBuilder attr_builder{ctx.llvm()};
      attr_builder.addAttribute(llvm::Attribute::NoUndef);
      if(result.getFunctionType()->getReturnType()->isPointerTy()) {
        attr_builder.addAttribute(llvm::Attribute::NonNull);
      }
      attrs = attrs.addRetAttributes(ctx.llvm(), attr_builder);
    } else {
      llvm::AttrBuilder attr_builder{ctx.llvm()};
      attr_builder.addAttribute(llvm::Attribute::NonNull);
      attr_builder.addAttribute(llvm::Attribute::NoUndef);
      attr_builder.addAttribute(llvm::Attribute::get(ctx.llvm(),
        llvm::Attribute::StructRet, &ctx.get_type(*callee_type.return_type)));
      attrs = attrs.addParamAttributes(ctx.llvm(), 0, attr_builder);
    }
  }

  for(unsigned const i: std::views::iota(has_extra_return_param ? 1u : 0u,
      result.arg_size())) {
    llvm::AttrBuilder attr_builder{ctx.llvm()};
    attr_builder.addAttribute(llvm::Attribute::NoUndef);
    if(result.getArgOperand(i)->getType()->isPointerTy()) {
      attr_builder.addAttribute(llvm::Attribute::NonNull);
    }
    attrs = attrs.addParamAttributes(ctx.llvm(), i, attr_builder);
  }

  result.setAttributes(attrs);

  for(auto* const arg: args
      | std::views::drop(has_extra_return_param ? 1 : 0)) {
    ctx.drop_temp(*arg);
  }

  if(callee_type.return_type) {
    if(has_extra_return_param) {
      return dest;
    } else {
      return &result;
    }
  } else {
    return nullptr;
  }
}

llvm::Value* emit_code_for_expr_impl(
  context& ctx,
  hlir::block const& expr,
  llvm::Value* dest
) {
  for(auto const& stmt: expr.stmts) {
    assert(stmt);
    emit_code_for_stmt(ctx, *stmt);
  }

  if(expr.trail) {
    return emit_code_for_expr(ctx, *expr.trail, dest);
  } else {
    assert(!dest);
    return nullptr;
  }
}

llvm::Value* emit_code_for_expr(
  context& ctx,
  hlir::expr const& expr,
  llvm::Value* dest
) {
  assert(!std::holds_alternative<std::monostate>(expr.sem));

  [[maybe_unused]] auto const* const vs{
    std::get_if<value_semantics>(&expr.sem)};

  assert(dest == nullptr || vs && !vs->is_persistent()
    && !nycleus_llvm::is_trivial(*vs->type));

  llvm::Value* result{expr.visit([&](auto const& e)
      noexcept(noexcept(emit_code_for_expr(ctx, e, dest))) {
    return emit_code_for_expr_impl(ctx, e, dest);
  })};

  assert(!dest || result == dest);
  assert(result == nullptr || vs);

  return result;
}

// Statements //////////////////////////////////////////////////////////////////

void emit_code_for_stmt_impl(
  context& ctx,
  hlir::expr_stmt const& stmt
) {
  assert(stmt.content);
  ctx.push_temps();
  emit_code_for_expr(ctx, *stmt.content);
  ctx.pop_temps();
}

void emit_code_for_stmt_impl(
  context& ctx,
  hlir::assign_stmt const& stmt
) {
  assert(stmt.target);
  assert(stmt.value);

  ctx.push_temps();

  llvm::Value* target{emit_code_for_expr(ctx, *stmt.target)};
  llvm::Value* rhs{emit_code_for_expr(ctx, *stmt.value)};

  rhs = &nycleus_llvm::emit_transform(ctx, *rhs,
    *std::get_if<value_semantics>(&stmt.value->sem), stmt.conversion);

  ctx->CreateStore(rhs, target);

  ctx.pop_temps();
}

[[noreturn]] void emit_code_for_stmt_impl(
  context&,
  hlir::def_stmt const&
) noexcept {
  util::unreachable();
}

void emit_code_for_stmt_impl(
  context& ctx,
  hlir::local_var_init_stmt const& stmt
) {
  assert(stmt.value);

  auto const* const vs{std::get_if<value_semantics>(&stmt.value->sem)};
  assert(vs);

  llvm::Value& dest{ctx.get_local(*stmt.target)};

  ctx.push_temps();

  if(nycleus_llvm::is_trivial(*vs->type)) {
    assert(nycleus_llvm::is_trivial(stmt.target->type.assume_other()));
    llvm::Value* init_value{emit_code_for_expr(ctx, *stmt.value)};
    init_value = &nycleus_llvm::emit_transform(ctx, *init_value, *vs,
      stmt.conversion);
    ctx->CreateLifetimeStart(&dest, ctx->getInt64(ctx.get_module()
      .getDataLayout()
      .getTypeStoreSize(&ctx.get_type(stmt.target->type.assume_other()))
      .getFixedValue()));
    ctx->CreateStore(init_value, &dest);
  } else {
    assert(!vs->is_persistent());
    assert(std::holds_alternative<std::monostate>(stmt.conversion));
    emit_code_for_expr(ctx, *stmt.value, &dest);
  }

  ctx.pop_temps();
}

void emit_code_for_stmt(context& ctx, hlir::stmt const& stmt) {
  stmt.visit([&](auto const& s)
      noexcept(noexcept(emit_code_for_stmt(ctx, stmt))) {
    return emit_code_for_stmt_impl(ctx, s);
  });
}

} // (anonymous)

// Public API //////////////////////////////////////////////////////////////////

std::unique_ptr<llvm::Module> nycleus_llvm::codegen_function(
  llvm::LLVMContext& llvm_ctx,
  target& tgt,
  function const& fn
) {
  assert(std::ranges::all_of(fn.params,
    [](nycleus::function::param const& param) noexcept {
      return param.var && param.var->type;
    }));
  assert(std::ranges::all_of(fn.local_vars,
    [](std::unique_ptr<local_var> const& var) noexcept {
      return var && var->type;
    }));
  assert(fn.processing_state.load(std::memory_order::relaxed)
    == function::processing_state_t::analyzed);
  assert(fn.body);

  try {
    context ctx{llvm_ctx, tgt};

    llvm::Function& llvm_fn{ctx.get_function(fn)};

    ctx->SetInsertPoint(llvm::BasicBlock::Create(ctx.llvm(), "", &llvm_fn));

    llvm::Argument* arg{llvm_fn.arg_begin()};
    if(fn.return_type && !fn.initializee
        && !nycleus_llvm::is_trivial(fn.return_type.assume_other())) {
      ++arg;
    }
    for(auto const& param: fn.params) {
      assert(param.var);
      assert(param.var->type);
      if(nycleus_llvm::is_trivial(param.var->type.assume_other())) {
        llvm::Type& llvm_type{ctx.get_type(param.var->type.assume_other())};
        auto* const alloca_insn{ctx->CreateAlloca(&llvm_type)};
        ctx->CreateLifetimeStart(alloca_insn, ctx->getInt64(ctx.get_module()
          .getDataLayout().getTypeStoreSize(&llvm_type).getFixedValue()));
        ctx->CreateStore(arg, alloca_insn);
        ctx.add_local(*param.var, *alloca_insn);
      } else {
        ctx.add_local(*param.var, *arg);
      }
      ++arg;
    }

    for(auto const& local: fn.local_vars) {
      assert(local);
      assert(local->type);
      auto* const alloca_insn{ctx->CreateAlloca(&ctx.get_type(
        local->type.assume_other()))};
      ctx.add_local(*local, *alloca_insn);
    }

    assert(fn.body);
    assert(!std::holds_alternative<std::monostate>(fn.body->sem));

    llvm::Value* dest{nullptr};
    if(fn.return_type && !nycleus_llvm::is_trivial(
        *std::get_if<value_semantics>(&fn.body->sem)->type)) {
      if(fn.initializee) {
        dest = &ctx.get_global_var(*fn.initializee);
      } else {
        dest = llvm_fn.getArg(0);
      }
    }
    ctx.set_fn_dest(dest);

    ctx.push_temps();

    llvm::Value* return_value{emit_code_for_expr(ctx, *fn.body, dest)};

    if(fn.return_type && nycleus_llvm::is_trivial(
        fn.return_type.assume_other())) {
      assert(return_value);
      return_value = &nycleus_llvm::emit_transform(ctx, *return_value,
        *std::get_if<value_semantics>(&fn.body->sem), fn.body_conversion);
    }

    ctx.pop_temps();

    for(auto const& local: fn.local_vars) {
      llvm::Type& llvm_type{ctx.get_type(local->type.assume_other())};
      ctx->CreateLifetimeEnd(&ctx.get_local(*local),
        ctx->getInt64(ctx.get_module().getDataLayout()
        .getTypeStoreSize(&llvm_type).getFixedValue()));
    }

    for(auto const& param: fn.params) {
      if(nycleus_llvm::is_trivial(param.var->type.assume_other())) {
        llvm::Type& llvm_type{ctx.get_type(param.var->type.assume_other())};
        ctx->CreateLifetimeEnd(&ctx.get_local(*param.var),
          ctx->getInt64(ctx.get_module().getDataLayout()
          .getTypeStoreSize(&llvm_type).getFixedValue()));
      }
    }

    if(fn.return_type
        && nycleus_llvm::is_trivial(fn.return_type.assume_other())) {
      if(fn.initializee) {
        ctx->CreateStore(return_value, &ctx.get_global_var(*fn.initializee));
        ctx->CreateRetVoid();
      } else {
        ctx->CreateRet(return_value);
      }
    } else {
      assert(std::holds_alternative<std::monostate>(fn.body_conversion));
      ctx->CreateRetVoid();
    }

    auto result{std::move(ctx).release_module()};
    #ifndef NDEBUG
      llvm::raw_os_ostream error_stream{std::cerr};
      [[maybe_unused]] bool const broken{
        llvm::verifyModule(*result, &error_stream)};
      // assert(!broken);
    #endif
    return result;
  } catch(std::bad_alloc const&) {
    throw;
  } catch(std::length_error const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(codegen_error{});
  }
}

std::unique_ptr<llvm::Module> nycleus_llvm::codegen_global_var(
  llvm::LLVMContext& llvm_ctx,
  target& tgt,
  global_var const& var
) {
  assert(var.processing_state.load(std::memory_order::relaxed)
    == global_var::processing_state_t::resolved);
  assert(var.type);
  assert(var.initializer);
  assert(var.initializer->processing_state.load(std::memory_order::relaxed)
    == function::processing_state_t::analyzed);

  try {
    context ctx{llvm_ctx, tgt};

    assert(var.type);
    auto& llvm_var{ctx.get_global_var(var)};
    llvm_var.setInitializer(llvm::UndefValue::get(llvm_var.getValueType()));

    assert(var.initializer);
    auto& llvm_fn{ctx.get_function(*var.initializer)};

    auto& global_ctor_type{*llvm::StructType::get(llvm_ctx, {
      ctx->getInt32Ty(),
      llvm::PointerType::getUnqual(llvm_ctx),
      llvm::PointerType::getUnqual(llvm_ctx)
    })};
    new llvm::GlobalVariable{
      ctx.get_module(),
      llvm::ArrayType::get(&global_ctor_type, 1),
      false,
      llvm::GlobalValue::AppendingLinkage,
      llvm::ConstantArray::get(
        llvm::ArrayType::get(&global_ctor_type, 1),
        {llvm::ConstantStruct::get(&global_ctor_type, {
          ctx->getInt32(65535),
          &llvm_fn,
          llvm::ConstantPointerNull::get(llvm::PointerType::getUnqual(llvm_ctx))
        })}
      ),
      u8"llvm.global_ctors"_ch
    };

    auto result{std::move(ctx).release_module()};
    #ifndef NDEBUG
      llvm::raw_os_ostream error_stream{std::cerr};
      [[maybe_unused]] bool const broken{
        llvm::verifyModule(*result, &error_stream)};
      // assert(!broken);
    #endif
    return result;
  } catch(std::bad_alloc const&) {
    throw;
  } catch(std::length_error const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(codegen_error{});
  }
}
