#ifndef NYCLEUS_LLVM_TRANSFORM_HPP_INCLUDED_IX2Y31AQ9C9O9V31PBGQKXSLI
#define NYCLEUS_LLVM_TRANSFORM_HPP_INCLUDED_IX2Y31AQ9C9O9V31PBGQKXSLI

#include "nycleus/conversion.hpp"
#include "nycleus/semantics.hpp"
#include <llvm/IR/Value.h>

namespace nycleus_llvm {

class context;

/** @brief Emits code that transforms the given value in preparation for its use
 * by a consumer.
 *
 * This does two things:
 * 1. Transient values of trivial types are represented directly as LLVM values,
 *    whereas other values are represented as LLVM pointers to values stored
 *    elsewhere. So when given a persistent value of a trivial type, this
 *    generates a load instruction to turn it into a transient one, modeling
 *    either a copy operation or a read in preparation for a computation.
 * 2. This also emits code for the specified implicit conversion. */
[[nodiscard]] llvm::Value& emit_transform(
  context&,
  llvm::Value& input,
  nycleus::value_semantics input_semantics,
  nycleus::conversion_t conversion
);

} // nycleus_llvm

#endif
