#include "nycleus_llvm/transform.hpp"
#include "nycleus/conversion.hpp"
#include "nycleus/semantics.hpp"
#include "nycleus_llvm/common.hpp"
#include "util/overflow.hpp"
#include "util/util.hpp"
#include <llvm/IR/Value.h>
#include <cassert>
#include <variant>

using namespace nycleus;

llvm::Value& nycleus_llvm::emit_transform(
  nycleus_llvm::context& ctx,
  llvm::Value& input,
  value_semantics input_semantics,
  conversion_t conversion
) {
  llvm::Value* input_loaded{&input};

  assert(input_semantics.type);
  if(input_semantics.is_persistent()
      && nycleus_llvm::is_trivial(*input_semantics.type)) {
    input_loaded = ctx->CreateLoad(
      &ctx.get_type(*input_semantics.type), &input);
  }

  return std::visit(util::overloaded{
    [&](std::monostate) -> llvm::Value& {
      return *input_loaded;
    },

    [&](unsigned_int_extend cvt) -> llvm::Value& {
      return *ctx->CreateZExt(input_loaded,
        ctx->getIntNTy(util::asserted_cast<unsigned>(cvt.to)));
    },

    [&](signed_int_extend cvt) -> llvm::Value& {
      return *ctx->CreateSExt(input_loaded,
        ctx->getIntNTy(util::asserted_cast<unsigned>(cvt.to)));
    },

    [&](unsigned_to_signed cvt) -> llvm::Value& {
      return *ctx->CreateZExt(input_loaded,
        ctx->getIntNTy(util::asserted_cast<unsigned>(cvt.to)));
    },

    [&](ptr_mutability_conversion) -> llvm::Value& {
      return *input_loaded;
    }
  }, conversion);
}
