#ifndef NYCLEUS_LLVM_COMMON_HPP_INCLUDED_I59ZUI10H7ODBFAYIVBTU0LO8
#define NYCLEUS_LLVM_COMMON_HPP_INCLUDED_I59ZUI10H7ODBFAYIVBTU0LO8

#include "nycleus_llvm/target.hpp"
#include "nycleus/semantics.hpp"
#include "util/non_null_ptr.hpp"
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Value.h>
#include <llvm/Target/TargetMachine.h>
#include <cassert>
#include <cstdint>
#include <memory>
#include <stack>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>

namespace nycleus {
  struct class_type;
  struct function;
  struct global_name;
  struct global_var;
  struct local_var;
  struct type;
}

namespace nycleus_llvm {

/** @brief Checks if the type is trivial.
 *
 * Values of non-trivial types are stored in memory allocated via `alloca` and
 * passed around by pointer. Values of trivial types are passed around directly.
 */
[[nodiscard]] bool is_trivial(nycleus::type const& t) noexcept;

/** @brief Creates an LLVM IR identifier corresponding to the given global name.
 * @param prefix A string to prepend to the generated name. */
[[nodiscard]] std::string make_llvm_id(
  nycleus::global_name const&,
  std::u8string_view prefix
);

// Context /////////////////////////////////////////////////////////////////////

/** @brief Common context for codegen functions. */
class context {
private:
  std::unique_ptr<llvm::Module> mod_;
  llvm::IRBuilder<> builder_;
  target& tgt_;

public:
  explicit context(
    llvm::LLVMContext& llvm_ctx,
    target& tgt
  );

  // Locals ////////////////////////////////////////////////////////////////////
private:
  std::unordered_map<util::non_null_ptr<nycleus::local_var const>,
    util::non_null_ptr<llvm::Value>> local_vars_{};

public:
  /** @brief Adds a mapping from a local variable to an LLVM value. */
  void add_local(nycleus::local_var const& var, llvm::Value& llvm_value);

  /** @brief Retrieves a mapping from a local variable to an LLVM value.
   *
   * If the mapping does not exist, the behavior is undefined. */
  [[nodiscard]] llvm::Value& get_local(
    nycleus::local_var const& var
  ) const noexcept;

  // Temporaries ///////////////////////////////////////////////////////////////
private:
  using temps_entry = std::unordered_map<util::non_null_ptr<llvm::Value>,
    util::non_null_ptr<nycleus::type>>;

  std::stack<temps_entry, std::vector<temps_entry>> temps_;

public:
  /** @brief Creates a new entry on the temporaries stack.
   * @throws std::bad_alloc */
  void push_temps();

  /** @brief Creates and returns a new temporary of the given type, placed in
   * the topmost stack entry. */
  [[nodiscard]] llvm::Value& add_temp(nycleus::type&);

  /** @brief Removes the given temporary, so that it is not destroyed when the
   * topmost stack entry is popped.
   *
   * If the given value is not a temporary, this does nothing. */
  void drop_temp(llvm::Value&);

  /** @brief Pops the topmost entry on the temporaries stack, destroying all the
   * temporaries it contains. */
  void pop_temps();

  // Remembering the function destination //////////////////////////////////////
private:
  llvm::Value* fn_dest_;

public:
  /** @brief Returns the currently configured function destination.
   *
   * This remembers the pointer to the location where the function is going to
   * store its result. */
  [[nodiscard]] llvm::Value* get_fn_dest() const noexcept {
    return fn_dest_;
  }

  /** @brief Updates the function destination. */
  void set_fn_dest(llvm::Value* fn_dest) noexcept {
    fn_dest_ = fn_dest;
  }

  // Accessing parts ///////////////////////////////////////////////////////////

  /** @brief Returns a reference to the module being constructed. */
  [[nodiscard]] llvm::Module& get_module() const noexcept {
    assert(mod_);
    return *mod_;
  }

  /** @brief Releases ownership of the module. */
  [[nodiscard]] std::unique_ptr<llvm::Module> release_module() && noexcept {
    return std::move(mod_);
  }

  /** @brief Returns the LLVM's target machine object. */
  [[nodiscard]] llvm::TargetMachine& target_machine() const noexcept {
    return tgt_.get_llvm_machine();
  }

  /** @brief Provides access to the IR builder. */
  [[nodiscard]] llvm::IRBuilder<>& operator*() noexcept {
    return builder_;
  }

  /** @brief Provides access to the IR builder. */
  [[nodiscard]] llvm::IRBuilder<>* operator->() noexcept {
    return &builder_;
  }

  /** @brief Returns a reference to the LLVM context. */
  [[nodiscard]] llvm::LLVMContext& llvm() const {
    return mod_->getContext();
  }

  // Converting Nycleus objects to LLVM objects ////////////////////////////////
private:
  std::unordered_map<util::non_null_ptr<nycleus::function const>,
    util::non_null_ptr<llvm::Function>> functions_{};

public:
  /** @brief Returns the LLVM function object corresponding to the given
   * function.
   *
   * If one does not exist, creates one and adds it to the module. */
  [[nodiscard]] llvm::Function& get_function(nycleus::function const& fn);

private:
  std::unordered_map<util::non_null_ptr<nycleus::global_var const>,
    util::non_null_ptr<llvm::GlobalVariable>> global_vars_{};

public:
  /** @brief Returns the LLVM global variable object corresponding to the given
   * global variable.
   *
   * If one does not exist, creates one and adds it to the module. */
  [[nodiscard]] llvm::GlobalVariable& get_global_var(
    nycleus::global_var const& var
  );

private:
  std::unordered_map<util::non_null_ptr<nycleus::class_type const>,
    util::non_null_ptr<llvm::StructType>> classes_{};

public:
  /** @brief Returns the LLVM type corresponding to the given Nycleus type. */
  [[nodiscard]] llvm::Type& get_type(nycleus::type const& t);
};

} // nycleus_llvm

#endif
