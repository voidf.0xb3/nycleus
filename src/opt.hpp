#ifndef OPT_HPP_INCLUDED_J2A88BATN61VO2YDGN4494U6X
#define OPT_HPP_INCLUDED_J2A88BATN61VO2YDGN4494U6X

#include "unicode/windows.hpp"
#include "options/enum_parser.hpp"
#include "options/options.hpp"
#include "options/parsers.hpp"
#include <cstdint>
#include <string_view>

namespace opt {
  struct help {};
  struct list_targets {};

  struct target {};

  struct jobs {};

  enum class diagnostic_format { human, machine, json, json_raw };
  enum class color_diagnostics { autodetect, on, off };
  struct error_limit {};

  struct temp_dir {};
}
using options_config_t = options::basic_parsed_options<unicode::native_char,
  options::option<
    opt::help,
    options::option_name<u8"help", u8"?">,
    options::flag
  >,
  options::option<
    opt::list_targets,
    options::option_name<u8"list-targets">,
    options::flag
  >,

  options::option<
    opt::target,
    options::option_name<u8"target">,
    options::optional_string<unicode::native_char>
  >,

  options::option<
    opt::jobs,
    options::option_name<u8"jobs", u8"j">,
    options::optional_unsigned_int<std::uintptr_t>
  >,

  options::option<
    opt::diagnostic_format,
    options::option_name<u8"diagnostic-format">,
    options::enum_parser<
      opt::diagnostic_format::human,
      options::enum_value<opt::diagnostic_format::human, u8"human">,
      options::enum_value<opt::diagnostic_format::machine, u8"machine">,
      options::enum_value<opt::diagnostic_format::json, u8"json">,
      options::enum_value<opt::diagnostic_format::json_raw, u8"json-raw">
    >
  >,
  options::option<
    opt::color_diagnostics,
    options::option_name<u8"color-diagnostics">,
    options::enum_parser_with_empty<
      opt::color_diagnostics::autodetect,
      opt::color_diagnostics::on,
      options::enum_value<opt::color_diagnostics::autodetect, u8"auto">,
      options::enum_value<opt::color_diagnostics::on, u8"on">,
      options::enum_value<opt::color_diagnostics::off, u8"off">
    >
  >,
  options::option<
    opt::error_limit,
    options::option_name<u8"error-limit">,
    options::defaulted_unsigned_int<std::uint64_t, 20>
  >,

  options::option<
    opt::temp_dir,
    options::option_name<u8"temp">,
    options::optional_string<unicode::native_char>
  >
>;

/** @throws std::ios_base::failure */
void print_help(std::basic_string_view<unicode::native_char> program_name);

/** @brief Returns the default number of jobs, if the `--jobs` option is not
 * specified. */
std::uintptr_t default_num_jobs() noexcept;

#endif
