#include "options/options.hpp"
#include "util/format.hpp"
#include "util/nums.hpp"
#include "util/u8compat.hpp"
#include <gsl/gsl>
#include <span>
#include <string_view>

using namespace std::string_view_literals;

gsl::czstring options::error::what() const noexcept {
  return util::as_char(message_->c_str());
}

options::internal_error::internal_error():
  error{u8"internal error while parsing options"} {}

options::unrecognized_option::unrecognized_option(
  std::u8string_view option_name
): error{util::format<u8"unrecognized option: --{}">(option_name)} {}

options::unrecognized_option::unrecognized_option(
  std::u8string_view option_name,
  std::span<std::u8string_view const> matches
): error{util::format<u8"unrecognized option: --{}; possible matches: --{}">(
  option_name, util::format_join{matches, u8", --"sv})
} {}

options::unrecognized_abbrev::unrecognized_abbrev(std::u8string_view str):
  error{util::format<u8"unrecognized option abbreviation at: {}">(str)} {}

options::duplicate_option::duplicate_option(std::u8string_view option_name):
  error{util::format<u8"duplicate use of option --">(option_name)} {}

options::expected_value::expected_value(std::u8string_view option_name):
  error{util::format<u8"option --{} requires a value">(option_name)} {}

options::unexpected_value::unexpected_value(
  std::u8string_view option_name
): error{
  util::format<u8"option --{} cannot be used with a value">(option_name)
} {}

options::invalid_value::invalid_value(
  std::u8string_view option_name,
  std::u8string_view value
): error{
  util::format<u8"unrecognized value \"{}\" for option --{}">(
    value, option_name)
} {}

options::value_too_large::value_too_large(
  std::u8string_view option_name,
  std::u8string_view value,
  std::uintmax_t max_value
): error{util::format<u8"the value \"{}\" for option --{} is too large; the "
  u8"maximum permitted value is {}">(value, option_name, max_value)
} {}
