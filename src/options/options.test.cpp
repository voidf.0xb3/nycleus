#include <boost/test/unit_test.hpp>

#include "options/options.hpp"
#include "util/fixed_string.hpp"
#include "util/u8compat.hpp"
#include <gsl/gsl>
#include <optional>
#include <ostream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

using namespace std::string_literals;
using namespace util::u8compat_literals;

namespace {

struct option_value {
  std::optional<std::string> value;

  option_value(std::nullopt_t) noexcept: value{std::nullopt} {}
  option_value(std::optional<std::string> value) noexcept:
    value{std::move(value)} {}
  option_value(std::string value) noexcept: value{std::move(value)} {}

  [[nodiscard]] bool operator==(option_value const& other) const noexcept {
    return value == other.value;
  }
};

// clangd isn't smart enough to recognize that this operator is actually used

[[maybe_unused]] std::ostream& operator<<(std::ostream& os,
    option_value const& v) {
  if(v.value.has_value()) {
    os << u8'"'_ch << *v.value << u8'"'_ch;
  } else {
    os << u8"(empty)"_ch;
  }
  return os;
}

template<bool ExpectsValue>
struct test_option_parser {
  std::vector<option_value> result{};

  explicit test_option_parser(std::u8string_view) noexcept {}

  static constexpr bool expects_value{ExpectsValue};

  void parse() {
    result.emplace_back(std::nullopt);
  }

  void parse(std::string_view value) {
    result.push_back(std::string{value});
  }

  void parse(std::u8string_view value) {
    result.push_back(std::string{util::as_char(value)});
  }

  [[nodiscard]] std::vector<option_value> get_result() && noexcept {
    return std::move(result);
  }
};

static_assert(options::parser<test_option_parser<true>, char>);
static_assert(options::parser<test_option_parser<false>, char>);

struct lon {};
struct lol {};
struct don {};
struct aon {};

using test_parsed_options = options::parsed_options<
  options::option<
    lon,
    options::option_name<u8"long-option-name">,
    test_option_parser<true>
  >,
  options::option<
    lol,
    options::option_name<u8"long-option-label">,
    test_option_parser<false>
  >,
  options::option<
    don,
    options::option_name<
      u8"distinct-option-name",
      u8"a"
    >,
    test_option_parser<false>
  >,
  options::option<
    aon,
    options::option_name<
      u8"another-option-name",
      u8"B"
    >,
    test_option_parser<true>
  >
>;

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_options)

BOOST_AUTO_TEST_CASE(empty) {
  gsl::czstring const args[]{u8"app"_ch, nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  BOOST_TEST(po.get_unnamed().size() == 0);
  BOOST_TEST(po.get<lon>().size() == 0);
  BOOST_TEST(po.get<lol>().size() == 0);
  BOOST_TEST(po.get<don>().size() == 0);
  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(unnamed) {
  gsl::czstring const args[]{u8"app"_ch, u8"hello"_ch, u8"world"_ch, nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  std::string_view const unnamed_test[]{u8"hello"_ch, u8"world"_ch};
  BOOST_TEST(po.get_unnamed() == unnamed_test,
    boost::test_tools::per_element());

  BOOST_TEST(po.get<lon>().size() == 0);
  BOOST_TEST(po.get<lol>().size() == 0);
  BOOST_TEST(po.get<don>().size() == 0);
  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(unnamed_hyphen) {
  gsl::czstring const args[]{u8"app"_ch, u8"hello"_ch, u8"-"_ch, u8"world"_ch,
    nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  std::string_view const unnamed_test[]{u8"hello"_ch, u8"-"_ch, u8"world"_ch};
  BOOST_TEST(po.get_unnamed() == unnamed_test,
    boost::test_tools::per_element());

  BOOST_TEST(po.get<lon>().size() == 0);
  BOOST_TEST(po.get<lol>().size() == 0);
  BOOST_TEST(po.get<don>().size() == 0);
  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(full_names) {
  gsl::czstring const args[]{u8"app"_ch, u8"--long-option-name"_ch,
    u8"hello"_ch, u8"--long-option-label"_ch, u8"world"_ch, u8"foobar"_ch,
    nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  std::string_view const unnamed_test[]{u8"world"_ch, u8"foobar"_ch};
  BOOST_TEST(po.get_unnamed() == unnamed_test,
    boost::test_tools::per_element());

  option_value const lon_test[]{u8"hello"_chs};
  BOOST_TEST(po.get<lon>() == lon_test, boost::test_tools::per_element());

  option_value const lol_test[]{std::nullopt};
  BOOST_TEST(po.get<lol>() == lol_test, boost::test_tools::per_element());

  BOOST_TEST(po.get<don>().size() == 0);
  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(full_names_eq) {
  gsl::czstring const args[]{u8"app"_ch, u8"--long-option-name=hello"_ch,
    u8"--long-option-label=world"_ch, u8"foobar"_ch, nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  std::string_view const unnamed_test[]{u8"foobar"_ch};
  BOOST_TEST(po.get_unnamed() == unnamed_test,
    boost::test_tools::per_element());

  option_value const lon_test[]{u8"hello"_chs};
  BOOST_TEST(po.get<lon>() == lon_test, boost::test_tools::per_element());

  option_value const lol_test[]{u8"world"_chs};
  BOOST_TEST(po.get<lol>() == lol_test, boost::test_tools::per_element());

  BOOST_TEST(po.get<don>().size() == 0);
  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(full_names_trim) {
  gsl::czstring const args[]{u8"app"_ch, u8"--long-option-name"_ch, nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  BOOST_TEST(po.get_unnamed().size() == 0);

  option_value const lon_test[]{std::nullopt};
  BOOST_TEST(po.get<lon>() == lon_test, boost::test_tools::per_element());

  BOOST_TEST(po.get<lol>().size() == 0);
  BOOST_TEST(po.get<don>().size() == 0);
  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(full_names_disambig) {
  gsl::czstring const args[]{u8"app"_ch, u8"--long-option-n"_ch, u8"hello"_ch,
    nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  BOOST_TEST(po.get_unnamed().size() == 0);

  option_value const lon_test[]{u8"hello"_chs};
  BOOST_TEST(po.get<lon>() == lon_test, boost::test_tools::per_element());

  BOOST_TEST(po.get<lol>().size() == 0);
  BOOST_TEST(po.get<don>().size() == 0);
  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(full_names_ambig) {
  gsl::czstring const args[]{u8"app"_ch, u8"--long-opt"_ch, u8"hello"_ch,
    nullptr};
  BOOST_CHECK_THROW(
    (test_parsed_options{sizeof(args) / sizeof(gsl::czstring) - 1, args}),
    options::unrecognized_option);
}

BOOST_AUTO_TEST_CASE(full_names_case) {
  gsl::czstring const args[]{u8"app"_ch, u8"--lOnG-opTIOn-NAmE"_ch,
    u8"hello"_ch, u8"--LonG-oPtiON-lA"_ch, u8"world"_ch, u8"foobar"_ch,
    nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  std::string_view const unnamed_test[]{u8"world"_ch, u8"foobar"_ch};
  BOOST_TEST(po.get_unnamed() == unnamed_test,
    boost::test_tools::per_element());

  option_value const lon_test[]{u8"hello"_chs};
  BOOST_TEST(po.get<lon>() == lon_test, boost::test_tools::per_element());

  option_value const lol_test[]{std::nullopt};
  BOOST_TEST(po.get<lol>() == lol_test, boost::test_tools::per_element());

  BOOST_TEST(po.get<don>().size() == 0);
  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(full_names_unknown) {
  gsl::czstring const args[]{u8"app"_ch, u8"--unknown"_ch, nullptr};
  BOOST_CHECK_THROW(
    (test_parsed_options{sizeof(args) / sizeof(gsl::czstring) - 1, args}),
    options::unrecognized_option);
}

BOOST_AUTO_TEST_CASE(abbrev) {
  gsl::czstring const args[]{u8"app"_ch, u8"-aBc"_ch, u8"foo"_ch, u8"bar"_ch,
    nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  std::string_view const unnamed_test[]{u8"foo"_ch, u8"bar"_ch};
  BOOST_TEST(po.get_unnamed() == unnamed_test,
    boost::test_tools::per_element());

  BOOST_TEST(po.get<lon>().size() == 0);
  BOOST_TEST(po.get<lol>().size() == 0);

  option_value const don_test[]{std::nullopt};
  BOOST_TEST(po.get<don>() == don_test, boost::test_tools::per_element());

  option_value const aon_test[]{u8"c"_chs};
  BOOST_TEST(po.get<aon>() == aon_test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(abbrev_value) {
  gsl::czstring const args[]{u8"app"_ch, u8"-B"_ch, u8"c"_ch, u8"foobar"_ch,
    nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  std::string_view const unnamed_test[]{u8"foobar"_ch};
  BOOST_TEST(po.get_unnamed() == unnamed_test,
    boost::test_tools::per_element());

  BOOST_TEST(po.get<lon>().size() == 0);
  BOOST_TEST(po.get<lol>().size() == 0);
  BOOST_TEST(po.get<don>().size() == 0);

  option_value const aon_test[]{u8"c"_chs};
  BOOST_TEST(po.get<aon>() == aon_test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(abbrev_trim) {
  gsl::czstring const args[]{u8"app"_ch, u8"-B"_ch, nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  BOOST_TEST(po.get_unnamed().size() == 0);
  BOOST_TEST(po.get<lon>().size() == 0);
  BOOST_TEST(po.get<lol>().size() == 0);
  BOOST_TEST(po.get<don>().size() == 0);

  option_value const aon_test[]{std::nullopt};
  BOOST_TEST(po.get<aon>() == aon_test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(abbrev_case) {
  gsl::czstring const args[]{u8"app"_ch, u8"-A"_ch, nullptr};
  BOOST_CHECK_THROW(
    (test_parsed_options{sizeof(args) / sizeof(gsl::czstring) - 1, args}),
    options::unrecognized_abbrev);
}

BOOST_AUTO_TEST_CASE(abbrev_unknown) {
  gsl::czstring const args[]{u8"app"_ch, u8"-C"_ch, nullptr};
  BOOST_CHECK_THROW(
    (test_parsed_options{sizeof(args) / sizeof(gsl::czstring) - 1, args}),
    options::unrecognized_abbrev);
}

BOOST_AUTO_TEST_CASE(duplicates) {
  gsl::czstring const args[]{u8"app"_ch, u8"-aaa"_ch,
    u8"--long-option-label=hello"_ch, u8"-Bx"_ch, u8"--long-option-label"_ch,
    u8"-By"_ch, nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  BOOST_TEST(po.get_unnamed().size() == 0);
  BOOST_TEST(po.get<lon>().size() == 0);

  option_value const lol_test[]{u8"hello"_chs, std::nullopt};
  BOOST_TEST(po.get<lol>() == lol_test, boost::test_tools::per_element());

  option_value const don_test[]{std::nullopt, std::nullopt, std::nullopt};
  BOOST_TEST(po.get<don>() == don_test, boost::test_tools::per_element());

  option_value const aon_test[]{u8"x"_chs, u8"y"_chs};
  BOOST_TEST(po.get<aon>() == aon_test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(stop_options) {
  gsl::czstring const args[]{u8"app"_ch, u8"-a"_ch,
    u8"--long-option-label=hello"_ch, u8"world"_ch, u8"--"_ch, u8"foobar"_ch,
    u8"--not-an=option"_ch, u8"-xyz"_ch, nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  std::string_view const unnamed_test[]{u8"world"_ch, u8"foobar"_ch,
    u8"--not-an=option"_ch, u8"-xyz"_ch};
  BOOST_TEST(po.get_unnamed() == unnamed_test,
    boost::test_tools::per_element());

  BOOST_TEST(po.get<lon>().size() == 0);

  option_value const lol_test[]{u8"hello"_chs};
  BOOST_TEST(po.get<lol>() == lol_test, boost::test_tools::per_element());

  option_value const don_test[]{std::nullopt};
  BOOST_TEST(po.get<don>() == don_test, boost::test_tools::per_element());

  BOOST_TEST(po.get<aon>().size() == 0);
}

BOOST_AUTO_TEST_CASE(weird_values) {
  gsl::czstring const args[]{u8"app"_ch, u8"-B"_ch, u8"-B"_ch, u8"-B"_ch,
    u8"--"_ch, u8"--long-option-name"_ch, u8"-x"_ch, u8"--long-option-name"_ch,
    u8"--hello"_ch, nullptr};
  test_parsed_options const po{
    sizeof(args) / sizeof(gsl::czstring) - 1, args};

  BOOST_TEST(po.get_unnamed().size() == 0);

  option_value const lon_test[]{u8"-x"_chs, u8"--hello"_chs};
  BOOST_TEST(po.get<lon>() == lon_test, boost::test_tools::per_element());

  BOOST_TEST(po.get<lol>().size() == 0);
  BOOST_TEST(po.get<don>().size() == 0);

  option_value const aon_test[]{u8"-B"_chs, u8"--"_chs};
  BOOST_TEST(po.get<aon>() == aon_test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(invalid_syntax) {
  gsl::czstring const args[]{u8"app"_ch, u8"--="_ch, nullptr};
  BOOST_CHECK_THROW(
    (test_parsed_options{sizeof(args) / sizeof(gsl::czstring) - 1, args}),
    options::invalid_syntax);
}

BOOST_AUTO_TEST_SUITE_END() // test_options
