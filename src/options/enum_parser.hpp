#ifndef OPTIONS_ENUM_PARSER_HPP_INCLUDED_NDQ64OZJQTK1PDDP2675VZCO3
#define OPTIONS_ENUM_PARSER_HPP_INCLUDED_NDQ64OZJQTK1PDDP2675VZCO3

#include "options/options.hpp"
#include "unicode/casefold.hpp"
#include "unicode/encoding.hpp"
#include "unicode/normal.hpp"
#include "util/fixed_string.hpp"
#include "util/static_trie.hpp"
#include <concepts>
#include <span>
#include <string_view>
#include <type_traits>

namespace options {

// Utilities ///////////////////////////////////////////////////////////////////

/** @brief An exception that is thrown when an unrecognized option value is
 * supplied to an enum-based option parser. */
class unrecognized_enum_value: public error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit unrecognized_enum_value(std::u8string_view option_name,
    std::u8string_view value);

  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit unrecognized_enum_value(std::u8string_view option_name,
    std::u8string_view value, std::span<std::u8string_view const> matches);
};

/** @brief A helper type for configuring enum parsers.
 * @see enum_parser */
template<auto Value, util::fixed_string Str>
struct enum_value {};

/** @brief A metafunction to test that a type is a specialization of enum_value.
 */
template<typename T>
struct is_enum_value: std::false_type {};
template<typename T>
constexpr bool is_enum_value_v{is_enum_value<T>::value};
template<auto Value, util::fixed_string Str>
struct is_enum_value<enum_value<Value, Str>>: std::true_type {};

/** @brief A metafunction to extract the underlying enum type from a
 * specialization of enum_value. */
template<typename T>
struct enum_type_of;
template<typename T>
using enum_type_of_t = enum_type_of<T>::type;
template<auto Value, util::fixed_string Str>
struct enum_type_of<enum_value<Value, Str>> {
  using type = decltype(Value);
};

// Trie ////////////////////////////////////////////////////////////////////////

namespace detail_ {

template<parser_char_type Ch>
struct enum_trie_state {
  std::u8string_view option_name;
  std::basic_string_view<Ch> value;
};

template<parser_char_type Ch, typename EnumType>
struct enum_parser_fail_handler {
  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws unrecognized_enum_value */
  [[noreturn]] static EnumType fail(enum_trie_state<Ch> state,
      std::span<std::u8string_view const> matches) {
    // It could be the case that the value is invalid UTF-8, but we failed to
    // find a match *before* we found an encoding error. So to be safe, let's
    // decode & re-encode. (Also, we need to turn char into char8_t).
    std::u8string const value{encoding_error::fix_string(state.value)};

    if(matches.empty()) {
      throw unrecognized_enum_value{state.option_name, value};
    } else {
      throw unrecognized_enum_value{state.option_name, value, matches};
    }
  }
};

template<auto Value>
struct enum_parser_entry_handler {
  using enum_type_t = decltype(Value);

  template<parser_char_type Ch>
  [[nodiscard]] static constexpr enum_type_t match(enum_trie_state<Ch>)
      noexcept {
    return Value;
  }

  template<parser_char_type Ch>
  [[nodiscard]] static constexpr enum_type_t partial_match(enum_trie_state<Ch>)
      noexcept {
    return Value;
  }
};

template<parser_char_type Ch, typename... Values>
struct make_enum_trie;
template<parser_char_type Ch, typename... Values>
using make_enum_trie_t = make_enum_trie<Ch, Values...>::type;
template<parser_char_type Ch, typename EnumType, EnumType... Values,
  util::fixed_string... Strings>
struct make_enum_trie<Ch, enum_value<Values, Strings>...> {
  using type = util::static_trie::trie_t<
    enum_trie_state<Ch>,
    enum_parser_fail_handler<Ch, EnumType>,
    util::static_trie::entry<Strings, enum_parser_entry_handler<Values>>...
  >;
};

// Parsers /////////////////////////////////////////////////////////////////////

/** @brief The base class for `enum_parser` and `enum_parser_with_empty`. */
template<auto Default, typename... Values>
class enum_parser_base {
protected:
  using enum_type_t = decltype(Default);

  static_assert(std::is_enum_v<enum_type_t>);
  static_assert((... && is_enum_value_v<Values>));
  static_assert((... && std::same_as<enum_type_t, enum_type_of_t<Values>>));

private:
  enum_type_t value_;
  std::u8string_view option_name_;

protected:
  [[nodiscard]] std::u8string_view get_option_name() const noexcept {
    return option_name_;
  }

  void set_value(enum_type_t value) noexcept {
    value_ = value;
  }

public:
  constexpr enum_parser_base(std::u8string_view option_name) noexcept:
    value_{Default}, option_name_{option_name} {}

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws unrecognized_enum_value */
  template<parser_char_type Ch>
  void parse(std::basic_string_view<Ch> value_text) {
    using trie_t = make_enum_trie_t<Ch, Values...>;

    auto decoded{[&]() noexcept {
      if constexpr(std::same_as<Ch, wchar_t>) {
        return value_text | unicode::utf16_decode(unicode::eh_throw{});
      } else {
        return value_text | unicode::utf8_decode(unicode::eh_throw{});
      }
    }()};

    try {
      set_value(trie_t::lookup(
        enum_trie_state<Ch>{option_name_, value_text},
        decoded | unicode::casefold | unicode::nfd | unicode::utf8_encode()
      ));
    } catch(unicode::convert_exception const&) {
      std::throw_with_nested(encoding_error{value_text});
    }
  }

  [[nodiscard]] enum_type_t get_result() noexcept {
    return value_;
  }
};

} // detail_

/** @brief An options parser that produces a value of an enumeration type,
 * selected based on one of a preconfigured set of strings.
 *
 * The first template argument is the value returned if the option is absent.
 * It must be a value of an enumeration type. The rest of the template
 * arguments are specializations of enum_value, specifying the strings allowed
 * as option values. Each argument gives one string as the option value and the
 * corresponding enum value; the enum value must be of the same enumeration type
 * as the first template argument.
 *
 * The matching is done in a case-insensitive manner, and unambiguous prefixes
 * are allowed, similarly to matching option names. This means that option value
 * strings **must** be in casefolded & NFD form, otherwise the behavior is
 * undefined; this is **not** checked in any way!
 *
 * If the option is presented without a value, `expected_value` is thrown.
 *
 * If the value does not match any valid values, including as an unambiguous
 * prefix, `unrecognized_enum_value` is thrown.
 *
 * If the value is not a valid UTF-8 string, `encoding_error` is thrown. */
template<auto Default, typename... Values>
class enum_parser: public detail_::enum_parser_base<Default, Values...> {
private:
  using base = detail_::enum_parser_base<Default, Values...>;

public:
  using base::base;
  using base::parse;

  static constexpr bool expects_value{true};

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws expected_value */
  [[noreturn]] void parse() {
    throw expected_value{base::get_option_name()};
  }
};

/** @brief A parser that is like enum_parser, but also allows omitting the
 * value.
 *
 * If the option is encountered without a value, the value assumed is the value
 * of the EmptyValue template parameter.
 *
 * @see enum_parser */
template<auto Default, decltype(Default) EmptyValue, typename... Values>
class enum_parser_with_empty:
    public detail_::enum_parser_base<Default, Values...> {
private:
  using base = detail_::enum_parser_base<Default, Values...>;

public:
  using base::base;
  using base::parse;

  static constexpr bool expects_value{false};

  void parse() noexcept {
    base::set_value(EmptyValue);
  }
};

} // options

#endif
