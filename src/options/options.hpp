#ifndef OPTIONS_OPTIONS_HPP_INCLUDED_F07DV7XWGJTXVC93MBCE3V38U
#define OPTIONS_OPTIONS_HPP_INCLUDED_F07DV7XWGJTXVC93MBCE3V38U

#include "unicode/casefold.hpp"
#include "unicode/encoding.hpp"
#include "unicode/normal.hpp"
#include "util/cow.hpp"
#include "util/fixed_string.hpp"
#include "util/overflow.hpp"
#include "util/ranges.hpp"
#include "util/static_trie.hpp"
#include "util/type_traits.hpp"
#include "util/zstring_view.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <cassert>
#include <concepts>
#include <cstddef>
#include <exception>
#include <iterator>
#include <limits>
#include <optional>
#include <ranges>
#include <span>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace options {

/** @brief A type that can be used as the character type of options.
 *
 * It is either char, in which case it is interpreted as UTF-8, or wchar_t, in
 * which case it is interpreted as UTF-16 and must be two bytes long. */
template<typename T>
concept char_type = std::same_as<T, char>
  || std::same_as<T, wchar_t> && sizeof(T) == 2;

/** @brief A type that can be used as the character type of strings passed to
 * individual option parsers.
 *
 * It is char or char8_t, in which case it is interpreted as UTF-8, or wchar_t,
 * in which case it is interpreted as UTF-16 and must be two bytes long. */
template<typename T>
concept parser_char_type = std::same_as<T, char> || std::same_as<T, char8_t>
  || std::same_as<T, wchar_t> && sizeof(T) == 2;

// Errors //////////////////////////////////////////////////////////////////////

/** @brief The base class for all options-related exceptions. */
class error: public std::exception {
private:
  // TODO: Create a proper rcstring class
  util::cow<std::u8string> message_;

public:
  /** @throws std::bad_alloc */
  explicit error(std::u8string message): message_{std::move(message)} {}

  [[nodiscard]] virtual gsl::czstring what() const noexcept override final;

  [[nodiscard]] std::u8string_view message() const noexcept {
    return *message_;
  }
};

/** @brief An internal options-related exception of unspecified nature. */
class internal_error: public error, public std::nested_exception {
public:
  internal_error();
};

/** @brief The exception thrown if a program argument contains invalid encoding.
 *
 * The nested exception should represent the specific encoding problem. */
class encoding_error: public error, public std::nested_exception {
public:
  /** @brief Fixes the supplied string's encoding and returns the result as a
   * std::u8string.
   *
   * May also throw whatever is thrown by the operations on the provided range.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<typename S>
  requires std::ranges::input_range<S&&>
    && std::same_as<std::ranges::range_value_t<S&&>, char>
  [[nodiscard]] static std::u8string fix_string(S&& str) {
    return str | unicode::utf8_decode() | unicode::utf8_encode()
      | util::range_to<std::u8string>();
  }

  /** @brief Fixes the supplied string's encoding and returns the result as a
   * std::u8string.
   *
   * May also throw whatever is thrown by the operations on the provided range.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<typename S>
  requires std::ranges::input_range<S&&>
    && std::same_as<std::ranges::range_value_t<S&&>, wchar_t>
    && (sizeof(wchar_t) == 2)
  [[nodiscard]] static std::u8string fix_string(S&& str) {
    return str | unicode::utf16_decode() | unicode::utf8_encode()
      | util::range_to<std::u8string>();
  }

  /** @brief Returns the provided string unchanged as a std::u8string. Does not
   * check the encoding.
   *
   * This can be used as an overload in generic code.
   *
   * May also throw whatever is thrown by the operations on the provided range.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<typename S>
  requires std::ranges::input_range<S&&>
    && std::same_as<std::ranges::range_value_t<S&&>, char8_t>
  [[nodiscard]] static std::u8string fix_string(S&& str) {
    return str | util::range_to<std::u8string>();
  }

  /** @brief Wraps around the current exception.
   *
   * May also throw whatever is thrown by the operations on the provided range.
   *
   * @param str The string with invalid encoding. The encoding will be corrected
   *            before including it into the exception message.
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<typename S>
  requires std::ranges::input_range<S&&>
    && parser_char_type<std::ranges::range_value_t<S&&>>
  explicit encoding_error(S&& str):
    error{encoding_error::fix_string(std::forward<S>(str))} {}
};

/** @brief Thrown if an option name is unrecognized.
 *
 * This applies when no options match, or when a prefix could match more than
 * one option. */
class unrecognized_option: public error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit unrecognized_option(std::u8string_view option_name);

  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit unrecognized_option(std::u8string_view option_name,
    std::span<std::u8string_view const> matches);
};

/** @brief Thrown if an option abbreviation is unrecognized. */
class unrecognized_abbrev: public error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit unrecognized_abbrev(std::u8string_view);
};

/** @brief Thrown if an argument has invalid syntax.
 *
 * This only applies in the event an argument begins with the characters `--=`.
 */
class invalid_syntax: public error {
public:
  /** @throws std::bad_alloc */
  explicit invalid_syntax(std::u8string message) noexcept:
    error{std::move(message)} {}
};

/** @brief Thrown if an option expects to be used at most once, but is used
 * multiple times. */
class duplicate_option: public error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit duplicate_option(std::u8string_view option_name);
};

/** @brief Thrown if an option expects a value, but is used without one. */
class expected_value: public error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit expected_value(std::u8string_view option_name);
};

/** @brief Thrown if an option does not expect a value, but is used with one. */
class unexpected_value: public error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit unexpected_value(std::u8string_view option_name);
};

/** @brief Thrown if an option has an invalid value. */
class invalid_value: public error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit invalid_value(std::u8string_view option_name,
    std::u8string_view value);
};

/** @brief Thrown if a numeric value exceeds its allowed upper bound. */
class value_too_large: public error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  explicit value_too_large(std::u8string_view option_name,
    std::u8string_view value, std::uintmax_t max_value);
};

// Parser concept //////////////////////////////////////////////////////////////

/** @brief A parser is used to process raw option input.
 *
 * When options are parsed, the following operations are performed on parsers:
 * 1. For each recognized option, a corresponding parser is created. It is given
 *    a single constructor argument: a string view to a statically allocated
 *    string containing the option's name.
 * 2. Every time an option appears in a program argument, the `parse` member
 *    function is called on its parser. If no option value is provided, it is
 *    called without arguments; if a value is provided, a string view to it is
 *    passed as a single argument. The lifetime of the string is not guaranteed
 *    to extend past the end of the `parse` call. The parser must accept either
 *    strings with the native character type (char or wchar_t, depending on
 *    which concept is being tested) or std::u8string_view.
 * 3. After all arguments are processed, the `get_result` member function is
 *    called without arguments. The returned value is then used as the parsed
 *    option value.
 *
 * All operations on parsers are only allowed to throw either `std::bad_alloc`
 * or `options::error` (including any descendants thereof).
 *
 * A parser must contain a static constexpr data member called `expects_value`
 * which indicates whether the option expects a value. This affects the syntax
 * used for options. Regardless of the value, the parser must expect to receive
 * the option both with and without a value. */
template<typename T, typename Ch>
concept parser = char_type<Ch> && std::is_object_v<T>
  && std::is_constructible_v<T, std::u8string_view>
  && std::is_move_constructible_v<T>
  && requires(T& t) {
    t.parse();
    t.parse(std::basic_string_view<Ch>{});
    t.parse(std::u8string_view{});
    { std::declval<T&&>().get_result() } -> util::object;
    { T::expects_value } -> util::same_without_cvref<bool>;
  };

/** @brief Determines the result type of an option parser. */
template<typename T>
using parser_result_t = decltype(std::declval<T&&>().get_result());

// Configuration ///////////////////////////////////////////////////////////////

/** @brief A dummy type used to configure an option name along with an optional
 * option abbreviation. */
template<util::fixed_string Name, util::fixed_string Abbrev = u8"">
struct option_name {};

/** @brief A dummy type used to configure an option. */
template<typename Tag, typename Name, typename Parser>
struct option {};

namespace detail_ {

/** @brief A metafunction that returns the index of the given tag in the given
 * pack of tags.
 *
 * It is expected that the requested tag is actually present in the pack, and
 * that all tags are distinct. */
template<typename Tag, typename... Tags>
struct find_tag;
template<typename Tag, typename... Tags>
constexpr std::size_t find_tag_v{find_tag<Tag, Tags...>::value};
template<typename Tag, typename... Tags>
struct find_tag<Tag, Tag, Tags...>: std::integral_constant<std::size_t, 0> {};
template<typename Tag, typename Tag0, typename... Tags>
struct find_tag<Tag, Tag0, Tags...>:
  std::integral_constant<std::size_t, find_tag_v<Tag, Tags...> + 1> {};

/** @brief A metafunction that checks that all given tag types are distinct. */
template<typename... Ts>
struct tags_are_unique;
template<typename... Ts>
constexpr bool tags_are_unique_v{tags_are_unique<Ts...>::value};
template<>
struct tags_are_unique<>: std::true_type {};
template<typename T, typename... Ts>
struct tags_are_unique<T, Ts...>: std::bool_constant<
  !util::one_of<T, Ts...>
  && tags_are_unique_v<Ts...>
> {};

/** @brief A metafunction that checks that all given option names are distinct.
 */
template<util::fixed_string... Strs>
struct names_are_unique;
template<util::fixed_string... Strs>
constexpr bool names_are_unique_v{names_are_unique<Strs...>::value};
template<>
struct names_are_unique<>: std::true_type {};
template<util::fixed_string Str, util::fixed_string... Strs>
struct names_are_unique<Str, Strs...>: std::bool_constant<
  (... && (Str != Strs))
  && names_are_unique_v<Strs...>
> {};

/** @brief A metafunction that checks that all given option abbreviations are
 * distinct. */
template<util::fixed_string... Strs>
struct abbrevs_are_unique;
template<util::fixed_string... Strs>
constexpr bool abbrevs_are_unique_v{abbrevs_are_unique<Strs...>::value};
template<>
struct abbrevs_are_unique<>: std::true_type {};
template<util::fixed_string Str, util::fixed_string... Strs>
struct abbrevs_are_unique<Str, Strs...>: std::bool_constant<
  (Str == util::fixed_string{u8""} || (... && (Str != Strs)))
  && abbrevs_are_unique_v<Strs...>
> {};

// Option name tries ///////////////////////////////////////////////////////////

template<char_type Ch, parser<Ch>... Parsers>
struct name_state {
  std::span<gsl::basic_zstring<Ch const> const>::iterator* cur;
  std::span<gsl::basic_zstring<Ch const> const>::iterator end;
  std::basic_string_view<Ch> name;
  std::optional<std::basic_string_view<Ch>> value;
  std::tuple<Parsers...>* parsers;
};

template<char_type Ch>
struct name_fail_handler {
  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws unrecognized_option */
  template<parser<Ch>... Parsers>
  [[noreturn]] static void fail(name_state<Ch, Parsers...> state,
      std::span<std::u8string_view const> matches) {
    // It could be the case that the name is invalid UTF-8, but we failed to
    // find a match *before* we found an encoding error. So to be safe, let's
    // decode & re-encode. (Also, we need to turn char into char8_t).
    std::u8string const name{encoding_error::fix_string(state.name)};

    if(matches.empty()) {
      throw unrecognized_option{name};
    } else {
      throw unrecognized_option{name, matches};
    }
  }
};

template<char_type Ch, std::size_t I, parser<Ch>... Parsers>
struct name_entry_handler {
  using parser_t = std::tuple_element_t<I, std::tuple<Parsers...>>;

  /** @throws std::bad_alloc
   * @throws options::error */
  static void match(name_state<Ch, Parsers...> state) noexcept(
    noexcept(std::declval<parser_t&>().parse())
    && noexcept(std::declval<parser_t&>().parse(std::basic_string_view<Ch>{}))
  ) {
    parser_t& parser{std::get<I>(*state.parsers)};

    if(state.value.has_value()) {
      parser.parse(*state.value);
    } else if constexpr(!parser_t::expects_value) {
      parser.parse();
    } else if(*state.cur + 1 == state.end) {
      parser.parse();
    } else {
      ++*state.cur;
      parser.parse(std::basic_string_view<Ch>{**state.cur});
    }
    ++*state.cur;
  }

  /** @throws std::bad_alloc
   * @throws options::error */
  static void partial_match(name_state<Ch, Parsers...> state) noexcept(
    noexcept(std::declval<parser_t&>().parse())
    && noexcept(std::declval<parser_t&>().parse(std::basic_string_view<Ch>{}))
  ) {
    name_entry_handler::match(state);
  }
};

template<char_type Ch, typename IndexSequence, typename... Options>
struct make_name_trie_impl;
template<char_type Ch, typename IndexSequence, typename... Options>
using make_name_trie_impl_t
  = make_name_trie_impl<Ch, IndexSequence, Options...>::type;
template<char_type Ch, std::size_t... Indexes, typename... Tags,
  util::fixed_string... Names, util::fixed_string... Abbrevs,
  typename... Parsers>
struct make_name_trie_impl<Ch, std::index_sequence<Indexes...>,
    option<Tags, option_name<Names, Abbrevs>, Parsers>...> {
  using type = util::static_trie::trie_t<
    name_state<Ch, Parsers...>,
    name_fail_handler<Ch>,
    util::static_trie::entry<Names,
      name_entry_handler<Ch, Indexes, Parsers...>>...
  >;
};

template<char_type Ch, typename... Options>
struct make_name_trie: make_name_trie_impl<Ch,
  std::make_index_sequence<sizeof...(Options)>, Options...> {};
template<char_type Ch, typename... Options>
using make_name_trie_t = make_name_trie<Ch, Options...>::type;

// Option abbreviation tries ///////////////////////////////////////////////////

template<char_type Ch, parser<Ch>... Parsers>
struct abbrev_state {
  std::span<gsl::basic_zstring<Ch const> const>::iterator* cur;
  std::span<gsl::basic_zstring<Ch const> const>::iterator end;
  std::u8string::const_iterator arg_cur;
  std::u8string::const_iterator arg_end;
  std::tuple<Parsers...>* parsers;
};

template<char_type Ch>
struct abbrev_fail_handler {
  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws unrecognized_abbrev */
  template<parser<Ch>... Parsers>
  [[noreturn]] static std::u8string::const_iterator fail(
      abbrev_state<Ch, Parsers...> state) {
    throw unrecognized_abbrev{std::u8string_view{&*state.arg_cur,
      util::asserted_cast<std::size_t>(state.arg_end - state.arg_cur)}};
  }
};

template<char_type Ch, std::size_t I, parser<Ch>... Parsers>
struct abbrev_entry_handler {
  using parser_t = std::tuple_element_t<I, std::tuple<Parsers...>>;

  /** @throws std::bad_alloc
   * @throws options::error */
  [[nodiscard]] static std::u8string::const_iterator prefix_match(
    abbrev_state<Ch, Parsers...> state,
    std::u8string::const_iterator it
  ) noexcept(
    noexcept(std::declval<parser_t&>().parse()) && (!parser_t::expects_value
    || noexcept(std::declval<parser_t&>().parse(std::basic_string_view<Ch>{})))
  ) {
    parser_t& parser{std::get<I>(*state.parsers)};

    if constexpr(!parser_t::expects_value) {
      parser.parse();
      if(it == state.arg_end) {
        ++*state.cur;
      }
      return it;
    } else {
      if(it != state.arg_end) {
        parser.parse(std::u8string_view{&*it,
          util::asserted_cast<std::size_t>(state.arg_end - it)});
      } else if(*state.cur + 1 == state.end) {
        parser.parse();
      } else {
        ++*state.cur;
        parser.parse(std::basic_string_view<Ch>{**state.cur});
      }
      ++*state.cur;
      return state.arg_end;
    }
  }

  /** @throws std::bad_alloc
   * @throws options::error */
  [[nodiscard]] static std::u8string::const_iterator match(
    abbrev_state<Ch, Parsers...> state
  ) noexcept(
    noexcept(std::declval<parser_t&>().parse()) && (!parser_t::expects_value
    || noexcept(std::declval<parser_t&>().parse(std::basic_string_view<Ch>{})))
  ) {
    return abbrev_entry_handler::prefix_match(state, state.arg_end);
  }
};

template<char_type Ch, typename IndexSequence, typename Options,
  typename AllParsers>
struct make_abbrev_trie_filtered;
template<char_type Ch, typename IndexSequence, typename Options,
  typename AllParsers>
using make_abbrev_trie_filtered_t = make_abbrev_trie_filtered<Ch,
  IndexSequence, Options, AllParsers>::type;
template<
  char_type Ch,
  std::size_t... Indexes,
  typename... Tags,
  util::fixed_string... Names,
  util::fixed_string... Abbrevs,
  typename... Parsers,
  typename... AllParsers
>
struct make_abbrev_trie_filtered<
  Ch,
  std::index_sequence<Indexes...>,
  std::tuple<option<Tags, option_name<Names, Abbrevs>, Parsers>...>,
  std::tuple<AllParsers...>
> {
  using type = util::static_trie::trie_t<
    abbrev_state<Ch, AllParsers...>,
    abbrev_fail_handler<Ch>,
    util::static_trie::entry<Abbrevs,
      abbrev_entry_handler<Ch, Indexes, AllParsers...>>...
  >;
};

template<typename Options, std::size_t First = 0>
struct filter_abbrevs;
template<typename Options, std::size_t First = 0>
using filter_abbrevs_t = filter_abbrevs<Options, First>::type;
template<typename Options, std::size_t First = 0>
using filter_abbrevs_s = filter_abbrevs<Options, First>::index_seq;
template<std::size_t First>
struct filter_abbrevs<std::tuple<>, First> {
  using type = std::tuple<>;
  using index_seq = std::index_sequence<>;
};
template<typename Tag, util::fixed_string Name, typename Parser,
  typename... Options, std::size_t First>
struct filter_abbrevs<
  std::tuple<option<Tag, option_name<Name, u8"">, Parser>, Options...>,
  First
>: filter_abbrevs<std::tuple<Options...>, First + 1> {};
template<typename Tag, util::fixed_string Name, util::fixed_string Abbrev,
  typename Parser, typename... Options, std::size_t First>
requires (Abbrev.size() >= 1)
struct filter_abbrevs<
  std::tuple<
    option<Tag, option_name<Name, Abbrev>, Parser>,
    Options...
  >,
  First
> {
  using type = util::tuple_concat_t<
    std::tuple<option<Tag, option_name<Name, Abbrev>, Parser>>,
    filter_abbrevs_t<std::tuple<Options...>, First + 1>
  >;
  using index_seq = util::int_seq_concat_t<
    std::index_sequence<First>,
    filter_abbrevs_s<std::tuple<Options...>, First + 1>
  >;
};

template<typename Options>
struct extract_parsers;
template<typename Options>
using extract_parsers_t = extract_parsers<Options>::type;
template<typename... Tags, util::fixed_string... Names,
  util::fixed_string... Abbrevs, typename... Parsers>
struct extract_parsers<std::tuple<option<Tags, option_name<Names, Abbrevs>,
    Parsers>...>> {
  using type = std::tuple<Parsers...>;
};

template<char_type Ch, typename... Options>
struct make_abbrev_trie: make_abbrev_trie_filtered<
  Ch,
  filter_abbrevs_s<std::tuple<Options...>>,
  filter_abbrevs_t<std::tuple<Options...>>,
  extract_parsers_t<std::tuple<Options...>>
> {};
template<char_type Ch, typename... Options>
using make_abbrev_trie_t = make_abbrev_trie<Ch, Options...>::type;

} // detail_

// Options parsing /////////////////////////////////////////////////////////////

/** @brief A class that stores the result of parsing program arguments.
 *
 * The result consists of two parts: the options and the unnamed arguments,
 * which are unrelated to any options. The exact options that are recognized are
 * statically specified via template arguments.
 *
 * Each template argument corresponds to a recognized option. It has to be a
 * specialization of the `option` template, which has three parameters: the tag,
 * the name, and the parser.
 * - The tag can be any type; its only use is to be passed to the `get` member
 *   function to indicate which option to get. All tags must be distinct.
 * - The name must be a specialization of the `option_name` template, which has
 *   two parameters: the full name and the abbreviation.
 *   - The full name must be a string specifying the option's name. It must be a
 *     valid UTF-8 string obtaninable as the NFD of a casefolding of some
 *     string. All names must be distinct.
 *   - The abbreviation may be an empty string, in which case the option has no
 *     abbreviation. Otherwise, it must be a string specifying the abbreviation.
 *     It must be a valid UTF-8 string in NFC. No abbreviation may be a prefix
 *     of any other abbreviation; in particular, all abbreviations must be
 *     distinct.
 *
 *   ATTENTION: The restrictions on the name and abbreviation are not checked in
 *   any way due to the limitations on compile-time computation. Therefore, it
 *   is the client code's responsibility to provide correct data. If incorrect
 *   configuration is provided, the behavior is undefined!
 * - The parser must be a type that models the `parser` concept. It is used to
 *   produce the option value, which is stored in the `parsed_options` object.
 *   See the documentation of that concept for information about exactly how it
 *   is used.
 *
 * The constructor receives as input a non-empty array of strings and parses
 * them according to the specified syntax. The parsing is done in the
 * constructor, and the resulting object contains parsing results.
 *
 * The first argument is ignored, as it is expected to be the program name.
 * Subsequent arguments that do not start with the `-` character are treated as
 * unnamed arguments. An argument consisting just of the `-` character is also
 * considered an unnamed argument. If an argument is equal to `--`, then all
 * subsequent arguments are treated as unnamed.
 *
 * If an argument begins with `--` and has more characters after that, it is an
 * option referenced by its full name. Everything up to the first `=` character,
 * or the rest of the argument if it does not contain that character, is treated
 * as the option's name. The name is used to select an option. If there is a `=`
 * character, everything after it is treated as the option's value. Otherwise,
 * if the option expects a value (as determined by the parser), the next
 * argument is treated as the option's value (and is ignored for the purposes of
 * parsing arguments). If there is no next argument or if the option does not
 * expect a value, then it has no value.
 *
 * Option names are matched case-insensitively. An option name may be specified
 * as a prefix; as long as the prefix is non-empty and unambiguous, it is still
 * recognized as referring to an option.
 *
 * If an argument begins with `-` but not with `--`, and is not just the `-`
 * character, it is treated as an abbreviation pack. At each point, a prefix of
 * the remainder of the argument is matched (case-sensitively) against option
 * abbreviations; the match must be unambiguous. If the option does not expect a
 * value, then it does not have a value, and processing continues after the
 * abbreviation (or proceeds to the next argument, if the end of this argument
 * is reached). If the option expects a value, the rest of this argument is
 * treated as the option value; if the abbreviation occured at the end of the
 * argument, then the entire next argument is treated as the option's value (and
 * is ignored for the purposes of parsing arguments); if there is no next
 * argument, the option has no value.
 *
 * This class is parametrized by a character type, which must be either char or
 * wchar_t. The former is interpreted as UTF-8, while the latter is interpreted
 * as UTF-16 and must be two bytes long.
 *
 * Sometimes the program may want to handle arguments containing invalid
 * encodings; it is therefore desirable to not needlessly impose encoding
 * requirements. However, some encoding requirements are necessary to parse
 * option syntax. Therefore, encoding matters are handled as follows:
 * - The characters `-` and `=`, which are relevant for option syntax, are
 *   detected via a simple search of the corresponding code units.
 * - Unnamed arguments -- the ones that do not start with the `-` code unit, the
 *   ones that consist solely of the `-` code unit, and the ones that follow a
 *   two-code-unit `--` argument (that is not treated as an option value) are
 *   not processed in any other way. The application may therefore interpret
 *   them as strings in some other encoding, if desired.
 * - Full option names are decoded and matched against available option names.
 *   (If a full option name contains an invalid encoding, `encoding_error` is
 *   thrown.)
 * - Abbreviation packs are decoded and normalized to NFC prior to matching.
 *   This means that option values supplied as part of abbreviation packs always
 *   contain valid encodings and are in NFC.
 * - Option values supplied after the `=` sign, as well as option values
 *   supplied as separate arguments, are passed directly to option parsers and
 *   are not examined further (or, in case of separate arguments, at all). This
 *   means that parsers must expect to deal with values that are not correctly
 *   encoded. */
template<char_type Ch, typename... Options>
class basic_parsed_options;

template<typename... Options>
using parsed_options = basic_parsed_options<char, Options...>;

template<typename... Options>
using wparsed_options = basic_parsed_options<wchar_t, Options...>;

template<char_type Ch, typename... Tags, util::fixed_string... Names,
  util::fixed_string... Abbrevs, parser<Ch>... Parsers>
class basic_parsed_options<Ch,
    option<Tags, option_name<Names, Abbrevs>, Parsers>...> {
  static_assert(detail_::tags_are_unique_v<Tags...>);
  static_assert(detail_::names_are_unique_v<Names...>);
  static_assert(detail_::abbrevs_are_unique_v<Abbrevs...>);

private:
  std::vector<std::basic_string_view<Ch>> unnamed_;
  std::tuple<parser_result_t<Parsers>...> options_;

  [[nodiscard]] static constexpr Ch minus() noexcept {
    if constexpr(std::same_as<Ch, char>) {
      return static_cast<char>(u8'-');
    } else {
      return L'-';
    }
  }

  [[nodiscard]] static constexpr Ch equal() noexcept {
    if constexpr(std::same_as<Ch, char>) {
      return static_cast<char>(u8'=');
    } else {
      return L'=';
    }
  }

  [[nodiscard]] static auto decode() noexcept {
    if constexpr(std::same_as<Ch, char>) {
      return unicode::utf8_decode(unicode::eh_throw{});
    } else {
      return unicode::utf16_decode(unicode::eh_throw{});
    }
  }

  /** @throws std::bad_alloc
   * @throws options::error */
  [[nodiscard]] static std::tuple<parser_result_t<Parsers>...> parse(
    std::span<gsl::basic_zstring<Ch const> const> args,
    std::vector<std::basic_string_view<Ch>>& unnamed
  ) {
    using namespace util::u8compat_literals;

    assert(!args.empty());
    auto cur{args.begin()};
    std::tuple<Parsers...> parsers{Parsers{
      static_cast<std::u8string_view>(util::fixed_string_buf<Names>)}...};

    // Drop the first argument, which is expected to be the name of the
    // executable
    ++cur;

    while(cur != args.end()) {
      std::basic_string_view<Ch> const arg{*cur};

      if(arg.size() <= 1 || arg[0] != minus()) {
        unnamed.push_back(arg);
        ++cur;
        continue;
      }

      if(arg[1] == minus()) {
        if(arg.size() == 2) {
          ++cur;
          std::ranges::copy(std::ranges::subrange{cur, args.end()},
            std::back_inserter(unnamed));
          break;
        }

        auto const eq{std::find(arg.cbegin() + 2, arg.cend(), equal())};
        auto const name{arg.substr(2, eq - arg.cbegin() - 2)};
        auto const value{eq == arg.cend() ? std::nullopt
          : std::optional{arg.substr((eq + 1) - arg.cbegin())}};

        if(name.empty()) {
          throw invalid_syntax{u8"argument cannot begin with '--='"};
        }

        using name_trie_t = detail_::make_name_trie_t<Ch,
          option<Tags, option_name<Names, Abbrevs>, Parsers>...>;

        try {
          name_trie_t::lookup(
            detail_::name_state<Ch, Parsers...>{
              &cur, args.end(), name, value, &parsers},
            name | decode()
              | unicode::casefold | unicode::nfd | unicode::utf8_encode()
          );
        } catch(unicode::convert_exception const&) {
          std::throw_with_nested(encoding_error{name});
        }
      } else {
        std::u8string const arg_str{[&]() {
          auto view{util::basic_zstring_view<Ch>{*cur + 1} | decode()
            | unicode::nfc | unicode::utf8_encode()
          };
          std::u8string arg_str;
          try {
            std::ranges::copy(view, std::back_inserter(arg_str));
          } catch(unicode::convert_exception const&) {
            std::throw_with_nested(encoding_error{
              util::basic_zstring_view<Ch>{*cur}});
          }
          return arg_str;
        }()};

        using abbrev_trie_t = detail_::make_abbrev_trie_t<Ch,
          option<Tags, option_name<Names, Abbrevs>, Parsers>...>;

        auto cur_ch{arg_str.cbegin()};

        do {
          cur_ch = abbrev_trie_t::lookup(
            detail_::abbrev_state<Ch, Parsers...>{
              &cur, args.end(), cur_ch, arg_str.cend(), &parsers},
            std::ranges::subrange{cur_ch, arg_str.cend()}
          );
        } while(cur_ch != arg_str.cend());
      }
    }

    return std::apply([](Parsers&&... parsers) {
      return std::tuple<parser_result_t<Parsers>...>{
        std::move(parsers).get_result()...};
    }, std::move(parsers));
  }

public:
  /** @brief Parses the options.
   *
   * The arguments are accepted in the same format as the ones that are passed
   * to `main`. That is, `argv` must point to an array of C strings, where the
   * last element must be null and all the other ones must not be null; `argc`
   * must contain the number of non-null elements.
   *
   * @throws std::bad_alloc
   * @throws options::error */
  explicit basic_parsed_options(
    int argc,
    gsl::basic_zstring<Ch const> const* argv
  ):
    unnamed_{},
    options_{basic_parsed_options::parse({argv,
      util::asserted_cast<unsigned>(argc)}, unnamed_)}
  {
    assert(argc >= 1);
    assert(static_cast<unsigned>(argc) <= std::numeric_limits<std::size_t>::max());
    assert(std::all_of(argv, argv + argc,
      [](gsl::basic_zstring<Ch const> s) noexcept { return s != nullptr; }));
    assert(argv[argc] == nullptr);
  }

  /** @brief Returns a vector of arguments that don't correspond to options.
   *
   * The string views point to the strings that were originally passed to the
   * constructor. */
  [[nodiscard]] std::vector<std::basic_string_view<Ch>> const& get_unnamed()
      const noexcept {
    return unnamed_;
  }

  /** @brief Returns the value of the option with the given tag. */
  template<typename Tag>
  requires util::one_of<Tag, Tags...>
  [[nodiscard]] auto const& get() const noexcept {
    return std::get<detail_::find_tag_v<Tag, Tags...>>(options_);
  }
};

} // options

#endif
