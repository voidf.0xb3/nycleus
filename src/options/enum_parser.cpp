#include "options/enum_parser.hpp"
#include "options/options.hpp"
#include "util/format.hpp"
#include <span>
#include <string_view>

using namespace std::string_view_literals;

options::unrecognized_enum_value::unrecognized_enum_value(
  std::u8string_view option_name,
  std::u8string_view value
): error{util::format<u8"unrecognized value \"{}\" for option --{}">(
  value, option_name)} {}

options::unrecognized_enum_value::unrecognized_enum_value(
  std::u8string_view option_name,
  std::u8string_view value,
  std::span<std::u8string_view const> matches
): error{util::format<
  u8"unrecognized value \"{}\" for option --{}; possible matches: \"{}\"">(
  value, option_name, util::format_join{matches, u8"\", \""sv})
} {}
