#ifndef OPTIONS_PARSER_HPP_INLCUDED_93JVDXDMAXF69YC59ZY3HWS4I
#define OPTIONS_PARSER_HPP_INLCUDED_93JVDXDMAXF69YC59ZY3HWS4I

#include "options/options.hpp"
#include "unicode/encoding.hpp"
#include "unicode/props.hpp"
#include "util/fixed_string.hpp"
#include "util/overflow.hpp"
#include "util/ranges.hpp"
#include "util/u8compat.hpp"
#include "util/zstring_view.hpp"
#include <concepts>
#include <exception>
#include <limits>
#include <optional>
#include <ranges>
#include <string>
#include <string_view>
#include <utility>

namespace options {

/** @brief An options parser that produces a Boolean value based simply on the
 * presence or absence of the option. */
class flag {
private:
  bool value_{false};
  std::u8string_view option_name_;

public:
  explicit flag(std::u8string_view option_name) noexcept:
    option_name_{option_name} {}

  static constexpr bool expects_value{false};

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws duplicate_option */
  void parse() {
    if(value_) {
      throw duplicate_option{option_name_};
    } else {
      value_ = true;
    }
  }

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws unexpected_value */
  template<typename Ch>
  void parse(std::basic_string_view<Ch>) {
    throw unexpected_value{option_name_};
  }

  [[nodiscard]] bool get_result() const noexcept {
    return value_;
  }
};

/** @brief An option parser that produces the supplied value as a string, or a
 * default value if none is supplied.
 *
 * An option that uses this parser must be supplied with a value. The default
 * value is used only if the option is not present in the input. */
template<char_type Ch, util::fixed_string Default>
class defaulted_string {
private:
  std::optional<std::basic_string<Ch>> value_;
  std::u8string_view option_name_;

public:
  static constexpr bool expects_value{true};

  explicit defaulted_string(std::u8string_view option_name) noexcept:
    option_name_{option_name} {}

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws expected_value */
  [[noreturn]] void parse() {
    throw expected_value{option_name_};
  }

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws duplicate_option */
  void parse(std::basic_string_view<Ch> value) {
    if(value_) {
      throw duplicate_option{option_name_};
    }

    value_ = value;
  }

  void parse(std::u8string_view value) {
    if(value_) {
      throw duplicate_option{option_name_};
    }

    if constexpr(std::same_as<Ch, char>) {
      value_ = util::as_char(value);
    } else {
      value_ = value | unicode::utf8_decode(unicode::eh_assume_valid{})
        | unicode::utf16_encode<Ch>() | util::range_to<std::wstring>();
    }
  }

  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] std::basic_string<Ch> get_result() {
    if(value_) {
      return *std::move(value_);
    } else {
      if constexpr(std::same_as<Ch, char>) {
        return std::string{util::as_char(util::fixed_string_buf<Default>)};
      } else {
        return util::u8zstring_view{util::fixed_string_buf<Default>}
          | unicode::utf8_decode(unicode::eh_assume_valid{})
          | unicode::utf16_encode() | util::range_to<std::wstring>();
      }
    }
  }
};

/** @brief An option parser that produces the supplied value as a string, or an
 * empty optional.
 *
 * An option that uses this parser must be supplied with a value. The empty
 * optional is only returned if the option is not present in the input. */
template<char_type Ch>
class optional_string {
private:
  std::optional<std::basic_string<Ch>> value_;
  std::u8string_view option_name_;

public:
  static constexpr bool expects_value{true};

  explicit optional_string(std::u8string_view option_name) noexcept:
    option_name_{option_name} {}

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws expected_value */
  [[noreturn]] void parse() {
    throw expected_value{option_name_};
  }

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws duplicate_option */
  void parse(std::basic_string_view<Ch> value) {
    if(value_) {
      throw duplicate_option{option_name_};
    }

    value_ = value;
  }

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws duplicate_option */
  void parse(std::u8string_view value) {
    if(value_) {
      throw duplicate_option{option_name_};
    }

    if constexpr(std::same_as<Ch, char>) {
      value_ = util::as_char(value);
    } else {
      value_ = value | unicode::utf8_decode(unicode::eh_assume_valid{})
        | unicode::utf16_encode<Ch>() | util::range_to<std::wstring>();
    }
  }

  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] std::optional<std::basic_string<Ch>> get_result() {
    return std::move(value_);
  }
};

/** @brief An option parser that parses the supplied value as an unsigned
 * integer of the specified type, or an empty optional if the option is omitted.
 *
 * The value is represented in base 10 using any Unicode code points that serve
 * as decimal digits.
 *
 * An option that uses this parser must be supplied with a value. The empty
 * optional is returned only if the option is not present in the input. */
template<std::unsigned_integral T>
requires (!std::same_as<T, bool>)
class optional_unsigned_int {
private:
  std::optional<T> value_;
  std::u8string_view option_name_;

public:
  static constexpr bool expects_value{true};

  explicit optional_unsigned_int(std::u8string_view option_name) noexcept:
    option_name_{option_name} {}

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws expected_value */
  [[noreturn]] void parse() {
    throw expected_value{option_name_};
  }

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws duplicate_option
   * @throws encoding_error
   * @throws invalid_value */
  template<parser_char_type Ch>
  void parse(std::basic_string_view<Ch> value) {
    if(value_) {
      throw duplicate_option{option_name_};
    }

    if(value.empty()) {
      throw invalid_value{option_name_, u8""};
    }

    value_ = 0;

    try {
      auto decoded{[&]() noexcept {
        if constexpr(std::same_as<Ch, wchar_t>) {
          return value | unicode::utf16_decode(unicode::eh_throw{});
        } else {
          return value | unicode::utf8_decode(unicode::eh_throw{});
        }
      }()};

      for(char32_t const ch: decoded) {
        util::uint8_t const ch_value{unicode::decimal_value(ch)};

        if(ch_value == unicode::no_decimal_value) {
          std::u8string const fixed_value{encoding_error::fix_string(value)};
          throw invalid_value{option_name_, fixed_value};
        }

        value_ = util::throwing_add<T>(util::throwing_mul<T>(*value_, 10u), ch_value.unwrap());
      }
    } catch(unicode::convert_exception const&) {
      std::throw_with_nested(encoding_error{value});
    } catch(std::overflow_error const&) {
      std::u8string const fixed_value{encoding_error::fix_string(value)};
      std::throw_with_nested(value_too_large{option_name_, fixed_value,
        std::numeric_limits<T>::max()});
    }
  }

  [[nodiscard]] std::optional<T> get_result() const noexcept {
    return value_;
  }
};

/** @brief An option parser that parses the supplied value as an unsigned
 * integer of the specified type, or a default value if none is supplied.
 *
 * The value is represented in base 10 using any Unicode code points that serve
 * as decimal digits.
 *
 * An option that uses this parser must be supplied with a value. The default
 * value is used only if the option is not present in the input. */
template<std::unsigned_integral T, T Default>
requires (!std::same_as<T, bool>)
class defaulted_unsigned_int: public optional_unsigned_int<T> {
public:
  using optional_unsigned_int<T>::optional_unsigned_int;

  [[nodiscard]] T get_result() const noexcept {
    auto const value{optional_unsigned_int<T>::get_result()};
    if(value) {
      return *value;
    } else {
      return Default;
    }
  }
};

} // options

#endif
