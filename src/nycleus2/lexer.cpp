#include "nycleus2/lexer.hpp"
#include "nycleus2/diagnostic.hpp"
#include "nycleus2/source_file.hpp"
#include "nycleus2/source_location.hpp"
#include "nycleus2/token.hpp"
#include "nycleus2/util.hpp"
#include "unicode/basic.hpp"
#include "unicode/encoding.hpp"
#include "unicode/normal.hpp"
#include "unicode/props.hpp"
#include "util/concat_view.hpp"
#include "util/enumerate_view.hpp"
#include "util/non_null_ptr.hpp"
#include "util/ranges.hpp"
#include "util/safe_int.hpp"
#include "util/unbounded_view.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <functional>
#include <iterator>
#include <memory>
#include <optional>
#include <ranges>
#include <string>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

#include <iostream>

using namespace nycleus2;

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Lexer

bool single_lexer::wants(char32_t ch) const noexcept {
  return std::visit([&](auto const& state) noexcept {
    return state.wants(ch);
  }, state_);
}

bool single_lexer::feed(char32_t ch, source_metric loc) {
  assert(wants(ch));
  return std::visit([&](auto& state) noexcept(noexcept(state.feed(ch, loc, *this))) {
    return state.feed(ch, loc, *this);
  }, state_);
}

token single_lexer::get_token() {
  return std::visit([&](auto& state) noexcept(noexcept(state.get_token(*this))) {
    return state.get_token(*this);
  }, state_);
}

std::vector<std::unique_ptr<diagnostic>> single_lexer::get_diags() noexcept {
  return std::move(diags_);
}

bool single_lexer::state_init::feed(char32_t ch, source_metric loc, single_lexer& lexer) {
  // XID_Start characters start unprefixed words.
  if(unicode::xid_start(ch)) {
    lexer.state_ = state_in_word{tok::word::sigil_t::none, {ch}, loc};
    return true;
  }

  // Decimal digits start numeric literals.
  if(util::uint8_t const value{unicode::decimal_value(ch)};
      value != unicode::no_decimal_value) {
    lexer.state_ = state_in_num{{ch}, loc, value == 0
      ? state_in_num::substate_t::expects_x : state_in_num::substate_t::dec};
    return true;
  }

  switch(ch) {
    // Hash signs start comments.
    case U'#':
      lexer.state_ = state_in_comment_begin{loc};
      return true;

    // Dollar signs start identifiers.
    case U'$':
      lexer.state_ = state_in_word{tok::word::sigil_t::dollar, {}, loc};
      return true;

    // Underscores start extension words.
    case U'_':
      lexer.state_ = state_in_word{tok::word::sigil_t::underscore, {}, loc};
      return true;

    // Double quotes start string literals, and single quotes start character
    // literals.
    case U'"':
    case U'\'':
      lexer.state_ = state_in_string{U"", loc, ch == U'\''};
      return true;

    // Certain punctuation signs start punctuation tokens.
    case U'!':
      lexer.state_ = state_seen_excl{};
      return true;
    case U'%':
      lexer.state_ = state_seen_percent{};
      return true;
    case U'&':
      lexer.state_ = state_seen_amp{};
      return true;
    case U'(':
      lexer.state_ = state_seen_lparen{};
      return false;
    case U')':
      lexer.state_ = state_seen_rparen{};
      return false;
    case U'*':
      lexer.state_ = state_seen_star{};
      return true;
    case U'+':
      lexer.state_ = state_seen_plus{};
      return true;
    case U',':
      lexer.state_ = state_seen_comma{};
      return false;
    case U'-':
      lexer.state_ = state_seen_minus{};
      return true;
    case U'.':
      lexer.state_ = state_seen_point{loc};
      return true;
    case U'/':
      lexer.state_ = state_seen_slash{};
      return true;
    case U':':
      lexer.state_ = state_seen_colon{};
      return false;
    case U';':
      lexer.state_ = state_seen_semicolon{};
      return false;
    case U'<':
      lexer.state_ = state_seen_lt{};
      return true;
    case U'=':
      lexer.state_ = state_seen_assign{};
      return true;
    case U'>':
      lexer.state_ = state_seen_gt{};
      return true;
    case U'?':
      lexer.state_ = state_seen_question{};
      return false;
    case U'@':
      lexer.state_ = state_seen_at{};
      return false;
    case U'[':
      lexer.state_ = state_seen_lsquare{};
      return false;
    case U']':
      lexer.state_ = state_seen_rsquare{};
      return false;
    case U'^':
      lexer.state_ = state_seen_bitwise_xor{};
      return true;
    case U'{':
      lexer.state_ = state_seen_lbrace{};
      return false;
    case U'|':
      lexer.state_ = state_seen_pipe{};
      return true;
    case U'}':
      lexer.state_ = state_seen_rbrace{};
      return false;
    case U'~':
      lexer.state_ = state_seen_tilde{};
      return false;

    // Otherwise, this is an invalid token.
    default:
      lexer.diags_.push_back(std::make_unique<diag::unexpected_code_point>(
        source_metric_range{{0, 0}, loc}, ch));
      lexer.state_ = state_error{};
      return false;
  }
}

token single_lexer::state_error::get_token(single_lexer&) noexcept {
  return tok::error{};
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Comments

// TODO: Warn if comments contain starting sequences of other comments

// TODO: Warn on exotic newlines in comments

namespace {

/** @brief Checks if the code point terminates a single-line comment.
 *
 * This is used for lexing comments and is independent of any logic for determining source
 * locations of code points. */
bool is_comment_terminator(char32_t ch) noexcept {
  assert(unicode::is_scalar_value(ch));
  return ch == U'\n'
      || ch == U'\v'
      || ch == U'\f'
      || ch == U'\u0085'
      || ch == U'\u2028'
      || ch == U'\u2029';
}

} // (anonymous)

void single_lexer::comment_bidi_tracker::feed(
  char32_t ch,
  source_metric_range loc,
  single_lexer& lexer
) {
  auto is_isolate_code{[](open_code ch) noexcept {
    return ch == open_code::lri || ch == open_code::rli || ch == open_code::fsi;
  }};

  switch(ch) {
    case U'\u202A': // LEFT-TO-RIGHT EMBEDDING
    case U'\u202B': // RIGHT-TO-LEFT EMBEDDING
    case U'\u202D': // LEFT-TO-RIGHT OVERRIDE
    case U'\u202E': // RIGHT-TO-LEFT OVERRIDE
      if(open_codes_.empty() || !is_isolate_code(open_codes_.back().first)) {
        open_codes_.emplace_back(
          ch == U'\u202A' ? open_code::lre
          : ch == U'\u202B' ? open_code::rle
          : ch == U'\u202D' ? open_code::lro
          : open_code::rlo, loc);
      }
      break;

    case U'\u202C': // POP DIRECTIONAL FORMATTING
      if(open_codes_.empty()) {
        lexer.diags_.emplace_back(std::make_unique<diag::comment_unmatched_bidi_code>(
          loc, diag::comment_unmatched_bidi_code::bidi_code_t::pdf));
      } else if(!is_isolate_code(open_codes_.back().first)) {
        open_codes_.pop_back();
      }
      break;

    case U'\u2066': // LEFT-TO-RIGHT ISOLATE
    case U'\u2067': // RIGHT-TO-LEFT ISOLATE
    case U'\u2068': // FIRST STRONG ISOLATE
      open_codes_.emplace_back(
        ch == U'\u2066' ? open_code::lri
        : ch == U'\u2067' ? open_code::rli
        : open_code::fsi, std::move(loc));
      break;

    case U'\u2069': // POP DIRECTIONAL ISOLATE
      if(open_codes_.empty() || !is_isolate_code(open_codes_.back().first)) {
        lexer.diags_.emplace_back(std::make_unique<diag::comment_unmatched_bidi_code>(loc,
          diag::comment_unmatched_bidi_code::bidi_code_t::pdi));
      } else {
        open_codes_.pop_back();
      }
      break;

    case U'\u000A': // line feed
    case U'\u000B': // line tabulation
    case U'\u000C': // form feed
    case U'\u000D': // carriage return
    case U'\u0085': // next line
    case U'\u2028': // LINE SEPARATOR
    case U'\u2029': // PARAGRAPH SEPARATOR
      // If there's a carriage return followed by a line feed, the state will be reset twice.
      // That's fine: we're not interested here in counting lines, just resetting the state
      // whenever a line ends.
      this->end(lexer);
      break;

    default:
      break;
  }
}

void single_lexer::comment_bidi_tracker::end(single_lexer& lexer) {
  if(open_codes_.empty()) {
    return;
  }
  for(auto& [ch, loc]: open_codes_) {
    diag::comment_unmatched_bidi_code::bidi_code_t code;
    switch(ch) {
      using enum diag::comment_unmatched_bidi_code::bidi_code_t;
      case open_code::lre: code = lre; break;
      case open_code::rle: code = rle; break;
      case open_code::lro: code = lro; break;
      case open_code::rlo: code = rlo; break;
      case open_code::lri: code = lri; break;
      case open_code::rli: code = rli; break;
      case open_code::fsi: code = fsi; break;
    }
    lexer.diags_.emplace_back(std::make_unique<diag::comment_unmatched_bidi_code>(loc, code));
  }
  open_codes_.clear();
}

bool single_lexer::state_in_comment_begin::feed(
  char32_t ch,
  source_metric loc,
  single_lexer& lexer
) const {
  if(ch == U'*') {
    lexer.state_ = state_in_comment_ml{token_loc.add(loc)};
    return true;
  }

  if(ch == U'+') {
    lexer.state_ = state_in_comment_mlnest{token_loc.add(loc)};
    return true;
  }

  if(ch == U'\r') {
    lexer.state_ = state_in_comment_sl_seen_cr{token_loc.add(loc)};
    return true;
  }

  comment_bidi_tracker bidi_tracker;
  bidi_tracker.feed(ch, source_metric_range{token_loc, loc}, lexer);
  lexer.state_ = state_in_comment_sl{token_loc.add(loc), std::move(bidi_tracker)};
  return !is_comment_terminator(ch);
}

bool single_lexer::state_in_comment_sl::feed(
  char32_t ch,
  source_metric loc,
  single_lexer& lexer
) {
  bidi_tracker.feed(ch, source_metric_range{token_loc, loc}, lexer);
  token_loc = token_loc.add(loc);

  if(ch == U'\r') {
    bidi_tracker.end(lexer);
    lexer.state_ = state_in_comment_sl_seen_cr{token_loc};
    return true;
  }

  if(is_comment_terminator(ch)) {
    bidi_tracker.end(lexer);
    return false;
  }

  return true;
}

token single_lexer::state_in_comment_sl::get_token(single_lexer& lexer) {
  bidi_tracker.end(lexer);
  return tok::comment{};
}

bool single_lexer::state_in_comment_sl_seen_cr::wants(char32_t ch) noexcept {
  return ch == U'\n';
}

bool single_lexer::state_in_comment_sl_seen_cr::feed(
  char32_t ch,
  source_metric loc,
  single_lexer&
) {
  assert(ch == U'\n');
  token_loc = token_loc.add(loc);
  return false;
}

bool single_lexer::state_in_comment_ml::feed(
  char32_t ch,
  source_metric loc,
  single_lexer& lexer
) {
  assert(substate != substate_t::seen_end);

  bidi_tracker.feed(ch, {token_loc, loc}, lexer);
  token_loc = token_loc.add(loc);

  if(ch == U'*') {
    substate = substate_t::seen_star;
    return true;
  } else if(substate == substate_t::seen_star && ch == U'#') {
    substate = substate_t::seen_end;
    bidi_tracker.end(lexer);
    return false;
  } else {
    substate = substate_t::init;
    return true;
  }
}

token single_lexer::state_in_comment_ml::get_token(single_lexer& lexer) {
  bidi_tracker.end(lexer);
  if(substate != substate_t::seen_end) {
    lexer.diags_.push_back(std::make_unique<diag::unterminated_comment>(
      source_metric_range{{0, 0}, token_loc}));
  }
  return tok::comment{};
}

bool single_lexer::state_in_comment_mlnest::feed(
  char32_t ch,
  source_metric loc,
  single_lexer& lexer
) {
  assert(substate != substate_t::seen_end);

  bidi_tracker.feed(ch, source_metric_range{token_loc, loc}, lexer);
  token_loc = token_loc.add(loc);

  switch(substate) {
    case substate_t::init:
      if(ch == U'+') {
        substate = substate_t::seen_plus;
      } else if(ch == U'#') {
        substate = substate_t::seen_hash;
      }
      break;

    case substate_t::seen_plus:
      if(ch == U'+') {
        substate = substate_t::seen_plus;
      } else if(ch == U'#') {
        if(nesting == 0) {
          bidi_tracker.end(lexer);
          substate = substate_t::seen_end;
          return false;
        } else {
          --nesting;
          substate = substate_t::init;
        }
      } else {
        substate = substate_t::init;
      }
      break;

    case substate_t::seen_hash:
      if(ch == U'+') {
        ++nesting;
        substate = substate_t::init;
      } else if(ch == U'#') {
        substate = substate_t::seen_hash;
      } else {
        substate = substate_t::init;
      }
      break;

    case substate_t::seen_end:
      util::unreachable();
  }

  return true;
}

token single_lexer::state_in_comment_mlnest::get_token(single_lexer& lexer) {
  bidi_tracker.end(lexer);
  if(substate != substate_t::seen_end) {
    lexer.diags_.push_back(std::make_unique<diag::unterminated_comment>(
      source_metric_range{{0, 0}, token_loc}));
  }
  return tok::comment{};
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Words

namespace {

constexpr char32_t zwnj{U'\u200C'};
constexpr char32_t zwj{U'\u200D'};

} // (anonymous)

bool single_lexer::state_in_word::wants(char32_t ch) noexcept {
  return ch == U'$' || ch == zwj || ch == zwnj || unicode::xid_continue(ch);
}

bool single_lexer::state_in_word::feed(
  char32_t ch,
  source_metric loc,
  single_lexer&
) {
  if(ch == zwj || ch == zwnj) {
    format_codes.emplace_back(util::wrap(buf.size()), source_metric_range{token_loc, loc});
  }

  buf.push_back(ch);
  token_loc = token_loc.add(loc);
  return true;
}

token single_lexer::state_in_word::get_token(single_lexer& lexer) {
  if(buf.empty()) {
    switch(sigil) {
      case tok::word::sigil_t::dollar:
        lexer.diags_.push_back(std::make_unique<diag::empty_id>(
          source_metric_range{{0, 0}, token_loc}));
        return tok::error{};

      case tok::word::sigil_t::underscore:
        lexer.diags_.push_back(std::make_unique<diag::empty_ext>(
          source_metric_range{{0, 0}, token_loc}));
        return tok::error{};

      case tok::word::sigil_t::none:
        util::unreachable();
    }
  }

  std::u8string text;

  if(format_codes.empty()) {
    std::ranges::copy(buf | unicode::nfc | unicode::utf8_encode(), std::back_inserter(text));
  } else {
    if(!unicode::is_nfc_qc(buf)) {
      // Normalize the buffer while remembering the locations of the ZW(N)Js (note that they
      // are NFC-stable, so we can just normalize the parts in between)
      std::u32string normalized;
      std::u32string::const_iterator cur_begin{buf.cbegin()};
      for(auto& [index, loc]: format_codes) {
        std::u32string::const_iterator const cur_end{buf.cbegin()
          + index.cast<util::ptrdiff_t>().unwrap()};
        unicode::to_nfc(std::ranges::subrange{cur_begin, cur_end}, normalized);

        // `index` used to be the index of this code point in the old, unnormalized, buffer.
        // Change it to be the index of this code point in the new, normalized, buffer.
        util::size_t const new_index{util::wrap(normalized.size())};
        assert(buf[index.unwrap()] == zwj || buf[index.unwrap()] == zwnj);
        normalized += buf[index.unwrap()];
        index = new_index;

        cur_begin = cur_end + 1;
      }

      unicode::to_nfc(std::ranges::subrange{cur_begin, buf.cend()}, normalized);
      assert(normalized == unicode::to_nfc(buf));
      buf = std::move(normalized);
    }
    std::ranges::copy(buf | std::views::filter([](char32_t ch) noexcept {
      return ch != zwj && ch != zwnj;
    }) | unicode::nfc | unicode::utf8_encode(), std::back_inserter(text));

    // Now verify the context of all ZW(N)J occurences.

    for(auto [index, loc]: format_codes) {
      unicode::script_t context_script{unicode::script_t::unknown};

      // First, let's check for the prefix context from rules A2 and B. It's going to be useful
      // regardless of whether we're checking the context of a ZWJ or a ZWNJ.

      // $L $M* $V $M₁* ZW(N)J ...
      // where:
      // $L = {gc=L}
      // $V = {ccc=virama}
      // $M = {gc=Mn}
      // $M₁ = {gc=Mn & ccc!=0}
      bool common_prefix_matched{false};

      // Whether, at the current position, we could be in the run of non-spacing marks after
      // the virama.
      bool after_virama{true};
      // Whether, at the current position, we could be in the run of non-spacing marks before
      // the virama.
      bool before_virama{false};

      std::u32string::const_iterator cur{buf.cbegin()
        + index.cast<util::ptrdiff_t>().unwrap()};
      while(cur != buf.cbegin()) {
        --cur;

        unicode::script_t const script{unicode::script(*cur)};
        if(context_script == unicode::script_t::unknown) {
          if(
            script != unicode::script_t::unknown
            && script != unicode::script_t::common
            && script != unicode::script_t::inherited
          ) {
            context_script = script;
          }
        } else if(script != context_script) {
          break;
        }

        unicode::gc_t const gc{unicode::gc(*cur)};
        unicode::ccc_t const ccc{unicode::ccc(*cur)};

        if(before_virama) {
          if(unicode::is_gc_group(gc, unicode::gc_group_t::l)) {
            common_prefix_matched = true;
            break;
          }
          before_virama = gc == unicode::gc_t::mn;
        }

        if(after_virama) {
          before_virama = before_virama || ccc == unicode::ccc_virama;
          after_virama = gc == unicode::gc_t::mn && ccc != 0;
        }

        if(!before_virama && !after_virama) {
          break;
        }
      }

      if(buf[index.unwrap()] == zwj) {
        // Rule B suffix context:
        // ... ZWJ (?!$D)
        // where:
        // $D = {Indic_Syllabic_Category=Vowel_Dependent}
        if(!common_prefix_matched || (index + 1 != util::wrap(buf.size())
            && unicode::indic_syllabic_category(buf[(index + 1).unwrap()])
            == unicode::indic_syllabic_category_t::vowel_dependent)) {
          // Rule B violated.
          lexer.diags_.push_back(std::make_unique<diag::unexpected_zwj>(loc));
        }
      } else {
        assert(buf[index.unwrap()] == zwnj);

        if(common_prefix_matched) {
          // Prefix part of Rule A2 is satisfied. Check the suffix part also:
          // ... ZWNJ $M₁* $L
          // where:
          // $L = {gc=L}
          // $M₁ = {gc=Mn & ccc!=0}

          auto next_letter{buf.cbegin() + (index + 1).cast<util::ptrdiff_t>().unwrap()};
          while(next_letter != buf.cend()) {
            if(
              unicode::gc(*next_letter) != unicode::gc_t::mn
              || unicode::ccc(*next_letter) == 0
            ) {
              break;
            }

            unicode::script_t const script{unicode::script(*next_letter)};
            if(context_script == unicode::script_t::unknown) {
              if(
                script != unicode::script_t::unknown
                && script != unicode::script_t::common
                && script != unicode::script_t::inherited
              ) {
                context_script = script;
              }
            } else if(script != context_script) {
              break;
            }

            ++next_letter;
          }

          if(
            next_letter != buf.cend()
            && unicode::is_gc_group(*next_letter, unicode::gc_group_t::l)
            && (context_script == unicode::script_t::unknown
              || unicode::script(*next_letter) == context_script)
          ) {
            // Suffix part of Rule A2 satisfied.
            continue;
          }
        }

        // Rule A2 has not been satisfied. Check Rule A1 now.

        context_script = unicode::script_t::unknown;

        // Rule A1 prefix context:
        // $LJ $T* ZWNJ ...
        // where:
        // $LJ = {Joining_Type=Dual_Joining|Joining_Type=Left_Joining}
        // $T = {Joining_Type=Transparent}
        std::u32string::const_iterator prev_letter{buf.cbegin()
          + index.cast<util::ptrdiff_t>().unwrap()};
        bool matched{false};
        while(prev_letter != buf.cbegin()) {
          --prev_letter;

          unicode::script_t const script{unicode::script(*prev_letter)};
          if(context_script == unicode::script_t::unknown) {
            if(
              script != unicode::script_t::unknown
              && script != unicode::script_t::common
              && script != unicode::script_t::inherited
            ) {
              context_script = script;
            }
          } else if(script != context_script) {
            break;
          }

          auto const jt{unicode::joining_type(*prev_letter)};
          if(
            jt == unicode::joining_type_t::dual_joining
            || jt == unicode::joining_type_t::left_joining
          ) {
            matched = true;
            break;
          }
          if(jt != unicode::joining_type_t::transparent) {
            break;
          }
        }
        if(!matched) {
          lexer.diags_.emplace_back(
            std::make_unique<diag::unexpected_zwnj>(loc));
          continue;
        }

        // Rule A1 suffix context:
        // ... ZWNJ $T* $RJ
        // where:
        // $T = {Joining_Type=Transparent}
        // $RJ = {Joining_Type=Dual_Joining|Joining_Type=Right_Joining}
        std::u32string::const_iterator next_letter{buf.cbegin()
          + (index + 1).cast<util::ptrdiff_t>().unwrap()};
        matched = false;
        while(next_letter != buf.cend()) {
          unicode::script_t const script{unicode::script(*next_letter)};
          if(context_script == unicode::script_t::unknown) {
            if(
              script != unicode::script_t::unknown
              && script != unicode::script_t::common
              && script != unicode::script_t::inherited
            ) {
              context_script = script;
            }
          } else if(script != context_script) {
            break;
          }

          auto const jt{unicode::joining_type(*next_letter)};
          if(
            jt == unicode::joining_type_t::dual_joining
            || jt == unicode::joining_type_t::right_joining
          ) {
            matched = true;
            break;
          }
          if(jt != unicode::joining_type_t::transparent) {
            break;
          }

          ++next_letter;
        }
        if(!matched) {
          lexer.diags_.emplace_back(std::make_unique<diag::unexpected_zwnj>(loc));
        }

        // Rule A1 satisfied.
      }
    }
  }

  text.shrink_to_fit();
  return tok::word{std::move(text), sigil};
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Numeric literals

// TODO: Floating-point literals

bool single_lexer::state_in_num::wants(char32_t ch) const noexcept {
  return ch == U'.' || ch == U'$' || unicode::xid_continue(ch) || (
    (ch == U'+' || ch == U'-')
    && (substate == substate_t::dec_exp || substate == substate_t::hex_exp)
  );
}

bool single_lexer::state_in_num::feed(
  char32_t ch,
  source_metric loc,
  single_lexer&
) {
  switch(substate) {
    case substate_t::expects_x:
      if(ch == U'X' || ch == U'x') {
        substate = substate_t::hex;
      } else if(ch != U'_') {
        substate = substate_t::dec;
      }
      break;

    case substate_t::dec:
    case substate_t::dec_exp:
      if(ch == U'E' || ch == U'e' || ch == U'P' || ch == U'p') {
        substate = substate_t::dec_exp;
      } else {
        substate = substate_t::dec;
      }
      break;

    case substate_t::hex:
    case substate_t::hex_exp:
      if(ch == U'P' || ch == U'p') {
        substate = substate_t::hex_exp;
      } else {
        substate = substate_t::hex;
      }
      break;
  }

  buf.push_back(ch);
  token_loc = token_loc.add(loc);
  return true;
}

token single_lexer::state_in_num::get_token(single_lexer& lexer) {
  if(!unicode::is_nfc_qc(buf)) {
    buf = unicode::to_nfc(buf);
  }

  buf.erase(std::remove(buf.begin(), buf.end(), U'_'), buf.cend());

  std::u32string_view str{buf};

  auto make_error{[&](std::unique_ptr<diagnostic> d) {
    lexer.diags_.push_back(std::move(d));
    return tok::error{};
  }};

  // Handle type suffixes

  if(
    str.size() >= 3
    && (
      str[str.size() - 3] == U'R'
      || str[str.size() - 3] == U'r'
      || !(
        unicode::decimal_value(str[0]) == 0
        && (str[1] == U'X' || str[1] == U'x')
      ) && (
        str[str.size() - 3] == U'F'
        || str[str.size() - 3] == U'f'
      )
    ) && (
      (
        unicode::decimal_value(str[str.size() - 2]) == 3
        && unicode::decimal_value(str[str.size() - 1]) == 2
      ) || (
        unicode::decimal_value(str[str.size() - 2]) == 6
        && unicode::decimal_value(str[str.size() - 1]) == 4
      )
    )
  ) {
    return make_error(std::make_unique<diag::fp_literals_unsupported>(
      source_metric_range{{0, 0}, token_loc}));
  }

  std::optional<int_type_suffix> type_suffix;
  if(
    auto last_non_digit{util::find_last_if(
      str,
      [](char32_t ch) noexcept {
        return unicode::decimal_value(ch) == unicode::no_decimal_value;
      }
    )};
    last_non_digit.begin() != str.cend()
    && (
      *last_non_digit.begin() == U'S'
      || *last_non_digit.begin() == U's'
      || *last_non_digit.begin() == U'U'
      || *last_non_digit.begin() == U'u'
    )
  ) {
    auto const suffix_length{util::wrap(str.cend() - last_non_digit.begin())
      .cast<util::size_t>()};
    if(last_non_digit.begin() + 1 == str.cend()) {
      diag::num_literal_suffix_missing_width::prefix_letter_t ch;
      switch(*last_non_digit.begin()) {
        using enum diag::num_literal_suffix_missing_width::prefix_letter_t;
        case u8's': ch = lower_s; break;
        case u8'u': ch = lower_u; break;
        case u8'S': ch = upper_s; break;
        case u8'U': ch = upper_u; break;
        default: util::unreachable();
      }
      lexer.diags_.push_back(std::make_unique<diag::num_literal_suffix_missing_width>(
        source_metric_range{{0, 0}, token_loc}, ch));
    } else {
      type_suffix = int_type_suffix{
        .width = int_width_t::uninitialized(),
        .is_signed = *last_non_digit.begin() == U'S' || *last_non_digit.begin() == U's'
      };

      type_suffix->width = 0;
      auto digit{last_non_digit.begin() + 1};
      while(digit != str.cend()) {
        type_suffix->width = type_suffix->width * 10 + unicode::decimal_value(*digit);

        if(type_suffix->width > max_int_width) {
          lexer.diags_.emplace_back(std::make_unique<diag::num_literal_suffix_too_wide>(
            source_metric_range{{0, 0}, token_loc}));
          type_suffix.reset();
          break;
        }

        ++digit;
      }
      if(type_suffix && type_suffix->width == 0) {
        lexer.diags_.emplace_back(std::make_unique<diag::num_literal_suffix_zero_width>(
          source_metric_range{{0, 0}, token_loc}));
        type_suffix.reset();
      }
    }
    str.remove_suffix(suffix_length.unwrap());
  }

  // Determine the base

  util::uint8_t base{10};

  {
    bool const has_hex_prefix{str.size() >= 2 && unicode::decimal_value(str[0]) == 0
      && (str[1] == U'X' || str[1] == U'x')};
    bool const has_hex_suffix{!str.empty() && (str.back() == U'H' || str.back() == U'h')};

    if(has_hex_prefix) {
      if(has_hex_suffix) {
        return make_error(std::make_unique<diag::num_literal_base_prefix_and_suffix>(
          source_metric_range{{0, 0}, token_loc}));
      } else {
        base = 16;
        str.remove_prefix(2);
      }
    } else if(has_hex_suffix) {
      base = 16;
      str.remove_suffix(1);
    }
  }

  if(base == 10) {
    bool const has_oct_prefix{str.size() >= 2 && unicode::decimal_value(str[0]) == 0
      && (str[1] == U'O' || str[1] == U'o')};
    bool const has_oct_suffix{!str.empty() && (str.back() == U'O' || str.back() == U'o')};

    if(has_oct_prefix) {
      if(has_oct_suffix) {
        return make_error(std::make_unique<diag::num_literal_base_prefix_and_suffix>(
          source_metric_range{{0, 0}, token_loc}));
      } else {
        base = 8;
        str.remove_prefix(2);
      }
    } else if(has_oct_suffix) {
      base = 8;
      str.remove_suffix(1);
    }
  }

  if(base == 10) {
    bool const has_bin_prefix{str.size() >= 2 && unicode::decimal_value(str[0]) == 0
      && (str[1] == U'B' || str[1] == U'b')};
    bool const has_bin_suffix{!str.empty() && (str.back() == U'B' || str.back() == U'b')};

    if(has_bin_prefix) {
      if(has_bin_suffix) {
        return make_error(std::make_unique<diag::num_literal_base_prefix_and_suffix>(
          source_metric_range{{0, 0}, token_loc}));
      } else {
        base = 2;
        str.remove_prefix(2);
      }
    } else if(has_bin_suffix) {
      base = 2;
      str.remove_suffix(1);
    }
  }

  // Extract the three parts (integral, fractional, exponential)

  auto is_digit{[&](char32_t ch) noexcept {
    if(base == 16 && (U'A' <= ch && ch <= U'F' || U'a' <= ch && ch <= U'f')) {
      return true;
    }

    util::uint8_t const value{unicode::decimal_value(ch)};
    if(value == unicode::no_decimal_value) {
      return false;
    }

    return value < base;
  }};

  std::u32string_view const integral_part{[&]() noexcept {
    auto const integral_part_size{util::wrap(
      std::ranges::find_if(
        str,
        [&](char32_t ch) noexcept { return !is_digit(ch); }
      ) - str.cbegin()).cast<util::size_t>()};
    std::u32string_view const integral_part{str.substr(0, integral_part_size.unwrap())};
    str.remove_prefix(integral_part_size.unwrap());
    return integral_part;
  }()};

  std::u32string_view const fractional_part{[&]() noexcept {
    if(str.empty() || str[0] != U'.') {
      return std::u32string_view{};
    }

    auto const fractional_part_size{util::wrap(
      std::ranges::find_if(
        str | std::views::drop(1),
        [&](char32_t ch) noexcept { return !is_digit(ch); }
      ) - str.cbegin()).cast<util::size_t>()};
    std::u32string_view const fractional_part{str.substr(0, fractional_part_size.unwrap())};
    str.remove_prefix(fractional_part_size.unwrap());
    return fractional_part;
  }()};

  std::u32string_view const exponential_part{[&]() noexcept {
    if(
      str.size() < 2
      || (base == 10 && str[0] != U'E' && str[0] != U'e')
      || (base != 10 && str[0] != U'P' && str[0] != U'p')
    ) {
      return std::u32string_view{};
    }

    auto const exp_prefix_size{str[1] == U'+' || str[1] == U'-'
      ? util::size_t{2} : util::size_t{1}};

    auto const exponential_part_size{util::wrap(
      std::ranges::find_if(
        str | std::views::drop(exp_prefix_size.unwrap()),
        [](char32_t ch) noexcept {
          return unicode::decimal_value(ch) == unicode::no_decimal_value;
        }
      ) - str.cbegin()
    ).cast<util::size_t>()};
    if(exponential_part_size == exp_prefix_size) {
      return std::u32string_view{};
    }

    std::u32string_view const exponential_part{str.substr(0, exponential_part_size.unwrap())};
    str.remove_prefix(exponential_part_size.unwrap());
    return exponential_part;
  }()};

  // Determine semantics

  if(!str.empty() || integral_part.empty() && fractional_part.empty()) {
    return make_error(std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, token_loc}));
  }

  if(!fractional_part.empty() || !exponential_part.empty()) {
    return make_error(std::make_unique<diag::fp_literals_unsupported>(
      source_metric_range{{0, 0}, token_loc}));
  }

  util::bigint num{util::bigint::from_string(integral_part
    | std::views::transform([](char32_t ch) {
      if(U'A' <= ch && ch <= U'F' || U'a' <= ch && ch <= U'f') {
        return ch;
      } else {
        return static_cast<char32_t>(U'0' + unicode::decimal_value(ch).unwrap());
      }
    }), base.unwrap()).first};

  return tok::int_literal{std::move(num), type_suffix};
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: String and character literals

bool single_lexer::state_in_string::feed(
  char32_t ch,
  source_metric loc,
  single_lexer& lexer
) {
  assert(!is_terminated);

  source_metric_range ch_loc{token_loc, loc};

  {
    std::optional<diag::bidi_code_in_string::bidi_code_t> bad_code;
    switch(ch) {
      using enum diag::bidi_code_in_string::bidi_code_t;
      case U'\u202A': bad_code = is_char ? char_lre : str_lre; break;
      case U'\u202B': bad_code = is_char ? char_rle : str_rle; break;
      case U'\u202C': bad_code = is_char ? char_pdf : str_pdf; break;
      case U'\u202D': bad_code = is_char ? char_lro : str_lro; break;
      case U'\u202E': bad_code = is_char ? char_rlo : str_rlo; break;
      case U'\u2066': bad_code = is_char ? char_lri : str_lri; break;
      case U'\u2067': bad_code = is_char ? char_rli : str_rli; break;
      case U'\u2068': bad_code = is_char ? char_fsi : str_fsi; break;
      case U'\u2069': bad_code = is_char ? char_pdi : str_pdi; break;
      default: break;
    }

    if(bad_code) {
      lexer.diags_.emplace_back(std::make_unique<diag::bidi_code_in_string>(
        ch_loc, *bad_code));
    }
  }

  // TODO: Only warn once on a CRLF in strings
  {
    switch(ch) {
    using enum diag::string_newline::newline_code_t;

    case U'\n':
      lexer.diags_.emplace_back(std::make_unique<diag::string_newline>(
        ch_loc, is_char ? char_lf : str_lf));
      break;

    case U'\v':
      lexer.diags_.emplace_back(std::make_unique<diag::string_newline>(
        ch_loc, is_char ? char_vt : str_vt));
      break;

    case U'\f':
      lexer.diags_.emplace_back(std::make_unique<diag::string_newline>(
        ch_loc, is_char ? char_ff : str_ff));
      break;

    case U'\r':
      lexer.diags_.emplace_back(std::make_unique<diag::string_newline>(
        ch_loc, is_char ? char_cr : str_cr));
      break;

    case U'\u0085':
      lexer.diags_.emplace_back(std::make_unique<diag::string_newline>(
        ch_loc, is_char ? char_nel : str_nel));
      break;

    case U'\u2028':
      lexer.diags_.emplace_back(std::make_unique<diag::string_newline>(
        ch_loc, is_char ? char_line_sep : str_line_sep));
      break;

    case U'\u2029':
      lexer.diags_.emplace_back(std::make_unique<diag::string_newline>(
        ch_loc, is_char ? char_para_sep : str_para_sep));
      break;

    default:
      break;
    }
  }

  token_loc = token_loc.add(loc);

  bool check_new_escape_code{true};
  if(escape_state == escape_state_pending) {
    check_new_escape_code = false;
    buf.push_back(ch);

    assert(!escape_codes.empty());
    auto& code_info{escape_codes.back()};
    code_info.code_loc.end = code_info.code_loc.end.add(loc);
    code_info.code.push_back(ch);

    if(ch == U'u') {
      escape_state = 4;
    } else if(ch == U'U') {
      escape_state = 6;
    } else {
      escape_state = 0;
    }
  } else if(escape_state > 0) {
    if((U'A' <= ch && ch <= U'F') || (U'a' <= ch && ch <= U'f')
        || unicode::decimal_value(ch) != unicode::no_decimal_value) {
      check_new_escape_code = false;
      buf.push_back(ch);
      --escape_state;

      assert(!escape_codes.empty());
      auto& code_info{escape_codes.back()};
      code_info.code_loc.end = code_info.code_loc.end.add(loc);
      code_info.code.push_back(ch);
    } else {
      escape_state = 0;
    }
  }

  if(check_new_escape_code) {
    if(ch == (is_char ? U'\'' : U'"')) {
      is_terminated = true;
      return false;
    } else {
      buf.push_back(ch);
      if(ch == U'\\') {
        escape_state = escape_state_pending;
        escape_codes.push_back({ch_loc, ch_loc, U""});
      }
    }
  }

  return true;
}

token single_lexer::state_in_string::get_token(single_lexer& lexer) {
  if(!is_terminated) {
    source_metric_range range{{0, 0}, token_loc};
    if(is_char) {
      lexer.diags_.emplace_back(std::make_unique<diag::unterminated_char>(range));
    } else {
      lexer.diags_.emplace_back(std::make_unique<diag::unterminated_string>(range));
    }
  }

  if(!unicode::is_nfc_qc(buf)) {
    buf = unicode::to_nfc(buf);
  }

  // Process escape codes. For each of them, we replace the code point after the backslash with
  // the code point that the escape code stands for. We also need to remember the number of
  // code points in the escape code after the backslash; we reuse the buffer in the
  // escape_code_info structure by setting it to a one-code-point string where the only code
  // point is the number in question.
  {
    std::u32string::iterator next_escape_code{buf.begin()};

    auto reject_escape_code{[&](escape_code_info const& info) {
      lexer.diags_.emplace_back(std::make_unique<diag::invalid_escape_code>(
        util::starts_with(std::ranges::subrange{next_escape_code, buf.cend()},
        info.code) ? info.code_loc : info.slash_loc));
    }};

    for(auto& info: escape_codes) {
      next_escape_code = std::ranges::find(util::unbounded_view{next_escape_code}, U'\\');
      ++next_escape_code;

      if(next_escape_code == buf.end()) {
        // This situation happens when there is an unterminated literal, and the last code
        // point of the literal -- and of the file itself -- is a backslash.
        assert(&info == &escape_codes.back());

        reject_escape_code(info);

        // Since this degenerate escape code consists only of one code point -- the
        // backslash -- the second loop below won't know how to process it. Fortunately, it
        // doesn't need to: we can just replace the backslash with U+FFFD right now, and
        // pretend that this was never an escape code.
        *(next_escape_code - 1) = U'\uFFFD';
        escape_codes.pop_back();

        break;
      }

      char32_t const escape_code_kind{*next_escape_code};
      char32_t value;
      switch(escape_code_kind) {
        case U'"':  value = U'"';  break;
        case U'\'': value = U'\''; break;
        case U'\\': value = U'\\'; break;
        case U'a':  value = U'\a'; break;
        case U'b':  value = U'\b'; break;
        case U'e':  value = U'\u001B'; break;
        case U'f':  value = U'\f'; break;
        case U'n':  value = U'\n'; break;
        case U'r':  value = U'\r'; break;
        case U't':  value = U'\t'; break;
        case U'v':  value = U'\v'; break;

        case U'u':
        case U'U': {
          auto digit_it = next_escape_code + 1;
          auto num_digits{escape_code_kind == U'u' ? util::uint8_t{4} : util::uint8_t{6}};
          util::uint32_t numeric_value{0};

          while(digit_it != buf.end()) {
            auto digit_value = util::uint32_t::uninitialized();
            if(U'A' <= *digit_it && *digit_it <= U'F') {
              digit_value = 10 + util::wrap(static_cast<std::uint32_t>(*digit_it - U'A'));
            } else if(U'a' <= *digit_it && *digit_it <= U'f') {
              digit_value = 10 + util::wrap(static_cast<std::uint32_t>(*digit_it - U'a'));
            } else {
              digit_value = unicode::decimal_value(*digit_it);
              if(digit_value == unicode::no_decimal_value) {
                break;
              }
            }

            numeric_value = (numeric_value << 4) | digit_value;
            ++digit_it;
            if(--num_digits == 0) {
              break;
            }
          }

          value = static_cast<char32_t>(numeric_value.unwrap());
          if(num_digits != 0 || !unicode::is_scalar_value(value)) {
            reject_escape_code(info);
            value = U'\uFFFD';
          }
          *next_escape_code = value;

          info.code.clear();
          info.code.push_back(static_cast<char32_t>(digit_it - next_escape_code));
          next_escape_code = digit_it;

          continue;
        }

        default:
          reject_escape_code(info);
          value = U'\uFFFD';
          break;
      }

      *next_escape_code++ = value;
      info.code.clear();
      info.code.push_back(1);
    }
  }

  // Remove the extra code points in the escape codes. Every time we find a backslash, which
  // indicates the beginning of an escape code, we replace it with the next code point, which
  // is where we've previously stored the escape code's value. Then we remove the rest of the
  // code points of the escape code.
  //
  // For example, if the original string contains the escape code `\u0041`, which corresponds
  // to the code point `A`, the loop above transformed it into `\A0041`, and the loop below
  // will transform it to just `A`.

  if(!escape_codes.empty()) {
    std::u32string::iterator dst;
    std::u32string::const_iterator src{buf.cbegin()};
    bool first{true};

    for(auto const& info: escape_codes) {
      auto const next_escape_code{std::ranges::find(util::unbounded_view{src}, U'\\')};
      if(first) {
        dst = buf.begin() + (next_escape_code - buf.cbegin());
        first = false;
      } else {
        dst = std::copy(src, next_escape_code, dst);
      }
      src = next_escape_code;

      assert(info.code.size() == 1);
      auto const escape_code_size{util::wrap(static_cast<std::uint32_t>(info.code.front()))};
      assert(1 <= escape_code_size && escape_code_size <= 7);

      ++src;
      *dst++ = *src;

      assert(util::wrap(buf.cend() - src) >= escape_code_size);
      src += escape_code_size.unwrap();
    }

    dst = std::copy(src, buf.cend(), dst);
    buf.erase(dst, buf.cend());
  }

  // Now generate the actual token.
  std::u8string value;
  std::ranges::copy(buf | unicode::utf8_encode(), std::back_inserter(value));
  value.shrink_to_fit();
  if(is_char) {
    return tok::char_literal{std::move(value)};
  } else {
    return tok::string_literal{std::move(value)};
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Punctuation

bool single_lexer::state_seen_excl::wants(char32_t ch) noexcept {
  return ch == U'=';
}

bool single_lexer::state_seen_excl::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_neq{};
  return false;
}

token single_lexer::state_seen_excl::get_token(single_lexer&) noexcept {
  return tok::excl{};
}

bool single_lexer::state_seen_percent::wants(char32_t ch) noexcept {
  return ch == U'=';
}

bool single_lexer::state_seen_percent::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_percent_assign{};
  return false;
}

token single_lexer::state_seen_percent::get_token(single_lexer&) noexcept {
  return tok::percent{};
}

bool single_lexer::state_seen_amp::wants(char32_t ch) noexcept {
  return ch == U'&' || ch == U'=';
}

bool single_lexer::state_seen_amp::feed(
  char32_t ch,
  source_metric loc,
  single_lexer& lexer
) noexcept {
  if(ch == U'&') {
    lexer.state_ = state_seen_logical_and{.midpoint = loc};
    return false;
  } else {
    lexer.state_ = state_seen_amp_assign{};
    return false;
  }
}

token single_lexer::state_seen_amp::get_token(single_lexer&) noexcept {
  return tok::amp{};
}

token single_lexer::state_seen_logical_and::get_token(single_lexer&) noexcept {
  return tok::logical_and{.midpoint = midpoint};
}

bool single_lexer::state_seen_star::wants(char32_t ch) noexcept {
  return ch == U'=';
}

bool single_lexer::state_seen_star::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_star_assign{};
  return false;
}

token single_lexer::state_seen_star::get_token(single_lexer&) noexcept {
  return tok::star{};
}

bool single_lexer::state_seen_plus::wants(char32_t ch) noexcept {
  return ch == U'+' || ch == U'=';
}

bool single_lexer::state_seen_plus::feed(
  char32_t ch,
  source_metric,
  single_lexer& lexer
) noexcept {
  if(ch == U'+') {
    lexer.state_ = state_seen_incr{};
    return false;
  } else {
    lexer.state_ = state_seen_plus_assign{};
    return false;
  }
}

token single_lexer::state_seen_plus::get_token(single_lexer&) noexcept {
  return tok::plus{};
}

bool single_lexer::state_seen_minus::wants(char32_t ch) noexcept {
  return ch == U'-' || ch == U'=' || ch == U'>';
}

bool single_lexer::state_seen_minus::feed(
  char32_t ch,
  source_metric,
  single_lexer& lexer
) noexcept {
  if(ch == U'-') {
    lexer.state_ = state_seen_decr{};
    return false;
  } else if(ch == U'=') {
    lexer.state_ = state_seen_minus_assign{};
    return false;
  } else {
    lexer.state_ = state_seen_arrow{};
    return false;
  }
}

token single_lexer::state_seen_minus::get_token(single_lexer&) noexcept {
  return tok::minus{};
}

bool single_lexer::state_seen_slash::wants(char32_t ch) noexcept {
  return ch == U'=';
}

bool single_lexer::state_seen_slash::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_slash_assign{};
  return false;
}

token single_lexer::state_seen_slash::get_token(single_lexer&) noexcept {
  return tok::slash{};
}

bool single_lexer::state_seen_lt::wants(char32_t ch) noexcept {
  return ch == U'/' || ch == U'<' || ch == U'=';
}

bool single_lexer::state_seen_lt::feed(
  char32_t ch,
  source_metric,
  single_lexer& lexer
) noexcept {
  if(ch == U'/') {
    lexer.state_ = state_seen_lrot{};
    return true;
  } else if(ch == U'<') {
    lexer.state_ = state_seen_lshift{};
    return true;
  } else {
    lexer.state_ = state_seen_leq{};
    return true;
  }
}

token single_lexer::state_seen_lt::get_token(single_lexer&) noexcept {
  return tok::lt{};
}

bool single_lexer::state_seen_lrot::wants(char32_t ch) noexcept {
  return ch == U'=';
}

bool single_lexer::state_seen_lrot::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_lrot_assign{};
  return false;
}

token single_lexer::state_seen_lrot::get_token(single_lexer&) noexcept {
  return tok::lrot{};
}

bool single_lexer::state_seen_lshift::wants(char32_t ch) noexcept {
  return ch == U'=';
}

bool single_lexer::state_seen_lshift::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_lshift_assign{};
  return false;
}

token single_lexer::state_seen_lshift::get_token(single_lexer&) noexcept {
  return tok::lshift{};
}

bool single_lexer::state_seen_leq::wants(char32_t ch) noexcept {
  return ch == U'>';
}

bool single_lexer::state_seen_leq::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_three_way_cmp{};
  return false;
}

token single_lexer::state_seen_leq::get_token(single_lexer&) noexcept {
  return tok::leq{};
}

bool single_lexer::state_seen_assign::wants(char32_t ch) noexcept {
  return ch == U'=' || ch == U'>';
}

bool single_lexer::state_seen_assign::feed(
  char32_t ch,
  source_metric,
  single_lexer& lexer
) noexcept {
  if(ch == U'=') {
    lexer.state_ = state_seen_eq{};
    return false;
  } else {
    lexer.state_ = state_seen_fat_arrow{};
    return false;
  }
}

token single_lexer::state_seen_assign::get_token(single_lexer&) noexcept {
  return tok::assign{};
}

bool single_lexer::state_seen_gt::wants(char32_t ch) noexcept {
  return ch == U'/' || ch == U'=' || ch == U'>';
}

bool single_lexer::state_seen_gt::feed(
  char32_t ch,
  source_metric,
  single_lexer& lexer
) noexcept {
  if(ch == U'/') {
    lexer.state_ = state_seen_rrot{};
    return true;
  } else if(ch == U'=') {
    lexer.state_ = state_seen_geq{};
    return false;
  } else {
    lexer.state_ = state_seen_rshift{};
    return true;
  }
}

token single_lexer::state_seen_gt::get_token(single_lexer&) noexcept {
  return tok::gt{};
}

bool single_lexer::state_seen_rrot::wants(char32_t ch) noexcept {
  return ch == U'=';
}

bool single_lexer::state_seen_rrot::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_rrot_assign{};
  return false;
}

token single_lexer::state_seen_rrot::get_token(single_lexer&) noexcept {
  return tok::rrot{};
}

bool single_lexer::state_seen_rshift::wants(char32_t ch) noexcept {
  return ch == U'=';
}

bool single_lexer::state_seen_rshift::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_rshift_assign{};
  return false;
}

token single_lexer::state_seen_rshift::get_token(
  single_lexer&
) noexcept {
  return tok::rshift{};
}

bool single_lexer::state_seen_bitwise_xor::wants(char32_t ch) noexcept {
  return ch == U'=' || ch == U'^';
}

bool single_lexer::state_seen_bitwise_xor::feed(
  char32_t ch,
  source_metric,
  single_lexer& lexer
) noexcept {
  if(ch == U'=') {
    lexer.state_ = state_seen_bitwise_xor_assign{};
    return false;
  } else {
    lexer.state_ = state_seen_logical_xor{};
    return false;
  }
}

token single_lexer::state_seen_bitwise_xor::get_token(single_lexer&) noexcept {
  return tok::bitwise_xor{};
}

bool single_lexer::state_seen_pipe::wants(char32_t ch) noexcept {
  return ch == U'=' || ch == U'>' || ch == U'|';
}

bool single_lexer::state_seen_pipe::feed(
  char32_t ch,
  source_metric,
  single_lexer& lexer
) noexcept {
  if(ch == U'=') {
    lexer.state_ = state_seen_pipe_assign{};
    return false;
  } else if(ch == U'>') {
    lexer.state_ = state_seen_subst{};
    return false;
  } else {
    lexer.state_ = state_seen_logical_or{};
    return false;
  }
}

token single_lexer::state_seen_pipe::get_token(single_lexer&) noexcept {
  return tok::pipe{};
}

bool single_lexer::state_seen_point::wants(char32_t ch) noexcept {
  return ch == U'.' || unicode::decimal_value(ch) != unicode::no_decimal_value;
}

bool single_lexer::state_seen_point::feed(
  char32_t ch,
  source_metric loc,
  single_lexer& lexer
) const {
  if(ch == U'.') {
    lexer.state_ = state_seen_double_point{};
    return true;
  } else {
    lexer.state_ = state_in_num{{U'.', ch}, token_loc.add(loc), state_in_num::substate_t::dec};
    return true;
  }
}

token single_lexer::state_seen_point::get_token(single_lexer&) noexcept {
  return tok::point{};
}

bool single_lexer::state_seen_double_point::wants(char32_t ch) noexcept {
  return ch == U'.';
}

bool single_lexer::state_seen_double_point::feed(
  char32_t,
  source_metric,
  single_lexer& lexer
) noexcept {
  lexer.state_ = state_seen_ellipsis{};
  return false;
}

token single_lexer::state_seen_double_point::get_token(single_lexer&) noexcept {
  return tok::double_point{};
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Incremental lexing

incremental_token_source::incremental_token_source(
  source_file& file,
  std::span<source_file_delta const> deltas
) noexcept:
  file_{file},
  deltas_{deltas},
  old_metric_{0, 0},
  cur_node_metric_{0, 0},
  cur_node_{src_pos_t{0}}, // GCC bug 114311
  cur_node_offset_{src_pos_t{0}},
  cur_token_boundary_{src_pos_t{0}},
  reuse_remains_{0, 0},
  cur_delta_remains_{src_pos_t{0}},
  cur_delta_{deltas_.begin()},
  cur_diags_{&file_.diags_},
  spare_diags_{}
{
  #ifndef NDEBUG
    file_.validate();

    source_metric cur_pos{0, 0};
    auto cur_node{file_.nodes_.cbegin()};
    source_metric cur_node_start{0, 0};
    source_metric cur_node_end{cur_node == file_.nodes_.cend()
      ? cur_node_start : cur_node->metric};

    auto validate_boundary{[&](source_metric boundary) noexcept {
      // Move to the next node until we reach the one containing the boundary
      while(boundary >= cur_node_end) {
        // Special case for a delta boundary at the end of a file
        if(cur_node == file_.nodes_.cend() && boundary == cur_node_end) {
          // If the boundary is at the end of the file, it cannot possibly be on a code point
          // boundary, so the subsequent check is unnecessary.
          return;
        }

        ++cur_node;
        cur_node_start = cur_node_end;
        if(cur_node != file_.nodes_.cend()) {
          cur_node_end = cur_node_start.add(cur_node->metric);
        }
      }

      // Compute boundary coordinates
      auto line_start{cur_node->data.cbegin()};
      if(boundary.line != cur_node_start.line) {
        src_pos_t counted_lines{0};
        do {
          auto const ch{unicode::utf8<>::decode(line_start,
            util::unreachable_sentinel<decltype(line_start)>{}, unicode::eh_assume_valid<>{})};
          // Note that we won't ever reach the end of the node's buffer, or else the above loop
          // would have picked a later node
          if(is_line_terminator(ch, file_.mode_) || ch == U'\r' && *line_start != u8'\n') {
            ++counted_lines;
          }
        } while(counted_lines < boundary.line - cur_node_start.line);
      }

      src_pos_t col{
        boundary.line == cur_node_start.line
        ? boundary.col - cur_node_start.col
        : boundary.col
      };

      // Check that the boundary is in range of the line
      if(boundary.line != cur_node_end.line) {
        // If this is the last line of the node, we're definitely in range (otherwise the above
        // loop would have picked the next node). So we only need to check if this is not the
        // last line.

        auto line_end{line_start};
        for(;;) {
          auto const line_end_old{line_end};
          auto const ch{unicode::utf8<>::decode(line_end,
            util::unreachable_sentinel<decltype(line_end)>{}, unicode::eh_assume_valid<>{})};
          if(is_line_terminator(ch, file_.mode_) || ch == U'\r' && *line_end != u8'\n') {
            line_end = line_end_old;
            break;
          }
        }

        assert(col <= num_columns_in_range(std::span{line_start, line_end}, file_.mode_));
      }

      // Check that the boundary is not in the middle of a code point
      src_pos_t counted_cols{0};
      while(counted_cols < col) {
        auto const ch{unicode::utf8<>::decode(line_start,
          util::unreachable_sentinel<decltype(line_start)>{}, unicode::eh_assume_valid<>{})};
        counted_cols += num_columns(ch, file_.mode_);
      }
      assert(counted_cols == col);
    }};

    for(auto const& delta: deltas) {
      assert(unicode::is_valid_utf8(delta.replacement));
      assert(cur_pos <= delta.old_start);
      validate_boundary(delta.old_start);
      cur_pos = delta.old_start.add(delta.old_metric);
      validate_boundary(cur_pos);
    }
  #endif
}

bool incremental_token_source::is_at_end() const noexcept {
  return cur_node_ == util::wrap(file_.nodes_.size());
}

incremental_token_source::advance_by_metric_result
incremental_token_source::advance_by_metric(source_metric metric) const noexcept {
  advance_by_metric_result result{
    .node_index = src_pos_t::uninitialized(),
    .node_offset = src_pos_t::uninitialized(),
    .node_metric = source_metric::uninitialized(),
    .token_boundary = src_pos_t::uninitialized()
  };

  source_metric remaining_metric{cur_node_metric_.add(metric)};
  result.node_index = cur_node_;

  // Skip nodes until we find the one we're looking for
  while(remaining_metric >= file_.nodes_[result.node_index.unwrap()].metric) {
    remaining_metric = remaining_metric.from(file_.nodes_[result.node_index.unwrap()].metric);
    ++result.node_index;

    if(result.node_index == util::wrap(file_.nodes_.size())) {
      assert((remaining_metric == source_metric{0, 0}));
      return {
        .node_index = result.node_index,
        .node_offset = 0,
        .node_metric = source_metric{0, 0},
        .token_boundary = 0,
      };
    }
  }

  auto const& node{file_.nodes_[result.node_index.unwrap()]};

  result.node_metric = remaining_metric;

  // Scan the node until we find the destination
  auto data_it{node.data.cbegin()};
  if(result.node_index == cur_node_) {
    // If the destination is in the current node, we only need to scan from the current
    // position, not from the start.
    remaining_metric = metric;
    data_it += cur_node_offset_.unwrap();
  }
  while(remaining_metric != source_metric{0, 0}) {
    auto const ch{unicode::utf8<>::decode(data_it,
      util::unreachable_sentinel<decltype(data_it)>{}, unicode::eh_assume_valid<>{})};

    // Note that we won't ever reach the end of the node's buffer, or else the above loop
    // would have picked a later node
    source_metric loc{source_metric::uninitialized()};
    if(is_line_terminator(ch, file_.mode_) || ch == U'\r' && *data_it != u8'\n') {
      loc = source_metric{1, 0};
    } else {
      loc = source_metric{0, num_columns(ch, file_.mode_)};
    }

    remaining_metric = remaining_metric.from(loc);
  }

  result.node_offset = util::wrap(data_it - node.data.cbegin()).cast<src_pos_t>();

  result.token_boundary = util::wrap(
    std::ranges::lower_bound(
      result.node_index == cur_node_
        ? node.tokens.cbegin() + cur_token_boundary_.unwrap()
        : node.tokens.cbegin(),
      node.tokens.cend(),
      result.node_offset,
      std::ranges::less{},
      &source_file::token_boundary::offset
    ) - node.tokens.cbegin()
  ).cast<src_pos_t>();

  return result;
}

char32_t incremental_token_source::get_char() const noexcept {
  assert(!is_at_end());
  auto it{file_.nodes_[cur_node_.unwrap()].data.cbegin() + cur_node_offset_.unwrap()};
  return unicode::utf8<>::decode(it, util::unreachable_sentinel<decltype(it)>{},
    unicode::eh_assume_valid<>{});
}

source_metric incremental_token_source::finish_delta() noexcept {
  assert(cur_delta_ != deltas_.end());
  assert(cur_delta_remains_ == 0);

  auto const delta_metric{cur_delta_->old_metric};
  old_metric_ = old_metric_.add(delta_metric);

  ++cur_delta_;

  return delta_metric;
}

source_metric incremental_token_source::try_start_delta() {
  // Accumulate the offsets of all the empty-replacement deltas we skipped
  source_metric skipped_deltas_metric{0, 0};

  // Keep going while we're skipping empty-replacement deltas
  while(cur_delta_ != deltas_.end() && old_metric_ == cur_delta_->old_start) {
    // Remove skipped diagnostic nodes
    {
      source_metric const diags_cutoff{old_metric_.add(cur_delta_->old_metric)};
      while(*cur_diags_ && (*cur_diags_)->token_loc < diags_cutoff) {
        *cur_diags_ = std::move((*cur_diags_)->next);
      }
    }

    // If the last code point we saw was a carriage return, remember this fact, as well as
    // whether it was followed by a line feed. We will use this information later to adjust
    // the current node metric as necessary.
    bool cr_followed_by_lf{false};
    bool cr_not_followed_by_lf{false};
    if(
      cur_node_offset_ == 0
      ? cur_node_ != 0 && file_.nodes_[(cur_node_ - 1).unwrap()].data.back() == u8'\r'
      : file_.nodes_[cur_node_.unwrap()].data[(cur_node_offset_ - 1).unwrap()] == u8'\r'
    ) {
      if(
        cur_node_ != util::wrap(file_.nodes_.size())
        && file_.nodes_[cur_node_.unwrap()].data[cur_node_offset_.unwrap()] == u8'\n'
      ) {
        cr_followed_by_lf = true;
      } else {
        cr_not_followed_by_lf = true;
      }
    }

    auto const advance_result{
      cur_delta_->old_metric == source_metric{0, 0}
      ? advance_by_metric_result{
        .node_index = cur_node_,
        .node_offset = cur_node_offset_,
        .node_metric = cur_node_metric_,
        .token_boundary = cur_token_boundary_
      }
      : this->advance_by_metric(cur_delta_->old_metric)
    };

    std::u8string prefix_text;
    std::vector<source_file::token_boundary> prefix_tokens;

    // We record whether the prefix starts inside a token to set the corresponding flag on the
    // first of the new nodes.
    //
    // If there is no prefix, then we are on a node boundary. Then, after the delta is applied,
    // we will still be on a node boundary, at the start of the first new node. Therefore, in
    // this situation, the starts_in_token flag of the first new node has to be based on the
    // new token sequence, not the old one. And the new token sequence necessarily starts
    // outside a token, since we always remove token boundaries in pairs. Therefore, this flag
    // should be set to false.
    bool prefix_starts_in_token{false};

    if(cur_node_offset_ != 0) {
      auto const& cur_node{file_.nodes_[cur_node_.unwrap()]};
      prefix_text = std::span{cur_node.data}.subspan(0, cur_node_offset_.unwrap())
        | util::range_to<std::u8string>();
      prefix_tokens = cur_node.tokens | std::views::take(cur_token_boundary_.unwrap())
        | util::range_to<std::vector>();
      prefix_starts_in_token = cur_node.starts_in_token;
    }

    std::u8string suffix_text;
    std::vector<source_file::token_boundary> suffix_tokens;
    if(advance_result.node_offset != 0) {
      auto const& final_node{file_.nodes_[advance_result.node_index.unwrap()]};
      assert(advance_result.node_offset != util::wrap(final_node.data.size()));
      suffix_text = std::span{final_node.data}.subspan(advance_result.node_offset.unwrap())
        | util::range_to<std::u8string>();

      auto suffix_token_it{final_node.tokens.cbegin()
        + advance_result.token_boundary.unwrap()};
      if(suffix_token_it != final_node.tokens.cend()) {
        // If the boundary after the end position is a token end, it needs to be removed.
        bool const is_token_end{
          cur_node_ == advance_result.node_index
          ? (advance_result.token_boundary - cur_token_boundary_) % 2 != 0
          : (advance_result.token_boundary % 2 != 0) != final_node.starts_in_token
        };
        if(is_token_end) {
          ++suffix_token_it;
        }
      }

      suffix_tokens = std::ranges::subrange{suffix_token_it, final_node.tokens.cend()}
        | std::views::transform([&](source_file::token_boundary tb) noexcept {
          return source_file::token_boundary{
            .offset = tb.offset - advance_result.node_offset,
            .is_lookahead = tb.is_lookahead
          };
        })
        | util::range_to<std::vector>();
    }

    src_pos_t num_old_nodes{advance_result.node_index - cur_node_
      + (advance_result.node_offset == 0 ? src_pos_t{0} : src_pos_t{1})};

    auto new_text_size{
      util::wrap(prefix_text.size()).cast<src_pos_t>() +
      util::wrap(cur_delta_->replacement.size()).cast<src_pos_t>() +
      util::wrap(suffix_text.size()).cast<src_pos_t>()
    };

    // This lambda computes the given node's metric and fills it into the node. If the node
    // ends in a carriage return, the carriage return is not considered a line terminator. If
    // it should be a line terminator, its metric needs to be adjusted by the lambda's caller.
    auto const compute_node_metric{[&](source_file::node& node) noexcept {
      node.metric = source_metric{0, 0};
      auto it{node.data.cbegin()};
      while(it != node.data.cend()) {
        char32_t const ch{unicode::utf8<>::decode(it,
          util::unreachable_sentinel<decltype(it)>{}, unicode::eh_assume_valid<>{})};
        if(
          is_line_terminator(ch, file_.mode_)
          || ch == U'\r' && (it == node.data.cend() || *it != u8'\n')
        ) {
          node.metric = node.metric.add(source_metric{1, 0});
        } else {
          node.metric = node.metric.add(source_metric{0, num_columns(ch, file_.mode_)});
        }
      }
    }};

    // This lambda adjusts the metric of the node preceding the current one (the one indexed by
    // `cur_node_`) to properly account for CRLFs on node boundaries. That is to say, if the
    // previous node ends in a carriage return:
    // - If it used to be part of a CRLF pair, but no longer is, the node's metric is adjusted
    //   to consider it a line terminator.
    // - If it used to not be part of a CRLF pair, but now is, the node's metric is adjusted to
    //   not consider it a line terminator.
    // This lambda must be called before any changes to the node array are made. The parameter
    // is used to indicate whether the previous node is followed by a line feed (i. e. whether
    // the new nodes' text starts with a line feed or, if the new nodes' text is empty, whether
    // the text after the old nodes starts with a line feed).
    auto const adjust_prev_node_crlf{[&](bool is_followed_by_lf) {
      if(cur_node_ != 0 && file_.nodes_[(cur_node_ - 1).unwrap()].data.back() == u8'\r') {
        auto& prev_node{file_.nodes_[(cur_node_ - 1).unwrap()]};
        bool const was_followed_by_lf{cur_node_ != util::wrap(file_.nodes_.size())
          && file_.nodes_[cur_node_.unwrap()].data.front() == u8'\n'};
        if(was_followed_by_lf && !is_followed_by_lf) {
          ++prev_node.metric.line;
          prev_node.metric.col = 0;
        } else if(!was_followed_by_lf && is_followed_by_lf) {
          assert(prev_node.metric.line != 0);
          assert(prev_node.metric.col == 0);

          --prev_node.metric.line;

          auto it{prev_node.data.cend() - 1};
          while(it != prev_node.data.cbegin()) {
            unicode::utf8<>::decrement(it);
            auto it_copy{it};
            char32_t const ch{unicode::utf8<>::decode(it_copy,
              util::unreachable_sentinel<decltype(it_copy)>{}, unicode::eh_assume_valid<>{})};
            if(is_line_terminator(ch, file_.mode_) || ch == U'\r') {
              break;
            }
            prev_node.metric.col += num_columns(ch, file_.mode_);
          }
        }
      }
    }};

    if(new_text_size == 0) {
      adjust_prev_node_crlf(
        cur_node_ + num_old_nodes != util::wrap(file_.nodes_.size())
        && file_.nodes_[(cur_node_ + num_old_nodes).unwrap()].data.front() == u8'\n');
      file_.nodes_.erase(file_.nodes_.cbegin() + cur_node_.unwrap(),
        file_.nodes_.cbegin() + (cur_node_ + num_old_nodes).unwrap());
    } else {
      if(new_text_size < source_file::min_node_size) {
        // The text is smaller than the minimum node size. We need to combine it with one of
        // the adjacent nodes and then process it as usual.

        if(cur_node_ + num_old_nodes < util::wrap(file_.nodes_.size())) {
          // We can merge with the next node.
          auto const& next_node{file_.nodes_[(cur_node_ + num_old_nodes).unwrap()]};

          auto suffix_token_it{next_node.tokens.cbegin()};
          // If the suffix so far has no tokens, and the added node starts in a token, then the
          // first boundary of the added node is a token end, so we need to remove it.
          if(suffix_tokens.empty() && next_node.starts_in_token && !next_node.tokens.empty()) {
            ++suffix_token_it;
          }
          std::ranges::copy(
            std::ranges::subrange{suffix_token_it, next_node.tokens.cend()}
            | std::views::transform([&](source_file::token_boundary tb) noexcept {
              return source_file::token_boundary{
                .offset = tb.offset + util::wrap(suffix_text.size()).cast<src_pos_t>(),
                .is_lookahead = tb.is_lookahead
              };
            }),
            std::back_inserter(suffix_tokens)
          );

          auto const old_suffix_text_size{util::wrap(suffix_text.size())};
          suffix_text.resize((old_suffix_text_size + util::wrap(next_node.data.size()))
            .unwrap());
          std::memcpy(suffix_text.data() + old_suffix_text_size.unwrap(),
            next_node.data.data(), next_node.data.size());

          new_text_size += util::wrap(next_node.data.size()).cast<src_pos_t>();
          ++num_old_nodes;
        }

        else if(cur_node_ > 0) {
          // We can merge with the preceding node.
          //
          // We adjust the data members tracking the current position so that they are now
          // relative the preceding node. After the text is rearranged into new nodes, these
          // members will be adjusted later as necessary.

          --cur_node_;
          auto const& prev_node{file_.nodes_[cur_node_.unwrap()]};
          cur_node_offset_ += util::wrap(prev_node.data.size()).cast<src_pos_t>();
          cur_node_metric_ = cur_node_metric_.add(prev_node.metric);
          cur_token_boundary_ += util::wrap(prev_node.tokens.size()).cast<src_pos_t>();

          std::u8string new_prefix_text;
          new_prefix_text.resize((util::wrap(prev_node.data.size())
            + util::wrap(prefix_text.size())).unwrap());
          std::memcpy(new_prefix_text.data(), prev_node.data.data(), prev_node.data.size());
          std::memcpy(new_prefix_text.data() + prev_node.data.size(), prefix_text.data(),
            prefix_text.size());
          prefix_text = std::move(new_prefix_text);

          for(auto& tb: prefix_tokens) {
            tb.offset += util::wrap(prev_node.data.size()).cast<src_pos_t>();
          }
          prefix_tokens.insert(prefix_tokens.cbegin(),
            prev_node.tokens.cbegin(), prev_node.tokens.cend());

          new_text_size += util::wrap(prev_node.data.size()).cast<src_pos_t>();
          ++num_old_nodes;
        }

        // Otherwise, this will be the only node, so we're allowed to violate the minimum node
        // size limit.

        // After this operation, the new text is guaranteed to fit into at most two nodes.
        assert(new_text_size <= source_file::max_node_size * 2 - 3);
      }

      src_pos_t num_new_nodes{src_pos_t::uninitialized()};

      if(new_text_size <= source_file::max_node_size) {
        // The text fits in one node.
        num_new_nodes = 1;

        source_file::node new_node{
          .metric = source_metric::uninitialized(),
          .data = util::concat(prefix_text, cur_delta_->replacement, suffix_text)
            | util::range_to<decltype(new_node.data)>(),
          .tokens = {},
          .starts_in_token = false
        };

        adjust_prev_node_crlf(new_node.data.front() == u8'\n');

        compute_node_metric(new_node);
        if(
          new_node.data.back() == u8'\r'
          && (cur_node_ + num_old_nodes == util::wrap(file_.nodes_.size())
          || file_.nodes_[(cur_node_ + num_old_nodes).unwrap()].data.front() != u8'\n')
        ) {
          ++new_node.metric.line;
          new_node.metric.col = 0;
        }

        if(num_old_nodes == 0) {
          file_.nodes_.insert(file_.nodes_.cbegin() + cur_node_.unwrap(), std::move(new_node));
        } else {
          file_.nodes_[cur_node_.unwrap()] = std::move(new_node);
          file_.nodes_.erase(file_.nodes_.cbegin() + (cur_node_ + 1).unwrap(),
            file_.nodes_.cbegin() + (cur_node_ + num_old_nodes).unwrap());
        }
      }

      else if(new_text_size <= source_file::max_node_size * 2 - 3) {
        // The new text fits in two nodes.
        //
        // Note that we're subtracting 3 in the comparison above. This is because, if the new
        // text is too close to the maximum that can be stored in two nodes, there are fewer
        // than 4 different positions where the text can be split into two halves. Therefore,
        // we may not have a split point that is on a code point boundary.
        num_new_nodes = 2;

        std::u8string new_nodes_data{
          util::concat(prefix_text, cur_delta_->replacement, suffix_text)
          | util::range_to<std::u8string>()};

        auto const half{(util::wrap(new_nodes_data.size()) / 2).cast<src_pos_t>()};
        assert(half > 1);
        assert(half + 2 < util::wrap(new_nodes_data.size()));
        auto const split{
          !unicode::is_utf8_continuation(new_nodes_data[half.unwrap()])
          ? half
          : !unicode::is_utf8_continuation(new_nodes_data[(half + 1).unwrap()])
          ? half + 1
          : !unicode::is_utf8_continuation(new_nodes_data[(half - 1).unwrap()])
          ? half - 1
          : half + 2
        };
        assert(!unicode::is_utf8_continuation(new_nodes_data[split.unwrap()]));

        source_file::node new_nodes[2]{
          source_file::node{
            .metric = source_metric::uninitialized(),
            .data = new_nodes_data.substr(0, split.unwrap())
              | util::range_to<decltype(new_nodes[0].data)>(),
            .tokens = {},
            .starts_in_token = false
          },
          source_file::node{
            .metric = source_metric::uninitialized(),
            .data = new_nodes_data.substr(split.unwrap())
              | util::range_to<decltype(new_nodes[0].data)>(),
            .tokens = {},
            .starts_in_token = false
          }
        };

        adjust_prev_node_crlf(new_nodes[0].data.front() == u8'\n');

        compute_node_metric(new_nodes[0]);
        if(new_nodes[0].data.back() == u8'\r' && new_nodes[1].data.front() != u8'\n') {
          ++new_nodes[0].metric.line;
          new_nodes[0].metric.col = 0;
        }

        compute_node_metric(new_nodes[1]);
        if(
          new_nodes[1].data.back() == u8'\r'
          && (cur_node_ + num_old_nodes == util::wrap(file_.nodes_.size())
          || file_.nodes_[(cur_node_ + num_old_nodes).unwrap()].data.front() != u8'\n')
        ) {
          ++new_nodes[1].metric.line;
          new_nodes[1].metric.col = 0;
        }

        if(num_old_nodes == 0) {
          file_.nodes_.insert(file_.nodes_.cbegin() + cur_node_.unwrap(),
            std::move_iterator{std::begin(new_nodes)}, std::move_iterator{std::end(new_nodes)});
        } else if(num_old_nodes == 1) {
          file_.nodes_[cur_node_.unwrap()] = std::move(new_nodes[0]);
          file_.nodes_.insert(file_.nodes_.cbegin() + (cur_node_ + 1).unwrap(),
            std::move(new_nodes[1]));
        } else {
          file_.nodes_[cur_node_.unwrap()] = std::move(new_nodes[0]);
          file_.nodes_[(cur_node_ + 1).unwrap()] = std::move(new_nodes[1]);
          file_.nodes_.erase(file_.nodes_.cbegin() + (cur_node_ + 2).unwrap(),
            file_.nodes_.cbegin() + (cur_node_ + num_old_nodes).unwrap());
        }
      }

      else {
        // The new text requires three or more nodes.

        assert(!cur_delta_->replacement.empty());
        adjust_prev_node_crlf(prefix_text.empty()
          ? cur_delta_->replacement.front() == u8'\n'
          : prefix_text.front() == u8'\n');

        // If a split point at the end of a particular node is in the middle of a code point,
        // we add up to 3 bytes to that node until we reach a code point boundary. Therefore,
        // we want to leave 3 bytes of room in each node that would be filled with those bytes.
        // This is why we divide by the maximum node size reduced by 3.
        //
        // However, the last node does not end in a split point, so it is guaranteed to end on
        // a code point boundary and will not need that extra room. Therefore, in the
        // calculation below, we subtract 3 from the new text size, distribute the remaining
        // text size among the nodes, then later add those 3 bytes back to the last node.

        num_new_nodes = util::div_ceil<src_pos_t>(new_text_size - 3,
          source_file::max_node_size - 3);

        if(num_new_nodes > num_old_nodes) {
          file_.nodes_.insert(
            file_.nodes_.cbegin() + (cur_node_ + num_old_nodes).unwrap(),
            (num_new_nodes - num_old_nodes).unwrap(),
            source_file::node{
              .metric = source_metric::uninitialized(),
              .data = {},
              .tokens = {},
              .starts_in_token = {}
            }
          );
        }

        std::u8string_view const parts[]{prefix_text, cur_delta_->replacement, suffix_text};
        std::u8string_view const* cur_part{parts};
        src_pos_t cur_part_offset{0};

        // This is the base size of each node, before it is adjusted to account for code point
        // boundaries, the division remainder, and re-adding the 3 bytes at the end of the last
        // node.
        auto const bytes_per_node{((new_text_size - 3) / num_new_nodes).cast<src_pos_t>()};

        // A node can lose up to 3 bytes to the previous node due to moving a split point to a
        // code point boundary. Even after it loses those 3 bytes, it should not violate the
        // minimum node size limit.
        assert(bytes_per_node - 3 >= source_file::min_node_size);

        // The new text size may not be exactly divisible by the number of nodes. This leaves
        // as a remainder several bytes that need to be accounted for. So we increase the size
        // of the first few nodes by 1 byte until we've distributed the entire remainder. This
        // produces the "big" nodes, whose size is 1 greater than the size of other nodes
        // (before split point adjustment).
        auto const big_nodes{((new_text_size - 3) % num_new_nodes).cast<src_pos_t>()};

        // The number of bytes we added to the last node to get the split point to a code point
        // boundary. We need to subtract this number of bytes from the next node's size.
        src_pos_t last_node_excess{0};

        for(
          auto [index_raw, node]:
          std::span{file_.nodes_.begin() + cur_node_.unwrap(), num_new_nodes.unwrap()}
            | util::enumerate
        ) {
          auto const index{util::wrap(index_raw)};
          assert(index < num_new_nodes);

          // The amount of bytes we need to put into this node.
          src_pos_t amount{bytes_per_node - last_node_excess};
          if(index < big_nodes) {
            // If this is supposed to be a "big" node, add 1 byte. This distributes the
            // remainder from dividing the new text size by the number of new nodes.
            ++amount;
          }
          assert(source_file::min_node_size <= amount);
          assert(amount <= source_file::max_node_size - 3);
          if(index == num_new_nodes - 1) {
            // If this is the last node, re-add the three bytes that we subtracted at the
            // start.
            amount += 3;
          }

          node.data.clear();
          do {
            assert(cur_part != std::end(parts));
            src_pos_t const transfer_amount{std::min(amount,
              (util::wrap(cur_part->size()) - cur_part_offset).cast<src_pos_t>())};
            std::ranges::copy(cur_part->substr(cur_part_offset.unwrap(),
              transfer_amount.unwrap()), std::back_inserter(node.data));
            amount -= transfer_amount;
            cur_part_offset += transfer_amount;
            if(cur_part_offset == util::wrap(cur_part->size())) {
              do {
                ++cur_part;
                cur_part_offset = 0;
              } while(cur_part != std::end(parts)
                && cur_part_offset == util::wrap(cur_part->size()));
            }
          } while(amount != 0);

          // Note that the boundary between the three parts always falls on a code point
          // boundary. Therefore, all the extra bytes we copy here will always come from the
          // current part.
          last_node_excess = 0;
          if(cur_part != std::end(parts)) {
            while(unicode::is_utf8_continuation((*cur_part)[cur_part_offset.unwrap()])) {
              node.data.push_back((*cur_part)[(cur_part_offset++).unwrap()]);
              ++last_node_excess;
              if(cur_part_offset == util::wrap(cur_part->size())) {
                do {
                  ++cur_part;
                  cur_part_offset = 0;
                } while(cur_part != std::end(parts)
                  && cur_part_offset == util::wrap(cur_part->size()));
                break;
              }
            }
          }

          compute_node_metric(node);
          if(index != 0) {
            auto& prev_node{file_.nodes_[(cur_node_ + index - 1).unwrap()]};
            if(prev_node.data.back() == u8'\r' && node.data.front() != u8'\n') {
              ++prev_node.metric.line;
              prev_node.metric.col = 0;
            }
          }

          node.tokens.clear();
          node.starts_in_token = false;
        }

        {
          auto& last_node{file_.nodes_[(cur_node_ + num_new_nodes - 1).unwrap()]};
          if(
            last_node.data.back() == u8'\r'
            && (cur_node_ + num_old_nodes == util::wrap(file_.nodes_.size())
            || file_.nodes_[(cur_node_ + num_old_nodes).unwrap()].data.front() != u8'\n')
          ) {
            ++last_node.metric.line;
            last_node.metric.col = 0;
          }
        }

        assert(last_node_excess == 0);
        assert(cur_part == std::end(parts));

        if(num_new_nodes < num_old_nodes) {
          file_.nodes_.erase(
            file_.nodes_.cbegin() + (cur_node_ + num_new_nodes).unwrap(),
            file_.nodes_.cbegin() + (cur_node_ + num_old_nodes).unwrap()
          );
        }
      }

      // The prefix text fits in at most two new nodes; here is why. Note that we only need to
      // consider the third of the three cases above: where the full new text does not fit in
      // one or two nodes. In particular, if the new text initially is below the minimum node
      // size and we merge it with one of the adjacent nodes, the resulting new text still fits
      // entirely into at most two nodes, so we don't need to consider that case here either.
      // Let N be the `node_size_base` value; then the minimum node size is N - 1; the maximum
      // node size is 2N; and the maximum prefix size is 2N - 1.
      //
      // The algorithm is designed to split the text into nodes so that, before the split point
      // adjustment to get it to a code point boundary, each node is at least 3 bytes greater
      // than the minimum size, or N + 2, so the first two nodes combined will be of size at
      // least 2N + 4. The split point adjustment will not remove any bytes from the first
      // node, and any bytes removed from the second node will be added to the first one, so
      // it will not decrease their total size. Since 2N + 4 > 2N - 1, the prefix will fit into
      // two nodes.
      //
      // Likewise, the suffix text fits in at most two new nodes. The proof is analogous,
      // except that split point adjustment can remove bytes from the first of the two last
      // nodes; this will decrease its size by at most 3 bytes, so the last two nodes combined
      // will be of size at least 2N + 1, which is still greater than 2N - 1.

      // Copy suffix token boundaries
      if(!suffix_tokens.empty()) {
        auto& last_node{file_.nodes_[(cur_node_ + num_new_nodes - 1).unwrap()]};
        if(suffix_text.size() > last_node.data.size()) {
          // The suffix has been split into two nodes
          auto const suffix_split{util::wrap(suffix_text.size())
            - util::wrap(last_node.data.size())};
          auto& pre_last_node{file_.nodes_[(cur_node_ + num_new_nodes - 2).unwrap()]};
          auto const suffix_offset{util::wrap(pre_last_node.data.size())
            - suffix_split};

          auto token_split{std::ranges::lower_bound(suffix_tokens, suffix_split, {},
            &source_file::token_boundary::offset)};
          if(
            token_split != suffix_tokens.cend()
            && token_split->offset == suffix_split
            && (token_split - suffix_tokens.cbegin()) % 2 != 0
          ) {
            // If the token boundary we found is a token end that is exactly at a node
            // boundary, it belongs in the first node.
            ++token_split;
          }

          pre_last_node.tokens = std::ranges::subrange{suffix_tokens.cbegin(), token_split}
            | std::views::transform([&](source_file::token_boundary tb) noexcept {
              return source_file::token_boundary{
                .offset = (tb.offset + suffix_offset).cast<src_pos_t>(),
                .is_lookahead = tb.is_lookahead
              };
            })
            | util::range_to<std::vector>();

          last_node.tokens = std::ranges::subrange{token_split, suffix_tokens.cend()}
            | std::views::transform([&](source_file::token_boundary tb) noexcept {
              return source_file::token_boundary{
                .offset = (tb.offset - suffix_split).cast<src_pos_t>(),
                .is_lookahead = tb.is_lookahead
              };
            })
            | util::range_to<std::vector>();
          last_node.starts_in_token = (token_split - suffix_tokens.cbegin()) % 2 != 0;
        } else {
          // The suffix fits into one node
          auto const suffix_offset{util::wrap(last_node.data.size())
            - util::wrap(suffix_text.size())};
          last_node.tokens = suffix_tokens
            | std::views::transform([&](source_file::token_boundary tb) noexcept {
              return source_file::token_boundary{
                .offset = (tb.offset + suffix_offset).cast<src_pos_t>(),
                .is_lookahead = tb.is_lookahead
              };
            })
            | util::range_to<std::vector>();
        }
      }

      // There are two things we need to do if the prefix has been split into two nodes.
      if(
        cur_node_ != util::wrap(file_.nodes_.size())
        && cur_node_offset_ >= util::wrap(file_.nodes_[cur_node_.unwrap()].data.size())
      ) {
        auto& node{file_.nodes_[cur_node_.unwrap()]};

        // Firstly, copy the token boundaries.

        // Copy the appropraite part of the prefix's token boundaries into the first node.
        auto token_split{std::ranges::lower_bound(prefix_tokens,
          util::wrap(node.data.size()), {}, &source_file::token_boundary::offset)};
        if(
          token_split != prefix_tokens.cend()
          && token_split->offset == util::wrap(node.data.size())
          && ((token_split - prefix_tokens.cbegin()) % 2 != 0) != prefix_starts_in_token
        ) {
          // If the token boundary we found is a token end that is exactly at a node boundary,
          // it belongs in the first node.
          ++token_split;
        }
        node.tokens = std::ranges::subrange{prefix_tokens.cbegin(), token_split}
          | util::range_to<std::vector>();
        node.starts_in_token = prefix_starts_in_token;

        // Now copy the rest of the prefix tokens into the second node.
        if(cur_node_offset_ != util::wrap(node.data.size())) {
          auto tokens_to_add{std::ranges::subrange{token_split, prefix_tokens.end()}
            | std::views::transform([&](source_file::token_boundary tb) noexcept {
              return source_file::token_boundary{
                .offset = tb.offset - util::wrap(node.data.size()).cast<src_pos_t>(),
                .is_lookahead = tb.is_lookahead
              };
            })};
          auto& second_node{file_.nodes_[(cur_node_ + 1).unwrap()]};
          second_node.tokens.insert(second_node.tokens.cbegin(),
            tokens_to_add.begin(), tokens_to_add.end());
          second_node.starts_in_token
            = ((token_split - prefix_tokens.cbegin()) % 2 != 0) != prefix_starts_in_token;
        }

        // Secondly, adjust the current position to account for the fact that it has now moved
        // one node forward.
        cur_node_offset_ -= util::wrap(node.data.size()).cast<src_pos_t>();

        // If the prefix fit into exactly one new node, the value of cur_node_metric_ will
        // usually be equal to the first new node's metric at this point, and should be set to
        // zero. However, if the prefix ended in a carriage return that changed its metric due
        // to this delta, the two metric will be different, so we can't just use `from`.
        // Therefore, if the current position is exactly at the node boundary, we just
        // explicitly set the current metric to zero to avoid this issue.
        if(cur_node_offset_ == 0) {
          cur_node_metric_ = source_metric{0, 0};
        } else {
          cur_node_metric_ = cur_node_metric_.from(node.metric);
        }

        cur_token_boundary_ -= util::wrap(node.tokens.size()).cast<src_pos_t>();
        ++cur_node_;

        assert(
          cur_node_ == util::wrap(file_.nodes_.size())
          || cur_node_offset_ < util::wrap(file_.nodes_[cur_node_.unwrap()].data.size())
        );
      } else {
        // If the prefix text fits entirely into the first new node, copy all of the prefix
        // tokens there.
        if(cur_node_offset_ != 0) {
          auto& node{file_.nodes_[cur_node_.unwrap()]};
          node.tokens.insert(node.tokens.cbegin(),
            prefix_tokens.cbegin(), prefix_tokens.cend());
          node.starts_in_token = prefix_starts_in_token;
        }
      }
    }

    // If the last code point read was a carriage return, check if its metric changed. But if
    // the carriage return is in the previous node, it does not matter, because we're only
    // tracking the metric from the last node.
    if(cur_node_offset_ != 0) {
      if(
        cr_followed_by_lf
        && (cur_node_ == util::wrap(file_.nodes_.size())
        || file_.nodes_[cur_node_.unwrap()].data[cur_node_offset_.unwrap()] != u8'\n')
      ) {
        // The carriage return was followed by a line feed, but no longer is.
        ++cur_node_metric_.line;
        cur_node_metric_.col = 0;
      } else if(
        cr_not_followed_by_lf
        && cur_node_ != util::wrap(file_.nodes_.size())
        && file_.nodes_[cur_node_.unwrap()].data[cur_node_offset_.unwrap()] == u8'\n'
      ) {
        // The carriage return was not followed by a line feed, but now is.

        assert(cur_node_metric_.line != 0);
        assert(cur_node_metric_.col == 0);
        --cur_node_metric_.line;

        auto const& node{file_.nodes_[cur_node_.unwrap()]};
        auto it{node.data.cbegin() + cur_node_offset_.unwrap()};
        while(it != node.data.cbegin()) {
          unicode::utf8<>::decrement(it);
          auto it_copy{it};
          char32_t const ch{unicode::utf8<>::decode(it_copy,
            util::unreachable_sentinel<decltype(it_copy)>{}, unicode::eh_assume_valid{})};
          if(is_line_terminator(ch, file_.mode_) || ch == U'\r') {
            break;
          }
          cur_node_metric_.col += num_columns(ch, file_.mode_);
        }
      }
    }

    if(cur_delta_->replacement.empty()) {
      // If the delta we've just entered has an empty replacement, finish it immediately and
      // try to start another delta.
      skipped_deltas_metric = skipped_deltas_metric.add(finish_delta());
    } else {
      // If the delta we've just entered has a non-empty replacement, stop iterating. We're now
      // inside a delta.
      cur_delta_remains_ = util::wrap(cur_delta_->replacement.size()).cast<src_pos_t>();
      break;
    }
  }

  return skipped_deltas_metric;
}

source_metric incremental_token_source::advance_char() {
  auto& node{file_.nodes_[cur_node_.unwrap()]};
  auto it{node.data.cbegin() + cur_node_offset_.unwrap()};
  auto const it_old{it};

  char32_t const ch{unicode::utf8<>::decode(it, util::unreachable_sentinel<decltype(it)>{},
    unicode::eh_assume_valid<>{})};

  auto const ch_offset{util::wrap(it - it_old).cast<src_pos_t>()};

  if(
    cur_token_boundary_ < util::wrap(node.tokens.size())
    && node.tokens[cur_token_boundary_.unwrap()].offset == cur_node_offset_
  ) {
    // We're entering an old token; erase its information now
    if(cur_token_boundary_ == util::wrap(node.tokens.size()) - 1) {
      // This token ends in a later node. Remove just this boundary; the end will be removed
      // when we reach the node containing it
      node.tokens.pop_back();
    } else {
      // The token ends in the same node. Remove both boundaries.
      node.tokens.erase(node.tokens.cbegin() + cur_token_boundary_.unwrap(),
        node.tokens.cbegin() + (cur_token_boundary_ + 2).unwrap());
    }
  }

  cur_node_offset_ += ch_offset;
  bool const is_node_end{cur_node_offset_ == util::wrap(node.data.size())};
  if(is_node_end) {
    ++cur_node_;
    cur_node_offset_ = 0;
    cur_token_boundary_ = 0;
    if(cur_node_ != util::wrap(file_.nodes_.size())) {
      auto& next_node{file_.nodes_[cur_node_.unwrap()]};
      if(next_node.starts_in_token) {
        // If the next node starts in the middle of a token, we must have removed its starting
        // boundary already. Mark it as not being inside a token
        next_node.starts_in_token = false;

        // If the next node contains the token's end, remove it now. Otherwise, it will be
        // removed when we reach the node containing it
        if(!next_node.tokens.empty()) {
          next_node.tokens.erase(next_node.tokens.cbegin());
        }
      }
    }
    cur_node_metric_ = source_metric{0, 0};
  }

  source_metric ch_metric{source_metric::uninitialized()};
  if(is_line_terminator(ch, file_.mode_)) {
    ch_metric = {1, 0};
  } else if(ch == U'\r') {
    if(cur_node_ == util::wrap(file_.nodes_.size())) {
      ch_metric = {1, 0};
    } else if(file_.nodes_[cur_node_.unwrap()].data[cur_node_offset_.unwrap()] == u8'\n') {
      ch_metric = {0, num_columns(ch, file_.mode_)};
    } else {
      ch_metric = {1, 0};
    }
  } else {
    ch_metric = {0, num_columns(ch, file_.mode_)};
  }
  if(!is_node_end) {
    cur_node_metric_ = cur_node_metric_.add(ch_metric);
  }

  source_metric old_metric_change{0, 0};

  if(cur_delta_remains_ == 0) {
    // We are not in a delta. The old offset changes by the code point's metrics.
    old_metric_change = ch_metric;
    old_metric_ = old_metric_.add(ch_metric);

    // Remove skipped diagnostic nodes
    while(*cur_diags_ && (*cur_diags_)->token_loc < old_metric_) {
      *cur_diags_ = std::move((*cur_diags_)->next);
    }

    // Reduce the remaining reuse amount, if we have any
    if(reuse_remains_ != source_metric{0, 0}) {
      assert(reuse_remains_ >= ch_metric);
      reuse_remains_ = reuse_remains_.from(ch_metric);
    }
  } else {
    // We are in a delta. Adjust the remaining delta offset.
    assert(cur_delta_remains_ >= ch_offset);
    cur_delta_remains_ -= ch_offset;
    if(cur_delta_remains_ == 0) {
      old_metric_change = old_metric_change.add(finish_delta());
    }

    assert((reuse_remains_ == source_metric{0, 0}));
  }

  // If we're not in a delta or we've just finished a delta, try to start one.
  if(cur_delta_remains_ == 0) {
    old_metric_change = old_metric_change.add(try_start_delta());
  }

  return old_metric_change;
}

void incremental_token_source::synchronize() noexcept {
  assert((reuse_remains_ == source_metric{0, 0}));
  if(old_metric_ != source_metric{0, 0}) {
    assert(cur_node_ != util::wrap(file_.nodes_.size()));
    assert(cur_token_boundary_ != util::wrap(file_.nodes_[cur_node_.unwrap()].tokens.size()));
    assert((cur_token_boundary_ % 2 == 0)
      != file_.nodes_[cur_node_.unwrap()].starts_in_token);
    assert(cur_node_offset_ == file_.nodes_[cur_node_.unwrap()]
      .tokens[cur_token_boundary_.unwrap()].offset);
  }

  // TODO: Synchronize the lexer on token ends, not just token beginnings

  if(cur_node_ == util::wrap(file_.nodes_.size())) {
    return;
  }

  // If there are no more deltas, we can reuse until the end of the file.
  if(cur_delta_ == deltas_.end()) {
    reuse_remains_ = file_.nodes_[cur_node_.unwrap()].metric.from(cur_node_metric_);
    for(auto const& node: file_.nodes_ | std::views::drop((cur_node_ + 1).unwrap())) {
      reuse_remains_ = reuse_remains_.add(node.metric);
    }
    return;
  }

  // If we're right at the start of a delta, we can't reuse anything.
  if(cur_delta_->old_start == old_metric_) {
    return;
  }

  auto const metric_to_next_delta{cur_delta_->old_start.from(old_metric_)};
  auto advance_result{this->advance_by_metric(metric_to_next_delta)};

  // If we're at the start of a node and the preceding node ends in a token end, then that's
  // the boundary we want to look at, so move there. Note that this cannot be the first node,
  // because then we're already at the start of a delta, which is a situation we've already
  // handled.
  if(advance_result.node_offset == 0) {
    assert(advance_result.node_index != 0);
    auto const& prev_node{file_.nodes_[(advance_result.node_index - 1).unwrap()]};
    if(
      !prev_node.tokens.empty()
      && prev_node.tokens.back().offset == util::wrap(prev_node.data.size())
    ) {
      --advance_result.node_index;
      advance_result.node_offset = util::wrap(prev_node.data.size()).cast<src_pos_t>();
      advance_result.node_metric = prev_node.metric;
      advance_result.token_boundary
        = util::wrap(prev_node.tokens.size() - 1).cast<src_pos_t>();
    }
  }

  // If we're at the end of the file and there isn't a token that ends at the file end (this
  // case is handled by the code right above), we can reuse up to the end.
  if(advance_result.node_index == util::wrap(file_.nodes_.size())) {
    reuse_remains_ = metric_to_next_delta;

    // If the file ends in a carriage return, its metric might change due to the delta, so we
    // cannot reuse it. Exclude it from the reuse amount.
    auto cur_node{file_.nodes_.cend() - 1};
    if(cur_node->data.back() == u8'\r') {
      // A carriage return at the end of a file is necessarily a line terminator. So we need to
      // decrement the line count and the column count to the length of the last line.
      assert(reuse_remains_.line != 0);
      assert(reuse_remains_.col == 0);
      --reuse_remains_.line;

      assert(reuse_remains_.col == 0);

      if(cur_node->metric.line == 1) {
        // The last line could span several nodes.

        // Count the length of the part of the line that is in the last node (this is the
        // entire node except for the carriage return).
        reuse_remains_.col = num_columns_in_range(std::span{cur_node->data}
          .subspan(0, cur_node->data.size() - 1), file_.mode_);

        // Iterate over the nodes until we find the beginning of the last line.
        if(cur_node != file_.nodes_.cbegin()) {
          do {
            --cur_node;
            reuse_remains_.col += cur_node->metric.col;
          } while(cur_node->metric.line == 0 && cur_node != file_.nodes_.cbegin());
        }
      } else {
        // The last line is contained entirely in the last node.
        auto data_it{(cur_node->data.cend() - 1)}; // skip the CR
        for(;;) {
          assert(data_it != cur_node->data.cbegin());
          unicode::utf8<>::decrement(data_it);
          auto data_copy{data_it};
          char32_t const ch{unicode::utf8<>::decode(data_copy,
            util::unreachable_sentinel<decltype(data_it)>{}, unicode::eh_assume_valid<>{})};
          if(is_line_terminator(ch, file_.mode_) || ch == U'\r') {
            break;
          } else {
            reuse_remains_.col += num_columns(ch, file_.mode_);
          }
        }
      }
    }

    return;
  }

  // If we're not exactly at a token boundary, then we need to move to the preceding token
  // boundary: that's the limit to which we can reuse.
  //
  // If we're exactly at the start of a token, we can reuse right up to that token. (But we
  // might still need to see if the gap between tokens continues afterwards, since the delta
  // might have extended it!)
  //
  // If we're exactly at the end of a token, then:
  // - If it's a delimited token, we can reuse right up to and including that token.
  // - If it's a lookahead token, we can only reuse up to that token's beginning, so we need to
  //   move to the preceding token boundary.
  //
  // Therefore, we need to move to the preceding token boundary if we're not exactly at a token
  // boundary or if we are exactly at one and it is the end of a lookahead token.
  //
  // Note that there definitely is a token boundary that we will find that is not earlier than
  // the current position. That is because the current position, by assumption, coincides with
  // a token beginning. Therefore, if the next delta begins between token boundaries or at a
  // token end, there is at least one token beginning in the way.

  if(
    auto const& node{file_.nodes_[advance_result.node_index.unwrap()]};
    advance_result.token_boundary != util::wrap(node.tokens.size())
    && node.tokens[advance_result.token_boundary.unwrap()].offset == advance_result.node_offset
    && !node.tokens[advance_result.token_boundary.unwrap()].is_lookahead
  ) {
    // The found position is the reuse limit.
    reuse_remains_ = metric_to_next_delta;

    bool const cr_before_delta{
      advance_result.node_offset == 0
      ? file_.nodes_[(advance_result.node_index - 1).unwrap()].data.back() == u8'\r'
      : node.data[(advance_result.node_offset - 1).unwrap()] == u8'\r'
    };
    if(cr_before_delta) {
      // The delta is immediately preceded by a carriage return. Its metric might change due to
      // the delta, so we cannot reuse it. Exclude it from the reuse amount.
      bool const lf_at_delta{node.data[advance_result.node_offset.unwrap()] == u8'\n'};
      if(lf_at_delta) {
        // The CR is followed by an LF, so it is not a line break. Simply decrease the column
        // count.
        auto const cr_cols{num_columns(U'\r', file_.mode_)};
        assert(reuse_remains_.col >= cr_cols);
        reuse_remains_.col -= cr_cols;
      } else {
        // The CR is followed by an LF, so it is a line break. We need to decrement the line
        // count and set the column count to the length of the preceding line.
        assert(reuse_remains_.line != 0);
        assert(reuse_remains_.col == 0);
        --reuse_remains_.line;

        bool found_line_start{false};
        if(advance_result.node_offset != 0) {
          // skip the CR
          auto data_it{node.data.cbegin() + (advance_result.node_offset - 1).unwrap()};

          while(data_it != node.data.cbegin()) {
            unicode::utf8<>::decrement(data_it);
            auto data_copy{data_it};
            char32_t const ch{unicode::utf8<>::decode(data_copy,
              util::unreachable_sentinel<decltype(data_it)>{}, unicode::eh_assume_valid<>{})};
            if(is_line_terminator(ch, file_.mode_) || ch == U'\r') {
              found_line_start = true;
              break;
            } else {
              reuse_remains_.col += num_columns(ch, file_.mode_);
            }
          }
        }

        if(!found_line_start) {
          // The preceding line extends to previous nodes. Iterate over the nodes until we find
          // the beginning of the last line.
          auto cur_node{file_.nodes_.cbegin() + advance_result.node_index.unwrap()};
          if(cur_node != file_.nodes_.cbegin()) {
            do {
              --cur_node;
              reuse_remains_.col += cur_node->metric.col;
            } while(cur_node->metric.line == 0 && cur_node != file_.nodes_.cbegin());
          }
        }
      }
    }

    return;
  }

  // We need to move to the preceding token boundary.

  while(advance_result.token_boundary == 0) {
    --advance_result.node_index;
    advance_result.token_boundary
      = util::wrap(file_.nodes_[advance_result.node_index.unwrap()].tokens.size())
      .cast<src_pos_t>();
  }
  --advance_result.token_boundary;
  auto const& final_node{file_.nodes_[advance_result.node_index.unwrap()]};
  advance_result.node_offset
    = final_node.tokens[advance_result.token_boundary.unwrap()].offset;
  // NB: `advance_result.node_metric` is no longer correct!

  // Figure out the metric from the current position to the discovered token boundary. We'll
  // first calculate the metric from the start of the current node, then adjust it by the
  // current node metric.

  src_pos_t n{cur_node_};
  source_metric total_node_metric{0, 0};

  // Add up the metrics of all intermediate nodes
  while(n != advance_result.node_index) {
    total_node_metric = total_node_metric.add(file_.nodes_[n.unwrap()].metric);
    ++n;
  }

  // Add the metric of the last node up to the target position
  auto final_node_it{final_node.data.cbegin()};
  auto const final_node_iend{final_node_it + advance_result.node_offset.unwrap()};
  while(final_node_it != final_node_iend) {
    char32_t const ch{unicode::utf8<>::decode(final_node_it,
      util::unreachable_sentinel<decltype(final_node_it)>{},
      unicode::eh_assume_valid{})};
    if(is_line_terminator(ch, file_.mode_)) {
      total_node_metric = total_node_metric.add({1, 0});
    } else if(ch == U'\r') {
      // We will never reach the end of the node, otherwise we would be looking at the next
      // node instead.
      if(*final_node_it == u8'\n') {
        total_node_metric = total_node_metric.add({0, num_columns(ch, file_.mode_)});
      } else {
        total_node_metric = total_node_metric.add({1, 0});
      }
    } else {
      total_node_metric = total_node_metric.add({0, num_columns(ch, file_.mode_)});
    }
  }

  // Now adjust by the current node metric to exclude the part of the current node up to the
  // current position
  reuse_remains_ = total_node_metric.from(cur_node_metric_);
}

incremental_token_source::skip_gap_result incremental_token_source::skip_gap() {
  assert(spare_diags_);
  skip_gap_result result{
    .new_metric = {0, 0},
    .old_metric = {0, 0}
  };

  // TODO: Add sync opportunities now that gaps are simplified

  if(reuse_remains_ != source_metric{0, 0}) {
    // If there is any allowed reuse amount at all, it necessarily has to include at least one
    // gap.

    source_metric skip_metric{0, 0};
    source_metric const old_cur_node_metric{cur_node_metric_};

    bool skipped_node{false};
    while(cur_token_boundary_ == util::wrap(file_.nodes_[cur_node_.unwrap()].tokens.size())) {
      skipped_node = true;
      skip_metric = skip_metric.add(file_.nodes_[cur_node_.unwrap()].metric);
      ++cur_node_;
      assert(
        cur_node_ == util::wrap(file_.nodes_.size())
        || !file_.nodes_[cur_node_.unwrap()].starts_in_token
      );
      cur_token_boundary_ = 0;
      cur_node_metric_ = source_metric{0, 0};
      if(cur_node_ == util::wrap(file_.nodes_.size())) {
        break;
      }
    }

    if(cur_node_ == util::wrap(file_.nodes_.size())) {
      cur_node_offset_ = 0;
    } else {
      auto const& node{file_.nodes_[cur_node_.unwrap()]};
      // auto const node_span{file_.get_node_data(file_.nodes_[cur_node_])};

      auto data_it{node.data.cbegin()};
      if(!skipped_node) {
        data_it += cur_node_offset_.unwrap();
      }

      cur_node_offset_ = node.tokens[cur_token_boundary_.unwrap()].offset;
      if(skipped_node) {
        cur_node_metric_ = source_metric{0, 0};
      }

      auto const data_iend{node.data.cbegin() + cur_node_offset_.unwrap()};

      while(data_it != data_iend) {
        char32_t const ch{unicode::utf8<>::decode(data_it, data_iend,
          unicode::eh_assume_valid<>{})};
        if(is_line_terminator(ch, file_.mode_) || ch == U'\r' && *data_it != u8'\n') {
          cur_node_metric_ = cur_node_metric_.add({1, 0});
        } else {
          cur_node_metric_ = cur_node_metric_.add({0, num_columns(ch, file_.mode_)});
        }
      }
    }

    skip_metric = skip_metric.add(cur_node_metric_);
    skip_metric = skip_metric.from(old_cur_node_metric);

    result.old_metric = result.old_metric.add(skip_metric);
    result.new_metric = result.new_metric.add(skip_metric);

    auto const old_old_metric{old_metric_};
    auto const old_new_metric{spare_diags_->token_loc};

    old_metric_ = old_metric_.add(result.old_metric);
    reuse_remains_ = reuse_remains_.from(result.old_metric);

    while(*cur_diags_ && (*cur_diags_)->token_loc < old_metric_) {
      (*cur_diags_)->token_loc = old_new_metric.add(
        (*cur_diags_)->token_loc.from(old_old_metric));
      cur_diags_ = util::non_null_ptr{&(*cur_diags_)->next};
    }

    spare_diags_->token_loc = spare_diags_->token_loc.add(result.old_metric);

    // If the reuse limit extends past this token beginning, then it definitely begins a token,
    // so we don't need to check any further. But if the next delta begins right at this token
    // beginning, we also need to proceed with non-incremental gap lexing, since the delta may
    // have extended the gap.
    if(reuse_remains_ != source_metric{0, 0}) {
      return result;
    } else {
      result.old_metric = result.old_metric.add(try_start_delta());
    }
  }

  while(!is_at_end()) {
    auto const ch = get_char();
    if(!unicode::white_space(ch)) {
      break;
    }

    result.old_metric = result.old_metric.add(advance_char());

    auto const loc{[&]() noexcept -> source_metric {
      if(
        is_line_terminator(ch, file_.mode_)
        || ch == U'\r' && (is_at_end() || get_char() != U'\n')
      ) {
        return {1, 0};
      } else {
        return {0, num_columns(ch, file_.mode_)};
      }
    }()};

    result.new_metric = result.new_metric.add(loc);
    spare_diags_->token_loc = spare_diags_->token_loc.add(loc);
  }

  if(reuse_remains_ == source_metric{0, 0} && cur_node_ != util::wrap(file_.nodes_.size())) {
    auto const& node{file_.nodes_[cur_node_.unwrap()]};
    if(
      cur_token_boundary_ != util::wrap(node.tokens.size())
      && node.tokens[cur_token_boundary_.unwrap()].offset == cur_node_offset_
    ) {
      synchronize();
    }
  }

  return result;
}

token_source::first_gap_result incremental_token_source::do_first_gap() {
  assert(!spare_diags_);
  spare_diags_ = std::make_unique<token_diagnostics>(source_metric{0, 0});

  synchronize();

  auto const skipped_deltas_metric{try_start_delta()};

  auto const gap_result{skip_gap()};

  return {
    .new_metric = gap_result.new_metric,
    .old_metric = skipped_deltas_metric.add(gap_result.old_metric),
    .max_reuse = reuse_remains_
  };
}

token_source::next_token_result incremental_token_source::do_next_token() {
  if(is_at_end()) {
    return {
      .next_token = tok::eof{},
      .token_new_metric = source_metric::uninitialized(),
      .gap_new_metric = source_metric::uninitialized(),
      .old_metric = source_metric::uninitialized(),
      .max_reuse = source_metric::uninitialized()
    };
  }

  assert(spare_diags_);
  single_lexer lexer;
  source_metric token_new_metric{0, 0};
  source_metric token_old_metric{0, 0};

  {
    // Insert the token start boundary
    auto& cur_node{file_.nodes_[cur_node_.unwrap()]};
    cur_node.tokens.insert(
      cur_node.tokens.cbegin() + cur_token_boundary_.unwrap(),
      source_file::token_boundary{
        .offset = cur_node_offset_,
        .is_lookahead = false
      }
    );
    ++cur_token_boundary_;
  }

  bool is_lookahead{true};
  bool first{true};
  for(;;) {
    auto const ch = get_char();

    if(!lexer.wants(ch)) {
      is_lookahead = true;
      break;
    }

    if(first) {
      first = false;
    } else if(cur_node_offset_ == 0 && cur_node_ != util::wrap(file_.nodes_.size())) {
      file_.nodes_[cur_node_.unwrap()].starts_in_token = true;
    }

    token_old_metric = token_old_metric.add(advance_char());

    auto const loc{[&]() noexcept -> source_metric {
      if(
        is_line_terminator(ch, file_.mode_)
        || ch == U'\r' && (is_at_end() || get_char() != U'\n')) {
        return {1, 0};
      } else {
        return {0, num_columns(ch, file_.mode_)};
      }
    }()};

    token_new_metric = token_new_metric.add(loc);

    if(!lexer.feed(ch, loc)) {
      is_lookahead = false;
      break;
    }

    if(is_at_end()) {
      is_lookahead = true;
      break;
    }
  }

  // Insert the token end boundary
  if(cur_node_offset_ == 0) {
    auto& last_node{file_.nodes_[(cur_node_ - 1).unwrap()]};
    last_node.tokens.push_back(source_file::token_boundary{
      .offset = util::wrap(last_node.data.size()).cast<src_pos_t>(),
      .is_lookahead = is_lookahead
    });
  } else {
    auto& cur_node{file_.nodes_[cur_node_.unwrap()]};
    cur_node.tokens.insert(
      cur_node.tokens.cbegin() + cur_token_boundary_.unwrap(),
      source_file::token_boundary{
        .offset = cur_node_offset_,
        .is_lookahead = is_lookahead
      }
    );
    ++cur_token_boundary_;
  }

  token result{lexer.get_token()};

  spare_diags_->diags = lexer.get_diags();
  if(spare_diags_->diags.empty()) {
    spare_diags_->token_loc = spare_diags_->token_loc.add(token_new_metric);
  } else {
    auto new_spare_diags{std::make_unique<token_diagnostics>(
      spare_diags_->token_loc.add(token_new_metric))};
    assert(!spare_diags_->next);
    spare_diags_->next = std::move(*cur_diags_);
    *cur_diags_ = std::move(spare_diags_);
    cur_diags_ = util::non_null_ptr{&(*cur_diags_)->next};
    spare_diags_ = std::move(new_spare_diags);
  }

  auto const gap_result{skip_gap()};
  return {
    .next_token = std::move(result),
    .token_new_metric = token_new_metric,
    .gap_new_metric = gap_result.new_metric,
    .old_metric = token_old_metric.add(gap_result.old_metric),
    .max_reuse = reuse_remains_
  };
}

token_source::reuse_result
incremental_token_source::do_reuse(source_metric metric) noexcept {
  assert(metric <= reuse_remains_);

  auto const advance_result{this->advance_by_metric(metric)};

  cur_node_ = advance_result.node_index;
  cur_node_offset_ = advance_result.node_offset;
  cur_node_metric_ = advance_result.node_metric;
  cur_token_boundary_ = advance_result.token_boundary;

  if(cur_node_offset_ == 0) {
    assert(cur_node_ != 0);
    assert(cur_node_ - 1 < util::wrap(file_.nodes_.size()));
    [[maybe_unused]] auto const& prev_node{file_.nodes_[(cur_node_ - 1).unwrap()]};
    assert(!prev_node.tokens.empty());
    assert(prev_node.tokens.back().offset == util::wrap(prev_node.data.size()));
  } else {
    assert(cur_node_ < util::wrap(file_.nodes_.size()));
    [[maybe_unused]] auto const& node{file_.nodes_[cur_node_.unwrap()]};
    assert(cur_token_boundary_ < util::wrap(node.tokens.size()));
    assert(node.tokens[cur_token_boundary_.unwrap()].offset == cur_node_offset_);

    // We should have landed right at a token end. But if we keep advancing from this position,
    // we will remove the token boundary. This is not what we want: the boundary is part of a
    // token we reused. So we need to advance the current token index. But if the token end was
    // at the end of the previous node, we don't need to do this.
    ++cur_token_boundary_;
  }

  auto const old_old_metric{old_metric_};
  auto const old_new_metric{spare_diags_->token_loc};

  old_metric_ = old_metric_.add(metric);
  reuse_remains_ = reuse_remains_.from(metric);

  while(*cur_diags_ && (*cur_diags_)->token_loc < old_metric_) {
    (*cur_diags_)->token_loc = old_new_metric.add(
      (*cur_diags_)->token_loc.from(old_old_metric));
    cur_diags_ = util::non_null_ptr{&(*cur_diags_)->next};
  }

  spare_diags_->token_loc = spare_diags_->token_loc.add(metric);

  auto const delta_metric{try_start_delta()};

  auto const gap_result{skip_gap()};
  return {
    .gap_new_metric = gap_result.new_metric,
    .gap_old_metric = delta_metric.add(gap_result.old_metric),
    .max_reuse = reuse_remains_
  };
}
