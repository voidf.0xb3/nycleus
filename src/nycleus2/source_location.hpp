#ifndef NYCLEUS2_SOURCE_LOCATION_HPP_INCLUDED_AX4HRQK5GV36ESW315WQFVR65
#define NYCLEUS2_SOURCE_LOCATION_HPP_INCLUDED_AX4HRQK5GV36ESW315WQFVR65

#include "nycleus2/util.hpp"
#include "unicode/basic.hpp"
#include "unicode/encoding.hpp"
#include "util/safe_int.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <cassert>
#include <compare>
#include <cstdint>
#include <filesystem>
#include <numeric>
#include <span>

namespace nycleus2 {

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Metrics

/** @brief A range of positions, represented as metrics to the start and end of the range.
 *
 * The metrics are interpreted as having been measured from some common base. (In other words,
 * `end` is not relative to `start`, but rather to the same base that `start` is relative to.)
 */
struct source_metric_range;

/** @brief A metric from an earlier source location (the base) to a later one (the target).
 *
 * Source files are interpreted in a line-based manner: each source file is split into lines,
 * and each line is split into columns. A source location has two parts: the line number, and
 * the number of the column within the line.
 *
 * The precise interpretation of line and column numbers is controlled by the metric mode, see
 * below. It controls how many columns each code point takes and the positions of line endings.
 * (Note that regardless of how line numbers are calculated, all line terminators must be
 * recognized by the lexer.)
 *
 * The interpretation of this structure is as follows:
 * - If `line` is 0, then the target is on the same line as the base; `col` is the column
 *   offset that needs to be added to the column of the base to obtain the target.
 * - If `line` is not 0, then the target is on a later line; `line` is the line offset that
 *   needs to be added to the line of the base to obtain the target line, and `col` is the
 *   target's column position. The base's column position is ignored. */
struct source_metric {
  src_pos_t line;
  src_pos_t col;

  /** @brief Returns an uninitialized source metric. */
  [[nodiscard]] static source_metric uninitialized() noexcept {
    return {.line = src_pos_t::uninitialized(), .col = src_pos_t::uninitialized()};
  }

  /** @brief Adds two metrics together, applying this one first and the other one second.
   *
   * Since addition of metrics is not commutative, this is a named function, not an overloaded
   * operator. */
  [[nodiscard]] source_metric add(source_metric metric) const noexcept {
    if(metric.line == 0) {
      return {.line = line, .col = col + metric.col};
    } else {
      return {.line = line + metric.line, .col = metric.col};
    }
  }

  /** @brief Returns a metric that needs to be added to the given base to obtain this metric.
   */
  [[nodiscard]] source_metric from(source_metric base) const noexcept {
    assert(*this >= base);
    if(base.line == line) {
      return {.line = 0, .col = col - base.col};
    } else {
      return {.line = line - base.line, .col = col};
    }
  }

  /** @brief Returns a range where the start position is this metric and the end position is
   * obtained by adding the supplied argument. */
  [[nodiscard]] source_metric_range range(source_metric to_end) const noexcept;

  [[nodiscard]] friend bool operator==(source_metric, source_metric) = default;

  [[nodiscard]] friend std::strong_ordering operator<=>(
    source_metric a,
    source_metric b
  ) noexcept {
    if(auto const cmp{a.line <=> b.line}; cmp != 0) {
      return cmp;
    }
    return a.col <=> b.col;
  }
};

/** @brief An absolute source location, consisting of a file name and a metric from the start
 * of the file. */
struct source_location {
  gsl::not_null<std::filesystem::path const*> file;
  source_metric metric;
};

[[nodiscard]] inline bool operator==(source_location a, source_location b) noexcept {
  return *a.file == *b.file && a.metric == b.metric;
}

struct source_metric_range {
  source_metric start;
  source_metric end;

  [[nodiscard]] friend bool operator==(source_metric_range, source_metric_range) = default;
};

inline source_metric_range source_metric::range(source_metric to_end) const noexcept {
  return source_metric_range{*this, this->add(to_end)};
}

/** @brief An absolute source range, consisting of a file name and a metric range from the
 * start of the file. */
struct source_range {
  gsl::not_null<std::filesystem::path const*> file;
  source_metric_range range;
};

[[nodiscard]] inline bool operator==(source_range a, source_range b) noexcept {
  return *a.file == *b.file && a.range == b.range;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Metric modes

/** @brief The encoding used to compute the metrics of a source file. */
enum class metric_encoding: std::uint8_t {
  /** @brief The column is the offset in UTF-8 code units. */
  utf8 = 0,
  /** @brief The column is the offset in UTF-16 code units. */
  utf16 = 1,
  /** @brief The column is the offset in UTF-32 code units (i. e. Unicode code points). */
  utf32 = 2
};

/** @brief An indication of which code points are treated as line terminators. */
enum class metric_line_term: std::uint8_t {
  /** @brief All line terminators recognized by the language's semantics are also used for
   * computing metrics. */
  full = 0,
  /** @brief The only line terminators recognized for metrics are line feeds, carriage returns,
   * and CRLF pairs. */
  basic = 1,
  /** @brief The only line terminators recognized for metrics are line feeds, carriage returns,
   * and CRLF pairs. In addition, the carriage return that is part of a CRLF pair is measured
   * as having zero columns.
   *
   * The Language Server Protocol does not allow pointing to the position in the middle of a
   * CRLF pair. This mode makes the metrics of the positions before and after the carriage
   * return identical, so that there's no way to name that position. */
  lsp = 2
};

/** @brief A metric mode is a rule for computing the metrics of source text.
 *
 * It consists of two parts: the encoding and the line terminator mode. */
class metric_mode {
private:
  util::uint8_t data_;

public:
  void set_params(metric_encoding enc, metric_line_term lt) noexcept {
    data_ = util::wrap(static_cast<std::uint8_t>(enc)) << 2
      | util::wrap(static_cast<std::uint8_t>(lt));
  }

  explicit metric_mode(metric_encoding enc, metric_line_term lt) noexcept:
    data_{util::uint8_t::uninitialized()}
  {
    this->set_params(enc, lt);
  }

  metric_encoding encoding() const noexcept {
    return static_cast<metric_encoding>(((data_ >> 2) & 0x3).unwrap());
  }

  void encoding(metric_encoding enc) noexcept {
    this->set_params(enc, line_term());
  }

  metric_line_term line_term() const noexcept {
    return static_cast<metric_line_term>((data_ & 0x3).unwrap());
  }

  void line_term(metric_line_term lt) noexcept {
    this->set_params(encoding(), lt);
  }
};

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Metric computation

/** @brief Checks if the code point is counted as a line terminator.
 *
 * This is used for calculating source locations and should not be used for determining program
 * semantics.
 *
 * This function does not consider a carriage return as a line terminator. In real input, it
 * should be handled as one iff it is not part of a CRLF pair. This is the responsibility of
 * the caller. */
[[nodiscard]] inline bool is_line_terminator(char32_t ch, metric_mode mode) noexcept {
  assert(unicode::is_scalar_value(ch));
  switch(mode.line_term()) {
    case metric_line_term::full:
      return ch == U'\n'
          || ch == U'\v'
          || ch == U'\f'
          || ch == U'\u0085'
          || ch == U'\u2028'
          || ch == U'\u2029';

    case metric_line_term::basic:
    case metric_line_term::lsp:
      return ch == U'\n';
  }
}

/** @brief Counts the number of columns taken by a code point.
 *
 * The code point must not be a line terminator. A carriage return is allowed and is not
 * considered a line terminator. */
[[nodiscard]] inline src_pos_t num_columns(char32_t ch, metric_mode mode) noexcept {
  assert(unicode::is_scalar_value(ch));
  assert(!is_line_terminator(ch, mode));
  if(ch == U'\r' && mode.line_term() == metric_line_term::lsp) {
    return 0;
  }
  switch(mode.encoding()) {
    case metric_encoding::utf8:
      if(ch < 0x0080) {
        return 1;
      } else if(ch < 0x0800) {
        return 2;
      } else if(ch < 0x10000) {
        return 3;
      } else {
        return 4;
      }

    case metric_encoding::utf16:
      if(ch < 0x10000) {
        return 1;
      } else {
        return 2;
      }

    case metric_encoding::utf32:
      return 1;
  }
}

/** @brief Counts the number of columns taken by the code points in the given range.
 *
 * The code points in the range must not be line terminators. A carriage return is allowed at
 * the end and is not considered a line terminator.
 *
 * If the encoding is UTF-8, this simply returns the range's size (adjusted for the carriage
 * return at the end). Otherwise, this actually iterates over the range and adds up the
 * columns. */
[[nodiscard]] inline src_pos_t num_columns_in_range(
  std::span<char8_t const> range,
  metric_mode mode
) noexcept {
  assert(util::size_t::wrap(range.size()) <= max_file_size);
  assert(unicode::is_valid_utf8(range));

  assert(std::ranges::none_of(
    range | unicode::utf8_decode(unicode::eh_assume_valid<>{}),
    [&](char32_t ch) noexcept { return is_line_terminator(ch, mode); }
  ));
  assert(range.empty() || std::ranges::find(range, u8'\r') >= range.end() - 1);

  if(mode.encoding() == metric_encoding::utf8) {
    auto result{util::size_t::wrap(range.size()).cast<src_pos_t>()};
    if(mode.line_term() == metric_line_term::lsp && !range.empty() && range.back() == u8'\r') {
      --result;
    }
    return result;
  } else {
    auto view{range | unicode::utf8_decode(unicode::eh_assume_valid<>{})
      | std::views::transform([&](char32_t ch) noexcept { return num_columns(ch, mode); })};
    return std::accumulate(view.begin(), view.end(), src_pos_t{0});
  }
}

} // nycleus2

#endif
