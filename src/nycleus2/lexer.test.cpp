#include <boost/test/unit_test.hpp>

#include "nycleus2/diagnostic.hpp"
#include "nycleus2/diagnostic_tools.hpp"
#include "nycleus2/lexer.hpp"
#include "nycleus2/source_file.hpp"
#include "nycleus2/source_file_tools.hpp"
#include "nycleus2/source_location.hpp"
#include "nycleus2/source_location_tools.hpp"
#include "nycleus2/token.hpp"
#include "nycleus2/token_tools.hpp"
#include "unicode/encoding.hpp"
#include "util/bigint.hpp"
#include "single_pass_view.hpp"
#include <algorithm>
#include <array>
#include <boost/test/unit_test_suite.hpp>
#include <filesystem>
#include <functional>
#include <memory>
#include <optional>
#include <span>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

using namespace nycleus2;
using namespace util::bigint_literals;

namespace {

struct lexer_fixture {
  std::filesystem::path f{u8"test.ny"};
};

using d = source_file_delta;
using pn = test::nycleus2::source_file_access::proto_node;
using fg = token_source::first_gap_result;
using nt = token_source::next_token_result;
using r = token_source::reuse_result;
using test::nycleus2::diags;

void do_lexer_test(
  std::u8string_view input,
  fg expected_first_gap,
  std::span<nt const> expected_tokens,
  std::unique_ptr<token_diagnostics> expected_diags = nullptr,
  nycleus2::metric_mode mode
    = nycleus2::metric_mode{nycleus2::metric_encoding::utf8, nycleus2::metric_line_term::full}
) {
  full_token_source tok_src{input | tests::single_pass, mode};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == expected_first_gap);

  std::vector<token_source::next_token_result> tokens;
  do {
    tokens.push_back(tok_src.next_token());
  } while(!std::holds_alternative<tok::eof>(tokens.back().next_token));
  tokens.pop_back();
  BOOST_TEST(tokens == expected_tokens, boost::test_tools::per_element());

  auto const cur_diags_p{tok_src.get_diags()};
  auto const* cur_diags{cur_diags_p.get()};
  auto const* cur_expected_diags{expected_diags.get()};
  while(cur_diags && cur_expected_diags) {
    BOOST_TEST(cur_diags->token_loc == cur_expected_diags->token_loc);

    BOOST_TEST(test::nycleus2::diagseq(cur_diags->diags, cur_expected_diags->diags));

    cur_diags = cur_diags->next.get();
    cur_expected_diags = cur_expected_diags->next.get();
  }
  BOOST_TEST(!cur_diags);
  BOOST_TEST(!cur_expected_diags);
}

#if NYCLEUS_SOURCE_FILE_NODE_SIZE_BASE == 11

void do_lexer_test_incr(
  std::span<pn const> initial_nodes,
  std::span<d const> deltas,
  std::span<pn const> expected_nodes,
  std::filesystem::path f,
  fg expected_first_gap,
  std::span<nt const> expected_tokens,
  std::unique_ptr<token_diagnostics> initial_diags = nullptr,
  std::unique_ptr<token_diagnostics> expected_diags = nullptr,
  nycleus2::metric_mode mode
    = nycleus2::metric_mode{nycleus2::metric_encoding::utf8, nycleus2::metric_line_term::full}
) {
  auto src{test::nycleus2::source_file_access::create(
    std::move(f), initial_nodes, std::move(initial_diags), mode)};
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == expected_first_gap);

  std::vector<nycleus2::token_source::next_token_result> tokens;
  do {
    tokens.push_back(tok_src.next_token());
  } while(!std::holds_alternative<tok::eof>(tokens.back().next_token));
  tokens.pop_back();
  BOOST_TEST(tokens == expected_tokens, boost::test_tools::per_element());

  test::nycleus2::source_file_access::compare(src, expected_nodes);

  src.validate();

  auto const* cur_diags{
    test::nycleus2::source_file_access::get_diags(src).get()};
  auto const* cur_expected_diags{expected_diags.get()};
  while(cur_diags && cur_expected_diags) {
    BOOST_TEST(cur_diags->token_loc == cur_expected_diags->token_loc);

    BOOST_TEST(test::nycleus2::diagseq(cur_diags->diags, cur_expected_diags->diags));

    cur_diags = cur_diags->next.get();
    cur_expected_diags = cur_expected_diags->next.get();
  }
  BOOST_TEST(!cur_diags);
  BOOST_TEST(!cur_expected_diags);
}

#endif

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_nycleus2)
BOOST_FIXTURE_TEST_SUITE(lexer, lexer_fixture)

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Punctuation

BOOST_AUTO_TEST_SUITE(punctuation)

BOOST_AUTO_TEST_CASE(excl) {
  do_lexer_test(u8"!", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::excl{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"! ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::excl{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(neq) {
  do_lexer_test(u8"!=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::neq{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"!= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::neq{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(percent) {
  do_lexer_test(u8"%", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::percent{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"% ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::percent{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(percent_assign) {
  do_lexer_test(u8"%=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::percent_assign{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"%= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::percent_assign{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(amp) {
  do_lexer_test(u8"&", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::amp{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"& ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::amp{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(logical_and) {
  do_lexer_test(u8"&&", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::logical_and{{0, 1}}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"&& ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::logical_and{{0, 1}}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(amp_assign) {
  do_lexer_test(u8"&=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::amp_assign{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"&= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::amp_assign{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(lparen) {
  do_lexer_test(u8"(", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lparen{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"( ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lparen{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(rparen) {
  do_lexer_test(u8")", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rparen{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8") ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rparen{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(star) {
  do_lexer_test(u8"*", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::star{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"* ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::star{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(star_assign) {
  do_lexer_test(u8"*=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::star_assign{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"*= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::star_assign{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(plus) {
  do_lexer_test(u8"+", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::plus{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"+ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::plus{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(incr) {
  do_lexer_test(u8"++", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::incr{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"++ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::incr{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(plus_assign) {
  do_lexer_test(u8"+=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::plus_assign{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"+= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::plus_assign{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comma) {
  do_lexer_test(u8",", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comma{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8", ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comma{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(minus) {
  do_lexer_test(u8"-", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::minus{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"- ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::minus{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(decr) {
  do_lexer_test(u8"--", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::decr{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"-- ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::decr{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(minus_assign) {
  do_lexer_test(u8"-=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::minus_assign{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"-= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::minus_assign{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(arrow) {
  do_lexer_test(u8"->", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::arrow{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"-> ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::arrow{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(point) {
  do_lexer_test(u8".", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8". ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::point{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(double_point) {
  do_lexer_test(u8"..", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::double_point{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8".. ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::double_point{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(ellipsis) {
  do_lexer_test(u8"...", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::ellipsis{}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"... ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::ellipsis{}, {0, 3}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(slash) {
  do_lexer_test(u8"/", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::slash{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"/ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::slash{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(slash_assign) {
  do_lexer_test(u8"/=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::slash_assign{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"/= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::slash_assign{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(colon) {
  do_lexer_test(u8":", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::colon{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8": ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::colon{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(semicolon) {
  do_lexer_test(u8";", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::semicolon{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"; ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::semicolon{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(lt) {
  do_lexer_test(u8"<", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lt{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"< ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lt{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(lrot) {
  do_lexer_test(u8"</", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lrot{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"</ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lrot{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(lrot_assign) {
  do_lexer_test(u8"</=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lrot_assign{}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"</= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lrot_assign{}, {0, 3}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(lshift) {
  do_lexer_test(u8"<<", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lshift{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"<< ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lshift{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(lshift_assign) {
  do_lexer_test(u8"<<=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lshift_assign{}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"<<= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lshift_assign{}, {0, 3}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(leq) {
  do_lexer_test(u8"<=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::leq{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"<= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::leq{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(three_way_cmp) {
  do_lexer_test(u8"<=>", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::three_way_cmp{}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"<=> ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::three_way_cmp{}, {0, 3}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(assign) {
  do_lexer_test(u8"=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::assign{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::assign{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(eq) {
  do_lexer_test(u8"==", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::eq{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"== ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::eq{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(fat_arrow) {
  do_lexer_test(u8"=>", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::fat_arrow{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"=> ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::fat_arrow{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(gt) {
  do_lexer_test(u8">", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::gt{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"> ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::gt{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(rrot) {
  do_lexer_test(u8">/", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rrot{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8">/ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rrot{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(rrot_assign) {
  do_lexer_test(u8">/=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rrot_assign{}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8">/= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rrot_assign{}, {0, 3}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(geq) {
  do_lexer_test(u8">=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::geq{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8">= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::geq{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(rshift) {
  do_lexer_test(u8">>", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rshift{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8">> ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rshift{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(rshift_assign) {
  do_lexer_test(u8">>=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rshift_assign{}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8">>= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rshift_assign{}, {0, 3}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(question) {
  do_lexer_test(u8"?", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::question{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"? ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::question{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(at) {
  do_lexer_test(u8"@", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::at{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"@ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::at{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(lsquare) {
  do_lexer_test(u8"[", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lsquare{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"[ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lsquare{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(rsquare) {
  do_lexer_test(u8"]", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rsquare{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"] ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rsquare{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(bitwise_xor) {
  do_lexer_test(u8"^", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::bitwise_xor{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"^ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::bitwise_xor{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(bitwise_xor_assign) {
  do_lexer_test(u8"^=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::bitwise_xor_assign{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"^= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::bitwise_xor_assign{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(logical_xor) {
  do_lexer_test(u8"^^", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::logical_xor{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"^^ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::logical_xor{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(lbrace) {
  do_lexer_test(u8"{", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lbrace{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"{ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::lbrace{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(pipe) {
  do_lexer_test(u8"|", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::pipe{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"| ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::pipe{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(pipe_assign) {
  do_lexer_test(u8"|=", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::pipe_assign{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"|= ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::pipe_assign{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(subst) {
  do_lexer_test(u8"|>", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::subst{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"|> ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::subst{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(logical_or) {
  do_lexer_test(u8"||", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::logical_or{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"|| ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::logical_or{}, {0, 2}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(rbrace) {
  do_lexer_test(u8"}", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rbrace{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"} ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::rbrace{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(tilde) {
  do_lexer_test(u8"~", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::tilde{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"~ ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::tilde{}, {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(maximal_munch) {
  do_lexer_test(u8".....>>>==", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::ellipsis{}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::double_point{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::rshift{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::geq{}, {0, 2}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::assign{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_SUITE_END() // punctuation

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Words

BOOST_AUTO_TEST_SUITE(words)

BOOST_AUTO_TEST_CASE(unprefixed) {
  do_lexer_test(u8"foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });

  do_lexer_test(u8"foo ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 1}, {0, 0}, {0, 0}}
  });

  // "строка"
  do_lexer_test(
    u8"\u0441\u0442\u0440\u043E\u043A\u0430",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0441\u0442\u0440\u043E\u043A\u0430",
        tok::word::sigil_t::none}, {0, 12}, {0, 0}, {0, 0}, {0, 0}}
    }
  );

  // "細繩"
  do_lexer_test(u8"\u7D30\u7E69", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"\u7D30\u7E69", tok::word::sigil_t::none},
      {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  });

  // "𝕗𝕠𝕠"
  do_lexer_test(
    u8"\U0001D557\U0001D560\U0001D560",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\U0001D557\U0001D560\U0001D560",
        tok::word::sigil_t::none}, {0, 12}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(id) {
  do_lexer_test(u8"$foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::dollar},
      {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });

  do_lexer_test(u8"$foo ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::dollar},
      {0, 4}, {0, 1}, {0, 0}, {0, 0}}
  });

  // "строка"
  do_lexer_test(
    u8"$\u0441\u0442\u0440\u043E\u043A\u0430",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0441\u0442\u0440\u043E\u043A\u0430",
        tok::word::sigil_t::dollar}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    }
  );

  // "細繩"
  do_lexer_test(u8"$\u7D30\u7E69", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"\u7D30\u7E69", tok::word::sigil_t::dollar},
      {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  });

  // "𝕗𝕠𝕠"
  do_lexer_test(
    u8"$\U0001D557\U0001D560\U0001D560",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\U0001D557\U0001D560\U0001D560",
        tok::word::sigil_t::dollar}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(ext) {
  do_lexer_test(u8"_foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::underscore},
      {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });

  do_lexer_test(u8"_foo ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::underscore},
      {0, 4}, {0, 1}, {0, 0}, {0, 0}}
  });

  // "строка"
  do_lexer_test(
    u8"_\u0441\u0442\u0440\u043E\u043A\u0430",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0441\u0442\u0440\u043E\u043A\u0430",
        tok::word::sigil_t::underscore}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    }
  );

  // "細繩"
  do_lexer_test(u8"_\u7D30\u7E69", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"\u7D30\u7E69", tok::word::sigil_t::underscore},
      {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  });

  // "𝕗𝕠𝕠"
  do_lexer_test(
    u8"_\U0001D557\U0001D560\U0001D560",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\U0001D557\U0001D560\U0001D560",
        tok::word::sigil_t::underscore}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(internal_sigils) {
  do_lexer_test(u8"foo_bar$qux", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo_bar$qux", tok::word::sigil_t::none},
      {0, 11}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"$foo_bar$qux", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo_bar$qux", tok::word::sigil_t::dollar},
      {0, 12}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"_foo_bar$qux", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo_bar$qux", tok::word::sigil_t::underscore},
      {0, 12}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(empty_id) {
  do_lexer_test(u8"$", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::empty_id>(source_metric_range{{0, 0}, {0, 1}})
  )));
}

BOOST_AUTO_TEST_CASE(empty_ext) {
  do_lexer_test(u8"_", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::empty_ext>(source_metric_range{{0, 0}, {0, 1}})
  )));
}

BOOST_AUTO_TEST_CASE(zwnj) {
  do_lexer_test(
    u8"\u0646\u0627\u0645\u0647\u200C\u0627\u06CC",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0646\u0627\u0645\u0647\u0627\u06CC",
        tok::word::sigil_t::none}, {0, 15}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"\u0D26\u0D43\u0D15\u0D4D\u200C\u0D38\u0D3E\u0D15\u0D4D\u0D37\u0D3F",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{
        u8"\u0D26\u0D43\u0D15\u0D4D\u0D38\u0D3E\u0D15\u0D4D\u0D37\u0D3F",
        tok::word::sigil_t::none}, {0, 33}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(zwj) {
  do_lexer_test(
    u8"\u0DC1\u0DCA\u200D\u0DBB\u0DD3\u0DBD\u0D82\u0D9A\u0DCF",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0DC1\u0DCA\u0DBB\u0DD3\u0DBD\u0D82\u0D9A\u0DCF",
        tok::word::sigil_t::none}, {0, 27}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"\u0DC1\u0DCA\u200D",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0DC1\u0DCA", tok::word::sigil_t::none},
        {0, 9}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(zwnj_fail) {
  do_lexer_test(
    u8"\u0646\u0627\u0645\u0647\u200C",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0646\u0627\u0645\u0647", tok::word::sigil_t::none},
        {0, 11}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::unexpected_zwnj>(source_metric_range{{0, 8}, {0, 3}})
    ))
  );
  do_lexer_test(
    u8"\u0D26\u0D43\u0D15\u0D4D\u200C",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0D26\u0D43\u0D15\u0D4D", tok::word::sigil_t::none},
        {0, 15}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::unexpected_zwnj>(source_metric_range{{0, 12}, {0, 3}})
    ))
  );
}

BOOST_AUTO_TEST_CASE(zwj_fail) {
  do_lexer_test(
    u8"\u0DC1\u0DCA\u200D\u0DCF",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::word{u8"\u0DC1\u0DCA\u0DCF", tok::word::sigil_t::none},
        {0, 12}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::unexpected_zwj>(source_metric_range{{0, 6}, {0, 3}})
    ))
  );
}

BOOST_AUTO_TEST_SUITE_END() // words

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Numeric literals

BOOST_AUTO_TEST_SUITE(numeric)

BOOST_AUTO_TEST_CASE(dec) {
  do_lexer_test(u8"1\U0001D7DF9", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"1\U0001D7DF9 ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 6}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(hex) {
  do_lexer_test(
    u8"\U0001D7D8x00BaDC0De1\U0001D7DA3b",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{0xBADC0DE123B_big, std::nullopt},
        {0, 21}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"0BaDC0De1\U0001D7DA3h",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{0xBADC0DE123_big, std::nullopt},
        {0, 15}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(oct) {
  do_lexer_test(
    u8"\U0001D7D8o001\U0001D7DA3",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{0123_big, std::nullopt},
        {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"001\U0001D7DA3o",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{0123_big, std::nullopt},
        {0, 9}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(bin) {
  do_lexer_test(
    u8"\U0001D7D8b101\U0001D7D90011",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{179_big, std::nullopt},
        {0, 16}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"101\U0001D7D90011b",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{179_big, std::nullopt},
        {0, 12}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(big) {
  do_lexer_test(
    u8"1234567890123456789012345678901234567890123456789012345678901234567",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{
        1234567890123456789012345678901234567890123456789012345678901234567_big,
        std::nullopt}, {0, 67}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"0x123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{
        0x123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF_big,
        std::nullopt}, {0, 65}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"0o1234567012345670123456701234567012345670123456701234567012345670",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{
        01234567012345670123456701234567012345670123456701234567012345670_big,
        std::nullopt}, {0, 66}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"0b011100100110001011100011001011001000011010110101001111010001",
    fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
      nt{tok::int_literal{0x7262E32C86B53D1_big, std::nullopt},
        {0, 62}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(underscores) {
  do_lexer_test(u8"1_7__9", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"0__x_B3", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"10_1100__11_b", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"0_o26_3", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(prefix_and_suffix) {
  do_lexer_test(u8"0x123h", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_base_prefix_and_suffix>(
      source_metric_range{{0, 0}, {0, 6}})
  )));
  do_lexer_test(u8"0o123o", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_base_prefix_and_suffix>(
      source_metric_range{{0, 0}, {0, 6}})
  )));
  do_lexer_test(u8"0b111b", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_base_prefix_and_suffix>(
      source_metric_range{{0, 0}, {0, 6}})
  )));
}

BOOST_AUTO_TEST_CASE(type_suffix) {
  do_lexer_test(u8"179u64", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, int_type_suffix{64, false}},
      {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"179s032", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, int_type_suffix{32, true}},
      {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"179U100", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, int_type_suffix{100, false}},
      {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"179S002", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, int_type_suffix{2, true}},
      {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(type_suffix_empty) {
  do_lexer_test(u8"179u", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_suffix_missing_width>(
      source_metric_range{{0, 0}, {0, 4}},
      diag::num_literal_suffix_missing_width::prefix_letter_t::lower_u
    )
  )));
  do_lexer_test(u8"179s", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_suffix_missing_width>(
      source_metric_range{{0, 0}, {0, 4}},
      diag::num_literal_suffix_missing_width::prefix_letter_t::lower_s
    )
  )));
  do_lexer_test(u8"179U", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_suffix_missing_width>(
      source_metric_range{{0, 0}, {0, 4}},
      diag::num_literal_suffix_missing_width::prefix_letter_t::upper_u
    )
  )));
  do_lexer_test(u8"179S", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_suffix_missing_width>(
      source_metric_range{{0, 0}, {0, 4}},
      diag::num_literal_suffix_missing_width::prefix_letter_t::upper_s
    )
  )));
}

BOOST_AUTO_TEST_CASE(type_suffix_zero) {
  do_lexer_test(u8"179u0", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(
      source_metric_range{{0, 0}, {0, 5}})
  )));
  do_lexer_test(u8"179s00", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(
      source_metric_range{{0, 0}, {0, 6}})
  )));
  do_lexer_test(u8"179U000", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(
      source_metric_range{{0, 0}, {0, 7}})
  )));
  do_lexer_test(u8"179S0000", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(
      source_metric_range{{0, 0}, {0, 8}})
  )));
}

BOOST_AUTO_TEST_CASE(type_suffix_big) {
  do_lexer_test(
    u8"179u99999999999999999999999999999999",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::int_literal{179_big, std::nullopt},
        {0, 36}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::num_literal_suffix_too_wide>(
        source_metric_range{{0, 0}, {0, 36}})
    ))
  );
  do_lexer_test(
    u8"179U99999999999999999999999999999999",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::int_literal{179_big, std::nullopt},
        {0, 36}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::num_literal_suffix_too_wide>(
        source_metric_range{{0, 0}, {0, 36}})
    ))
  );
  do_lexer_test(
    u8"179s99999999999999999999999999999999",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::int_literal{179_big, std::nullopt},
        {0, 36}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::num_literal_suffix_too_wide>(
        source_metric_range{{0, 0}, {0, 36}})
    ))
  );
  do_lexer_test(
    u8"179S99999999999999999999999999999999",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::int_literal{179_big, std::nullopt},
        {0, 36}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::num_literal_suffix_too_wide>(
        source_metric_range{{0, 0}, {0, 36}})
    ))
  );
}

BOOST_AUTO_TEST_CASE(invalid) {
  do_lexer_test(u8"0x123.456hello", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 14}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 14}})
  )));
  do_lexer_test(u8"123hello", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 8}})
  )));
  do_lexer_test(u8".456hello", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 9}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 9}})
  )));
  do_lexer_test(u8"123e+5hello", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 11}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 11}})
  )));
  do_lexer_test(u8"123.456e+5hello", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 15}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 15}})
  )));
  do_lexer_test(u8".456e+5hello", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 12}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 12}})
  )));
  do_lexer_test(u8"0x123foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 8}})
  )));
  do_lexer_test(u8"123$foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 7}})
  )));
  do_lexer_test(u8"0x123$foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::error{}, {0, 9}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::num_literal_invalid>(
      source_metric_range{{0, 0}, {0, 9}})
  )));
}

BOOST_AUTO_TEST_SUITE_END() // numeric

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: String & character literals

BOOST_AUTO_TEST_SUITE(strings)

BOOST_AUTO_TEST_CASE(string_literal) {
  do_lexer_test(u8"\"test\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"test"}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"\"test\" ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"test"}, {0, 6}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(char_literal) {
  do_lexer_test(u8"'test'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"test"}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"'test' ", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"test"}, {0, 6}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(string_literal_normalization) {
  do_lexer_test(
    u8"\"franc\u0327ais\"",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::string_literal{u8"fran\u00E7ais"},
        {0, 12}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(char_literal_normalization) {
  do_lexer_test(u8"'c\u0327'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"\u00E7"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(escape_codes) {
  do_lexer_test(
    u8"\"\\\"\\'\\\\\\a\\b\\e\\f\\n\\r\\t\\v\"\n'\\'' '\\n'",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::string_literal{u8"\"'\\\a\b\u001B\f\n\r\t\v"},
        {0, 24}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::char_literal{u8"'"}, {0, 4}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::char_literal{u8"\n"}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(unicode_escape_codes) {
  do_lexer_test(
    u8"\"\\u0041\\u0416\\u4E2D\"\n\"\\U000041\\U000416\\U004E2D\\U01F4A9\"",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::string_literal{u8"A\u0416\u4E2D"},
        {0, 20}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::string_literal{u8"A\u0416\u4E2D\U0001F4A9"},
        {0, 34}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(invalid_escape_codes) {
  do_lexer_test(u8"\"\\x\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"\uFFFD"}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 2}})
  )));
  do_lexer_test(u8"\"\\u12 \"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"\uFFFD "}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 4}})
  )));
  do_lexer_test(u8"\"abc\\U12345\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"abc\uFFFD"}, {0, 12}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 4}, {0, 7}})
  )));
}

BOOST_AUTO_TEST_CASE(invalid_escape_codes_unicode) {
  do_lexer_test(u8"\"\\uD800\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"\uFFFD"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 6}})
  )));
  do_lexer_test(u8"\"\\U00D800\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"\uFFFD"}, {0, 10}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 8}})
  )));
  do_lexer_test(u8"\"\\UFFFFFF\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"\uFFFD"}, {0, 10}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 8}})
  )));
}

BOOST_AUTO_TEST_CASE(escape_codes_normalization) {
  do_lexer_test(u8"\"\\a\u0301\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"\uFFFD"}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 1}})
  )));
  do_lexer_test(u8"\"\\u\u0301\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"\uFFFD"}, {0, 6}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 1}})
  )));
  do_lexer_test(u8"\"\\u12A\u0307\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"\uFFFD\u0226"}, {0, 9}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 1}})
  )));
}

BOOST_AUTO_TEST_CASE(unterminated_string_literal) {
  do_lexer_test(u8"\"abc", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"abc"}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::unterminated_string>(source_metric_range{{0, 0}, {0, 4}})
  )));
  do_lexer_test(u8"\"abc\\", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"abc\uFFFD"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 4}, {0, 1}}),
    std::make_unique<diag::unterminated_string>(source_metric_range{{0, 0}, {0, 5}})
  )));
  do_lexer_test(u8"\"abc\\u12", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"abc\uFFFD"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 4}, {0, 4}}),
    std::make_unique<diag::unterminated_string>(source_metric_range{{0, 0}, {0, 8}})
  )));
}

BOOST_AUTO_TEST_CASE(unterminated_char_literal) {
  do_lexer_test(u8"'abc", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"abc"}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::unterminated_char>(source_metric_range{{0, 0}, {0, 4}})
  )));
  do_lexer_test(u8"'abc\\", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"abc\uFFFD"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 4}, {0, 1}}),
    std::make_unique<diag::unterminated_char>(source_metric_range{{0, 0}, {0, 5}})
  )));
  do_lexer_test(u8"'abc\\u12", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"abc\uFFFD"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 4}, {0, 4}}),
    std::make_unique<diag::unterminated_char>(source_metric_range{{0, 0}, {0, 8}})
  )));
}

// NOLINTBEGIN(misc-misleading-bidirectional)

BOOST_AUTO_TEST_CASE(bidi_code_in_string) {
  do_lexer_test(u8"\"foo\u202A\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u202A"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_lre)
  )));
  do_lexer_test(u8"\"foo\u202B\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u202B"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_rle)
  )));
  do_lexer_test(u8"\"foo\u202C\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u202C"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_pdf)
  )));
  do_lexer_test(u8"\"foo\u202D\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u202D"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_lro)
  )));
  do_lexer_test(u8"\"foo\u202E\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u202E"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_rlo)
  )));
  do_lexer_test(u8"\"foo\u2066\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u2066"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_lri)
  )));
  do_lexer_test(u8"\"foo\u2067\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u2067"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_rli)
  )));
  do_lexer_test(u8"\"foo\u2068\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u2068"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_fsi)
  )));
  do_lexer_test(u8"\"foo\u2069\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\u2069"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::str_pdi)
  )));
}

BOOST_AUTO_TEST_CASE(bidi_code_in_char) {
  do_lexer_test(u8"'foo\u202A'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u202A"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_lre)
  )));
  do_lexer_test(u8"'foo\u202B'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u202B"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_rle)
  )));
  do_lexer_test(u8"'foo\u202C'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u202C"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_pdf)
  )));
  do_lexer_test(u8"'foo\u202D'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u202D"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_lro)
  )));
  do_lexer_test(u8"'foo\u202E'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u202E"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_rlo)
  )));
  do_lexer_test(u8"'foo\u2066'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u2066"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_lri)
  )));
  do_lexer_test(u8"'foo\u2067'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u2067"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_rli)
  )));
  do_lexer_test(u8"'foo\u2068'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u2068"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_fsi)
  )));
  do_lexer_test(u8"'foo\u2069'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\u2069"}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::bidi_code_in_string>(source_metric_range{{0, 4}, {0, 3}},
      diag::bidi_code_in_string::bidi_code_t::char_pdi)
  )));
}

// NOLINTEND(misc-misleading-bidirectional)

BOOST_AUTO_TEST_CASE(newline) {
  do_lexer_test(u8"\"foo\nbar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\nbar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
      diag::string_newline::newline_code_t::str_lf)
  )));
  do_lexer_test(u8"\"foo\vbar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\vbar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
      diag::string_newline::newline_code_t::str_vt)
  )));
  do_lexer_test(u8"\"foo\rbar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\rbar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
      diag::string_newline::newline_code_t::str_cr)
  )));
  do_lexer_test(u8"\"foo\r\nbar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo\r\nbar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {0, 1}},
      diag::string_newline::newline_code_t::str_cr),
    std::make_unique<diag::string_newline>(source_metric_range{{0, 5}, {1, 0}},
      diag::string_newline::newline_code_t::str_lf)
  )));
  do_lexer_test(
    u8"\"foo\u0085bar\"",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::string_literal{u8"foo\u0085bar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
        diag::string_newline::newline_code_t::str_nel)
    ))
  );
  do_lexer_test(
    u8"\"foo\u2028bar\"",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::string_literal{u8"foo\u2028bar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
        diag::string_newline::newline_code_t::str_line_sep)
    ))
  );
  do_lexer_test(
    u8"\"foo\u2029bar\"",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::string_literal{u8"foo\u2029bar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
        diag::string_newline::newline_code_t::str_para_sep)
    ))
  );

  do_lexer_test(u8"'foo\nbar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\nbar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
      diag::string_newline::newline_code_t::char_lf)
  )));
  do_lexer_test(u8"'foo\vbar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\vbar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
      diag::string_newline::newline_code_t::char_vt)
  )));
  do_lexer_test(u8"'foo\rbar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\rbar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
      diag::string_newline::newline_code_t::char_cr)
  )));
  do_lexer_test(u8"'foo\r\nbar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo\r\nbar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {0, 1}},
      diag::string_newline::newline_code_t::char_cr),
    std::make_unique<diag::string_newline>(source_metric_range{{0, 5}, {1, 0}},
      diag::string_newline::newline_code_t::char_lf)
  )));
  do_lexer_test(
    u8"'foo\u0085bar'",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::char_literal{u8"foo\u0085bar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
        diag::string_newline::newline_code_t::char_nel)
    ))
  );
  do_lexer_test(
    u8"'foo\u2028bar'",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::char_literal{u8"foo\u2028bar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
        diag::string_newline::newline_code_t::char_line_sep)
    ))
  );
  do_lexer_test(
    u8"'foo\u2029bar'",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::char_literal{u8"foo\u2029bar"}, {1, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
        diag::string_newline::newline_code_t::char_para_sep)
    ))
  );
}

BOOST_AUTO_TEST_SUITE_END() // strings

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Invalid code points

BOOST_AUTO_TEST_CASE(unexpected_code_point) {
  do_lexer_test(u8"foo \u20BDbar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
    nt{tok::error{}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  }, std::make_unique<token_diagnostics>(source_metric{0, 4}, diags(
    std::make_unique<diag::unexpected_code_point>(
      source_metric_range{{0, 0}, {0, 3}}, U'\u20BD')
  )));
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Gaps

BOOST_AUTO_TEST_SUITE(gaps)

BOOST_AUTO_TEST_CASE(white_space) {
  do_lexer_test(u8"   a  b\t", fg{{0, 3}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"a", tok::word::sigil_t::none},
      {0, 1}, {0, 2}, {0, 0}, {0, 0}},
    nt{tok::word{u8"b", tok::word::sigil_t::none},
      {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"   a \u2000 b\t", fg{{0, 3}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"a", tok::word::sigil_t::none},
      {0, 1}, {0, 5}, {0, 0}, {0, 0}},
    nt{tok::word{u8"b", tok::word::sigil_t::none},
      {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"   a \u2028 b\t", fg{{0, 3}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"a", tok::word::sigil_t::none},
      {0, 1}, {1, 1}, {0, 0}, {0, 0}},
    nt{tok::word{u8"b", tok::word::sigil_t::none},
      {0, 1}, {0, 1}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(single_line) {
  do_lexer_test(u8"#comment\nfoo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {1, 0}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"#comment\r\nfoo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {1, 0}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"#comment\rfoo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {1, 0}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"#comment\ffoo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {1, 0}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"#comment\u0085foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {1, 0}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"#comment\u2028foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {1, 0}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"#comment\u2029foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {1, 0}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
  do_lexer_test(u8"#comment", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 8}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(multiline_non_nestable) {
  do_lexer_test(
    u8"#* This\ncomment #* doesn't *# nest",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {1, 21}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"nest", tok::word::sigil_t::none},
        {0, 4}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(multiline_nestable) {
  do_lexer_test(
    u8"#+ This\ncomment #+ actually +# nests +# foo",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {1, 31}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"foo", tok::word::sigil_t::none},
        {0, 3}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(multiline_non_nestable_unterminated) {
  do_lexer_test(
    u8"#* unterminated",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 15}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::unterminated_comment>(source_metric_range{{0, 0}, {0, 15}})
    ))
  );
}

BOOST_AUTO_TEST_CASE(multiline_nestable_unterminated) {
  do_lexer_test(
    u8"#+ unterminated #+ comment +#",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 29}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::unterminated_comment>(source_metric_range{{0, 0}, {0, 29}})
    ))
  );
}

BOOST_AUTO_TEST_SUITE(bidi)
// NOLINTBEGIN(misc-misleading-bidirectional)

BOOST_AUTO_TEST_CASE(embed_basic) {
  do_lexer_test(
    u8"#a\u202Ab\u202Cc\u202Bd\u202Ce\u202Df\u202Cg\u202Eh\u202C",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 33}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(embed_nested) {
  do_lexer_test(
    u8"#a\u202Ab\u202Bc\u202Dd\u202Ce\u202Cf\u202Eg\u202Ch\u202C",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 33}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(embed_missing_close) {
  do_lexer_test(
    u8"#a\u202Ab\u202Bc\u202Cd",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 14}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{0, 2}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::lre)
    ))
  );
}

BOOST_AUTO_TEST_CASE(embed_missing_open) {
  do_lexer_test(
    u8"#a\u202Cb\u202Ac\u202Cd",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 14}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{0, 2}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::pdf)
    ))
  );
}

BOOST_AUTO_TEST_CASE(embed_lines) {
  do_lexer_test(
    u8"#+a\u202Ab\u202Cd\ne\u202Bf\u202Cg+#",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {1, 11}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(embed_lines_bad) {
  do_lexer_test(
    u8"#+a\u202Ab\nc\u202Cd+#",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {1, 7}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{0, 3}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::lre),
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{1, 1}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::pdf)
    ))
  );
}

BOOST_AUTO_TEST_CASE(isolate_basic) {
  do_lexer_test(
    u8"#a\u2066b\u2067c\u2069d\u2068e\u2069f\u2069g",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 26}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(isolate_missing_close) {
  do_lexer_test(
    u8"#a\u2066b\u2067c\u2069d",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 14}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{0, 2}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::lri)
    ))
  );
}

BOOST_AUTO_TEST_CASE(isolate_missing_open) {
  do_lexer_test(
    u8"#a\u2069b\u2066c\u2069d",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 14}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{0, 2}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::pdi)
    ))
  );
}

BOOST_AUTO_TEST_CASE(isolate_lines) {
  do_lexer_test(
    u8"#+a\u2066b\u2069d\ne\u2067f\u2069g+#",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {1, 11}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(isolate_lines_bad) {
  do_lexer_test(
    u8"#+a\u2066b\nc\u2069d+#",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {1, 7}, {0, 0}, {0, 0}, {0, 0}}
    },
    std::make_unique<token_diagnostics>(source_metric{0, 0}, diags(
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{0, 3}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::lri),
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{1, 1}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::pdi)
    ))
  );
}

BOOST_AUTO_TEST_CASE(embed_inside_isolate) {
  do_lexer_test(
    u8"#\u2066\u2067\u202A\u202B\u2069\u202C\u2069",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 22}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(isolate_inside_embed) {
  do_lexer_test(
    u8"#\u202A\u202B\u2066\u202C\u2069\u202C\u202C",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 22}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(nested_comments) {
  do_lexer_test(
    u8"#+\u202A#+\u202C+#+#",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 14}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
  do_lexer_test(
    u8"#+#+\u2066+#\u2069+#",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::comment{}, {0, 14}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

// NOLINTEND(misc-misleading-bidirectional)
BOOST_AUTO_TEST_SUITE_END() // bidi

BOOST_AUTO_TEST_CASE(mixed) {
  do_lexer_test(
    u8"foo # bar\n #+ qux +# #* abc *# xyz",
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {1, 0}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"xyz", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_SUITE_END() // gaps

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Token combinations

BOOST_AUTO_TEST_SUITE(token_combo)

BOOST_AUTO_TEST_CASE(kw_string) {
  do_lexer_test(u8"foo\"bar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::string_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(kw_char) {
  do_lexer_test(u8"foo'bar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::char_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(kw_punct) {
  do_lexer_test(u8"foo.", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(kw_comment) {
  do_lexer_test(u8"foo#bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(id_string) {
  do_lexer_test(u8"$foo\"bar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::dollar}, {0, 4}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::string_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(id_char) {
  do_lexer_test(u8"$foo'bar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::dollar}, {0, 4}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::char_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(id_punct) {
  do_lexer_test(u8"$foo.", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::dollar}, {0, 4}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(id_comment) {
  do_lexer_test(u8"$foo#bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::dollar}, {0, 4}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(ext_string) {
  do_lexer_test(u8"_foo\"bar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::underscore}, {0, 4}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::string_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(ext_char) {
  do_lexer_test(u8"_foo'bar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::underscore}, {0, 4}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::char_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(ext_punct) {
  do_lexer_test(u8"_foo.", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::underscore}, {0, 4}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(ext_comment) {
  do_lexer_test(u8"_foo#bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::word{u8"foo", tok::word::sigil_t::underscore}, {0, 4}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(num_string) {
  do_lexer_test(u8"179\"bar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::string_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(num_char) {
  do_lexer_test(u8"179'bar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::char_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(num_punct) {
  do_lexer_test(u8"179:", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::colon{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(num_comment) {
  do_lexer_test(u8"179#bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::int_literal{179_big, std::nullopt}, {0, 3}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(string_kw) {
  do_lexer_test(u8"\"foo\"bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(string_id) {
  do_lexer_test(u8"\"foo\"$bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::dollar}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(string_ext) {
  do_lexer_test(u8"\"foo\"_bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::underscore}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(string_string) {
  do_lexer_test(u8"\"foo\"\"bar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::string_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(string_char) {
  do_lexer_test(u8"\"foo\"'bar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::char_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(string_punct) {
  do_lexer_test(u8"\"foo\".", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(string_comment) {
  do_lexer_test(u8"\"foo\"#bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::string_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(char_kw) {
  do_lexer_test(u8"'foo'bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::none},
      {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(char_id) {
  do_lexer_test(u8"'foo'$bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::dollar},
      {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(char_ext) {
  do_lexer_test(u8"'foo'_bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::underscore},
      {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(char_string) {
  do_lexer_test(u8"'foo'\"bar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::string_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(char_char) {
  do_lexer_test(u8"'foo''bar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::char_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(char_punct) {
  do_lexer_test(u8"'foo'.", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(char_comment) {
  do_lexer_test(u8"'foo'#bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::char_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(punct_kw) {
  do_lexer_test(u8".foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(punct_id) {
  do_lexer_test(u8".$foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::dollar}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(punct_ext) {
  do_lexer_test(u8"._foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"foo", tok::word::sigil_t::underscore}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(punct_num) {
  do_lexer_test(u8":179", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::colon{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::int_literal{179_big, std::nullopt}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(punct_string) {
  do_lexer_test(u8".\"foo\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::string_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(punct_char) {
  do_lexer_test(u8".'foo'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::char_literal{u8"foo"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(punct_comment) {
  do_lexer_test(u8".#foo", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comment_kw) {
  do_lexer_test(u8"#*foo*#bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comment_id) {
  do_lexer_test(u8"#*foo*#$bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::dollar}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comment_ext) {
  do_lexer_test(u8"#*foo*#_bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::word{u8"bar", tok::word::sigil_t::underscore}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comment_num) {
  do_lexer_test(u8"#*foo*#179", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::int_literal{179_big, std::nullopt}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comment_string) {
  do_lexer_test(u8"#*foo*#\"bar\"", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::string_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comment_char) {
  do_lexer_test(u8"#*foo*#'bar'", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::char_literal{u8"bar"}, {0, 5}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comment_punct) {
  do_lexer_test(u8"#*foo*#.", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::point{}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_CASE(comment_comment) {
  do_lexer_test(u8"#*foo*##bar", fg{{0, 0}, {0, 0}, {0, 0}}, std::array{
    nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}},
    nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
  });
}

BOOST_AUTO_TEST_SUITE_END() // token_combo

BOOST_AUTO_TEST_SUITE(metric_modes)

BOOST_AUTO_TEST_CASE(utf8_full) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 10}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 9}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf8,
      nycleus2::metric_line_term::full
    }
  );
}

BOOST_AUTO_TEST_CASE(utf16_full) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf16,
      nycleus2::metric_line_term::full
    }
  );
}

BOOST_AUTO_TEST_CASE(utf32_full) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf32,
      nycleus2::metric_line_term::full
    }
  );
}

BOOST_AUTO_TEST_CASE(utf8_basic) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 3}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 10}, {0, 3}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 9}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf8,
      nycleus2::metric_line_term::basic
    }
  );
}

BOOST_AUTO_TEST_CASE(utf16_basic) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf16,
      nycleus2::metric_line_term::basic
    }
  );
}

BOOST_AUTO_TEST_CASE(utf32_basic) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf32,
      nycleus2::metric_line_term::basic
    }
  );
}

BOOST_AUTO_TEST_CASE(utf8_lsp) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 3}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 10}, {0, 3}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 9}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf8,
      nycleus2::metric_line_term::lsp
    }
  );
}

BOOST_AUTO_TEST_CASE(utf16_lsp) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf16,
      nycleus2::metric_line_term::lsp
    }
  );
}

BOOST_AUTO_TEST_CASE(utf32_lsp) {
  do_lexer_test(u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲", fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
    }, {}, nycleus2::metric_mode{
      nycleus2::metric_encoding::utf32,
      nycleus2::metric_line_term::lsp
    }
  );
}

BOOST_AUTO_TEST_SUITE_END() // metric_modes

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Incremental lexing

#if NYCLEUS_SOURCE_FILE_NODE_SIZE_BASE == 11

BOOST_AUTO_TEST_SUITE(incremental)

BOOST_AUTO_TEST_CASE(unchanged) {
  auto const make_diag_nodes{[&] {
    auto node{std::make_unique<token_diagnostics>(source_metric{1, 0})};
    node->diags = diags(std::make_unique<diag::string_newline>(
      source_metric_range{{0, 4}, {1, 0}},
      diag::string_newline::newline_code_t::str_lf));
    return node;
  }};

  do_lexer_test_incr(
    std::array{
      pn{u8" foo bar\n\"baz\nqux\"", {{1, false}, {4, true}, {5, false},
        {8, true}, {9, false}, {18, false}}, false}
    },
    std::span<d>{},
    std::array{
      pn{u8" foo bar\n\"baz\nqux\"", {{1, false}, {4, true}, {5, false},
        {8, true}, {9, false}, {18, false}}, false}
    },
    f,
    fg{{0, 1}, {0, 1}, {2, 4}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 4}, {2, 4}},
      nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {1, 4}},
      nt{tok::string_literal{u8"baz\nqux"}, {1, 4}, {0, 0}, {1, 4}, {0, 0}}
    },
    make_diag_nodes(),
    make_diag_nodes()
  );
}

BOOST_AUTO_TEST_CASE(unchanged_many_nodes) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo3456789", {{0, false}, {10, true}}, false},
      pn{u8" bar456789", {{1, false}, {10, true}}, false},
      pn{u8" \"ba456789", {{1, false}}, false},
      pn{u8"z12345678\"", {{10, false}}, true},
      pn{u8" 123456789", {{1, false}, {10, true}}, false}
    },
    std::span<d>{},
    std::array{
      pn{u8"foo3456789", {{0, false}, {10, true}}, false},
      pn{u8" bar456789", {{1, false}, {10, true}}, false},
      pn{u8" \"ba456789", {{1, false}}, false},
      pn{u8"z12345678\"", {{10, false}}, true},
      pn{u8" 123456789", {{1, false}, {10, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 50}},
    std::array{
      nt{tok::word{u8"foo3456789", tok::word::sigil_t::none},
        {0, 10}, {0, 1}, {0, 11}, {0, 39}},
      nt{tok::word{u8"bar456789", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 29}},
      nt{tok::string_literal{u8"ba456789z12345678"}, {0, 19}, {0, 1}, {0, 20}, {0, 9}},
      nt{tok::int_literal{123456789, std::nullopt}, {0, 9}, {0, 0}, {0, 9}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(one_delta) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo bar", {{0, false}, {3, true}, {4, false}, {7, true}}, false}
    },
    std::array{
      d{{0, 5}, {0, 1}, u8"xyz"}
    },
    std::array{
      pn{u8"foo bxyzr", {{0, false}, {3, true}, {4, false}, {9, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 4}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 4}, {0, 0}},
      nt{tok::word{u8"bxyzr", tok::word::sigil_t::none}, {0, 5}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(many_deltas) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo bar baz", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}}, false}
    },
    std::array{
      d{{0, 2}, {0, 3}, u8"abcde"},
      d{{0, 9}, {0, 1}, u8"xyz"}
    },
    std::array{
      pn{u8"foabcdear bxyzz", {{0, false}, {9, true}, {10, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foabcdear", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 8}, {0, 0}},
      nt{tok::word{u8"bxyzz", tok::word::sigil_t::none}, {0, 5}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(multiline_deltas) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\nbar\nbaz", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}}, false}
    },
    std::array{
      d{{0, 1}, {2, 2}, u8"abc\n xyz"}
    },
    std::array{
      pn{u8"fabc\n xyzz", {{0, false}, {4, true}, {6, false}, {10, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fabc", tok::word::sigil_t::none}, {0, 4}, {1, 1}, {0, 1}, {0, 0}},
      nt{tok::word{u8"xyzz", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {2, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_SUITE(insertion)

BOOST_AUTO_TEST_CASE(inside_node) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"fXoo", {{0, false}, {4, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fXoo", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_boundary) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo3456789", {{0, false}}, false},
      pn{u8"bar3456789", {{10, true}}, true}
    },
    std::array{
      d{{0, 10}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"foo3456789", {{0, false}}, false},
      pn{u8"Xbar3456789", {{11, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo3456789Xbar3456789", tok::word::sigil_t::none},
        {0, 21}, {0, 0}, {0, 20}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_start) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 0}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"Xfoo", {{0, false}, {4, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"Xfoo", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"fooX", {{0, false}, {4, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooX", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(into_empty) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"X", {{0, false}, {1, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"X", tok::word::sigil_t::none}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_SUITE_END() // insertion

BOOST_AUTO_TEST_SUITE(removal)

BOOST_AUTO_TEST_CASE(inside_node) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 1}, u8""}
    },
    std::array{
      pn{u8"fo", {{0, false}, {2, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fo", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(across_nodes) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 ", {{0, false}, {9, true}}, false},
      pn{u8"baz3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 28}, u8""}
    },
    std::array{
      pn{u8"f9", {{0, false}, {2, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"f9", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 30}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(boundary_to_same_node) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 0}, {0, 2}, u8""}
    },
    std::array{
      pn{u8"o", {{0, false}, {1, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 2}, {0, 0}},
    std::array{
      nt{tok::word{u8"o", tok::word::sigil_t::none}, {0, 1}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(boundary_to_later_node) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 ", {{0, false}, {9, true}}, false},
      pn{u8"ba2345678z", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 0}, {0, 29}, u8""}
    },
    std::array{
      pn{u8"z", {{0, false}, {1, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 29}, {0, 0}},
    std::array{
      nt{tok::word{u8"z", tok::word::sigil_t::none}, {0, 1}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(same_node_to_boundary) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8""}
    },
    std::array{
      pn{u8"f", {{0, false}, {1, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"f", tok::word::sigil_t::none}, {0, 1}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(earlier_node_to_boundary) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 ", {{0, false}, {9, true}}, false},
      pn{u8"baz3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 29}, u8""}
    },
    std::array{
      pn{u8"f", {{0, false}, {1, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"f", tok::word::sigil_t::none}, {0, 1}, {0, 0}, {0, 30}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(full_node) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 ", {{0, false}, {9, true}}, false},
      pn{u8"baz3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 10}, {0, 10}, u8""}
    },
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"baz3456789", {{0, false}, {10, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 10}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 20}, {0, 10}},
      nt{tok::word{u8"baz3456789", tok::word::sigil_t::none},
        {0, 10}, {0, 0}, {0, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(many_nodes) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 ", {{0, false}, {9, true}}, false},
      pn{u8"baz345678 ", {{0, false}, {9, true}}, false},
      pn{u8"qux345678 ", {{0, false}, {9, true}}, false},
      pn{u8"abc3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 10}, {0, 30}, u8""}
    },
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"abc3456789", {{0, false}, {10, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 10}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 40}, {0, 10}},
      nt{tok::word{u8"abc3456789", tok::word::sigil_t::none},
        {0, 10}, {0, 0}, {0, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(all_nodes) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 ", {{0, false}, {9, true}}, false},
      pn{u8"baz3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 0}, {0, 30}, u8""}
    },
    std::span<pn>{},
    f,
    fg{{0, 0}, {0, 30}, {0, 0}},
    std::span<nt>{}
  );
}

BOOST_AUTO_TEST_CASE(at_start) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8""}
    },
    std::array{
      pn{u8"oo", {{0, false}, {2, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 1}, {0, 0}},
    std::array{
      nt{tok::word{u8"oo", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 2}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 2}, {0, 1}, u8""}
    },
    std::array{
      pn{u8"fo", {{0, false}, {2, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fo", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_SUITE_END() // removal

BOOST_AUTO_TEST_SUITE(null)

BOOST_AUTO_TEST_CASE(inside_node) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 0}, u8""}
    },
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_boundary) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo3456789", {{0, false}}, false},
      pn{u8"bar3456789", {{10, true}}, true}
    },
    std::array{
      d{{0, 10}, {0, 0}, u8""}
    },
    std::array{
      pn{u8"foo3456789", {{0, false}}, false},
      pn{u8"bar3456789", {{10, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo3456789bar3456789", tok::word::sigil_t::none},
        {0, 20}, {0, 0}, {0, 20}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_start) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 0}, {0, 0}, u8""}
    },
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 3}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8""}
    },
    std::array{
      pn{u8"foo", {{0, false}, {3, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(inside_empty) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8""}
    },
    std::span<pn>{},
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::span<nt>{}
  );
}

BOOST_AUTO_TEST_SUITE_END() // null

BOOST_AUTO_TEST_SUITE(back_to_back)

BOOST_AUTO_TEST_CASE(mod_mod) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8"XY"},
      d{{0, 3}, {0, 2}, u8"ZW"}
    },
    std::array{
      pn{u8"fXYZWr", {{0, false}, {6, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fXYZWr", tok::word::sigil_t::none}, {0, 6}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(mod_ins) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8"XY"},
      d{{0, 3}, {0, 0}, u8"ZW"}
    },
    std::array{
      pn{u8"fXYZWbar", {{0, false}, {8, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fXYZWbar", tok::word::sigil_t::none}, {0, 8}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(mod_rem) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8"XY"},
      d{{0, 3}, {0, 2}, u8""}
    },
    std::array{
      pn{u8"fXYr", {{0, false}, {4, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fXYr", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(mod_nul) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8"XY"},
      d{{0, 3}, {0, 0}, u8""}
    },
    std::array{
      pn{u8"fXYbar", {{0, false}, {6, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fXYbar", tok::word::sigil_t::none}, {0, 6}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(ins_mod) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8"XY"},
      d{{0, 3}, {0, 2}, u8"ZW"}
    },
    std::array{
      pn{u8"fooXYZWr", {{0, false}, {8, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooXYZWr", tok::word::sigil_t::none}, {0, 8}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(ins_ins) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8"XY"},
      d{{0, 3}, {0, 0}, u8"ZW"}
    },
    std::array{
      pn{u8"fooXYZWbar", {{0, false}, {10, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooXYZWbar", tok::word::sigil_t::none}, {0, 10}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(ins_rem) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8"XY"},
      d{{0, 3}, {0, 2}, u8""}
    },
    std::array{
      pn{u8"fooXYr", {{0, false}, {6, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooXYr", tok::word::sigil_t::none}, {0, 6}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(ins_nul) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8"XY"},
      d{{0, 3}, {0, 0}, u8""}
    },
    std::array{
      pn{u8"fooXYbar", {{0, false}, {8, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooXYbar", tok::word::sigil_t::none}, {0, 8}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(rem_mod) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8""},
      d{{0, 3}, {0, 2}, u8"ZW"}
    },
    std::array{
      pn{u8"fZWr", {{0, false}, {4, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fZWr", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(rem_ins) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8""},
      d{{0, 3}, {0, 0}, u8"ZW"}
    },
    std::array{
      pn{u8"fZWbar", {{0, false}, {6, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fZWbar", tok::word::sigil_t::none}, {0, 6}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(rem_rem) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8""},
      d{{0, 3}, {0, 2}, u8""}
    },
    std::array{
      pn{u8"fr", {{0, false}, {2, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fr", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(rem_nul) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 2}, u8""},
      d{{0, 3}, {0, 0}, u8""}
    },
    std::array{
      pn{u8"fbar", {{0, false}, {4, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fbar", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(nul_mod) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8""},
      d{{0, 3}, {0, 2}, u8"ZW"}
    },
    std::array{
      pn{u8"fooZWr", {{0, false}, {6, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooZWr", tok::word::sigil_t::none}, {0, 6}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(nul_ins) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8""},
      d{{0, 3}, {0, 0}, u8"ZW"}
    },
    std::array{
      pn{u8"fooZWbar", {{0, false}, {8, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooZWbar", tok::word::sigil_t::none}, {0, 8}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(nul_rem) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8""},
      d{{0, 3}, {0, 2}, u8""}
    },
    std::array{
      pn{u8"foor", {{0, false}, {4, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foor", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(nul_nul) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8""},
      d{{0, 3}, {0, 0}, u8""}
    },
    std::array{
      pn{u8"foobar", {{0, false}, {6, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foobar", tok::word::sigil_t::none}, {0, 6}, {0, 0}, {0, 6}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_SUITE_END() // back_to_back

BOOST_AUTO_TEST_SUITE(line_breaks)

BOOST_AUTO_TEST_CASE(change) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\nbar\nbaz", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}}, false}
    },
    std::array{
      d{{0, 2}, {2, 1}, u8"XY\nZW"}
    },
    std::array{
      pn{u8"foXY\nZWaz", {{0, false}, {4, true}, {5, false}, {9, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXY", tok::word::sigil_t::none}, {0, 4}, {1, 0}, {0, 2}, {0, 0}},
      nt{tok::word{u8"ZWaz", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {2, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_of_replacement) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\nbar\nbaz", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}}, false}
    },
    std::array{
      d{{0, 2}, {2, 1}, u8"XY\n"}
    },
    std::array{
      pn{u8"foXY\naz", {{0, false}, {4, true}, {5, false}, {7, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXY", tok::word::sigil_t::none}, {0, 4}, {1, 0}, {2, 1}, {0, 0}},
      nt{tok::word{u8"az", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 2}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_after) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\n", {{0, false}, {3, true}, {4, false}, {9, true}}, false},
      pn{u8"qux3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 2}, {2, 2}, u8"XY"}
    },
    std::array{
      pn{u8"foXYx3456789", {{0, false}, {12, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXYx3456789", tok::word::sigil_t::none},
        {0, 12}, {0, 0}, {2, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_exact) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\n", {{0, false}, {3, true}, {4, false}, {9, true}}, false},
      pn{u8"qux3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 2}, {2, 0}, u8"XY"}
    },
    std::array{
      pn{u8"foXYqux3456789", {{0, false}, {14, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXYqux3456789", tok::word::sigil_t::none},
        {0, 14}, {0, 0}, {2, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_last_line) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\n", {{0, false}, {3, true}, {4, false}, {9, true}}, false},
      pn{u8"qux3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 2}, {1, 1}, u8"XY"}
    },
    std::array{
      pn{u8"foXYaz78\nqux3456789", {{0, false}, {8, true}, {9, false}, {19, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXYaz78", tok::word::sigil_t::none}, {0, 8}, {1, 0}, {2, 0}, {0, 10}},
      nt{tok::word{u8"qux3456789", tok::word::sigil_t::none},
        {0, 10}, {0, 0}, {0, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_before) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\n", {{0, false}, {3, true}, {4, false}, {9, true}}, false},
      pn{u8"qux3456789", {{0, false}, {10, true}}, false}
    },
    std::array{
      d{{0, 2}, {0, 10}, u8"XY"}
    },
    std::array{
      pn{u8"foXYr\nbaz78\n", {{0, false}, {5, true}, {6, false}, {11, true}}, false},
      pn{u8"qux3456789", {{0, false}, {10, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXYr", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {1, 0}, {1, 10}},
      nt{tok::word{u8"baz78", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {1, 0}, {0, 10}},
      nt{tok::word{u8"qux3456789", tok::word::sigil_t::none}, {0, 10}, {0, 0}, {0, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_of_text_exact) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\n", {{0, false}, {3, true}, {4, false}, {9, true}}, false}
    },
    std::array{
      d{{0, 2}, {2, 0}, u8"XY"}
    },
    std::array{
      pn{u8"foXY", {{0, false}, {4, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXY", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {2, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_of_text_last_line) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\n", {{0, false}, {3, true}, {4, false}, {9, true}}, false}
    },
    std::array{
      d{{0, 2}, {1, 1}, u8"XY"}
    },
    std::array{
      pn{u8"foXYaz78\n", {{0, false}, {8, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXYaz78", tok::word::sigil_t::none}, {0, 8}, {1, 0}, {2, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_of_text_before) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\n", {{0, false}, {3, true}, {4, false}, {9, true}}, false}
    },
    std::array{
      d{{0, 2}, {0, 10}, u8"XY"}
    },
    std::array{
      pn{u8"foXYr\nbaz78\n", {{0, false}, {5, true}, {6, false}, {11, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foXYr", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {1, 0}, {1, 0}},
      nt{tok::word{u8"baz78", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {1, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(at_end_of_text_append) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\n", {{0, false}, {3, true}, {4, false}, {9, true}}, false}
    },
    std::array{
      d{{2, 0}, {0, 0}, u8"qux"}
    },
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78\nqux", {{0, false}, {3, true}, {4, false}, {9, true},
        {10, false}, {13, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {2, 0}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {2, 0}},
      nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {1, 0}},
      nt{tok::word{u8"baz78", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {1, 0}, {0, 0}},
      nt{tok::word{u8"qux", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_SUITE_END() // line_breaks

BOOST_AUTO_TEST_SUITE(crlf)

BOOST_AUTO_TEST_CASE(unchanged) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\r\nbar", {{0, false}, {3, true}, {5, false}, {8, true}}, false}
    },
    std::span<d>{},
    std::array{
      pn{u8"foo\r\nbar", {{0, false}, {3, true}, {5, false}, {8, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {1, 3}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {0, 3}},
      nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(insert_crlf) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo bar", {{0, false}, {3, true}, {4, false}, {7, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 1}, u8"X\r\nY"}
    },
    std::array{
      pn{u8"fooX\r\nYbar", {{0, false}, {4, true}, {6, false}, {10, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooX", tok::word::sigil_t::none}, {0, 4}, {1, 0}, {0, 3}, {0, 0}},
      nt{tok::word{u8"Ybar", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 4}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(remove_crlf) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\r\nbar", {{0, false}, {3, true}, {5, false}, {8, true}}, false}
    },
    std::array{
      d{{0, 3}, {1, 0}, u8"X Y"}
    },
    std::array{
      pn{u8"fooX Ybar", {{0, false}, {4, true}, {5, false}, {9, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooX", tok::word::sigil_t::none}, {0, 4}, {0, 1}, {0, 3}, {0, 0}},
      nt{tok::word{u8"Ybar", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {1, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(replace_crlf) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\r\nbar", {{0, false}, {3, true}, {5, false}, {8, true}}, false}
    },
    std::array{
      d{{0, 3}, {1, 0}, u8"X\r\nqux\r\nY"}
    },
    std::array{
      pn{u8"fooX\r\nqux\r\nYbar", {{0, false}, {4, true}, {6, false}, {9, true},
        {11, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooX", tok::word::sigil_t::none}, {0, 4}, {1, 0}, {0, 3}, {0, 0}},
      nt{tok::word{u8"qux", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"Ybar", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {1, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(insert_into_crlf_break) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\r\nbar", {{0, false}, {3, true}, {5, false}, {8, true}}, false}
    },
    std::array{
      d{{0, 4}, {0, 0}, u8"qux"}
    },
    std::array{
      pn{u8"foo\rqux\nbar", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 3}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 4}, {0, 0}},
      nt{tok::word{u8"qux", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {0, 3}},
      nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(insert_into_crlf_keep) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\r\nbar", {{0, false}, {3, true}, {5, false}, {8, true}}, false}
    },
    std::array{
      d{{0, 4}, {0, 0}, u8"\nqux\r"}
    },
    std::array{
      pn{u8"foo\r\nqux\r\nbar", {{0, false}, {3, true}, {5, false}, {8, true},
        {10, false}, {13, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 3}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 4}, {0, 0}},
      nt{tok::word{u8"qux", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {0, 3}},
      nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(insert_after_cr) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\rbar", {{0, false}, {3, true}, {4, false}, {7, true}}, false}
    },
    std::array{
      d{{1, 0}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"foo\rXbar", {{0, false}, {3, true}, {4, false}, {8, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 3}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {0, 0}},
      nt{tok::word{u8"Xbar", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(insert_lf_after_cr) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\rbar", {{0, false}, {3, true}, {4, false}, {7, true}}, false}
    },
    std::array{
      d{{1, 0}, {0, 0}, u8"\nX"}
    },
    std::array{
      pn{u8"foo\r\nXbar", {{0, false}, {3, true}, {5, false}, {9, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 3}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {0, 0}},
      nt{tok::word{u8"Xbar", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(insert_before_lf) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\nbar", {{0, false}, {3, true}, {4, false}, {7, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"fooX\nbar", {{0, false}, {4, true}, {5, false}, {8, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooX", tok::word::sigil_t::none}, {0, 4}, {1, 0}, {1, 0}, {0, 3}},
      nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(insert_cr_before_lf) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\nbar", {{0, false}, {3, true}, {4, false}, {7, true}}, false}
    },
    std::array{
      d{{0, 3}, {0, 0}, u8"X\r"}
    },
    std::array{
      pn{u8"fooX\r\nbar", {{0, false}, {4, true}, {6, false}, {9, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"fooX", tok::word::sigil_t::none}, {0, 4}, {1, 0}, {1, 0}, {0, 3}},
      nt{tok::word{u8"bar", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(append_after_cr) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\r", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{1, 0}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"foo\rX", {{0, false}, {3, true}, {4, false}, {5, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 3}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {0, 0}},
      nt{tok::word{u8"X", tok::word::sigil_t::none}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(append_lf_after_cr) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo\r", {{0, false}, {3, true}}, false}
    },
    std::array{
      d{{1, 0}, {0, 0}, u8"\nX"}
    },
    std::array{
      pn{u8"foo\r\nX", {{0, false}, {3, true}, {5, false}, {6, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 3}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {1, 0}, {0, 0}},
      nt{tok::word{u8"X", tok::word::sigil_t::none}, {0, 1}, {0, 0}, {0, 0}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(prepend_before_lf) {
  do_lexer_test_incr(
    std::array{
      pn{u8"\nfoo", {{1, false}, {4, true}}, false}
    },
    std::array{
      d{{0, 0}, {0, 0}, u8"X"}
    },
    std::array{
      pn{u8"X\nfoo", {{0, false}, {1, true}, {2, false}, {5, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"X", tok::word::sigil_t::none}, {0, 1}, {1, 0}, {1, 0}, {0, 3}},
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(prepend_cr_before_lf) {
  do_lexer_test_incr(
    std::array{
      pn{u8"\nfoo", {{1, false}, {4, true}}, false}
    },
    std::array{
      d{{0, 0}, {0, 0}, u8"X\r"}
    },
    std::array{
      pn{u8"X\r\nfoo", {{0, false}, {1, true}, {3, false}, {6, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"X", tok::word::sigil_t::none}, {0, 1}, {1, 0}, {1, 0}, {0, 3}},
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 3}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_SUITE_END() // crlf

BOOST_AUTO_TEST_SUITE(nodes)

BOOST_AUTO_TEST_CASE(underflow_start_one) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 ", {{0, false}, {9, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 7}, u8""}
    },
    std::array{
      pn{u8"f8 bar345678 ", {{0, false}, {2, true}, {3, false}, {12, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"f8", tok::word::sigil_t::none}, {0, 2}, {0, 1}, {0, 10}, {0, 10}},
      nt{tok::word{u8"bar345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(underflow_start_two) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 qux345678 ab", {{0, false}, {9, true}, {10, false}, {19, true},
        {20, false}, {22, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 7}, u8""}
    },
    std::array{
      pn{u8"f8 bar345678", {{0, false}, {2, true}, {3, false}, {12, true}}, false},
      pn{u8" qux345678 ab", {{1, false}, {10, true}, {11, false}, {13, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"f8", tok::word::sigil_t::none}, {0, 2}, {0, 1}, {0, 10}, {0, 22}},
      nt{tok::word{u8"bar345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 12}},
      nt{tok::word{u8"qux345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 2}},
      nt{tok::word{u8"ab", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 2}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(underflow_middle_one) {
  do_lexer_test_incr(
    std::array{
      pn{u8"xyz345678 ", {{0, false}, {9, true}}, false},
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 ", {{0, false}, {9, true}}, false}
    },
    std::array{
      d{{0, 11}, {0, 7}, u8""}
    },
    std::array{
      pn{u8"xyz345678 ", {{0, false}, {9, true}}, false},
      pn{u8"f8 bar345678 ", {{0, false}, {2, true}, {3, false}, {12, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 10}},
    std::array{
      nt{tok::word{u8"xyz345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 0}},
      nt{tok::word{u8"f8", tok::word::sigil_t::none}, {0, 2}, {0, 1}, {0, 10}, {0, 10}},
      nt{tok::word{u8"bar345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(underflow_middle_two) {
  do_lexer_test_incr(
    std::array{
      pn{u8"xyz345678 ", {{0, false}, {9, true}}, false},
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar345678 qux345678 ab", {{0, false}, {9, true}, {10, false}, {19, true},
        {20, false}, {22, true}}, false}
    },
    std::array{
      d{{0, 11}, {0, 7}, u8""}
    },
    std::array{
      pn{u8"xyz345678 ", {{0, false}, {9, true}}, false},
      pn{u8"f8 bar345678", {{0, false}, {2, true}, {3, false}, {12, true}}, false},
      pn{u8" qux345678 ab", {{1, false}, {10, true}, {11, false}, {13, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 10}},
    std::array{
      nt{tok::word{u8"xyz345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 0}},
      nt{tok::word{u8"f8", tok::word::sigil_t::none}, {0, 2}, {0, 1}, {0, 10}, {0, 22}},
      nt{tok::word{u8"bar345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 12}},
      nt{tok::word{u8"qux345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 2}},
      nt{tok::word{u8"ab", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 2}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(underflow_end_one) {
  do_lexer_test_incr(
    std::array{
      pn{u8"xyz345678 ", {{0, false}, {9, true}}, false},
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false}
    },
    std::array{
      d{{0, 11}, {0, 7}, u8""}
    },
    std::array{
      pn{u8"xyz345678 f8 ", {{0, false}, {9, true}, {10, false}, {12, true}}, false},
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 10}},
    std::array{
      nt{tok::word{u8"xyz345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 0}},
      nt{tok::word{u8"f8", tok::word::sigil_t::none}, {0, 2}, {0, 1}, {0, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(underflow_end_two) {
  do_lexer_test_incr(
    std::array{
      pn{u8"xyz345678 qux34567890 ", {{0, false}, {9, true}, {10, false}, {21, true}}, false},
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false}
    },
    std::array{
      d{{0, 23}, {0, 7}, u8""}
    },
    std::array{
      pn{u8"xyz345678 qu", {{0, false}, {9, true}, {10, false}}, false},
      pn{u8"x34567890 f8 ", {{9, true}, {10, false}, {12, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 22}},
    std::array{
      nt{tok::word{u8"xyz345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 10}, {0, 12}},
      nt{tok::word{u8"qux34567890", tok::word::sigil_t::none},
        {0, 11}, {0, 1}, {0, 12}, {0, 0}},
      nt{tok::word{u8"f8", tok::word::sigil_t::none}, {0, 2}, {0, 1}, {0, 10}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(underflow_only) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo34567890 ", {{0, false}, {11, true}}, false}
    },
    std::array{
      d{{0, 1}, {0, 10}, u8""}
    },
    std::array{
      pn{u8"f ", {{0, false}, {1, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"f", tok::word::sigil_t::none}, {0, 1}, {0, 1}, {0, 12}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(one_node) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo34567890 ", {{0, false}, {11, true}}, false},
      pn{u8"bar345678901", {{0, false}, {12, true}}, false}
    },
    std::array{
      d{{0, 9}, {0, 6}, u8""}
    },
    std::array{
      pn{u8"foo345678345678901", {{0, false}, {18, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345678345678901", tok::word::sigil_t::none},
        {0, 18}, {0, 0}, {0, 24}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(two_nodes) {
  do_lexer_test_incr(
    std::array{
      pn{u8"foo34567890 ", {{0, false}, {11, true}}, false},
      pn{u8"bar345678901", {{0, false}, {12, true}}, false}
    },
    std::array{
      d{{0, 11}, {0, 2}, u8"XY"}
    },
    std::array{
      pn{u8"foo34567890X", {{0, false}}, false},
      pn{u8"Yar345678901", {{12, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo34567890XYar345678901", tok::word::sigil_t::none},
        {0, 24}, {0, 0}, {0, 24}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(two_nodes_multibyte_1) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo345\U0001D538\U0001D539ar3456789"}
    },
    std::array{
      pn{u8"foo345\U0001D538", {{0, false}}, false},
      pn{u8"\U0001D539ar3456789", {{13, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345\U0001D538\U0001D539ar3456789", tok::word::sigil_t::none},
        {0, 23}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(two_nodes_multibyte_2) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo3456\U0001D538\U0001D539r3456789"}
    },
    std::array{
      pn{u8"foo3456\U0001D538", {{0, false}}, false},
      pn{u8"\U0001D539r3456789", {{12, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo3456\U0001D538\U0001D539r3456789", tok::word::sigil_t::none},
        {0, 23}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(two_nodes_multibyte_3) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo34567\U0001D538\U0001D5393456789"}
    },
    std::array{
      pn{u8"foo34567\U0001D538", {{0, false}}, false},
      pn{u8"\U0001D5393456789", {{11, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo34567\U0001D538\U0001D5393456789", tok::word::sigil_t::none},
        {0, 23}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(two_nodes_multibyte_4) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo345678\U0001D538\U0001D539456789"}
    },
    std::array{
      pn{u8"foo345678\U0001D538", {{0, false}}, false},
      pn{u8"\U0001D539456789", {{10, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345678\U0001D538\U0001D539456789", tok::word::sigil_t::none},
        {0, 23}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(three_nodes_exact) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo345678 bar345678 qux345678 abc345678 xy"}
    },
    std::array{
      pn{u8"foo345678 bar", {{0, false}, {9, true}, {10, false}}, false},
      pn{u8"345678 qux345", {{6, true}, {7, false}}, true},
      pn{u8"678 abc345678 xy", {{3, true}, {4, false}, {13, true},
        {14, false}, {16, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"bar345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"qux345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"abc345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"xy", tok::word::sigil_t::none}, {0, 2}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(three_nodes_round) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo345678 bar345678 qux345678 abc345678 xyz"}
    },
    std::array{
      pn{u8"foo345678 bar3", {{0, false}, {9, true}, {10, false}}, false},
      pn{u8"45678 qux3456", {{5, true}, {6, false}}, true},
      pn{u8"78 abc345678 xyz", {{2, true}, {3, false}, {12, true},
        {13, false}, {16, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"bar345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"qux345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"abc345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"xyz", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(three_nodes_multibyte) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo345678 bar\U0001D53878 qux345678 abc345678 xyz"}
    },
    std::array{
      pn{u8"foo345678 bar\U0001D538", {{0, false}, {9, true}, {10, false}}, false},
      pn{u8"78 qux3456", {{2, true}, {3, false}}, true},
      pn{u8"78 abc345678 xyz", {{2, true}, {3, false}, {12, true},
        {13, false}, {16, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"bar\U0001D53878", tok::word::sigil_t::none},
        {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"qux345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"abc345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"xyz", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(four_nodes_exact) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo345678 bar345678 qux345678 abc345678 xyz345678 def345678 uvw"}
    },
    std::array{
      pn{u8"foo345678 bar34", {{0, false}, {9, true}, {10, false}}, false},
      pn{u8"5678 qux345678 ", {{4, true}, {5, false}, {14, true}}, true},
      pn{u8"abc345678 xyz34", {{0, false}, {9, true}, {10, false}}, false},
      pn{u8"5678 def345678 uvw", {{4, true}, {5, false}, {14, true},
        {15, false}, {18, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"bar345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"qux345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"abc345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"xyz345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"def345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"uvw", tok::word::sigil_t::none}, {0, 3}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(four_nodes_round) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1}, u8"foo345678 bar345678 qux345678 abc345678 xyz345678 def345678 uvw3"}
    },
    std::array{
      pn{u8"foo345678 bar345", {{0, false}, {9, true}, {10, false}}, false},
      pn{u8"678 qux345678 a", {{3, true}, {4, false}, {13, true}, {14, false}}, true},
      pn{u8"bc345678 xyz345", {{8, true}, {9, false}}, true},
      pn{u8"678 def345678 uvw3", {{3, true}, {4, false}, {13, true},
        {14, false}, {18, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"bar345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"qux345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"abc345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"xyz345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"def345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"uvw3", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_CASE(four_nodes_multibyte) {
  do_lexer_test_incr(
    std::array{
      pn{u8" ", {}, false}
    },
    std::array{
      d{{0, 0}, {0, 1},
        u8"foo345678 bar34\U0001D538 qux345678 abc345678 xyz345678 def345678 uvw3"}
    },
    std::array{
      pn{u8"foo345678 bar34\U0001D538", {{0, false}, {9, true},
        {10, false}, {19, true}}, false},
      pn{u8" qux345678 a", {{1, false}, {10, true}, {11, false}}, false},
      pn{u8"bc345678 xyz345", {{8, true}, {9, false}}, true},
      pn{u8"678 def345678 uvw3", {{3, true}, {4, false}, {13, true},
        {14, false}, {18, true}}, true}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"bar34\U0001D538", tok::word::sigil_t::none},
        {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"qux345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"abc345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"xyz345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"def345678", tok::word::sigil_t::none}, {0, 9}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"uvw3", tok::word::sigil_t::none}, {0, 4}, {0, 0}, {0, 1}, {0, 0}}
    }
  );
}

BOOST_AUTO_TEST_SUITE_END() // nodes

BOOST_AUTO_TEST_CASE(diag_deltas) {
  auto initial_diags{std::make_unique<token_diagnostics>(source_metric{0, 0})};
  initial_diags->diags = diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 2}})
  );

  initial_diags->next = std::make_unique<token_diagnostics>(source_metric{0, 5});
  initial_diags->next->diags = diags(
    std::make_unique<diag::num_literal_base_prefix_and_suffix>(
      source_metric_range{{0, 0}, {0, 6}})
  );

  initial_diags->next->next = std::make_unique<token_diagnostics>(source_metric{0, 12});
  initial_diags->next->next->diags = diags(
    std::make_unique<diag::unterminated_string>(source_metric_range{{0, 0}, {0, 4}})
  );

  auto expected_diags{std::make_unique<token_diagnostics>(source_metric{0, 0})};
  expected_diags->diags = diags(
    std::make_unique<diag::invalid_escape_code>(source_metric_range{{0, 1}, {0, 2}})
  );

  expected_diags->next = std::make_unique<token_diagnostics>(source_metric{0, 5});
  expected_diags->next->diags = diags(
    std::make_unique<diag::num_literal_suffix_zero_width>(source_metric_range{{0, 0}, {0, 5}})
  );

  expected_diags->next->next = std::make_unique<token_diagnostics>(source_metric{0, 11});
  expected_diags->next->next->diags = diags(
    std::make_unique<diag::unterminated_string>(source_metric_range{{0, 0}, {0, 4}})
  );

  do_lexer_test_incr(
    std::array{
      pn{u8"\"\\x\" 0x123h \"foo", {{0, false}, {4, false},
        {5, false}, {11, true}, {12, false}, {16, true}}, false}
    },
    std::array{
      d{{0, 5}, {0, 2}, u8""},
      d{{0, 10}, {0, 1}, u8"s0"}
    },
    std::array{
      pn{u8"\"\\x\" 123s0 \"foo", {{0, false}, {4, false}, {5, false}, {10, true},
        {11, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 5}},
    std::array{
      nt{tok::string_literal{u8"\uFFFD"}, {0, 4}, {0, 1}, {0, 7}, {0, 0}},
      nt{tok::int_literal{123, std::nullopt}, {0, 5}, {0, 1}, {0, 5}, {0, 4}},
      nt{tok::string_literal{u8"foo"}, {0, 4}, {0, 0}, {0, 4}, {0, 0}}
    },
    std::move(initial_diags),
    std::move(expected_diags)
  );
}

BOOST_AUTO_TEST_SUITE(reuse)

BOOST_AUTO_TEST_CASE(partial) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo bar baz qux", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}, {12, false}, {15, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{0, 13}, {0, 1}, u8"abc"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {0, 12}}));

  auto const reuse_result{tok_src.reuse({0, 7})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 4}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"baz", tok::word::sigil_t::none},
    {0, 3}, {0, 1}, {0, 4}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(token_result2
    == (nt{tok::word{u8"qabcx", tok::word::sigil_t::none},
    {0, 5}, {0, 0}, {0, 3}, {0, 0}}));

  auto const token_result3{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result3.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo bar baz qabcx", {{0, false}, {3, true}, {4, false}, {7, true},
      {8, false}, {11, true}, {12, false}, {17, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(to_mid_gap) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo bar  qux", {{0, false}, {3, true}, {4, false}, {7, true},
        {9, false}, {12, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{0, 8}, {0, 0}, u8"ABC"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {0, 7}}));

  auto const reuse_result{tok_src.reuse({0, 7})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"ABC", tok::word::sigil_t::none},
    {0, 3}, {0, 1}, {0, 1}, {0, 3}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(token_result2 == (nt{tok::word{u8"qux", tok::word::sigil_t::none},
    {0, 3}, {0, 0}, {0, 3}, {0, 0}}));

  auto const token_result3{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result3.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo bar ABC qux", {{0, false}, {3, true}, {4, false}, {7, true},
      {8, false}, {11, true}, {12, false}, {15, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(to_mid_token) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo bar qux", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{0, 10}, {0, 0}, u8"ABC"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {0, 8}}));

  auto const reuse_result{tok_src.reuse({0, 7})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1
    == (nt{tok::word{u8"quABCx", tok::word::sigil_t::none},
    {0, 6}, {0, 0}, {0, 3}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result2.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo bar quABCx", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {14, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(to_token_start) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo bar qux", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{0, 8}, {0, 0}, u8"ABC"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {0, 8}}));

  auto const reuse_result{tok_src.reuse({0, 7})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"ABCqux", tok::word::sigil_t::none},
    {0, 6}, {0, 0}, {0, 3}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result2.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo bar ABCqux", {{0, false}, {3, true}, {4, false}, {7, true},
      {8, false}, {14, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(to_token_end_lookahead) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo bar qux", {{0, false}, {3, true}, {4, false}, {7, true},
        {8, false}, {11, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{0, 7}, {0, 0}, u8"ABC"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {0, 4}}));

  auto const reuse_result{tok_src.reuse({0, 3})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"barABC", tok::word::sigil_t::none},
    {0, 6}, {0, 1}, {0, 4}, {0, 3}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(token_result2 == (nt{tok::word{u8"qux", tok::word::sigil_t::none},
    {0, 3}, {0, 0}, {0, 3}, {0, 0}}));

  auto const token_result3{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result3.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo barABC qux", {{0, false}, {3, true}, {4, false}, {10, true},
      {11, false}, {14, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(to_token_end_delim) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo \"bar\" qux", {{0, false}, {3, true}, {4, false}, {9, false},
        {10, false}, {13, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{0, 9}, {0, 0}, u8"ABC"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {0, 9}}));

  auto const reuse_result{tok_src.reuse({0, 9})};
  BOOST_TEST(reuse_result == (r{{0, 0}, {0, 0}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"ABC", tok::word::sigil_t::none},
    {0, 3}, {0, 1}, {0, 1}, {0, 3}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(token_result2 == (nt{tok::word{u8"qux", tok::word::sigil_t::none},
    {0, 3}, {0, 0}, {0, 3}, {0, 0}}));

  auto const token_result3{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result3.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo \"bar\"ABC qux", {{0, false}, {3, true}, {4, false}, {9, false},
      {9, false}, {12, true}, {13, false}, {16, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(to_between_tokens) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo \"bar\"qux", {{0, false}, {3, true}, {4, false}, {9, false},
        {9, false}, {12, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{0, 9}, {0, 2}, u8""}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {0, 9}}));

  auto const reuse_result{tok_src.reuse({0, 9})};
  BOOST_TEST(reuse_result == (r{{0, 0}, {0, 2}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"x", tok::word::sigil_t::none},
    {0, 1}, {0, 0}, {0, 1}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result2.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo \"bar\"x", {{0, false}, {3, true}, {4, false}, {9, false},
      {9, false}, {10, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(many_nodes) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78 ", {{0, false}, {3, true}, {4, false}, {9, true}}, false},
      pn{u8"qux3456789", {{0, false}, {10, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{1, 7}, {0, 1}, u8"XY"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {1, 6}}));

  auto const reuse_result{tok_src.reuse({1, 5})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"qXYx3456789", tok::word::sigil_t::none},
    {0, 11}, {0, 0}, {0, 10}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result2.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
    pn{u8"bar\nbaz78 ", {{0, false}, {3, true}, {4, false}, {9, true}}, false},
    pn{u8"qXYx3456789", {{0, false}, {11, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(all) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz78 ", {{0, false}, {3, true}, {4, false}, {9, true}}, false},
      pn{u8"qux3456789", {{0, false}, {10, true}}, false}
    },
    nullptr
  )};
  incremental_token_source tok_src{src, std::span<d>{}};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {1, 16}}));

  auto const reuse_result{tok_src.reuse({1, 16})};
  BOOST_TEST(reuse_result == (r{{0, 0}, {0, 0}, {0, 0}}));

  auto const token_result{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
    pn{u8"bar\nbaz78 ", {{0, false}, {3, true}, {4, false}, {9, true}}, false},
    pn{u8"qux3456789", {{0, false}, {10, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(to_node_end_delim) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\n\"baz8\"", {{0, false}, {3, true}, {4, false},
        {10, false}}, false},
      pn{u8" qux456789", {{1, false}, {10, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{1, 8}, {0, 1}, u8"XY"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {1, 7}}));

  auto const reuse_result{tok_src.reuse({1, 6})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"qXYx456789", tok::word::sigil_t::none},
    {0, 10}, {0, 0}, {0, 9}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result2.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
    pn{u8"bar\n\"baz8\"", {{0, false}, {3, true}, {4, false},
      {10, false}}, false},
    pn{u8" qXYx456789", {{1, false}, {11, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(to_node_end_lookahead) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
      pn{u8"bar\nbaz789", {{0, false}, {3, true}, {4, false}, {10, true}}, false},
      pn{u8" qux456789", {{1, false}, {10, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{1, 8}, {0, 1}, u8"XY"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {1, 7}}));

  auto const reuse_result{tok_src.reuse({1, 6})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"qXYx456789", tok::word::sigil_t::none},
    {0, 10}, {0, 0}, {0, 9}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result2.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo345678 ", {{0, false}, {9, true}}, false},
    pn{u8"bar\nbaz789", {{0, false}, {3, true}, {4, false}, {10, true}}, false},
    pn{u8" qXYx456789", {{1, false}, {11, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(multiline) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo bar\nbz qx\nabc xyz", {{0, false}, {3, true}, {4, false},
        {7, true}, {8, false}, {10, true}, {11, false}, {13, true}, {14, false},
        {17, true}, {18, false}, {21, true}}, false}
    },
    nullptr
  )};
  std::array deltas{
    d{{2, 5}, {0, 1}, u8"XY"}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {2, 4}}));

  auto const reuse_result1{tok_src.reuse({1, 2})};
  BOOST_TEST(reuse_result1 == (r{{0, 1}, {0, 1}, {1, 4}}));

  auto const reuse_result2{tok_src.reuse({1, 3})};
  BOOST_TEST(reuse_result2 == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"xXYz", tok::word::sigil_t::none},
    {0, 4}, {0, 0}, {0, 3}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result2.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo bar\nbz qx\nabc xXYz", {{0, false}, {3, true}, {4, false},
        {7, true}, {8, false}, {10, true}, {11, false}, {13, true}, {14, false},
        {17, true}, {18, false}, {22, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_CASE(with_diags) {
  auto src_diags{
    std::make_unique<token_diagnostics>(source_metric{0, 8})};
  src_diags->diags = diags(
    std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
      diag::string_newline::newline_code_t::str_lf)
  );

  src_diags->next = std::make_unique<token_diagnostics>(source_metric{1, 9});
  src_diags->next->diags = diags(
    std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{0, 3}, {0, 3}},
      diag::comment_unmatched_bidi_code::bidi_code_t::lre)
  );

  auto src{test::nycleus2::source_file_access::create(
    f,
    // NOLINTBEGIN(misc-misleading-bidirectional)
    std::array{
      pn{u8"ABC foo \"bar\nqux\" ", {{0, false}, {3, true}, {4, false},
        {7, true}, {8, false}, {17, false}}, false},
      pn{u8"XYZ #* \u202A *# abc", {{0, false}, {3, true}, {4, false}, {13, false},
        {14, false}, {17, true}}, false}
    },
    // NOLINTEND(misc-misleading-bidirectional)
    std::move(src_diags)
  )};

  std::array deltas{
    d{{0, 1}, {0, 2}, u8""},
    d{{1, 6}, {0, 2}, u8""}
  };
  incremental_token_source tok_src{src, deltas};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {0, 0}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"A", tok::word::sigil_t::none},
    {0, 1}, {0, 1}, {0, 4}, {1, 5}}));

  auto const reuse_result{tok_src.reuse({1, 4})};
  BOOST_TEST(reuse_result == (r{{0, 1}, {0, 1}, {0, 0}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(token_result2 == (nt{tok::word{u8"X", tok::word::sigil_t::none},
    {0, 1}, {0, 1}, {0, 4}, {0, 13}}));

  auto const token_result3{tok_src.next_token()};
  BOOST_TEST(token_result3 == (nt{tok::comment{}, {0, 9}, {0, 1}, {0, 10}, {0, 3}}));

  auto const token_result4{tok_src.next_token()};
  BOOST_TEST(token_result4 == (nt{tok::word{u8"abc", tok::word::sigil_t::none},
    {0, 3}, {0, 0}, {0, 3}, {0, 0}}));

  auto const token_result5{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result5.next_token));

  // NOLINTBEGIN(misc-misleading-bidirectional)
  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"A foo \"bar\nqux\" ", {{0, false}, {1, true}, {2, false},
      {5, true}, {6, false}, {15, false}}, false},
    pn{u8"X #* \u202A *# abc", {{0, false}, {1, true}, {2, false}, {11, false},
      {12, false}, {15, true}}, false}
  });
  // NOLINTEND(misc-misleading-bidirectional)

  auto const& new_diags{test::nycleus2::source_file_access::get_diags(src)};
  BOOST_TEST(new_diags);
  BOOST_TEST(new_diags->token_loc == (source_metric{0, 6}));
  {
    auto const expected_diags{diags(
      std::make_unique<diag::string_newline>(source_metric_range{{0, 4}, {1, 0}},
        diag::string_newline::newline_code_t::str_lf)
    )};
    BOOST_TEST(test::nycleus2::diagseq(new_diags->diags, expected_diags));
  }

  BOOST_TEST(new_diags->next);
  BOOST_TEST(new_diags->next->token_loc == (source_metric{1, 7}));
  {
    auto const expected_diags{diags(
      std::make_unique<diag::comment_unmatched_bidi_code>(source_metric_range{{0, 3}, {0, 3}},
        diag::comment_unmatched_bidi_code::bidi_code_t::lre)
    )};
    BOOST_TEST(test::nycleus2::diagseq(new_diags->next->diags, expected_diags));
  }

  BOOST_TEST(!new_diags->next->next);
}

BOOST_AUTO_TEST_SUITE_END() // reuse

BOOST_AUTO_TEST_SUITE(metric_modes)

BOOST_AUTO_TEST_CASE(utf8_full) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 10}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 9}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf8,
      nycleus2::metric_line_term::full
    }
  );
}

BOOST_AUTO_TEST_CASE(utf16_full) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf16,
      nycleus2::metric_line_term::full
    }
  );
}

BOOST_AUTO_TEST_CASE(utf32_full) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf32,
      nycleus2::metric_line_term::full
    }
  );
}

BOOST_AUTO_TEST_CASE(utf8_basic) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 3}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 10}, {0, 3}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 9}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf8,
      nycleus2::metric_line_term::basic
    }
  );
}

BOOST_AUTO_TEST_CASE(utf16_basic) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf16,
      nycleus2::metric_line_term::basic
    }
  );
}

BOOST_AUTO_TEST_CASE(utf32_basic) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf32,
      nycleus2::metric_line_term::basic
    }
  );
}

BOOST_AUTO_TEST_CASE(utf8_lsp) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 3}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 10}, {0, 3}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 9}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 13}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf8,
      nycleus2::metric_line_term::lsp
    }
  );
}

BOOST_AUTO_TEST_CASE(utf16_lsp) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 7}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf16,
      nycleus2::metric_line_term::lsp
    }
  );
}

BOOST_AUTO_TEST_CASE(utf32_lsp) {
  do_lexer_test_incr(
    std::span<pn>{},
    std::array{
      d{{0, 0}, {0, 0}, u8"foo\u2028идент\u2029識別符\r\n#👉🎻🐲"}
    },
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    f,
    fg{{0, 0}, {0, 0}, {0, 0}},
    std::array{
      nt{tok::word{u8"foo", tok::word::sigil_t::none}, {0, 3}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"идент", tok::word::sigil_t::none}, {0, 5}, {0, 1}, {0, 0}, {0, 0}},
      nt{tok::word{u8"識別符", tok::word::sigil_t::none}, {0, 3}, {1, 0}, {0, 0}, {0, 0}},
      nt{tok::comment{}, {0, 4}, {0, 0}, {0, 0}, {0, 0}}
    },
    {},
    {},
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf32,
      nycleus2::metric_line_term::lsp
    }
  );
}

BOOST_AUTO_TEST_CASE(utf16_basic_reuse) {
  auto src{test::nycleus2::source_file_access::create(
    f,
    std::array{
      pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
      pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
      pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
    },
    nullptr,
    nycleus2::metric_mode{
      nycleus2::metric_encoding::utf16,
      nycleus2::metric_line_term::basic
    }
  )};
  incremental_token_source tok_src{src, std::span<d>{}};

  auto const first_gap{tok_src.first_gap()};
  BOOST_TEST(first_gap == (fg{{0, 0}, {0, 0}, {1, 7}}));

  auto const token_result1{tok_src.next_token()};
  BOOST_TEST(token_result1 == (nt{tok::word{u8"foo", tok::word::sigil_t::none},
    {0, 3}, {0, 1}, {0, 4}, {1, 7}}));

  auto const reuse_result{tok_src.reuse({0, 9})};
  BOOST_TEST(reuse_result == (r{{1, 0}, {1, 0}, {0, 7}}));

  auto const token_result2{tok_src.next_token()};
  BOOST_TEST(token_result2 == (nt{tok::comment{}, {0, 7}, {0, 0}, {0, 7}, {0, 0}}));

  auto const token_result3{tok_src.next_token()};
  BOOST_TEST(std::holds_alternative<tok::eof>(token_result3.next_token));

  test::nycleus2::source_file_access::compare(src, std::array{
    pn{u8"foo\u2028иден", {{0, false}, {3, true}, {6, false}}, false},
    pn{u8"т\u2029識別符", {{2, true}, {5, false}, {14, true}}, true},
    pn{u8"\r\n#👉🎻🐲", {{2, false}, {15, true}}, false}
  });
  BOOST_TEST(test::nycleus2::source_file_access::get_diags(src) == nullptr);
}

BOOST_AUTO_TEST_SUITE_END() // metric_modes

BOOST_AUTO_TEST_SUITE_END() // incremental

#endif

BOOST_AUTO_TEST_SUITE_END() // lexer
BOOST_AUTO_TEST_SUITE_END() // test_nycleus2
