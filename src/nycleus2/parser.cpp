#include "nycleus2/parser.hpp"
#include "nycleus2/diagnostic.hpp"
#include "nycleus2/hlir.hpp"
#include "nycleus2/source_file.hpp"
#include "nycleus2/source_location.hpp"
#include "nycleus2/token.hpp"
#include "nycleus2/util.hpp"
#include "unicode/encoding.hpp"
#include "unicode/props.hpp"
#include "util/overflow.hpp"
#include "util/safe_int.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <cassert>
#include <memory>
#include <optional>
#include <ranges>
#include <string>
#include <utility>
#include <variant>
#include <vector>

using namespace nycleus2;

namespace {

// TODO: Make parser actually incremental

/** @brief The implementation of the parser.
 *
 * This is a recursive descent parser. It has functions for various productions; each function
 * is given a reference to a `unique_ptr` to a production base type and is expected to parse
 * the production and store it in the supplied pointer.
 *
 * The functions `parse_type`, `parse_expr`, and `parse_def` are used to parse a subtree of the
 * corresponding kind. These functions also perform reuse if possible. For this reason, they
 * expect the entire subtree intended for parsing to be still in the token source. (Statements
 * are special in that they are always parsed as part of parsing a block, so they don't have
 * their own dedicated parsing functions.)
 *
 * The functions `parse_type_trail`, `parse_expr_trail`, and `parse_def_trail` are used for a
 * similar purpose, but they expect the first token of the subtree to have already been
 * consumed. They examine the last consumed token (which is preserved by the parser) to
 * determine what kind of subtree to parse. They cannot reuse.
 *
 * The rest of the functions ending in `_trail` all parse particular kinds of subtrees. They
 * likewise expect the first token of the subtree to have been consumed. They do not examine
 * that token, expecting it to have already been done by the caller.
 *
 * When a syntax error occurs, the parser enters panic mode. It sets the panic flag and goes
 * down the stack until it finds a point where recovery can happen. During this process
 * functions produce partial parsing results. When a recovery point is found, the parser
 * consumes tokens until a safe point is found where parsing can be resumed. (The parser may
 * not consume any tokens at all if the safe point is found immediately.) Then the panic flag
 * is cleared and parsing is resumed. The consumed tokens are called a panic sequence and are
 * stored in the last node before the syntax error (see hlir::panic_seq).
 *
 * When panic mode is entered, the panic sequence starts at the last consumed token. (If the
 * panic sequence is empty, parsing is resumed from the last consumed token.) A function that
 * returns while in panic mode must produce a partial result whose metric covers all the tokens
 * consumed by the function. This is important in order to account for the metrics of all
 * consumed tokens for subsequent reuse.
 *
 * For the `parse_type`, `parse_expr`, and `parse_def` functions and their `_trail` versions,
 * if the first token is not valid for the expected kind, the function produces a null pointer.
 * This does not trigger a panic. If the caller requires a subtree of the required kind, it
 * must trigger a panic itself. All other functions always produce a non-null pointer to a
 * subtree of its corresponding type. */
class parser {
private:
  parsed_file& file_;

  bool panic_{false};

  /** @throws std::bad_alloc */
  template<typename Derived, typename Base>
  Derived& make(std::unique_ptr<Base>& target) {
    auto unique_ptr{std::make_unique<Derived>()};
    auto& raw_ref{*unique_ptr};
    target = std::move(unique_ptr);
    return raw_ref;
  }

  using def_index_t = util::safe_int<decltype(file_.defs)::size_type>;
  using def_diff_t = util::safe_int<decltype(file_.defs)::difference_type>;

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Old position tracking

  /** @brief The current old definition, or null if we're in a gap.
   *
   * If this is null, we're in a gap between definitions. In that case, `old_def_index_` points
   * to the next definition, and `old_offset_` is the offset into the gap.
   *
   * If this is not null, we're in this definition. In that case, `old_def_index_` is the index
   * of the next old definition, and `old_offset_` is the offset into the definition.
   *
   * A position at the start of a gap is deemed to be part of the gap. This is because we're
   * already completely past the preceding definition, so we cannot possibly reuse any part of
   * it, thus keeping it around is useless.
   *
   * Likewise, a position at the end of a gap is deemed to be part of the gap. This is because,
   * at this point, we can still reuse the entire old definition. */
  std::unique_ptr<hlir::def> old_def_;

  /** @brief The index of the next old definition. */
  def_index_t old_def_index_{0};

  /** @brief The metric into the current old definition or gap between old definitions. */
  source_metric old_metric_{0, 0};

  /** @brief Increases the current position in the old parse tree by the given metric. */
  void increase_old_pos(source_metric metric) noexcept {
    while(metric != source_metric{0, 0}) {
      if(old_def_) {
        // We are currently in a definition.
        source_metric const def_remaining{old_def_->loc.from(old_metric_)};
        source_metric const advance{std::min(def_remaining, metric)};
        old_metric_ = old_metric_.add(advance);
        metric = metric.from(advance);

        // If we reached the end of the definition, get rid of it.
        if(old_metric_ == old_def_->loc) {
          old_def_.reset();
          old_metric_ = source_metric{0, 0};
        }
      } else {
        // We are currently in a gap.
        if(old_def_index_ == util::wrap(file_.defs.size())) {
          // We are past all old definitions; no more reuse is possible.
          return;
        }
        if(old_metric_ == file_.defs[old_def_index_.unwrap()].preceding_gap) {
          // We are at the end of the gap, but we still need to advance. Pull out the next
          // definition, so that we can advance into it.
          old_def_ = std::move(file_.defs[old_def_index_.unwrap()].def);
          file_.defs.erase(file_.defs.cbegin() + old_def_index_.cast<def_diff_t>().unwrap());
          old_metric_ = source_metric{0, 0};
        } else {
          // Advance through the gap.
          source_metric const gap_remaining{file_.defs[old_def_index_.unwrap()]
            .preceding_gap.from(old_metric_)};
          source_metric const advance{std::min(gap_remaining, metric)};
          old_metric_ = old_metric_.add(advance);
          metric = metric.from(advance);
        }
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Last consumed token

  class buffered_token_source {
  private:
    token_source& tok_src_;

    token last_token_;
    source_metric prev_gap_metric_{source_metric::uninitialized()};
    source_metric last_token_metric_{source_metric::uninitialized()};
    source_metric last_gap_metric_{source_metric::uninitialized()};

  public:
    explicit buffered_token_source(token_source& tok_src) noexcept: tok_src_{tok_src} {}

    /** @brief Returns a reference to the last token consumed from the source.
     *
     * Can only be called after `advance_token`.
     *
     * This is a mutable reference to allow moving data out of the token. */
    token& last_token() noexcept { return last_token_; }

    /** @brief Returns the metric of the gap preceding the last consumed token.
     *
     * Can only be called after `advance_token`. */
    source_metric prev_gap_metric() const noexcept { return prev_gap_metric_; }

    /** @brief Returns the metric of the last consumed token.
     *
     * Can only be called after `advance_token`. If the last token was `tok::eof`, this is a
     * zero metric. */
    source_metric last_token_metric() const noexcept { return last_token_metric_; }

    /** @brief Returns the metric of the gap preceding the next token.
     *
     * Can only be called after `start` or `advance_token`. If the last token was `tok::eof`,
     * this is a zero metric. */
    source_metric last_gap_metric() const noexcept { return last_gap_metric_; }

    /** @brief Starts iterating over the token source, filling the last gap metric.
     * @returns The amount by which the old position has been advanced.
     * @throws std::bad_alloc
     * @throws std::length_error
     * @throws token_source_error */
    source_metric start() {
      auto const first_gap{tok_src_.first_gap()};
      last_gap_metric_ = first_gap.new_metric;
      return first_gap.old_metric;
    }

    /** @brief Consumes one more token from the token source and buffers it.
     * @returns The amount by which the old position has been advanced.
     * @throws std::bad_alloc
     * @throws std::length_error
     * @throws token_source_error */
    source_metric advance_token() {
      auto next_token_result{tok_src_.next_token()};
      last_token_ = std::move(next_token_result.next_token);
      prev_gap_metric_ = last_gap_metric_;
      if(std::holds_alternative<tok::eof>(last_token_)) {
        last_token_metric_ = last_gap_metric_ = source_metric{0, 0};
        return source_metric{0, 0};
      } else {
        last_token_metric_ = next_token_result.token_new_metric;
        last_gap_metric_ = next_token_result.gap_new_metric;
        return next_token_result.old_metric;
      }
    }
  };

  buffered_token_source tok_buf_;

  /** @brief Consumes one more token from the token source and buffers it.
   *
   * This also adjusts the old position.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void advance_token() {
    this->increase_old_pos(tok_buf_.advance_token());
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Public API
public:
  explicit parser(token_source& tok_src, parsed_file& file) noexcept:
    file_{file}, tok_buf_{tok_src} {}

  parser(parser const&) = delete;
  parser& operator=(parser const&) = delete;
  ~parser() noexcept = default;

  /** @brief The main parser entrypoint.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse() {
    this->increase_old_pos(tok_buf_.start());

    // TODO: Parse version directives

    source_metric last_gap{tok_buf_.last_gap_metric()};
    for(;;) {
      if(panic_) {
        // TODO: Reuse top-level definition after panic

        panic_ = false;
      } else {
        // TODO: Reuse top-level definitions

        advance_token();
      }

      if(std::holds_alternative<tok::eof>(tok_buf_.last_token())) {
        break;
      }

      parsed_file::top_level_def top_def{
        .preceding_gap = last_gap,
        .def = nullptr,
        .panic_seq = {}
      };
      this->parse_def_trail(top_def.def);
      if(!top_def.def) {
        top_def.diags.push_back(std::make_unique<diag::top_level_def_expected>(
          source_metric_range{{0, 0}, tok_buf_.last_token_metric()}));
        panic_ = true;
      }
      if(panic_) {
        source_metric panic_metric{top_def.def ? top_def.def->loc : source_metric{0, 0}};
        for(;;) {
          if(std::holds_alternative<tok::eof>(tok_buf_.last_token())) {
            break;
          }
          if(
            auto const* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
            w && w->sigil == tok::word::sigil_t::none
            && (w->text == u8"fn" || w->text == u8"let" || w->text == u8"class")
          ) {
            break;
          }
          panic_metric = panic_metric.add(tok_buf_.prev_gap_metric())
            .add(tok_buf_.last_token_metric());
          top_def.panic_seq.metrics.push_back(panic_metric);
          advance_token();
        }

        top_def.panic_seq.metrics.push_back(panic_metric.add(tok_buf_.prev_gap_metric()));
      }

      file_.defs.insert(file_.defs.cbegin() + old_def_index_.cast<def_diff_t>().unwrap(),
        std::move(top_def));
      ++old_def_index_;

      last_gap = tok_buf_.last_gap_metric();
    }
  }

private:
  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Types

  /** @brief Reuses or parses a type name.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_type(std::unique_ptr<hlir::type>& p) {
    assert(!p);
    advance_token();
    this->parse_type_trail(p);
  }

  /** @brief Parses a type name, having consumed the first token.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_type_trail(std::unique_ptr<hlir::type>& p) {
    assert(!p);

    auto next_gap{source_metric::uninitialized()};

    // Pointer types
    if(auto const* const double_amp{std::get_if<tok::logical_and>(&tok_buf_.last_token())}) {
      auto& ptr_type_outer{this->make<hlir::ptr_type>(p)};
      ptr_type_outer.loc = tok_buf_.last_token_metric();

      auto& ptr_type_inner{this->make<hlir::ptr_type>(ptr_type_outer.pointee_type)};
      ptr_type_inner.loc = tok_buf_.last_token_metric().from(double_amp->midpoint);

      // TODO: Reuse pointee type name (after &&)

      next_gap = tok_buf_.last_gap_metric();
      advance_token();

      if(
        auto const* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
        w && w->sigil == tok::word::sigil_t::none
      ) {
        bool found_kw{false};
        if(w->text == u8"mut") {
          ptr_type_inner.is_mutable = true;
          found_kw = true;
        } else if(w->text == u8"const") {
          found_kw = true;
        }

        // TODO: Reuse pointee type name (after keyword, after &&)

        if(found_kw) {
          ptr_type_outer.loc = ptr_type_outer.loc.add(next_gap)
            .add(tok_buf_.last_token_metric());
          ptr_type_inner.loc = ptr_type_inner.loc.add(next_gap)
            .add(tok_buf_.last_token_metric());
          next_gap = tok_buf_.last_gap_metric();
          advance_token();
        }
      }

      this->parse_type_trail(ptr_type_inner.pointee_type);
      if(!ptr_type_inner.pointee_type) {
        ptr_type_inner.diags.push_back(std::make_unique<diag::type_name_expected>(
          ptr_type_inner.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      ptr_type_outer.loc = ptr_type_outer.loc.add(next_gap)
        .add(ptr_type_inner.pointee_type->loc);
      ptr_type_inner.loc = ptr_type_inner.loc.add(next_gap)
        .add(ptr_type_inner.pointee_type->loc);
      return;
    }

    if(std::holds_alternative<tok::amp>(tok_buf_.last_token())) {
      auto& ptr_type{this->make<hlir::ptr_type>(p)};

      ptr_type.loc = tok_buf_.last_token_metric();

      // TODO: Reuse pointee type name

      next_gap = tok_buf_.last_gap_metric();
      advance_token();

      if(
        auto const* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
        w && w->sigil == tok::word::sigil_t::none
      ) {
        bool found_kw{false};
        if(w->text == u8"mut") {
          ptr_type.is_mutable = true;
          found_kw = true;
        } else if(w->text == u8"const") {
          found_kw = true;
        }

        // TODO: Reuse pointee type name (after keyword)

        if(found_kw) {
          ptr_type.loc = ptr_type.loc.add(next_gap).add(tok_buf_.last_token_metric());
          next_gap = tok_buf_.last_gap_metric();
          advance_token();
        }
      }

      this->parse_type_trail(ptr_type.pointee_type);
      if(!ptr_type.pointee_type) {
        ptr_type.diags.push_back(std::make_unique<diag::type_name_expected>(
          ptr_type.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      ptr_type.loc = ptr_type.loc.add(next_gap).add(ptr_type.pointee_type->loc);
      return;
    }

    tok::word* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
    if(!w) {
      return;
    }

    // Identifier types
    if(w->sigil == tok::word::sigil_t::dollar) {
      p = std::make_unique<hlir::id_type>(tok_buf_.last_token_metric(), std::move(w->text));
      return;
    }

    if(w->sigil != tok::word::sigil_t::none) {
      return;
    }

    // Integer types
    if(
      w->text.size() >= 2
      && (w->text.front() == u8's' || w->text.front() == u8'u')
      && std::ranges::all_of(
        w->text | std::views::drop(1) | unicode::utf8_decode(unicode::eh_assume_valid<>{}),
        [](char32_t ch) noexcept {
          return unicode::decimal_value(ch) != unicode::no_decimal_value;
        }
      )
    ) {
      int_width_t width{0};
      std::vector<std::unique_ptr<diagnostic>> diags;
      for(char32_t ch: w->text
          | std::views::drop(1) | unicode::utf8_decode(unicode::eh_assume_valid<>{})) {
        width = width * 10 + unicode::decimal_value(ch);
        if(width > max_int_width) {
          diags.push_back(std::make_unique<diag::int_type_width_too_big>(
            source_metric_range{{0, 0}, tok_buf_.last_token_metric()}));
          width = 0;
          break;
        }
      }
      if(width == 0 && diags.empty()) {
        diags.push_back(std::make_unique<diag::int_type_width_is_zero>(
          source_metric_range{{0, 0}, tok_buf_.last_token_metric()}));
      }
      p = std::make_unique<hlir::int_type>(tok_buf_.last_token_metric(), width,
        w->text.front() == u8's');
      p->diags = std::move(diags);
      return;
    }

    // Boolean type
    if(w->text == u8"bool") {
      p = std::make_unique<hlir::bool_type>(tok_buf_.last_token_metric());
      return;
    }

    // Function types
    if(w->text == u8"fn") {
      this->parse_fn_type_trail(p);
      return;
    }
  }

  /** @brief Parses a function type name, having consumed the first token.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_fn_type_trail(std::unique_ptr<hlir::type>& p) {
    assert(!p);
    auto& fn_type{this->make<hlir::fn_type>(p)};

    fn_type.loc = tok_buf_.last_token_metric();

    // Eat the left parenthesis
    source_metric next_gap{tok_buf_.last_gap_metric()};
    advance_token();
    if(!std::holds_alternative<tok::lparen>(tok_buf_.last_token())) {
      fn_type.diags.push_back(std::make_unique<diag::fn_type_lparen_expected>(
        fn_type.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
    fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());

    // Eat the second left parenthesis, if one exists
    next_gap = tok_buf_.last_gap_metric();
    advance_token();
    bool extra_paren{false};
    if(std::holds_alternative<tok::lparen>(tok_buf_.last_token())) {
      fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());
      extra_paren = true;
      next_gap = tok_buf_.last_gap_metric();
      advance_token();
    }

    // Eat the parameters
    for(;;) {
      // Eat the right parenthesis, if any
      if(std::holds_alternative<tok::rparen>(tok_buf_.last_token())) {
        fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());
        break;
      }

      bool has_name{false};

      // Eat the parameter name, if any
      if(
        tok::word const* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
        w && (w->sigil == tok::word::sigil_t::dollar
        || w->sigil == tok::word::sigil_t::none)
      ) {
        fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());
        next_gap = tok_buf_.last_gap_metric();
        advance_token();
        has_name = true;
        fn_type.param_types.emplace_back();
      }

      // Eat the colon
      if(!std::holds_alternative<tok::colon>(tok_buf_.last_token())) {
        if(has_name) {
          fn_type.diags.push_back(std::make_unique<diag::fn_type_param_type_expected>(
            fn_type.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        } else {
          fn_type.diags.push_back(std::make_unique<diag::fn_type_param_name_expected>(
            fn_type.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        }
        panic_ = true;
        return;
      }
      fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());
      if(!has_name) {
        fn_type.param_types.emplace_back();
      }

      // Eat the parameter type
      next_gap = tok_buf_.last_gap_metric();
      this->parse_type(fn_type.param_types.back());
      if(!fn_type.param_types.back()) {
        fn_type.diags.push_back(std::make_unique<diag::type_name_expected>(
          fn_type.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      fn_type.loc = fn_type.loc.add(next_gap).add(fn_type.param_types.back()->loc);
      if(panic_) {
        return;
      }

      // Eat the right parenthesis or the comma
      next_gap = tok_buf_.last_gap_metric();
      advance_token();
      if(std::holds_alternative<tok::rparen>(tok_buf_.last_token())) {
        fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());
        break;
      }
      if(!std::holds_alternative<tok::comma>(tok_buf_.last_token())) {
        fn_type.diags.push_back(std::make_unique<diag::fn_type_param_next_expected>(
          fn_type.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());

      next_gap = tok_buf_.last_gap_metric();
      advance_token();
    }

    if(extra_paren) {
      next_gap = tok_buf_.last_gap_metric();
      advance_token();

      // Eat the return type, if any
      if(std::holds_alternative<tok::colon>(tok_buf_.last_token())) {
        fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());
        fn_type.return_type.emplace();
        next_gap = tok_buf_.last_gap_metric();
        this->parse_type(*fn_type.return_type);
        if(!*fn_type.return_type) {
          fn_type.diags.push_back(std::make_unique<diag::type_name_expected>(
            fn_type.loc.add(next_gap).range(tok_buf_.last_token_metric())));
          panic_ = true;
          return;
        }
        fn_type.loc = fn_type.loc.add(next_gap).add((*fn_type.return_type)->loc);
        if(panic_) {
          return;
        }
        next_gap = tok_buf_.last_gap_metric();
        advance_token();
      }

      // Eat the right parenthesis
      if(!std::holds_alternative<tok::rparen>(tok_buf_.last_token())) {
        fn_type.diags.push_back(std::make_unique<diag::fn_type_rparen_expected>(
          fn_type.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      fn_type.loc = fn_type.loc.add(next_gap).add(tok_buf_.last_token_metric());
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Expressions

  /** @brief Reuses or parses an expression.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_expr(std::unique_ptr<hlir::expr>& p) {
    assert(!p);
    advance_token();
    this->parse_expr_trail(p);
  }

  /** @brief Parses an expression, having consumed the first token.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_expr_trail(std::unique_ptr<hlir::expr>& p) {
    assert(!p);
    // TODO: Parse full expression grammar
    this->parse_atomic_expr_trail(p);
  }

  /** @brief Parses an atomic expression, having consumed the first token.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_atomic_expr_trail(std::unique_ptr<hlir::expr>& p) {
    assert(!p);
    if(this->try_parse_self_term_expr_trail(p), p) {
      return;
    }

    std::visit(util::overloaded{
      [&](tok::int_literal const& int_literal) {
        // This is an integer literal.
        p = std::make_unique<hlir::int_literal>(
          tok_buf_.last_token_metric(),
          std::move(int_literal.value),
          int_literal.type_suffix
        );
      },

      [&](tok::word const& word) {
        // This is a Boolean literal or an identifier.

        if(word.sigil == tok::word::sigil_t::none) {
          if(word.text == u8"true") {
            p = std::make_unique<hlir::bool_literal>(tok_buf_.last_token_metric(), true);
          } else if(word.text == u8"false") {
            p = std::make_unique<hlir::bool_literal>(tok_buf_.last_token_metric(), false);
          }
          return;
        }

        if(word.sigil == tok::word::sigil_t::dollar) {
          p = std::make_unique<hlir::id_expr>(tok_buf_.last_token_metric(),
            std::move(word.text));
          return;
        }
      },

      [&](tok::lparen) {
        auto& parens_expr{this->make<hlir::parens_expr>(p)};
        parens_expr.loc = tok_buf_.last_token_metric();

        // Eat the operand
        source_metric next_gap{tok_buf_.last_gap_metric()};
        this->parse_expr(parens_expr.op);
        if(!parens_expr.op) {
          parens_expr.diags.push_back(std::make_unique<diag::expr_expected>(
            parens_expr.loc.add(next_gap).range(tok_buf_.last_token_metric())));
          panic_ = true;
          return;
        }
        parens_expr.loc = parens_expr.loc.add(next_gap).add(parens_expr.op->loc);
        if(panic_) {
          return;
        }

        // Eat the right parenthesis
        next_gap = tok_buf_.last_gap_metric();
        advance_token();
        if(!std::holds_alternative<tok::rparen>(tok_buf_.last_token())) {
          parens_expr.diags.push_back(std::make_unique<diag::expr_rparen_expected>(
            parens_expr.loc.add(next_gap).range(tok_buf_.last_token_metric())));
          panic_ = true;
          return;
        }
        parens_expr.loc = parens_expr.loc.add(next_gap).add(tok_buf_.last_token_metric());
      },

      [&](auto const&) noexcept {}
    }, tok_buf_.last_token());
  }

  /** @brief Parses a self-terminating expression, having consumed the first token.
   *
   * A self-terminating expression is one that, when appearing as a statement, terminates on
   * its own without requiring a semicolon.
   *
   * If the first token does not indicate the start of a self-terminating expression, keeps the
   * pointer null. This does not panic, so the caller can try to parse a different
   * non-terminal. But if parsing the expression fails later, this still panics (and returns a
   * partial result).
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void try_parse_self_term_expr_trail(std::unique_ptr<hlir::expr>& p) {
    assert(!p);
    if(std::holds_alternative<tok::lbrace>(tok_buf_.last_token())) {
      // This is a block.
      this->parse_block_trail(p);
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Blocks

  /** @brief Parses a block, having consumed the first token.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_block_trail(std::unique_ptr<hlir::expr>& p) {
    assert(!p);
    auto& block{this->make<hlir::block>(p)};

    block.loc = tok_buf_.last_token_metric();

    source_metric next_gap{source_metric::uninitialized()};

    for(;;) {
      // TODO: Reuse statements

      next_gap = tok_buf_.last_gap_metric();
      advance_token();

      if(std::holds_alternative<tok::rbrace>(tok_buf_.last_token())) {
        block.loc = block.loc.add(next_gap).add(tok_buf_.last_token_metric());
        return;
      }

      // If we found a self-terminating expression on the last iteration, we considered it to
      // be the trailing expression. But since the block is not over, it is actually an
      // expression statement.
      if(block.trail) {
        block.stmts.push_back(std::make_unique<hlir::expr_stmt>(
          (*block.trail)->loc, std::move(*block.trail)));
        block.trail.reset();
      }

      if(std::unique_ptr<hlir::def> def; this->parse_def_trail(def), def) {
        // This is a definition statement.
        block.loc = block.loc.add(next_gap).add(def->loc);
        block.stmts.push_back(std::make_unique<hlir::def_stmt>(def->loc, std::move(def)));

        if(panic_) {
          return;
        }
        continue;
      }

      if(std::unique_ptr<hlir::expr> expr; this->try_parse_self_term_expr_trail(expr), expr) {
        // This is a self-terminating expression; it can be a statement on its own, without a
        // terminating semicolon, but it can also be a trailing expression. Consider it a
        // trailing expression for now, but if we find more stuff in the block, promote it to a
        // statement on the next iteration.
        block.trail = std::move(expr);
        block.loc = block.loc.add(next_gap).add((*block.trail)->loc);

        if(panic_) {
          // If we're panicking, treat this expression as an expression statement.
          block.stmts.push_back(std::make_unique<hlir::expr_stmt>((*block.trail)->loc,
            std::move(*block.trail)));
          block.trail.reset();
          return;
        }
        continue;
      }

      std::unique_ptr<hlir::expr> expr;
      this->parse_expr_trail(expr);
      if(!expr) {
        block.diags.push_back(std::make_unique<diag::stmt_expected>(
          block.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      if(panic_) {
        // If we're panicking, treat this expression as an expression statement.
        block.loc = block.loc.add(next_gap).add(expr->loc);
        block.stmts.push_back(std::make_unique<hlir::expr_stmt>(expr->loc, std::move(expr)));
        return;
      }
      auto const gap_before_expr{next_gap};

      next_gap = tok_buf_.last_gap_metric();
      advance_token();

      if(std::holds_alternative<tok::rbrace>(tok_buf_.last_token())) {
        // The expression we've just parsed is the trailing expression; the block is now over.
        block.loc = block.loc.add(gap_before_expr).add(expr->loc).add(next_gap)
          .add(tok_buf_.last_token_metric());
        block.trail = std::move(expr);
        return;
      }

      if(std::holds_alternative<tok::semicolon>(tok_buf_.last_token())) {
        // The expression we've just parsed is the body of an expression statement.
        block.stmts.push_back(std::make_unique<hlir::expr_stmt>(
          expr->loc.add(next_gap).add(tok_buf_.last_token_metric()),
          std::move(expr)
        ));
        block.loc = block.loc.add(gap_before_expr).add(block.stmts.back()->loc);
        continue;
      }

      if(std::holds_alternative<tok::assign>(tok_buf_.last_token())) {
        // This is an assignment statement.
        std::unique_ptr<hlir::stmt> stmt_p;
        auto& assign_stmt{this->make<hlir::assign_stmt>(stmt_p)};
        block.stmts.push_back(std::move(stmt_p));

        assign_stmt.loc = expr->loc.add(next_gap).add(tok_buf_.last_token_metric());
        assign_stmt.target = std::move(expr);

        next_gap = tok_buf_.last_gap_metric();
        this->parse_expr(assign_stmt.value);
        if(!assign_stmt.value) {
          block.loc = block.loc.add(gap_before_expr).add(assign_stmt.loc);
          assign_stmt.diags.push_back(std::make_unique<diag::expr_expected>(
            assign_stmt.loc.add(next_gap).range(tok_buf_.last_token_metric())));
          panic_ = true;
          return;
        }
        if(panic_) {
          assign_stmt.loc = assign_stmt.loc.add(next_gap).add(assign_stmt.value->loc);
          block.loc = block.loc.add(gap_before_expr).add(assign_stmt.loc);
          return;
        }
        assign_stmt.loc = assign_stmt.loc.add(next_gap).add(assign_stmt.value->loc);

        // Eat the semicolon
        next_gap = tok_buf_.last_gap_metric();
        advance_token();
        if(!std::holds_alternative<tok::semicolon>(tok_buf_.last_token())) {
          assign_stmt.diags.push_back(std::make_unique<diag::assign_semicolon_expected>(
            assign_stmt.loc.add(next_gap).range(tok_buf_.last_token_metric())));
          panic_ = true;
          block.loc = block.loc.add(gap_before_expr).add(assign_stmt.loc);
          return;
        }
        assign_stmt.loc = assign_stmt.loc.add(next_gap).add(tok_buf_.last_token_metric());

        block.loc = block.loc.add(gap_before_expr).add(assign_stmt.loc);
        continue;
      }

      // This is not a valid statement. Treat this expression as an expression statement, then
      // panic.
      block.loc = block.loc.add(gap_before_expr).add(expr->loc);
      block.stmts.push_back(std::make_unique<hlir::expr_stmt>(expr->loc, std::move(expr)));
      block.diags.push_back(std::make_unique<diag::block_rbrace_expected>(
        block.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Definitions

  /** @brief Reuses or parses a definition.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_def(std::unique_ptr<hlir::def>& p) {
    assert(!p);
    advance_token();
    this->parse_def_trail(p);
  }

  /** @brief Parses a definition, having consumed the first token.
   *
   * If the first token does not indicate the start of a definition, keeps the pointer null.
   * This does not panic, so the caller can try to parse a different non-terminal. But if
   * parsing the definition fails later, this still panics (and returns a partial result).
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_def_trail(std::unique_ptr<hlir::def>& p) {
    assert(!p);

    auto const* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
    if(!w) {
      return;
    }

    if(w->text == u8"fn") {
      this->parse_fn_def_trail(p);
    } else if(w->text == u8"let") {
      this->parse_var_def_trail(p);
    } else if(w->text == u8"class") {
      this->parse_class_def_trail(p);
    }
  }

  /** @brief Parses a function definition, having consumed the first token.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_fn_def_trail(std::unique_ptr<hlir::def>& p) {
    assert(!p);
    auto& fn_def{this->make<hlir::fn_def>(p)};

    fn_def.loc = tok_buf_.last_token_metric();

    // Eat the function name
    source_metric next_gap{tok_buf_.last_gap_metric()};
    advance_token();
    tok::word* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
    if(!w || w->sigil != tok::word::sigil_t::dollar && w->sigil != tok::word::sigil_t::none) {
      fn_def.diags.push_back(std::make_unique<diag::fn_def_name_expected>(
        fn_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
    fn_def.loc = fn_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
    fn_def.name = std::move(w->text);

    // Eat the left parenthesis
    next_gap = tok_buf_.last_gap_metric();
    advance_token();
    if(!std::holds_alternative<tok::lparen>(tok_buf_.last_token())) {
      fn_def.diags.push_back(std::make_unique<diag::fn_def_lparen_expected>(
        fn_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
    fn_def.loc = fn_def.loc.add(next_gap).add(tok_buf_.last_token_metric());

    // Eat the parameters
    for(;;) {
      // Eat the right parenthesis, if any
      next_gap = tok_buf_.last_gap_metric();
      advance_token();
      if(std::holds_alternative<tok::rparen>(tok_buf_.last_token())) {
        fn_def.loc = fn_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
        break;
      }

      fn_def.params.emplace_back();

      // Eat the parameter name, if any
      if(
        tok::word* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
        w && (w->sigil == tok::word::sigil_t::dollar
        || w->sigil == tok::word::sigil_t::none)
      ) {
        fn_def.params.back().name = std::move(w->text);
        fn_def.loc = fn_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
        next_gap = tok_buf_.last_gap_metric();
        advance_token();
      }

      // Eat the colon
      if(!std::holds_alternative<tok::colon>(tok_buf_.last_token())) {
        // If we haven't seen the name by this point, consider the parameter to be absent.
        if(fn_def.params.back().name) {
          fn_def.diags.push_back(std::make_unique<diag::fn_def_param_type_expected>(
            fn_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        } else {
          fn_def.diags.push_back(std::make_unique<diag::fn_def_param_name_expected>(
            fn_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
          fn_def.params.pop_back();
        }
        panic_ = true;
        return;
      }
      fn_def.loc = fn_def.loc.add(next_gap).add(tok_buf_.last_token_metric());

      // Eat the parameter type
      next_gap = tok_buf_.last_gap_metric();
      this->parse_type(fn_def.params.back().type);
      if(!fn_def.params.back().type) {
        fn_def.diags.push_back(std::make_unique<diag::type_name_expected>(
          fn_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      fn_def.loc = fn_def.loc.add(next_gap).add(fn_def.params.back().type->loc);
      if(panic_) {
        return;
      }

      // Eat the right parenthesis or the comma
      next_gap = tok_buf_.last_gap_metric();
      advance_token();
      if(std::holds_alternative<tok::rparen>(tok_buf_.last_token())) {
        fn_def.loc = fn_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
        break;
      }
      if(!std::holds_alternative<tok::comma>(tok_buf_.last_token())) {
        fn_def.diags.push_back(std::make_unique<diag::fn_def_param_next_expected>(
          fn_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      fn_def.loc = fn_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
    }

    // TODO: Reuse function body

    // Eat the colon and the return type, if any
    next_gap = tok_buf_.last_gap_metric();
    advance_token();
    if(std::holds_alternative<tok::colon>(tok_buf_.last_token())) {
      fn_def.loc = fn_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
      fn_def.return_type.emplace();
      next_gap = tok_buf_.last_gap_metric();
      this->parse_type(*fn_def.return_type);
      if(!*fn_def.return_type) {
        fn_def.diags.push_back(std::make_unique<diag::type_name_expected>(
          fn_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      fn_def.loc = fn_def.loc.add(next_gap).add((*fn_def.return_type)->loc);
      if(panic_) {
        return;
      }
      next_gap = tok_buf_.last_gap_metric();
      advance_token();
    }

    if(!std::holds_alternative<tok::lbrace>(tok_buf_.last_token())) {
      fn_def.diags.push_back(std::make_unique<diag::fn_def_body_expected>(
        fn_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }

    // Eat the function body
    this->parse_block_trail(fn_def.body);
    if(fn_def.body) {
      fn_def.loc = fn_def.loc.add(next_gap).add(fn_def.body->loc);
    }
  }

  /** @brief Parses a variable definition, having consumed the first token.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_var_def_trail(std::unique_ptr<hlir::def>& p) {
    assert(!p);
    auto& var_def{this->make<hlir::var_def>(p)};

    var_def.loc = tok_buf_.last_token_metric();

    // Eat the variable name
    source_metric next_gap{tok_buf_.last_gap_metric()};
    advance_token();
    tok::word* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
    if(!w || w->sigil != tok::word::sigil_t::dollar && w->sigil != tok::word::sigil_t::none) {
      var_def.diags.push_back(std::make_unique<diag::var_def_name_expected>(
        var_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
    var_def.loc = var_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
    var_def.name = std::move(w->text);

    // Eat the `mut` or `const`, if any
    next_gap = tok_buf_.last_gap_metric();
    advance_token();
    if(
      auto const* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
      w && w->sigil == tok::word::sigil_t::none
    ) {
      if(w->text == u8"mut") {
        var_def.loc = var_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
        var_def.mutable_spec = hlir::var_def::mutable_spec_t::mut;
        next_gap = tok_buf_.last_gap_metric();
        advance_token();
      } else if(w->text == u8"const") {
        var_def.loc = var_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
        var_def.mutable_spec = hlir::var_def::mutable_spec_t::constant;
        next_gap = tok_buf_.last_gap_metric();
        advance_token();
      }
    }

    // Eat the colon
    if(!std::holds_alternative<tok::colon>(tok_buf_.last_token())) {
      var_def.diags.push_back(std::make_unique<diag::var_def_type_expected>(
        var_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
    var_def.loc = var_def.loc.add(next_gap).add(tok_buf_.last_token_metric());

    // Eat the variable type
    next_gap = tok_buf_.last_gap_metric();
    this->parse_type(var_def.type);
    if(!var_def.type) {
      var_def.diags.push_back(std::make_unique<diag::type_name_expected>(
        var_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
    var_def.loc = var_def.loc.add(next_gap).add(var_def.type->loc);
    if(panic_) {
      return;
    }

    // Eat the assignment sign and the initializer, if any
    next_gap = tok_buf_.last_gap_metric();
    advance_token();
    if(std::holds_alternative<tok::assign>(tok_buf_.last_token())) {
      var_def.loc = var_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
      next_gap = tok_buf_.last_gap_metric();
      var_def.init.emplace();
      this->parse_expr(*var_def.init);
      if(!*var_def.init) {
        var_def.diags.push_back(std::make_unique<diag::expr_expected>(
          var_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      var_def.loc = var_def.loc.add(next_gap).add((*var_def.init)->loc);
      if(panic_) {
        return;
      }
      next_gap = tok_buf_.last_gap_metric();
      advance_token();
    }

    // Eat the semicolon
    if(!std::holds_alternative<tok::semicolon>(tok_buf_.last_token())) {
      if(var_def.init) {
        var_def.diags.push_back(std::make_unique<diag::var_def_semicolon_expected>(
          var_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      } else {
        var_def.diags.push_back(std::make_unique<diag::var_def_init_expected>(
          var_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      }
      panic_ = true;
      return;
    }
    var_def.loc = var_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
  }

  /** @brief Parses a class definition, having consumed the first token.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  void parse_class_def_trail(std::unique_ptr<hlir::def>& p) {
    assert(!p);
    auto& class_def{this->make<hlir::class_def>(p)};

    class_def.loc = tok_buf_.last_token_metric();

    // Eat the class name
    source_metric next_gap{tok_buf_.last_gap_metric()};
    advance_token();
    tok::word* const w{std::get_if<tok::word>(&tok_buf_.last_token())};
    if(!w || w->sigil != tok::word::sigil_t::dollar && w->sigil != tok::word::sigil_t::none) {
      class_def.diags.push_back(std::make_unique<diag::class_def_name_expected>(
        class_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
    class_def.loc = class_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
    class_def.name = std::move(w->text);

    // Eat the left brace
    next_gap = tok_buf_.last_gap_metric();
    advance_token();
    if(!std::holds_alternative<tok::lbrace>(tok_buf_.last_token())) {
      class_def.diags.push_back(std::make_unique<diag::class_def_body_expected>(
        class_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
      panic_ = true;
      return;
    }
    class_def.loc = class_def.loc.add(next_gap).add(tok_buf_.last_token_metric());

    // Eat the members
    for(;;) {
      // TODO: Reuse class member definitions

      // Eat the right brace
      next_gap = tok_buf_.last_gap_metric();
      advance_token();
      if(std::holds_alternative<tok::rbrace>(tok_buf_.last_token())) {
        class_def.loc = class_def.loc.add(next_gap).add(tok_buf_.last_token_metric());
        break;
      }

      std::unique_ptr<hlir::def> member_def;
      this->parse_def_trail(member_def);
      if(!member_def) {
        class_def.diags.push_back(std::make_unique<diag::class_def_rbrace_expected>(
          class_def.loc.add(next_gap).range(tok_buf_.last_token_metric())));
        panic_ = true;
        return;
      }
      class_def.loc = class_def.loc.add(next_gap).add(member_def->loc);
      class_def.members.emplace_back(std::move(member_def));
      if(panic_) {
        return;
      }
    }
  }
};

} // (anonymous)

void nycleus2::parse(token_source& tok_src, parsed_file& file) {
  parser p{tok_src, file};
  p.parse();
}
