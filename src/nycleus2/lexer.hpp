#ifndef NYCLEUS2_LEXER_HPP_INCLUDED_8C1EXQZ8P9O9VV7ZPYK4AVBM9
#define NYCLEUS2_LEXER_HPP_INCLUDED_8C1EXQZ8P9O9VV7ZPYK4AVBM9

#include "nycleus2/diagnostic.hpp"
#include "nycleus2/source_file.hpp"
#include "nycleus2/source_location.hpp"
#include "nycleus2/token.hpp"
#include "unicode/basic.hpp"
#include "unicode/encoding.hpp"
#include "unicode/props.hpp"
#include "util/non_null_ptr.hpp"
#include "util/safe_int.hpp"
#include "util/util.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <exception>
#include <memory>
#include <new>
#include <optional>
#include <ranges>
#include <span>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

namespace nycleus2 {

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Single lexer

/** @brief A mechanism for lexing a single token.
 *
 * The protocol for using a single_lexer for extracting a token from a code point sequence is
 * as follows:
 * 1. Ensure that you are at the beginning of a token. This means skipping whitespace code
 *    points.
 * 2. Create a single_lexer. The constructor accepts a reference to a source_location, which is
 *    supposed to indicate the location of the first code point that will be fed into the lexer
 *    and is used as the base source location for diagnostics.
 * 3. Feed code points to the lexer:
 *    1. Call `wants`, passing the next code point. It returns a Boolean value indicating
 *       whether the code point is part of the token currently being lexed. If it returns
 *       false, the token is fully lexed (but it could have continued if it were followed by a
 *       different code point).
 *    2. If `wants` returned true, call `feed`, passing the next code point and its metric. It
 *       returns a Boolean indicating whether the token continues after this code point. If it
 *       returns false, the token is fully lexed (regardless of what follows it), and the code
 *       point passed to `feed` is part of it.
 * 4. Once either `wants` or `feed` returns false or the end of the file is reached, lexing is
 *    done. The newly lexed token shall be retrieved via `get_token`. This should be called
 *    exactly once.
 * 5. The vector of diagnostics produced by the lexer can be obtained via `get_diags`. This
 *    should only be called once, after `get_token` is called.
 * 6. Afterwards, the single_lexer must be destroyed.
 * 7. If the caller wants to continue lexing more tokens, it must skip the gap after the token
 *    by skipping all whitespace code points, then use another single_lexer to extract the
 *    token. If the lexing ended because `wants` returned false, the process must continue with
 *    the code point passed into it. If the lexing ended because `feed` returned false, the
 *    process must continue with the code point after the one passed to it. */
class single_lexer {
private:
  struct state_init {
    [[nodiscard]] static bool wants(char32_t) noexcept { return true; }

    /** @brief std::bad_alloc
     * @brief std::length_error */
    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&);

    [[nodiscard, noreturn]] static token get_token(single_lexer&) noexcept {
      util::unreachable();
    }
  };

  struct state_error {
    [[nodiscard, noreturn]] static bool wants(char32_t) noexcept { util::unreachable(); }

    [[nodiscard, noreturn]] static bool feed(char32_t, source_metric, single_lexer&) noexcept {
      util::unreachable();
    }

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };

  /** @brief A utility that keeps track of bidirectional formatting codes inside comments and
   * produces diagnostics in case of invalid sequences. */
  class comment_bidi_tracker {
  private:
    enum class open_code: std::uint8_t { lre, rle, lro, rlo, lri, rli, fsi };

    std::vector<std::pair<open_code, source_metric_range>> open_codes_;

  public:
    comment_bidi_tracker() noexcept = default;

    /** @brief Updates the state upon seeing a new code point.
     * @throws std::bad_alloc */
    void feed(char32_t, source_metric_range, single_lexer&);

    /** @brief Updates the state upon seeing the end of a comment.
     *
     * This processing actually needs to be done at the end of every line of the comment, so
     * this is called internally after every line-breaking code point.
     *
     * @throws std::bad_alloc */
    void end(single_lexer&);
  };

  struct state_in_comment_begin {
    source_metric token_loc;

    [[nodiscard]] static bool wants(char32_t) noexcept { return true; }

    /** @throws std::bad_alloc */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&) const;

    [[nodiscard]] token get_token(single_lexer&) noexcept { return tok::comment{}; }
  };

  struct state_in_comment_sl {
    source_metric token_loc;

    comment_bidi_tracker bidi_tracker{};

    [[nodiscard]] static bool wants(char32_t) noexcept { return true; }

    /** @throws std::bad_alloc */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&);

    /** @throws std::bad_alloc */
    [[nodiscard]] token get_token(single_lexer&);
  };

  struct state_in_comment_sl_seen_cr {
    source_metric token_loc;

    [[nodiscard]] static bool wants(char32_t) noexcept;

    /** @throws std::bad_alloc */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&);

    [[nodiscard]] token get_token(single_lexer&) { return tok::comment{}; }
  };

  struct state_in_comment_ml {
    source_metric token_loc;

    comment_bidi_tracker bidi_tracker{};

    enum class substate_t { init, seen_star, seen_end };
    substate_t substate{substate_t::init};

    [[nodiscard]] static bool wants(char32_t) noexcept { return true; }

    /** @throws std::bad_alloc */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&);

    /** @throws std::bad_alloc */
    [[nodiscard]] token get_token(single_lexer&);
  };

  struct state_in_comment_mlnest {
    source_metric token_loc;
    src_pos_t nesting{0};

    comment_bidi_tracker bidi_tracker{};

    enum class substate_t { init, seen_plus, seen_hash, seen_end };
    substate_t substate{substate_t::init};

    [[nodiscard]] static bool wants(char32_t) noexcept { return true; }

    /** @throws std::bad_alloc */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&);

    /** @throws std::bad_alloc */
    [[nodiscard]] token get_token(single_lexer&);
  };

  struct state_in_word {
    tok::word::sigil_t sigil;
    std::u32string buf;
    source_metric token_loc;

    /** @brief The list of ZW(N)J codes in the identifier.
     *
     * For each code, we record its index in the buffer and the code's source range for
     * diagnostics. */
    std::vector<std::pair<util::size_t, source_metric_range>> format_codes{};

    [[nodiscard]] static bool wants(char32_t) noexcept;

    /** @brief std::bad_alloc
     * @brief std::length_error */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&);

    /** @brief std::bad_alloc
     * @brief std::length_error */
    [[nodiscard]] token get_token(single_lexer&);
  };

  struct state_in_num {
    std::u32string buf;
    source_metric token_loc;

    enum class substate_t { expects_x, dec, hex, dec_exp, hex_exp };
    substate_t substate;

    [[nodiscard]] bool wants(char32_t) const noexcept;

    /** @brief std::bad_alloc
     * @brief std::length_error */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&);

    /** @brief std::bad_alloc
     * @brief std::length_error */
    [[nodiscard]] token get_token(single_lexer&);
  };

  struct state_in_string {
    std::u32string buf;
    source_metric token_loc;

    /** @brief Whether this is a character literal.
     *
     * If false, this is a string literal. */
    bool is_char;

    /** @brief Whether this literal has been terminated. */
    bool is_terminated{false};

    /** @brief Information about an escape code, used during string and character literal
     * processing. */
    struct escape_code_info {
      /** @brief The source location of the backslash code point of the escape code.
       *
       * Used as the diagnostic location if the code point is changed by normalization. */
      source_metric_range slash_loc;

      /** @brief The source location of the entire escape code.
       *
       * Used as the diagnostic location if the code point is not changed by normalization. */
      source_metric_range code_loc;

      /** @brief The original (un-normalized) code points comprising the escape code, not
       * including the backslash.
       *
       * This is used to check, after normalization, whether the escape code has been changed
       * by the normalization.
       *
       * This field is also used to store the number of code points that need to be erased from
       * the string when replacing escape codes with their values. See comments in `get_token`
       * for more info. */
      std::u32string code;
    };

    std::vector<escape_code_info> escape_codes{};

    /** @brief The state of the escape code currently processed.
     *
     * - If this is zero, we are not in an escape code.
     * - If this is the special value `escape_state_pending`, we've just seen the backslash and
     *   are looking for the code point indicating the escape code's kind.
     * - If this is any other value, this is the number of remaining code points in the current
     *   Unicode escape code.
     *
     * This is used while initially building the literal, to construct the source locations of
     * the escape codes, for later use in diagnostics. This is not used to actually parse
     * escape code values, since escape codes can change after normalization. */
    util::uint8_t escape_state{0};
    static constexpr util::uint8_t escape_state_pending{255};

    [[nodiscard]] static bool wants(char32_t) noexcept { return true; }

    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&);

    /** @throws std::bad_alloc */
    [[nodiscard]] token get_token(single_lexer&);
  };

  template<typename T>
  struct state_punct_final {
    [[nodiscard, noreturn]] static bool wants(char32_t) noexcept { util::unreachable(); }

    [[nodiscard, noreturn]] static bool feed(char32_t, source_metric, single_lexer&) noexcept {
      util::unreachable();
    }

    [[nodiscard]] static token get_token(single_lexer&) noexcept { return T{}; }
  };

  struct state_seen_excl {
    [[nodiscard]] static bool wants(char32_t ch) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_neq: state_punct_final<tok::neq> {};
  struct state_seen_percent {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_percent_assign: state_punct_final<tok::percent_assign> {};
  struct state_seen_amp {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_logical_and {
    source_metric midpoint;

    [[nodiscard, noreturn]] static bool wants(char32_t) noexcept { util::unreachable(); }

    [[nodiscard, noreturn]] static bool feed(char32_t, source_metric, single_lexer&) noexcept {
      util::unreachable();
    }

    [[nodiscard]] token get_token(single_lexer&) noexcept;
  };
  struct state_seen_amp_assign: state_punct_final<tok::amp_assign> {};
  struct state_seen_lparen: state_punct_final<tok::lparen> {};
  struct state_seen_rparen: state_punct_final<tok::rparen> {};
  struct state_seen_star {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_star_assign: state_punct_final<tok::star_assign> {};
  struct state_seen_plus {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_incr: state_punct_final<tok::incr> {};
  struct state_seen_plus_assign: state_punct_final<tok::plus_assign> {};
  struct state_seen_comma: state_punct_final<tok::comma> {};
  struct state_seen_minus {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_decr: state_punct_final<tok::decr> {};
  struct state_seen_minus_assign: state_punct_final<tok::minus_assign> {};
  struct state_seen_arrow: state_punct_final<tok::arrow> {};
  struct state_seen_point {
    source_metric token_loc;

    [[nodiscard]] static bool wants(char32_t) noexcept;

    /** @throws std::bad_alloc
     * @throws std::length_error */
    [[nodiscard]] bool feed(char32_t, source_metric, single_lexer&) const;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_double_point {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_ellipsis: state_punct_final<tok::ellipsis> {};
  struct state_seen_slash {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_slash_assign: state_punct_final<tok::slash_assign> {};
  struct state_seen_colon: state_punct_final<tok::colon> {};
  struct state_seen_semicolon: state_punct_final<tok::semicolon> {};
  struct state_seen_lt {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_lrot {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_lrot_assign: state_punct_final<tok::lrot_assign> {};
  struct state_seen_lshift {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_lshift_assign: state_punct_final<tok::lshift_assign> {};
  struct state_seen_leq {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_three_way_cmp: state_punct_final<tok::three_way_cmp> {};
  struct state_seen_assign {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_eq: state_punct_final<tok::eq> {};
  struct state_seen_fat_arrow: state_punct_final<tok::fat_arrow> {};
  struct state_seen_gt {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_rrot {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_rrot_assign: state_punct_final<tok::rrot_assign> {};
  struct state_seen_geq: state_punct_final<tok::geq> {};
  struct state_seen_rshift {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_rshift_assign: state_punct_final<tok::rshift_assign> {};
  struct state_seen_question: state_punct_final<tok::question> {};
  struct state_seen_at: state_punct_final<tok::at> {};
  struct state_seen_lsquare: state_punct_final<tok::lsquare> {};
  struct state_seen_rsquare: state_punct_final<tok::rsquare> {};
  struct state_seen_bitwise_xor {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_bitwise_xor_assign: state_punct_final<tok::bitwise_xor_assign> {};
  struct state_seen_logical_xor: state_punct_final<tok::logical_xor> {};
  struct state_seen_lbrace: state_punct_final<tok::lbrace> {};
  struct state_seen_pipe {
    [[nodiscard]] static bool wants(char32_t) noexcept;

    [[nodiscard]] static bool feed(char32_t, source_metric, single_lexer&) noexcept;

    [[nodiscard]] static token get_token(single_lexer&) noexcept;
  };
  struct state_seen_pipe_assign: state_punct_final<tok::pipe_assign> {};
  struct state_seen_subst: state_punct_final<tok::subst> {};
  struct state_seen_logical_or: state_punct_final<tok::logical_or> {};
  struct state_seen_rbrace: state_punct_final<tok::rbrace> {};
  struct state_seen_tilde: state_punct_final<tok::tilde> {};

  using state_t = std::variant<
    state_init,
    state_error,

    state_in_comment_begin,
    state_in_comment_sl,
    state_in_comment_sl_seen_cr,
    state_in_comment_ml,
    state_in_comment_mlnest,

    state_in_word,
    state_in_num,

    state_in_string,

    state_seen_excl,
    state_seen_neq,
    state_seen_percent,
    state_seen_percent_assign,
    state_seen_amp,
    state_seen_logical_and,
    state_seen_amp_assign,
    state_seen_lparen,
    state_seen_rparen,
    state_seen_star,
    state_seen_star_assign,
    state_seen_plus,
    state_seen_incr,
    state_seen_plus_assign,
    state_seen_comma,
    state_seen_minus,
    state_seen_decr,
    state_seen_minus_assign,
    state_seen_arrow,
    state_seen_point,
    state_seen_double_point,
    state_seen_ellipsis,
    state_seen_slash,
    state_seen_slash_assign,
    state_seen_colon,
    state_seen_semicolon,
    state_seen_lt,
    state_seen_lrot,
    state_seen_lrot_assign,
    state_seen_lshift,
    state_seen_lshift_assign,
    state_seen_leq,
    state_seen_three_way_cmp,
    state_seen_assign,
    state_seen_eq,
    state_seen_fat_arrow,
    state_seen_gt,
    state_seen_rrot,
    state_seen_rrot_assign,
    state_seen_geq,
    state_seen_rshift,
    state_seen_rshift_assign,
    state_seen_question,
    state_seen_at,
    state_seen_lsquare,
    state_seen_rsquare,
    state_seen_bitwise_xor,
    state_seen_bitwise_xor_assign,
    state_seen_logical_xor,
    state_seen_lbrace,
    state_seen_pipe,
    state_seen_pipe_assign,
    state_seen_subst,
    state_seen_logical_or,
    state_seen_rbrace,
    state_seen_tilde
  >;

  state_t state_{state_init{}};
  std::vector<std::unique_ptr<diagnostic>> diags_;

public:
  explicit single_lexer() noexcept = default;

  [[nodiscard]] bool wants(char32_t) const noexcept;

  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] bool feed(char32_t, source_metric);

  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] token get_token();

  [[nodiscard]] std::vector<std::unique_ptr<diagnostic>> get_diags() noexcept;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Full lexer

/** @brief A token source that represents a full (non-incremental) lexing of a file.
 *
 * The old token sequence is deemed to be empty. The new sequence is the result of lexing the
 * supplied source file.
 *
 * Functions may throw std::bad_alloc or std::length_error if memory could not be allocated for
 * diagnostics. Also, they can throw whatever is thrown by operations on the underlying range;
 * if it is not std::bad_alloc or std::length_error, it is wrapped inside a token_source_error.
 */
template<std::ranges::input_range T>
requires
  std::same_as<std::ranges::range_value_t<T>, char8_t>
  && std::movable<T>
class full_token_source final: public token_source {
private:
  /** @brief The input range. */
  T input_;

  /** @brief The current position in the input range. */
  std::ranges::iterator_t<T> it_;

  /** @brief The end of the input range. */
  [[no_unique_address]] std::ranges::sentinel_t<T> iend_;

  /** @brief The start of the linked list of diagnostic nodes. */
  std::unique_ptr<token_diagnostics> diags_;

  /** @brief A pointer to the owning pointer to the last diagnostic node. */
  std::unique_ptr<token_diagnostics>* last_diags_;

  /** @brief The last code point read from the input range.
   *
   * If this is `unicode::sentinel`, the buffer is deemed empty. */
  char32_t last_char_{unicode::sentinel};

  /** @brief The metric mode used to compute metrics. */
  metric_mode mode_;

  /** @brief Checks if we've reached the end of the input range. */
  [[nodiscard]] bool is_at_end() const noexcept(noexcept(it_ == iend_)) {
    return last_char_ == unicode::sentinel && it_ == iend_;
  }

  /** @brief If the lookahead buffer is empty, reads one code point and puts it in the buffer.
   */
  void fill_buffer() {
    if(last_char_ == unicode::sentinel) {
      assert(!is_at_end());
      last_char_ = unicode::utf8<>::decode(it_, iend_, unicode::eh_throw{});
    }
  }

  /** @brief Clears the lookahead buffer. */
  void clear_buffer() noexcept {
    last_char_ = unicode::sentinel;
  }

  /** @brief Returns the metric of the given code point, assuming it is the last code point
   * read from the file.
   *
   * The lookahead buffer must be empty. If the code point is a carriage return, the lookahead
   * buffer is filled to check if the following code point is a line feed. */
  [[nodiscard]] source_metric get_metric(char32_t ch) {
    assert(unicode::is_scalar_value(ch));
    assert(last_char_ == unicode::sentinel);
    if(is_line_terminator(ch, mode_)) {
      return {1, 0};
    } else if(ch == U'\r') {
      if(is_at_end()) {
        return {1, 0};
      }
      fill_buffer();
      if(last_char_ == U'\n') {
        return {0, num_columns(ch, mode_)};
      } else {
        return {1, 0};
      }
    } else {
      return {0, num_columns(ch, mode_)};
    }
  }

  /** @brief Assuming that the iterator is pointing to the beginning of a gap, advances it to
   * the end of the gap.
   * @returns The amount by which the iterator was advanced. */
  [[nodiscard]] source_metric skip_gap() {
    source_metric gap_offset{0, 0};
    while(!is_at_end()) {
      fill_buffer();
      char32_t const ch{last_char_};
      if(!unicode::white_space(ch)) {
        break;
      }

      clear_buffer();

      auto const loc{get_metric(ch)};
      gap_offset = gap_offset.add(loc);
      (*last_diags_)->token_loc = (*last_diags_)->token_loc.add(loc);
    }
    return gap_offset;
  }

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  [[nodiscard]] virtual first_gap_result do_first_gap() override {
    try {
      auto const gap_offset{skip_gap()};
      return {
        .new_metric = gap_offset,
        .old_metric = {0, 0},
        .max_reuse = {0, 0}
      };
    } catch(std::bad_alloc const&) {
      throw;
    } catch(std::length_error const&) {
      throw;
    } catch(...) {
      std::throw_with_nested(token_source_error{});
    }
  }

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  [[nodiscard]] virtual next_token_result do_next_token() override {
    try {
      if(is_at_end()) {
        return {
          .next_token = tok::eof{},
          .token_new_metric = source_metric::uninitialized(),
          .gap_new_metric = source_metric::uninitialized(),
          .old_metric = source_metric::uninitialized(),
          .max_reuse = source_metric::uninitialized()
        };
      }

      assert(*last_diags_);
      single_lexer lexer;
      source_metric token_offset{0, 0};

      do {
        fill_buffer();
        char32_t const ch{last_char_};
        if(!lexer.wants(ch)) {
          break;
        }

        clear_buffer();

        auto const loc{get_metric(ch)};
        bool const want_more{lexer.feed(ch, loc)};
        token_offset = token_offset.add(loc);
        if(!want_more) {
          break;
        }
      } while(!is_at_end());

      token result{lexer.get_token()};

      (*last_diags_)->diags = lexer.get_diags();
      if((*last_diags_)->diags.empty()) {
        (*last_diags_)->token_loc = (*last_diags_)->token_loc.add(token_offset);
      } else {
        assert(!(*last_diags_)->next);
        (*last_diags_)->next = std::make_unique<token_diagnostics>(
          (*last_diags_)->token_loc.add(token_offset));
        last_diags_ = &(*last_diags_)->next;
      }

      source_metric gap_offset{skip_gap()};
      return {
        .next_token = std::move(result),
        .token_new_metric = token_offset,
        .gap_new_metric = gap_offset,
        .old_metric = {0, 0},
        .max_reuse = {0, 0}
      };
    } catch(std::bad_alloc const&) {
      throw;
    } catch(std::length_error const&) {
      throw;
    } catch(...) {
      std::throw_with_nested(token_source_error{});
    }
  }

  [[nodiscard, noreturn]] virtual reuse_result do_reuse(
    source_metric
  ) noexcept override {
    util::unreachable();
  }

public:
  /** @throws std::bad_alloc */
  explicit full_token_source(
    std::type_identity_t<T> input,
    metric_mode mode
  ):
    input_{std::move(input)},
    it_{std::ranges::begin(input_)},
    iend_{std::ranges::end(input_)},
    diags_{std::make_unique<token_diagnostics>(source_metric{0, 0})},
    last_diags_{&diags_},
    mode_{mode} {}

  [[nodiscard]] std::unique_ptr<token_diagnostics> get_diags() noexcept {
    assert(*last_diags_);
    last_diags_->reset();
    return std::move(diags_);
  }
};

template<typename T>
full_token_source(T, metric_mode) -> full_token_source<std::remove_cvref_t<T>>;

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Incremental lexer

/** @brief A single modification of a source file.
 *
 * There are four kinds of deltas:
 * - A *modification* replaces a non-empty old range with a non-empty replacement.
 * - An *insertion* replaces an empty old range with a non-empty replacement.
 * - A *removal* replaces a non-empty old range with an empty replacement.
 * - A *null* delta replaces an empty old range with an empty replacement. This is not a useful
 *   kind of delta, but it still is a particular case that an incremental lexer can handle. */
struct source_file_delta {
  /** @brief Metric from the start of the old source file to the start of the range being
   * replaced. */
  source_metric old_start;

  /** @brief Metric from the start of the old range being replaced to the end of said range. */
  source_metric old_metric;

  /** @brief The text replacing the specified range. */
  std::u8string replacement;
};

/** @brief A token source representing a modification to a previously lexed source file.
 *
 * An incremental token source is created with two inputs: the source_file, representing the
 * original source file with the lexing information left by the last lexer pass, and a sequence
 * of deltas repsenting the modification to the source file. The token source's old token
 * sequence is the result of lexing the supplied source_file. The token source's new token
 * sequence is the result of lexing the source_file with the supplied modifications.
 *
 * As the lexing progresses, the source_file is gradually modified to account for new lexing
 * results. When this token source is fully consumed, the source_file object represents the
 * modified source file and is suitable for use as input to another instance of
 * incremental_token_source.
 *
 * The supplied deltas are all deemed to apply simultaneously: their offsets are based on the
 * old source file and do not take into account modifications by other deltas. This means that
 * their old ranges are not allowed to overlap. The deltas must be sorted in the order of their
 * old ranges.
 *
 * If there are multiple insertion deltas covering the same empty old range, their replacements
 * are inserted in the order in which the deltas appear in the supplied sequence. */
class incremental_token_source final: public token_source {
private:
  source_file& file_;

  std::span<source_file_delta const> const deltas_;

  /** @brief The metric in the old file from the start of the file to the current position.
   *
   * If the current position is inside a delta's replacement, this is the metric to the start
   * of the delta. */
  source_metric old_metric_;

  /** @brief The metric from the start of the current node to the current position. */
  source_metric cur_node_metric_;

  /** @brief The index of the node where the current position is.
   *
   * If the current position is at the end of the file, this is equal to the number of nodes;
   * the current offset is then set to zero. */
  src_pos_t cur_node_;

  /** @brief The byte offset into the current node's text to the current position.
   *
   * This can be zero to indicate that we're at the start of the node, but cannot be equal to
   * the length of the node, because that position is indicated by pointing at the start of the
   * next node (or at the end).
   *
   * If the current position is at the end of the file, this is zero. */
  src_pos_t cur_node_offset_;

  /** @brief The index of the next token boundary (after the current position) in the current
   * node's token boundary array.
   *
   * If the next token boundary is in a later node, or does not exist at all, this is equal to
   * the number of token boundaries in the current node.
   *
   * As the lexer advances, the token boundary arrays are updated to match the new source.
   * Therefore, all token boundaries before the current position correspond to the new source;
   * all token boundaries after the current position correspond to the old source. Old token
   * boundaries are removed in pairs, unless a token straddles a node boundary, in which case
   * its end is removed when the node is reached; therefore, the boundary in the current node
   * immediately after the current position is always a token start. */
  src_pos_t cur_token_boundary_;

  /** @brief The metric of text, starting at the current position, that can be reused (i. e.
   * where the old and new texts overlap and produce the same tokens).
   *
   * If reuse is currently not possible, this is zero. */
  source_metric reuse_remains_;

  /** @brief The offset from the current position in the current delta to the end of the delta.
   *
   * If we are not in a delta, this is zero. */
  src_pos_t cur_delta_remains_;

  /** @brief The current delta.
   *
   * If we've already processed all deltas, this is the past-the-end iterator. */
  std::span<source_file_delta const>::iterator cur_delta_;

  /** @brief The first diagnostic node that we have not reached yet. */
  util::non_null_ptr<std::unique_ptr<token_diagnostics>> cur_diags_;

  /** @brief A spare diagnostic node that we're filling with diagnostics.
   *
   * When parsing a token or comment, this node is filled with diagnostics. If there are
   * diagnostics, this node is added to the source file and a new spare node is allocated. */
  std::unique_ptr<token_diagnostics> spare_diags_;

  // TODO: Minimize allocations of the spare diagnostics node

  /** @brief Determines if the current position is at the end of the file. */
  [[nodiscard]] bool is_at_end() const noexcept;

  /** @brief The return type of `advance_by_metric`. */
  struct advance_by_metric_result {
    /** @brief The index of the node containing the new position. */
    src_pos_t node_index;
    /** @brief The offset into the target node to the new position. */
    src_pos_t node_offset;
    /** @brief The metric from the start of the node to the new position. */
    source_metric node_metric;
    /** @brief The index, in the node's token boundary array, of the next token boundary after
     * the new position.
     *
     * This is the size of the node's token boundary array if the new position is past all the
     * node's token boundaries. */
    src_pos_t token_boundary;
  };

  /** @brief Finds and returns information about the position that is ahead from the current
   * position by the given metric.
   *
   * Does not actually change the current position, but merely returns information about the
   * new position. */
  [[nodiscard]] advance_by_metric_result advance_by_metric(source_metric) const noexcept;

  /** @brief Retrieves the code point at the current position.
   *
   * Must not be called if the current position is the end of the file. */
  [[nodiscard]] char32_t get_char() const noexcept;

  /** @brief Handles reaching the end of a delta.
   * @returns The metric by which the old offset was advanced (which is equal to the delta's
   *          old metric). */
  [[nodiscard]] source_metric finish_delta() noexcept;

  /** @brief Handles starting one or more deltas at the current position.
   *
   * If there are any empty-replacement deltas that start at the current position, enters and
   * immediately exits them. Once a delta with a non-empty replacement is found, it is entered
   * and the function returns.
   *
   * As deltas are entered, the source file buffer is updated to contain the new text.
   *
   * @returns The metric by which the old offset was advanced (due to exiting all the
   *          empty-replacement deltas).
   * @throws std::bad_alloc */
  [[nodiscard]] source_metric try_start_delta();

  /** @brief Moves the current position to the next code point.
   *
   * Must not be called if the current position is the end of the file.
   *
   * @returns The metric indicating the advance of the old position.
   * @throws std::bad_alloc */
  [[nodiscard]] source_metric advance_char();

  /** @brief Attempts to calculate and set the reuse metric at the current position.
   *
   * This is called whenever we are in a position in the text that matches between the old and
   * the new texts. The function figures out how long the match is and sets the reuse limit
   * accordingly. */
  void synchronize() noexcept;

  /** @brief The return type of skip_gap. */
  struct skip_gap_result {
    /** @brief The amount of new text consumed. */
    source_metric new_metric;
    /** @brief The amount of old text consumed. */
    source_metric old_metric;
  };

  /** @brief Assuming that the current position is pointing to the beginning of a gap, advances
   * it to the end of the gap.
   * @throws std::bad_alloc */
  [[nodiscard]] skip_gap_result skip_gap();

  /** @throws std::bad_alloc */
  [[nodiscard]] virtual first_gap_result do_first_gap() override;

  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] virtual next_token_result do_next_token() override;

  [[nodiscard]] virtual reuse_result do_reuse(source_metric) noexcept override;

public:
  explicit incremental_token_source(
    source_file& file,
    std::span<source_file_delta const> deltas
  ) noexcept;
};

} // nycleus2

#endif
