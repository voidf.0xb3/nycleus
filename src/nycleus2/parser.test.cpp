#include <boost/test/unit_test.hpp>

#include "nycleus2/diagnostic.hpp"
#include "nycleus2/diagnostic_tools.hpp"
#include "nycleus2/hlir.hpp"
#include "nycleus2/hlir_tools.hpp"
#include "nycleus2/lexer.hpp"
#include "nycleus2/parser.hpp"
#include "nycleus2/source_location.hpp"
#include "nycleus2/source_file.hpp"
#include "nycleus2/source_file_tools.hpp"
#include "nycleus2/util.hpp"
#include "util/zip_view.hpp"
#include "test_tools.hpp"
#include <cassert>
#include <filesystem>
#include <memory>
#include <optional>
#include <span>
#include <string_view>
#include <vector>

using namespace nycleus2;

namespace {

using sm = source_metric;
using smr = source_metric_range;
using tl = parsed_file::top_level_def;
using test::nycleus2::diags;

struct parser_fixture {
  std::filesystem::path f{u8"test.ny"};
};

template<std::derived_from<hlir::hlir_base> T>
std::unique_ptr<T> with_diags(
  std::unique_ptr<T> node,
  std::vector<std::unique_ptr<diagnostic>> diags
) {
  assert(node);
  node->diags = std::move(diags);
  return node;
}

void do_parser_test(
  std::u8string_view input,
  std::span<tl const> expected_defs
) {
  full_token_source tok_src{input, metric_mode{metric_encoding::utf8, metric_line_term::full}};
  parsed_file file;

  nycleus2::parse(tok_src, file);

  BOOST_TEST(file.defs == expected_defs, boost::test_tools::per_element());
}

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_nycleus2)
BOOST_FIXTURE_TEST_SUITE(parser, parser_fixture)

// ///////////////////////////////////////////////////////////////////////////////////////////////
// // MARK: VERSION DIRECTIVE
// ///////////////////////////////////////////////////////////////////////////////////////////////

// BOOST_AUTO_TEST_SUITE(version)

// BOOST_AUTO_TEST_CASE(basic) {
//   do_parser_test(
//     u8"version 0;", f,
//     syms()
//   );
// }

// BOOST_AUTO_TEST_CASE(hex) {
//   do_parser_test(
//     u8"version 0x0;", f,
//     syms()
//   );
// }

// BOOST_AUTO_TEST_CASE(unknown) {
//   do_parser_test(
//     u8"version 179;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::bad_version>(src_loc{f, {1, 9}, {1, 12}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(type_suffix) {
//   do_parser_test(
//     u8"version 0u64;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::version_type_suffix>(src_loc{f, {1, 9}, {1, 13}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(missing_semicolon) {
//   do_parser_test(
//     u8"version 0 !", f,
//     syms(),
//     diags(
//       std::make_unique<diag::version_semicolon_expected>(
//         src_loc{f, {1, 11}, {1, 12}})
//     )
//   );

//   do_parser_test(
//     u8"version 0", f,
//     syms(),
//     diags(
//       std::make_unique<diag::version_semicolon_expected>(
//         src_loc{f, {1, 10}, {1, 10}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(is_not_num) {
//   do_parser_test(
//     u8"version x", f,
//     syms(),
//     diags(
//       std::make_unique<diag::bad_version>(src_loc{f, {1, 9}, {1, 10}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(is_eof) {
//   do_parser_test(
//     u8"version", f,
//     syms(),
//     diags(
//       std::make_unique<diag::bad_version>(src_loc{f, {1, 8}, {1, 8}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_more) {
//   do_parser_test(
//     u8"version 0;\n"
//     u8"fn foo() {}", f,
//     syms(
//       sym{u8"foo", make<hlir::fn_def>(src_loc{f, {2, 4}, {2, 7}},
//         std::vector<hlir::fn_def::param>{},
//         make<hlir::block>(src_loc{f, {2, 10}, {2, 12}})
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(late) {
//   do_parser_test(
//     u8"fn foo() {}\n"
//     u8"version 0;", f,
//     syms(
//       sym{u8"foo", make<hlir::fn_def>(src_loc{f, {1, 4}, {1, 7}},
//         std::vector<hlir::fn_def::param>{},
//         make<hlir::block>(src_loc{f, {1, 10}, {1, 12}})
//       )}
//     ),
//     diags(
//       std::make_unique<diag::unexpected_version>(src_loc{f, {2, 1}, {2, 8}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(duplicate) {
//   do_parser_test(
//     u8"version 0;\n"
//     u8"version 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::unexpected_version>(src_loc{f, {2, 1}, {2, 8}})
//     )
//   );
// }

// BOOST_AUTO_TEST_SUITE_END() // version

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: DEFINITIONS
///////////////////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(defs)

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Function definitions

BOOST_AUTO_TEST_SUITE(fn_def)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"fn foo() {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 11}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(one_param) {
  do_parser_test(
    u8"fn foo(x: u64) {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 17}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::nullopt,
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(two_params) {
  do_parser_test(
    u8"fn foo(x: u64, y: s32) {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 25}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )},
            hlir::fn_def::param{u8"y", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{32},
              true
            )}
          ),
          std::nullopt,
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(param_and_comma) {
  do_parser_test(
    u8"fn foo(x: u64,) {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 18}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::nullopt,
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(only_param_without_name) {
  do_parser_test(
    u8"fn foo(:u64) {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 15}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{std::nullopt, std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::nullopt,
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(first_param_without_name) {
  do_parser_test(
    u8"fn foo(:u64, x: s32) {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 23}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{std::nullopt, std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )},
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{32},
              true
            )}
          ),
          std::nullopt,
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(last_param_without_name) {
  do_parser_test(
    u8"fn foo(x: u64, :s32) {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 23}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )},
            hlir::fn_def::param{std::nullopt, std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{32},
              true
            )}
          ),
          std::nullopt,
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(no_params_but_comma) {
  do_parser_test(
    u8"fn foo(,) {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 7}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_param_name_expected>(smr{{0, 7}, {0, 8}})
        )),
        .panic_seq = {{{0, 8}, {0, 9}, {0, 11}, {0, 12}, {0, 12}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(return_type) {
  do_parser_test(
    u8"fn foo(x: u64): bool {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 23}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::make_unique<hlir::bool_type>(sm{0, 4}),
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(dollar_ids) {
  do_parser_test(
    u8"fn $foo($x: u64) {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{0, 19}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::nullopt,
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_name) {
  do_parser_test(
    u8"fn !",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 2}, std::nullopt,
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_name_expected>(smr{{0, 3}, {0, 4}})
        )),
        .panic_seq = {{{0, 4}, {0, 4}}}
      }
    )
  );

  do_parser_test(
    u8"fn",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 2}, std::nullopt,
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_name_expected>(smr{{0, 2}, {0, 2}})
        )),
        .panic_seq = {{{0, 2}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_lparen) {
  do_parser_test(
    u8"fn foo!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 6}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_lparen_expected>(smr{{0, 6}, {0, 7}})
        )),
        .panic_seq = {{{0, 7}, {0, 7}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 6}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_lparen_expected>(smr{{0, 6}, {0, 6}})
        )),
        .panic_seq = {{{0, 6}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_name) {
  do_parser_test(
    u8"fn foo(!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 7}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_param_name_expected>(smr{{0, 7}, {0, 8}})
        )),
        .panic_seq = {{{0, 8}, {0, 8}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo(",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 7}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_param_name_expected>(smr{{0, 7}, {0, 7}})
        )),
        .panic_seq = {{{0, 7}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_colon) {
  do_parser_test(
    u8"fn foo(x!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 8}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", nullptr}
          ),
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_param_type_expected>(smr{{0, 8}, {0, 9}})
        )),
        .panic_seq = {{{0, 9}, {0, 9}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo(x",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 8}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", nullptr}
          ),
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_param_type_expected>(smr{{0, 8}, {0, 8}})
        )),
        .panic_seq = {{{0, 8}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_type) {
  do_parser_test(
    u8"fn foo(x:!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 9}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", nullptr}
          ),
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::type_name_expected>(smr{{0, 9}, {0, 10}})
        )),
        .panic_seq = {{{0, 10}, {0, 10}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo(x:",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 9}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", nullptr}
          ),
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::type_name_expected>(smr{{0, 9}, {0, 9}})
        )),
        .panic_seq = {{{0, 9}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_comma) {
  do_parser_test(
    u8"fn foo(x: u64!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 13}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_param_next_expected>(smr{{0, 13}, {0, 14}})
        )),
        .panic_seq = {{{0, 14}, {0, 14}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo(x: u64",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 13}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_param_next_expected>(smr{{0, 13}, {0, 13}})
        )),
        .panic_seq = {{{0, 13}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_body_without_return) {
  do_parser_test(
    u8"fn foo(x: u64)!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 14}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_body_expected>(smr{{0, 14}, {0, 15}})
        )),
        .panic_seq = {{{0, 15}, {0, 15}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo(x: u64)",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 14}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::nullopt,
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_body_expected>(smr{{0, 14}, {0, 14}})
        )),
        .panic_seq = {{{0, 14}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_return_type) {
  do_parser_test(
    u8"fn foo(x: u64):!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 15}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          nullptr,
          nullptr
        ), diags(
          std::make_unique<diag::type_name_expected>(smr{{0, 15}, {0, 16}})
        )),
        .panic_seq = {{{0, 16}, {0, 16}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo(x: u64):",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 15}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          nullptr,
          nullptr
        ), diags(
          std::make_unique<diag::type_name_expected>(smr{{0, 15}, {0, 15}})
        )),
        .panic_seq = {{{0, 15}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_body_with_return) {
  do_parser_test(
    u8"fn foo(x: u64): u64!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 19}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::make_unique<hlir::int_type>(sm{0, 3},
            int_width_t{64},
            false
          ),
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_body_expected>(smr{{0, 19}, {0, 20}})
        )),
        .panic_seq = {{{0, 20}, {0, 20}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo(x: u64): u64",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::fn_def>(sm{0, 19}, u8"foo",
          test::make_vector<hlir::fn_def::param>(
            hlir::fn_def::param{u8"x", std::make_unique<hlir::int_type>(sm{0, 3},
              int_width_t{64},
              false
            )}
          ),
          std::make_unique<hlir::int_type>(sm{0, 3},
            int_width_t{64},
            false
          ),
          nullptr
        ), diags(
          std::make_unique<diag::fn_def_body_expected>(smr{{0, 19}, {0, 19}})
        )),
        .panic_seq = {{{0, 19}}}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // fn_def

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Variable definitions

BOOST_AUTO_TEST_SUITE(var_def)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 11}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(with_init) {
  do_parser_test(
    u8"let x: u64 = 0;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 15}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(with_mut) {
  do_parser_test(
    u8"let x mut: u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 15}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::mut
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(with_const) {
  do_parser_test(
    u8"let x const: u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 17}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::constant
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(dollar_id) {
  do_parser_test(
    u8"let $x: u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_name) {
  do_parser_test(
    u8"let !",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 3}, std::nullopt,
          nullptr,
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::var_def_name_expected>(smr{{0, 4}, {0, 5}})
        )),
        .panic_seq = {{{0, 5}, {0, 5}}}
      }
    )
  );

  do_parser_test(
    u8"let",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 3}, std::nullopt,
          nullptr,
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::var_def_name_expected>(smr{{0, 3}, {0, 3}})
        )),
        .panic_seq = {{{0, 3}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_colon) {
  do_parser_test(
    u8"let x!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 5}, u8"x",
          nullptr,
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::var_def_type_expected>(smr{{0, 5}, {0, 6}})
        )),
        .panic_seq = {{{0, 6}, {0, 6}}}
      }
    )
  );

  do_parser_test(
    u8"let x",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 5}, u8"x",
          nullptr,
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::var_def_type_expected>(smr{{0, 5}, {0, 5}})
        )),
        .panic_seq = {{{0, 5}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_type) {
  do_parser_test(
    u8"let x:!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 6}, u8"x",
          nullptr,
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::type_name_expected>(smr{{0, 6}, {0, 7}})
        )),
        .panic_seq = {{{0, 7}, {0, 7}}}
      }
    )
  );

  do_parser_test(
    u8"let x:",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 6}, u8"x",
          nullptr,
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::type_name_expected>(smr{{0, 6}, {0, 6}})
        )),
        .panic_seq = {{{0, 6}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_eq) {
  do_parser_test(
    u8"let x: u64 !",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 10}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::var_def_init_expected>(smr{{0, 11}, {0, 12}})
        )),
        .panic_seq = {{{0, 12}, {0, 12}}}
      }
    )
  );

  do_parser_test(
    u8"let x: u64",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 10}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::var_def_init_expected>(smr{{0, 10}, {0, 10}})
        )),
        .panic_seq = {{{0, 10}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_init) {
  do_parser_test(
    u8"let x: u64 = @",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          nullptr,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::expr_expected>(smr{{0, 13}, {0, 14}})
        )),
        .panic_seq = {{{0, 14}, {0, 14}}}
      }
    )
  );

  do_parser_test(
    u8"let x: u64 =",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          nullptr,
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::expr_expected>(smr{{0, 12}, {0, 12}})
        )),
        .panic_seq = {{{0, 12}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_semicolon) {
  do_parser_test(
    u8"let x: u64 = 0!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 14}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt),
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::var_def_semicolon_expected>(smr{{0, 14}, {0, 15}})
        )),
        .panic_seq = {{{0, 15}, {0, 15}}}
      }
    )
  );

  do_parser_test(
    u8"let x: u64 = 0",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::var_def>(sm{0, 14}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt),
          hlir::var_def::mutable_spec_t::none
        ), diags(
          std::make_unique<diag::var_def_semicolon_expected>(smr{{0, 14}, {0, 14}})
        )),
        .panic_seq = {{{0, 14}}}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // var_def

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Class definitions

BOOST_AUTO_TEST_SUITE(class_def)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"class A {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::class_def>(sm{0, 10}, u8"A",
          std::vector<std::unique_ptr<hlir::def>>{}
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(with_field) {
  do_parser_test(
    u8"class A {\n"
    u8"\tlet x: u64;\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::class_def>(sm{2, 1}, u8"A",
          test::make_vector<std::unique_ptr<hlir::def>>(
            std::make_unique<hlir::var_def>(sm{0, 11}, u8"x",
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              std::nullopt,
              hlir::var_def::mutable_spec_t::none
            )
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(with_method) {
  do_parser_test(
    u8"class A {\n"
    u8"\tfn foo() {}\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::class_def>(sm{2, 1}, u8"A",
          test::make_vector<std::unique_ptr<hlir::def>>(
            std::make_unique<hlir::fn_def>(sm{0, 11}, u8"foo",
              std::vector<hlir::fn_def::param>{},
              std::nullopt,
              std::make_unique<hlir::block>(sm{0, 2},
                std::vector<std::unique_ptr<hlir::stmt>>{},
                std::nullopt
              )
            )
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(with_nested_class) {
  do_parser_test(
    u8"class A {\n"
    u8"\tclass B {}\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::class_def>(sm{2, 1}, u8"A",
          test::make_vector<std::unique_ptr<hlir::def>>(
            std::make_unique<hlir::class_def>(sm{0, 10}, u8"B",
              std::vector<std::unique_ptr<hlir::def>>{}
            )
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(dollar_id) {
  do_parser_test(
    u8"class $A {}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::class_def>(sm{0, 11}, u8"A",
          std::vector<std::unique_ptr<hlir::def>>{}
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_name) {
  do_parser_test(
    u8"class !",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::class_def>(sm{0, 5}, std::nullopt,
          std::vector<std::unique_ptr<hlir::def>>{}
        ), diags(
          std::make_unique<diag::class_def_name_expected>(smr{{0, 6}, {0, 7}})
        )),
        .panic_seq = {{{0, 7}, {0, 7}}}
      }
    )
  );

  do_parser_test(
    u8"class",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::class_def>(sm{0, 5}, std::nullopt,
          std::vector<std::unique_ptr<hlir::def>>{}
        ), diags(
          std::make_unique<diag::class_def_name_expected>(smr{{0, 5}, {0, 5}})
        )),
        .panic_seq = {{{0, 5}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_body) {
  do_parser_test(
    u8"class A!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::class_def>(sm{0, 7}, u8"A",
          std::vector<std::unique_ptr<hlir::def>>{}
        ), diags(
          std::make_unique<diag::class_def_body_expected>(smr{{0, 7}, {0, 8}})
        )),
        .panic_seq = {{{0, 8}, {0, 8}}}
      }
    )
  );

  do_parser_test(
    u8"class A",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::class_def>(sm{0, 7}, u8"A",
          std::vector<std::unique_ptr<hlir::def>>{}
        ), diags(
          std::make_unique<diag::class_def_body_expected>(smr{{0, 7}, {0, 7}})
        )),
        .panic_seq = {{{0, 7}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_rbrace) {
  do_parser_test(
    u8"class A { !",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::class_def>(sm{0, 9}, u8"A",
          std::vector<std::unique_ptr<hlir::def>>{}
        ), diags(
          std::make_unique<diag::class_def_rbrace_expected>(smr{{0, 10}, {0, 11}})
        )),
        .panic_seq = {{{0, 11}, {0, 11}}}
      }
    )
  );

  do_parser_test(
    u8"class A {",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = with_diags(std::make_unique<hlir::class_def>(sm{0, 9}, u8"A",
          std::vector<std::unique_ptr<hlir::def>>{}
        ), diags(
          std::make_unique<diag::class_def_rbrace_expected>(smr{{0, 9}, {0, 9}})
        )),
        .panic_seq = {{{0, 9}}}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // class_def

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Bad definitions

BOOST_AUTO_TEST_CASE(bad) {
  do_parser_test(
    u8"0",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = nullptr,
        .panic_seq = {{{0, 1}, {0, 1}}},
        .diags = diags(std::make_unique<diag::top_level_def_expected>(smr{{0, 0}, {0, 1}}))
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // defs

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: TYPES
///////////////////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(types)

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Integer types

BOOST_AUTO_TEST_SUITE(int_type)

BOOST_AUTO_TEST_CASE(basic_unsigned) {
  do_parser_test(
    u8"let x: u8;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 10}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 2}, int_width_t{8}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(basic_signed) {
  do_parser_test(
    u8"let x: s16;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 11}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{16}, true),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(fractional) {
  do_parser_test(
    u8"let x: u14;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 11}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{14}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(big) {
  do_parser_test(
    u8"let x: u99999;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 14}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 6}, int_width_t{99999}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(too_big) {
  do_parser_test(
    u8"let x: u99999999999999999999999999999999999;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 44}, u8"x",
          with_diags(
            std::make_unique<hlir::int_type>(sm{0, 36}, int_width_t{0}, false),
            diags(std::make_unique<diag::int_type_width_too_big>(smr{{0, 0}, {0, 36}}))
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(leading_zeros) {
  do_parser_test(
    u8"let x: u00032;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 14}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 6}, int_width_t{32}, false),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(zero_width) {
  do_parser_test(
    u8"let x: u0;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 10}, u8"x",
          with_diags(
            std::make_unique<hlir::int_type>(sm{0, 2}, int_width_t{0}, false),
            diags(std::make_unique<diag::int_type_width_is_zero>(smr{{0, 0}, {0, 2}}))
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );

  do_parser_test(
    u8"let x: u000;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          with_diags(
            std::make_unique<hlir::int_type>(sm{0, 4}, int_width_t{0}, false),
            diags(std::make_unique<diag::int_type_width_is_zero>(smr{{0, 0}, {0, 4}}))
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(unicode) {
  do_parser_test(
    u8"let x: s\U0001D7DE\U0001D7DC;", // "let x: s𝟞𝟜;"
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 17}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 9}, int_width_t{64}, true),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // int_type

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: The Boolean type

BOOST_AUTO_TEST_CASE(bool_type) {
  do_parser_test(
    u8"let x: bool;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          std::make_unique<hlir::bool_type>(sm{0, 4}),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Function reference types

BOOST_AUTO_TEST_SUITE(fn_type)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: fn();",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 4},
            std::vector<std::unique_ptr<hlir::type>>{},
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(one_param) {
  do_parser_test(
    u8"let x: fn(x: u64);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 18}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 10},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(two_params) {
  do_parser_test(
    u8"let x: fn(x: u64, y: s32);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 26}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 18},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{32}, true)
            ),
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(param_and_comma) {
  do_parser_test(
    u8"let x: fn(x: u64,);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 19}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 11},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(only_param_without_name) {
  do_parser_test(
    u8"let x: fn(:u64);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 16}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 8},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(first_param_without_name) {
  do_parser_test(
    u8"let x: fn(:u64, x: s32);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 24}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 16},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{32}, true)
            ),
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(last_param_without_name) {
  do_parser_test(
    u8"let x: fn(x: u64, :s32);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 24}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 16},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{32}, true)
            ),
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(no_params_but_comma) {
  do_parser_test(
    u8"let x: fn(,);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 10}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 3},
            std::vector<std::unique_ptr<hlir::type>>{},
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_param_name_expected>(smr{{0, 3}, {0, 4}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 11}, {0, 12}, {0, 13}, {0, 13}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(return_type) {
  do_parser_test(
    u8"let x: fn((x: u64): bool);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 26}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 18},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::make_unique<hlir::bool_type>(sm{0, 4})
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(extra_parens) {
  do_parser_test(
    u8"let x: fn((x: u64));",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 20}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 12},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(dollar_ids) {
  do_parser_test(
    u8"let x: fn($x: u64);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 19}, u8"x",
          std::make_unique<hlir::fn_type>(sm{0, 11},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_lparen) {
  do_parser_test(
    u8"let x: fn!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 9}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::type>>{},
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_lparen_expected>(smr{{0, 2}, {0, 3}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 10}, {0, 10}}}
      }
    )
  );

  do_parser_test(
    u8"let x: fn",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 9}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::type>>{},
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_lparen_expected>(smr{{0, 2}, {0, 2}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 9}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_name) {
  do_parser_test(
    u8"let x: fn(!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 10}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 3},
            std::vector<std::unique_ptr<hlir::type>>{},
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_param_name_expected>(smr{{0, 3}, {0, 4}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 11}, {0, 11}}}
      }
    )
  );

  do_parser_test(
    u8"let x: fn(",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 10}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 3},
            std::vector<std::unique_ptr<hlir::type>>{},
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_param_name_expected>(smr{{0, 3}, {0, 3}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 10}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_colon) {
  do_parser_test(
    u8"let x: fn(x!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 11}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 4},
            test::make_vector<std::unique_ptr<hlir::type>>(
              nullptr
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_param_type_expected>(smr{{0, 4}, {0, 5}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 12}, {0, 12}}}
      }
    )
  );

  do_parser_test(
    u8"let x: fn(x",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 11}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 4},
            test::make_vector<std::unique_ptr<hlir::type>>(
              nullptr
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_param_type_expected>(smr{{0, 4}, {0, 4}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 11}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_type) {
  do_parser_test(
    u8"let x: fn(x: !",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 5},
            test::make_vector<std::unique_ptr<hlir::type>>(
              nullptr
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::type_name_expected>(smr{{0, 6}, {0, 7}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 14}, {0, 14}}}
      }
    )
  );

  do_parser_test(
    u8"let x: fn(x:",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 5},
            test::make_vector<std::unique_ptr<hlir::type>>(
              nullptr
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::type_name_expected>(smr{{0, 5}, {0, 5}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 12}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_param_comma) {
  do_parser_test(
    u8"let x: fn(x: u64!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 16}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 9},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_param_next_expected>(smr{{0, 9}, {0, 10}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 17}, {0, 17}}}
      }
    )
  );

  do_parser_test(
    u8"let x: fn(x: u64",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 16}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 9},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_param_next_expected>(smr{{0, 9}, {0, 9}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 16}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_return_colon) {
  do_parser_test(
    u8"let x: fn((x: u64)!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 18}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 11},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_rparen_expected>(smr{{0, 11}, {0, 12}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 19}, {0, 19}}}
      }
    )
  );

  do_parser_test(
    u8"let x: fn((x: u64)",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 18}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 11},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::fn_type_rparen_expected>(smr{{0, 11}, {0, 11}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 18}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_return_type) {
  do_parser_test(
    u8"let x: fn((x: u64):!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 19}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 12},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            nullptr
          ), diags(
            std::make_unique<diag::type_name_expected>(smr{{0, 12}, {0, 13}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 20}, {0, 20}}}
      }
    )
  );

  do_parser_test(
    u8"let x: fn((x: u64):",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 19}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 12},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            nullptr
          ), diags(
            std::make_unique<diag::type_name_expected>(smr{{0, 12}, {0, 12}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 19}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_extra_rparen) {
  do_parser_test(
    u8"let x: fn((x: u64): u64!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 23}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 16},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
          ), diags(
            std::make_unique<diag::fn_type_rparen_expected>(smr{{0, 16}, {0, 17}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 24}, {0, 24}}}
      }
    )
  );

  do_parser_test(
    u8"let x: fn((x: u64): u64",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 23}, u8"x",
          with_diags(std::make_unique<hlir::fn_type>(sm{0, 16},
            test::make_vector<std::unique_ptr<hlir::type>>(
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
            ),
            std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false)
          ), diags(
            std::make_unique<diag::fn_type_rparen_expected>(smr{{0, 16}, {0, 16}})
          )),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 23}}}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // fn_type

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Pointer types

BOOST_AUTO_TEST_SUITE(ptr_type)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: &u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 12}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 4},
            std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_mut) {
  do_parser_test(
    u8"let x: &mut u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 16}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 8},
            std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
            true
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_const) {
  do_parser_test(
    u8"let x: &const u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 18}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 10},
            std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_ptr) {
  do_parser_test(
    u8"let x: &&u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 13}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 5},
            std::make_unique<hlir::ptr_type>(sm{0, 4},
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              false
            ),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );

  do_parser_test(
    u8"let x: & &u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 14}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 6},
            std::make_unique<hlir::ptr_type>(sm{0, 4},
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              false
            ),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_ptr_const) {
  do_parser_test(
    u8"let x: &&const u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 19}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 11},
            std::make_unique<hlir::ptr_type>(sm{0, 10},
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              false
            ),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );

  do_parser_test(
    u8"let x: & &const u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 20}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 12},
            std::make_unique<hlir::ptr_type>(sm{0, 10},
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              false
            ),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_ptr_mut) {
  do_parser_test(
    u8"let x: &&mut u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 17}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 9},
            std::make_unique<hlir::ptr_type>(sm{0, 8},
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              true
            ),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );

  do_parser_test(
    u8"let x: & &mut u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 18}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 10},
            std::make_unique<hlir::ptr_type>(sm{0, 8},
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              true
            ),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_mut_ptr) {
  do_parser_test(
    u8"let x: &mut &u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 17}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 9},
            std::make_unique<hlir::ptr_type>(sm{0, 4},
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              false
            ),
            true
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(ptr_mut_ptr_mut) {
  do_parser_test(
    u8"let x: &mut &mut u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 21}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 13},
            std::make_unique<hlir::ptr_type>(sm{0, 8},
              std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
              true
            ),
            true
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_pointee) {
  do_parser_test(
    u8"let x: &!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 8}, u8"x",
          with_diags(
            std::make_unique<hlir::ptr_type>(sm{0, 1}, nullptr, false),
            diags(std::make_unique<diag::type_name_expected>(smr{{0, 1}, {0, 2}}))
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 9}, {0, 9}}}
      }
    )
  );

  do_parser_test(
    u8"let x: &",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 8}, u8"x",
          with_diags(
            std::make_unique<hlir::ptr_type>(sm{0, 1}, nullptr, false),
            diags(std::make_unique<diag::type_name_expected>(smr{{0, 1}, {0, 1}}))
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 8}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_pointee_double) {
  do_parser_test(
    u8"let x: &&!",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 9}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 2},
            with_diags(
              std::make_unique<hlir::ptr_type>(sm{0, 1}, nullptr, false),
              diags(std::make_unique<diag::type_name_expected>(smr{{0, 1}, {0, 2}}))
            ),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 10}, {0, 10}}}
      }
    )
  );

  do_parser_test(
    u8"let x: &&",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 9}, u8"x",
          std::make_unique<hlir::ptr_type>(sm{0, 2},
            with_diags(
              std::make_unique<hlir::ptr_type>(sm{0, 1}, nullptr, false),
              diags(std::make_unique<diag::type_name_expected>(smr{{0, 1}, {0, 1}}))
            ),
            false
          ),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 9}}}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // ptr_type

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Named types

BOOST_AUTO_TEST_CASE(id_type) {
  do_parser_test(
    u8"let x: $A;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 10}, u8"x",
          std::make_unique<hlir::id_type>(sm{0, 2}, u8"A"),
          std::nullopt,
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // types

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: STATEMENTS
///////////////////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(stmts)

BOOST_AUTO_TEST_CASE(fn_def) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\tfn bar() {}\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{2, 1}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{2, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::def_stmt>(sm{0, 11},
                std::make_unique<hlir::fn_def>(sm{0, 11}, u8"bar",
                  std::vector<hlir::fn_def::param>{},
                  std::nullopt,
                  std::make_unique<hlir::block>(sm{0, 2},
                    std::vector<std::unique_ptr<hlir::stmt>>{},
                    std::nullopt
                  )
                )
              )
            ),
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(var_def) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\tlet x: u64;\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{2, 1}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{2, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::def_stmt>(sm{0, 11},
                std::make_unique<hlir::var_def>(sm{0, 11}, u8"x",
                  std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
                  std::nullopt,
                  hlir::var_def::mutable_spec_t::none
                )
              )
            ),
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(class_def) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\tclass A {}\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{2, 1}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{2, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::def_stmt>(sm{0, 10},
                std::make_unique<hlir::class_def>(sm{0, 10}, u8"A",
                  std::vector<std::unique_ptr<hlir::def>>{}
                )
              )
            ),
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(expr_stmt) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x;\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{2, 1}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{2, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::expr_stmt>(sm{0, 3},
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"x")
              )
            ),
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(assign_stmt) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x = $y;\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{2, 1}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{2, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::assign_stmt>(sm{0, 8},
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"x"),
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"y")
              )
            ),
            std::nullopt
          )
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_semicolon_after_expr) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x @\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{1, 3}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          with_diags(std::make_unique<hlir::block>(sm{1, 3},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"x")
              )
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::block_rbrace_expected>(smr{{1, 4}, {1, 5}})
          ))
        ),
        .panic_seq = {{{1, 5}, {2, 1}, {2, 1}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{1, 3}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          with_diags(std::make_unique<hlir::block>(sm{1, 3},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"x")
              )
            ),
            std::nullopt
          ), diags(
            std::make_unique<diag::block_rbrace_expected>(smr{{1, 3}, {1, 3}})
          ))
        ),
        .panic_seq = {{{1, 3}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_assign_value) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x = @\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{1, 5}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{1, 5},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              with_diags(std::make_unique<hlir::assign_stmt>(sm{0, 4},
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"x"),
                nullptr
              ), diags(
                std::make_unique<diag::expr_expected>(smr{{0, 5}, {0, 6}})
              ))
            ),
            std::nullopt
          )
        ),
        .panic_seq = {{{1, 7}, {2, 1}, {2, 1}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x =",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{1, 5}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{1, 5},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              with_diags(std::make_unique<hlir::assign_stmt>(sm{0, 4},
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"x"),
                nullptr
              ), diags(
                std::make_unique<diag::expr_expected>(smr{{0, 4}, {0, 4}})
              ))
            ),
            std::nullopt
          )
        ),
        .panic_seq = {{{1, 5}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_assign_semicolon) {
  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x = $y @\n"
    u8"}",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{1, 8}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{1, 8},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              with_diags(std::make_unique<hlir::assign_stmt>(sm{0, 7},
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"x"),
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"y")
              ), diags(
                std::make_unique<diag::assign_semicolon_expected>(smr{{0, 8}, {0, 9}})
              ))
            ),
            std::nullopt
          )
        ),
        .panic_seq = {{{1, 10}, {2, 1}, {2, 1}}}
      }
    )
  );

  do_parser_test(
    u8"fn foo() {\n"
    u8"\t$x = $y",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::fn_def>(sm{1, 8}, u8"foo",
          std::vector<hlir::fn_def::param>{},
          std::nullopt,
          std::make_unique<hlir::block>(sm{1, 8},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              with_diags(std::make_unique<hlir::assign_stmt>(sm{0, 7},
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"x"),
                std::make_unique<hlir::id_expr>(sm{0, 2}, u8"y")
              ), diags(
                std::make_unique<diag::assign_semicolon_expected>(smr{{0, 7}, {0, 7}})
              ))
            ),
            std::nullopt
          )
        ),
        .panic_seq = {{{1, 8}}}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // stmts

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: EXPRESSIONS
///////////////////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(exprs)

BOOST_AUTO_TEST_CASE(int_literal) {
  do_parser_test(
    u8"let x: u64 = 179;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 17}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::int_literal>(sm{0, 3}, 179, std::nullopt),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(int_literal_type_suffix) {
  do_parser_test(
    u8"let x: u64 = 179u64;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 20}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::int_literal>(sm{0, 6},
            179,
            int_type_suffix{int_width_t{64}, false}
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(bool_literal) {
  do_parser_test(
    u8"let x: bool = true;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 19}, u8"x",
          std::make_unique<hlir::bool_type>(sm{0, 4}),
          std::make_unique<hlir::bool_literal>(sm{0, 4}, true),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );

  do_parser_test(
    u8"let x: bool = false;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 20}, u8"x",
          std::make_unique<hlir::bool_type>(sm{0, 4}),
          std::make_unique<hlir::bool_literal>(sm{0, 5}, false),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(id_expr) {
  do_parser_test(
    u8"let x: u64 = $y;",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 16}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::id_expr>(sm{0, 2}, u8"y"),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

// BOOST_AUTO_TEST_SUITE(cmp_expr)

// BOOST_AUTO_TEST_CASE(lt) {
//   do_parser_test(
//     u8"let x: u64 = 0 < 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::lt,
//               make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//               src_loc{f, {1, 16}, {1, 17}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(gt) {
//   do_parser_test(
//     u8"let x: u64 = 0 > 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::gt,
//               make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//               src_loc{f, {1, 16}, {1, 17}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(le) {
//   do_parser_test(
//     u8"let x: u64 = 0 <= 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::le,
//               make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(ge) {
//   do_parser_test(
//     u8"let x: u64 = 0 >= 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::ge,
//               make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(eq) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(chain_lt) {
//   do_parser_test(
//     u8"let x: u64 = 0 < 0 <= 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 29}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::lt,
//               make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//               src_loc{f, {1, 16}, {1, 17}}
//             },
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::le,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             },
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 28}, {1, 29}}, 0),
//               src_loc{f, {1, 25}, {1, 27}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(chain_gt) {
//   do_parser_test(
//     u8"let x: u64 = 0 > 0 == 0 >= 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 29}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::gt,
//               make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//               src_loc{f, {1, 16}, {1, 17}}
//             },
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             },
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::ge,
//               make<hlir::int_literal>(src_loc{f, {1, 28}, {1, 29}}, 0),
//               src_loc{f, {1, 25}, {1, 27}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(chain_bad) {
//   do_parser_test(
//     u8"let x: u64 = 0 < 0 >= 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::lt,
//               make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//               src_loc{f, {1, 16}, {1, 17}}
//             },
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::ge,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     ),
//     diags(
//       std::make_unique<diag::bad_comparison_chain>(
//         src_loc{f, {1, 14}, {1, 24}},
//         src_loc{f, {1, 16}, {1, 17}},
//         src_loc{f, {1, 20}, {1, 22}},
//         true,
//         false,
//         true
//       )
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 > 0 <= 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::gt,
//               make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//               src_loc{f, {1, 16}, {1, 17}}
//             },
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::le,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     ),
//     diags(
//       std::make_unique<diag::bad_comparison_chain>(
//         src_loc{f, {1, 14}, {1, 24}},
//         src_loc{f, {1, 20}, {1, 22}},
//         src_loc{f, {1, 16}, {1, 17}},
//         false,
//         true,
//         false
//       )
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(missing_right) {
//   do_parser_test(
//     u8"let x: u64 = 0 < @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 <", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(missing_right_chain) {
//   do_parser_test(
//     u8"let x: u64 = 0 < 0 <= @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 23}, {1, 24}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 < 0 <=", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 22}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_SUITE_END() // cmp_expr

// BOOST_AUTO_TEST_CASE(or_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 || @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ||", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 && @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 &&", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^^ @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^^", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 != @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 !=", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 + @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 +", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 - @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 -", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 * @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 *", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 /", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 %", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 |", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 &", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 19}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 <<", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >>", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neg_expr) {
//   do_parser_test(
//     u8"let x: u64 = -0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//           make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = -@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = -", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bnot_expr) {
//   do_parser_test(
//     u8"let x: u64 = ~0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//           make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = ~@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = ~", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(not_expr) {
//   do_parser_test(
//     u8"let x: u64 = !0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//           make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = !@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = !", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
//     )
//   );
// }

// BOOST_AUTO_TEST_SUITE(addr_expr)

// BOOST_AUTO_TEST_CASE(basic) {
//   do_parser_test(
//     u8"let x: u64 = &0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//           make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(basic_mut) {
//   do_parser_test(
//     u8"let x: u64 = &mut 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//           true
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &mut @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &mut", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(basic_const) {
//   do_parser_test(
//     u8"let x: u64 = &const 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &const @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 21}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &const", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 20}, {1, 20}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(double_amp) {
//   do_parser_test(
//     u8"let x: u64 = &&0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//           make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//             make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//             false
//           ),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 17}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 16}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(double_amp_mut) {
//   do_parser_test(
//     u8"let x: u64 = &&mut 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
//             true
//           ),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&mut @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&mut", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 19}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(double_amp_const) {
//   do_parser_test(
//     u8"let x: u64 = &&const 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 23}},
//             make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0),
//             false
//           ),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&const @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 22}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&const", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 21}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_SUITE_END() // addr_expr

// BOOST_AUTO_TEST_CASE(deref_expr) {
//   do_parser_test(
//     u8"let x: u64 = *0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//           make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = *@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 16}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = *", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 15}, {1, 15}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(named_member_expr) {
//   do_parser_test(
//     u8"let x: u64 = $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::named_member_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::named_var_ref>(src_loc{f, {1, 14}, {1, 16}}, u8"y"),
//           u8"foo"
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = $y.$foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::named_member_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::named_var_ref>(src_loc{f, {1, 14}, {1, 16}}, u8"y"),
//           u8"foo"
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = $y.@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::member_expr_name_expected>(
//         src_loc{f, {1, 17}, {1, 18}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = $y.", f,
//     syms(),
//     diags(
//       std::make_unique<diag::member_expr_name_expected>(
//         src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_SUITE(call_expr)

// BOOST_AUTO_TEST_CASE(no_args) {
//   do_parser_test(
//     u8"let x: u64 = 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::call_expr>(src_loc{f, {1, 14}, {1, 17}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           std::vector<hlir::call_expr::arg>{}
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(one_arg) {
//   do_parser_test(
//     u8"let x: u64 = 0(0);", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::call_expr>(src_loc{f, {1, 14}, {1, 18}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_vector<hlir::call_expr::arg>(
//             hlir::call_expr::arg{
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0)
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(two_args) {
//   do_parser_test(
//     u8"let x: u64 = 0(0, 0);", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::call_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_vector<hlir::call_expr::arg>(
//             hlir::call_expr::arg{
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0)
//             },
//             hlir::call_expr::arg{
//               make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(arg_and_comma) {
//   do_parser_test(
//     u8"let x: u64 = 0(0,);", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::call_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_vector<hlir::call_expr::arg>(
//             hlir::call_expr::arg{
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0)
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(no_args_but_comma) {
//   do_parser_test(
//     u8"let x: u64 = 0(,);", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(missing_first_arg) {
//   do_parser_test(
//     u8"let x: u64 = 0(@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 17}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0(", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 16}, {1, 16}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(missing_comma) {
//   do_parser_test(
//     u8"let x: u64 = 0(0@", f,
//     syms(),
//     diags(
//       std::make_unique<diag::call_expr_arg_expected>(
//         src_loc{f, {1, 17}, {1, 18}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0(0", f,
//     syms(),
//     diags(
//       std::make_unique<diag::call_expr_arg_expected>(
//         src_loc{f, {1, 17}, {1, 17}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(missing_next_arg) {
//   do_parser_test(
//     u8"let x: u64 = 0(0, @", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 19}, {1, 20}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0(0,", f,
//     syms(),
//     diags(
//       std::make_unique<diag::expr_expected>(src_loc{f, {1, 18}, {1, 18}})
//     )
//   );
// }

// BOOST_AUTO_TEST_SUITE_END() // call_expr

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Blocks

BOOST_AUTO_TEST_SUITE(blocks)

BOOST_AUTO_TEST_CASE(empty) {
  do_parser_test(
    u8"let x: u64 = {};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 16}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{0, 2},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::nullopt
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(one_stmt) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\tlet y: bool;\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{2, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{2, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::def_stmt>(sm{0, 12},
                std::make_unique<hlir::var_def>(sm{0, 12}, u8"y",
                  std::make_unique<hlir::bool_type>(sm{0, 4}),
                  std::nullopt,
                  hlir::var_def::mutable_spec_t::none
                )
              )
            ),
            std::nullopt
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );

  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t0;\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{2, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{2, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt)
              )
            ),
            std::nullopt
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(two_stmts) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\tlet y: bool;\n"
    u8"\tlet z: bool;\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{3, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{3, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::def_stmt>(sm{0, 12},
                std::make_unique<hlir::var_def>(sm{0, 12}, u8"y",
                  std::make_unique<hlir::bool_type>(sm{0, 4}),
                  std::nullopt,
                  hlir::var_def::mutable_spec_t::none
                )
              ),
              std::make_unique<hlir::def_stmt>(sm{0, 12},
                std::make_unique<hlir::var_def>(sm{0, 12}, u8"z",
                  std::make_unique<hlir::bool_type>(sm{0, 4}),
                  std::nullopt,
                  hlir::var_def::mutable_spec_t::none
                )
              )
            ),
            std::nullopt
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );

  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t0;\n"
    u8"\t0;\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{3, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{3, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt)
              ),
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt)
              )
            ),
            std::nullopt
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(nested_block) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t{}\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{2, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{2, 1},
            std::vector<std::unique_ptr<hlir::stmt>>{},
            std::make_unique<hlir::block>(sm{0, 2},
              std::vector<std::unique_ptr<hlir::stmt>>{},
              std::nullopt
            )
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(block_then_stmt) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t{}\n"
    u8"\tlet y: bool;\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{3, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{3, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::block>(sm{0, 2},
                  std::vector<std::unique_ptr<hlir::stmt>>{},
                  std::nullopt
                )
              ),
              std::make_unique<hlir::def_stmt>(sm{0, 12},
                std::make_unique<hlir::var_def>(sm{0, 12}, u8"y",
                  std::make_unique<hlir::bool_type>(sm{0, 4}),
                  std::nullopt,
                  hlir::var_def::mutable_spec_t::none
                )
              )
            ),
            std::nullopt
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(stmt_then_block) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\tlet y: bool;\n"
    u8"\t{}\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{3, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{3, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::def_stmt>(sm{0, 12},
                std::make_unique<hlir::var_def>(sm{0, 12}, u8"y",
                  std::make_unique<hlir::bool_type>(sm{0, 4}),
                  std::nullopt,
                  hlir::var_def::mutable_spec_t::none
                )
              )
            ),
            std::make_unique<hlir::block>(sm{0, 2},
              std::vector<std::unique_ptr<hlir::stmt>>{},
              std::nullopt
            )
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(two_nested_blocks) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t{}\n"
    u8"\t{}\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{3, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{3, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::block>(sm{0, 2},
                  std::vector<std::unique_ptr<hlir::stmt>>{},
                  std::nullopt
                )
              )
            ),
            std::make_unique<hlir::block>(sm{0, 2},
              std::vector<std::unique_ptr<hlir::stmt>>{},
              std::nullopt
            )
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(block_as_stmt) {
  do_parser_test(
    u8"let x: u64 = {\n"
    u8"\t{}\n"
    // u8"\t-0;\n"
    u8"\t0;\n"
    u8"};",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{3, 2}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::block>(sm{3, 1},
            test::make_vector<std::unique_ptr<hlir::stmt>>(
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::block>(sm{0, 2},
                  std::vector<std::unique_ptr<hlir::stmt>>{},
                  std::nullopt
                )
              ),
              std::make_unique<hlir::expr_stmt>(sm{0, 2},
                std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt)
              )
            ),
            std::nullopt
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

// BOOST_AUTO_TEST_CASE(block_as_expr) {
//   do_parser_test(
//     u8"let x: u64 = {\n"
//     u8"\t({}) - 0;\n"
//     u8"};", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::block>(src_loc{f, {1, 14}, {3, 2}},
//           test::make_vector<hlir_ptr<hlir::stmt>>(
//             make<hlir::expr_stmt>(
//               make<hlir::sub_expr>(src_loc{f, {2, 2}, {2, 10}},
//                 make<hlir::block>(src_loc{f, {2, 3}, {2, 5}}),
//                 make<hlir::int_literal>(src_loc{f, {2, 9}, {2, 10}}, 0)
//               )
//             )
//           ),
//           nullptr
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

BOOST_AUTO_TEST_SUITE_END() // blocks

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Parentheses

BOOST_AUTO_TEST_SUITE(parens)

BOOST_AUTO_TEST_CASE(basic) {
  do_parser_test(
    u8"let x: u64 = (0);",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 17}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::parens_expr>(sm{0, 3},
            std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt)
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(double_parens) {
  do_parser_test(
    u8"let x: u64 = ((0));",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 19}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          std::make_unique<hlir::parens_expr>(sm{0, 5},
            std::make_unique<hlir::parens_expr>(sm{0, 3},
              std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt)
            )
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {}
      }
    )
  );
}

// BOOST_AUTO_TEST_CASE(as_operands) {
//   do_parser_test(
//     u8"let x: u64 = (0) + (0);", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

BOOST_AUTO_TEST_CASE(missing_expr) {
  do_parser_test(
    u8"let x: u64 = (@",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 14}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          with_diags(
            std::make_unique<hlir::parens_expr>(sm{0, 1}, nullptr),
            diags(std::make_unique<diag::expr_expected>(smr{{0, 1}, {0, 2}}))
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 15}, {0, 15}}}
      }
    )
  );

  do_parser_test(
    u8"let x: u64 = (",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 14}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          with_diags(
            std::make_unique<hlir::parens_expr>(sm{0, 1}, nullptr),
            diags(std::make_unique<diag::expr_expected>(smr{{0, 1}, {0, 1}}))
          ),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 14}}}
      }
    )
  );
}

BOOST_AUTO_TEST_CASE(missing_rparen) {
  do_parser_test(
    u8"let x: u64 = (0@",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 15}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          with_diags(std::make_unique<hlir::parens_expr>(sm{0, 2},
            std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt)
          ), diags(
            std::make_unique<diag::expr_rparen_expected>(smr{{0, 2}, {0, 3}})
          )),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 16}, {0, 16}}}
      }
    )
  );

  do_parser_test(
    u8"let x: u64 = (0",
    test::make_vector<tl>(
      tl{
        .preceding_gap = {0, 0},
        .def = std::make_unique<hlir::var_def>(sm{0, 15}, u8"x",
          std::make_unique<hlir::int_type>(sm{0, 3}, int_width_t{64}, false),
          with_diags(std::make_unique<hlir::parens_expr>(sm{0, 2},
            std::make_unique<hlir::int_literal>(sm{0, 1}, 0, std::nullopt)
          ), diags(
            std::make_unique<diag::expr_rparen_expected>(smr{{0, 2}, {0, 2}})
          )),
          hlir::var_def::mutable_spec_t::none
        ),
        .panic_seq = {{{0, 15}}}
      }
    )
  );
}

BOOST_AUTO_TEST_SUITE_END() // parens

BOOST_AUTO_TEST_SUITE_END() // exprs

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Associativity

// BOOST_AUTO_TEST_SUITE(associativity)

// BOOST_AUTO_TEST_CASE(or_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 != 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 & 0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 << 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_expr) {
//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_SUITE_END() // associativity

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Precedence

// BOOST_AUTO_TEST_SUITE(precedence)

// BOOST_AUTO_TEST_CASE(cmp_or) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             test::make_small_vector<hlir::cmp_expr::rel, 1>(
//               hlir::cmp_expr::rel{
//                 hlir::cmp_expr::rel_kind::eq,
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 src_loc{f, {1, 16}, {1, 18}}
//               }
//             )
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 || 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::cmp_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             test::make_small_vector<hlir::cmp_expr::rel, 1>(
//               hlir::cmp_expr::rel{
//                 hlir::cmp_expr::rel_kind::eq,
//                 make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
//                 src_loc{f, {1, 21}, {1, 23}}
//               }
//             )
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_and) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             test::make_small_vector<hlir::cmp_expr::rel, 1>(
//               hlir::cmp_expr::rel{
//                 hlir::cmp_expr::rel_kind::eq,
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 src_loc{f, {1, 16}, {1, 18}}
//               }
//             )
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 && 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::cmp_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             test::make_small_vector<hlir::cmp_expr::rel, 1>(
//               hlir::cmp_expr::rel{
//                 hlir::cmp_expr::rel_kind::eq,
//                 make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
//                 src_loc{f, {1, 21}, {1, 23}}
//               }
//             )
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_xor) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             test::make_small_vector<hlir::cmp_expr::rel, 1>(
//               hlir::cmp_expr::rel{
//                 hlir::cmp_expr::rel_kind::eq,
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 src_loc{f, {1, 16}, {1, 18}}
//               }
//             )
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::cmp_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             test::make_small_vector<hlir::cmp_expr::rel, 1>(
//               hlir::cmp_expr::rel{
//                 hlir::cmp_expr::rel_kind::eq,
//                 make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
//                 src_loc{f, {1, 21}, {1, 23}}
//               }
//             )
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_neq) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 != 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 != 0 == 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_add) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 + 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_sub) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 - 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_mul) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 * 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_div) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0),
//               src_loc{f, {1, 20}, {1, 22}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
//               src_loc{f, {1, 21}, {1, 23}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
//               src_loc{f, {1, 21}, {1, 23}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
//               src_loc{f, {1, 21}, {1, 23}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 == 0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//                 make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//                 make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0),
//               src_loc{f, {1, 21}, {1, 23}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
//               src_loc{f, {1, 17}, {1, 19}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
//               src_loc{f, {1, 17}, {1, 19}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
//               src_loc{f, {1, 17}, {1, 19}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
//               src_loc{f, {1, 17}, {1, 19}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0),
//               src_loc{f, {1, 18}, {1, 20}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 == 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0),
//               src_loc{f, {1, 17}, {1, 19}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 == $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//                 make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//                 u8"foo"
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(cmp_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 == $y();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::cmp_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           test::make_small_vector<hlir::cmp_expr::rel, 1>(
//             hlir::cmp_expr::rel{
//               hlir::cmp_expr::rel_kind::eq,
//               make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 23}},
//                 make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//                 std::vector<hlir::call_expr::arg>{}
//               ),
//               src_loc{f, {1, 16}, {1, 18}}
//             }
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_and) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::and_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 && 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_xor) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 ^^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 || 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_neq) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::neq_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 != 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_add) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 + 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_sub) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 - 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_mul) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 * 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_div) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 || 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 || $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(or_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 || 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::or_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_xor) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 ^^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 && 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_neq) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::neq_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 != 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_add) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 + 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_sub) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 - 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_mul) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 * 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_div) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 && 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 && $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(and_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 && 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::and_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_neq) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::neq_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 != 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_add) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 + 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_sub) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 - 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_mul) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 * 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_div) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 ^^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(xor_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^^ 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::xor_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_add) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::add_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 + 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_sub) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::sub_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 - 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_mul) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mul_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 * 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_div) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::div_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mod_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::bor_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::band_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::bxor_expr>(src_loc{f, {1, 19}, {1, 24}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 23}, {1, 24}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::lsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::rsh_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::lrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::rrot_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 20}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 24}, {1, 25}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 != 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 != $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neq_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 != 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neq_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_sub) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 - 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_mul) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mul_expr>(src_loc{f, {1, 18}, {1, 23}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 * 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_div) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::div_expr>(src_loc{f, {1, 18}, {1, 23}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mod_expr>(src_loc{f, {1, 18}, {1, 23}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 + 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 + 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 + 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 + 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 + 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 + 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 + 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 + 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 + $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(add_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 + 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::add_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_mul) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mul_expr>(src_loc{f, {1, 18}, {1, 23}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 * 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_div) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::div_expr>(src_loc{f, {1, 18}, {1, 23}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::mod_expr>(src_loc{f, {1, 18}, {1, 23}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 - 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 - 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 - 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 - 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 - 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 - 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 - 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 - 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 - $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(sub_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 - 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::sub_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_div) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 / 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 * 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 * 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 * 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 * 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 * 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 * 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 * 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 * 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 * $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mul_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 * 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mul_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_mod) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 % 0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 / 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 / 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 / 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 / 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 / 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 / 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 / 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 / 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 / $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(div_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 / 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::div_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_bor) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 | 0 % 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 % 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 % 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 % 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 % 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 % 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 % 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 % 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 % $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(mod_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 % 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::mod_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_band) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::band_expr>(src_loc{f, {1, 18}, {1, 23}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 & 0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 23}},
//           make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 19}},
//             make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 22}, {1, 23}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 | 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 | 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 | $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bor_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 | 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_bxor) {
//   do_parser_test(
//     u8"let x: u64 = 0 & 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 21}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 & 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 & 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 & 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 & 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 & 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 & 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 & $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(band_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 & 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::band_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_lsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 << 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 20}, {1, 22}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 ^ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 22}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 ^ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 20}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^ $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 24}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 18}, {1, 24}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 18}, {1, 20}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bxor_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 ^ 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bxor_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 18}, {1, 21}},
//             make<hlir::int_literal>(src_loc{f, {1, 18}, {1, 19}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_rsh) {
//   do_parser_test(
//     u8"let x: u64 = 0 << 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 << 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 << 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 << 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 << 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 << $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lsh_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 << 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lsh_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_lrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 >> 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 >> 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 >> 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 >> $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rsh_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 >> 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rsh_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_rrot) {
//   do_parser_test(
//     u8"let x: u64 = 0 </ 0 >/ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0 </ 0;", f,
//     syms(),
//     diags(
//       std::make_unique<diag::var_def_semicolon_expected>(
//         src_loc{f, {1, 21}, {1, 23}})
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 </ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 </ $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(lrot_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 </ 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::lrot_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_neg) {
//   do_parser_test(
//     u8"let x: u64 = -0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_bnot) {
//   do_parser_test(
//     u8"let x: u64 = ~0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_not) {
//   do_parser_test(
//     u8"let x: u64 = !0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_addr) {
//   do_parser_test(
//     u8"let x: u64 = &0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 17}},
//             make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 17}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               false
//             ),
//             false
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 21}, {1, 22}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_deref) {
//   do_parser_test(
//     u8"let x: u64 = *0 >/ 0;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 16}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0)
//           ),
//           make<hlir::int_literal>(src_loc{f, {1, 20}, {1, 21}}, 0)
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_member) {
//   do_parser_test(
//     u8"let x: u64 = 0 >/ $y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 25}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::named_member_expr>(src_loc{f, {1, 19}, {1, 25}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 19}, {1, 21}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(rrot_call) {
//   do_parser_test(
//     u8"let x: u64 = 0 >/ 0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::rrot_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::int_literal>(src_loc{f, {1, 14}, {1, 15}}, 0),
//           make<hlir::call_expr>(src_loc{f, {1, 19}, {1, 22}},
//             make<hlir::int_literal>(src_loc{f, {1, 19}, {1, 20}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neg_member) {
//   do_parser_test(
//     u8"let x: u64 = -$y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(neg_call) {
//   do_parser_test(
//     u8"let x: u64 = -0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::neg_expr>(src_loc{f, {1, 14}, {1, 18}},
//           make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bnot_member) {
//   do_parser_test(
//     u8"let x: u64 = ~$y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(bnot_call) {
//   do_parser_test(
//     u8"let x: u64 = ~0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::bnot_expr>(src_loc{f, {1, 14}, {1, 18}},
//           make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(not_member) {
//   do_parser_test(
//     u8"let x: u64 = !$y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(not_call) {
//   do_parser_test(
//     u8"let x: u64 = !0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::not_expr>(src_loc{f, {1, 14}, {1, 18}},
//           make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(addr_member) {
//   do_parser_test(
//     u8"let x: u64 = &$y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
//             u8"foo"
//           ),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&$y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 22}},
//           make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 22}},
//             make<hlir::named_member_expr>(src_loc{f, {1, 16}, {1, 22}},
//               make<hlir::named_var_ref>(src_loc{f, {1, 16}, {1, 18}}, u8"y"),
//               u8"foo"
//             ),
//             false
//           ),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(addr_call) {
//   do_parser_test(
//     u8"let x: u64 = &0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 18}},
//           make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           ),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );

//   do_parser_test(
//     u8"let x: u64 = &&0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::addr_expr>(src_loc{f, {1, 14}, {1, 19}},
//           make<hlir::addr_expr>(src_loc{f, {1, 15}, {1, 19}},
//             make<hlir::call_expr>(src_loc{f, {1, 16}, {1, 19}},
//               make<hlir::int_literal>(src_loc{f, {1, 16}, {1, 17}}, 0),
//               std::vector<hlir::call_expr::arg>{}
//             ),
//             false
//           ),
//           false
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(deref_member) {
//   do_parser_test(
//     u8"let x: u64 = *$y.foo;", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 21}},
//           make<hlir::named_member_expr>(src_loc{f, {1, 15}, {1, 21}},
//             make<hlir::named_var_ref>(src_loc{f, {1, 15}, {1, 17}}, u8"y"),
//             u8"foo"
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_CASE(deref_call) {
//   do_parser_test(
//     u8"let x: u64 = *0();", f,
//     syms(
//       sym{u8"x", make<hlir::var_def>(src_loc{f, {1, 5}, {1, 6}},
//         make<hlir::int_type>(src_loc{f, {1, 8}, {1, 11}},
//           int_width_t{64},
//           false
//         ),
//         make<hlir::deref_expr>(src_loc{f, {1, 14}, {1, 18}},
//           make<hlir::call_expr>(src_loc{f, {1, 15}, {1, 18}},
//             make<hlir::int_literal>(src_loc{f, {1, 15}, {1, 16}}, 0),
//             std::vector<hlir::call_expr::arg>{}
//           )
//         ),
//         hlir::var_def::mutable_spec_t::none
//       )}
//     )
//   );
// }

// BOOST_AUTO_TEST_SUITE_END() // precedence

BOOST_AUTO_TEST_SUITE_END() // parser
BOOST_AUTO_TEST_SUITE_END() // test_nycleus
