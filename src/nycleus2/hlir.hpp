#ifndef NYCLEUS2_HLIR_HPP_INCLUDED_88OVKXQ7LGC12D7DZWPXCKNIR
#define NYCLEUS2_HLIR_HPP_INCLUDED_88OVKXQ7LGC12D7DZWPXCKNIR

#include "nycleus2/diagnostic.hpp"
#include "nycleus2/source_location.hpp"
#include "util/bigint.hpp"
#include "util/pp_seq.hpp"
#include "util/small_vector.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <cassert>
#include <concepts>
#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

/** @brief The high-level intermediate representation (HLIR) nodes comprise the actual
 * description of the program, roughly corresponding to a parse tree.
 *
 * Certain nodes have requirements imposed on them. Since HLIR nodes are dumb data holders and
 * do not maintain their own invariants, code that uses HLIR has a responsibility to uphold
 * those requirements. HLIR nodes that violate these requrements are illegal; any appearance of
 * such HLIR nodes during any execution of the compiler is a compiler bug.
 *
 * Some nodes have fields whose values are considered "tainted" if they do not satisfy certain
 * conditions. Tainted values are used to represent ill-formed programs. HLIR with tainted
 * values can be used to discover more diagnostics about an ill-formed program, but cannot be
 * used to emit any output code. Unlike requirements, it is perfectly legal for tainted HLIR to
 * appear given an illegal input program. */
namespace nycleus2::hlir {

/** @brief A panic sequence obtained from parsing a grammatically incorrect node.
 *
 * Whenever the parser encounters a syntax error, it enters panic mode. In panic mode, it keeps
 * consuming tokens until it finds a point where it can resume parsing from what it hopes is a
 * known good state. The tokens consumed during panic mode form a panic sequence, which is
 * stored in the HLIR node followed by the nodes after the panic mode ends. (The panic sequence
 * can be empty if the parser resumes parsing immediately after discovering a syntax error.)
 *
 * The panic sequence object stores a sequence of source metrics. If there was no panic and the
 * node was parsed correctly, the vector is empty. Otherwise, for each token, the metric from
 * the node's start to the end of the token is stored in the vector; at the end, there is a
 * metric from the node's start to the start of the next token after the panic sequence, or to
 * the end of the file if there is no next token. This information helps the parser reuse
 * nodes that contain syntax errors. */
struct panic_seq {
  std::vector<source_metric> metrics;

  [[nodiscard]] bool did_panic() const noexcept {
    return !metrics.empty();
  }

  [[nodiscard]] friend bool operator==(panic_seq const&, panic_seq const&) noexcept = default;
};

/** @brief The base class of all HLIR nodes. */
struct hlir_base: util::hier_base {
  source_metric loc;

  /** @brief The diagnostics associated with this node.
   *
   * The base for the metrics is the start of this node. */
  std::vector<std::unique_ptr<diagnostic>> diags;

  explicit hlir_base(source_metric l) noexcept: loc{l} {}

protected:
  ~hlir_base() noexcept = default;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Types

#define HLIR_TYPES(x, y, z) \
  z(int_type) y \
  x(bool_type) y \
  x(fn_type) y \
  x(ptr_type) y \
  x(id_type)

#define HLIR_TYPE_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(HLIR_TYPES, HLIR_TYPE_FWD_DEF)
#undef HLIR_TYPE_FWD_DEF

template<typename Visitor>
concept type_visitor =
  #define HLIR_TYPE_IS_INVOCABLE(kind) std::invocable<Visitor, kind&>
  PP_SEQ_APPLY(HLIR_TYPES, HLIR_TYPE_IS_INVOCABLE, &&)
  #undef HLIR_TYPE_IS_INVOCABLE
  && util::all_same_v<
    #define HLIR_TYPE_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind&>
    PP_SEQ_LIST(HLIR_TYPES, HLIR_TYPE_INVOKE_RESULT)
    #undef HLIR_TYPE_INVOKE_RESULT
  >;

template<typename Visitor>
concept type_const_visitor =
  #define HLIR_TYPE_IS_CONST_INVOCABLE(kind) std::invocable<Visitor, kind const&>
  PP_SEQ_APPLY(HLIR_TYPES, HLIR_TYPE_IS_CONST_INVOCABLE, &&)
  #undef HLIR_TYPE_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define HLIR_TYPE_CONST_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind const&>
    PP_SEQ_LIST(HLIR_TYPES, HLIR_TYPE_CONST_INVOKE_RESULT)
    #undef HLIR_TYPE_CONST_INVOKE_RESULT
  >;

/** @brief The name of a type. */
struct type: hlir_base {
  using hlir_base::hlir_base;

  virtual ~type() noexcept = default;

  enum class kind_t {
    #define HLIR_TYPE_ENUM(kind) kind
    PP_SEQ_LIST(HLIR_TYPES, HLIR_TYPE_ENUM)
    #undef HLIR_TYPE_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<type_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&, PP_SEQ_FIRST(HLIR_TYPES)&>;

private:
  template<type_visitor Visitor>
  static constexpr bool visit_noexcept{
    #define HLIR_TYPE_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind&>()))
    PP_SEQ_APPLY(HLIR_TYPES, HLIR_TYPE_INVOKE_NOEXCEPT, &&)
    #undef HLIR_TYPE_INVOKE_NOEXCEPT
  };

public:
  template<type_visitor Visitor>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(type::visit_noexcept<Visitor>);

  template<type_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor) noexcept(type::visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<type_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&, PP_SEQ_FIRST(HLIR_TYPES) const&>;

private:
  template<type_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define HLIR_TYPE_CONST_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind const&>()))
    PP_SEQ_APPLY(HLIR_TYPES, HLIR_TYPE_CONST_INVOKE_NOEXCEPT, &&)
    #undef HLIR_TYPE_CONST_INVOKE_NOEXCEPT
  };

public:
  template<type_const_visitor Visitor>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(type::const_visit_noexcept<Visitor>);

  template<type_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(type::const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief The name of an integer type.
 *
 * If the width value is 0, it is considered tainted. */
struct int_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_type;
  }

  int_width_t width;
  bool is_signed;

  explicit int_type(
    source_metric loc,
    int_width_t w,
    bool s
  ) noexcept: type{loc}, width{w}, is_signed{s} {
    assert(width <= max_int_width);
  }
};

/** @brief The name of the Boolean type. */
struct bool_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bool_type;
  }

  using type::type;
};

/** @brief The name of a function reference type.
 *
 * If any parameter types or the return type is a null pointer, the value is tainted. If the
 * return type is an empty optional, there is no return type. */
struct fn_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type;
  }

  std::vector<std::unique_ptr<type>> param_types;
  std::optional<std::unique_ptr<type>> return_type;

  fn_type() noexcept: type{source_metric{0, 0}}, param_types{}, return_type{} {}

  explicit fn_type(
    source_metric loc,
    std::vector<std::unique_ptr<type>> p,
    std::optional<std::unique_ptr<type>> r
  ) noexcept:
    type{loc},
    param_types{std::move(p)},
    return_type{std::move(r)} {}
};

/** @brief The name of a pointer type.
 *
 * If the pointee type is a null pointer, the value is tainted. */
struct ptr_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::ptr_type;
  }

  std::unique_ptr<type> pointee_type;
  bool is_mutable;

  ptr_type() noexcept: type{source_metric{0, 0}}, pointee_type{}, is_mutable{false} {}

  explicit ptr_type(
    source_metric loc,
    std::unique_ptr<type> p,
    bool m
  ) noexcept: type{loc}, pointee_type{std::move(p)}, is_mutable{m} {}
};

/** @brief A type referred to by name. */
struct id_type final: type {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::id_type;
  }

  std::u8string name;

  explicit id_type(
    source_metric loc,
    std::u8string n
  ) noexcept: type{loc}, name{std::move(n)} {}
};

template<type_visitor Visitor>
type::visit_result_t<Visitor> type::mutable_visit(Visitor&& visitor)
    noexcept(type::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_TYPE_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(HLIR_TYPES, HLIR_TYPE_VISIT)
    #undef HLIR_TYPE_VISIT
  }
}

template<type_const_visitor Visitor>
type::const_visit_result_t<Visitor> type::const_visit(Visitor&& visitor) const
    noexcept(type::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_TYPE_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(HLIR_TYPES, HLIR_TYPE_CONST_VISIT)
    #undef HLIR_TYPE_CONST_VISIT
  }
}

#undef HLIR_TYPES

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Expressions

#define HLIR_EXPRS(x, y, z) \
  z(int_literal) y \
  x(bool_literal) y \
  x(id_expr) y \
  x(cmp_expr) y \
  x(or_expr) y \
  x(and_expr) y \
  x(xor_expr) y \
  x(neq_expr) y \
  x(add_expr) y \
  x(sub_expr) y \
  x(mul_expr) y \
  x(div_expr) y \
  x(mod_expr) y \
  x(bor_expr) y \
  x(band_expr) y \
  x(bxor_expr) y \
  x(lsh_expr) y \
  x(rsh_expr) y \
  x(lrot_expr) y \
  x(rrot_expr) y \
  x(neg_expr) y \
  x(bnot_expr) y \
  x(not_expr) y \
  x(addr_expr) y \
  x(deref_expr) y \
  x(member_expr) y \
  x(parens_expr) y \
  x(call_expr) y \
  x(block)

#define HLIR_EXPR_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(HLIR_EXPRS, HLIR_EXPR_FWD_DEF)
#undef HLIR_EXPR_FWD_DEF

template<typename Visitor>
concept expr_visitor =
  #define HLIR_EXPR_IS_INVOCABLE(kind) std::invocable<Visitor, kind&>
  PP_SEQ_APPLY(HLIR_EXPRS, HLIR_EXPR_IS_INVOCABLE, &&)
  #undef HLIR_EXPR_IS_INVOCABLE
  && util::all_same_v<
    #define HLIR_EXPR_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind&>
    PP_SEQ_LIST(HLIR_EXPRS, HLIR_EXPR_INVOKE_RESULT)
    #undef HLIR_EXPR_INVOKE_RESULT
  >;

template<typename Visitor>
concept expr_const_visitor =
  #define HLIR_EXPR_IS_CONST_INVOCABLE(kind) std::invocable<Visitor, kind const&>
  PP_SEQ_APPLY(HLIR_EXPRS, HLIR_EXPR_IS_CONST_INVOCABLE, &&)
  #undef HLIR_EXPR_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define HLIR_EXPR_CONST_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind const&>
    PP_SEQ_LIST(HLIR_EXPRS, HLIR_EXPR_CONST_INVOKE_RESULT)
    #undef HLIR_EXPR_CONST_INVOKE_RESULT
  >;

/** @brief An expression. */
struct expr: hlir_base {
  using hlir_base::hlir_base;

  virtual ~expr() noexcept = default;

  enum class kind_t {
    #define HLIR_EXPR_ENUM(kind) kind
    PP_SEQ_LIST(HLIR_EXPRS, HLIR_EXPR_ENUM)
    #undef HLIR_EXPR_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<expr_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&, PP_SEQ_FIRST(HLIR_EXPRS)&>;

private:
  template<expr_visitor Visitor>
  static constexpr bool visit_noexcept {
    #define HLIR_EXPR_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind&>()))
    PP_SEQ_APPLY(HLIR_EXPRS, HLIR_EXPR_INVOKE_NOEXCEPT, &&)
    #undef HLIR_EXPR_INVOKE_NOEXCEPT
  };

public:
  template<expr_visitor Visitor>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(expr::visit_noexcept<Visitor>);

  template<expr_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor) noexcept(expr::visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<expr_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_EXPRS) const&>;

private:
  template<expr_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define HLIR_EXPR_CONST_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind const&>()))
    PP_SEQ_APPLY(HLIR_EXPRS, HLIR_EXPR_CONST_INVOKE_NOEXCEPT, &&)
    #undef HLIR_EXPR_CONST_INVOKE_NOEXCEPT
  };

public:
  template<expr_const_visitor Visitor>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(expr::const_visit_noexcept<Visitor>);

  template<expr_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(expr::const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief An integer literal. */
struct int_literal final: expr {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_literal;
  }

  util::bigint value;
  std::optional<int_type_suffix> type_suffix;

  explicit int_literal(
    source_metric loc,
    util::bigint val,
    std::optional<int_type_suffix> ts
  ) noexcept: expr{loc}, value{std::move(val)}, type_suffix{ts} {
    assert(!type_suffix || type_suffix->width > 0);
    assert(!type_suffix || type_suffix->width < max_int_width);
  }
};

/** @brief A Boolean literal. */
struct bool_literal final: expr {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bool_literal;
  }

  bool value;

  explicit bool_literal(
    source_metric loc,
    bool val
  ) noexcept: expr{loc}, value{val} {}
};

/** @brief An identifier referrred to by name. */
struct id_expr final: expr {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::id_expr;
  }

  std::u8string name;

  explicit id_expr(
    source_metric loc,
    std::u8string n
  ) noexcept: expr{loc}, name{std::move(n)} {}
};

/** @brief A comparison expression.
 *
 * If any operand is a null pointer, the value is tainted.
 *
 * Requirement: there must be at least one operator.
 *
 * Note that there is no restriction on operators going in different directions. That error is
 * caught at the parsing stage; past that point, differently directed operators are treated
 * normally. */
struct cmp_expr final: expr {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::cmp_expr;
  }

  enum class rel_kind_t { eq, lt, le, gt, ge };

  struct rel {
    rel_kind_t kind;
    std::unique_ptr<expr> right_op;

    explicit rel(
      rel_kind_t k,
      std::unique_ptr<expr> r
    ) noexcept: kind{k}, right_op{std::move(r)} {}
  };

  std::unique_ptr<expr> first_op;
  util::small_vector<rel, 1> rels;

  explicit cmp_expr(
    source_metric loc,
    std::unique_ptr<expr> op,
    util::small_vector<rel, 1> r
  ) noexcept: expr{loc}, first_op{std::move(op)}, rels{std::move(r)} {
    assert(!rels.empty());
  }
};

/** @brief A common base class for expressions based on binary operators.
 *
 * If an operand is a null pointer, the value is tainted. */
struct binary_op_expr: expr {
  std::unique_ptr<expr> left_op;
  std::unique_ptr<expr> right_op;

  explicit binary_op_expr(
    source_metric loc,
    std::unique_ptr<expr> l,
    std::unique_ptr<expr> r
  ) noexcept: expr{loc}, left_op{std::move(l)}, right_op{std::move(r)} {}
};

#define HLIR_EXPR_BINARY(name) struct name final: binary_op_expr { \
  using binary_op_expr::binary_op_expr; \
  \
  [[nodiscard]] virtual kind_t get_kind() const noexcept override { \
    return kind_t::name; \
  } \
}

HLIR_EXPR_BINARY(or_expr);   // ||
HLIR_EXPR_BINARY(and_expr);  // &&
HLIR_EXPR_BINARY(xor_expr);  // ^^
HLIR_EXPR_BINARY(neq_expr);  // !=
HLIR_EXPR_BINARY(add_expr);  // +
HLIR_EXPR_BINARY(sub_expr);  // -
HLIR_EXPR_BINARY(mul_expr);  // *
HLIR_EXPR_BINARY(div_expr);  // /
HLIR_EXPR_BINARY(mod_expr);  // %
HLIR_EXPR_BINARY(bor_expr);  // |
HLIR_EXPR_BINARY(band_expr); // &
HLIR_EXPR_BINARY(bxor_expr); // ^
HLIR_EXPR_BINARY(lsh_expr);  // <<
HLIR_EXPR_BINARY(rsh_expr);  // >>
HLIR_EXPR_BINARY(lrot_expr); // </
HLIR_EXPR_BINARY(rrot_expr); // >/

#undef HLIR_EXPR_BINARY

/** @brief A common base class for expressions based on unary operators.
 *
 * If the operator is a null pointer, the value is tainted. */
struct unary_op_expr: expr {
  std::unique_ptr<expr> op;

  explicit unary_op_expr() noexcept: expr{source_metric{0, 0}} {}

  explicit unary_op_expr(
    source_metric loc,
    std::unique_ptr<expr> o
  ) noexcept: expr{loc}, op{std::move(o)} {}
};

#define HLIR_EXPR_UNARY(name) struct name final: unary_op_expr { \
  using unary_op_expr::unary_op_expr; \
  \
  [[nodiscard]] virtual kind_t get_kind() const noexcept override { \
    return kind_t::name; \
  } \
};

HLIR_EXPR_UNARY(neg_expr);    // -
HLIR_EXPR_UNARY(bnot_expr);   // ~
HLIR_EXPR_UNARY(not_expr);    // !
HLIR_EXPR_UNARY(deref_expr);  // *
HLIR_EXPR_UNARY(parens_expr); // ()

#undef HLIR_EXPR_UNARY

/** @brief An address expression.
 *
 * If the operand is a null pointer, the value is tainted. */
struct addr_expr final: unary_op_expr {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::addr_expr;
  }

  bool is_mutable;

  explicit addr_expr(
    source_metric loc,
    std::unique_ptr<expr> o,
    bool m
  ) noexcept: unary_op_expr{loc, std::move(o)}, is_mutable{m} {}
};

/** @brief A member access expression.
 *
 * If the operand is a null pointer, the value is tainted. */
struct member_expr final: unary_op_expr {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::member_expr;
  }

  std::u8string name;

  explicit member_expr(
    source_metric loc,
    std::unique_ptr<expr> o,
    std::u8string n
  ) noexcept: unary_op_expr{loc, std::move(o)}, name{std::move(n)} {}
};

/** @brief A call expression.
 *
 * If the callee or any arguments are null pointers, the value is tainted. */
struct call_expr final: expr {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::call_expr;
  }

  struct arg {
    std::unique_ptr<expr> value;

    explicit arg(std::unique_ptr<expr> val) noexcept: value{std::move(val)} {}
  };

  std::unique_ptr<expr> callee;
  std::vector<arg> args;

  explicit call_expr(
    source_metric loc,
    std::unique_ptr<expr> c,
    std::vector<arg> a
  ) noexcept: expr{loc}, callee{std::move(c)}, args{std::move(a)} {}
};

// block will be defined after stmt

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Statements

#define HLIR_STMTS(x, y, z) \
  z(expr_stmt) y \
  x(assign_stmt) y \
  x(def_stmt)

#define HLIR_STMT_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(HLIR_STMTS, HLIR_STMT_FWD_DEF)
#undef HLIR_STMT_FWD_DEF

template<typename Visitor>
concept stmt_visitor =
  #define HLIR_STMT_IS_INVOCABLE(kind) std::invocable<Visitor, kind&>
  PP_SEQ_APPLY(HLIR_STMTS, HLIR_STMT_IS_INVOCABLE, &&)
  #undef HLIR_STMT_IS_INVOCABLE
  && util::all_same_v<
    #define HLIR_STMT_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind&>
    PP_SEQ_LIST(HLIR_STMTS, HLIR_STMT_INVOKE_RESULT)
    #undef HLIR_STMT_INVOKE_RESULT
  >;

template<typename Visitor>
concept stmt_const_visitor =
  #define HLIR_STMT_IS_CONST_INVOCABLE(kind) std::invocable<Visitor, kind const&>
  PP_SEQ_APPLY(HLIR_STMTS, HLIR_STMT_IS_CONST_INVOCABLE, &&)
  #undef HLIR_STMT_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define HLIR_STMT_CONST_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind const&>
    PP_SEQ_LIST(HLIR_STMTS, HLIR_STMT_CONST_INVOKE_RESULT)
    #undef HLIR_STMT_CONST_INVOKE_RESULT
  >;

/** @brief A statement. */
struct stmt: hlir_base {
  using hlir_base::hlir_base;

  virtual ~stmt() noexcept = default;

  enum class kind_t {
    #define HLIR_STMT_ENUM(kind) kind
    PP_SEQ_LIST(HLIR_STMTS, HLIR_STMT_ENUM)
    #undef HLIR_STMT_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<stmt_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&, PP_SEQ_FIRST(HLIR_STMTS)&>;

private:
  template<typename Visitor>
  static constexpr bool visit_noexcept{
    #define HLIR_STMT_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind&>()))
    PP_SEQ_APPLY(HLIR_STMTS, HLIR_STMT_INVOKE_NOEXCEPT, &&)
    #undef HLIR_STMT_INVOKE_NOEXCEPT
  };

public:
  template<stmt_visitor Visitor>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(stmt::visit_noexcept<Visitor>);

  template<stmt_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor) noexcept(stmt::visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<stmt_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&&,
    PP_SEQ_FIRST(HLIR_STMTS) const&>;

private:
  template<stmt_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define HLIR_STMT_CONST_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind const&>()))
    PP_SEQ_APPLY(HLIR_STMTS, HLIR_STMT_CONST_INVOKE_NOEXCEPT, &&)
    #undef HLIR_STMT_CONST_INVOKE_NOEXCEPT
  };

public:
  template<stmt_const_visitor Visitor>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(stmt::const_visit_noexcept<Visitor>);

  template<stmt_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(stmt::const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief A block expression.
 *
 * If any statement or the trailing expression is a null pointer, the value is tainted. If the
 * trailing expression is an empty optional, there is no trailing value. */
struct block final: expr {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::block;
  }

  std::vector<std::unique_ptr<stmt>> stmts;
  std::optional<std::unique_ptr<expr>> trail;

  block() noexcept: expr{source_metric{0, 0}}, stmts{}, trail{} {}

  explicit block(
    source_metric loc,
    std::vector<std::unique_ptr<stmt>> s,
    std::optional<std::unique_ptr<expr>> t
  ) noexcept: expr{loc}, stmts{std::move(s)}, trail{std::move(t)} {}
};

template<expr_visitor Visitor>
expr::visit_result_t<Visitor> expr::mutable_visit(Visitor&& visitor)
    noexcept(expr::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_EXPR_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(HLIR_EXPRS, HLIR_EXPR_VISIT)
    #undef HLIR_EXPR_VISIT
  }
}

template<expr_const_visitor Visitor>
expr::const_visit_result_t<Visitor> expr::const_visit(Visitor&& visitor) const
    noexcept(expr::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_EXPR_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(HLIR_EXPRS, HLIR_EXPR_CONST_VISIT)
    #undef HLIR_EXPR_CONST_VISIT
  }
}

#undef HLIR_EXPRS

/** @brief A statement that consists of an expression.
 *
 * If the expression is a null pointer, the value is tainted. */
struct expr_stmt final: stmt {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::expr_stmt;
  }

  std::unique_ptr<expr> body;

  explicit expr_stmt(
    source_metric loc,
    std::unique_ptr<expr> b
  ) noexcept: stmt{loc}, body{std::move(b)} {}
};

/** @brief An assignment statement.
 *
 * If either the target or the value is a null pointer, the value is tainted. */
struct assign_stmt final: stmt {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_stmt;
  }

  std::unique_ptr<expr> target;
  std::unique_ptr<expr> value;

  assign_stmt() noexcept: stmt{source_metric{0, 0}}, target{}, value{} {}

  explicit assign_stmt(
    source_metric loc,
    std::unique_ptr<expr> t,
    std::unique_ptr<expr> v
  ) noexcept: stmt{loc}, target{std::move(t)}, value{std::move(v)} {}
};

// def_stmt will be defined after def

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Definitions

#define HLIR_DEFS(x, y, z) \
  z(fn_def) y \
  x(var_def) y \
  x(class_def)

#define HLIR_DEF_FWD_DEF(kind) struct kind;
PP_SEQ_FOR_EACH(HLIR_DEFS, HLIR_DEF_FWD_DEF)
#undef HLIR_DEF_FWD_DEF

template<typename Visitor>
concept def_visitor =
  #define HLIR_DEF_IS_INVOCABLE(kind) std::invocable<Visitor, kind&>
  PP_SEQ_APPLY(HLIR_DEFS, HLIR_DEF_IS_INVOCABLE, &&)
  #undef HLIR_DEF_IS_INVOCABLE
  && util::all_same_v<
    #define HLIR_DEF_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind&>
    PP_SEQ_LIST(HLIR_DEFS, HLIR_DEF_INVOKE_RESULT)
    #undef HLIR_DEF_INVOKE_RESULT
  >;

template<typename Visitor>
concept def_const_visitor =
  #define HLIR_DEF_IS_CONST_INVOCABLE(kind) std::invocable<Visitor, kind const&>
  PP_SEQ_APPLY(HLIR_DEFS, HLIR_DEF_IS_CONST_INVOCABLE, &&)
  #undef HLIR_DEF_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define HLIR_DEF_CONST_INVOKE_RESULT(kind) std::invoke_result_t<Visitor&&, kind const&>
    PP_SEQ_LIST(HLIR_DEFS, HLIR_DEF_CONST_INVOKE_RESULT)
    #undef HLIR_DEF_CONST_INVOKE_RESULT
  >;

/** @brief A definition.
 *
 * If the name is an empty optional, it is tainted. */
struct def: hlir_base {
  std::optional<std::u8string> name;

  explicit def(
    source_metric loc,
    std::optional<std::u8string> n
  ) noexcept: hlir_base{loc}, name{std::move(n)} {}

  virtual ~def() noexcept = default;

  enum class kind_t {
    #define HLIR_DEF_ENUM(kind) kind
    PP_SEQ_LIST(HLIR_DEFS, HLIR_DEF_ENUM)
    #undef HLIR_DEF_ENUM
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<def_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&, PP_SEQ_FIRST(HLIR_DEFS)&>;

private:
  template<def_visitor Visitor>
  static constexpr bool visit_noexcept{
    #define HLIR_DEF_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind&>()))
    PP_SEQ_APPLY(HLIR_DEFS, HLIR_DEF_INVOKE_NOEXCEPT, &&)
    #undef HLIR_DEF_INVOKE_NOEXCEPT
  };

public:
  template<def_visitor Visitor>
  visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(def::visit_noexcept<Visitor>);

  template<def_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor) noexcept(def::visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<def_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&&, PP_SEQ_FIRST(HLIR_DEFS) const&>;

private:
  template<def_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define HLIR_DEF_CONST_INVOKE_NOEXCEPT(kind) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<kind const&>()))
    PP_SEQ_APPLY(HLIR_DEFS, HLIR_DEF_CONST_INVOKE_NOEXCEPT, &&)
    #undef HLIR_DEF_CONST_INVOKE_NOEXCEPT
  };

public:
  template<def_const_visitor Visitor>
  const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(def::const_visit_noexcept<Visitor>);

  template<def_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
      noexcept(def::const_visit_noexcept<Visitor>) {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

/** @brief A statement that consists of a definition.
 *
 * If the definition is a null pointer, the value is tainted. */
struct def_stmt final: stmt {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::def_stmt;
  }

  std::unique_ptr<def> body;

  explicit def_stmt(
    source_metric loc,
    std::unique_ptr<def> b
  ) noexcept: stmt{loc}, body{std::move(b)} {}
};

template<stmt_visitor Visitor>
stmt::visit_result_t<Visitor> stmt::mutable_visit(Visitor&& visitor)
    noexcept(stmt::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_STMT_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(HLIR_STMTS, HLIR_STMT_VISIT)
    #undef HLIR_STMT_VISIT
  }
}

template<stmt_const_visitor Visitor>
stmt::const_visit_result_t<Visitor> stmt::const_visit(Visitor&& visitor) const
    noexcept(stmt::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_STMT_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(HLIR_STMTS, HLIR_STMT_CONST_VISIT)
    #undef HLIR_STMT_CONST_VISIT
  }
}

#undef HLIR_STMTS

/** @brief A function definition.
 *
 * If any parameter types, the return type, or the block is a null pointer, the value is
 * tainted. If the return type is an empty optional, there is no return type. */
struct fn_def final: def {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def;
  }

  struct param {
    std::optional<std::u8string> name;
    std::unique_ptr<hlir::type> type;

    param() noexcept = default;

    explicit param(
      std::optional<std::u8string> n,
      std::unique_ptr<hlir::type> t
    ) noexcept: name{std::move(n)}, type{std::move(t)} {}
  };

  std::vector<param> params;
  std::optional<std::unique_ptr<type>> return_type;
  std::unique_ptr<expr> body;

  fn_def() noexcept: def{source_metric{0, 0}, std::nullopt}, params{}, return_type{}, body{} {}

  explicit fn_def(
    source_metric loc,
    std::optional<std::u8string> name,
    std::vector<param> p,
    std::optional<std::unique_ptr<type>> rt,
    std::unique_ptr<expr> b
  ) noexcept:
    def{loc, std::move(name)},
    params{std::move(p)},
    return_type{std::move(rt)},
    body{std::move(b)} {}
};

/** @brief A variable definition.
 *
 * If the type or the initializer are null pointers, the value is tainted. If the initializer
 * is an empty optional, there is no initializer. */
struct var_def final: def {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def;
  }

  enum class mutable_spec_t { none, mut, constant };

  std::unique_ptr<hlir::type> type;
  std::optional<std::unique_ptr<expr>> init;
  mutable_spec_t mutable_spec;

  var_def() noexcept: def{source_metric{0, 0}, std::nullopt}, type{}, init{},
    mutable_spec{mutable_spec_t::none} {}

  explicit var_def(
    source_metric loc,
    std::optional<std::u8string> name,
    std::unique_ptr<hlir::type> t,
    std::optional<std::unique_ptr<expr>> i,
    mutable_spec_t ms
  ) noexcept:
    def{loc, std::move(name)},
    type{std::move(t)},
    init{std::move(i)},
    mutable_spec{ms} {}
};

/** @brief A class definition.
 *
 * If a member definition is a null pointer, the value is tainted. */
struct class_def final: def {
  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_def;
  }

  std::vector<std::unique_ptr<def>> members;

  class_def() noexcept: def{source_metric{0, 0}, std::nullopt}, members{} {}

  explicit class_def(
    source_metric loc,
    std::optional<std::u8string> name,
    std::vector<std::unique_ptr<def>> m
  ) noexcept: def{loc, std::move(name)}, members{std::move(m)} {}
};

template<def_visitor Visitor>
def::visit_result_t<Visitor> def::mutable_visit(Visitor&& visitor)
    noexcept(def::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_DEF_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<kind&>(*this));
    PP_SEQ_FOR_EACH(HLIR_DEFS, HLIR_DEF_VISIT)
    #undef HLIR_DEF_VISIT
  }
}

template<def_const_visitor Visitor>
def::const_visit_result_t<Visitor> def::const_visit(Visitor&& visitor) const
    noexcept(def::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define HLIR_DEF_CONST_VISIT(kind) \
      case kind_t::kind: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<kind const&>(*this));
    PP_SEQ_FOR_EACH(HLIR_DEFS, HLIR_DEF_CONST_VISIT)
    #undef HLIR_DEF_CONST_VISIT
  }
}

#undef HLIR_DEF_kinds

} // nycleus2::hlir

#endif
