#ifndef NYCLEUS2_DIAGNOSTIC_HPP_INCLUDED_IJBRPTPTVDDGS5QMUGM98QJFB
#define NYCLEUS2_DIAGNOSTIC_HPP_INCLUDED_IJBRPTPTVDDGS5QMUGM98QJFB

#include "nycleus2/source_location.hpp"
#include "util/pp_seq.hpp"
#include "util/type_traits.hpp"
#include <concepts>
#include <functional>
#include <type_traits>
#include <utility>

namespace nycleus2 {

#define NYCLEUS_ERRORS(x, y, z) \
  z(bidi_code_in_string) y \
  x(comment_unmatched_bidi_code) y \
  x(empty_id) y \
  x(empty_ext) y \
  x(invalid_escape_code) y \
  x(num_literal_base_prefix_and_suffix) y \
  x(num_literal_invalid) y \
  x(num_literal_suffix_missing_width) y \
  x(num_literal_suffix_too_wide) y \
  x(num_literal_suffix_zero_width) y \
  x(unexpected_code_point) y \
  x(unexpected_zwj) y \
  x(unexpected_zwnj) y \
  x(unterminated_char) y \
  x(unterminated_comment) y \
  x(unterminated_string) y \
  \
  x(assign_semicolon_expected) y \
  x(block_rbrace_expected) y \
  x(class_def_body_expected) y \
  x(class_def_name_expected) y \
  x(class_def_rbrace_expected) y \
  x(expr_expected) y \
  x(expr_rparen_expected) y \
  x(fn_def_body_expected) y \
  x(fn_def_lparen_expected) y \
  x(fn_def_name_expected) y \
  x(fn_def_param_name_expected) y \
  x(fn_def_param_next_expected) y \
  x(fn_def_param_type_expected) y \
  x(fn_type_lparen_expected) y \
  x(fn_type_param_name_expected) y \
  x(fn_type_param_next_expected) y \
  x(fn_type_rparen_expected) y \
  x(fn_type_param_type_expected) y \
  x(int_type_width_is_zero) y \
  x(int_type_width_too_big) y \
  x(stmt_expected) y \
  x(top_level_def_expected) y \
  x(type_name_expected) y \
  x(var_def_init_expected) y \
  x(var_def_name_expected) y \
  x(var_def_semicolon_expected) y \
  x(var_def_type_expected) y \
  \
  x(fp_literals_unsupported)

#define NYCLEUS_WARNINGS(x, y, z) \
  z(string_newline)

namespace diag {
  #define NYLCEUS_DIAG_FWD_DEF(x) struct x;
  PP_SEQ_FOR_EACH(NYCLEUS_ERRORS, NYLCEUS_DIAG_FWD_DEF)
  PP_SEQ_FOR_EACH(NYCLEUS_WARNINGS, NYLCEUS_DIAG_FWD_DEF)
  #undef NYCLEUS_DIAG_FWD_DEF
}

template<typename Visitor>
concept diagnostic_visitor =
  #define NYCLEUS_DIAG_IS_INVOCABLE(x) std::invocable<Visitor, diag::x&>
  PP_SEQ_APPLY(NYCLEUS_ERRORS, NYCLEUS_DIAG_IS_INVOCABLE, &&)
  && PP_SEQ_APPLY(NYCLEUS_WARNINGS, NYCLEUS_DIAG_IS_INVOCABLE, &&)
  #undef NYCLEUS_DIAG_IS_INVOCABLE
  && util::all_same_v<
    #define NYCLEUS_DIAG_INVOKE_RESULT(x) std::invoke_result_t<Visitor&&, diag::x&>
    PP_SEQ_LIST(NYCLEUS_ERRORS, NYCLEUS_DIAG_INVOKE_RESULT),
    PP_SEQ_LIST(NYCLEUS_WARNINGS, NYCLEUS_DIAG_INVOKE_RESULT)
    #undef NYCLEUS_DIAG_INVOKE_RESULT
  >;

template<typename Visitor>
concept diagnostic_const_visitor =
  #define NYCLEUS_DIAG_IS_CONST_INVOCABLE(x) std::invocable<Visitor, diag::x const&>
  PP_SEQ_APPLY(NYCLEUS_ERRORS, NYCLEUS_DIAG_IS_CONST_INVOCABLE, &&)
  && PP_SEQ_APPLY(NYCLEUS_WARNINGS, NYCLEUS_DIAG_IS_CONST_INVOCABLE, &&)
  #undef NYCLEUS_DIAG_IS_CONST_INVOCABLE
  && util::all_same_v<
    #define NYCLEUS_DIAG_CONST_INVOKE_RESULT(x) std::invoke_result_t<Visitor&&, diag::x const&>
    PP_SEQ_LIST(NYCLEUS_ERRORS, NYCLEUS_DIAG_CONST_INVOKE_RESULT),
    PP_SEQ_LIST(NYCLEUS_WARNINGS, NYCLEUS_DIAG_CONST_INVOKE_RESULT)
    #undef NYCLEUS_DIAG_CONST_INVOKE_RESULT
  >;

struct diagnostic {
  virtual ~diagnostic() noexcept = default;

  diagnostic() noexcept = default;
  diagnostic(diagnostic const&) noexcept = delete;
  diagnostic& operator=(diagnostic const&) noexcept = delete;

  enum class kind_t {
    #define NYCLEUS_DIAG_KIND(x) x
    PP_SEQ_LIST(NYCLEUS_ERRORS, NYCLEUS_DIAG_KIND),
    PP_SEQ_LIST(NYCLEUS_WARNINGS, NYCLEUS_DIAG_KIND)
    #undef NYCLEUS_DIAG_KIND
  };

  [[nodiscard]] virtual kind_t get_kind() const noexcept = 0;

  template<diagnostic_visitor Visitor>
  using visit_result_t = std::invoke_result_t<Visitor&&, diag::PP_SEQ_FIRST(NYCLEUS_ERRORS)&>;

private:
  template<diagnostic_visitor Visitor>
  static constexpr bool visit_noexcept{
    #define NYCLEUS_DIAG_INVOKE_NOEXCEPT(x) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<diag::x&>()))
    PP_SEQ_APPLY(NYCLEUS_ERRORS, NYCLEUS_DIAG_INVOKE_NOEXCEPT, &&)
    && PP_SEQ_APPLY(NYCLEUS_WARNINGS, NYCLEUS_DIAG_INVOKE_NOEXCEPT, &&)
    #undef NYCLEUS_DIAG_INVOKE_NOEXCEPT
  };

public:
  template<diagnostic_visitor Visitor>
  diagnostic::visit_result_t<Visitor> mutable_visit(Visitor&& visitor)
    noexcept(diagnostic::visit_noexcept<Visitor>);

  template<diagnostic_visitor Visitor>
  visit_result_t<Visitor> visit(Visitor&& visitor)
      noexcept(visit_noexcept<Visitor>) {
    return this->mutable_visit(std::forward<Visitor>(visitor));
  }

  template<diagnostic_const_visitor Visitor>
  using const_visit_result_t = std::invoke_result_t<Visitor&,
    diag::PP_SEQ_FIRST(NYCLEUS_ERRORS) const&>;

private:
  template<diagnostic_const_visitor Visitor>
  static constexpr bool const_visit_noexcept{
    #define NYCLEUS_DIAG_CONST_INVOKE_NOEXCEPT(x) \
      noexcept(std::invoke(std::declval<Visitor&&>(), std::declval<diag::x const&>()))
    PP_SEQ_APPLY(NYCLEUS_ERRORS, NYCLEUS_DIAG_CONST_INVOKE_NOEXCEPT, &&)
    && PP_SEQ_APPLY(NYCLEUS_WARNINGS, NYCLEUS_DIAG_CONST_INVOKE_NOEXCEPT, &&)
    #undef NYCLEUS_DIAG_CONST_INVOKE_NOEXCEPT
  };

public:
  template<diagnostic_const_visitor Visitor>
  diagnostic::const_visit_result_t<Visitor> const_visit(Visitor&& visitor) const
    noexcept(diagnostic::const_visit_noexcept<Visitor>);

  template<diagnostic_const_visitor Visitor>
  const_visit_result_t<Visitor> visit(Visitor&& visitor) const
  noexcept(const_visit_noexcept<Visitor>)
  {
    return this->const_visit(std::forward<Visitor>(visitor));
  }
};

struct error: diagnostic {};

struct warning: diagnostic {};

/** @brief Base class for errors that carry source location information. */
struct error_with_location: error {
  source_metric_range loc;

  explicit error_with_location(source_metric_range l) noexcept: loc{l} {}
};

/** @brief Base class for warnings that carry source location information. */
struct warning_with_location: warning {
  source_metric_range loc;

  explicit warning_with_location(source_metric_range l) noexcept: loc{l} {}
};

namespace diag {

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Lexer errors

/** @brief Bidirectional formatting code inside a string or character literal. */
struct bidi_code_in_string final: error_with_location {
  enum class bidi_code_t {
    str_lre,
    str_rle,
    str_pdf,
    str_lro,
    str_rlo,
    str_lri,
    str_rli,
    str_fsi,
    str_pdi,
    char_lre,
    char_rle,
    char_pdf,
    char_lro,
    char_rlo,
    char_lri,
    char_rli,
    char_fsi,
    char_pdi
  };
  bidi_code_t code;

  explicit bidi_code_in_string(
    source_metric_range l,
    bidi_code_t c
  ) noexcept: error_with_location{l}, code{c} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::bidi_code_in_string;
  }
};

/** @brief Unmatched bidirectional formatting code inside a comment. */
struct comment_unmatched_bidi_code final: error_with_location {
  enum class bidi_code_t { lre, rle, pdf, lro, rlo, lri, rli, fsi, pdi };
  bidi_code_t code;

  explicit comment_unmatched_bidi_code(
    source_metric_range l,
    bidi_code_t c
  ) noexcept: error_with_location{l}, code{c} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::comment_unmatched_bidi_code;
  }
};

/** @brief An empty identifier. */
struct empty_id final: error_with_location {
  explicit empty_id(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::empty_id;
  }
};

/** @brief An empty extension word. */
struct empty_ext final: error_with_location {
  explicit empty_ext(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::empty_ext;
  }
};

/** @brief A string or character literal has an invalid escape code. */
struct invalid_escape_code final: error_with_location {
  explicit invalid_escape_code(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::invalid_escape_code;
  }
};

/** @brief An integer literal has both a prefix and a suffix indicating the literal's base. */
struct num_literal_base_prefix_and_suffix final: error_with_location {
  explicit num_literal_base_prefix_and_suffix(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_base_prefix_and_suffix;
  }
};

/** @brief An integer literal has invalid syntax. */
struct num_literal_invalid final: error_with_location {
  explicit num_literal_invalid(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_invalid;
  }
};

/** @brief A numeric literal's type suffix  */
struct num_literal_suffix_missing_width final: error_with_location {
  enum class prefix_letter_t { lower_s, lower_u, upper_s, upper_u };
  prefix_letter_t prefix_letter;

  explicit num_literal_suffix_missing_width(
    source_metric_range l,
    prefix_letter_t pl
  ) noexcept: error_with_location{l}, prefix_letter{pl} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_suffix_missing_width;
  }
};

/** @brief A numeric literal's type suffix specifies a width that is too big. */
struct num_literal_suffix_too_wide final: error_with_location {
  explicit num_literal_suffix_too_wide(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_suffix_too_wide;
  }
};

/** @brief A numeric literal's type suffix specifies a zero width. */
struct num_literal_suffix_zero_width final: error_with_location {
  explicit num_literal_suffix_zero_width(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::num_literal_suffix_zero_width;
  }
};

/** @brief A source code contains an unexpected code point. */
struct unexpected_code_point final: error_with_location {
  char32_t code_point;

  explicit unexpected_code_point(
    source_metric_range l,
    char32_t cp
  ) noexcept: error_with_location{l}, code_point{cp} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unexpected_code_point;
  }
};

/** @brief An unexpected ZWJ (zero-width joiner) in a word. */
struct unexpected_zwj final: error_with_location {
  explicit unexpected_zwj(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unexpected_zwj;
  }
};

/** @brief An unexpected ZWNJ (zero-width non-joiner) in a word. */
struct unexpected_zwnj final: error_with_location {
  explicit unexpected_zwnj(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unexpected_zwnj;
  }
};

/** @brief An unterminated character literal. */
struct unterminated_char final: error_with_location {
  explicit unterminated_char(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unterminated_char;
  }
};

/** @brief An unterminated comment. */
struct unterminated_comment final: error_with_location {
  explicit unterminated_comment(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unterminated_comment;
  }
};

/** @brief An unterminated string or character literal. */
struct unterminated_string final: error_with_location {
  explicit unterminated_string(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::unterminated_string;
  }
};

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Parser errors

/** @brief Missing semicolon at the end of an assignment statement. */
struct assign_semicolon_expected final: error_with_location {
  explicit assign_semicolon_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::assign_semicolon_expected;
  }
};

/** @brief A non-statement expression in a statement position is not followed by a correct
 * token.
 *
 * The right brace (which would terminate the containing block) is not the only token that
 * could be correct in that situation: an assignment punctuation (for an assignment statement)
 * or a semicolon (for an expression statement) would also be acceptable. */
struct block_rbrace_expected final: error_with_location {
  explicit block_rbrace_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::block_rbrace_expected;
  }
};

/** @brief Missing left brace at the start of a class body. */
struct class_def_body_expected final: error_with_location {
  explicit class_def_body_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_def_body_expected;
  }
};

/** @brief Missing name in a class definition. */
struct class_def_name_expected final: error_with_location {
  explicit class_def_name_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_def_name_expected;
  }
};

/** @brief Missing right brace at the end of a class definition. */
struct class_def_rbrace_expected final: error_with_location {
  explicit class_def_rbrace_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::class_def_rbrace_expected;
  }
};

/** @brief Could not parse the first token of an expression. */
struct expr_expected final: error_with_location {
  explicit expr_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::expr_expected;
  }
};

/** @brief Missing right parenthesis at the end of a parenthesized expression. */
struct expr_rparen_expected final: error_with_location {
  explicit expr_rparen_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::expr_rparen_expected;
  }
};

/** @brief Missing left brace at the start of a function body. */
struct fn_def_body_expected final: error_with_location {
  explicit fn_def_body_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_body_expected;
  }
};

/** @brief Missing left parenthesis to enclose the parameters of a function definition. */
struct fn_def_lparen_expected final: error_with_location {
  explicit fn_def_lparen_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_lparen_expected;
  }
};

/** @brief Missing name in a function definition. */
struct fn_def_name_expected final: error_with_location {
  explicit fn_def_name_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_name_expected;
  }
};

/** @brief Unexpected token at the start of a parameter in a function definition.
 *
 * Apart from an actual parameter name, this position could also accept a colon as part of an
 * anonymous parameter or a right parenthesis for an end of the parameter list. */
struct fn_def_param_name_expected final: error_with_location {
  explicit fn_def_param_name_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_param_name_expected;
  }
};

/** @brief Unexpected token after a parameter in a function definition parameter list.
 *
 * This location expects either a comma or a right parenthesis. */
struct fn_def_param_next_expected final: error_with_location {
  explicit fn_def_param_next_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_param_next_expected;
  }
};

/** @brief Unexpected token after a parameter name in a function definition parameter list.
 *
 * This location expects a colon to separate the parameter name from a parameter type. */
struct fn_def_param_type_expected final: error_with_location {
  explicit fn_def_param_type_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_def_param_type_expected;
  }
};

/** @brief Missing left parenthesis after the `fn` keyword in a function reference type name.
 */
struct fn_type_lparen_expected final: error_with_location {
  explicit fn_type_lparen_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_lparen_expected;
  }
};

/** @brief Unexpected token at the start of a parameter in a function reference type name.
 *
 * Apart from an actual parameter name, this position could also accept a colon as part of an
 * anonymous parameter or a right parenthesis for an end of the parameter list. */
struct fn_type_param_name_expected final: error_with_location {
  explicit fn_type_param_name_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_param_name_expected;
  }
};

/** @brief Unexpected token after a parameter in the parameter list of a function reference
 * type name.
 *
 * This location expects either a comma or a right parenthesis. */
struct fn_type_param_next_expected final: error_with_location {
  explicit fn_type_param_next_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_param_next_expected;
  }
};

/** @brief Missing right parenthesis at the end of a function reference type name wrapped in an
 * extra pair of parentheses. */
struct fn_type_rparen_expected final: error_with_location {
  explicit fn_type_rparen_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_rparen_expected;
  }
};

/** @brief Unexpected token after a parameter name in the parameter list of a function
 * reference type name.
 *
 * This location expects a colon to separate the parameter name from a parameter type. */
struct fn_type_param_type_expected final: error_with_location {
  explicit fn_type_param_type_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fn_type_param_type_expected;
  }
};

/** @brief An integer type name specifies a zero width. */
struct int_type_width_is_zero final: error_with_location {
  explicit int_type_width_is_zero(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_type_width_is_zero;
  }
};

/** @brief An integer type name specifies a width greater than the maximum allowed width. */
struct int_type_width_too_big final: error_with_location {
  explicit int_type_width_too_big(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::int_type_width_too_big;
  }
};

/** @brief Could not parse the first token of a statement. */
struct stmt_expected final: error_with_location {
  explicit stmt_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::stmt_expected;
  }
};

/** @brief Could not parse the first token of a top-level definition. */
struct top_level_def_expected final: error_with_location {
  explicit top_level_def_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::top_level_def_expected;
  }
};

/** @brief Could not parse the first token of a type name. */
struct type_name_expected final: error_with_location {
  explicit type_name_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::type_name_expected;
  }
};

/** @brief Missing initializer of a variable definition.
 *
 * This location also accepts a semicolon to end the variable definition without an
 * initializer. */
struct var_def_init_expected final: error_with_location {
  explicit var_def_init_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def_init_expected;
  }
};

/** @brief Missing name in a variable definition. */
struct var_def_name_expected final: error_with_location {
  explicit var_def_name_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def_name_expected;
  }
};

/** @brief Missing semicolon at the end of a variable definition after the initializer. */
struct var_def_semicolon_expected final: error_with_location {
  explicit var_def_semicolon_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def_semicolon_expected;
  }
};

/** @brief Missing colon after the variable name of a variable definition. */
struct var_def_type_expected final: error_with_location {
  explicit var_def_type_expected(
    source_metric_range l
  ) noexcept: error_with_location{l} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::var_def_type_expected;
  }
};

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Errors about unsupported features

/** @brief Floating-point literals are unsupported. */
struct fp_literals_unsupported final: error_with_location {
  explicit fp_literals_unsupported(
    source_metric_range l
  ) noexcept: error_with_location{std::move(l)} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::fp_literals_unsupported;
  }
};

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Warnings

/** @brief A warning about line breaks inside string or character literals. */
struct string_newline final: warning_with_location {
  enum class newline_code_t {
    str_lf, str_vt, str_ff, str_cr, str_nel, str_line_sep, str_para_sep,
    char_lf, char_vt, char_ff, char_cr, char_nel, char_line_sep, char_para_sep
  };
  newline_code_t newline_code;

  explicit string_newline(
    source_metric_range l,
    newline_code_t nc
  ) noexcept: warning_with_location{l}, newline_code{nc} {}

  [[nodiscard]] virtual kind_t get_kind() const noexcept override {
    return kind_t::string_newline;
  }
};

} // diag

template<diagnostic_visitor Visitor>
diagnostic::visit_result_t<Visitor> diagnostic::mutable_visit(Visitor&& visitor)
    noexcept(diagnostic::visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define NYCLEUS_DIAG_VISIT(x) \
      case kind_t::x: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<diag::x&>(*this));
    PP_SEQ_FOR_EACH(NYCLEUS_ERRORS, NYCLEUS_DIAG_VISIT)
    PP_SEQ_FOR_EACH(NYCLEUS_WARNINGS, NYCLEUS_DIAG_VISIT)
    #undef NYCLEUS_DIAG_VISIT
  }
}

template<diagnostic_const_visitor Visitor>
diagnostic::const_visit_result_t<Visitor>
    diagnostic::const_visit(Visitor&& visitor) const
    noexcept(diagnostic::const_visit_noexcept<Visitor>) {
  switch(get_kind()) {
    #define NYCLEUS_DIAG_CONST_VISIT(x) \
      case kind_t::x: \
        return std::invoke(std::forward<Visitor>(visitor), static_cast<diag::x const&>(*this));
    PP_SEQ_FOR_EACH(NYCLEUS_ERRORS, NYCLEUS_DIAG_CONST_VISIT)
    PP_SEQ_FOR_EACH(NYCLEUS_WARNINGS, NYCLEUS_DIAG_CONST_VISIT)
    #undef NYCLEUS_DIAG_CONST_VISIT
  }
}

} // nycleus2

#endif
