#include "nycleus2/source_file.hpp"
#include <utility>

#ifndef NDEBUG
  #include "nycleus2/source_location.hpp"
  #include "nycleus2/util.hpp"
  #include "unicode/encoding.hpp"
  #include "util/safe_int.hpp"
  #include <cassert>
  #include <limits>
#endif

nycleus2::token_diagnostics::~token_diagnostics() noexcept {
  while(next) {
    auto new_next = std::move(next->next);
    next = std::move(new_next);
  }
}

#ifndef NDEBUG

void nycleus2::source_file::validate() const noexcept {
  bool in_token{false};
  src_pos_t file_size{0};
  for(node const& n: nodes_) {
    assert(!n.data.empty());
    assert(util::size_t::wrap(n.data.size()) <= max_node_size);
    file_size += util::size_t::wrap(n.data.size()).cast<src_pos_t>();
    assert(unicode::is_valid_utf8(n.data));

    source_metric computed_metric{0, 0};
    auto text_it{n.data.cbegin()};
    while(text_it != n.data.cend()) {
      char32_t const ch{unicode::utf8<>::decode(text_it, n.data.cend(),
        unicode::eh_assume_valid<>{})};
      bool const is_line_end{
        ch != U'\r'
        ? is_line_terminator(ch, mode_)
        : text_it != n.data.cend()
        ? *text_it != u8'\n'
        : &n == &nodes_.back()
        ? true
        : (&n)[1].data.front() != u8'\n'
      };
      if(is_line_end) {
        computed_metric = computed_metric.add({1, 0});
      } else {
        computed_metric = computed_metric.add({0, num_columns(ch, mode_)});
      }
    }
    assert(computed_metric == n.metric);

    assert(n.starts_in_token == in_token);
    src_pos_t last_boundary{0};
    auto token_it{n.tokens.cbegin()};
    while(token_it != n.tokens.cend()) {
      if(in_token) {
        assert(token_it->offset > last_boundary);
        in_token = false;
      } else {
        assert(token_it->offset >= last_boundary);
        assert(!token_it->is_lookahead);
        in_token = true;
      }
      assert(token_it->offset <= util::size_t::wrap(n.data.size()));
      if(token_it->offset < util::size_t::wrap(n.data.size())) {
        assert(!unicode::is_utf8_continuation(n.data[token_it->offset.unwrap()]));
      } else {
        assert(!in_token);
      }
      last_boundary = token_it->offset;
      ++token_it;
    }
  }
  assert(!in_token);
  assert(file_size <= max_file_size);

  if(nodes_.size() > 1) {
    for(node const& n: nodes_) {
      assert(util::size_t::wrap(n.data.size()) >= min_node_size);
    }
  }
}

#endif
