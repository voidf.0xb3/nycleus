#ifndef NYCLEUS2_UTIL_HPP_INCLUDED_EC6V9CHQGKRPW86YWP4H2NLIP
#define NYCLEUS2_UTIL_HPP_INCLUDED_EC6V9CHQGKRPW86YWP4H2NLIP

#include "util/safe_int.hpp"
#include <algorithm>
#include <cstdint>
#include <limits>

namespace nycleus2 {

/** @brief An unsigned integer type big enough to store an index of a code unit within a source
 * file.
 *
 * This can also be used for integers that are bounded from above by the number of code units
 * in a file.
 *
 * A 32-bit integer is deemed large enough: it is highly unlikely that a 4 GiB source file
 * would be practical. */
using src_pos_t = util::uint32_t;

/** @brief The maximum allowed size of a source file.
 *
 * We set it to one less than the number of possible indexes. */
constexpr src_pos_t max_file_size{std::numeric_limits<src_pos_t::base_type>::max()};

/** @brief The unsigned integer data type used to represent widths of Nycleus integer types. */
using int_width_t = util::uint32_t;

/** @brief A type suffix that may be present on an integer literal. */
struct int_type_suffix {
  /** @brief The width of the requested type.
   *
   * This must be positive and not greater than max_int_width. */
  int_width_t width;

  /** @brief Whether a signed type is requested. */
  bool is_signed;

  [[nodiscard]] friend bool operator==(
    nycleus2::int_type_suffix,
    nycleus2::int_type_suffix
  ) noexcept = default;
};

/** @brief The maximum possible width of an integer type.
 *
 * This is set to 2<sup>23</sup>, which is the maximum width supported by LLVM, or the largest
 * value of the `unsigned` data type, which is used in LLVM's APIs. */
constexpr int_width_t max_int_width{std::min(
  util::safe_int<unsigned>{std::numeric_limits<unsigned>::max()}, int_width_t{1} << 23)};

} // nycleus2

#endif
