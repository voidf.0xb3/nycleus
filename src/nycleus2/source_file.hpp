#ifndef NYCLEUS2_SOURCE_FILE_HPP_INCLUDED_PZDC2LE1EEKIVD6SONZDQHWVQ
#define NYCLEUS2_SOURCE_FILE_HPP_INCLUDED_PZDC2LE1EEKIVD6SONZDQHWVQ

#include "nycleus2/diagnostic.hpp"
#include "nycleus2/hlir.hpp"
#include "nycleus2/source_location.hpp"
#include "nycleus2/util.hpp"
#include "util/inplace_vector.hpp"
#include <filesystem>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#ifndef NDEBUG
  namespace test::nycleus2 {
    struct source_file_access;
  }
#endif

namespace nycleus2 {

class incremental_token_source;

/** @brief The result of parsing a source file, represented in a way that can be updated via
 * incremental parsing. */
struct parsed_file {
  struct top_level_def {
    /** @brief The offset to the start of this definition from the end of the preceding
     * definition, or from the start of the file for the first definition. */
    source_metric preceding_gap;

    std::unique_ptr<hlir::def> def;

    hlir::panic_seq panic_seq;

    /** @brief The diagnostics associated with this node if the definition is absent.
     *
     * If the definition pointer is null, the associated diagnostic is stored here. But if
     * there is a definition, then it is used to store the diagnostics instead.
     *
     * The base for the metrics is the end of the preceding gap. */
    std::vector<std::unique_ptr<diagnostic>> diags{};
  };

  // TODO: Organize parsed_file definitions in a tree
  std::vector<top_level_def> defs;
};

/** @brief A container for diagnostic associated with a single token. */
struct token_diagnostics {
  /** @brief The metric from the start of the file to the start of this token.
   *
   * Serves as a base for the metrics of the token's diagnostics. */
  source_metric token_loc;
  std::vector<std::unique_ptr<diagnostic>> diags;
  std::unique_ptr<token_diagnostics> next;

  explicit token_diagnostics(source_metric tl) noexcept: token_loc{tl} {}

  explicit token_diagnostics(
    source_metric tl,
    std::vector<std::unique_ptr<diagnostic>> d
  ) noexcept: token_loc{tl}, diags{std::move(d)} {}

  token_diagnostics(token_diagnostics const&) = delete;
  token_diagnostics& operator=(token_diagnostics const&) = delete;

  ~token_diagnostics() noexcept;
};

/** @brief A source file, representing the source code of the file in a way that enables
 * incremental lexing. */
class source_file {
private:
  friend class incremental_token_source;
  #ifndef NDEBUG
    friend struct test::nycleus2::source_file_access;
  #endif

  std::filesystem::path name_;

  metric_mode mode_;

  // TODO: Actually implement source file node trees (not arrays)
  // TODO: Pool allocation for source file nodes
  // TODO: More compact representation of token boundaries

  #ifndef NYCLEUS_SOURCE_FILE_NODE_SIZE_BASE
    #define NYCLEUS_SOURCE_FILE_NODE_SIZE_BASE 64
  #endif

  /** @brief A configurable parameter that controls minimum and maximum node sizes.
   *
   * The minimum node size is one less than this value and the maximum node size is twice this
   * value.
   *
   * This value can be configured by setting the `NYCLEUS_SOURCE_FILE_NODE_SIZE_BASE` macro in
   * the compiler's flags. If the macro is not set, it defaults to 64. */
  static constexpr src_pos_t node_size_base{NYCLEUS_SOURCE_FILE_NODE_SIZE_BASE};

  // We want to ensure that the algorithm that spreads text over three or more nodes does not
  // create nodes that violate the minimum node size.
  //
  // Let N be the value of node_size_base. Then the maximum node size is 2N and the minimum
  // node size is N - 1. We aim to leave 3 bytes of room in each node to adjust split points to
  // code point boundaries, so we divide the text size by 2N - 3. (We don't want any room in
  // the last node, so we initally remove 3 bytes from the node text and add them back to the
  // last node at the end. The rest of this discussion will deal with the node text after the
  // removal of those 3 bytes.)
  //
  // The minimum amount of text that will not fit in K nodes (K ≥ 2) is (2N - 3)K + 1. But for
  // ease of calculation, let's first consider the value (2N - 3)K. After it is spread over
  // K + 1 nodes, the smaller of the two node sizes will be ⌊(2N - 3)K / (K + 1)⌋. We want this
  // to be no less than N + 2; since that's an integer, the floor can be removed. So we want:
  // (2N - 3) (K / K + 1) ≥ N + 2.
  //
  // The value of K / (K + 1) increases with K, so we only need to check the smallest value:
  // K = 2. Then the inequality becomse 2(2N - 3) / 3 ≥ N + 2, which is equivalent to N ≥ 12.
  //
  // But we don't actually need this condition to be satisfied for text of size (2N - 3)K; it
  // is enough to satisfy it for greater text sizes. So smaller values of N might work, too.
  // Indeed, if N = 11, then nodes are between 10 and 22 bytes long, and we want
  // (19K + 1) / (K + 1) ≥ 13, which is true for all K ≥ 2. So N = 11 works.
  //
  // Note that N = 10 does not work: the nodes are between 9 and 20 bytes long; starting from
  // 35 bytes, we try to fit the text into three nodes; this makes the smallest node size equal
  // to 11 bytes, which is only 2 bytes greater than 9 bytes.
  static_assert(node_size_base >= 11);

  // The node_size_base parameter cannot exceed 64 so that the maximum node size does not
  // exceed 128. This will eventually be used by the compact representation of token
  // boundaries.
  static_assert(node_size_base <= 64);

  /** @brief The maximum number of UTF-8 code units that can be stored in a node.
   *
   * This is twice the value of node_size_base. */
  static constexpr src_pos_t max_node_size{node_size_base * 2};

  /** @brief The minimum number of UTF-8 code units that can be stored in a node.
   *
   * This is one less than the value of node_size_base.
   *
   * This limit may be violated if there is only one node. This way files smaller than the
   * minimum node size can be represented. */
  static constexpr src_pos_t min_node_size{node_size_base - 1};

  /** @brief The start or end of a token.
   *
   * Appears in the token boundary list of a node. */
  struct token_boundary {
    /** @brief The offset of the boundary from the beginning of the node's text. */
    src_pos_t offset;

    /** @brief Whether this boundary is the end of a lookahead token.
     *
     * This must not be set on a token start boundary. */
    bool is_lookahead;
  };

  struct node {
    /** @brief The source metric from the start of the part of the source file represented by
     * the node to its end. */
    source_metric metric;

    /** @brief The node's data. */
    util::inplace_vector<char8_t, max_node_size.unwrap()> data;

    /** @brief The token boundaries occuring in this node.
     *
     * In this array, token starts and ends alternate. The first boundary is a token start if
     * the node does not start in the middle of a token or the token end if it does (this is
     * determined by the `starts_in_token` value). Depending on the number of token boundaries,
     * the last token is either a token start or the token end; the next node's
     * `starts_in_token` indicates whether this node ends in the middle of a token.
     *
     * If a token boundary coincides with a node boundary, then the start of a token is
     * attributed to the node after the boundary and the end of a token is attributed to the
     * node before the boundary. */
    std::vector<token_boundary> tokens;

    /** @brief Whether this node starts in the middle of a token. */
    bool starts_in_token;
  };

  std::vector<node> nodes_;

  std::unique_ptr<token_diagnostics> diags_;

  /** @brief The result of parsing this file. */
  parsed_file parsed_;

public:
  explicit source_file(
    std::filesystem::path name,
    metric_mode mode
  ) noexcept: name_{std::move(name)}, mode_{mode} {}

  /** @brief Using assertions, checks that the class's invariants are held. */
  #ifdef NDEBUG
    void validate() const noexcept {}
  #else
    void validate() const noexcept;
  #endif

};

} // nycleus2

#endif
