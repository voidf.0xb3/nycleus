#include "nycleus2/token.hpp"
#include <gsl/gsl>

using namespace util::u8compat_literals;

gsl::czstring nycleus2::token_source_error::what() const noexcept {
  return "nycleus2::token_source_error";
}
