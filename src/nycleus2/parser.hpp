#ifndef NYCLEUS2_PARSER_HPP_INCLUDED_GYCJDJ13A3FQ6I5QI1GOBTBVB
#define NYCLEUS2_PARSER_HPP_INCLUDED_GYCJDJ13A3FQ6I5QI1GOBTBVB

namespace nycleus2 {

class token_source;
struct parsed_file;

/** @brief Performs an incremental parse.
 *
 * The given parsed file must be the result of parsing the token source's old token sequence;
 * otherwise, the behavior is undefined. This call alters the parsed file to be the result of
 * parsing the token source's new token sequence instead.
 *
 * This can also be used for full parsing. Simply provide an empty parsed file and a token
 * source whose old token sequence is empty.
 *
 * A thrown exception invalidates the parsed file, which can then only be discarded.
 *
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws token_source_error */
void parse(token_source&, parsed_file&);

} // nycleus2

#endif
