#ifndef NYCLEUS2_TOKEN_HPP_INCLUDED_0JIDRSVXE5ZHHRE6MKNVQFP3G
#define NYCLEUS2_TOKEN_HPP_INCLUDED_0JIDRSVXE5ZHHRE6MKNVQFP3G

#include "nycleus2/source_location.hpp"
#include "nycleus2/util.hpp"
#include "util/bigint.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <concepts>
#include <exception>
#include <optional>
#include <string>
#include <utility>
#include <variant>

namespace nycleus2 {

namespace tok {
  struct word {
    enum class sigil_t { none, dollar, underscore };

    std::u8string text;
    sigil_t sigil;

    [[nodiscard]] friend bool operator==(word const&, word const&) noexcept = default;
  };

  struct int_literal {
    /** @brief The integer value of the token.
     *
     * Must be non-negative. */
    util::bigint value;

    std::optional<int_type_suffix> type_suffix;

    [[nodiscard]] friend bool operator==(
      int_literal const&,
      int_literal const&
    ) noexcept = default;
  };

  struct string_literal {
    std::u8string value;

    [[nodiscard]] friend bool operator==(
      string_literal const&,
      string_literal const&
    ) noexcept = default;
  };

  struct char_literal {
    std::u8string value;

    [[nodiscard]] friend bool operator==(
      char_literal const&,
      char_literal const&
    ) noexcept = default;
  };

  struct excl {
    [[nodiscard]] friend bool operator==(excl, excl) noexcept = default;
  };
  struct neq {
    [[nodiscard]] friend bool operator==(neq, neq) noexcept = default;
  };
  struct percent {
    [[nodiscard]] friend bool operator==(percent, percent) noexcept = default;
  };
  struct percent_assign {
    [[nodiscard]] friend bool operator==(percent_assign, percent_assign) noexcept = default;
  };
  struct amp {
    [[nodiscard]] friend bool operator==(amp, amp) noexcept = default;
  };
  struct logical_and {
    source_metric midpoint;
    [[nodiscard]] friend bool operator==(logical_and, logical_and) noexcept = default;
  };
  struct amp_assign {
    [[nodiscard]] friend bool operator==(amp_assign, amp_assign) noexcept = default;
  };
  struct lparen {
    [[nodiscard]] friend bool operator==(lparen, lparen) noexcept = default;
  };
  struct rparen {
    [[nodiscard]] friend bool operator==(rparen, rparen) noexcept = default;
  };
  struct star {
    [[nodiscard]] friend bool operator==(star, star) noexcept = default;
  };
  struct star_assign {
    [[nodiscard]] friend bool operator==(star_assign, star_assign) noexcept = default;
  };
  struct plus {
    [[nodiscard]] friend bool operator==(plus, plus) noexcept = default;
  };
  struct incr {
    [[nodiscard]] friend bool operator==(incr, incr) noexcept = default;
  };
  struct plus_assign {
    [[nodiscard]] friend bool operator==(plus_assign, plus_assign) noexcept = default;
  };
  struct comma {
    [[nodiscard]] friend bool operator==(comma, comma) noexcept = default;
  };
  struct minus {
    [[nodiscard]] friend bool operator==(minus, minus) noexcept = default;
  };
  struct decr {
    [[nodiscard]] friend bool operator==(decr, decr) noexcept = default;
  };
  struct minus_assign {
    [[nodiscard]] friend bool operator==(minus_assign, minus_assign) noexcept = default;
  };
  struct arrow {
    [[nodiscard]] friend bool operator==(arrow, arrow) noexcept = default;
  };
  struct point {
    [[nodiscard]] friend bool operator==(point, point) noexcept = default;
  };
  struct double_point {
    [[nodiscard]] friend bool operator==(double_point, double_point)
      noexcept = default;
  };
  struct ellipsis {
    [[nodiscard]] friend bool operator==(ellipsis, ellipsis) noexcept = default;
  };
  struct slash {
    [[nodiscard]] friend bool operator==(slash, slash) noexcept = default;
  };
  struct slash_assign {
    [[nodiscard]] friend bool operator==(slash_assign, slash_assign) noexcept = default;
  };
  struct colon {
    [[nodiscard]] friend bool operator==(colon, colon) noexcept = default;
  };
  struct semicolon {
    [[nodiscard]] friend bool operator==(semicolon, semicolon) noexcept = default;
  };
  struct lt {
    [[nodiscard]] friend bool operator==(lt, lt) noexcept = default;
  };
  struct lrot {
    [[nodiscard]] friend bool operator==(lrot, lrot) noexcept = default;
  };
  struct lrot_assign {
    [[nodiscard]] friend bool operator==(lrot_assign, lrot_assign) noexcept = default;
  };
  struct lshift {
    [[nodiscard]] friend bool operator==(lshift, lshift) noexcept = default;
  };
  struct lshift_assign {
    [[nodiscard]] friend bool operator==(lshift_assign, lshift_assign) noexcept = default;
  };
  struct leq {
    [[nodiscard]] friend bool operator==(leq, leq) noexcept = default;
  };
  struct three_way_cmp {
    [[nodiscard]] friend bool operator==(three_way_cmp, three_way_cmp) noexcept = default;
  };
  struct assign {
    [[nodiscard]] friend bool operator==(assign, assign) noexcept = default;
  };
  struct eq {
    [[nodiscard]] friend bool operator==(eq, eq) noexcept = default;
  };
  struct fat_arrow {
    [[nodiscard]] friend bool operator==(fat_arrow, fat_arrow) noexcept = default;
  };
  struct gt {
    [[nodiscard]] friend bool operator==(gt, gt) noexcept = default;
  };
  struct rrot {
    [[nodiscard]] friend bool operator==(rrot, rrot) noexcept = default;
  };
  struct rrot_assign {
    [[nodiscard]] friend bool operator==(rrot_assign, rrot_assign) noexcept = default;
  };
  struct geq {
    [[nodiscard]] friend bool operator==(geq, geq) noexcept = default;
  };
  struct rshift {
    [[nodiscard]] friend bool operator==(rshift, rshift) noexcept = default;
  };
  struct rshift_assign {
    [[nodiscard]] friend bool operator==(rshift_assign, rshift_assign) noexcept = default;
  };
  struct question {
    [[nodiscard]] friend bool operator==(question, question) noexcept = default;
  };
  struct at {
    [[nodiscard]] friend bool operator==(at, at) noexcept = default;
  };
  struct lsquare {
    [[nodiscard]] friend bool operator==(lsquare, lsquare) noexcept = default;
  };
  struct rsquare {
    [[nodiscard]] friend bool operator==(rsquare, rsquare) noexcept = default;
  };
  struct bitwise_xor {
    [[nodiscard]] friend bool operator==(bitwise_xor, bitwise_xor) noexcept = default;
  };
  struct bitwise_xor_assign {
    [[nodiscard]] friend bool operator==(
      bitwise_xor_assign,
      bitwise_xor_assign
    ) noexcept = default;
  };
  struct logical_xor {
    [[nodiscard]] friend bool operator==(logical_xor, logical_xor) noexcept = default;
  };
  struct lbrace {
    [[nodiscard]] friend bool operator==(lbrace, lbrace) noexcept = default;
  };
  struct pipe {
    [[nodiscard]] friend bool operator==(pipe, pipe) noexcept = default;
  };
  struct pipe_assign {
    [[nodiscard]] friend bool operator==(pipe_assign, pipe_assign) noexcept = default;
  };
  struct subst {
    [[nodiscard]] friend bool operator==(subst, subst) noexcept = default;
  };
  struct logical_or {
    [[nodiscard]] friend bool operator==(logical_or, logical_or) noexcept = default;
  };
  struct rbrace {
    [[nodiscard]] friend bool operator==(rbrace, rbrace) noexcept = default;
  };
  struct tilde {
    [[nodiscard]] friend bool operator==(tilde, tilde) noexcept = default;
  };

  /** @brief A special token, indicating a comment. */
  struct comment {
    [[nodiscard]] friend bool operator==(comment, comment) noexcept = default;
  };

  /** @brief A special token, indicating a lexing error. */
  struct error {
    [[nodiscard]] friend bool operator==(error, error) noexcept = default;
  };

  /** @brief A special token, indicating the end of a source file.
   *
   * Such a token cannot appear as part of a token sequence. Instead, it is used as a sentinel
   * value to indicate the end of a token sequence. */
  struct eof {
    [[nodiscard]] friend bool operator==(eof, eof) noexcept = default;
  };
}

using token = std::variant<
  tok::word,
  tok::int_literal,
  tok::string_literal,
  tok::char_literal,

  tok::excl,
  tok::neq,
  tok::percent,
  tok::percent_assign,
  tok::amp,
  tok::logical_and,
  tok::amp_assign,
  tok::lparen,
  tok::rparen,
  tok::star,
  tok::star_assign,
  tok::plus,
  tok::incr,
  tok::plus_assign,
  tok::comma,
  tok::minus,
  tok::decr,
  tok::minus_assign,
  tok::arrow,
  tok::point,
  tok::double_point,
  tok::ellipsis,
  tok::slash,
  tok::slash_assign,
  tok::colon,
  tok::semicolon,
  tok::lt,
  tok::lrot,
  tok::lrot_assign,
  tok::lshift,
  tok::lshift_assign,
  tok::leq,
  tok::three_way_cmp,
  tok::assign,
  tok::eq,
  tok::fat_arrow,
  tok::gt,
  tok::rrot,
  tok::rrot_assign,
  tok::geq,
  tok::rshift,
  tok::rshift_assign,
  tok::question,
  tok::at,
  tok::lsquare,
  tok::rsquare,
  tok::bitwise_xor,
  tok::bitwise_xor_assign,
  tok::logical_xor,
  tok::lbrace,
  tok::pipe,
  tok::pipe_assign,
  tok::subst,
  tok::logical_or,
  tok::rbrace,
  tok::tilde,

  tok::comment,
  tok::error,
  tok::eof
>;

class token_source_error: public std::exception {
public:
  [[nodiscard]] virtual gsl::czstring what() const noexcept override;
};

/** @brief A token source is a mechanism for providing tokens to an incremental parser.
 *
 * A token source deals with two sequences of tokens: the old one and the new one. The two
 * sequences overlap, so that some tokens are part of both sequences. The token source produces
 * the tokens of the new sequence and information about their relationship with the tokens of
 * the old sequence; the old tokens themselves are not explicitly available.
 *
 * The token source object maintains a cursor pointing at the beginning of a new-sequence token
 * or at the end of the new token stream. Calls to virtual functions move the cursor forward
 * and return metrics indicating how the cursor was adjusted.
 *
 * The token source object also maintains a cursor pointing into the old source text, which is
 * used for reusing old tokens, as specified below. Initially, the cursor's metric from the
 * start of the old source file is specified via `first_gap_result::old_metric`. Afterwards,
 * each call to `next_token` advances the cursor by the metric specified via
 * `next_token_result::old_metric`, and each call to `reuse` advances it by the reuse amount,
 * and then by the amount specified via `reuse_result::gap_old_metric`. The exact ways in which
 * the derived class chooses these metrics is unspecified, provided that the reuse mechanism,
 * as specified below, works properly.
 *
 * Each function call returns a `max_reuse` metric, which is used to indicate how much of the
 * new source file can be reused on account of being unchanged from the old source file. If
 * this metric is zero, no reuse is possible. Otherwise, it shall be a metric from the start of
 * the current token such that the specified range does not contain any changes between the old
 * and the new source files. In this case, the old source file cursor, described above, shall
 * also point at the beginning of the first token in this range. */
class token_source: public util::hier_base {
public:
  virtual ~token_source() noexcept = default;

  /** @brief The return type of `first_gap`. */
  struct first_gap_result {
    /** @brief The metric from the start of the new source file to the start of the first token
     * in the new source file. */
    source_metric new_metric;

    /** @brief The old source file's cursor initial metric from the start of the old source
     * file. */
    source_metric old_metric;

    /** @brief The initial maximum reuse amount. */
    source_metric max_reuse;

    [[nodiscard]] friend bool operator==(
      first_gap_result const&,
      first_gap_result const&
    ) noexcept = default;
  };

  /** @brief Returns information about the start of the token sequence, specifically the gap
   * before the first token. Must be called exactly once before calling any other functions on
   * the token source object.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  [[nodiscard]] first_gap_result first_gap() {
    return do_first_gap();
  }

  /** @brief The return type of `next_token`. */
  struct next_token_result {
    /** @brief The lexed token or tok::eof. */
    token next_token;

    /** @brief The metric in the new source file from the start of this token to its end. */
    source_metric token_new_metric;

    /** @brief The metric in the new source file from the end of this token to the beginning of
     * the next token, or to the end of the file if this is the last token. */
    source_metric gap_new_metric;

    /** @brief The metric added to the old source file's cursor upon lexing this token. */
    source_metric old_metric;

    /** @brief The maximum reuse amount after consuming this token. */
    source_metric max_reuse;

    [[nodiscard]] friend bool operator==(
      next_token_result const&,
      next_token_result const&
    ) noexcept = default;
  };

  /** @brief Lexes and returns one token.
   *
   * One token is lexed and returned. The cursor is advanced to point to the start of the next
   * token.
   *
   * If the cursor is at the end of the source file, the returned token is tok::eof; all
   * offsets have unspecified values and should not be used.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  [[nodiscard]] next_token_result next_token() {
    return do_next_token();
  }

  /** @brief The return type of `reuse`. */
  struct reuse_result {
    /** @brief The metric in the new source file from the end of the last reused token to the
     * beginning of the next token, or to the end of the new source file if there are no more
     * tokens. */
    source_metric gap_new_metric;

    /** @brief The metric added to the old source file's cursor after this reuse, in addition
     * to the reused amount itself. */
    source_metric gap_old_metric;

    /** @brief The maximum reuse amount after this reuse. */
    source_metric max_reuse;

    [[nodiscard]] friend bool operator==(
      reuse_result const&,
      reuse_result const&
    ) noexcept = default;
  };

  /** @brief Reuses the given amount of tokens without lexing them.
   *
   * The metric shall be a valid metric from the beginning of the current token to the end of
   * the last token being reused (either the same or a later one). It shall not exceeed the
   * maximum reuse amount, as returned from the last function call. All these tokens shall be
   * skipped; the new source file cursor shall be advanced to the beginning of the next token
   * after the ones reused, or to the end of the new source file if there are no more tokens;
   * the old source file cursor shall be advanced to the end of the last reused token, and then
   * further by the amount returned via `gap_old_metric`.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  [[nodiscard]] reuse_result reuse(source_metric offset) {
    return this->do_reuse(offset);
  }

private:
  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  [[nodiscard]] virtual first_gap_result do_first_gap() = 0;

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  [[nodiscard]] virtual next_token_result do_next_token() = 0;

  /** @throws std::bad_alloc
   * @throws std::length_error
   * @throws token_source_error */
  [[nodiscard]] virtual reuse_result do_reuse(source_metric) = 0;
};

} // nycleus2

#endif
