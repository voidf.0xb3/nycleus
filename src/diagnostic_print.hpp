#ifndef DIAGNOSTIC_PRINT_HPP_INCLUDED_SVDRC7UZOHLSBQAWTMFP5AE06
#define DIAGNOSTIC_PRINT_HPP_INCLUDED_SVDRC7UZOHLSBQAWTMFP5AE06

#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_print.hpp"
#include "nycleus/source_location.hpp"
#include "util/util.hpp"
#include <cstddef>
#include <iterator>
#include <memory>
#include <vector>

class diag_message_printer: public util::hier_base {
public:
  virtual ~diag_message_printer() noexcept = default;

  /** @param too_many_errors Whether the build terminated due to too many
   *                         errors.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws std::ios_base::failure */
  virtual void print(std::vector<std::unique_ptr<nycleus::diagnostic>> const&,
    bool too_many_errors) = 0;
};

/** @brief A diagnostic message handler that prints the messages to the standard
 * error output in a format targeted at humans, with no expectation of
 * machine-readability. Optionally the messages can be colored. */
class diag_message_handler_human: public diag_message_printer {
private:
  bool color_;

public:
  explicit diag_message_handler_human(bool color) noexcept: color_{color} {}

  class iterator {
  public:
    using iterator_category = std::output_iterator_tag;
    using value_type = void;
    using difference_type = std::ptrdiff_t;
    using pointer = void;
    using reference = void;

    iterator() noexcept = default;

    /** @throws std::ios_base::failure */
    iterator& operator=(char8_t ch);

    iterator& operator++() noexcept { return *this; }

    iterator& operator++(int) noexcept { return *this; }

    [[nodiscard]] iterator& operator*() noexcept { return *this; }
  };

  static_assert(std::output_iterator<iterator, char8_t>);

  /** @throws std::ios_base::failure */
  [[nodiscard]] iterator message_start(
    nycleus::source_location loc,
    nycleus::diag_message_kind kind
  ) const;

  /** @throws std::ios_base::failure */
  static void message_end();

  /** @throws std::ios_base::failure */
  virtual void print(std::vector<std::unique_ptr<nycleus::diagnostic>> const&,
    bool too_many_errors) override;
};

// TODO: diag_message_handler_machine

/** @brief A diagnostic message handler that prints the messages to the standard
 * error output in a JSON-based format.
 *
 * The output is a JSON object with the key `diagnostics`, containing an array
 * of messages. Each message is represented as a JSON object with the following
 * keys:
 * - `kind`: one of the strings `error`, `warning`, or `notice`.
 * - `loc`: an object representing the message's location with the following
 *   keys:
 *   - `file`: a string containing the file name, or null if there is no
 *     information.
 *   - `first`: an object representing the start of the location range.
 *   - `last`: an object representing the end of the location range.
 *
 *   The objects `first` and `last` have the following keys:
 *   - `line`: a positive number indicating a one-based line index, or null if
 *     there is no information.
 *   - `col`: a positive number indicating a one-based column index within the
 *     line, or null if there is no information.
 * - `text`: a string, containing the text of the message.
 *
 * The top-level output object also has the key `too_many_errors`, containing a
 * Boolean value indicating whether the build terminated due to too many errors.
 *
 * Unknown keys should be ignored, so that they can be used for future
 * extensions. */
class diag_message_handler_json: public diag_message_printer {
public:
  class iterator {
  public:
    using iterator_category = std::output_iterator_tag;
    using value_type = void;
    using difference_type = std::ptrdiff_t;
    using pointer = void;
    using reference = void;

    /** @throws std::ios_base::failure */
    iterator& operator=(char8_t ch);

    iterator& operator++() noexcept { return *this; }

    iterator& operator++(int) noexcept { return *this; }

    [[nodiscard]] iterator& operator*() noexcept { return *this; }
  };

  static_assert(std::output_iterator<iterator, char8_t>);

  /** @throws std::ios_base::failure */
  [[nodiscard]] static iterator message_start(
    nycleus::source_location loc,
    nycleus::diag_message_kind kind
  );

  /** @throws std::ios_base::failure */
  static void message_end();

  /** @throws std::ios_base::failure */
  virtual void print(std::vector<std::unique_ptr<nycleus::diagnostic>> const&,
    bool too_many_errors) override;
};

// TODO: diag_message_handler_json_raw

#endif
