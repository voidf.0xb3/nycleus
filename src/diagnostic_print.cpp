#include "diagnostic_print.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_print.hpp"
#include "nycleus/source_location.hpp"
#include "unicode/windows.hpp"
#include "util/nums.hpp"
#include "util/u8compat.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iostream>
#include <memory>
#include <vector>

using namespace util::u8compat_literals;

static_assert(nycleus::diag_message_handler<diag_message_handler_human>);

diag_message_handler_human::iterator&
    diag_message_handler_human::iterator::operator=(char8_t ch) {
  std::cerr << static_cast<char>(ch);
  return *this;
}

diag_message_handler_human::iterator diag_message_handler_human::message_start(
  nycleus::source_location loc,
  nycleus::diag_message_kind kind
) const {
  using enum nycleus::diag_message_kind;
  if(color_) {
    switch(kind) {
      case error:
        // bold red
        std::cerr << "\x1B\x5B\x31\x3B\x33\x31\x6D";
        break;

      case warning:
        // bold yellow
        std::cerr << "\x1B\x5B\x31\x3B\x33\x33\x6D";
        break;

      case notice:
        // bold blue
        std::cerr << "\x1B\x5B\x31\x3B\x33\x34\x6D";
        break;

      default:
        util::unreachable();
    }
  }
  switch(kind) {
    case error: std::cerr << u8"error: "_ch; break;
    case warning: std::cerr << u8"warning: "_ch; break;
    case notice: std::cerr << u8"notice: "_ch; break;
    default: util::unreachable();
  }
  if(color_) {
    // reset; bold
    std::cerr << "\x1B\x5B\x3B\x31\x6D";
  }
  if(loc.file) {
    std::cerr << util::as_char((*loc.file)->u8string());
  } else {
    std::cerr << u8"(unknown file)"_ch;
  }
  std::cerr
    << u8':'_ch << loc.first.line
    << u8':'_ch << loc.first.col
    << u8'-'_ch << loc.last.line
    << u8':'_ch << loc.last.col;
  if(color_) {
    // reset
    std::cerr << "\x1B\x5B\x6D";
  }
  std::cerr << u8": "_ch;

  return iterator{};
}

void diag_message_handler_human::message_end() {
  std::cerr << u8'\n'_ch;
}

void diag_message_handler_human::print(
  std::vector<std::unique_ptr<nycleus::diagnostic>> const& diags,
  bool too_many_errors
) {
  unicode::init_unicode_output();
  for(auto const& diag: diags) {
    assert(diag);
    nycleus::print_diagnostic(*diag, *this);
  }
  if(too_many_errors) {
    if(color_) {
      // bold red
      std::cerr << "\x1B\x5B\x31\x3B\x33\x31\x6D";
    }
    std::cerr << u8"too many errors, stopping\n"_ch;
    if(color_) {
      // reset
      std::cerr << "\x1B\x5B\x6D";
    }
  }
}

static_assert(nycleus::diag_message_handler<diag_message_handler_json>);

diag_message_handler_json::iterator&
    diag_message_handler_json::iterator::operator=(char8_t ch) {
  if(ch == u8'\b') {
    std::cerr << u8"\\b"_ch;
  } else if(ch == u8'\t') {
    std::cerr << u8"\\t"_ch;
  } else if(ch == u8'\n') {
    std::cerr << u8"\\n"_ch;
  } else if(ch == u8'\f') {
    std::cerr << u8"\\f"_ch;
  } else if(ch == u8'\r') {
    std::cerr << u8"\\r"_ch;
  } else if(ch == u8'"') {
    std::cerr << u8"\\\""_ch;
  } else if(ch == u8'\\') {
    std::cerr << u8"\\\\"_ch;
  } else if(ch < 0x10) {
    std::cerr << u8"\\u000"_ch << static_cast<char>(u8'0' + ch);
  } else if(ch < 0x20) {
    std::cerr << u8"\\u001"_ch
      << static_cast<char>(u8'0' + ch - 0x10);
  } else {
    std::cerr << static_cast<char>(ch);
  }
  return *this;
}

diag_message_handler_json::iterator diag_message_handler_json::message_start(
  nycleus::source_location loc,
  nycleus::diag_message_kind kind
) {
  std::cerr << u8"{\"kind\":\""_ch;

  using enum nycleus::diag_message_kind;
  switch(kind) {
    case error: std::cerr << u8"error"_ch; break;
    case warning: std::cerr << u8"warning"_ch; break;
    case notice: std::cerr << u8"notice"_ch; break;
    default: util::unreachable();
  }

  std::cerr << u8"\",\"loc\":{\"file\":"_ch;
  if(loc.file) {
    std::cerr << u8'"'_ch;
    std::ranges::copy((*loc.file)->u8string(), iterator{});
    std::cerr << u8'"'_ch;
  } else {
    std::cerr << u8"null"_ch;
  }
  std::cerr << u8",\"first\":{\"line\":"_ch;

  auto print_num{[](std::uint64_t x) {
    if(x == 0) {
      std::cerr << u8"null"_ch;
    } else {
      std::cerr << util::as_char(util::to_u8string(x));
    }
  }};

  print_num(loc.first.line);
  std::cerr << u8",\"col\":"_ch;
  print_num(loc.first.col);
  std::cerr << u8"},\"last\":{\"line\":"_ch;
  print_num(loc.last.line);
  std::cerr << u8",\"col\":"_ch;
  print_num(loc.last.col);
  std::cerr << u8"}},\"text\":\""_ch;

  return iterator{};
}

void diag_message_handler_json::message_end() {
  std::cerr << u8"\"}"_ch;
}

void diag_message_handler_json::print(
  std::vector<std::unique_ptr<nycleus::diagnostic>> const& diags,
  bool too_many_errors
) {
  std::cerr << u8"{\"diagnostics\":["_ch;

  bool first{true};
  for(auto const& diag: diags) {
    assert(diag);
    if(first) {
      first = false;
    } else {
      std::cerr << u8','_ch;
    }
    nycleus::print_diagnostic(*diag, *this);
  }

  std::cerr << u8"],\"too_many_errors\":"_ch
    << (too_many_errors ? u8"true"_ch : u8"false"_ch) << u8"}\n"_ch;
}
