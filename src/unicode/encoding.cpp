#include "unicode/encoding.hpp"
#include <gsl/gsl>
#include <cstdint>

namespace u = unicode;

gsl::czstring u::convert_exception::what() const noexcept {
  return "unicode::convert_exception";
}

template struct u::utf8<char8_t>;
template struct u::utf8<char>;

static_assert(u::error_handler<u::eh_assume_valid<char32_t>, char32_t,
  u::utf8_error>);
static_assert(u::error_handler<u::eh_assume_valid<char32_t>, char32_t,
  u::utf16_error>);

static_assert(u::error_handler<u::eh_replace<char32_t>, char32_t,
  u::utf8_error>);
static_assert(u::error_handler<u::eh_replace<char32_t>, char32_t,
  u::utf16_error>);

static_assert(u::error_handler<u::eh_throw<char32_t>, char32_t,
  u::utf8_error>);
static_assert(u::error_handler<u::eh_throw<char32_t>, char32_t,
  u::utf16_error>);

static_assert(u::encoding<u::utf8<char8_t>>);
static_assert(u::encoding<u::utf8<std::uint8_t>>);
static_assert(u::encoding<u::utf8<char>>);
static_assert(u::encoding<u::utf16<char16_t>>);
static_assert(u::encoding<u::utf16<std::uint16_t>>);
#ifdef _WIN32
  static_assert(u::encoding<u::utf16<wchar_t>>);
#endif

static_assert(u::decodable_common_encoding<u::utf8<char8_t>>);
static_assert(u::decodable_common_encoding<u::utf8<std::uint8_t>>);
static_assert(u::decodable_common_encoding<u::utf8<char>>);
static_assert(u::decodable_common_encoding<u::utf16<char16_t>>);
static_assert(u::decodable_common_encoding<u::utf16<std::uint16_t>>);
#ifdef _WIN32
  static_assert(u::decodable_common_encoding<u::utf16<wchar_t>>);
#endif

static_assert(u::encodable_encoding<u::utf8<char8_t>>);
static_assert(u::encodable_encoding<u::utf8<std::uint8_t>>);
static_assert(u::encodable_encoding<u::utf8<char>>);
static_assert(u::encodable_encoding<u::utf16<char16_t>>);
static_assert(u::encodable_encoding<u::utf16<std::uint16_t>>);
#ifdef _WIN32
  static_assert(u::encodable_encoding<u::utf16<wchar_t>>);
#endif
