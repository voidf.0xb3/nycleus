#include <boost/test/unit_test.hpp>

#include "unicode/casefold.hpp"
#include "unicode/normal.hpp"
#include <algorithm>
#include <concepts>
#include <iterator>
#include <ios>
#include <ranges>
#include <sstream>
#include <string>

namespace {

using sp_cf_view = unicode::casefold_view<
  std::ranges::subrange<std::istreambuf_iterator<char32_t>>>;
using mp_cf_view = unicode::casefold_view<
  std::views::all_t<std::u32string&>>;

static_assert(std::ranges::view<sp_cf_view>);
static_assert(std::ranges::input_range<sp_cf_view>);
static_assert(!std::ranges::forward_range<sp_cf_view>);
static_assert(std::same_as<char32_t, std::ranges::range_value_t<sp_cf_view>>);

static_assert(std::ranges::view<mp_cf_view>);
static_assert(std::ranges::forward_range<mp_cf_view>);
static_assert(!std::ranges::bidirectional_range<mp_cf_view>);
static_assert(std::same_as<char32_t, std::ranges::range_value_t<mp_cf_view>>);

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_unicode)
BOOST_AUTO_TEST_SUITE(casefold)

BOOST_AUTO_TEST_CASE(casefold_one) {
  {
    auto const cf{unicode::detail_::casefold_one(U'a')};
    std::u32string cf_test{U"a"};
    BOOST_TEST(std::ranges::equal(cf, cf_test));
  }

  {
    auto const cf{unicode::detail_::casefold_one(U'A')};
    std::u32string cf_test{U"a"};
    BOOST_TEST(std::ranges::equal(cf, cf_test));
  }

  {
    // CYRILLIC SMALL LETTER YERU
    auto const cf{unicode::detail_::casefold_one(U'\u044B')};
    std::u32string cf_test{U"\u044B"};
    BOOST_TEST(std::ranges::equal(cf, cf_test));
  }

  {
    // CYRILLIC CAPITAL LETTER YERU
    auto const cf{unicode::detail_::casefold_one(U'\u042B')};
    std::u32string cf_test{U"\u044B"};
    BOOST_TEST(std::ranges::equal(cf, cf_test));
  }

  {
    // LATIN SMALL LETTER SHARP S
    auto const cf{unicode::detail_::casefold_one(U'\u00DF')};
    std::u32string cf_test{U"ss"};
    BOOST_TEST(std::ranges::equal(cf, cf_test));
  }

  {
    // ADLAM CAPITAL LETTER ALIF
    auto const cf{unicode::detail_::casefold_one(U'\U0001E900')};
    std::u32string cf_test{U"\U0001E922"};
    BOOST_TEST(std::ranges::equal(cf, cf_test));
  }

  {
    // ADLAM SMALL LETTER ALIF
    auto const cf{unicode::detail_::casefold_one(U'\U0001E922')};
    std::u32string cf_test{U"\U0001E922"};
    BOOST_TEST(std::ranges::equal(cf, cf_test));
  }
}

BOOST_AUTO_TEST_CASE(lazy_input) {
  {
    std::u32string const input{U"tEst STrInG"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    std::u32string const test{U"test string"};
    std::u32string output;
    std::ranges::copy(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    } | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "тЕсТОвАя сТрОкА"
    std::u32string const input{
      U"\u0442\u0415\u0441\u0422\u041E\u0432\u0410\u044F "
      U"\u0441\u0422\u0440\u041E\u043A\u0410"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    // "тестовая строка"
    std::u32string const test{
      U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
      U"\u0441\u0442\u0440\u043E\u043A\u0430"};
    std::u32string output;
    std::ranges::copy(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    } | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ChUỖi kIỂM TRa"
    std::u32string const input{U"ChU\u1ED6i kI\u1EC2M TRa"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    // "chuỗi kiểm tra"
    std::u32string const test{U"chu\u1ED7i ki\u1EC3m tra"};
    std::u32string output;
    std::ranges::copy(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    } | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ᾠδῇ"
    std::u32string const input{U"\u1FA0\u03B4\u1FC7"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    // "ὠιδῆι"
    std::u32string const test{U"\u1F60\u03B9\u03B4\u1FC6\u03B9"};
    std::u32string output;
    std::ranges::copy(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    } | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ᾠδῇ", alternative canonically equivalent form
    std::u32string const input{U"\u03C9\u0345\u0313\u03B4\u03B7\u0345\u0342"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    // "ὠιδῆι"
    std::u32string const test{U"\u1F60\u03B9\u03B4\u1FC6\u03B9"};
    std::u32string output;
    std::ranges::copy(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    } | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }
}

BOOST_AUTO_TEST_CASE(lazy_forward) {
  {
    std::u32string const input{U"tEst STrInG"};
    std::u32string const test{U"test string"};
    std::u32string output;
    std::ranges::copy(input | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "тЕсТОвАя сТрОкА"
    std::u32string const input{
      U"\u0442\u0415\u0441\u0422\u041E\u0432\u0410\u044F "
      U"\u0441\u0422\u0440\u041E\u043A\u0410"};
    // "тестовая строка"
    std::u32string const test{
      U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
      U"\u0441\u0442\u0440\u043E\u043A\u0430"};
    std::u32string output;
    std::ranges::copy(input | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ChUỖi kIỂM TRa"
    std::u32string const input{U"ChU\u1ED6i kI\u1EC2M TRa"};
    // "chuỗi kiểm tra"
    std::u32string const test{U"chu\u1ED7i ki\u1EC3m tra"};
    std::u32string output;
    std::ranges::copy(input | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ᾠδῇ"
    std::u32string const input{U"\u1FA0\u03B4\u1FC7"};
    // "ὠιδῆι"
    std::u32string const test{U"\u1F60\u03B9\u03B4\u1FC6\u03B9"};
    std::u32string output;
    std::ranges::copy(input | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ᾠδῇ", alternative canonically equivalent form
    std::u32string const input{U"\u03C9\u0345\u0313\u03B4\u03B7\u0345\u0342"};
    // "ὠιδῆι"
    std::u32string const test{U"\u1F60\u03B9\u03B4\u1FC6\u03B9"};
    std::u32string output;
    std::ranges::copy(input | unicode::casefold, std::back_inserter(output));
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }
}

BOOST_AUTO_TEST_CASE(eager_input) {
  {
    std::u32string const input{U"tEst STrInG"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    std::u32string const test{U"test string"};
    std::u32string const output{unicode::to_casefold(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    })};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "тЕсТОвАя сТрОкА"
    std::u32string const input{
      U"\u0442\u0415\u0441\u0422\u041E\u0432\u0410\u044F "
      U"\u0441\u0422\u0440\u041E\u043A\u0410"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    // "тестовая строка"
    std::u32string const test{
      U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
      U"\u0441\u0442\u0440\u043E\u043A\u0430"};
    std::u32string const output{unicode::to_casefold(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    })};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ChUỖi kIỂM TRa"
    std::u32string const input{U"ChU\u1ED6i kI\u1EC2M TRa"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    // "chuỗi kiểm tra"
    std::u32string const test{U"chu\u1ED7i ki\u1EC3m tra"};
    std::u32string const output{unicode::to_casefold(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    })};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ᾠδῇ"
    std::u32string const input{U"\u1FA0\u03B4\u1FC7"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    // "ὠιδῆι"
    std::u32string const test{U"\u1F60\u03B9\u03B4\u1FC6\u03B9"};
    std::u32string const output{unicode::to_casefold(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    })};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ᾠδῇ", alternative canonically equivalent form
    std::u32string const input{U"\u03C9\u0345\u0313\u03B4\u03B7\u0345\u0342"};
    std::basic_istringstream<char32_t> input_stream{input};
    input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
    // "ὠιδῆι"
    std::u32string const test{U"\u1F60\u03B9\u03B4\u1FC6\u03B9"};
    std::u32string const output{unicode::to_casefold(std::ranges::subrange{
      std::istreambuf_iterator<char32_t>{input_stream},
      std::istreambuf_iterator<char32_t>{}
    })};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }
}

BOOST_AUTO_TEST_CASE(eager_forward) {
  {
    std::u32string const input{U"tEst STrInG"};
    std::u32string const test{U"test string"};
    std::u32string const output{unicode::to_casefold(input)};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "тЕсТОвАя сТрОкА"
    std::u32string const input{
      U"\u0442\u0415\u0441\u0422\u041E\u0432\u0410\u044F "
      U"\u0441\u0422\u0440\u041E\u043A\u0410"};
    // "тестовая строка"
    std::u32string const test{
      U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
      U"\u0441\u0442\u0440\u043E\u043A\u0430"};
    std::u32string const output{unicode::to_casefold(input)};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ChUỖi kIỂM TRa"
    std::u32string const input{U"ChU\u1ED6i kI\u1EC2M TRa"};
    // "chuỗi kiểm tra"
    std::u32string const test{U"chu\u1ED7i ki\u1EC3m tra"};
    std::u32string const output{unicode::to_casefold(input)};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ᾠδῇ"
    std::u32string const input{U"\u1FA0\u03B4\u1FC7"};
    // "ὠιδῆι"
    std::u32string const test{U"\u1F60\u03B9\u03B4\u1FC6\u03B9"};
    std::u32string const output{unicode::to_casefold(input)};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }

  {
    // "ᾠδῇ", alternative canonically equivalent form
    std::u32string const input{U"\u03C9\u0345\u0313\u03B4\u03B7\u0345\u0342"};
    // "ὠιδῆι"
    std::u32string const test{U"\u1F60\u03B9\u03B4\u1FC6\u03B9"};
    std::u32string const output{unicode::to_casefold(input)};
    BOOST_TEST(std::ranges::equal(test | unicode::nfc, output | unicode::nfc));
  }
}

BOOST_AUTO_TEST_SUITE_END() // casefold
BOOST_AUTO_TEST_SUITE_END() // test_unicode
