#ifndef UNICODE_BASIC_HPP_INCLUDED_UN7X3O9KXCTXU2MV53WU7GVQJ
#define UNICODE_BASIC_HPP_INCLUDED_UN7X3O9KXCTXU2MV53WU7GVQJ

namespace unicode {

/** @brief The first surrogate code point. */
constexpr char32_t surrogates_start{0xD800};

/** @brief The first code point after the surrogate range. */
constexpr char32_t surrogates_end{0xE000};

/** @brief Determines whether the code point is a surrogate. */
[[nodiscard]] constexpr bool is_surrogate(char32_t codepoint) noexcept {
  return codepoint >= surrogates_start && codepoint < surrogates_end;
}

/** @brief The highest valid code point. */
constexpr char32_t last_codepoint{0x10FFFF};

/** @brief Determines whether the given integer is a valid Unicode scalar value.
 *
 * The integer must be within the Unicode code space and not be a surrogate code
 * point. */
[[nodiscard]] constexpr bool is_scalar_value(char32_t codepoint) noexcept {
  return codepoint <= last_codepoint && !is_surrogate(codepoint);
}

/** @brief An invalid code point value that can be used as a sentinel. */
constexpr char32_t sentinel{0x110000};

} // unicode

#endif
