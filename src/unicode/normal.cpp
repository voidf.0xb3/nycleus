#include "unicode/normal.hpp"
#include "unicode/basic.hpp"
#include "unicode/encoding.hpp"
#include "unicode/props.hpp"
#include "unicode/utrie.hpp"
#include "util/safe_int.hpp"
#include "util/unbounded_view.hpp"
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <ranges>
#include <string>

#include "unicode/ucd/comp.hpp"
#include "unicode/ucd/decomp.hpp"
#include "unicode/ucd/full_comp_excl.hpp"
#include "unicode/ucd/nfc_qc.hpp"
#include "unicode/ucd/nfd_qc.hpp"

unicode::qc_t unicode::nfc_traits::qc(char32_t ch) noexcept {
  assert(is_scalar_value(ch));
  return qc_t{UTRIE(nfc_qc)::get(ch).unwrap()};
}

unicode::qc_t unicode::nfd_traits::qc(char32_t ch) noexcept {
  assert(is_scalar_value(ch));
  return qc_t{UTRIE(nfd_qc)::get(ch).unwrap()};
}

bool unicode::detail_::full_comp_excl(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return UTRIE(full_comp_excl)::get(codepoint).unwrap();
}

unicode::detail_::vec_char32<unicode::detail_::max_decomp_size.unwrap()>
unicode::detail_::canon_decompose(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  auto codepoint_int{util::wrap(static_cast<std::uint32_t>(codepoint))};

  if(
    detail_::hangul_s_base <= codepoint_int
    && codepoint_int < detail_::hangul_s_base + detail_::hangul_s_count
  ) {
    codepoint_int -= detail_::hangul_s_base;
    util::uint32_t const t_index{codepoint_int % (detail_::hangul_t_count + 1)};
    if(t_index == 0) {
      return {
        (detail_::hangul_l_base + codepoint_int / detail_::hangul_n_count).unwrap(),
        (detail_::hangul_v_base + (codepoint_int % detail_::hangul_n_count)
          / (detail_::hangul_t_count + 1)).unwrap()
      };
    } else {
      return {
        (detail_::hangul_s_base + (codepoint_int - t_index)).unwrap(),
        (detail_::hangul_t_base + (t_index - 1)).unwrap()
      };
    }
  }

  util::size_t const decomp{UTRIE(decomp)::get(codepoint)};
  util::size_t const len{decomp & 0x3};
  if(len == 0) {
    return {codepoint};
  }

  vec_char32<max_decomp_size.unwrap()> result{};
  if(len == 2) {
    constexpr util::size_t index_width{ucd::decomp_index0_width + ucd::decomp_index1_width};
    util::size_t indexes_packed{util::size_t::uninitialized()};
    if constexpr(index_width <= 16) {
      indexes_packed = ucd::decomp_array[(decomp >> 2).unwrap()];
    } else if constexpr(index_width <= 32) {
      indexes_packed = ucd::decomp_array[(decomp >> 2).unwrap()]
        | (ucd::decomp_array[((decomp >> 2) + 1).unwrap()] << 16);
    } else {
      indexes_packed = ucd::decomp_array[(decomp >> 2).unwrap()]
        | (ucd::decomp_array[((decomp >> 2) + 1).unwrap()] << 16)
        | (ucd::decomp_array[((decomp >> 2) + 2).unwrap()] << 32)
        | (ucd::decomp_array[((decomp >> 2) + 3).unwrap()] << 48);
    }

    util::size_t const index0{indexes_packed
      & ((util::size_t{1} << ucd::decomp_index0_width) - 1)};
    util::size_t const index1{indexes_packed >> ucd::decomp_index0_width};

    std::ranges::copy(util::unbounded_view{&ucd::decomp_array0[index0.unwrap()]}
      | std::views::transform(&util::uint16_t::unwrap)
      | utf16_decode() | std::views::take(1), std::back_inserter(result));
    std::ranges::copy(util::unbounded_view{&ucd::decomp_array1[index1.unwrap()]}
      | std::views::transform(&util::uint16_t::unwrap)
      | utf16_decode() | std::views::take(1), std::back_inserter(result));

    return result;
  }

  std::ranges::copy(util::unbounded_view{&ucd::decomp_array[(decomp >> 2).unwrap()]}
    | std::views::transform(&util::uint16_t::unwrap)
    | utf16_decode() | std::views::take(len.unwrap()), std::back_inserter(result));
  return result;
}

char32_t unicode::detail_::canon_compose(char32_t a, char32_t b) noexcept {
  assert(is_scalar_value(a));
  assert(is_scalar_value(b));

  auto const a_int{util::wrap(static_cast<std::uint32_t>(a))};
  auto const b_int{util::wrap(static_cast<std::uint32_t>(b))};

  if(detail_::hangul_l_base <= a_int
      && a_int < detail_::hangul_l_base + detail_::hangul_l_count
      && detail_::hangul_v_base <= b_int
      && b_int < detail_::hangul_v_base + detail_::hangul_v_count) {
    util::uint32_t const l_index{a_int - detail_::hangul_l_base};
    util::uint32_t const v_index{b_int - detail_::hangul_v_base};
    return (detail_::hangul_s_base + l_index * detail_::hangul_n_count
      +  v_index * (detail_::hangul_t_count + 1)).unwrap();
  }

  if(
    detail_::hangul_s_base <= a_int
    && a_int < detail_::hangul_s_base + detail_::hangul_s_count
    && (a_int - detail_::hangul_s_base) % (detail_::hangul_t_count + 1) == 0
    && detail_::hangul_t_base <= b_int
    && b_int < detail_::hangul_t_base + detail_::hangul_t_count
  ) {
    util::uint32_t const t_index{b_int - detail_::hangul_t_base};
    return (a_int + t_index + 1).unwrap();
  }

  util::size_t const idx0{UTRIE(comp0)::get(a)};
  util::size_t const idx1{UTRIE(comp1)::get(b)};
  if(idx0 == 0 || idx1 == 0) {
    return sentinel;
  }
  util::uint16_t comp{ucd::comp_array[
    ((idx1 - 1) * (ucd::comp_array_mult) + (idx0 - 1)).unwrap()]};
  if(comp == 0xDFFF) {
    return sentinel;
  }
  if(!is_surrogate(comp.unwrap())) {
    return comp.unwrap();
  }

  // Surrogate values (except for 0xDFFF) in the composition array represent
  // indexes into the supplementary code points array.

  comp -= 0xD800;
  util::size_t const base{5 * (comp / 2)};
  if(comp % 2 == 0) {
    return (
      (
        ucd::comp_suppl[base.unwrap()].cast<util::uint32_t>()
        | (ucd::comp_suppl[(base + 1).unwrap()].cast<util::uint32_t>() << 8)
        | ((ucd::comp_suppl[(base + 2).unwrap()].cast<util::uint32_t>() & 0x0F) << 16)
      ) + 0x10000
    ).unwrap();
  } else {
    return (
      (
        (ucd::comp_suppl[(base + 2).unwrap()].cast<util::uint32_t>() >> 4)
        | (ucd::comp_suppl[(base + 3).unwrap()].cast<util::uint32_t>() << 4)
        | (ucd::comp_suppl[(base + 4).unwrap()].cast<util::uint32_t>() << 12)
      ) + 0x10000
    ).unwrap();
  }
}

unicode::detail_::vec_char32<unicode::detail_::max_decomp_size.unwrap()>
unicode::detail_::full_decompose(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  auto codepoint_int{util::wrap(static_cast<std::uint32_t>(codepoint))};

  if(hangul_s_base <= codepoint_int && codepoint_int < hangul_s_base + hangul_s_count) {
    codepoint_int -= hangul_s_base;

    vec_char32<max_decomp_size.unwrap()> decomp{
      (hangul_l_base + codepoint_int / hangul_n_count).unwrap(),
      (hangul_v_base + (codepoint_int % hangul_n_count) / (hangul_t_count + 1)).unwrap()
    };

    util::uint32_t const t_index{codepoint_int % (hangul_t_count + 1)};
    if(t_index != 0) {
      decomp.push_back((hangul_t_base + (t_index - 1)).unwrap());
    }
    return decomp;
  }

  vec_char32<max_decomp_size.unwrap()> ret{canon_decompose(codepoint)};
  if(ret.size() == 1 && ret[0] == codepoint) {
    return ret;
  }

  for(auto it{ret.begin()}; it != ret.end(); ++it) {
    auto decomp{full_decompose(*it)};
    assert(util::wrap(ret.size()) + util::wrap(decomp.size()) - 1 <= max_decomp_size);
    it = ret.replace(it, it + 1, decomp.cbegin(), decomp.cend(), util::unchecked);
  }
  return ret;
}

void unicode::detail_::canonical_ordering(
  std::u32string& buf,
  std::u32string::iterator it_begin
) noexcept {
  for(auto begin{it_begin};;) {
    auto const end{std::find_if(begin, buf.end(), [](char32_t ch) noexcept {
      return ccc(ch) == 0;
    })};
    std::stable_sort(begin, end, [](char32_t a, char32_t b) noexcept {
      return ccc(a) < ccc(b);
    });
    if(end == buf.end()) {
      break;
    }
    begin = end + 1;
  }
}

void unicode::detail_::canonical_composition(
  std::u32string& buf,
  std::u32string::iterator it_begin
) noexcept {
  auto starter{util::wrap(
    std::find_if(it_begin, buf.end(), [](char32_t ch) {
      return ccc(ch) == 0;
    }) - buf.begin()).cast<util::size_t>()};
  if(starter == util::wrap(buf.size())) {
    return;
  }

  util::size_t i{starter + 1};
  ccc_t last_ccc{0};
  while(i != util::wrap(buf.size())) {
    ccc_t const cur_ccc{ccc(buf[i.unwrap()])};
    bool has_composed{false};

    if(last_ccc < cur_ccc || last_ccc == 0) {
      char32_t const comp{canon_compose(buf[starter.unwrap()], buf[i.unwrap()])};
      if(comp != sentinel && !full_comp_excl(comp)) {
        buf[starter.unwrap()] = comp;
        buf.erase(i.unwrap(), 1);
        has_composed = true;
      }
    }

    if(!has_composed) {
      if(cur_ccc == 0) {
        starter = i;
      }
      last_ccc = cur_ccc;
      ++i;
    }
  }
}
