#ifndef UNICODE_ENCODING_HPP_INCLUDED_4UZ1DMAWBQ5QQM6P9W5DMZM0I
#define UNICODE_ENCODING_HPP_INCLUDED_4UZ1DMAWBQ5QQM6P9W5DMZM0I

#include "unicode/basic.hpp"
#include "util/iterator.hpp"
#include "util/iterator_facade.hpp"
#include "util/range_adaptor_closure.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <optional>
#include <ranges>
#include <type_traits>
#include <utility>

namespace unicode {

// Concepts ////////////////////////////////////////////////////////////////////

/** @brief A concept for values worked upon by encodings. */
template<typename T>
concept encoding_data = std::regular<T>;

/** @brief A concept for an error handler used by encodings' decoding
 * operations.
 *
 * Whenever an error occurs, the error handler is invoked and given a value
 * representing the specific error that occured. The handler can handle the
 * error in any way it chooses, and if it chooses to continue iteration, it must
 * return a value indicating the result of decoding the erroneous sequence. */
template<typename EH, typename Ch, typename Error>
concept error_handler = std::movable<EH> && encoding_data<Ch>
  && encoding_data<Error> && std::is_invocable_r_v<Ch, EH const&, Error>;

/** @brief A concrete encoding that can be plugged into the generic encoding
 * machinery.
 *
 * In general, an encoding needs to define two types: the code unit type (the
 * element type of the encoded range) and the code point type (the element type
 * of the decoded range). More functionality is required for specific
 * operations. */
template<typename T>
concept encoding = std::regular<T>
  && encoding_data<typename T::encoded_type>
  && encoding_data<typename T::decoded_type>;

/** @brief An encoding that can be used for decoding.
 *
 * The range with the given iterator and sentinel types needs to produce
 * elements with the given type. The encoding type needs to provide the
 * following hooks:
 * - decode(Iterator&, Sentinel const&, EH const&): Reads the iterator to
 *   retrieve the code units forming a single code point and returns it. After
 *   the call returns, the iterator shall point to the first code unit after the
 *   decoded code point. In case of error, the error handler is invoked to
 *   either produce the decoded code point or handle the error in some other
 *   way.
 * - increment(Iterator&): Increment the iterator, consuming the code units
 *   comprising the single code point without actually decoding it. It is
 *   assumed that the supplied range contains no encoding errors; in particular,
 *   it is assumed that the underlying range will not unexpectedly end, so there
 *   is no need to provide a sentinel.
 * - eh_output_type: The return type expected from the error handler. A value of
 *   this type will be converted into an output code point by the `decode` call
 *   in an encoding-specific manner.
 * - optional_decoded_type: A type that can store an output code point or a
 *   special empty value. This doesn't have to be an std::optional; for
 *   instance, char32_t has a lot of values that are not valid Unicode code
 *   points, which can be used to indicate empty values.
 * - make_optional(decoded_type): A static member function that converts the
 *   given code point value into a value of the optional type.
 * - make_empty_optional(): A static member function that returns the empty
 *   value of the optional type.
 * - get_optional_value(optional_decoded_type): A static member function that
 *   converts the given value of the optional type into a true std::optional.
 *
 * For a stateful encoding, the encoding object itself stores the encoding's
 * state, which is updated by the calls to `decode` and `increment`. */
template<typename T, typename Iterator, typename Sentinel, typename EH>
concept decodable_encoding = encoding<T>
  && encoding_data<typename T::optional_decoded_type>
  && encoding_data<typename T::eh_output_type>
  && std::input_iterator<Iterator>
  && std::same_as<std::iter_value_t<Iterator>, typename T::encoded_type>
  && std::sentinel_for<Sentinel, Iterator>
  && error_handler<EH, typename T::eh_output_type, typename T::error_type>
  && requires(
    T& t,
    Iterator& it,
    Sentinel iend,
    EH eh,
    typename T::decoded_type ch,
    typename T::optional_decoded_type opt_ch
  ) {
    { t.decode(it, iend, eh) } -> std::same_as<typename T::decoded_type>;
    { T::make_optional(ch) } -> std::same_as<typename T::optional_decoded_type>;
    { T::make_empty_optional() }
      -> std::same_as<typename T::optional_decoded_type>;
    { T::get_optional_value(opt_ch) }
      -> std::same_as<std::optional<typename T::decoded_type>>;
    t.increment(it);
  };

/** @brief An encoding type that can produce a common decoded range.
 *
 * For a common range, we need to be able to obtain the encoding object to use
 * for the end iterator of the range. The `make_end` function does just that. */
template<typename T>
concept decodable_common_encoding = encoding<T> && requires(T& t) {
  { t.make_end() } -> std::same_as<T>;
};

/** @brief An encoding type that can produce a bidirectional decoded range.
 *
 * This requires a hook, `decrement(Iterator&)`, which decrements the underlying
 * iterator to skip over the code units comprising a single code point. It is
 * assumed that the underlying range contains no encoding errors; in particular,
 * it is assumed that the underlying range will not unexpectedly end, so there
 * is no need to provide an iterator to the beginning of the underlying range.
 */
template<typename T, typename Iterator>
concept decrementable_encoding = encoding<T>
  && std::bidirectional_iterator<Iterator>
  && std::same_as<std::iter_value_t<Iterator>, typename T::encoded_type>
  && requires(T& t, Iterator& it) {
    t.decrement(it);
  };

/** @brief An encoding type that supports encoding text.
 *
 * This requires the following hooks:
 * - num_code_units(decoded_type): Returns the number of code units necessary to
 *   encode the given code point.
 * - encode(decoded_type, std::size_t): Returns the code unit at the given index
 *   in the encoding of the given code point.
 *
 * Note that, unlike the decoding API, this API does not support stateful
 * encodings. This is sufficient for our needs. */
template<typename T>
concept encodable_encoding = encoding<T>
  && requires(T const& t, typename T::decoded_type ch, std::size_t byte) {
    { t.num_code_units(ch) } -> std::same_as<std::size_t>;
    { t.encode(ch, byte) } -> std::same_as<typename T::encoded_type>;
  };

// Error handlers //////////////////////////////////////////////////////////////

/** @brief A decoding error handler that produces undefined behavior in case of
 * errors.
 *
 * This is used to indicate that the underlying range is guaranteed to have no
 * errors in order to disable error checking. This is also required to enable
 * an optimized increment procedure and to have a bidirectional range. */
template<encoding_data Ch = char32_t>
struct eh_assume_valid {
  [[noreturn]] Ch operator()(encoding_data auto) const noexcept {
    util::unreachable();
  }
};

/** @brief A decoding error handler that replaces erroneous sequences with the
 * given code point. */
template<encoding_data Ch = char32_t, Ch Replacement = U'\uFFFD'>
struct eh_replace {
  constexpr Ch operator()(encoding_data auto) const noexcept {
    return Replacement;
  }
};

/** @brief An exception thrown by the eh_throw decoding error handler. */
class convert_exception: public std::exception {
public:
  convert_exception() noexcept = default;
  [[nodiscard]] virtual gsl::czstring what() const noexcept override;
};

/** @brief A decoding error handler that throws convert_exception on errors. */
template<encoding_data Ch = char32_t>
struct eh_throw {
  /** @throws convert_exception */
  [[noreturn]] Ch operator()(encoding_data auto) const {
    throw convert_exception{};
  }
};

// Concrete encodings //////////////////////////////////////////////////////////

namespace detail_ {

/** @brief The base class for UTF-8 and UTF-16 encodings. */
struct encoding_base {
  using decoded_type = char32_t;
  using optional_decoded_type = char32_t;
  using eh_output_type = char32_t;

  [[nodiscard]] static constexpr char32_t make_optional(char32_t ch) noexcept {
    return ch;
  }

  [[nodiscard]] static constexpr char32_t make_empty_optional() noexcept {
    return sentinel;
  }

  [[nodiscard]] static constexpr std::optional<char32_t> get_optional_value(
    char32_t ch
  ) noexcept {
    if(ch == sentinel) {
      return std::nullopt;
    } else {
      return ch;
    }
  }
};

} // detail_

/** @brief UTF-8 encoding errors. */
enum class utf8_error {
  /** @brief A continuation byte found where the initial byte of a code point is
   * expected. */
  unexpected_continuation_byte,

  /** @brief A non-continuation byte found where a continuation byte was
   * expected. */
  expected_continuation_byte,

  /** @brief The end of input has been encountered while processing a multibyte
   * encoding of a code point. */
  unexpected_end_of_input,

  /** @brief The encoding of a surrogate code point has been observed. */
  surrogate,

  /** @brief A two-byte overlong encoding has been encountered. */
  overlong_two_bytes,

  /** @brief A three-byte overlong encoding has been encountered. */
  overlong_three_bytes,

  /** @brief A four-byte overlong encoding has been encountered. */
  overlong_four_bytes,

  /** @brief A four-byte encoding encodes a value that is outside the Unicode
   * code space. */
  out_of_range,

  /** @brief A code unit was encountered that does not fit UTF-8 syntax.
   *
   * Note that certain code units can never occur in valid UTF-8, but
   * technically fit UTF-8 syntax. In those cases, more specific error codes are
   * used. Specifically:
   * - Bytes 0xC0 and 0xC1 fit the two-byte encoding syntax, but all two-byte
   *   encodings starting with those bytes are overlong. Those bytes produce the
   *   overlong_two_bytes error. (This is the only situation when that error
   *   code is produced.)
   * - Bytes 0xF5, 0xF6, and 0xF7 fit the four-byte encoding syntax, but all
   *   four-byte encodings starting with those bytes encode values outside of
   *   the Unicode code space. Those bytes produce the out_of_range error. */
  invalid_code_unit
};

/** @brief A type that can be used for UTF-8 code units: one of char, char8_t,
 * or std::uint8_t. */
template<typename T>
concept utf8_code_unit = util::one_of<T, char, char8_t, std::uint8_t>;

/** @brief The UTF-8 encoding.
 *
 * It is parametrized by the type used to represent code units: either char or
 * char8_t. */
template<utf8_code_unit Ch = char8_t>
struct utf8: detail_::encoding_base {
  using encoded_type = Ch;
  using error_type = utf8_error;

  template<
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel,
    error_handler<char32_t, utf8_error> EH
  >
  requires std::same_as<std::iter_value_t<Iterator>, Ch>
  static constexpr char32_t decode(Iterator& it, Sentinel iend, EH eh) noexcept(
    noexcept(*it) && noexcept(++it) && noexcept(it == iend)
    && std::is_nothrow_invocable_v<EH const&, utf8_error>
  ) {
    using ch_t = std::make_unsigned_t<Ch>;
    ch_t code_unit{static_cast<ch_t>(*it)};
    ch_t const first_code_unit{code_unit};
    ++it;

    if(code_unit <= 0x7F) {
      // One-byte encoding
      return static_cast<char32_t>(code_unit);
    } else if(code_unit <= 0xBF) {
      return std::invoke(eh, utf8_error::unexpected_continuation_byte);
    } else if(code_unit == 0xC0 || code_unit == 0xC1) {
      return std::invoke(eh, utf8_error::overlong_two_bytes);
    } else if(code_unit <= 0xDF) {
      // Two-byte encoding
      char32_t result{static_cast<char32_t>(code_unit & 0x1F) << 6};

      if(it == iend) {
        return std::invoke(eh, utf8_error::unexpected_end_of_input);
      }
      code_unit = static_cast<ch_t>(*it);
      if((code_unit & 0xC0) != 0x80) {
        return std::invoke(eh, utf8_error::expected_continuation_byte);
      }
      ++it;
      result |= code_unit & 0x3F;

      return result;
    } else if(code_unit <= 0xEF) {
      // Three-byte encoding
      char32_t result{static_cast<char32_t>(code_unit & 0x0F) << 12};

      if(it == iend) {
        return std::invoke(eh, utf8_error::unexpected_end_of_input);
      }
      code_unit = static_cast<ch_t>(*it);
      if((code_unit & 0xC0) != 0x80) {
        return std::invoke(eh, utf8_error::expected_continuation_byte);
      }
      if(first_code_unit == 0xE0 && (code_unit & 0xE0) == 0x80) {
        return std::invoke(eh, utf8_error::overlong_three_bytes);
      }
      if(first_code_unit == 0xED && (code_unit & 0xE0) == 0xA0) {
        return std::invoke(eh, utf8_error::surrogate);
      }
      ++it;
      result |= (code_unit & 0x3F) << 6;

      if(it == iend) {
        return std::invoke(eh, utf8_error::unexpected_end_of_input);
      }
      code_unit = static_cast<ch_t>(*it);
      if((code_unit & 0xC0) != 0x80) {
        return std::invoke(eh, utf8_error::expected_continuation_byte);
      }
      ++it;
      result |= code_unit & 0x3F;

      return result;
    } else if(code_unit <= 0xF4) {
      // Four-byte encoding
      char32_t result{static_cast<char32_t>(code_unit & 0x07) << 18};

      if(it == iend) {
        return std::invoke(eh, utf8_error::unexpected_end_of_input);
      }
      code_unit = static_cast<ch_t>(*it);
      if((code_unit & 0xC0) != 0x80) {
        return std::invoke(eh, utf8_error::expected_continuation_byte);
      }
      if(first_code_unit == 0xF0 && (code_unit & 0xF0) == 0x80) {
        return std::invoke(eh, utf8_error::overlong_four_bytes);
      }
      if(first_code_unit == 0xF4 && (code_unit & 0xF0) != 0x80) {
        return std::invoke(eh, utf8_error::out_of_range);
      }
      ++it;
      result |= (code_unit & 0x3F) << 12;

      if(it == iend) {
        return std::invoke(eh, utf8_error::unexpected_end_of_input);
      }
      code_unit = static_cast<ch_t>(*it);
      if((code_unit & 0xC0) != 0x80) {
        return std::invoke(eh, utf8_error::expected_continuation_byte);
      }
      ++it;
      result |= (code_unit & 0x3F) << 6;

      if(it == iend) {
        return std::invoke(eh, utf8_error::unexpected_end_of_input);
      }
      code_unit = static_cast<ch_t>(*it);
      if((code_unit & 0xC0) != 0x80) {
        return std::invoke(eh, utf8_error::expected_continuation_byte);
      }
      ++it;
      result |= code_unit & 0x3F;

      return result;
    } else if(code_unit <= 0xF7) {
      return std::invoke(eh, utf8_error::out_of_range);
    } else {
      return std::invoke(eh, utf8_error::invalid_code_unit);
    }
  }

  [[nodiscard]] static constexpr utf8 make_end() noexcept {
    return {};
  }

  template<std::input_iterator Iterator>
  requires std::same_as<std::iter_value_t<Iterator>, Ch>
  static constexpr void increment(Iterator& it)
      noexcept(noexcept(*it) && noexcept(util::advance(it, 0))) {
    auto const code_unit{*it};
    std::size_t byte_count;
    if((code_unit & 0x80) == 0) {
      byte_count = 1;
    } else if((code_unit & 0xE0) == 0xC0) {
      byte_count = 2;
    } else if((code_unit & 0xF0) == 0xE0) {
      byte_count = 3;
    } else if((code_unit & 0xF8) == 0xF0) {
      byte_count = 4;
    } else {
      util::unreachable();
    }
    util::advance(it, byte_count);
  }

  template<std::bidirectional_iterator Iterator>
  requires std::same_as<std::iter_value_t<Iterator>, Ch>
  static constexpr void decrement(Iterator& it)
      noexcept(noexcept(--it) && noexcept(*it)) {
    do {
      --it;
    } while((*it & 0xC0) == 0x80);
  }

  [[nodiscard]] static constexpr std::size_t num_code_units(char32_t ch)
      noexcept {
    assert(is_scalar_value(ch));
    if(ch < 0x0080) {
      return 1;
    } else if(ch < 0x0800) {
      return 2;
    } else if(ch < 0x10000) {
      return 3;
    } else {
      return 4;
    }
  }

  [[nodiscard]] static constexpr Ch encode(char32_t ch, std::size_t byte)
      noexcept {
    assert(is_scalar_value(ch));
    assert(byte < num_code_units(ch));
    if(ch < 0x0080) {
      return static_cast<Ch>(ch);
    } else if(ch < 0x0800) {
      switch(byte) {
        case 0: return static_cast<Ch>(0xC0 | (ch >> 6));
        case 1: return static_cast<Ch>(0x80 | (ch & 0x3F));
        default: util::unreachable();
      }
    } else if(ch < 0x10000) {
      switch(byte) {
        case 0: return static_cast<Ch>(0xE0 | (ch >> 12));
        case 1: return static_cast<Ch>(0x80 | ((ch >> 6) & 0x3F));
        case 2: return static_cast<Ch>(0x80 | (ch & 0x3F));
        default: util::unreachable();
      }
    } else {
      switch(byte) {
        case 0: return static_cast<Ch>(0xF0 | (ch >> 18));
        case 1: return static_cast<Ch>(0x80 | ((ch >> 12) & 0x3F));
        case 2: return static_cast<Ch>(0x80 | ((ch >> 6) & 0x3F));
        case 3: return static_cast<Ch>(0x80 | (ch & 0x3F));
        default: util::unreachable();
      }
    }
  }

  constexpr bool operator==(utf8) const noexcept {
    return true;
  }
};

extern template struct utf8<char8_t>;
extern template struct utf8<char>;

/** @brief Determines if the given UTF-8 code unit is a continuation code unit.
 */
constexpr bool is_utf8_continuation(char8_t ch) noexcept {
  return (ch & 0xC0) == 0x80;
}

/** @brief UTF-16 decoding errors. */
enum class utf16_error {
  /** @brief A leading surrogate code unit is not followed by a trailing
   * surrogate code unit.
   *
   * This is produced when a leading surrogate code unit is followed by a code
   * unit that is not a trailing surrogate code unit, or when a leading
   * surrogate code unit is found at the end of the range. */
  unpaired_leading_surrogate,

  /** @brief A trailing surrogate code unit is not preceded by a leading
   * surrogate code unit. */
  unpaired_trailing_surrogate
};

/** @brief A type that can be used for UTF-16 code units.
 *
 * The char16_t and std::uint16_t types can always be used for UTF-16 code
 * units. Also, wchar_t can be used if it is two bytes long (which is the case
 * on Windows). */
template<typename T>
concept utf16_code_unit = util::one_of<T, char16_t, std::uint16_t>
  || std::same_as<T, wchar_t> && (sizeof(T) == 2);

/** @brief The UTF-16 encoding.
 *
 * It is parametrized by the type used to represent code units: either char16_t
 * or wchar_t if is two bytes long. */
template<utf16_code_unit Ch = char16_t>
struct utf16: detail_::encoding_base {
  using encoded_type = Ch;
  using error_type = utf16_error;

  template<
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel,
    error_handler<char32_t, utf16_error> EH
  >
  requires std::same_as<std::iter_value_t<Iterator>, Ch>
  static constexpr char32_t decode(Iterator& it, Sentinel iend, EH eh) noexcept(
    noexcept(*it) && noexcept(++it) && noexcept(it == iend)
    && std::is_nothrow_invocable_v<EH const&, utf16_error>
  ) {
    using ch_t = std::make_unsigned_t<Ch>;
    ch_t const code_unit{static_cast<ch_t>(*it)};
    ++it;

    if(0xD800 <= code_unit && code_unit <= 0xDBFF) {
      if(it == iend) {
        return std::invoke(eh, utf16_error::unpaired_leading_surrogate);
      }

      ch_t const trailing_code_unit = static_cast<ch_t>(*it);
      if(0xDC00 <= trailing_code_unit && trailing_code_unit <= 0xDFFF) {
        ++it;
        return (static_cast<char32_t>(code_unit - 0xD800) << 10)
          + (trailing_code_unit - 0xDC00) + 0x10000;
      } else {
        return std::invoke(eh, utf16_error::unpaired_leading_surrogate);
      }
    } else if(0xC000 <= code_unit && code_unit <= 0xDFFF) {
      return std::invoke(eh, utf16_error::unpaired_trailing_surrogate);
    } else {
      return static_cast<char32_t>(code_unit);
    }
  }

  [[nodiscard]] static constexpr utf16 make_end() noexcept {
    return {};
  }

  template<std::input_iterator Iterator>
  requires std::same_as<std::iter_value_t<Iterator>, Ch>
  static constexpr void increment(Iterator& it)
      noexcept(noexcept(*it) && noexcept(util::advance(it, 2))) {
    std::size_t byte_count;
    if(auto const code_unit{*it}; 0xD800 <= code_unit && code_unit <= 0xDBFF) {
      byte_count = 2;
    } else {
      byte_count = 1;
    }
    util::advance(it, byte_count);
  }

  template<std::bidirectional_iterator Iterator>
  requires std::same_as<std::iter_value_t<Iterator>, Ch>
  static constexpr void decrement(Iterator& it)
      noexcept(noexcept(--it) && noexcept(*it)) {
    --it;
    if(auto const code_unit{*it}; 0xDC00 <= code_unit && code_unit <= 0xDFFF) {
      --it;
    }
  }

  [[nodiscard]] static constexpr std::size_t num_code_units(char32_t ch)
      noexcept {
    assert(is_scalar_value(ch));
    if(ch <= 0xFFFF) {
      return 1;
    } else {
      return 2;
    }
  }

  [[nodiscard]] static constexpr Ch encode(char32_t ch, std::size_t byte) {
    assert(is_scalar_value(ch));
    assert(byte < num_code_units(ch));
    if(ch <= 0xFFFF) {
      return static_cast<Ch>(ch);
    } else {
      ch -= 0x10000;
      switch(byte) {
        case 0: return static_cast<Ch>(0xD800 | (ch >> 10));
        case 1: return static_cast<Ch>(0xDC00 | (ch & 0x3FF));
        default: util::unreachable();
      }
    }
  }

  constexpr bool operator==(utf16) const noexcept {
    return true;
  }
};

// Decoding ////////////////////////////////////////////////////////////////////

/** @brief A range adaptor that decodes the underlying range using the given
 * encoding.
 *
 * There are two distinct implementations: for underlying single-pass ranges and
 * for underlying multipass ranges. For single-pass ranges, the view object
 * itself caches the current iteration state and the current decoded element. To
 * do that, it needs to read ahead in the underlying range, and therefore cannot
 * work with unbounded views. Also, since iterators simply hold pointers to the
 * view object, the view cannot be a common view.
 *
 * For multipass ranges, the iterator stores the iteration state and decodes the
 * underlying code point whenever it is dereferenced. Therefore, it can produce
 * a common range if the underlying range is common and the encoding allows for
 * it. Also, if the error handler is eh_assume_valid, then a bidirectional range
 * can be adapted into a bidirectional range.
 *
 * There are ways to retrieve the current encoding state, which are different
 * for single-pass and multipass ranges. For a single-pass range, the get_state
 * member function on the view object itself produces the encoding state *after*
 * the current element (since elements are proactively retrieved and cached).
 * For a multipass range, the get_state member function on the iterator produces
 * the encoding state *at* the current element. */
template<std::ranges::view View, typename Encoding, typename EH>
requires decodable_encoding<Encoding, std::ranges::iterator_t<View>,
  std::ranges::sentinel_t<View>, EH>
class decoded_view:
    public std::ranges::view_interface<decoded_view<View, Encoding, EH>> {
private:
  View view_;
  [[no_unique_address]] Encoding encoding_;
  [[no_unique_address]] EH eh_;
  std::ranges::iterator_t<View> it_;
  typename Encoding::optional_decoded_type ch_;

  constexpr void increment_impl() noexcept(
    noexcept(it_ == std::ranges::end(view_))
    && noexcept(ch_ = Encoding::make_empty_optional())
    && noexcept(ch_ = Encoding::make_optional(encoding_.decode(it_,
      std::ranges::end(view_), eh_)))
  ) {
    if(it_ == std::ranges::end(view_)) {
      ch_ = Encoding::make_empty_optional();
    } else {
      ch_ = Encoding::make_optional(encoding_.decode(it_,
        std::ranges::end(view_), eh_));
    }
  }

  class iterator_core {
  private:
    decoded_view* range_;

    friend class decoded_view;
    explicit constexpr iterator_core(decoded_view& range) noexcept:
      range_{&range} {}

  public:
    constexpr iterator_core() = default;

    constexpr void swap(iterator_core& other) noexcept {
      std::swap(range_, other.range_);
    }

  protected:
    static constexpr bool is_single_pass{true};

    [[nodiscard]] constexpr typename Encoding::decoded_type dereference() const
        noexcept(
      noexcept(Encoding::get_optional_value(range_->ch_))
      && std::is_nothrow_move_constructible_v<typename Encoding::decoded_type>
    ) {
      auto const value{Encoding::get_optional_value(range_->ch_)};
      assert(value.has_value());
      return std::move(*value);
    }

    constexpr void increment() noexcept(noexcept(range_->increment_impl())) {
      range_->increment_impl();
    }

    [[nodiscard]] constexpr bool equal_to(std::default_sentinel_t) const
        noexcept(noexcept(Encoding::get_optional_value(range_->ch_))) {
      return !Encoding::get_optional_value(range_->ch_).has_value();
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  constexpr explicit decoded_view(
    View view,
    Encoding encoding,
    EH eh
  ) noexcept(
    std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_move_constructible_v<Encoding>
    && std::is_nothrow_move_constructible_v<EH>
    && std::is_nothrow_default_constructible_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_default_constructible_v<
      typename Encoding::optional_decoded_type>
  ):
    view_{std::move(view)},
    encoding_{std::move(encoding)},
    eh_{std::move(eh)},
    it_{},
    ch_{} {}

  constexpr decoded_view(decoded_view const&) = default;

  constexpr decoded_view(decoded_view&&) = default;

  constexpr decoded_view& operator=(decoded_view const& other) noexcept(
    std::is_nothrow_copy_assignable_v<View>
    && std::is_nothrow_copy_assignable_v<Encoding>
    && std::is_nothrow_copy_assignable_v<EH>
    && std::is_nothrow_copy_assignable_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_copy_assignable_v<
      typename Encoding::optional_decoded_type>
    || std::is_nothrow_copy_constructible_v<View>
    && std::is_nothrow_copy_constructible_v<Encoding>
    && std::is_nothrow_copy_constructible_v<EH>
    && std::is_nothrow_copy_constructible_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_copy_constructible_v<
      typename Encoding::optional_decoded_type>
    && std::is_nothrow_swappable_v<View>
    && std::is_nothrow_swappable_v<Encoding>
    && std::is_nothrow_swappable_v<EH>
    && std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_swappable_v<typename Encoding::optional_decoded_type>
  )
  requires std::copyable<View> && std::copyable<std::ranges::iterator_t<View>>
  {
    if constexpr(
      std::is_nothrow_copy_assignable_v<View>
      && std::is_nothrow_copy_assignable_v<Encoding>
      && std::is_nothrow_copy_assignable_v<EH>
      && std::is_nothrow_copy_assignable_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_copy_assignable_v<
        typename Encoding::optional_decoded_type>
    ) {
      view_ = other.view_;
      encoding_ = other.encoding_;
      eh_ = other.eh_;
      it_ = other.it_;
      ch_ = other.ch_;
    } else {
      decoded_view copy{other};
      this->swap(copy);
    }
    return *this;
  }

  constexpr decoded_view& operator=(decoded_view&& other) noexcept(
    std::is_nothrow_move_assignable_v<View>
    && std::is_nothrow_move_assignable_v<Encoding>
    && std::is_nothrow_move_assignable_v<EH>
    && std::is_nothrow_move_assignable_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_move_assignable_v<
      typename Encoding::optional_decoded_type>
    || std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_move_constructible_v<Encoding>
    && std::is_nothrow_move_constructible_v<EH>
    && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_move_constructible_v<
      typename Encoding::optional_decoded_type>
    && std::is_nothrow_swappable_v<View>
    && std::is_nothrow_swappable_v<Encoding>
    && std::is_nothrow_swappable_v<EH>
    && std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_swappable_v<typename Encoding::optional_decoded_type>
  ) {
    if constexpr(
      std::is_nothrow_move_assignable_v<View>
      && std::is_nothrow_move_assignable_v<Encoding>
      && std::is_nothrow_move_assignable_v<EH>
      && std::is_nothrow_move_assignable_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_move_assignable_v<
        typename Encoding::optional_decoded_type>
    ) {
      view_ = std::move(other.view_);
      encoding_ = std::move(other.encoding_);
      eh_ = std::move(other.eh_);
      it_ = std::move(other.it_);
      ch_ = std::move(other.ch_);
    } else {
      decoded_view copy{std::move(other)};
      this->swap(copy);
    }
    return *this;
  }

  constexpr void swap(decoded_view& other) noexcept(
    std::is_nothrow_swappable_v<View>
    && std::is_nothrow_swappable_v<Encoding>
    && std::is_nothrow_swappable_v<EH>
    && std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_swappable_v<typename Encoding::optional_decoded_type>
  ) {
    using std::swap;
    swap(view_, other.view_);
    swap(encoding_, other.encoding_);
    swap(eh_, other.eh_);
    swap(it_, other.it_);
    swap(ch_, other.ch_);
  }

  constexpr ~decoded_view() noexcept = default;

  [[nodiscard]] constexpr iterator begin() noexcept(
    noexcept(it_ = std::ranges::begin(view_))
    && noexcept(increment_impl())
  ) {
    it_ = std::ranges::begin(view_);
    increment_impl();
    return iterator{*this};
  }

  [[nodiscard]] static constexpr auto end() noexcept {
    return std::default_sentinel;
  }

  [[nodiscard]] constexpr Encoding const& get_state() const&
      noexcept(std::is_nothrow_copy_constructible_v<Encoding>) {
    return encoding_;
  }

  [[nodiscard]] constexpr Encoding get_state() &&
      noexcept(std::is_nothrow_copy_constructible_v<Encoding>) {
    return std::move(encoding_);
  }
};

template<std::ranges::view View, typename Encoding, typename EH>
requires
  decodable_encoding<Encoding, std::ranges::iterator_t<View>,
    std::ranges::sentinel_t<View>, EH>
  && std::ranges::forward_range<View>
class decoded_view<View, Encoding, EH>:
    public std::ranges::view_interface<decoded_view<View, Encoding, EH>> {
private:
  View view_;
  [[no_unique_address]] Encoding encoding_;
  [[no_unique_address]] EH eh_;

  class iterator_core;

  class sentinel {
  private:
    [[no_unique_address]] std::ranges::sentinel_t<View> iend_;

    friend class decoded_view;
    friend class iterator_core;
    constexpr explicit sentinel(std::ranges::sentinel_t<View> iend)
      noexcept(std::is_nothrow_move_constructible_v<std::ranges::sentinel_t<View>>):
      iend_{std::move(iend)} {}

  public:
    constexpr sentinel() = default;
  };

  class iterator_core {
  private:
    decoded_view* range_;
    std::ranges::iterator_t<View> it_;
    [[no_unique_address]] Encoding encoding_;

    friend class decoded_view;
    constexpr explicit iterator_core(
      decoded_view& range,
      std::ranges::iterator_t<View> it,
      Encoding encoding
    ) noexcept(
      std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_move_constructible_v<Encoding>
    ):
      range_{&range},
      it_{std::move(it)},
      encoding_{std::move(encoding)} {}

  public:
    constexpr iterator_core() = default;

    constexpr iterator_core(iterator_core const&) = default;

    constexpr iterator_core(iterator_core&&) = default;

    // NOLINTNEXTLINE(bugprone-unhandled-self-assignment)
    constexpr iterator_core& operator=(iterator_core const& other) noexcept(
      std::is_nothrow_copy_assignable_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_copy_assignable_v<Encoding>
      || std::is_nothrow_copy_constructible_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_copy_constructible_v<Encoding>
      && std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_swappable_v<Encoding>
    ) {
      if constexpr(
        std::is_nothrow_copy_assignable_v<std::ranges::iterator_t<View>>
        && std::is_nothrow_copy_assignable_v<Encoding>
      ) {
        range_ = other.range_;
        it_ = other.it_;
        encoding_ = other.encoding_;
      } else {
        iterator_core copy{other};
        this->swap(copy);
      }
      return *this;
    }

    constexpr iterator_core& operator=(iterator_core&& other) noexcept(
      std::is_nothrow_move_assignable_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_move_assignable_v<Encoding>
      || std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_move_constructible_v<Encoding>
      && std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_swappable_v<Encoding>
    ) {
      if constexpr(
        std::is_nothrow_move_assignable_v<std::ranges::iterator_t<View>>
        && std::is_nothrow_move_assignable_v<Encoding>
      ) {
        range_ = other.range_;
        it_ = std::move(other.it_);
        encoding_ = std::move(other.encoding_);
      } else {
        iterator_core copy{std::move(other)};
        this->swap(copy);
      }
      return *this;
    }

    constexpr ~iterator_core() noexcept = default;

    constexpr void swap(iterator_core& other) noexcept(
      std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_swappable_v<Encoding>
    ) {
      using std::swap;
      swap(range_, other.range_);
      swap(it_, other.it_);
      swap(encoding_, other.encoding_);
    }

    [[nodiscard]] constexpr Encoding const& get_state() const&
        noexcept(std::is_nothrow_copy_constructible_v<Encoding>) {
      return encoding_;
    }

    [[nodiscard]] constexpr Encoding get_state() &&
        noexcept(std::is_nothrow_copy_constructible_v<Encoding>) {
      return std::move(encoding_);
    }

  protected:
    [[nodiscard]] constexpr auto dereference() const noexcept(
      std::is_nothrow_copy_constructible_v<Encoding>
      && std::is_nothrow_copy_constructible_v<std::ranges::iterator_t<View>>
      && noexcept(std::declval<Encoding&>().decode(
        std::declval<std::ranges::iterator_t<View>&>(),
        std::ranges::end(range_->view_), range_->eh_))
    ) {
      auto encoding_copy{encoding_};
      auto it_copy{it_};
      return encoding_copy.decode(it_copy, std::ranges::end(range_->view_),
        range_->eh_);
    }

  private:
    [[nodiscard]] static consteval bool increment_noexcept() noexcept {
      if constexpr(std::same_as<EH,
          eh_assume_valid<std::ranges::range_value_t<View>>>) {
        return noexcept(encoding_.increment(it_));
      } else {
        return noexcept(encoding_.decode(it_, std::ranges::end(range_->view_),
          range_->eh_));
      }
    }

  protected:
    constexpr void increment() noexcept(increment_noexcept()) {
      if constexpr(std::same_as<EH,
          eh_assume_valid<std::ranges::range_value_t<View>>>) {
        encoding_.increment(it_);
      } else {
        encoding_.decode(it_, std::ranges::end(range_->view_), range_->eh_);
      }
    }

    [[nodiscard]] constexpr bool equal_to(iterator_core const& other) const noexcept(
      noexcept(it_ == other.it_) && noexcept(encoding_ == other.encoding_)
    ) {
      return it_ == other.it_ && encoding_ == other.encoding_;
    }

    [[nodiscard]] constexpr bool equal_to(sentinel const& iend)
        const noexcept(noexcept(it_ == iend.iend_)) {
      return it_ == iend.iend_;
    }

    constexpr void decrement()
    noexcept(noexcept(encoding_.decrement(it_)))
    requires decrementable_encoding<Encoding, std::ranges::iterator_t<View>>
      && std::same_as<EH, eh_assume_valid<typename Encoding::decoded_type>>
    {
      encoding_.decrement(it_);
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  constexpr explicit decoded_view(
    View view,
    Encoding encoding,
    EH eh = eh_replace<char32_t>{}
  ) noexcept(
    std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_move_constructible_v<Encoding>
    && std::is_nothrow_move_constructible_v<EH>
  ):
    view_{std::move(view)},
    encoding_{std::move(encoding)},
    eh_{std::move(eh)} {}

  constexpr decoded_view(decoded_view const&) = default;

  constexpr decoded_view(decoded_view&&) = default;

  constexpr decoded_view& operator=(decoded_view const& other) noexcept(
    std::is_nothrow_copy_assignable_v<View>
    && std::is_nothrow_copy_assignable_v<Encoding>
    && std::is_nothrow_copy_assignable_v<EH>
    || std::is_nothrow_copy_constructible_v<View>
    && std::is_nothrow_copy_constructible_v<Encoding>
    && std::is_nothrow_copy_constructible_v<EH>
    && std::is_nothrow_swappable_v<View>
    && std::is_nothrow_swappable_v<Encoding>
    && std::is_nothrow_swappable_v<EH>
  ) requires std::copyable<View> {
    if constexpr(
      std::is_nothrow_copy_assignable_v<View>
      && std::is_nothrow_copy_assignable_v<Encoding>
      && std::is_nothrow_copy_assignable_v<EH>
    ) {
      view_ = other.view_;
      encoding_ = other.encoding_;
      eh_ = other.eh_;
    } else {
      decoded_view copy{other};
      this->swap(copy);
    }
    return *this;
  }

  constexpr decoded_view& operator=(decoded_view&& other) noexcept(
    std::is_nothrow_move_assignable_v<View>
    && std::is_nothrow_move_assignable_v<Encoding>
    && std::is_nothrow_move_assignable_v<EH>
    || std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_move_constructible_v<Encoding>
    && std::is_nothrow_move_constructible_v<EH>
    && std::is_nothrow_swappable_v<View>
    && std::is_nothrow_swappable_v<Encoding>
    && std::is_nothrow_swappable_v<EH>
  ) {
    if constexpr(
      std::is_nothrow_move_assignable_v<View>
      && std::is_nothrow_move_assignable_v<Encoding>
      && std::is_nothrow_move_assignable_v<EH>
    ) {
      view_ = std::move(other.view_);
      encoding_ = std::move(other.encoding_);
      eh_ = std::move(other.eh_);
    } else {
      decoded_view copy{std::move(other)};
      this->swap(copy);
    }
    return *this;
  }

  constexpr ~decoded_view() noexcept = default;

  constexpr void swap(decoded_view& other) noexcept(
    std::is_nothrow_swappable_v<Encoding>
    && std::is_nothrow_swappable_v<EH>
  ) {
    using std::swap;
    swap(view_, other.view_);
    swap(encoding_, other.encoding_);
    swap(eh_, other.eh_);
  }

  [[nodiscard]] constexpr iterator begin() noexcept(
    noexcept(std::ranges::begin(view_))
    && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_move_constructible_v<Encoding>
  ) {
    return iterator{*this, std::ranges::begin(view_), encoding_};
  }

private:
  [[nodiscard]] static consteval bool end_noexcept() noexcept {
    if constexpr(std::ranges::common_range<View>
        && decodable_common_encoding<Encoding>) {
      return noexcept(std::ranges::end(view_)) && noexcept(encoding_.make_end())
        && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
        && std::is_nothrow_move_constructible_v<Encoding>;
    } else {
      return noexcept(std::ranges::end(view_))
        && std::is_nothrow_move_constructible_v<std::ranges::sentinel_t<View>>;
    }
  }

public:
  [[nodiscard]] constexpr auto end() noexcept(end_noexcept()) {
    if constexpr(std::ranges::common_range<View>
        && decodable_common_encoding<Encoding>) {
      return iterator{*this, std::ranges::end(view_), encoding_.make_end()};
    } else {
      return sentinel{std::ranges::end(view_)};
    }
  }
};

template<typename R, typename Encoding, typename EH>
decoded_view(R&&, Encoding&&, EH&&) -> decoded_view<
  std::ranges::views::all_t<R>,
  std::remove_cvref_t<Encoding>,
  std::remove_cvref_t<EH>
>;

template<std::ranges::view View, typename Encoding, typename EH>
requires decodable_encoding<Encoding, std::ranges::iterator_t<View>,
  std::ranges::sentinel_t<View>, EH>
constexpr void swap(
  typename decoded_view<View, Encoding, EH>::iterator_core& a,
  typename decoded_view<View, Encoding, EH>::iterator_core& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

template<std::ranges::view View, typename Encoding, typename EH>
requires decodable_encoding<Encoding, std::ranges::iterator_t<View>,
  std::ranges::sentinel_t<View>, EH>
constexpr void swap(
  decoded_view<View, Encoding, EH>& a,
  decoded_view<View, Encoding, EH>& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

template<
  std::ranges::viewable_range Range,
  typename Encoding,
  typename EH = eh_replace<typename Encoding::eh_output_type>
>
requires decodable_encoding<
  Encoding,
  std::ranges::iterator_t<std::views::all_t<Range>>,
  std::ranges::sentinel_t<std::views::all_t<Range>>,
  EH
>
[[nodiscard]] constexpr auto decode(
  Range&& range,
  Encoding encoding,
  EH eh = eh_replace<typename Encoding::eh_output_type>{}
) noexcept(
  std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
  && std::is_nothrow_move_constructible_v<Encoding>
  && std::is_nothrow_move_constructible_v<EH>
  && (
    std::ranges::forward_range<std::views::all_t<Range>>
    || std::is_nothrow_default_constructible_v<std::ranges::iterator_t<
      std::views::all_t<Range>>>
  )
) {
  return decoded_view<std::views::all_t<Range>, Encoding, EH>{
    std::views::all(std::forward<Range>(range)),
    std::move(encoding),
    std::move(eh)
  };
}

template<
  std::ranges::viewable_range Range,
  typename EH = eh_replace<char32_t>
>
requires
  utf8_code_unit<std::ranges::range_value_t<std::views::all_t<Range>>>
  && decodable_encoding<
    utf8<std::ranges::range_value_t<std::views::all_t<Range>>>,
    std::ranges::iterator_t<std::views::all_t<Range>>,
    std::ranges::sentinel_t<std::views::all_t<Range>>,
    EH
  >
[[nodiscard]] constexpr auto utf8_decode(
  Range&& range,
  EH eh = eh_replace<char32_t>{}
) noexcept(
  std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
  && std::is_nothrow_move_constructible_v<EH>
  && (
    std::ranges::forward_range<std::views::all_t<Range>>
    || std::is_nothrow_default_constructible_v<std::ranges::iterator_t<
      std::views::all_t<Range>>>
  )
) {
  return decode(
    std::forward<Range>(range),
    utf8<std::ranges::range_value_t<std::views::all_t<Range>>>{},
    std::move(eh)
  );
}

template<
  std::ranges::viewable_range Range,
  typename EH = eh_replace<char32_t>
>
requires
  utf16_code_unit<std::ranges::range_value_t<std::views::all_t<Range>>>
  && decodable_encoding<
    utf16<std::ranges::range_value_t<std::views::all_t<Range>>>,
    std::ranges::iterator_t<std::views::all_t<Range>>,
    std::ranges::sentinel_t<std::views::all_t<Range>>,
    EH
  >
[[nodiscard]] constexpr auto utf16_decode(
  Range&& range,
  EH eh = eh_replace<char32_t>{}
) noexcept(
  std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
  && std::is_nothrow_move_constructible_v<EH>
  && (
    std::ranges::forward_range<std::views::all_t<Range>>
    || std::is_nothrow_default_constructible_v<std::ranges::iterator_t<
      std::views::all_t<Range>>>
  )
) {
  return decode(
    std::forward<Range>(range),
    utf16<std::ranges::range_value_t<std::views::all_t<Range>>>{},
    std::move(eh)
  );
}

namespace detail_ {

template<encoding Encoding, std::movable EH>
class decode_adaptor:
    public util::range_adaptor_closure<decode_adaptor<Encoding, EH>> {
private:
  [[no_unique_address]] Encoding encoding_;
  [[no_unique_address]] EH eh_;

public:
  constexpr explicit decode_adaptor(
    Encoding encoding,
    EH eh
  ) noexcept(
    std::is_nothrow_move_constructible_v<Encoding>
    && std::is_nothrow_move_constructible_v<EH>
  ): encoding_{std::move(encoding)}, eh_{std::move(eh)} {}

  template<std::ranges::viewable_range Range>
  requires decodable_encoding<
    Encoding,
    std::ranges::iterator_t<std::views::all_t<Range>>,
    std::ranges::sentinel_t<std::views::all_t<Range>>,
    EH
  >
  [[nodiscard]] constexpr auto operator()(Range&& range) noexcept(
    std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
    && std::is_nothrow_move_constructible_v<Encoding>
    && std::is_nothrow_move_constructible_v<EH>
    && (
      std::ranges::forward_range<std::views::all_t<Range>>
      || std::is_nothrow_default_constructible_v<std::ranges::iterator_t<
        std::views::all_t<Range>>>
    )
  ) {
    return decode(
      std::forward<Range>(range),
      std::move(encoding_),
      std::move(eh_)
    );
  }
};

} // detail_

template<
  encoding Encoding,
  std::movable EH = eh_replace<typename Encoding::eh_output_type>
>
[[nodiscard]] constexpr auto decode(
  Encoding encoding,
  EH eh = eh_replace<typename Encoding::eh_output_type>{}
) noexcept(
  std::is_nothrow_move_constructible_v<Encoding>
  && std::is_nothrow_move_constructible_v<EH>
) {
  return detail_::decode_adaptor<Encoding, EH>{
    std::move(encoding), std::move(eh)};
}

namespace detail_ {

template<std::movable EH>
class utf8_decode_adaptor:
    public util::range_adaptor_closure<utf8_decode_adaptor<EH>> {
private:
  [[no_unique_address]] EH eh_;

public:
  constexpr explicit utf8_decode_adaptor(EH eh)
    noexcept(std::is_nothrow_move_constructible_v<EH>): eh_{std::move(eh)} {}

  template<std::ranges::viewable_range Range>
  requires
    utf8_code_unit<std::ranges::range_value_t<std::views::all_t<Range>>>
    && decodable_encoding<
      utf8<std::ranges::range_value_t<std::views::all_t<Range>>>,
      std::ranges::iterator_t<std::views::all_t<Range>>,
      std::ranges::sentinel_t<std::views::all_t<Range>>,
      EH
    >
  [[nodiscard]] constexpr auto operator()(Range&& range) noexcept(
    std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
    && std::is_nothrow_move_constructible_v<EH>
    && (
      std::ranges::forward_range<std::views::all_t<Range>>
      || std::is_nothrow_default_constructible_v<std::ranges::iterator_t<
        std::views::all_t<Range>>>
    )
  ) {
    return utf8_decode(std::forward<Range>(range), std::move(eh_));
  }
};

} // detail_

template<std::movable EH = eh_replace<char32_t>>
[[nodiscard]] constexpr auto utf8_decode(EH eh = eh_replace<char32_t>{})
    noexcept(std::is_nothrow_move_constructible_v<EH>) {
  return detail_::utf8_decode_adaptor<EH>{std::move(eh)};
}

namespace detail_ {

template<std::movable EH>
class utf16_decode_adaptor:
    public util::range_adaptor_closure<utf16_decode_adaptor<EH>> {
private:
  [[no_unique_address]] EH eh_;

public:
  constexpr explicit utf16_decode_adaptor(EH eh)
    noexcept(std::is_nothrow_move_constructible_v<EH>): eh_{std::move(eh)} {}

  template<std::ranges::viewable_range Range>
  requires
    utf16_code_unit<std::ranges::range_value_t<std::views::all_t<Range>>>
    && decodable_encoding<
      utf16<std::ranges::range_value_t<std::views::all_t<Range>>>,
      std::ranges::iterator_t<std::views::all_t<Range>>,
      std::ranges::sentinel_t<std::views::all_t<Range>>,
      EH
    >
  [[nodiscard]] constexpr auto operator()(Range&& range) noexcept(
    std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
    && std::is_nothrow_move_constructible_v<EH>
    && (
      std::ranges::forward_range<std::views::all_t<Range>>
      || std::is_nothrow_default_constructible_v<std::ranges::iterator_t<
        std::views::all_t<Range>>>
    )
  ) {
    return utf16_decode(std::forward<Range>(range), std::move(eh_));
  }
};

} // detail_

template<std::movable EH = eh_replace<char32_t>>
[[nodiscard]] constexpr auto utf16_decode(EH eh = eh_replace<char32_t>{})
    noexcept(std::is_nothrow_move_constructible_v<EH>) {
  return detail_::utf16_decode_adaptor<EH>{std::move(eh)};
}

// Validation //////////////////////////////////////////////////////////////////

namespace detail_ {

template<encoding_data Output, encoding_data Error>
struct eh_validation {
  bool* found_error_;

  constexpr Output operator()(Error) const
      noexcept(std::is_nothrow_default_constructible_v<Output>) {
    *found_error_ = true;
    return Output{};
  }
};

} // detail_

template<typename Encoding, std::ranges::input_range Range>
requires decodable_encoding<
  Encoding,
  std::ranges::iterator_t<Range>,
  std::ranges::sentinel_t<Range>,
  detail_::eh_validation<typename Encoding::eh_output_type,
    typename Encoding::error_type>
>
[[nodiscard]] constexpr bool is_valid(Range&& range, Encoding encoding) {
  bool found_error{false};
  auto it{std::ranges::begin(range)};
  auto const iend{std::ranges::end(range)};
  while(it != iend) {
    encoding.decode(
      it,
      iend,
      detail_::eh_validation<
        typename Encoding::eh_output_type,
        typename Encoding::error_type
      >{&found_error}
    );
  }
  return !found_error;
}

template<std::ranges::input_range Range>
requires utf8_code_unit<std::ranges::range_value_t<Range>>
[[nodiscard]] constexpr bool is_valid_utf8(Range&& range) {
  return unicode::is_valid(std::forward<Range>(range),
    utf8<std::ranges::range_value_t<Range>>{});
}

template<std::ranges::input_range Range>
requires utf16_code_unit<std::ranges::range_value_t<Range>>
[[nodiscard]] constexpr bool is_valid_utf16(Range&& range) {
  return unicode::is_valid(std::forward<Range>(range),
    utf16<std::ranges::range_value_t<Range>>{});
}

// Encoding ////////////////////////////////////////////////////////////////////

/** @brief A range adaptor that encodes the underlying range using the given
 * encoding. */
template<std::ranges::view View, encodable_encoding Encoding>
requires std::same_as<std::ranges::range_value_t<View>,
  typename Encoding::decoded_type>
class encoded_view:
    public std::ranges::view_interface<encoded_view<View, Encoding>> {
private:
  View view_;
  [[no_unique_address]] Encoding encoding_;

  class iterator_core;

  class sentinel {
  private:
    [[no_unique_address]] std::ranges::sentinel_t<View> iend_;

    friend class encoded_view;
    friend class iterator_core;
    constexpr explicit sentinel(std::ranges::sentinel_t<View> iend)
      noexcept(std::is_nothrow_move_constructible_v<std::ranges::sentinel_t<View>>):
      iend_{std::move(iend)} {}

  public:
    constexpr sentinel() = default;
  };

  class iterator_core {
  private:
    [[no_unique_address]] Encoding encoding_;
    std::ranges::iterator_t<View> it_;
    std::size_t byte_;

    friend class encoded_view;
    constexpr explicit iterator_core(
      Encoding encoding,
      std::ranges::iterator_t<View> it
    ) noexcept(
      std::is_nothrow_move_constructible_v<Encoding>
      && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
    ): encoding_{std::move(encoding)}, it_{std::move(it)}, byte_{0} {}

  public:
    constexpr iterator_core() = default;

    constexpr iterator_core(const iterator_core&) = default;

    constexpr iterator_core(iterator_core&&) = default;

    constexpr iterator_core& operator=(iterator_core const& other) noexcept(
      std::is_nothrow_copy_assignable_v<Encoding>
      && std::is_nothrow_copy_assignable_v<std::ranges::iterator_t<View>>
      || std::is_nothrow_copy_constructible_v<Encoding>
      && std::is_nothrow_copy_constructible_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_swappable_v<Encoding>
      && std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
    ) {
      if constexpr(
        std::is_nothrow_copy_assignable_v<Encoding>
        && std::is_nothrow_copy_assignable_v<std::ranges::iterator_t<View>>
      ) {
        encoding_ = other.encoding_;
        it_ = other.it_;
        byte_ = other.byte_;
      } else {
        iterator_core copy{other};
        this->swap(copy);
      }
      return *this;
    }

    constexpr iterator_core& operator=(iterator_core&& other) noexcept(
      std::is_nothrow_move_assignable_v<Encoding>
      && std::is_nothrow_move_assignable_v<std::ranges::iterator_t<View>>
      || std::is_nothrow_move_constructible_v<Encoding>
      && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
      && std::is_nothrow_swappable_v<Encoding>
      && std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
    ) {
      if constexpr(
        std::is_nothrow_move_assignable_v<Encoding>
        && std::is_nothrow_move_assignable_v<std::ranges::iterator_t<View>>
      ) {
        encoding_ = std::move(other.encoding_);
        it_ = std::move(other.it_);
        byte_ = other.byte_;
      } else {
        iterator_core copy{std::move(other)};
        this->swap(copy);
      }
      return *this;
    }

    constexpr ~iterator_core() noexcept = default;

    constexpr void swap(iterator_core& other) noexcept(
      std::is_nothrow_swappable_v<Encoding>
      && std::is_nothrow_swappable_v<std::ranges::iterator_t<View>>
    ) {
      using std::swap;
      swap(encoding_, other.encoding_);
      swap(it_, other.it_);
      swap(byte_, other.byte_);
    }

  protected:
    static constexpr bool is_single_pass{!std::ranges::forward_range<View>};

    [[nodiscard]] constexpr typename Encoding::encoded_type dereference() const
        noexcept(noexcept(encoding_.encode(*it_, byte_))) {
      return encoding_.encode(*it_, byte_);
    }

    constexpr void increment()
        noexcept(noexcept(encoding_.num_code_units(*it_)) && noexcept(++it_)) {
      if(++byte_ == encoding_.num_code_units(*it_)) {
        ++it_;
        byte_ = 0;
      }
    }

    [[nodiscard]] constexpr bool equal_to(iterator_core const& other) const
        noexcept(noexcept(it_ == other.it_)) {
      return it_ == other.it_ && byte_ == other.byte_;
    }

    [[nodiscard]] constexpr bool equal_to(sentinel const& iend)
        const noexcept(noexcept(it_ == iend.iend_)) {
      return it_ == iend.iend_;
    }

    constexpr void decrement()
        noexcept(noexcept(--it_) && noexcept(encoding_.num_code_units(*it_)))
        requires std::ranges::bidirectional_range<View> {
      if(byte_ != 0) {
        --byte_;
      } else {
        --it_;
        std::size_t const size{encoding_.num_code_units(*it_)};
        assert(size != 0);
        byte_ = size - 1;
      }
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  constexpr explicit encoded_view(View view, Encoding encoding) noexcept(
    std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_move_constructible_v<Encoding>
  ): view_{std::move(view)}, encoding_{std::move(encoding)} {}

  constexpr encoded_view(encoded_view const&) = default;

  constexpr encoded_view(encoded_view&&) = default;

  constexpr encoded_view& operator=(encoded_view const& other) noexcept(
    std::is_nothrow_copy_assignable_v<View>
    && std::is_nothrow_copy_assignable_v<Encoding>
    || std::is_nothrow_copy_constructible_v<View>
    && std::is_nothrow_copy_constructible_v<Encoding>
    && std::is_nothrow_swappable_v<View>
    && std::is_nothrow_swappable_v<Encoding>
  ) requires std::copyable<View> {
    if constexpr(
      std::is_nothrow_copy_assignable_v<View>
      && std::is_nothrow_copy_assignable_v<Encoding>
    ) {
      view_ = other.view_;
      encoding_ = other.encoding_;
    } else {
      encoded_view copy{other};
      this->swap(copy);
    }
    return *this;
  }

  constexpr encoded_view& operator=(encoded_view&& other) noexcept(
    std::is_nothrow_move_assignable_v<View>
    && std::is_nothrow_move_assignable_v<Encoding>
    || std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_move_constructible_v<Encoding>
    && std::is_nothrow_swappable_v<View>
    && std::is_nothrow_swappable_v<Encoding>
  ) {
    if constexpr(
      std::is_nothrow_move_assignable_v<View>
      && std::is_nothrow_move_assignable_v<Encoding>
    ) {
      view_ = std::move(other.view_);
      encoding_ = std::move(other.encoding_);
    } else {
      encoded_view copy{std::move(other)};
      this->swap(copy);
    }
    return *this;
  }

  constexpr ~encoded_view() noexcept = default;

  constexpr void swap(encoded_view& other) noexcept(
    std::is_nothrow_swappable_v<View>
    && std::is_nothrow_swappable_v<Encoding>
  ) {
    using std::swap;
    swap(view_, other.view_);
    swap(encoding_, other.encoding_);
  }

  [[nodiscard]] constexpr iterator begin() noexcept(
    noexcept(std::ranges::begin(view_))
    && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
    && std::is_nothrow_move_constructible_v<Encoding>
  ) {
    return iterator{encoding_, std::ranges::begin(view_)};
  }

private:
  [[nodiscard]] static consteval bool end_noexcept() noexcept {
    if constexpr(std::ranges::common_range<View>) {
      return noexcept(std::ranges::end(view_))
        && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<View>>
        && std::is_nothrow_move_constructible_v<Encoding>;
    } else {
      return noexcept(std::ranges::end(view_))
        && std::is_nothrow_move_constructible_v<std::ranges::sentinel_t<View>>;
    }
  }

public:
  [[nodiscard]] constexpr auto end() noexcept(end_noexcept()) {
    if constexpr(std::ranges::common_range<View>) {
      return iterator{encoding_, std::ranges::end(view_)};
    } else {
      return sentinel{std::ranges::end(view_)};
    }
  }
};

template<typename R, typename Encoding, typename EH>
encoded_view(R&&, Encoding&&, EH&&) -> encoded_view<
  std::ranges::views::all_t<R>,
  std::remove_cvref_t<Encoding>
>;

template<std::ranges::view View, encodable_encoding Encoding>
requires std::same_as<std::ranges::range_value_t<View>,
  typename Encoding::decoded_type>
constexpr void swap(
  typename encoded_view<View, Encoding>::iterator_core& a,
  typename encoded_view<View, Encoding>::iterator_core& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

template<std::ranges::view View, encodable_encoding Encoding>
requires std::same_as<std::ranges::range_value_t<View>,
  typename Encoding::decoded_type>
constexpr void swap(
  encoded_view<View, Encoding>& a,
  encoded_view<View, Encoding>& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

template<std::ranges::viewable_range Range, encodable_encoding Encoding>
requires std::same_as<std::ranges::range_value_t<Range>,
  typename Encoding::decoded_type>
[[nodiscard]] constexpr auto encode(Range&& range, Encoding encoding) noexcept(
  std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
  && std::is_nothrow_move_constructible_v<Encoding>
) {
  return encoded_view<std::ranges::views::all_t<Range>, Encoding>{
    std::views::all(std::forward<Range>(range)),
    std::move(encoding)
  };
}

template<utf8_code_unit Ch = char8_t, std::ranges::viewable_range Range>
requires std::same_as<std::ranges::range_value_t<Range>, char32_t>
[[nodiscard]] constexpr auto utf8_encode(Range&& range) noexcept(
  std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
) {
  return encode(std::forward<Range>(range), utf8<Ch>{});
}

template<utf16_code_unit Ch = char16_t, std::ranges::viewable_range Range>
requires std::same_as<std::ranges::range_value_t<Range>, char32_t>
[[nodiscard]] constexpr auto utf16_encode(Range&& range) noexcept(
  std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
) {
  return encode(std::forward<Range>(range), utf16<Ch>{});
}

namespace detail_ {

template<encodable_encoding Encoding>
class encode_adaptor:
    public util::range_adaptor_closure<encode_adaptor<Encoding>> {
private:
  [[no_unique_address]] Encoding encoding_;

public:
  constexpr explicit encode_adaptor(Encoding encoding)
    noexcept(std::is_nothrow_move_constructible_v<Encoding>):
    encoding_{std::move(encoding)} {}

  template<std::ranges::viewable_range Range>
  requires std::same_as<std::ranges::range_value_t<Range>,
    typename Encoding::decoded_type>
  [[nodiscard]] constexpr auto operator()(Range&& range) noexcept(
    std::is_nothrow_move_constructible_v<std::views::all_t<Range>>
    && std::is_nothrow_move_constructible_v<Encoding>
  ) {
    return encode(std::forward<Range>(range), encoding_);
  }
};

} // detail_

template<encodable_encoding Encoding>
[[nodiscard]] constexpr auto encode(Encoding encoding)
    noexcept(std::is_nothrow_move_constructible_v<Encoding>) {
  return detail_::encode_adaptor<Encoding>{std::move(encoding)};
}

template<utf8_code_unit Ch = char8_t>
[[nodiscard]] constexpr auto utf8_encode() noexcept {
  return encode(utf8<Ch>{});
}

template<utf16_code_unit Ch = char16_t>
[[nodiscard]] constexpr auto utf16_encode() noexcept {
  return encode(utf16<Ch>{});
}

} // unicode

#endif
