#include "unicode/props.hpp"
#include "unicode/basic.hpp"
#include "unicode/utrie.hpp"
#include <cassert>
#include <cstdint>

#include "unicode/ucd/ccc.hpp"
#include "unicode/ucd/decimal_value.hpp"
#include "unicode/ucd/default_ignorable.hpp"
#include "unicode/ucd/gc.hpp"
#include "unicode/ucd/indic_syllabic_category.hpp"
#include "unicode/ucd/joining_type.hpp"
#include "unicode/ucd/scripts.hpp"
#include "unicode/ucd/scripts_values.hpp"
#include "unicode/ucd/xid_continue.hpp"
#include "unicode/ucd/xid_start.hpp"

unicode::gc_t unicode::gc(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return gc_t{UTRIE(gc)::get(codepoint).unwrap()};
}

bool unicode::is_gc_group(unicode::gc_t gc_value, unicode::gc_group_t group) noexcept {
  switch(gc_value) {
    case gc_t::uppercase_letter:
    case gc_t::lowercase_letter:
    case gc_t::titlecase_letter:
      if(group == gc_group_t::letter_cased) {
        return true;
      }
    [[fallthrough]];
    case gc_t::modifier_letter:
    case gc_t::other_letter:
      return group == gc_group_t::letter;

    case gc_t::nonspacing_mark:
    case gc_t::spacing_mark:
    case gc_t::enclosing_mark:
      return group == gc_group_t::mark;

    case gc_t::decimal_number:
    case gc_t::letter_number:
    case gc_t::other_number:
      return group == gc_group_t::number;

    case gc_t::connector_punctuation:
    case gc_t::dash_punctuation:
    case gc_t::open_punctuation:
    case gc_t::close_punctuation:
    case gc_t::initial_punctuation:
    case gc_t::final_punctuation:
    case gc_t::other_punctuation:
      return group == gc_group_t::punctuation;

    case gc_t::math_symbol:
    case gc_t::currency_symbol:
    case gc_t::modifier_symbol:
    case gc_t::other_symbol:
      return group == gc_group_t::symbol;

    case gc_t::space_separator:
    case gc_t::line_separator:
    case gc_t::paragraph_separator:
      return group == gc_group_t::separator;

    case gc_t::control:
    case gc_t::format:
    case gc_t::surrogate:
    case gc_t::private_use:
    case gc_t::unassigned:
      return group == gc_group_t::other;
  }
}

unicode::ccc_t unicode::ccc(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return UTRIE(ccc)::get(codepoint);
}

util::uint8_t unicode::decimal_value(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  util::uint8_t const result{UTRIE(decimal_value)::get(codepoint)};
  assert(result <= 9 || result == 15);
  return result == 15 ? no_decimal_value : result;
}

bool unicode::xid_start(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return UTRIE(xid_start)::get(codepoint).unwrap();
}

bool unicode::xid_continue(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return UTRIE(xid_continue)::get(codepoint).unwrap();
}

bool unicode::white_space(char32_t ch) noexcept {
  assert(is_scalar_value(ch));

  // Since there are so few White_Space code points, we simply hardcode them
  // here instead of using a Unicode trie. This is the list of such code points
  // as of Unicode 15.0.0.
  return
    U'\u0009' <= ch && ch <= U'\u000D'
    || ch == U'\u0020'
    || ch == U'\u0085'
    || ch == U'\u00A0'
    || ch == U'\u1680'
    || U'\u2000' <= ch && ch <= U'\u200A'
    || ch == U'\u2028'
    || ch == U'\u2029'
    || ch == U'\u202F'
    || ch == U'\u205F'
    || ch == U'\u3000';
}

bool unicode::default_ignorable(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return UTRIE(default_ignorable)::get(codepoint).unwrap();
}

unicode::joining_type_t unicode::joining_type(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return joining_type_t{UTRIE(joining_type)::get(codepoint).unwrap()};
}

unicode::indic_syllabic_category_t
unicode::indic_syllabic_category(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return indic_syllabic_category_t{UTRIE(indic_syllabic_category)::get(codepoint).unwrap()};
}

unicode::hangul_syllable_type_t
unicode::hangul_syllable_type(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  auto const codepoint_int{util::wrap(static_cast<std::uint32_t>(codepoint))};

  // Since the Unicode standard prescribes arithmetic calculations for algorithmic generation
  // of canonical decompositions of precomposed hangul syllables, we're also algorithmically
  // generating the value of this property, as well. See HangulSyllableType.txt from the UCD:
  // https://www.unicode.org/Public/15.0.0/ucd/HangulSyllableType.txt
  if(0x1100 <= codepoint_int && codepoint_int <= 0x115F) {
    return hangul_syllable_type_t::leading_jamo;
  }
  if(0xA960 <= codepoint_int && codepoint_int <= 0xA97C) {
    return hangul_syllable_type_t::leading_jamo;
  }

  if(0x1160 <= codepoint_int && codepoint_int <= 0x11A7) {
    return hangul_syllable_type_t::vowel_jamo;
  }
  if(0xD7B0 <= codepoint_int && codepoint_int <= 0xD7C6) {
    return hangul_syllable_type_t::vowel_jamo;
  }

  if(0x11A8 <= codepoint_int && codepoint_int <= 0x11FF) {
    return hangul_syllable_type_t::trailing_jamo;
  }
  if(0xD7CB <= codepoint_int && codepoint_int <= 0xD7FB) {
    return hangul_syllable_type_t::trailing_jamo;
  }

  if(
    detail_::hangul_s_base <= codepoint_int
    && codepoint_int < detail_::hangul_s_base + detail_::hangul_s_count
  ) {
    if((codepoint_int - detail_::hangul_s_base) % (detail_::hangul_t_count + 1) == 0) {
      return hangul_syllable_type_t::lv_syllable;
    } else {
      return hangul_syllable_type_t::lvt_syllable;
    }
  }

  return hangul_syllable_type_t::not_applicable;
}

unicode::script_t unicode::script(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return script_t{UTRIE(scripts)::get(codepoint).unwrap()};
}
