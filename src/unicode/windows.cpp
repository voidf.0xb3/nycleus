#include "unicode/windows.hpp"

#ifdef _WIN32

#include <windows.h>
#include <system_error>

void unicode::init_unicode_output() {
  if(!SetConsoleOutputCP(CP_UTF8)) {
    throw std::system_error{static_cast<int>(GetLastError()),
      std::system_category()};
  }
}

#endif
