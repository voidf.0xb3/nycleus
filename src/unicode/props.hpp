#ifndef UNICODE_PROPS_HPP_INCLUDED_BLXXSGQOH9BLDV88CRJV8Z9H4
#define UNICODE_PROPS_HPP_INCLUDED_BLXXSGQOH9BLDV88CRJV8Z9H4

#include "unicode/basic.hpp"
#include <gsl/gsl>
#include <cassert>
#include <cstdint>

#include "unicode/ucd/scripts_values.hpp"

namespace unicode {

enum class gc_t: std::uint8_t {
  lu = 1, uppercase_letter = lu,
  ll = 2, lowercase_letter = ll,
  lt = 3, titlecase_letter = lt,
  lm = 4, modifier_letter = lm,
  lo = 5, other_letter = lo,

  mn = 6, nonspacing_mark = mn,
  mc = 7, spacing_mark = mc,
  me = 8, enclosing_mark = me,

  nd = 9, decimal_number = nd,
  nl = 10, letter_number = nl,
  no = 11, other_number = no,

  pc = 12, connector_punctuation = pc,
  pd = 13, dash_punctuation = pd,
  ps = 14, open_punctuation = ps,
  pe = 15, close_punctuation = pe,
  pi = 16, initial_punctuation = pi,
  pf = 17, final_punctuation = pf,
  po = 18, other_punctuation = po,

  sm = 19, math_symbol = sm,
  sc = 20, currency_symbol = sc,
  sk = 21, modifier_symbol = sk,
  so = 22, other_symbol = so,

  zs = 23, space_separator = zs,
  zl = 24, line_separator = zl,
  zp = 25, paragraph_separator = zp,

  cc = 26, control = cc,
  cf = 27, format = cf,
  cs = 28, surrogate = cs,
  co = 29, private_use = co,
  cn = 0, unassigned = cn
};

/** @brief Reads the value of the General_Category property. */
gc_t gc(char32_t) noexcept;

enum class gc_group_t {
  lc, letter_cased = lc,
  l, letter = l,
  m, mark = m,
  n, number = n,
  p, punctuation = p,
  s, symbol = s,
  z, separator = z,
  c, other = c
};

/** @brief Determines whether the given value of the General_Category property falls within the
 * specified group. */
[[nodiscard]] bool is_gc_group(gc_t, gc_group_t) noexcept;

/** @brief Determines whether the code point has a General_Category value within the specified
 * group. */
[[nodiscard]] inline bool is_gc_group(char32_t ch, gc_group_t group) noexcept {
  assert(is_scalar_value(ch));
  return is_gc_group(gc(ch), group);
}

using ccc_t = util::uint8_t;
constexpr ccc_t ccc_not_reordered{0};
constexpr ccc_t ccc_overlay{1};
constexpr ccc_t ccc_nukta{7};
constexpr ccc_t ccc_kana_voicing{8};
constexpr ccc_t ccc_virama{9};
constexpr ccc_t ccc_attached_below_left{200};
constexpr ccc_t ccc_attached_below{202};
constexpr ccc_t ccc_attached_above{214};
constexpr ccc_t ccc_attached_above_right{216};
constexpr ccc_t ccc_below_left{218};
constexpr ccc_t ccc_below{220};
constexpr ccc_t ccc_below_right{222};
constexpr ccc_t ccc_left{224};
constexpr ccc_t ccc_right{226};
constexpr ccc_t ccc_above_left{228};
constexpr ccc_t ccc_above{230};
constexpr ccc_t ccc_above_right{232};
constexpr ccc_t ccc_double_below{233};
constexpr ccc_t ccc_double_above{234};
constexpr ccc_t ccc_iota_subscript{240};

/** @brief Reads the value of the Canonical_Combining_Category property. */
[[nodiscard]] ccc_t ccc(char32_t) noexcept;

/** @brief If Numeric_Type=Decimal, reads the value of the Numeric_Value property; otherwise,
 * returns no_decimal_value. */
[[nodiscard]] util::uint8_t decimal_value(char32_t) noexcept;

inline constexpr util::uint8_t no_decimal_value{255};

/** @brief Reads the value of the White_Space property. */
[[nodiscard]] bool white_space(char32_t) noexcept;

/** @brief Reads the value of the Default_Ignorable_Code_Point property. */
[[nodiscard]] bool default_ignorable(char32_t) noexcept;

/** @brief Reads the value of the XID_Start property. */
[[nodiscard]] bool xid_start(char32_t) noexcept;

/** @brief Reads the value of the XID_Continue property. */
[[nodiscard]] bool xid_continue(char32_t) noexcept;

enum class joining_type_t: std::uint8_t {
  non_joining = 0,
  right_joining = 1,
  left_joining = 2,
  dual_joining = 3,
  join_causing = 4,
  transparent = 5
};

/** @brief Reads the value of the Joining_Type property. */
[[nodiscard]] joining_type_t joining_type(char32_t) noexcept;

enum class indic_syllabic_category_t: std::uint8_t {
  other = 0,
  bindu = 1,
  visarga = 2,
  avagraha = 3,
  nukta = 4,
  virama = 5,
  pure_killer = 6,
  invisible_stacker = 7,
  vowel_independent = 8,
  vowel_dependent = 9,
  vowel = 10,
  consonant_placeholder = 11,
  consonant = 12,
  consonant_dead = 13,
  consonant_with_stacker = 14,
  consonant_prefixed = 15,
  consonant_preceding_repha = 16,
  consonant_initial_postfixed = 17,
  consonant_succeeding_repha = 18,
  consonant_subjoined = 19,
  consonant_medial = 20,
  consonant_final = 21,
  consonant_head_letter = 22,
  modifying_letter = 23,
  tone_letter = 24,
  tone_mark = 25,
  gemination_mark = 26,
  cantillation_mark = 27,
  register_shifter = 28,
  syllable_modifier = 29,
  consonant_killer = 30,
  non_joiner = 31,
  joiner = 32,
  number_joiner = 33,
  number = 34,
  brahmi_joining_number = 35
};

/** @brief Reads the value of the Indic_Syllabic_Category property. */
[[nodiscard]] indic_syllabic_category_t indic_syllabic_category(char32_t)
    noexcept;

namespace detail_ {

constexpr util::uint32_t hangul_l_count{19};
constexpr util::uint32_t hangul_v_count{21};
constexpr util::uint32_t hangul_t_count{27};
constexpr util::uint32_t hangul_n_count{hangul_v_count * (hangul_t_count + 1)};
constexpr util::uint32_t hangul_s_count{hangul_l_count * hangul_n_count};

constexpr util::uint32_t hangul_l_base{0x1100};
constexpr util::uint32_t hangul_v_base{0x1161};
constexpr util::uint32_t hangul_t_base{0x11A8};
constexpr util::uint32_t hangul_s_base{0xAC00};

}

enum class hangul_syllable_type_t {
  leading_jamo, l = leading_jamo,
  vowel_jamo, v = vowel_jamo,
  trailing_jamo, t = trailing_jamo,
  lv_syllable, lv = lv_syllable,
  lvt_syllable, lvt = lvt_syllable,
  not_applicable, na = not_applicable
};

/** @brief Reads the value of the Hangul_Syllabic_Type property. */
[[nodiscard]] hangul_syllable_type_t hangul_syllable_type(char32_t) noexcept;

/** @brief An enumeration of all values of the Script property.
 *
 * Since more scripts are usually added with each Unicode version, this list is automatically
 * generated. For each value of that property, there is a value of this type whose name is the
 * lowercased version of the property value. */
using script_t = ucd::script_t;

/** @brief Reads the value of the Script property. */
[[nodiscard]] script_t script(char32_t) noexcept;

} // unicode

#endif
