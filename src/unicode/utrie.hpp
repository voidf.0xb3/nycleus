#ifndef UNICODE_UTRIE_HPP_INCLUDED_3F13CWXHJ4CODDUKRU8YHRANH
#define UNICODE_UTRIE_HPP_INCLUDED_3F13CWXHJ4CODDUKRU8YHRANH

#include "unicode/basic.hpp"
#include "util/safe_int.hpp"
#include "util/type_traits.hpp"
#include <cassert>
#include <bit>
#include <cstddef>
#include <cstdint>
#include <type_traits>

namespace unicode {

namespace detail_ {

template<std::size_t DataWidth>
using utrie_value_t =
  std::conditional_t<DataWidth <= 8, std::uint8_t,
  std::conditional_t<DataWidth <= 16, std::uint16_t,
  std::conditional_t<DataWidth <= 32, std::uint32_t, std::uint64_t>>>;

} // detail_

/** @brief Wraps around externally provided data arrays and implements retrieval operations.
 *
 * A Unicode trie is a fast and compact data structure for storing large sets of read-only data
 * for Unicode code points. It consists of three arrays: the data array, the first-level
 * indirection array, and the second-level indirection array.
 *
 * For code points below U+0100 (Basic Latin & Latin-1 Supplement blocks), which are expected
 * to be the most common, the data is stored directly in the data array, indexed by the code
 * point itself.
 *
 * For the rest of the BMP, one level of indirection is used. Everything above the five least
 * significant bits of the code point is used to index into the first-level indirection array
 * (starting with index 0 for U+0100). This is used to retrieve the index of the beginning of a
 * range in the data array; the code point's bottom five bits are then used as an index into
 * that range to retrieve the data.
 *
 * Note that the BMP contains surrogate code points, which are invalid. The trie does not store
 * any data for those code points. This means that the first-level indirection array simply
 * skips these code points: code points U+D7E0 to U+D7FF use index 1719, and code points U+E000
 * to U+E01F use index 1720.
 *
 * For the supplementary code points, two levels of indirection are used. Everything above the
 * eleven least significant bits of the code point is used to index into the second-level
 * indirection array (starting with index 0 for U+10000). This is used to retrieve the index of
 * the beginning of a range in the first-level indirection array; the next six bits of the code
 * point are then used as an index into that range. This produces the index of the beginning of
 * a range in the data array; the code point's bottom five bits are then used as an index into
 * that range to retrieve the data.
 *
 * For the sake of compactness, the following tricks are used:
 * - Code points that are too high are simply not stored in the trie at all; that is, the
 *   first-level or the second-level indirection array simply ends early. Instead, the trie
 *   simply contains a cutoff point and the default value for code points above that point.
 * - In the data array and the first-level indirection array, the ranges for first- and
 *   second-level indirection, respectively, are stored in an overlapped manner to reuse the
 *   same memory for multiple ranges. An optimal overlapping is determined at build time (see
 *   `tools/UTrieBuilder.py`).
 * - If the data width is 1, 2, or 4 bits, multiple data values are packed into a single byte,
 *   starting with the least significant bits for the lowest index. The first-level indirection
 *   array then holds an index into a hypothetical unpacked data array. When retrieving data,
 *   the index needs to be translated into retrieval of the appropriate bits of the appropriate
 *   byte from the packed data array. */
template<
  std::size_t DataWidth,
  util::one_of<util::uint8_t, util::uint16_t, util::uint32_t> Lvl1T,
  util::one_of<util::uint8_t, util::uint16_t> Lvl2T,
  char32_t Top,
  detail_::utrie_value_t<DataWidth> InitialValue,
  util::safe_int<detail_::utrie_value_t<DataWidth>> const* Data,
  Lvl1T const* Lvl1,
  Lvl2T const* Lvl2
>
class utrie {
  static_assert(DataWidth > 0 && DataWidth <= 64);
  static_assert(std::has_single_bit(DataWidth));

public:
  using value_t = util::safe_int<detail_::utrie_value_t<DataWidth>>;

private:
  /** @brief Retrieves the value that would be at the given index if the data array were not
   * packed. */
  [[nodiscard]] static value_t unpack_data(util::size_t index) noexcept {
    if constexpr(DataWidth == 1) {
      return (Data[(index >> 3).unwrap()] >> (index & 7)) & 1;
    } else if constexpr(DataWidth == 2) {
      return (Data[(index >> 2).unwrap()] >> (2 * (index & 3))) & 3;
    } else if constexpr(DataWidth == 4) {
      return (Data[(index >> 1).unwrap()] >> (4 * (index & 1))) & 15;
    } else {
      return Data[index.unwrap()];
    }
  }

  /** @brief The number of least significant bits of the code point that are used as the index
   * into the data array.
   *
   * In case of one-level indirection, the first-level index is the rest of the code point's
   * bits; in case of two-level indirection, it is determined by the index2_width_ value. */
  static constexpr util::size_t index1_width_{5};

  /** @brief The mask selecting first-level index bits from a code point that uses one-level
   * indirection, or first- and second-level index bits from a code point that uses two-level
   * indirection. */
  static constexpr util::uint32_t index1_mask_{~((util::uint32_t{1} << index1_width_) - 1)};

  /** @brief The number of bits of the code point, above those used to index into the data
   * array, that are used to index into the first-level indirection array, in the case of
   * two-level indexing.
   *
   * The second-level index is the rest of the code point's bits. */
  static constexpr util::size_t index2_width_{6};

  /** @brief The mask selecting second-level index bits from a code point that uses two-level
   * indirection. */
  static constexpr util::uint32_t index2_mask_{
    ~((util::uint32_t{1} << (index1_width_ + index2_width_)) - 1)};

  /** @brief The mask selecting the bits of the code point containing the index into the data
   * array. */
  static constexpr util::uint32_t data_index_mask_{(util::uint32_t{1} << index1_width_) - 1};

  /** @brief The number of surrogate code points. */
  static constexpr util::uint32_t surrogate_codepoints_{surrogates_end - surrogates_start};

  /** @brief The lowest code point where one-level indirection begins (below that, we use the
   * code point as a direct index into the data array). */
  static constexpr util::uint32_t lvl1_threshold_{0x0100};

  /** @brief The lowest code point where two-level indirection begins. */
  static constexpr util::uint32_t lvl2_threshold_{0x10000};

public:
  [[nodiscard]] static value_t get(char32_t codepoint) noexcept {
    assert(is_scalar_value(codepoint));
    auto const codepoint_int{util::wrap(static_cast<std::uint32_t>(codepoint))};
    if(codepoint_int >= static_cast<std::uint32_t>(Top)) {
      return util::wrap(InitialValue);
    }

    // Code point uses direct indexing.
    if(codepoint_int < lvl1_threshold_) {
      return utrie::unpack_data(codepoint_int);
    }

    // Code point uses one-level indirection.
    if(codepoint_int < lvl2_threshold_) {
      util::uint32_t shifted{codepoint_int - lvl1_threshold_};
      if(codepoint_int >= static_cast<std::uint32_t>(surrogates_end)) {
        shifted -= surrogate_codepoints_;
      }
      util::size_t const data_idx{Lvl1[(shifted >> index1_width_).unwrap()]};
      return utrie::unpack_data(data_idx + (shifted & data_index_mask_));
    }

    // Code point uses two-level indirection.
    util::uint32_t const shifted{codepoint_int - lvl2_threshold_};
    util::size_t const lvl1_idx{Lvl2[(shifted >> (index1_width_ + index2_width_)).unwrap()]};
    util::size_t const data_idx{Lvl1[
      (lvl1_idx + ((shifted & ~index2_mask_) >> index1_width_)).unwrap()]};
    return utrie::unpack_data(data_idx + (shifted & data_index_mask_));
  }
};

} // unicode

#define UTRIE(prefix) ::unicode::utrie< \
  ::unicode::ucd::prefix##_data_width.unwrap(), \
  ::unicode::ucd::prefix##_lvl1_t, \
  ::unicode::ucd::prefix##_lvl2_t, \
  ::unicode::ucd::prefix##_top, \
  ::unicode::ucd::prefix##_initial_value.unwrap(), \
  ::unicode::ucd::prefix##_data, \
  ::unicode::ucd::prefix##_lvl1, \
  ::unicode::ucd::prefix##_lvl2 \
>

#endif
