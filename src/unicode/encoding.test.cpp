#include "unicode/encoding.hpp"
#include "single_pass_view.hpp"
#include <concepts>
#include <cstdint>
#include <ranges>
#include <string_view>
#include <utility>

using namespace std::string_view_literals;
namespace u = unicode;

namespace {

using u8it = std::u8string_view::iterator;
using i8it = std::uint8_t const*;
using it = std::string_view::iterator;
using u16it = std::u16string_view::iterator;
using i16it = std::uint16_t const*;
using wit = std::wstring_view::iterator;

} // (anonymous)

static_assert(u::decodable_encoding<u::utf8<char8_t>,
  u8it, u8it, u::eh_assume_valid<>>);
static_assert(u::decodable_encoding<u::utf8<std::uint8_t>,
  i8it, i8it, u::eh_assume_valid<>>);
static_assert(u::decodable_encoding<u::utf8<char>,
  it, it, u::eh_assume_valid<>>);
static_assert(u::decodable_encoding<u::utf16<char16_t>,
  u16it, u16it, u::eh_assume_valid<>>);
static_assert(u::decodable_encoding<u::utf16<std::uint16_t>,
  i16it, i16it, u::eh_assume_valid<>>);
#ifdef _WIN32
  static_assert(u::decodable_encoding<u::utf16<wchar_t>,
    wit, wit, u::eh_assume_valid<>>);
#endif

static_assert(u::decrementable_encoding<u::utf8<char8_t>, u8it>);
static_assert(u::decrementable_encoding<u::utf8<std::uint8_t>, i8it>);
static_assert(u::decrementable_encoding<u::utf8<char>, it>);
static_assert(u::decrementable_encoding<u::utf16<char16_t>, u16it>);
static_assert(u::decrementable_encoding<u::utf16<std::uint16_t>, i16it>);
#ifdef _WIN32
  static_assert(u::decrementable_encoding<u::utf16<wchar_t>, wit>);
#endif

namespace {

using u8dec_sp = decltype(std::declval<std::u8string_view>()
  | tests::single_pass | u::utf8_decode());
using i8dec_sp = decltype(std::declval<std::ranges::subrange<i8it>>()
  | tests::single_pass | u::utf8_decode());
using dec_sp = decltype(std::declval<std::string_view>()
  | tests::single_pass | u::utf8_decode());
using u16dec_sp = decltype(std::declval<std::u16string_view>()
  | tests::single_pass | u::utf16_decode());
using i16dec_sp = decltype(std::declval<std::ranges::subrange<i16it>>()
  | tests::single_pass | u::utf16_decode());
#ifdef _WIN32
  using wdec_sp = decltype(std::declval<std::wstring_view>()
    | tests::single_pass | u::utf16_decode());
#endif

using u8dec = decltype(std::declval<std::u8string_view>()
  | u::utf8_decode());
using i8dec = decltype(std::declval<std::ranges::subrange<i8it>>()
  | u::utf8_decode());
using dec = decltype(std::declval<std::string_view>()
  | u::utf8_decode());
using u16dec = decltype(std::declval<std::u16string_view>()
  | u::utf16_decode());
using i16dec = decltype(std::declval<std::ranges::subrange<i16it>>()
  | u::utf16_decode());
#ifdef _WIN32
  using wdec = decltype(std::declval<std::wstring_view>()
    | u::utf16_decode());
#endif

using u8dec_a = decltype(std::declval<std::u8string_view>()
  | u::utf8_decode(u::eh_assume_valid<>{}));
using i8dec_a = decltype(std::declval<std::ranges::subrange<i8it>>()
  | u::utf8_decode(u::eh_assume_valid<>{}));
using dec_a = decltype(std::declval<std::string_view>()
  | u::utf8_decode(u::eh_assume_valid<>{}));
using u16dec_a = decltype(std::declval<std::u16string_view>()
  | u::utf16_decode(u::eh_assume_valid<>{}));
using i16dec_a = decltype(std::declval<std::ranges::subrange<i16it>>()
  | u::utf16_decode(u::eh_assume_valid<>{}));
#ifdef _WIN32
  using wdec_a = decltype(std::declval<std::wstring_view>()
    | u::utf16_decode(u::eh_assume_valid<>{}));
#endif

} // (anonymous)

static_assert(std::ranges::input_range<u8dec_sp>);
static_assert(!std::ranges::forward_range<u8dec_sp>);
static_assert(std::ranges::view<u8dec_sp>);
static_assert(std::same_as<std::ranges::range_value_t<u8dec_sp>, char32_t>);

static_assert(std::ranges::input_range<i8dec_sp>);
static_assert(!std::ranges::forward_range<i8dec_sp>);
static_assert(std::ranges::view<i8dec_sp>);
static_assert(std::same_as<std::ranges::range_value_t<i8dec_sp>, char32_t>);

static_assert(std::ranges::input_range<dec_sp>);
static_assert(!std::ranges::forward_range<dec_sp>);
static_assert(std::ranges::view<dec_sp>);
static_assert(std::same_as<std::ranges::range_value_t<dec_sp>, char32_t>);

static_assert(std::ranges::input_range<u16dec_sp>);
static_assert(!std::ranges::forward_range<u16dec_sp>);
static_assert(std::ranges::view<u16dec_sp>);
static_assert(std::same_as<std::ranges::range_value_t<u16dec_sp>, char32_t>);

static_assert(std::ranges::input_range<i16dec_sp>);
static_assert(!std::ranges::forward_range<i16dec_sp>);
static_assert(std::ranges::view<i16dec_sp>);
static_assert(std::same_as<std::ranges::range_value_t<i16dec_sp>, char32_t>);

#ifdef _WIN32
  static_assert(std::ranges::input_range<wdec_sp>);
  static_assert(!std::ranges::forward_range<wdec_sp>);
  static_assert(std::ranges::view<wdec_sp>);
  static_assert(std::same_as<std::ranges::range_value_t<wdec_sp>, char32_t>);
#endif

static_assert(std::ranges::forward_range<u8dec>);
static_assert(!std::ranges::bidirectional_range<u8dec>);
static_assert(std::ranges::view<u8dec>);
static_assert(std::ranges::common_range<u8dec>);
static_assert(std::same_as<std::ranges::range_value_t<u8dec>, char32_t>);

static_assert(std::ranges::forward_range<i8dec>);
static_assert(!std::ranges::bidirectional_range<i8dec>);
static_assert(std::ranges::view<i8dec>);
static_assert(std::ranges::common_range<i8dec>);
static_assert(std::same_as<std::ranges::range_value_t<i8dec>, char32_t>);

static_assert(std::ranges::forward_range<dec>);
static_assert(!std::ranges::bidirectional_range<dec>);
static_assert(std::ranges::view<dec>);
static_assert(std::ranges::common_range<dec>);
static_assert(std::same_as<std::ranges::range_value_t<dec>, char32_t>);

static_assert(std::ranges::forward_range<u16dec>);
static_assert(!std::ranges::bidirectional_range<u16dec>);
static_assert(std::ranges::view<u16dec>);
static_assert(std::ranges::common_range<u16dec>);
static_assert(std::same_as<std::ranges::range_value_t<u16dec>, char32_t>);

static_assert(std::ranges::forward_range<i16dec>);
static_assert(!std::ranges::bidirectional_range<i16dec>);
static_assert(std::ranges::view<i16dec>);
static_assert(std::ranges::common_range<i16dec>);
static_assert(std::same_as<std::ranges::range_value_t<i16dec>, char32_t>);

#ifdef _WIN32
  static_assert(std::ranges::forward_range<wdec>);
  static_assert(!std::ranges::bidirectional_range<wdec>);
  static_assert(std::ranges::view<wdec>);
  static_assert(std::ranges::common_range<wdec>);
  static_assert(std::same_as<std::ranges::range_value_t<wdec>, char32_t>);
#endif

static_assert(std::ranges::bidirectional_range<u8dec_a>);
static_assert(!std::ranges::random_access_range<u8dec_a>);
static_assert(std::ranges::view<u8dec_a>);
static_assert(std::ranges::common_range<u8dec_a>);
static_assert(std::same_as<std::ranges::range_value_t<u8dec_a>, char32_t>);

static_assert(std::ranges::bidirectional_range<i8dec_a>);
static_assert(!std::ranges::random_access_range<i8dec_a>);
static_assert(std::ranges::view<i8dec_a>);
static_assert(std::ranges::common_range<i8dec_a>);
static_assert(std::same_as<std::ranges::range_value_t<i8dec_a>, char32_t>);

static_assert(std::ranges::bidirectional_range<dec_a>);
static_assert(!std::ranges::random_access_range<dec_a>);
static_assert(std::ranges::view<dec_a>);
static_assert(std::ranges::common_range<dec_a>);
static_assert(std::same_as<std::ranges::range_value_t<dec_a>, char32_t>);

static_assert(std::ranges::bidirectional_range<u16dec_a>);
static_assert(!std::ranges::random_access_range<u16dec_a>);
static_assert(std::ranges::view<u16dec_a>);
static_assert(std::ranges::common_range<u16dec_a>);
static_assert(std::same_as<std::ranges::range_value_t<u16dec_a>, char32_t>);

static_assert(std::ranges::bidirectional_range<i16dec_a>);
static_assert(!std::ranges::random_access_range<i16dec_a>);
static_assert(std::ranges::view<i16dec_a>);
static_assert(std::ranges::common_range<i16dec_a>);
static_assert(std::same_as<std::ranges::range_value_t<i16dec_a>, char32_t>);

#ifdef _WIN32
  static_assert(std::ranges::bidirectional_range<wdec_a>);
  static_assert(!std::ranges::random_access_range<wdec_a>);
  static_assert(std::ranges::view<wdec_a>);
  static_assert(std::ranges::common_range<wdec_a>);
  static_assert(std::same_as<std::ranges::range_value_t<wdec_a>, char32_t>);
#endif

namespace {

// We represent an error code as a surrogate code point, which is guaranteed to
// never otherwise appear in the output.

struct eh_utf8_get_code {
  [[nodiscard]] constexpr char32_t operator()(u::utf8_error error) const
      noexcept {
    std::uint32_t value;
    switch(error) {
      using enum u::utf8_error;
      case unexpected_continuation_byte: value = 0; break;
      case expected_continuation_byte:   value = 1; break;
      case unexpected_end_of_input:      value = 2; break;
      case surrogate:                    value = 3; break;
      case overlong_two_bytes:           value = 4; break;
      case overlong_three_bytes:         value = 5; break;
      case overlong_four_bytes:          value = 6; break;
      case out_of_range:                 value = 7; break;
      case invalid_code_unit:            value = 8; break;
      default: util::unreachable();
    }
    return static_cast<std::uint32_t>(0xD800 + value);
  }
};

struct eh_utf16_get_code {
  [[nodiscard]] constexpr char32_t operator()(u::utf16_error error) const
      noexcept {
    std::uint32_t value;
    switch(error) {
      using enum u::utf16_error;
      case unpaired_leading_surrogate:  value = 0; break;
      case unpaired_trailing_surrogate: value = 1; break;
      default: util::unreachable();
    }
    return static_cast<std::uint32_t>(0xD800 + value);
  }
};

} // (anonymous)

////////////////////////////////////////////////////////////////////////////////
// UTF-8 ///////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Decoding: single-pass ///////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  u8"test string"sv | tests::single_pass | u::utf8_decode(),
  U"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  u8"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u8"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | tests::single_pass | u::utf8_decode(),
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  u8"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | tests::single_pass | u::utf8_decode(),
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  u8"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u8"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | tests::single_pass | u::utf8_decode(),
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Decoding: eh_replace ////////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  u8"test string"sv | u::utf8_decode(), U"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  u8"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u8"\u0441\u0442\u0440\u043E\u043A\u0430"sv | u::utf8_decode(),
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  u8"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv | u::utf8_decode(),
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  u8"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u8"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv | u::utf8_decode(),
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Decoding: eh_assume_valid ///////////////////////////////////////////////////

static_assert(std::ranges::equal(
  u8"test string"sv | u::utf8_decode(u::eh_assume_valid<>{}),
  U"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  u8"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u8"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | u::utf8_decode(u::eh_assume_valid<>{}),
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  u8"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | u::utf8_decode(u::eh_assume_valid<>{}),
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  u8"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u8"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | u::utf8_decode(u::eh_assume_valid<>{}),
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Decoding: reverse ///////////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  u8"test string"sv
  | u::utf8_decode(u::eh_assume_valid<>{}) | std::views::reverse,
  U"test string"sv | std::views::reverse));

// "тестовая строка"
static_assert(std::ranges::equal(
  u8"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u8"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | u::utf8_decode(u::eh_assume_valid<>{}) | std::views::reverse,
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv | std::views::reverse));

// "測試字符串"
static_assert(std::ranges::equal(
  u8"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | u::utf8_decode(u::eh_assume_valid<>{}) | std::views::reverse,
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv | std::views::reverse));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  u8"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u8"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | u::utf8_decode(u::eh_assume_valid<>{}) | std::views::reverse,
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv | std::views::reverse));

// Decoding errors: replacement ////////////////////////////////////////////////

// unexpected end of input
static_assert(std::ranges::equal(
  u8"\xF0\x9F"sv // ..."\x90\xB2"
  | u::utf8_decode(),
  U"\uFFFD"sv));

// unexpected continuation byte
static_assert(std::ranges::equal(
  u8"\x90\xB2"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD"sv));

// invalid code unit
static_assert(std::ranges::equal(
  u8"\xFF"sv
  | u::utf8_decode(),
  U"\uFFFD"sv));

// expected continuation byte for a 2-byte encoding
static_assert(std::ranges::equal(
  u8"\xD0\x20"sv
  | u::utf8_decode(),
  U"\uFFFD\u0020"sv));

// expected continuation byte for a 3-byte encoding
static_assert(std::ranges::equal(
  u8"\xE4\xBF\x20"sv
  | u::utf8_decode(),
  U"\uFFFD\u0020"sv));

// expected continuation byte for a 4-byte encoding
static_assert(std::ranges::equal(
  u8"\xF0\x9F\x92\x20"sv
  | u::utf8_decode(),
  U"\uFFFD\u0020"sv));

// overlong encoding: 2 bytes instead of 1
static_assert(std::ranges::equal(
  u8"\xC0\xA0"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD"sv));

// overlong encoding: 3 bytes instead of 1
static_assert(std::ranges::equal(
  u8"\xE0\x80\xA0"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD\uFFFD"sv));

// overlong encoding: 4 bytes instead of 1
static_assert(std::ranges::equal(
  u8"\xF0\x80\x80\xA0"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD\uFFFD\uFFFD"sv));

// overlong encoding: 3 bytes instead of 2
static_assert(std::ranges::equal(
  u8"\xE0\x90\x96"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD\uFFFD"sv));

// overlong encoding: 4 bytes instead of 2
static_assert(std::ranges::equal(
  u8"\xF0\x80\x90\x96"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD\uFFFD\uFFFD"sv));

// overlong encoding: 4 bytes instead of 3
static_assert(std::ranges::equal(
  u8"\xF0\x84\xBF\xA1"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD\uFFFD\uFFFD"sv));

// surrogate code point encoded
static_assert(std::ranges::equal(
  u8"\xED\xA0\xBD\xED\xB2\xA9"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD\uFFFD"sv));

// value outside of Unicode code space
static_assert(std::ranges::equal(
  u8"\xF4\x90\x80\x80"sv
  | u::utf8_decode(),
  U"\uFFFD\uFFFD\uFFFD\uFFFD"sv));

// Decoding errors: recognition ////////////////////////////////////////////////

// unexpected end of input
static_assert(std::ranges::equal(
  u8"\xF0\x9F"sv // ..."\x90\xB2"
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD802"sv));

// unexpected continuation byte
static_assert(std::ranges::equal(
  u8"\x90\xB2"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD800\xD800"sv));

// invalid code unit
static_assert(std::ranges::equal(
  u8"\xFF"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD808"sv));

// expected continuation byte for a 2-byte encoding
static_assert(std::ranges::equal(
  u8"\xD0\x20"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD801\u0020"sv));

// expected continuation byte for a 3-byte encoding
static_assert(std::ranges::equal(
  u8"\xE4\xBF\x20"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD801\u0020"sv));

// expected continuation byte for a 4-byte encoding
static_assert(std::ranges::equal(
  u8"\xF0\x9F\x92\x20"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD801\u0020"sv));

// overlong encoding: 2 bytes instead of 1
static_assert(std::ranges::equal(
  u8"\xC0\xA0"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD804\xD800"sv));

// overlong encoding: 3 bytes instead of 1
static_assert(std::ranges::equal(
  u8"\xE0\x80\xA0"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD805\xD800\xD800"sv));

// overlong encoding: 4 bytes instead of 1
static_assert(std::ranges::equal(
  u8"\xF0\x80\x80\xA0"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD806\xD800\xD800\xD800"sv));

// overlong encoding: 3 bytes instead of 2
static_assert(std::ranges::equal(
  u8"\xE0\x90\x96"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD805\xD800\xD800"sv));

// overlong encoding: 4 bytes instead of 2
static_assert(std::ranges::equal(
  u8"\xF0\x80\x90\x96"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD806\xD800\xD800\xD800"sv));

// overlong encoding: 4 bytes instead of 3
static_assert(std::ranges::equal(
  u8"\xF0\x84\xBF\xA1"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD806\xD800\xD800\xD800"sv));

// surrogate code point encoded
static_assert(std::ranges::equal(
  u8"\xED\xA0\xBD\xED\xB2\xA9"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD803\xD800\xD800\xD803\xD800\xD800"sv));

// value outside of Unicode code space
static_assert(std::ranges::equal(
  u8"\xF4\x90\x80\x80"sv
  | u::utf8_decode(eh_utf8_get_code{}),
  U"\xD807\xD800\xD800\xD800"sv));

// Validation //////////////////////////////////////////////////////////////////

static_assert(u::is_valid_utf8(u8"test string"sv));
static_assert(!u::is_valid_utf8(u8"test\xFFstring"sv));

// Encoding: single-pass ///////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  U"test string"sv | tests::single_pass | u::utf8_encode<char8_t>(),
  u8"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | tests::single_pass | u::utf8_encode<char8_t>(),
  u8"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u8"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | tests::single_pass | u::utf8_encode<char8_t>(),
  u8"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | tests::single_pass | u::utf8_encode<char8_t>(),
  u8"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u8"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Encoding: multipass /////////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  U"test string"sv | u::utf8_encode<char8_t>(), u8"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv | u::utf8_encode<char8_t>(),
  u8"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u8"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv | u::utf8_encode<char8_t>(),
  u8"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv | u::utf8_encode<char8_t>(),
  u8"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u8"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Encoding: reverse ///////////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  U"test string"sv | u::utf8_encode<char8_t>() | std::views::reverse,
  u8"test string"sv | std::views::reverse));

// "тестовая строка"
static_assert(std::ranges::equal(
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | u::utf8_encode<char8_t>() | std::views::reverse,
  u8"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u8"\u0441\u0442\u0440\u043E\u043A\u0430"sv | std::views::reverse));

// "測試字符串"
static_assert(std::ranges::equal(
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | u::utf8_encode<char8_t>() | std::views::reverse,
  u8"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv | std::views::reverse));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | u::utf8_encode<char8_t>() | std::views::reverse,
  u8"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u8"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv | std::views::reverse));

////////////////////////////////////////////////////////////////////////////////
// UTF-16 //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Decoding: single-pass ///////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  u"test string"sv | tests::single_pass | u::utf16_decode(),
  U"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  u"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | tests::single_pass | u::utf16_decode(),
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  u"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | tests::single_pass | u::utf16_decode(),
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  u"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | tests::single_pass | u::utf16_decode(),
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Decoding: eh_replace ////////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  u"test string"sv | u::utf16_decode(), U"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  u"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u"\u0441\u0442\u0440\u043E\u043A\u0430"sv | u::utf16_decode(),
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  u"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv | u::utf16_decode(),
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  u"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv | u::utf16_decode(),
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Decoding: eh_assume_valid ///////////////////////////////////////////////////

static_assert(std::ranges::equal(
  u"test string"sv | u::utf16_decode(u::eh_assume_valid<>{}),
  U"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  u"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | u::utf16_decode(u::eh_assume_valid<>{}),
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  u"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | u::utf16_decode(u::eh_assume_valid<>{}),
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  u"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | u::utf16_decode(u::eh_assume_valid<>{}),
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Decoding: reverse ///////////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  u"test string"sv
  | u::utf16_decode(u::eh_assume_valid<>{}) | std::views::reverse,
  U"test string"sv | std::views::reverse));

// "тестовая строка"
static_assert(std::ranges::equal(
  u"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | u::utf16_decode(u::eh_assume_valid<>{}) | std::views::reverse,
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv | std::views::reverse));

// "測試字符串"
static_assert(std::ranges::equal(
  u"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | u::utf16_decode(u::eh_assume_valid<>{}) | std::views::reverse,
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv | std::views::reverse));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  u"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | u::utf16_decode(u::eh_assume_valid<>{}) | std::views::reverse,
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv | std::views::reverse));

// Decoding errors: replacement ////////////////////////////////////////////////

// unpaired leading surrogate at the end
static_assert(std::ranges::equal(
  u"\xD83D"sv // ..."\xDC49"
  | u::utf16_decode(),
  U"\uFFFD"sv));

// unpaired leading surrogate in the middle
static_assert(std::ranges::equal(
  u"\xD83D\u0020"sv
  | u::utf16_decode(),
  U"\uFFFD\u0020"sv));

// unpaired trailing surrogate
static_assert(std::ranges::equal(
  u"\xDC49"sv
  | u::utf16_decode(),
  U"\uFFFD"sv));

// Decoding errors: recognition ////////////////////////////////////////////////

// unpaired leading surrogate at the end
static_assert(std::ranges::equal(
  u"\xD83D"sv // ..."\xDC49"
  | u::utf16_decode(eh_utf16_get_code{}),
  U"\xD800"sv));

// unpaired leading surrogate in the middle
static_assert(std::ranges::equal(
  u"\xD83D\u0020"sv
  | u::utf16_decode(eh_utf16_get_code{}),
  U"\xD800\u0020"sv));

// unpaired trailing surrogate
static_assert(std::ranges::equal(
  u"\xDC49"sv
  | u::utf16_decode(eh_utf16_get_code{}),
  U"\xD801"sv));

// Encoding: single-pass ///////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  U"test string"sv | tests::single_pass | u::utf16_encode<char16_t>(),
  u"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | tests::single_pass | u::utf16_encode<char16_t>(),
  u"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | tests::single_pass | u::utf16_encode<char16_t>(),
  u"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | tests::single_pass | u::utf16_encode<char16_t>(),
  u"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Encoding: multipass /////////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  U"test string"sv | u::utf16_encode<char16_t>(), u"test string"sv));

// "тестовая строка"
static_assert(std::ranges::equal(
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv | u::utf16_encode<char16_t>(),
  u"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u"\u0441\u0442\u0440\u043E\u043A\u0430"sv));

// "測試字符串"
static_assert(std::ranges::equal(
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv | u::utf16_encode<char16_t>(),
  u"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv | u::utf16_encode<char16_t>(),
  u"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv));

// Encoding: reverse ///////////////////////////////////////////////////////////

static_assert(std::ranges::equal(
  U"test string"sv | u::utf16_encode<char16_t>() | std::views::reverse,
  u"test string"sv | std::views::reverse));

// "тестовая строка"
static_assert(std::ranges::equal(
  U"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  U"\u0441\u0442\u0440\u043E\u043A\u0430"sv
  | u::utf16_encode<char16_t>() | std::views::reverse,
  u"\u0442\u0435\u0441\u0442\u043E\u0432\u0430\u044F "
  u"\u0441\u0442\u0440\u043E\u043A\u0430"sv | std::views::reverse));

// "測試字符串"
static_assert(std::ranges::equal(
  U"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv
  | u::utf16_encode<char16_t>() | std::views::reverse,
  u"\u6E2C\u8A66\u5B57\u7B26\u4E32"sv | std::views::reverse));

// "👉🎻🐲⚙️🎯🔬🔠-8⃣🛠📟"
static_assert(std::ranges::equal(
  U"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  U"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv
  | u::utf16_encode<char16_t>() | std::views::reverse,
  u"\U0001F449\U0001F3BB\U0001F432\u2699\uFE0F\U0001F3AF\U0001F52C\U0001F520"
  u"\u002D\u0038\u20E3\U0001F6E0\U0001F4DF"sv | std::views::reverse));
