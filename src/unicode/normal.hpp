#ifndef UNICODE_NORMAL_HPP_INCLUDED_V788N9L644GGU5R769GRKTE1G
#define UNICODE_NORMAL_HPP_INCLUDED_V788N9L644GGU5R769GRKTE1G

#include "unicode/basic.hpp"
#include "unicode/props.hpp"
#include "util/iterator_facade.hpp"
#include "util/overflow.hpp"
#include "util/range_adaptor_closure.hpp"
#include "util/safe_int.hpp"
#include "util/sentinel_vector.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <cassert>
#include <concepts>
#include <cstddef>
#include <exception>
#include <iterator>
#include <limits>
#include <ranges>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

namespace unicode {

/** @brief Values of Quick_Check properties. */
enum class qc_t { yes = 0, no = 1, maybe = 2 };

namespace detail_ {

/** @brief The maximum size of a full decomposition of a code point. */
constexpr util::size_t max_decomp_size{4};

/** @brief Read the value of the Full_Composition_Exclusion property. */
[[nodiscard]] bool full_comp_excl(char32_t) noexcept;

template<std::size_t N>
using vec_char32 = util::sentinel_vector<char32_t, N, sentinel>;

/** @brief Returns the canonical decomposition of the code point. If the code
 * point doesn't have a decomposition, returns just that code point.
 *
 * Decompositions (except for algorithmically computed Hangul decompositions)
 * are stored in a Unicode trie (see unicode::utrie). The value associated with
 * a code point stores the decomposition's length (which is assumed to be no
 * longer than 3) in the two least significant bits. If the code point has no
 * decomposition, the two lower bits are zero. The rest of the bits store an
 * index into the decomposition array.
 *
 * The decomposition array stores the actual decompositions. For decompositions
 * of length other than 2, the decomposition is stored directly in the array,
 * encoded using UTF-16. (UTF-16 was chosen instead of UTF-8, because the
 * distribution of code points in the decomposition array is likely to be very
 * different than in text.)
 *
 * For decompositions of length 2, a different, more compact, scheme is used.
 * Every codepoint that can appear in the first position in a length-2
 * decomposition is stored in a separate array, encoded in UTF-16. Codepoints
 * that can appear in the second position are likewise stored in a different
 * array. Then, the main decomposition array stores two indexes into these two
 * arrays, bit-packed so that the first index is stored in the lower bits. If
 * the indexes are small enough (which, as of Unicode 15.0.0, they are), this
 * allows storing the decomposition in one two-byte integer instead of two.
 * (This decrease is space is not completely offset by the extra space for the
 * two additional arrays.) */
[[nodiscard]] vec_char32<max_decomp_size.unwrap()> canon_decompose(char32_t) noexcept;

/** @brief If the two code points can be canonically composed (in the given
 * order), returns their composition. Otherwise, returns the sentinel value.
 *
 * Compositions are the inverse of length-2 decompositions. They are stored in
 * a compact format, as follows. First, there are two Unicode tries (see
 * unicode::utrie); the first code point is looked up in one of them, and the
 * second, in the other one. Each lookup produces an ID, which is 0 if the code
 * point cannot appear in the corresponding position in a decomposition.
 *
 * If neither ID is 0, they are (after subtracting 1) used as indexes into
 * a composition array, which is treated as two-dimensional. This retrieves a
 * two-byte array element, which encodes the composition, if it exists.
 *
 * Because supplementary code points are rare, a compact encoding scheme is
 * used. A composition which is a BMP code point is stored directly in the
 * array. This cannot be a surrogate code point, so those values are reused.
 * First, the value 0xDFFF (the highest possible surrogate value) represents the
 * absence of a composition. Second, all other surrogate values represent
 * indexes (increased by 0xD800) into a separate array, containing supplementary
 * code points.
 *
 * The secondary array stores supplementary code point values. Since a
 * supplementary code point fits into 20 bits (after subtracting 0x10000), two
 * code points can be efficiently packed into 5 bytes, starting with the least
 * significant bits (in little-endian order) for the first of the two code
 * points. The index, retrieved from the primary array, is used to unpack the
 * desired supplementary code point.
 *
 * This system cannot store more than 2047 supplementary code points. As of
 * Unicode 15.0.0, this is nowhere near a problem, as the actual number of
 * supplementary code points is much lower. */
[[nodiscard]] char32_t canon_compose(char32_t, char32_t) noexcept;

/** @brief Returns the full decomposition of the code point. */
[[nodiscard]] vec_char32<max_decomp_size.unwrap()> full_decompose(char32_t) noexcept;

/** @brief Performs the Canonical Ordering Algoithm on part of a string.
 * @param buf The string to be processed.
 * @param it An iterator into the string, indicating the beginning of the part
 *           to process. */
void canonical_ordering(std::u32string& buf,
  std::u32string::iterator it_begin) noexcept;

/** @brief Performs the Canonical Composition Algorithm on part of a string.
 * @param buf The string to be processed.
 * @param it An iterator into the string, indicating the beginning of the part
 *           to process. */
void canonical_composition(std::u32string& buf,
  std::u32string::iterator it_begin) noexcept;

} // detail_

struct nfc_traits {
  [[nodiscard]] static qc_t qc(char32_t ch) noexcept;

  static constexpr bool qc_is_conclusive{false};

  static void cvt_chunk(std::u32string& buf,
      std::u32string::iterator it) noexcept {
    detail_::canonical_ordering(buf, it);
    detail_::canonical_composition(buf, it);
  }
};

struct nfd_traits {
  [[nodiscard]] static qc_t qc(char32_t ch) noexcept;

  static constexpr bool qc_is_conclusive{true};

  static void cvt_chunk(std::u32string& buf,
      std::u32string::iterator it) noexcept {
    detail_::canonical_ordering(buf, it);
  }
};

/** @brief A concept that checks that the type is one of the normalization
 * form traits types.
 *
 * This is a closed set of types that can be used as template arguments to
 * various normalization code to select the desired normal form. */
template<typename T>
concept normal_form_traits = util::one_of<T, nfc_traits, nfd_traits>;

/** @brief A class that stores the state of the algorithm for quickly checking
 * for a Unicode normal form.
 *
 * To perform a quick check, an instance of this class has to be created, then
 * the code points of the string to be checked have to be fed to it one by one
 * via the `feed` function. It returns a quick check result for the string of
 * code points fed so far. The result never goes up in value (unless `reset` is
 * called): if `maybe` is returned at some point, then `yes` will never be
 * returned; and if `no` is returned, then `no` will be returned forever. */
template<normal_form_traits Traits>
class normal_quick_checker {
private:
  ccc_t last_ccc_{0};
  qc_t result_{qc_t::yes};

public:
  /** @brief Advances the checker by one code point.
   * @returns The quick check result for the string that has been fed so far. */
  qc_t feed(char32_t ch) noexcept {
    assert(is_scalar_value(ch));
    if(result_ == qc_t::no) {
      return qc_t::no;
    }

    ccc_t const cur_ccc{ccc(ch)};
    if(last_ccc_ > cur_ccc && cur_ccc != 0) {
      result_ = qc_t::no;
      return qc_t::no;
    }
    last_ccc_ = cur_ccc;

    switch(Traits::qc(ch)) {
      case qc_t::yes:
        break;
      case qc_t::maybe:
        if(result_ == qc_t::yes) {
          result_ = qc_t::maybe;
        }
        break;
      case qc_t::no:
        result_ = qc_t::no;
        break;
    }
    return result_;
  }

  /** @brief Determines the result of quick-cheking the string that has been fed
   * so far.
   * @returns The return value of the last call to `feed`, or `yes` if there was
   *          no such call. */
  [[nodiscard]] qc_t get_result() const noexcept {
    return result_;
  }

  /** @brief Resets the checker to the state corresponding to an empty
   * string. */
  void reset() noexcept {
    last_ccc_ = 0;
    result_ = qc_t::yes;
  }
};

/** @brief Performs a quick check for a normal form.
 *
 * @returns true if the string is definitely in the normal form. False means
 *          that the string may or may not be in the normal form. */
template<normal_form_traits Traits, typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<std::ranges::range_value_t<Range&&>, char32_t>
[[nodiscard]] bool is_normal_qc(Range&& range) noexcept(
  noexcept(std::ranges::begin(range)) && noexcept(std::ranges::end(range))
  && noexcept(std::declval<std::ranges::iterator_t<Range&&>&>()
    != std::declval<std::ranges::sentinel_t<Range&&>&>())
  && noexcept(++std::declval<std::ranges::iterator_t<Range&&>&>())
  && noexcept(*std::declval<std::ranges::iterator_t<Range&&>&>())
) {
  normal_quick_checker<Traits> checker;
  for(char32_t const ch: range) {
    assert(is_scalar_value(ch));
    if(checker.feed(ch) != qc_t::yes) {
      return false;
    }
  }
  return true;
}

/** @brief Performs a quick check for NFC.
 *
 * @returns true if the string is definitely in NFC. False means that the string
 *          may or may not be in NFC. */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<std::ranges::range_value_t<Range&&>, char32_t>
[[nodiscard]] bool is_nfc_qc(Range&& range)
    noexcept(noexcept(is_normal_qc<nfc_traits>(std::forward<Range>(range)))) {
  return is_normal_qc<nfc_traits>(std::forward<Range>(range));
}

/** @brief Performs a quick check for NFD.
 *
 * @returns true if the string is definitely in NFD. False means that the string
 *          may or may not be in NFD. */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<std::ranges::range_value_t<Range&&>, char32_t>
[[nodiscard]] bool is_nfd_qc(Range&& range)
    noexcept(noexcept(is_normal_qc<nfd_traits>(std::forward<Range>(range)))) {
  return is_normal_qc<nfd_traits>(std::forward<Range>(range));
}

namespace detail_ {

/** @brief Determines whether the code point is "stable" with respect to a
 * normal form.
 *
 * A code point is stable with respect to a normal form iff:
 * - it has CCC=0;
 * - it is not modified by normalization to that form;
 * - in case of normal forms that perform composition, it does not compose with
 *   a preceding character.
 *
 * Note that this definition is weaker than the one found in UAX #15. */
template<normal_form_traits Traits>
[[nodiscard]] bool is_stable_codepoint(char32_t codepoint) noexcept {
  assert(is_scalar_value(codepoint));
  return Traits::qc(codepoint) == qc_t::yes && ccc(codepoint) == 0;
}

} // detail_

////////////////////////////////////////////////////////////////////////////////

/** @brief A view that converts an underlying view of code points to a normal
 * form.
 *
 * Whenever appropriate, iterator operations can throw anything that can be
 * thrown by operations on underlying iterators, or std::length_error.
 *
 * If the underlying view type and its iterator type can be swapped without
 * throwing, then assignment operations of this view and its iterator have
 * strong exception safety guarantees.
 *
 * In all other cases, operations have no exception safety guarantees: after an
 * operation throws an exception, the only valid actions are destroying the
 * object or assigning to it. */
template<normal_form_traits Traits, std::ranges::view View>
requires std::same_as<std::ranges::range_value_t<View>, char32_t>
class normal_view:
    public std::ranges::view_interface<normal_view<Traits, View>> {
private:
  using base_iterator = std::ranges::iterator_t<View>;

  View view_;
  base_iterator base_it_{};
  std::u32string buf_{};
  std::u32string::size_type buf_idx_{0};

  /** @brief Fills the internal buffer with part of the underlying range up to
   * the next stable code point, or the end of the range, and converts that part
   * to NFC.
   *
   * In addition to the specified exception class, this can throw whatever can
   * be thrown by various operations on underlying iterators.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  void next_chunk() {
    auto const base_iend{std::ranges::end(view_)};

    buf_idx_ = 0;
    buf_.clear();
    if(base_it_ == base_iend) {
      return;
    }

    {
      normal_quick_checker<Traits> checker;

      do {
        char32_t const ch{*base_it_};
        assert(is_scalar_value(ch));
        if(checker.feed(ch) != qc_t::yes) {
          break;
        }
        ++base_it_;
        buf_.push_back(ch);
      } while(base_it_ != base_iend
        && !detail_::is_stable_codepoint<Traits>(*base_it_));

      if(checker.get_result() == qc_t::yes) {
        return;
      }
    }

    // Replace the already-copied part of the chunk with its full decomposition
    util::size_t i{0};
    while(i != util::wrap(buf_.size())) {
      auto const decomp{detail_::full_decompose(buf_[i.unwrap()])};
      auto decomp_common{decomp | std::views::common};
      buf_.replace(buf_.begin() + i.unwrap(),
        buf_.begin() + (i + 1).unwrap(),
        decomp_common.begin(), decomp_common.end());
      i += util::wrap(decomp.size());
    }

    // Add the full decompositions of the rest of the chunk
    while(base_it_ != base_iend
        && !detail_::is_stable_codepoint<Traits>(*base_it_)) {
      auto const decomp{detail_::full_decompose(*base_it_)};
      std::ranges::copy(decomp, std::back_inserter(buf_));
      ++base_it_;
    }

    Traits::cvt_chunk(buf_, buf_.begin());
  }

  class iterator_core {
  private:
    normal_view* range_;

  public:
    iterator_core() = default;

    [[nodiscard]] base_iterator base() const
        noexcept(std::is_nothrow_copy_constructible_v<base_iterator>) {
      return range_->it_;
    }

  private:
    friend class normal_view;
    explicit iterator_core(normal_view& range) noexcept: range_{&range} {}

  protected:
    static constexpr bool is_single_pass{true};

    [[nodiscard]] char32_t dereference() const noexcept {
      assert(range_->buf_idx_ < range_->buf_.size());
      return range_->buf_[range_->buf_idx_];
    }

    [[nodiscard]] bool equal_to(iterator_core const& other) const noexcept {
      return range_ == other.range_;
    }

    [[nodiscard]] bool equal_to(std::default_sentinel_t) const
        noexcept(noexcept(range_->base_it_
        == std::ranges::end(range_->view_))) {
      return range_->buf_.empty()
        && range_->base_it_ == std::ranges::end(range_->view_);
    }

    void increment() {
      if(++range_->buf_idx_ == range_->buf_.size()) {
        range_->next_chunk();
      }
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  normal_view() = default;

  normal_view(normal_view const& other)
    noexcept(std::is_nothrow_copy_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{other.view_} {}

  normal_view(normal_view&& other)
    noexcept(std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{std::move(other.view_)} {}

  normal_view& operator=(normal_view const& other)
      requires std::is_copy_constructible_v<View> {
    view_ = other.view_;
    return *this;
  }

  normal_view& operator=(normal_view&& other)
      noexcept(std::is_nothrow_move_assignable_v<View>) {
    view_ = std::move(other.view_);
    return *this;
  }

  ~normal_view() = default;

  void swap(normal_view& other) noexcept(std::is_nothrow_swappable_v<View>) {
    using std::swap;
    swap(view_, other.view_);
  }

  explicit normal_view(View view)
    noexcept(std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{std::move(view)} {}

  /** @brief Returns an iterator to the start of the range.
   *
   * Since this is a single-pass version, this can only be called once.
   *
   * May also throw whatever is thrown by operations on the underlying view.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] iterator begin() {
    base_it_ = std::ranges::begin(view_);
    next_chunk();
    iterator it{*this};
    return it;
  }

  [[nodiscard]] std::default_sentinel_t end() noexcept {
    return std::default_sentinel;
  }

  [[nodiscard]] View base() const&
      noexcept(std::is_nothrow_copy_constructible_v<View>)
      requires std::is_copy_constructible_v<View> {
    return view_;
  }

  [[nodiscard]] View base() &&
      noexcept(std::is_nothrow_move_constructible_v<View>) {
    return std::move(view_);
  }
};

template<normal_form_traits Traits, std::ranges::view View>
requires std::same_as<std::ranges::range_value_t<View>, char32_t>
  && std::ranges::forward_range<View>
class normal_view<Traits, View>:
    public std::ranges::view_interface<normal_view<Traits, View>> {
private:
  using base_iterator = std::ranges::iterator_t<View>;

  /** @brief The internal data of an iterator.
   *
   * This is not a proper class, in the sense that it does not encapsulate its
   * data and does not maintain its invariant. It should be viewed as an
   * implementation detail of `normal_view`. It is used both as the "guts" of an
   * iterator and the first-iterator cache of `normal_view`.
   *
   * An iterator, at any time, can be in one of three modes: null, buffered, or
   * passthrough.
   *
   * In buffered mode, the current chunk is stored in the buffer `buf_`. The
   * field `idx_` stores the (non-negative) index of the current code point in
   * that buffer. The `base_it_` iterator is the beginning of the next chunk.
   *
   * In passthrough mode, the current chunk is identical to the corresponding
   * chunk of the underlying range. The buffer is absent, and the field `idx_`
   * stores the (negative) offset from the beginning of the next chunk to the
   * current position. The `base_it_` iterator is the current position.
   *
   * Null mode represents one of two situations: an empty cache within
   * `normal_view` and a past-the-end iterator. In this mode, `idx_` is set to
   * is minimum value. */
  struct iterator_impl {
    base_iterator base_it_;

    using idx_t = std::common_type_t<std::ranges::range_difference_t<View>,
      std::u32string::difference_type>;
    static constexpr idx_t idx_sentinel{std::numeric_limits<idx_t>::min()};
    idx_t idx_;

    union { std::u32string buf_; };

    iterator_impl()
      noexcept(std::is_nothrow_default_constructible_v<base_iterator>):
      base_it_{}, idx_{idx_sentinel} {}

    /** @brief Copy-constructs the object.
     *
     * May also throw whatever is thrown by copy-construction of the underlying
     * iterator.
     *
     * @throws std::bad_alloc */
    iterator_impl(iterator_impl const& other): base_it_{other.base_it_},
        idx_{other.idx_} {
      if(idx_ >= 0) {
        new(&buf_) std::u32string{other.buf_};
      }
    }

    iterator_impl(iterator_impl&& other)
        noexcept(std::is_nothrow_move_constructible_v<base_iterator>):
        base_it_{std::move(other.base_it_)}, idx_{other.idx_} {
      if(idx_ >= 0) {
        new(&buf_) std::u32string{std::move(other.buf_)};
      }
    }

    /** @brief Copy-constructs the object.
     *
     * May also throw whatever is thrown by copy-construction of the underlying
     * iterator.
     *
     * @throws std::bad_alloc */
    iterator_impl& operator=(iterator_impl const& other) {
      iterator_impl copy{other};
      this->swap(copy);
      return *this;
    }

    iterator_impl& operator=(iterator_impl&& other)
        noexcept(std::is_nothrow_move_assignable_v<base_iterator>
        || (std::is_nothrow_move_constructible_v<iterator_impl>
        && noexcept(this->swap(std::declval<iterator_impl&>())))) {
      if constexpr(std::is_nothrow_move_assignable_v<base_iterator>) {
        if(idx_ >= 0) {
          if(other.idx_ >= 0) {
            buf_ = std::move(other.buf_);
          } else {
            using std::u32string;
            buf_.~u32string();
          }
        } else {
          if(other.idx_ >= 0) {
            new(&buf_) std::u32string{std::move(other.buf_)};
          }
        }

        base_it_ = std::move(other.base_it_);
        idx_ = other.idx_;
      } else {
        iterator_impl copy{std::move(other)};
        this->swap(copy);
        return *this;
      }
      return *this;
    }

    ~iterator_impl() noexcept {
      if(idx_ >= 0) {
        using std::u32string;
        buf_.~u32string();
      }
    }

    void swap(iterator_impl& other)
        noexcept(std::is_nothrow_swappable_v<base_iterator>) {
      using std::swap;
      swap(base_it_, other.base_it_);
      swap(idx_, other.idx_);

      if(idx_ >= 0) {
        if(other.idx_ >= 0) {
          swap(buf_, other.buf_);
        } else {
          using std::u32string;
          new(&buf_) u32string{std::move(other.buf_)};
          other.buf_.~u32string();
        }
      } else {
        if(other.idx_ >= 0) {
          using std::u32string;
          new(&other.buf_) u32string{std::move(buf_)};
          buf_.~u32string();
        }
      }
    }

    /** @brief Fills the internal buffer with part of the underlying range up
     * to the next stable code point, or the end of the range, and converts that
     * part to NFC.
     *
     * This should only be called while there is no buffer object constructed.
     * If this throws an exception, there is still no buffer constructed. In
     * this case it is the caller's responsibility to ensure that `idx_` is set
     * to a negative value.
     *
     * In addition to the specified exception class, this can throw whatever can
     * be thrown by various operations on underlying iterators.
     *
     * @throws std::length_error
     * @throws std::bad_alloc */
    void next_chunk(View& view) {
      auto const end{std::ranges::end(view)};
      if(base_it_ == end) {
        idx_ = idx_sentinel;
        return;
      }

      idx_t count{0};
      base_iterator next{base_it_};
      do {
        assert(count < std::numeric_limits<idx_t>::max());
        ++next;
        ++count;
      } while(next != end && !detail_::is_stable_codepoint<Traits>(*next));

      if(is_normal_qc<Traits>(std::ranges::subrange{base_it_, next})) {
        idx_ = -count;
        return;
      }

      // Construct the chunk's full decomposition
      std::u32string new_buf;
      for(idx_t i{0}; i < count; ++i, ++base_it_) {
        auto const decomp{detail_::full_decompose(*base_it_)};
        std::ranges::copy(decomp, std::back_inserter(new_buf));
      }

      Traits::cvt_chunk(new_buf, new_buf.begin());
      new(&buf_) std::u32string{std::move(new_buf)};
      idx_ = 0;
    }
  };

  View view_;
  iterator_impl cache_{};

  class iterator_core {
  private:
    normal_view* range_;
    iterator_impl impl_;

  public:
    iterator_core() = default;

    [[nodiscard]] base_iterator base() const&
        noexcept(std::is_nothrow_copy_constructible_v<base_iterator>) {
      return impl_.base_it_;
    }

    [[nodiscard]] base_iterator base() &&
        noexcept(std::is_nothrow_move_constructible_v<base_iterator>) {
      return std::move(impl_.base_it_);
    }

  private:
    friend class normal_view;
    /** @brief Constructs a new iterator core.
     *
     * May also throw whatever is thrown by base iterator copy-construction.
     *
     * @throws std::bad_alloc */
    // NOLINTNEXTLINE(modernize-pass-by-value)
    explicit iterator_core(normal_view* range, iterator_impl const& impl):
      range_{range}, impl_{impl} {}

  protected:
    [[nodiscard]] char32_t dereference() const
        noexcept(noexcept(*std::declval<base_iterator>())) {
      assert(impl_.idx_ != iterator_impl::idx_sentinel);
      if(impl_.idx_ >= 0) {
        assert(static_cast<std::size_t>(impl_.idx_) < impl_.buf_.size());
        return impl_.buf_[impl_.idx_];
      } else {
        return *impl_.base_it_;
      }
    }

    void increment() {
      try {
        if(impl_.idx_ < 0) {
          ++impl_.base_it_;
        }
        ++impl_.idx_;

        if(impl_.idx_ == 0) {
          impl_.next_chunk(range_->view_);
        } else if(
          impl_.idx_ > 0
          && impl_.idx_ == static_cast<typename iterator_impl::idx_t>(impl_.buf_.size())
        ) {
          using std::u32string;
          impl_.buf_.~u32string();
          impl_.next_chunk(range_->view_);
        }
      } catch(...) {
        impl_.idx_ = -1;
        throw;
      }
    }

    [[nodiscard]] bool equal_to(iterator_core const& other) const
        noexcept(noexcept(std::declval<base_iterator>()
        == std::declval<base_iterator>())) {
      return impl_.idx_ == other.impl_.idx_
        && impl_.base_it_ == other.impl_.base_it_;
    }

    [[nodiscard]] bool equal_to(std::default_sentinel_t) const noexcept {
      return impl_.idx_ == iterator_impl::idx_sentinel;
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  normal_view() = default;

  normal_view(normal_view const& other)
    noexcept(std::is_nothrow_copy_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{other.view_} {}

  normal_view(normal_view&& other)
    noexcept(std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{std::move(other.view_)} {}

  normal_view& operator=(normal_view const& other)
      noexcept(std::is_nothrow_copy_assignable_v<View>)
      /* requires std::is_copy_constructible<View> */ {
    view_ = other.view_;
    cache_.idx_ = iterator_impl::idx_sentinel;
    return *this;
  }

  normal_view& operator=(normal_view&& other)
      noexcept(std::is_nothrow_move_assignable_v<View>) {
    view_ = std::move(other.view_);
    cache_.idx_ = iterator_impl::idx_sentinel;
    other.cache_.idx_ = iterator_impl::idx_sentinel;
    return *this;
  }

  ~normal_view() = default;

  void swap(normal_view& other) noexcept(std::is_nothrow_swappable_v<View>) {
    using std::swap;
    swap(view_, other.view_);
    cache_.idx_ = iterator_impl::idx_sentinel;
    other.cache_.idx_ = iterator_impl::idx_sentinel;
  }

  explicit normal_view(View view)
    noexcept(std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{std::move(view)} {}

  /** @brief Returns an iterator to the start of the range.
   *
   * May also throw whatever is thrown by operations on the underlying view.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] iterator begin() {
    if(cache_.idx_ == iterator_impl::idx_sentinel) {
      cache_.base_it_ = std::ranges::begin(view_);
      cache_.next_chunk(view_);
    }
    iterator it{this, cache_};
    return it;
  }

  [[nodiscard]] std::default_sentinel_t end() noexcept {
    return std::default_sentinel;
  }

  [[nodiscard]] View base() const&
      noexcept(std::is_nothrow_copy_constructible_v<View>)
      requires std::is_copy_constructible_v<View> {
    return view_;
  }

  [[nodiscard]] View base() &&
      noexcept(std::is_nothrow_move_constructible_v<View>) {
    return std::move(view_);
  }
};

template<normal_form_traits Traits, std::ranges::view View>
void swap(
  typename normal_view<Traits, View>::iterator_impl& a,
  typename normal_view<Traits, View>::iterator_impl& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

template<normal_form_traits Traits, std::ranges::view View>
void swap(
  normal_view<Traits, View>& a,
  normal_view<Traits, View>& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

template<std::ranges::view View>
using nfc_view = normal_view<nfc_traits, View>;

template<std::ranges::view View>
using nfd_view = normal_view<nfd_traits, View>;

namespace detail_ {

template<normal_form_traits Traits>
class normal_adaptor:
    public util::range_adaptor_closure<normal_adaptor<Traits>> {
public:
  template<typename Range>
  requires std::ranges::viewable_range<Range&&>
    && std::same_as<std::ranges::range_value_t<Range&&>, char32_t>
  [[nodiscard]] auto operator()(Range&& range) const noexcept(
    std::is_nothrow_move_constructible_v<std::views::all_t<Range&&>>
    && std::is_nothrow_default_constructible_v<std::ranges::iterator_t<
      std::views::all_t<Range&&>&>>
    && noexcept(std::ranges::begin(std::declval<std::views::all_t<Range&&>&>()))
  ) {
    return normal_view<Traits, std::views::all_t<Range&&>>{
      std::views::all(std::forward<Range>(range))};
  }
};

} // detail_

inline constexpr detail_::normal_adaptor<nfc_traits> nfc;
inline constexpr detail_::normal_adaptor<nfd_traits> nfd;

////////////////////////////////////////////////////////////////////////////////

/** @brief Eagerly converts the given string to a normal form.
 *
 * The output string is passed in by reference to add the option of reusing
 * existing capacity. The result is appended to it. If an exception is thrown,
 * the output string may have any valid value.
 *
 * May also throw whatever is thrown by the operations on the input range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<normal_form_traits Traits, typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<std::ranges::range_value_t<Range&&>, char32_t>
void to_normal(Range&& in, std::u32string& out) {
  if(std::ranges::empty(in)) {
    return;
  }

  if constexpr(std::ranges::sized_range<Range&&>) {
    try {
      out.reserve(util::throwing_add(out.size(), std::ranges::size(in)));
    } catch(std::overflow_error const&) {
      std::throw_with_nested(std::bad_alloc{});
    }
  }

  auto chunk_begin{std::ranges::begin(in)};
  auto const in_end{std::ranges::end(in)};
  std::u32string::size_type out_chunk_begin{0};

  for(;;) {
    auto chunk{[&]() {
      if constexpr(std::ranges::forward_range<Range&&>) {
        auto const chunk_end{std::ranges::find_if(std::ranges::subrange{
          std::next(chunk_begin), in_end}, [](char32_t ch) noexcept {
            return detail_::is_stable_codepoint<Traits>(ch);
          })};
        auto const old_chunk_begin{chunk_begin};
        chunk_begin = chunk_end;
        return std::ranges::subrange{old_chunk_begin, chunk_end};
      } else {
        std::u32string chunk;
        chunk.push_back(*chunk_begin);
        ++chunk_begin;
        auto view{std::ranges::subrange{chunk_begin, in_end}
          | std::views::take_while([](char32_t ch) noexcept {
            return !detail_::is_stable_codepoint<Traits>(ch);
          })};
        chunk_begin = std::ranges::copy(view, std::back_inserter(chunk)).in;
        return chunk;
      }
    }()};

    if(is_normal_qc<Traits>(chunk)) {
      std::ranges::copy(chunk, std::back_inserter(out));
    } else {
      for(char32_t const ch: chunk) {
        auto const decomp{detail_::full_decompose(ch)};
        std::ranges::copy(decomp, std::back_inserter(out));
      }
      Traits::cvt_chunk(out, out.begin()
        + static_cast<std::ptrdiff_t>(out_chunk_begin));
    }

    if(chunk_begin == in_end) {
      return;
    }

    out_chunk_begin = out.size();
  }
  util::unreachable();
}

/** @brief Eagerly converts the given string to a normal form.
 *
 * May also throw whatever is thrown by the operations on the input range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<normal_form_traits Traits, typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<char32_t, std::ranges::range_value_t<Range&&>>
[[nodiscard]] std::u32string to_normal(Range&& in) {
  std::u32string out;
  unicode::to_normal<Traits>(std::forward<Range>(in), out);
  return out;
}

/** @brief Eagerly converts the given string to NFC.
 *
 * The output string is passed in by reference to add the option of reusing
 * existing capacity. The result is appended to it. If an exception is thrown,
 * the output string may have any valid value.
 *
 * May also throw whatever is thrown by the operations on the input range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<char32_t, std::ranges::range_value_t<Range&&>>
void to_nfc(Range&& in, std::u32string& out) {
  unicode::to_normal<nfc_traits>(std::forward<Range>(in), out);
}

/** @brief Eagerly converts the given string to NFC.
 *
 * May also throw whatever is thrown by the operations on the input range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<char32_t, std::ranges::range_value_t<Range&&>>
[[nodiscard]] std::u32string to_nfc(Range&& in) {
  return unicode::to_normal<nfc_traits>(std::forward<Range>(in));
}

/** @brief Eagerly converts the given string to NFD.
 *
 * The output string is passed in by reference to add the option of reusing
 * existing capacity. The result is appended to it. If an exception is thrown,
 * the output string may have any valid value.
 *
 * May also throw whatever is thrown by the operations on the input range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<char32_t, std::ranges::range_value_t<Range&&>>
void to_nfd(Range&& in, std::u32string& out) {
  unicode::to_normal<nfd_traits>(std::forward<Range>(in), out);
}

/** @brief Eagerly converts the given string to NFD.
 *
 * May also throw whatever is thrown by the operations on the input range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<char32_t, std::ranges::range_value_t<Range&&>>
[[nodiscard]] std::u32string to_nfd(Range&& in) {
  return unicode::to_normal<nfd_traits>(std::forward<Range>(in));
}

////////////////////////////////////////////////////////////////////////////////

/** @brief Checks whether the given range of characters is in the given normal
 * form.
 *
 * May also throw whatever is thrown by various range operations.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<normal_form_traits Traits, typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<std::ranges::range_value_t<Range&&>, char32_t>
[[nodiscard]] bool is_normal(Range&& range) {
  if constexpr(std::ranges::forward_range<Range&&>) {
    auto const iend{std::ranges::end(range)};
    auto chunk_begin{std::ranges::begin(range)};

    while(chunk_begin != iend) {
      auto chunk_end{std::ranges::find_if(std::next(chunk_begin), iend,
        [](char32_t ch) noexcept {
          return detail_::is_stable_codepoint<Traits>(ch);
        })};
      std::ranges::subrange const chunk{chunk_begin, chunk_end};

      bool slow_test{false};
      normal_quick_checker<Traits> checker;
      for(char32_t const ch: chunk) {
        assert(is_scalar_value(ch));
        switch(checker.feed(ch)) {
          case qc_t::yes:
            break;
          case qc_t::maybe:
            slow_test = true;
            break;
          case qc_t::no:
            return false;
        }
      }

      if(slow_test) {
        if constexpr(Traits::qc_is_conclusive) {
          util::unreachable();
        }
        std::u32string normalized;
        for(char32_t const ch: chunk) {
          auto const decomp{detail_::full_decompose(ch)};
          std::ranges::copy(decomp, std::back_inserter(normalized));
        }
        Traits::cvt_chunk(normalized, normalized.begin());
        if(!std::ranges::equal(chunk, normalized)) {
          return false;
        }
      }

      chunk_begin = chunk_end;
    }

    return true;
  } else {
    auto it{std::ranges::begin(range)};
    auto const iend{std::ranges::end(range)};
    while(it != iend) {
      std::u32string str;
      str.push_back(*it++);
      auto chunk{std::ranges::subrange{it, iend}
      | std::views::take_while([](char32_t ch) noexcept {
        return !detail_::is_stable_codepoint<Traits>(ch);
      })};
      it = std::ranges::copy(chunk, std::back_inserter(str)).in;
      if(!is_normal<Traits>(str)) {
        return false;
      }
    }
    return true;
  }
}

/** @brief Checks whether the given range of characters is in NFC.
 *
 * May also throw whatever is thrown by various range operations.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<std::ranges::range_value_t<Range&&>, char32_t>
[[nodiscard]] bool is_nfc(Range&& range) {
  return is_normal<nfc_traits>(std::forward<Range>(range));
}

/** @brief Checks whether the given range of characters is in NFD.
 *
 * May also throw whatever is thrown by various range operations.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<std::ranges::range_value_t<Range&&>, char32_t>
[[nodiscard]] bool is_nfd(Range&& range) {
  return is_normal<nfd_traits>(std::forward<Range>(range));
}

} // unicode

#endif
