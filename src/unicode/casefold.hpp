#ifndef UNICODE_CASEFOLD_HPP_INCLUDED_EH5QXOQIKOQF2UUMOX9G5SN4T
#define UNICODE_CASEFOLD_HPP_INCLUDED_EH5QXOQIKOQF2UUMOX9G5SN4T

#include "unicode/normal.hpp"
#include "util/iterator_facade.hpp"
#include "util/range_adaptor_closure.hpp"
#include "util/safe_int.hpp"
#include <algorithm>
#include <cassert>
#include <concepts>
#include <cstddef>
#include <iterator>
#include <ranges>
#include <string>
#include <type_traits>
#include <utility>

namespace unicode {

namespace detail_ {

/** @brief The maximum size of the casefolding of a code point. */
constexpr util::size_t max_casefold_size{4};

/** @brief Returns the casefolding of one code point.
 *
 * Casefoldings are stored in a 16-bit trie. If the casefolding is trivial
 * (i. e. it consists of just that one code point), the value is 0. If the
 * casefolding is short, i. e. it consists of one code point in the basic plane
 * that is not U+0000 and not a private-use code point, that code point is the
 * value.
 *
 * In any other case, the casefolding is long. This technically also includes
 * the situation where the casefolding consists of a single code point that is
 * either U+0000 or a private use code point, which cannot be encoded as a short
 * casefolding since these values have other meanings. But these code points do
 * not currently appear in any casefoldings, and most likely never will.
 *
 * For long casefoldings, the actual data is stored in a separate long
 * casefoldings array, encoded in UTF-16. (Since this is not regular text,
 * UTF-16 is expected to be superior to UTF-8.) The trie then stores a surrogate
 * code unit, whose eleven available bits are used as follows: the two least
 * significant bits store the value one less than the codefolding's length; the
 * remaining nine bits store the index into the long casefoldings array to the
 * beginning of the actual casefolding data. (This restricts casefoldings to be
 * no longer than four code points long; this restriction is currently satsified
 * in Unicode 15.0. This also puts a limit on the size of the long casefoldings
 * array; this restriction is also satisfied in Unicode 15.0.) */
[[nodiscard]] vec_char32<max_casefold_size.unwrap()> casefold_one(char32_t) noexcept;

/** @brief Determines whether the given code point is "casefold-crazy".
 *
 * If a string contains casefold-crazy code points, it needs to be normalized to
 * NFD before applying casefolding. If there are no casefold-crazy code points,
 * the string does not need to be normalized.
 *
 * This is computed as the set of all code points whose full decompositions
 * contain U+0345 COMBINING GREEK YPOGEGRAMMENI. */
[[nodiscard]] constexpr bool is_casefold_crazy(char32_t ch) noexcept {
  return ch == 0x0345
    || (0x1F80 <= ch && ch <= 0x1FAF)
    || ch == 0x1FB2
    || ch == 0x1FB3
    || ch == 0x1FB4
    || ch == 0x1FB7
    || ch == 0x1FBC
    || ch == 0x1FC2
    || ch == 0x1FC3
    || ch == 0x1FC4
    || ch == 0x1FC7
    || ch == 0x1FCC
    || ch == 0x1FF2
    || ch == 0x1FF3
    || ch == 0x1FF4
    || ch == 0x1FF7
    || ch == 0x1FFC;
}

/** @brief Obtains the casefolding of one NFD chunk.
 *
 * This function extracts one NFD chunk from the range, adjusts the iterator to
 * point to the beginning of the next chunk (or to the end of the range), and
 * appends to the output string a sequence of code points that is canonically
 * equivalent to the result of normalizing the chunk to NFD and casefolding the
 * result.
 *
 * If the supplied range is empty, does nothing.
 *
 * May also throw whatever is thrown by the operations on the provided iterator
 * and sentinel.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
requires std::same_as<char32_t, std::iter_value_t<Iterator>>
void casefold_next_chunk(Iterator& it, Sentinel iend, std::u32string& out) {
  if(it == iend) {
    return;
  }

  if constexpr(std::forward_iterator<Iterator>) {
    auto const chunk_begin{it};
    bool has_crazy{false};
    bool crazy_is_nfd{true};
    char32_t next_char{*it};
    do {
      if(!has_crazy && is_casefold_crazy(next_char)) {
        has_crazy = true;
        // U+0345 is the only casefold-crazy code point that can be present in
        // NFD. So if we found any other code point, then NFD is rejected
        // without needing a separate quick check.
        crazy_is_nfd = next_char == U'\u0345';
      }

      if(++it == iend) {
        break;
      }
      next_char = *it;
    } while(!is_stable_codepoint<nfd_traits>(next_char));

    std::ranges::subrange const chunk{chunk_begin, it};

    if(has_crazy && (!crazy_is_nfd || !is_nfd_qc(chunk))) {
      std::u32string buf;
      for(char32_t const ch: chunk) {
        std::ranges::copy(full_decompose(ch), std::back_inserter(buf));
      }
      nfd_traits::cvt_chunk(buf, buf.begin());
      for(char32_t const ch: buf) {
        std::ranges::copy(casefold_one(ch), std::back_inserter(out));
      }
    } else {
      for(char32_t const ch: chunk) {
        std::ranges::copy(casefold_one(ch), std::back_inserter(out));
      }
    }
  } else {
    std::u32string buf;
    char32_t next_char{*it};

    auto slow_path{[&]() {
      bool chunk_is_nfd{true};

      // Check if the already-copied part of the chunk is in NFD
      normal_quick_checker<nfd_traits> checker;
      for(char32_t const ch: buf) {
        if(checker.feed(ch) != qc_t::yes) {
          chunk_is_nfd = false;
          break;
        }
      }

      if(chunk_is_nfd) {
        // If necessary, keep copying code points and feeding them into the
        // quick checker until we either reach the end of the chunk or find
        // something that breaks NFD
        do {
          if(checker.feed(next_char) != qc_t::yes) {
            chunk_is_nfd = false;
            break;
          }
          buf.push_back(next_char);
          if(++it == iend) {
            break;
          }
          next_char = *it;
        } while(!is_stable_codepoint<nfd_traits>(next_char));
      }

      if(!chunk_is_nfd) {
        // We need to normalize.
        std::u32string normalized;

        // Fully decompose the already-copied part
        for(char32_t const ch: buf) {
          std::ranges::copy(full_decompose(ch), std::back_inserter(normalized));
        }

        // Fully decompose the rest of the chunk
        do {
          std::ranges::copy(full_decompose(next_char),
            std::back_inserter(normalized));
          if(++it == iend) {
            break;
          }
          next_char = *it;
        } while(!is_stable_codepoint<nfd_traits>(next_char));

        nfd_traits::cvt_chunk(normalized, normalized.begin());
        buf = std::move(normalized);
      }

      for(char32_t const ch: buf) {
        std::ranges::copy(casefold_one(ch), std::back_inserter(out));
      }
    }};

    bool next_is_crazy{is_casefold_crazy(next_char)};
    if(next_is_crazy) {
      slow_path();
      return;
    }

    do {
      buf.push_back(next_char);
      if(++it == iend) {
        break;
      }
      next_char = *it;
      next_is_crazy = is_casefold_crazy(next_char);
    } while(!next_is_crazy && !is_stable_codepoint<nfd_traits>(next_char));

    if(next_is_crazy) {
      slow_path();
      return;
    }

    for(char32_t const ch: buf) {
      std::ranges::copy(casefold_one(ch), std::back_inserter(out));
    }
  }
}

} // detail_

////////////////////////////////////////////////////////////////////////////////

/** @brief A view that casefolds an underlying view of code points.
 *
 * The result is an unspecified string that is canonically equivalent to the
 * result of normalizing the input to NFD, then casefolding the result.
 *
 * According to the Unicode standard, the correct way to case-insensitively
 * compare strings is to normalize them to NFD, casefold the result, then
 * normalize the result to NFD again, then compare the results. The last step
 * can obviously be replaced with NFC, so this API does not force either normal
 * form; instead, client code is expected to normalize the output on its own.
 * This means that the result of this transformation does not have to be equal
 * to the result of the first two steps, but merely canonically equivalent to
 * it; this allows for an optimization where the first NFD conversion is omitted
 * most of the time.
 *
 * Whenever appropriate, iterator operations can throw anything that can be
 * thrown by operations on underlying iterators, or std::length_error.
 *
 * If the underlying view type can be swapped without throwing, then assignment
 * operations of this view and its iterator have strong exception safety
 * guarantees.
 *
 * In all other cases, operations have no exception safety guarantees: after an
 * operation throws an exception, the only valid actions are destroying the
 * object or assigning to it. */
template<std::ranges::view View>
requires std::same_as<char32_t, std::ranges::range_value_t<View>>
class casefold_view: public std::ranges::view_interface<casefold_view<View>> {
private:
  using base_iterator = std::ranges::iterator_t<View>;

  View view_;
  std::ranges::iterator_t<View> it_;
  std::u32string buf_;
  util::size_t buf_idx_;

  class iterator_core {
  private:
    casefold_view* view_;

    friend class casefold_view;
    explicit iterator_core(casefold_view& view) noexcept: view_{&view} {}

  public:
    iterator_core() = default;

    void swap(iterator_core& other) noexcept {
      std::swap(view_, other.view_);
    }

    [[nodiscard]] base_iterator base() const
        noexcept(std::is_nothrow_copy_constructible_v<base_iterator>) {
      return view_->it_;
    }

  protected:
    static constexpr bool is_single_pass{true};

    [[nodiscard]] char32_t dereference() const noexcept {
      assert(view_->buf_idx_ < util::wrap(view_->buf_.size()));
      return view_->buf_[view_->buf_idx_.unwrap()];
    }

    void increment() {
      if(++view_->buf_idx_ == util::wrap(view_->buf_.size())) {
        view_->buf_idx_ = 0;
        view_->buf_.clear();
        detail_::casefold_next_chunk(view_->it_, std::ranges::end(view_->view_),
          view_->buf_);
      }
    }

    [[nodiscard]] bool equal_to(iterator_core other) const noexcept {
      return view_ == other.view_;
    }

    [[nodiscard]] bool equal_to(std::default_sentinel_t) const noexcept {
      return view_->buf_.empty();
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  /** @brief Default-constructs a casefolded view.
   *
   * The resulting view is useless. This constructor only exists to satisfy the
   * view concept. */
  casefold_view() = default;

  /** @brief Copy-constructs a casefolded view.
   *
   * May also throw whatever is thrown by the operations on the underlying view.
   *
   * @throws std::bad_alloc */
  casefold_view(casefold_view const&) = default;

  /** @brief Move-constructs a casefolded view.
   *
   * Exception safety guarantees: none.
   *
   * Move safety guarantees: strong guarantee if the underlying view and its
   * iterator type have a strong guarantee, none otherwise. */
  casefold_view(casefold_view&&) = default;

  /** @brief Copy-assigns a casefolded view.
   *
   * May also throw whatever is thrown by the operations on the underlying view.
   *
   * @throws std::bad_alloc */
  casefold_view& operator=(casefold_view const& other)
      requires std::is_copy_constructible_v<View> {
    casefold_view copy{other};
    this->swap(copy);
    return *this;
  }

  /** @brief Move-assigns a casefolded view.
   *
   * Exception safety guarantees: strong if the swap operation does not throw
   * exceptions, none otherwise. */
  casefold_view& operator=(casefold_view&& other)
      noexcept(std::is_nothrow_move_assignable_v<View>
      || (std::is_nothrow_move_constructible_v<View>
      && std::is_nothrow_swappable_v<View>)) {
    if constexpr(std::is_nothrow_move_assignable_v<View>) {
      view_ = std::move(other.view_);
    } else {
      casefold_view copy{std::move(other)};
      this->swap(copy);
    }
    return *this;
  }

  ~casefold_view() = default;

  /** @brief Swaps the two casefolded views.
   *
   * Exception safety guarantees: none. */
  void swap(casefold_view& other)
      noexcept(std::is_nothrow_swappable_v<View>) {
    using std::swap;
    swap(view_, other.view_);
  }

  /** @brief Creates a new casefolded view. */
  explicit casefold_view(View view)
    noexcept(std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{std::move(view)}, it_{}, buf_{}, buf_idx_{util::size_t{0}} {} // GCC bug 114311

  /** @brief Returns an iterator to the beginning of the range.
   *
   * May also throw whatever is thrown by the operations on the underlying view.
   *
   * @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] iterator begin() {
    assert(buf_.empty());
    assert(buf_idx_ == 0);
    it_ = std::ranges::begin(view_);
    detail_::casefold_next_chunk(it_, std::ranges::end(view_), buf_);

    iterator it{*this};
    return it;
  }

  [[nodiscard]] std::default_sentinel_t end() const noexcept {
    return std::default_sentinel;
  }

  [[nodiscard]] View base() const&
      noexcept(std::is_nothrow_copy_constructible_v<View>)
      requires std::is_copy_constructible_v<View> {
    return view_;
  }

  [[nodiscard]] View base() &&
      noexcept(std::is_nothrow_move_constructible_v<View>) {
    return std::move(view_);
  }
};

template<std::ranges::view View>
requires std::same_as<char32_t, std::ranges::range_value_t<View>>
  && std::ranges::forward_range<View>
class casefold_view<View>: public std::ranges::view_interface<casefold_view<View>> {
private:
  using base_iterator = std::ranges::iterator_t<View>;
  using base_sentinel = std::ranges::sentinel_t<View>;

  View view_;
  base_iterator it_;
  std::u32string buf_;

  class iterator_core {
  private:
    base_iterator it_;
    base_sentinel iend_;
    util::size_t buf_idx_{util::size_t::uninitialized()};
    std::u32string buf_;

    friend class casefold_view;
    explicit iterator_core(casefold_view& view): it_{view.it_},
      iend_{std::ranges::end(view.view_)}, buf_idx_{util::size_t{0}}, // GCC bug 114311
      buf_{view.buf_} {}

  public:
    iterator_core() = default;
    iterator_core(iterator_core const&) = default;
    iterator_core(iterator_core&&) = default;

    iterator_core& operator=(iterator_core const& other) {
      iterator_core copy{other};
      this->swap(copy);
      return *this;
    }

    iterator_core& operator=(iterator_core&& other) noexcept(
      std::is_nothrow_move_assignable_v<base_iterator>
      && std::is_nothrow_move_assignable_v<base_sentinel>
      || std::is_nothrow_move_constructible_v<iterator_core>
      && std::is_nothrow_swappable_v<base_iterator>
      && std::is_nothrow_swappable_v<base_sentinel>
    ) {
      if constexpr(std::is_nothrow_move_assignable_v<base_iterator>
          && std::is_nothrow_move_assignable_v<base_sentinel>) {
        it_ = std::move(other.it_);
        iend_ = std::move(other.iend_);
        buf_idx_ = other.buf_idx_;
        buf_ = std::move(other.buf_);
      } else {
        iterator_core copy{std::move(other)};
        this->swap(copy);
      }
      return *this;
    }

    ~iterator_core() = default;

    void swap(iterator_core& other) noexcept(
      std::is_nothrow_swappable_v<base_iterator>
      && std::is_nothrow_swappable_v<base_sentinel>
    ) {
      using std::swap;
      swap(it_, other.it_);
      swap(iend_, other.iend_);
      swap(buf_idx_, other.buf_idx_);
      swap(buf_, other.buf_);
    }

    [[nodiscard]] base_iterator base() const&
        noexcept(std::is_nothrow_copy_constructible_v<base_iterator>) {
      return it_;
    }

    [[nodiscard]] base_iterator base() &&
        noexcept(std::is_nothrow_move_constructible_v<base_iterator>) {
      return std::move(it_);
    }

  protected:
    [[nodiscard]] char32_t dereference() const noexcept {
      assert(buf_idx_ != util::wrap(buf_.size()));
      return buf_[buf_idx_.unwrap()];
    }

    void increment() {
      if(++buf_idx_ == util::wrap(buf_.size())) {
        buf_idx_ = 0;
        buf_.clear();
        detail_::casefold_next_chunk(it_, iend_, buf_);
      }
    }

    [[nodiscard]] bool equal_to(iterator_core const& other) const
        noexcept(noexcept(it_ == other.it_)) {
      return it_ == other.it_ && buf_idx_ == other.buf_idx_
        && buf_ == other.buf_;
    }

    [[nodiscard]] bool equal_to(std::default_sentinel_t) const noexcept {
      return buf_.empty();
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  /** @brief Default-constructs a casefolded view.
   *
   * The resulting view is useless. This constructor only exists to satisfy the
   * view concept. */
  casefold_view() = default;

  /** @brief Copy-constructs a casefolded view.
   *
   * May also throw whatever is thrown by the operations on the underlying view.
   *
   * @throws std::bad_alloc */
  casefold_view(casefold_view const& other):
    view_{other.view_}, it_{}, buf_{} {}

  /** @brief Move-constructs a casefolded view.
   *
   * Exception safety guarantees: none.
   *
   * Move safety guarantees: strong guarantee if the underlying view has a
   * strong guarantee, none otherwise. */
  casefold_view(casefold_view&& other)
      noexcept(std::is_nothrow_move_constructible_v<View>
      && std::is_nothrow_default_constructible_v<base_iterator>):
      view_{std::move(other.view_)}, it_{}, buf_{} {
    other.buf_.clear();
  }

  /** @brief Copy-assigns a casefolded view.
   *
   * May also throw whatever is thrown by the copy construction of the
   * underlying view or its iterator type.
   *
   * @throws std::bad_alloc */
  casefold_view& operator=(casefold_view const& other)
      /* requires std::is_copy_constructible_v<View> */ {
    casefold_view copy{other};
    this->swap(copy);
    return *this;
  }

  casefold_view& operator=(casefold_view&& other) noexcept(
    std::is_nothrow_move_assignable_v<View>
    || std::is_nothrow_move_constructible_v<casefold_view>
    && std::is_nothrow_swappable_v<View>
  ) {
    if constexpr(std::is_nothrow_move_assignable_v<View>) {
      view_ = std::move(other.view_);
      buf_.clear();
      other.buf_.clear();
    } else {
      casefold_view copy{std::move(other)};
      this->swap(copy);
    }
    return *this;
  }

  ~casefold_view() = default;

  /** @brief Swaps the two casefolded views.
   *
   * Exception safety guarantees: none. */
  void swap(casefold_view& other) noexcept(std::is_nothrow_swappable_v<View>) {
    using std::swap;
    swap(view_, other.view_);
    buf_.clear();
    other.buf_.clear();
  }

  /** @brief Creates a new casefolded view.
   *
   * May also throw whatever is thrown by the operations on the underlying view.
   *
   * @throws std::bad_alloc */
  explicit casefold_view(View view)
    noexcept(std::is_nothrow_move_constructible_v<View>
    && std::is_nothrow_default_constructible_v<base_iterator>):
    view_{std::move(view)}, it_{}, buf_{} {}

  /** @brief Returns an iterator to the beginning of the range.
   *
   * May also throw whatever is thrown by the operations on the underlying view.
   *
   * @throws std::bad_alloc */
  [[nodiscard]] iterator begin() {
    if(buf_.empty()) {
      it_ = std::ranges::begin(view_);
      detail_::casefold_next_chunk(it_, std::ranges::end(view_), buf_);
    }

    iterator it{*this};
    return it;
  }

  [[nodiscard]] std::default_sentinel_t end() const noexcept {
    return std::default_sentinel;
  }

  [[nodiscard]] View base() const&
      noexcept(std::is_nothrow_copy_constructible_v<View>)
      requires std::is_copy_constructible_v<View> {
    return view_;
  }

  [[nodiscard]] View base() &&
      noexcept(std::is_nothrow_move_constructible_v<View>) {
    return std::move(view_);
  }
};

template<typename Range>
casefold_view(Range&&) -> casefold_view<std::views::all_t<Range&&>>;

template<typename View>
void swap(
  typename casefold_view<View>::iterator_core& a,
  typename casefold_view<View>::iterator_core& b
) noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

template<typename View>
void swap(casefold_view<View>& a, casefold_view<View>& b)
    noexcept(std::is_nothrow_swappable_v<View>) {
  a.swap(b);
}

namespace detail_ {

class casefold_adaptor: public util::range_adaptor_closure<casefold_adaptor> {
public:
  template<typename Range>
  requires std::ranges::viewable_range<Range&&>
    && std::same_as<char32_t, std::ranges::range_value_t<Range&&>>
  [[nodiscard]] auto operator()(Range&& range) const noexcept(
    std::is_nothrow_move_constructible_v<std::views::all_t<Range&&>>
    && noexcept(std::ranges::begin(std::declval<std::views::all_t<Range&&>&>()))
  ) {
    return casefold_view<std::views::all_t<Range&&>>{std::views::all(
      std::forward<Range>(range))};
  }
};

} // detail_

inline constexpr detail_::casefold_adaptor casefold;

////////////////////////////////////////////////////////////////////////////////

/** @brief Eagerly casefolds the given string.
 *
 * The output string is passed in by reference to add the option of reusing
 * existing capacity. It is expected to be empty. If an exception is thrown, the
 * output string may have any valid value.
 *
 * May also throw whatever is thrown by the operations on the input range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<char32_t, std::ranges::range_value_t<Range&&>>
void to_casefold(Range&& in, std::u32string& out) {
  assert(out.empty());
  auto it{std::ranges::begin(in)};
  auto const iend{std::ranges::end(in)};
  while(it != iend) {
    detail_::casefold_next_chunk(it, iend, out);
  }
}

/** @brief Eagerly casefolds the given string.
 *
 * May also throw whatever is thrown by the operations on the input range.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<typename Range>
requires std::ranges::input_range<Range&&>
  && std::same_as<char32_t, std::ranges::range_value_t<Range&&>>
[[nodiscard]] std::u32string to_casefold(Range&& in) {
  std::u32string result;
  unicode::to_casefold(std::forward<Range>(in), result);
  return result;
}

} // unicode

#endif
