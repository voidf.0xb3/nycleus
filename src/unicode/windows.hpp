#ifndef UNICODE_WINDOWS_HPP_INCLUDED_BWJFDPOQEXAMRAKUQ7SYFFLBC
#define UNICODE_WINDOWS_HPP_INCLUDED_BWJFDPOQEXAMRAKUQ7SYFFLBC

namespace unicode {

/** @brief Calls the necessary Windows API to enable Unicode output.
 *
 * After this is called, UTF-8 may be sent to the standard output and error
 * streams.
 * @throws std::system_error */
#ifdef _WIN32
  void init_unicode_output();
#else
  inline void init_unicode_output() noexcept {}
#endif

#ifdef _WIN32
  using native_char = wchar_t;
#else
  using native_char = char;
#endif

} // unicode

#endif
