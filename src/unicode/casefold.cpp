#include "unicode/casefold.hpp"
#include "unicode/normal.hpp"
#include "unicode/basic.hpp"
#include "unicode/utrie.hpp"
#include "util/safe_int.hpp"
#include "util/small_vector.hpp"
#include <cassert>
#include <cstddef>
#include <cstdint>

#include "unicode/ucd/casefold.hpp"

unicode::detail_::vec_char32<unicode::detail_::max_casefold_size.unwrap()>
unicode::detail_::casefold_one(char32_t ch) noexcept {
  assert(is_scalar_value(ch));
  util::uint16_t const value{UTRIE(casefold)::get(ch)};

  if(value == 0) {
    return {ch};
  }
  if(value < 0xD800 || value >= 0xF900) {
    return {value.unwrap()};
  }

  auto const casefold_size{((value & 0x3) + 1).cast<util::uint8_t>()};
  vec_char32<max_casefold_size.unwrap()> result;

  auto index{((value - 0xD800) >> 2).cast<util::size_t>()};
  assert(index < ucd::casefold_long_size);

  while(util::wrap(result.size()) < casefold_size) {
    if(is_surrogate(ucd::casefold_long[index.unwrap()])) {
      result.push_back((((ucd::casefold_long[index.unwrap()] & 0x3FF) << 10)
        | (ucd::casefold_long[(index + 1).unwrap()] & 0x3FF)) | 0x10000);
      index += 2;
    } else {
      result.push_back(ucd::casefold_long[index.unwrap()]);
      ++index;
    }
  }
  return result;
}
