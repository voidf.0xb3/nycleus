#include <boost/test/unit_test.hpp>

#include "unicode/normal.hpp"
#include <algorithm>
#include <concepts>
#include <iterator>
#include <ios>
#include <ranges>
#include <sstream>
#include <string>

namespace {

using sp_nfc_view = unicode::nfc_view<
  std::ranges::subrange<std::istreambuf_iterator<char32_t>>>;
using mp_nfc_view = unicode::nfc_view<std::views::all_t<std::u32string&>>;
using sp_nfd_view = unicode::nfd_view<
  std::ranges::subrange<std::istreambuf_iterator<char32_t>>>;
using mp_nfd_view = unicode::nfd_view<std::views::all_t<std::u32string&>>;

static_assert(std::ranges::view<sp_nfc_view>);
static_assert(std::ranges::input_range<sp_nfc_view>);
static_assert(!std::ranges::forward_range<sp_nfc_view>);
static_assert(std::same_as<char32_t, std::ranges::range_value_t<sp_nfc_view>>);

static_assert(std::ranges::view<mp_nfc_view>);
static_assert(std::ranges::forward_range<mp_nfc_view>);
static_assert(!std::ranges::bidirectional_range<mp_nfc_view>);
static_assert(std::same_as<char32_t, std::ranges::range_value_t<mp_nfc_view>>);

static_assert(std::ranges::view<sp_nfd_view>);
static_assert(std::ranges::input_range<sp_nfd_view>);
static_assert(!std::ranges::forward_range<sp_nfd_view>);
static_assert(std::same_as<char32_t, std::ranges::range_value_t<sp_nfd_view>>);

static_assert(std::ranges::view<mp_nfd_view>);
static_assert(std::ranges::forward_range<mp_nfd_view>);
static_assert(!std::ranges::bidirectional_range<mp_nfd_view>);
static_assert(std::same_as<char32_t, std::ranges::range_value_t<mp_nfd_view>>);

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_unicode)
BOOST_AUTO_TEST_SUITE(normal)

BOOST_AUTO_TEST_CASE(canon_decompose) {
  auto const decomp{unicode::detail_::canon_decompose(U'\u01DE')};
  char32_t const values[]{U'\u00C4', U'\u0304'};
  BOOST_TEST(std::ranges::equal(decomp, values));
}

BOOST_AUTO_TEST_CASE(full_decompose) {
  auto const decomp{unicode::detail_::full_decompose(U'\u01DE')};
  char32_t const values[]{U'\u0041', U'\u0308', U'\u0304'};
  BOOST_TEST(std::ranges::equal(decomp, values));
}

////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(nfc)

BOOST_AUTO_TEST_CASE(lazy_input) {
  std::u32string const input{U"test string"};
  std::basic_istringstream<char32_t> input_stream{input};
  input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
  std::u32string output;
  std::ranges::copy(std::ranges::subrange{
    std::istreambuf_iterator<char32_t>{input_stream},
    std::istreambuf_iterator<char32_t>{}
  } | unicode::nfc, std::back_inserter(output));
  BOOST_TEST((input == output));
}

BOOST_AUTO_TEST_CASE(lazy_forward) {
  std::u32string const input{U"test string"};
  std::u32string output;
  std::ranges::copy(input | unicode::nfc, std::back_inserter(output));
  BOOST_TEST((input == output));
}

BOOST_AUTO_TEST_CASE(eager_input) {
  std::u32string const input{U"test string"};
  std::basic_istringstream<char32_t> input_stream{input};
  input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
  std::u32string const output{unicode::to_nfc(std::ranges::subrange{
    std::istreambuf_iterator<char32_t>{input_stream},
    std::istreambuf_iterator<char32_t>{}
  })};
  BOOST_TEST((input == output));
}

BOOST_AUTO_TEST_CASE(eager_forward) {
  std::u32string const input{U"test string"};
  std::u32string const output{unicode::to_nfc(input)};
  BOOST_TEST((input == output));
}

BOOST_AUTO_TEST_CASE(check_input) {
  std::u32string const input{U"test string"};
  std::basic_istringstream<char32_t> input_stream{input};
  input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
  BOOST_TEST(unicode::is_nfc(std::ranges::subrange{
    std::istreambuf_iterator<char32_t>{input_stream},
    std::istreambuf_iterator<char32_t>{}
  }));
}

BOOST_AUTO_TEST_CASE(check_forward) {
  std::u32string const input{U"test string"};
  BOOST_TEST(unicode::is_nfc(input));
}

BOOST_AUTO_TEST_SUITE_END() // nfc

////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(nfd)

BOOST_AUTO_TEST_CASE(lazy_input) {
  std::u32string const input{U"test string"};
  std::basic_istringstream<char32_t> input_stream{input};
  input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
  std::u32string output;
  std::ranges::copy(std::ranges::subrange{
    std::istreambuf_iterator<char32_t>{input_stream},
    std::istreambuf_iterator<char32_t>{}
  } | unicode::nfd, std::back_inserter(output));
  BOOST_TEST((input == output));
}

BOOST_AUTO_TEST_CASE(lazy_forward) {
  std::u32string const input{U"test string"};
  std::u32string output;
  std::ranges::copy(input | unicode::nfd, std::back_inserter(output));
  BOOST_TEST((input == output));
}

BOOST_AUTO_TEST_CASE(eager_input) {
  std::u32string const input{U"test string"};
  std::basic_istringstream<char32_t> input_stream{input};
  input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
  std::u32string const output{unicode::to_nfd(std::ranges::subrange{
    std::istreambuf_iterator<char32_t>{input_stream},
    std::istreambuf_iterator<char32_t>{}
  })};
  BOOST_TEST((input == output));
}

BOOST_AUTO_TEST_CASE(eager_forward) {
  std::u32string const input{U"test string"};
  std::u32string const output{unicode::to_nfd(input)};
  BOOST_TEST((input == output));
}

BOOST_AUTO_TEST_CASE(check_input) {
  std::u32string const input{U"test string"};
  std::basic_istringstream<char32_t> input_stream{input};
  input_stream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
  BOOST_TEST(unicode::is_nfd(std::ranges::subrange{
    std::istreambuf_iterator<char32_t>{input_stream},
    std::istreambuf_iterator<char32_t>{}
  }));
}

BOOST_AUTO_TEST_CASE(check_forward) {
  std::u32string const input{U"test string"};
  BOOST_TEST(unicode::is_nfd(input));
}

BOOST_AUTO_TEST_SUITE_END() // nfd

// There are more normalization tests in UCD

BOOST_AUTO_TEST_SUITE_END() // normal
BOOST_AUTO_TEST_SUITE_END() // test_unicode
