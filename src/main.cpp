#include "nycleus/target.hpp"
#include "nycleus_llvm/codegen.hpp"
#include "nycleus_llvm/target.hpp"
#include "options/options.hpp"
#include "unicode/encoding.hpp"
#include "unicode/windows.hpp"
#include "util/format.hpp"
#include "util/pp_seq.hpp"
#include "util/u8compat.hpp"
#include "util/util.hpp"
#include "diagnostic_print.hpp"
#include "driver.hpp"
#include "nycleus_targets.hpp"
#include "opt.hpp"
#include <gsl/gsl>
#include <llvm/MC/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/TargetParser/Host.h>
#include <cassert>
#include <exception>
#include <filesystem>
#include <ios>
#include <iostream>
#include <memory>
#include <new>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <system_error>
#include <utility>

#ifdef _WIN32
  #include "util/ranges.hpp"
  #include <windows.h>
  #include <shellapi.h>
#endif

using namespace util::u8compat_literals;

#ifdef _WIN32

  #include <io.h>
  #include <stdio.h>

  namespace {

  bool is_stderr_tty() noexcept {
    return _isatty(_fileno(stderr));
  }

  } // (anonymous)

#else

  #if __has_include(<unistd.h>)
    #include <unistd.h>
    #ifdef _POSIX_VERSION
      #define SYSTEM_IS_POSIX
    #endif
  #endif

  #ifdef SYSTEM_IS_POSIX

    // NOLINTNEXTLINE(modernize-deprecated-headers)
    #include <stdio.h>

    namespace {

    bool is_stderr_tty() noexcept {
      return isatty(fileno(stderr)) != 0;
    }

    } // (anonymous)

  #else

    namespace {

    bool is_stderr_tty() noexcept {
      return false;
    }

    } // (anonymous)

  #endif

#endif

namespace {

/** @throws std::ios::failure */
void list_targets() {
  unicode::init_unicode_output();
  #define STRINGIZE(x) #x
  #define LIST_TARGET(target) std::cout << u8"" STRINGIZE(target) "\n"_ch;
  PP_SEQ_FOR_EACH(NYCLEUS_TARGETS, LIST_TARGET)
  #undef LIST_TARGET
}

/** @brief Thrown if an invalid target triple is specified.
 *
 * Wraps the error message supplied by LLVM. */
class bad_target_triple final: public options::error {
public:
  /** @param triple The invalid target triple.
   * @param error The error message supplied by LLVM.
   * @throws std::bad_alloc
   * @throws std::length_error */
  explicit bad_target_triple(
    std::string_view triple,
    std::string_view error
  ): options::error{util::format<u8"invalid target triple \"{}\": {}">(
    std::u8string_view{options::encoding_error::fix_string(triple)},
    std::u8string_view{options::encoding_error::fix_string(error)})} {}
};

/** @param program_name The name of the program (for printing help)
 * @returns Whether the program can continue
 * @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::ios_base::failure
 * @throws options::error
 * @throws nycleus::target_error */
[[nodiscard]] bool handle_options(
  options_config_t const& opts,
  std::unique_ptr<diag_message_printer>& diag_printer,
  std::unique_ptr<nycleus_llvm::target>& tgt,
  std::basic_string_view<unicode::native_char> program_name
) {
  if(opts.get<opt::help>()) {
    print_help(program_name);
    return false;
  }

  if(opts.get<opt::list_targets>()) {
    list_targets();
    return false;
  }

  if(auto const jobs{opts.get<opt::jobs>()}; jobs && *jobs == 0) {
    throw options::error{u8"number of jobs cannot be zero"};
  }

  if(opts.get_unnamed().empty()) {
    throw options::error{u8"no inputs specified"};
  }

  if(!opts.get<opt::temp_dir>().has_value()) {
    throw options::error{u8"no outputs specified"};
  }

  // Create target
  try {
    std::string const target_triple{[&]() -> std::string {
      if(auto const& target_opt{opts.get<opt::target>()}) {
        #ifdef _WIN32
          try {
            return *target_opt | unicode::utf16_decode(unicode::eh_throw{})
              | unicode::utf8_encode<char>() | util::range_to<std::string>();
          } catch(unicode::convert_exception const&) {
            std::throw_with_nested(options::encoding_error{*target_opt});
          }
        #else
          return *target_opt;
        #endif
      } else {
        return llvm::sys::getDefaultTargetTriple();
      }
    }()};

    #define INIT_TARGET(target) INIT_TARGET_CALL(target)

    #define INIT_TARGET_CALL(x) LLVMInitialize##x##TargetInfo();
    PP_SEQ_FOR_EACH(NYCLEUS_TARGETS, INIT_TARGET)
    #undef INIT_TARGET_CALL

    #define INIT_TARGET_CALL(x) LLVMInitialize##x##Target();
    PP_SEQ_FOR_EACH(NYCLEUS_TARGETS, INIT_TARGET)
    #undef INIT_TARGET_CALL

    #define INIT_TARGET_CALL(x) LLVMInitialize##x##TargetMC();
    PP_SEQ_FOR_EACH(NYCLEUS_TARGETS, INIT_TARGET)
    #undef INIT_TARGET_CALL

    #undef INIT_TARGET

    auto const& target{[&]() -> llvm::Target const& {
      std::string error;
      auto const* const p_target{
        llvm::TargetRegistry::lookupTarget(target_triple, error)};
      if(!p_target) {
        throw bad_target_triple{target_triple, error};
      }
      return *p_target;
    }()};

    std::unique_ptr<llvm::TargetMachine> target_machine{
      target.createTargetMachine(
        target_triple,
        "",
        "",
        llvm::TargetOptions{},
        std::nullopt
      )};
    assert(target_machine);

    tgt = std::make_unique<nycleus_llvm::target>(std::move(target_machine));
  } catch(std::bad_alloc const&) {
    throw;
  } catch(std::length_error const&) {
    throw;
  } catch(options::error const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(nycleus::target_error{});
  }

  // Create a diagnostic printer
  switch(opts.get<opt::diagnostic_format>()) {
    case opt::diagnostic_format::human: {
      bool color_diagnostics;
      switch(opts.get<opt::color_diagnostics>()) {
        case opt::color_diagnostics::on:
          color_diagnostics = true;
          break;

        case opt::color_diagnostics::off:
          color_diagnostics = false;
          break;

        case opt::color_diagnostics::autodetect:
          color_diagnostics = is_stderr_tty();
          break;

        default:
          util::unreachable();
      }
      diag_printer = std::make_unique<diag_message_handler_human>(
        color_diagnostics);
      break;
    }

    case opt::diagnostic_format::machine:
      throw options::error{u8"the value \"machine\" is not supported for "
        u8"the \"diagnostic-format\" option"};

    case opt::diagnostic_format::json:
      diag_printer = std::make_unique<diag_message_handler_json>();
      break;

    case opt::diagnostic_format::json_raw:
      throw options::error{u8"the value \"json-raw\" is not supported for "
        u8"the \"diagnostic-format\" option"};
  }

  return true;
}

} // (anonymous)

int main(
  #ifndef _WIN32
    int argc, gsl::zstring* argv
  #endif
) {
  try {
    std::cout.exceptions(
      std::ios_base::failbit | std::ios_base::badbit);
    std::cerr.exceptions(
      std::ios_base::failbit | std::ios_base::badbit);

    try {
      #ifdef _WIN32
        {
          DWORD mode;
          HANDLE const console_handle{GetStdHandle(STD_OUTPUT_HANDLE)};
          if(console_handle == NULL || console_handle == INVALID_HANDLE_VALUE) {
            throw std::system_error{static_cast<int>(GetLastError()),
              std::system_category()};
          }
          if(!GetConsoleMode(console_handle, &mode)) {
            throw std::system_error{static_cast<int>(GetLastError()),
              std::system_category()};
          }
          mode |= ENABLE_PROCESSED_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING;
          if(!SetConsoleMode(console_handle, mode)) {
            throw std::system_error{static_cast<int>(GetLastError()),
              std::system_category()};
          }
        }

        struct local_free_deleter {
          void operator()(void* ptr) const noexcept {
            LocalFree(ptr);
          }
        };

        int argc;
        std::unique_ptr<gsl::wzstring, local_free_deleter> args{
          CommandLineToArgvW(GetCommandLineW(), &argc)};
        if(!args) {
          throw std::system_error{static_cast<int>(GetLastError()),
            std::system_category()};
        }
        gsl::cwzstring const* const argv{args.get()};
      #endif

      options_config_t const opts{argc, argv};

      std::unique_ptr<diag_message_printer> diag_printer;
      std::unique_ptr<nycleus_llvm::target> tgt;
      if(!handle_options(opts, diag_printer, tgt, argv[0])) {
        return 0;
      }

      bool const success{run_compiler(opts, *diag_printer, *tgt)};
      return success ? 0 : 1;
    } catch(std::bad_alloc const&) {
      unicode::init_unicode_output();
      std::cerr << u8"not enough memory\n"_ch;
      return 2;
    } catch(std::length_error const&) {
      unicode::init_unicode_output();
      std::cerr << u8"not enough memory\n"_ch;
      return 2;
    } catch(std::bad_exception const&) {
      unicode::init_unicode_output();
      // std::bad_exception is thrown by stackful coroutine mechanisms if memory
      // cannot be allocated for some other exception while it is being
      // propagated along the stack
      std::cerr << u8"not enough memory\n"_ch;
      return 2;
    } catch(options::error const& e) {
      unicode::init_unicode_output();
      std::cerr << util::as_char(e.message()) << u8"\nuse the --help option "
        u8"for information about command-line options\n"_ch;
      return 2;
    } catch(unicode::convert_exception const&) {
      unicode::init_unicode_output();
      std::cerr << u8"incorrect UTF-8 encoding detected\n"_ch;
      return 2;
    } catch(std::filesystem::filesystem_error const& e) {
      unicode::init_unicode_output();
      std::cerr << u8"I/O error\n"_ch;
      return 2;
    } catch(std::ios_base::failure const& e) {
      unicode::init_unicode_output();
      std::cerr << u8"I/O error\n"_ch;
      return 2;
    } catch(nycleus::target_error const& e) {
      unicode::init_unicode_output();
      std::cerr << u8"internal error\n"_ch;
      return 2;
    } catch(nycleus_llvm::codegen_error const& e) {
      unicode::init_unicode_output();
      std::cerr << u8"internal error\n"_ch;
      return 2;
    } catch(std::system_error const& e) {
      // TODO: Print system_error message
      std::cerr << u8"error\n"_ch;
      return 2;
    }

    return 0;
  } catch(std::exception const& e) {
    unicode::init_unicode_output();
    std::cerr.exceptions(std::ios_base::goodbit);
    std::cerr << u8"internal error\n"_ch;
    return 2;
  } catch(...) {
    unicode::init_unicode_output();
    std::cerr.exceptions(std::ios_base::goodbit);
    std::cerr << u8"internal error\n"_ch;
    return 2;
  }
}
