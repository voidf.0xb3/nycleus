#ifndef UTIL_GUARDED_VAR_HPP_INCLUDED_71JQA7N5XNRDGYN2BU5TD1JJF
#define UTIL_GUARDED_VAR_HPP_INCLUDED_71JQA7N5XNRDGYN2BU5TD1JJF

#include "util/non_null_ptr.hpp"
#include <mutex>
#include <type_traits>
#include <utility>

namespace util {

/** @brief A variable guarded by a mutex.
 *
 * This ensures that the variable can only be accessed while the lock is held by
 * only allowing access via the lock guard objects. */
template<typename T>
class guarded_var {
  static_assert(std::is_destructible_v<T>);

private:
  T value_;
  mutable std::mutex mutex_;

public:
  /** @brief Constructs the guarded variable.
   *
   * Can throw whatever is thrown by the construction of the underlying object.
   */
  template<typename... Args>
  requires std::is_constructible_v<T, Args&&...>
  explicit guarded_var(Args&&... args)
    noexcept(std::is_nothrow_constructible_v<T, Args&&...>):
    value_{std::forward<Args>(args)...} {}

  guarded_var(guarded_var const&) = delete;
  guarded_var& operator=(guarded_var const&) = delete;
  ~guarded_var() = default;

  class guard {
  private:
    non_null_ptr<T> value_;
    std::unique_lock<std::mutex> guard_;

    friend class guarded_var;

    explicit guard(T& value, std::mutex& m) noexcept:
      value_{&value}, guard_{m} {}

  public:
    guard(guard const&) = delete;
    guard(guard&&) noexcept = default;
    guard& operator=(guard const&) = delete;
    guard& operator=(guard&&) noexcept = default;
    ~guard() noexcept = default;

    /** @brief Retrieves the underlying lock guard object.
     *
     * Care must be taken when working directly with the lock guard to avoid
     * unguarded access to the variable. */
    std::unique_lock<std::mutex>& get_guard() noexcept {
      return guard_;
    }

    [[nodiscard]] T& operator*() const noexcept {
      return *value_;
    }

    [[nodiscard]] T* operator->() const noexcept {
      return value_.get();
    }
  };

  [[nodiscard]] guard lock() noexcept {
    return guard{value_, mutex_};
  }

  class unlocked_guard {
  private:
    non_null_ptr<T> value_;

    friend class guarded_var;

    explicit unlocked_guard(T& value) noexcept: value_{&value} {}

  public:
    unlocked_guard(unlocked_guard const&) = delete;
    unlocked_guard(unlocked_guard&&) noexcept = default;
    unlocked_guard& operator=(unlocked_guard const&) = delete;
    unlocked_guard& operator=(unlocked_guard&&) noexcept = default;
    ~unlocked_guard() noexcept = default;

    [[nodiscard]] T& operator*() const noexcept {
      return *value_;
    }

    [[nodiscard]] T* operator->() const noexcept {
      return value_.get();
    }
  };

  /** @brief Allows access to the value while the mutex is not held.
   *
   * This is not safe and should be used with extreme care! */
  [[nodiscard]] unlocked_guard unlocked() noexcept {
    return unlocked_guard{value_};
  }
};

} // util

#endif
