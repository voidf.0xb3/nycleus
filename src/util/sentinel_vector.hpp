#ifndef UTIL_SENTINEL_VECTOR_HPP_INCLUDED_TUDDBPFJBOOXSNHCDBR4GP4HA
#define UTIL_SENTINEL_VECTOR_HPP_INCLUDED_TUDDBPFJBOOXSNHCDBR4GP4HA

#include "util/inplace_vector.hpp"
#include "util/iterator.hpp"
#include "util/overflow.hpp"
#include "util/repeat_view.hpp"
#include "util/u8compat.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <cassert>
#include <compare>
#include <concepts>
#include <cstddef>
#include <initializer_list>
#include <iterator>
#include <limits>
#include <ranges>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

namespace util {

/** @brief A tag for unchecked range operations. */
struct unchecked_t {};
inline constexpr unchecked_t unchecked{};

/** @brief An exception thrown whenever the maximum size of a sentinel_vector is
 * exceeded. */
class sentinel_vector_length_error: public std::runtime_error {
public:
  /** @throws std::bad_alloc
   * @throws std::length_error */
  sentinel_vector_length_error();
};

/** @brief A statically allocated vector whose size is implicitly determined by
 * filling the "empty" tail with special sentinel values.
 *
 * Since the sentinel value is used to indicate the end of the vector, no
 * element can have a value equal to the sentinel.
 *
 * Since the sentinel is passed as a non-type template parameter, the element
 * type must be a valid type for such template parameter. The set of allowed
 * types is very small in C++17, but somewhat expanded in C++20.
 *
 * Exception safety guarantees: unless otherwise specified, every member
 * function has a weak exception safety guarantee, except that some elements
 * can be in a moved-from state, and if the exception was thrown by some
 * function provided by the element type, then the states of the element(s)
 * affected by that function depend on that function's exception safety
 * guarantees.
 *
 * Move safety guarantees: the moved-from vector will be in a valid but
 * unspecified state, except that some elements may be in the moved-from
 * state. */
template<std::equality_comparable T, std::size_t N, T S>
requires std::is_copy_constructible_v<T>
  && std::is_copy_assignable_v<T>
class sentinel_vector {
  static_assert(N > 0, "util::sentinel_vector cannot have capacity 0");
  static_assert(N < std::numeric_limits<std::ptrdiff_t>::max(),
    "util::sentinel_vector's capacity too large");

public:
  using value_type = T;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using reference = T&;
  using const_reference = T const&;
  using pointer = T*;
  using const_pointer = T const*;

  static constexpr std::size_t capacity{N};

private:
  T buf_[capacity];

  // Construction & destruction ////////////////////////////////////////////////
  template<std::size_t... Is>
  explicit sentinel_vector(std::index_sequence<Is...>)
    noexcept(std::is_nothrow_copy_constructible_v<T>):
    buf_{(static_cast<void>(Is), S)...} {}

public:
  /** @brief Creates an empty vector. */
  sentinel_vector() noexcept(std::is_nothrow_copy_constructible_v<T>):
    sentinel_vector{std::make_index_sequence<N>{}} {}

private:
  template<std::size_t... Is>
  requires std::is_default_constructible_v<T>
  explicit sentinel_vector(size_type count, std::index_sequence<Is...>)
    noexcept(std::is_nothrow_copy_constructible_v<T>
    && std::is_nothrow_default_constructible_v<T>):
    buf_{(static_cast<void>(Is), Is < count ? T{} : S)...} {}

public:
  /** @brief Creates a vector with the given number of default-constructed
   * objects. */
  explicit sentinel_vector(size_type count)
    noexcept(std::is_nothrow_copy_constructible_v<T>
    && std::is_nothrow_default_constructible_v<T>)
    requires std::is_default_constructible_v<T>:
    sentinel_vector{count, std::make_index_sequence<N>{}} {}

private:
  template<bool Checked, std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires std::is_constructible_v<T, std::iter_reference_t<Iterator>&&>
  [[nodiscard]] static T construct_from_iterator_helper(
    Iterator& it,
    Sentinel iend
  ) noexcept(
    (!Checked || !std::sized_sentinel_for<Sentinel, Iterator>)
    && noexcept(it == iend) && noexcept(*it++)
    && std::is_nothrow_constructible_v<T,
    std::iter_reference_t<Iterator>&&>
    && std::is_nothrow_copy_constructible_v<T>
  ) {
    if constexpr(Checked && std::sized_sentinel_for<Sentinel, Iterator>) {
      if(std::ranges::subrange{it, iend}.size() > N) {
        throw sentinel_vector_length_error{};
      }
    }

    if(it == iend) {
      return S;
    } else {
      T t{*it++};
      assert_if_noexcept(t != S);
      return t;
    }
  }

  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel, std::size_t... Is, bool Checked>
  requires std::is_constructible_v<T, std::iter_reference_t<Iterator>&&>
  explicit sentinel_vector(
    Iterator it,
    Sentinel iend,
    std::index_sequence<Is...>,
    std::bool_constant<Checked>
  ) noexcept(
    !Checked && noexcept(it == iend) && noexcept(*it++)
    && std::is_nothrow_constructible_v<T, std::iter_reference_t<Iterator>&&>
    && std::is_nothrow_copy_constructible_v<T>
    && std::is_nothrow_copy_constructible_v<Sentinel>
  ):
    buf_{(static_cast<void>(Is),
      construct_from_iterator_helper<Checked && Is == 0>(it, iend))...}
  {
    if constexpr(Checked) {
      if constexpr(!std::sized_sentinel_for<Sentinel, Iterator>) {
        if(it != iend) {
          throw sentinel_vector_length_error{};
        }
      }
    } else {
      assert_if_noexcept(it == iend);
    }
  }

public:
  /** @brief Constructs a vector containing the elements from the given range.
   *
   * May also throw:
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by copy-construction of elements.
   * - whatever is thrown by the construction of elements from the result of
   *   dereferencing an iterator.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws sentinel_vector_length_error */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires std::is_constructible_v<T, std::iter_reference_t<Iterator>&&>
  explicit sentinel_vector(Iterator it, Sentinel iend):
    sentinel_vector{std::move(it), std::move(iend),
    std::make_index_sequence<N>{}, std::true_type{}} {}

  /** @brief Constructs a vector containing the elements from the given range.
   *
   * If the range does not fit into the vector, the behavior is undefined. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires std::is_constructible_v<T, std::iter_reference_t<Iterator>&&>
  explicit sentinel_vector(Iterator it, Sentinel iend, unchecked_t)
    noexcept(noexcept(it == iend) && noexcept(*it++)
    && std::is_nothrow_constructible_v<T,
    std::iter_reference_t<Iterator>&&>
    && std::is_nothrow_copy_constructible_v<T>
    && std::is_nothrow_copy_constructible_v<Sentinel>):
    sentinel_vector{std::move(it), std::move(iend),
    std::make_index_sequence<N>{}, std::false_type{}} {}

private:
  struct repeat_tag {};

  explicit sentinel_vector(repeat_tag, util::repeat_view<T, size_type> view)
    noexcept(std::is_nothrow_copy_constructible_v<T>):
    sentinel_vector{view.begin(), view.end(), unchecked} {}

public:
  /** @brief Constructs a vector containing the given number of elements constructed as copies
   * of the given one. */
  explicit sentinel_vector(size_type count, T const& t)
  noexcept(std::is_nothrow_copy_constructible_v<T>):
    sentinel_vector{repeat_tag{}, util::repeat(t, count)}
  {
    assert(count <= N);
  }

  /** @brief Constructs a vector from the given initializer list. */
  sentinel_vector(std::initializer_list<T> il)
      noexcept(std::is_nothrow_copy_constructible_v<T>):
      sentinel_vector{il.begin(), il.end(), unchecked} {
    assert(il.size() <= N);
  }

  sentinel_vector(sentinel_vector const&) = default;
  sentinel_vector(sentinel_vector&&) = default;
  ~sentinel_vector() = default;

  // Assignment ////////////////////////////////////////////////////////////////

private:
  template<bool Checked, std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void assign_impl(Iterator it, Sentinel iend) noexcept(!Checked
      && noexcept(std::declval<T const&>() != S) && noexcept(it != iend)
      && noexcept(*it++) && std::is_nothrow_copy_assignable_v<T>
      && std::is_nothrow_assignable_v<T&,
      std::iter_reference_t<Iterator>&&>) {
    if constexpr(Checked && std::sized_sentinel_for<Sentinel, Iterator>) {
      if(std::ranges::subrange{it, iend}.size() > N) {
        throw sentinel_vector_length_error{};
      }
    }

    // Assign the range
    T* p{buf_};
    while(it != iend) {
      if constexpr(Checked && !std::sized_sentinel_for<Sentinel, Iterator>) {
        if(p - buf_ >= N) {
          throw sentinel_vector_length_error{};
        }
      }
      *p++ = *it++;
    }

    // Erase the end
    this->erase_after(p);
  }

public:
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void assign(Iterator it, Sentinel iend) {
    this->assign_impl<true>(std::move(it), std::move(iend));
  }

  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void assign(Iterator it, Sentinel iend, unchecked_t)
      noexcept(noexcept(std::declval<T const&>() != S) && noexcept(it != iend)
      && noexcept(*it++) && std::is_nothrow_copy_assignable_v<T>
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>) {
    this->assign_impl<false>(std::move(it), std::move(iend));
  }

  void assign(size_type count, T const& t)
      noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(count <= N);
    auto const view{util::repeat(t, count)};
    this->assign(view.begin(), view.end(), unchecked);
  }

  void assign(std::initializer_list<T> il)
      noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(il.size() <= N);
    this->assign(il.begin(), il.end(), unchecked);
  }

  sentinel_vector& operator=(sentinel_vector const& other)
      noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    if(this == &other) {
      return *this;
    }
    this->assign(other.cbegin(), other.cend(), unchecked);
    return *this;
  }

  sentinel_vector& operator=(sentinel_vector&& other)
      noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>
      && std::is_nothrow_move_assignable_v<T>) {
    this->assign(std::move_iterator{other.cbegin()},
      std::move_sentinel{other.cend()}, unchecked);
    return *this;
  }

  // Swap //////////////////////////////////////////////////////////////////////

  void swap(sentinel_vector& other) noexcept(std::is_nothrow_swappable_v<T>) {
    for(std::size_t i{0}; i < N; ++i) {
      using std::swap;
      swap(buf_[i], other.buf_[i]);
    }
  }

  // Element access ////////////////////////////////////////////////////////////

  /** @brief Accesses the element at the given index.
   *
   * @throws std::out_of_range If the index is out of range. */
  [[nodiscard]] T& at(size_type index) {
    if(index >= N) {
      throw std::out_of_range{"sentinel_vector::at: index out of range"};
    }
    return operator[](index);
  }

  /** @brief Accesses the element at the given index.
   *
   * @throws std::out_of_range If the index is out of range. */
  [[nodiscard]] T const& at(size_type index) const {
    if(index >= N) {
      throw std::out_of_range{"sentinel_vector::at: index out of range"};
    }
    return operator[](index);
  }

  /** @brief Accesses the element at the given index.
   *
   * If the index is out of range, the behavior is undefined. */
  [[nodiscard]] T& operator[](size_type index) noexcept {
    assert(index < N);
    return buf_[index];
  }

  /** @brief Accesses the element at the given index.
   *
   * If the index is out of range, the behavior is undefined. */
  [[nodiscard]] T const& operator[](size_type index) const noexcept {
    assert(index < N);
    return buf_[index];
  }

  /** @brief Accesses the first element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] T& front() noexcept {
    assert(!empty());
    return buf_[0];
  }

  /** @brief Accesses the first element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] T const& front() const noexcept {
    assert(!empty());
    return buf_[0];
  }

  /** @brief Accesses the last element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] T& back() noexcept {
    assert(!empty());
    return buf_[size() - 1];
  }

  /** @brief Accesses the last element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] T const& back() const noexcept {
    assert(!empty());
    return buf_[size() - 1];
  }

  // Iterators /////////////////////////////////////////////////////////////////

  using iterator = T*;
  using const_iterator = T const*;

  [[nodiscard]] iterator begin() noexcept { return buf_; }
  [[nodiscard]] const_iterator begin() const noexcept { return buf_; }
  [[nodiscard]] const_iterator cbegin() const noexcept { return begin(); }

private:
  template<bool IsConst>
  class base_sentinel {
  private:
    using vec_t
      = std::conditional_t<IsConst, sentinel_vector const, sentinel_vector>;

    vec_t* vec_;

    friend class sentinel_vector;
    explicit base_sentinel(vec_t* vec) noexcept: vec_{vec} {}

  public:
    base_sentinel() = default;

    [[nodiscard]] bool operator==(T const* other) const
        noexcept(noexcept(std::declval<T const&>() == S)) {
      return other == vec_->buf_ + N || *other == S;
    }

    [[nodiscard]] friend bool operator==(T const* a, base_sentinel b)
        noexcept(noexcept(std::declval<T const&>() == S)) {
      return b == a;
    }

    [[nodiscard]] bool operator==(T* other) const
        noexcept(noexcept(std::declval<T const&>() == S)) {
      return other == vec_->buf_ + N || *other == S;
    }

    [[nodiscard]] friend bool operator==(T* a, base_sentinel b)
        noexcept(noexcept(std::declval<T const&>() == S)) {
      return b == a;
    }

    [[nodiscard]] operator std::conditional_t<IsConst, T const*, T*>()
        noexcept(noexcept(std::declval<T const&>() == S)) {
      return std::find(vec_->buf_, vec_->buf_ + N, S);
    }
  };

public:
  using sentinel = base_sentinel<false>;
  using const_sentinel = base_sentinel<true>;

  [[nodiscard]] sentinel end() noexcept { return sentinel{this}; }
  [[nodiscard]] const_sentinel end() const noexcept {
    return const_sentinel{this};
  }
  [[nodiscard]] const_sentinel cend() const noexcept { return end(); }

  [[nodiscard]] iterator end_common()
      noexcept(noexcept(std::declval<T const&>() == S)) {
    return end();
  }
  [[nodiscard]] const_iterator end_common() const
      noexcept(noexcept(std::declval<T const&>() == S)) {
    return end();
  }
  [[nodiscard]] const_iterator cend_common() const
      noexcept(noexcept(std::declval<T const&>() == S)) {
    return cend();
  }

  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  [[nodiscard]] reverse_iterator rbegin()
      noexcept(noexcept(std::declval<T const&>() == S)) {
    return reverse_iterator{end_common()};
  }
  [[nodiscard]] const_reverse_iterator rbegin() const
      noexcept(noexcept(std::declval<T const&>() == S)) {
    return const_reverse_iterator{end_common()};
  }
  [[nodiscard]] const_reverse_iterator crbegin() const
      noexcept(noexcept(std::declval<T const&>() == S)) {
    return rbegin();
  }

  [[nodiscard]] reverse_iterator rend() noexcept {
    return reverse_iterator{begin()};
  }
  [[nodiscard]] const_reverse_iterator rend() const noexcept {
    return const_reverse_iterator{begin()};
  }
  [[nodiscard]] const_reverse_iterator crend() const noexcept { return rend(); }

  // Size //////////////////////////////////////////////////////////////////////

  [[nodiscard]] std::size_t size() const noexcept(noexcept(std::declval<T const&>() == S)) {
    return util::asserted_cast<std::size_t>(cend_common() - cbegin());
  }

  [[nodiscard]] bool empty() const noexcept(noexcept(buf_[0] == S)) {
    return buf_[0] == S;
  }

  [[nodiscard]] bool full() const noexcept(noexcept(buf_[N - 1] != S)) {
    return buf_[N - 1] != S;
  }

  // Emplacement ///////////////////////////////////////////////////////////////

  /** @brief Emplaces an element immediately before the given one.
   *
   * The element type must be move-constructible and move-assignable.
   *
   * Exception safety guarantees: If the new element's constructor throws,
   * strong guarantee. Otherwise, the same guarantee as for other member
   * functions.
   *
   * @returns An iterator to the newly emplaced element. */
  template<typename... Args>
  requires std::is_constructible_v<T, Args&&...>
  iterator emplace(const_iterator pos_it, Args&&... args)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_constructible_v<T, Args&&...>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_destructible_v<T>) {
    assert(buf_ <= pos_it);
    assert(pos_it <= buf_ + N);
    assert_if_noexcept(!full());

    std::size_t const pos{util::asserted_cast<std::size_t>(pos_it - buf_)};
    T* cur{std::find(buf_ + pos, buf_ + N, S)};
    assert(cur != buf_ + N);

    T new_el{std::forward<Args>(args)...};
    assert_if_noexcept(new_el != S);
    while(cur != pos_it) {
      *cur = std::move(*(cur - 1));
      --cur;
    }
    *cur = std::move(new_el);
    return cur;
  }

  /** @brief Emplaces an element at the end of the vector.
   *
   * Exception safety guarantees: strong.
   *
   * @returns A reference to the newly emplaced element. */
  template<typename... Args>
  requires std::is_constructible_v<T, Args&&...>
  T& emplace_back(Args&&... args)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_constructible_v<T, Args&&...>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_destructible_v<T>) {
    assert_if_noexcept(!full());
    T* const p{this->end_common()};
    assert(p != buf_ + N);
    *p = T{std::forward<Args>(args)...};
    return *p;
  }

  /** @brief Emplaces an element at the beginning of the vector.
   *
   * See emplace() for exception safety guarantees.
   *
   * @returns A reference to the newly emplaced element. */
  template<typename... Args>
  requires std::is_constructible_v<T, Args&&...>
  T& emplace_front(Args&&... args)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_constructible_v<T, Args&&...>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_destructible_v<T>) {
    assert_if_noexcept(!full());
    return *this->emplace(cbegin(), std::forward<Args>(args)...);
  }

  // Insertion /////////////////////////////////////////////////////////////////

  /** @brief Inserts the given range immediately before the element pointed to
   * by the given iterator.
   *
   * May also throw:
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the construction or assignment of elements from
   *   the result of dereferencing an iterator;
   * - whatever is thrown by move-construction or move-assignment of elements.
   *
   * @returns An iterator to the beginning of the inserted range.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws sentinel_vector_length_error */
  template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  iterator insert(const_iterator pos_it, Iterator it, Sentinel iend) {
    assert(buf_ <= pos_it);
    assert(pos_it <= buf_ + N);
    return this->replace(pos_it, pos_it, std::move(it), std::move(iend));
  }

  /** @brief Inserts the given range immediately before the element pointed to
   * by the given iterator.
   *
   * The element type must be move-constructible and move-assignable. It must
   * also be constructible with an rvalue result of dereferencing a source
   * iterator as the sole argument. Further, it must be assignable from the
   * rvalue result of dereferencing a source iterator.
   *
   * @returns An iterator to the beginning of the inserted range. */
  template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  iterator insert(const_iterator pos_it, Iterator it, Sentinel iend,
      unchecked_t) noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && (
        std::forward_iterator<Iterator>
        ? noexcept(*it) && noexcept(++it)
        && noexcept(util::next_until(it, iend, 0))
        && noexcept(util::distance(it, iend))
        && std::is_nothrow_copy_constructible_v<Iterator>
        && std::is_nothrow_copy_constructible_v<Sentinel>
        : std::is_nothrow_move_constructible_v<T>
        && std::is_nothrow_destructible_v<T>
      )) {
    assert(buf_ <= pos_it);
    assert(pos_it <= buf_ + N);
    return this->replace(pos_it, pos_it, std::move(it), std::move(iend),
      unchecked);
  }

  /** @brief Inserts the provided initializer list immediately before the
   * element pointed to by the given iterator.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator insert(const_iterator pos, std::initializer_list<T> il) {
    assert(buf_ <= pos);
    assert(pos <= buf_ + N);
    assert(il.size() <= N);
    assert((pos - buf_) + il.size() <= N);
    return this->insert(pos, il.begin(), il.end());
  }

  /** @brief Inserts the provided initializer list immediately before the
   * element pointed to by the given iterator.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator insert(const_iterator pos, std::initializer_list<T> il,
      unchecked_t) noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(buf_ <= pos);
    assert(pos <= buf_ + N);
    assert(il.size() <= N);
    assert((pos - buf_) + il.size() <= N);
    return this->insert(pos, il.begin(), il.end(), unchecked);
  }

  /** @brief Inserts the given number of copies of the provided element
   * immediately before the element pointed to by the given iterator.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator insert(const_iterator pos, size_type count, T const& t) {
    assert(N >= count);
    auto const view{util::repeat(t, count)};
    return this->insert(pos, view.begin(), view.end());
  }

  /** @brief Inserts the given number of copies of the provided element
   * immediately before the element pointed to by the given iterator.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator insert(const_iterator pos, size_type count, T const& t, unchecked_t)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(N >= count);
    auto const view{util::repeat(t, count)};
    return this->insert(pos, view.begin(), view.end(), unchecked);
  }

  /** @brief Inserts the given element immediately before the element pointed to
   * by the given iterator.
   *
   * See emplace() for thrown exceptions and exception safety guarantees. (The
   * new element is move-constructed from the one passed to this function.)
   *
   * @returns An iterator to the newly inserted element. */
  iterator insert(const_iterator pos, T t)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_constructible_v<T>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_destructible_v<T>) {
    assert(buf_ <= pos);
    assert(pos <= buf_ + N);
    assert_if_noexcept(!full());
    return this->emplace(pos, std::move(t));
  }

private:
  template<bool Checked, std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void append_impl(Iterator it, Sentinel iend) noexcept(!Checked
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && noexcept(std::declval<T const&>() == S) && noexcept(it != iend)
      && noexcept(*it++)) {
    T* last{end_common()};
    constexpr bool is_multipass{std::forward_iterator<Iterator>};
    if constexpr(is_multipass && (Checked || noexcept(util::distance(it, iend)))) {
      using range_diff_type = std::iter_difference_t<Iterator>;
      range_diff_type const range_size{util::distance(it, iend)};

      // Capacity is required to fit in std::ptrdiff_t, so cast it to that type to compare it
      // to the signed value that is the new range's size
      auto const remaining_capacity{util::asserted_cast<std::ptrdiff_t>(
        capacity - (last - buf_))};

      if constexpr(Checked) {
        if(remaining_capacity < range_size) {
          throw sentinel_vector_length_error{};
        }
      } else {
        assert(remaining_capacity >= range_size);
      }
    }

    while(it != iend) {
      if constexpr(Checked && !is_multipass) {
        if(last == buf_ + N) {
          throw sentinel_vector_length_error{};
        }
      }
      *last++ = *it++;
    }
  }

public:
  /** @brief Inserts the given range at the end of the vector.
   *
   * May also throw:
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the assignment of elements from the result of
   *   dereferencing an iterator.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws sentinel_vector_length_error */
  template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void append(Iterator it, Sentinel iend) {
    this->append_impl<true>(std::move(it), std::move(iend));
  }

  /** @brief Inserts the given range at the end of the vector. */
  template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void append(Iterator it, Sentinel iend, unchecked_t) noexcept(
      std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && noexcept(std::declval<T const&>() == S) && noexcept(it != iend)
      && noexcept(*it++)) {
    this->append_impl<false>(std::move(it), std::move(iend));
  }

  void push_back(T t) noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_constructible_v<T>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_destructible_v<T>) {
    assert_if_noexcept(!full());
    this->emplace_back(std::move(t));
    assert_if_noexcept(!empty());
  }

  /** @brief Inserts the given range at the beginning of the vector.
   *
   * May also throw:
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the construction or assignment of elements from
   *   the result of dereferencing an iterator;
   * - whatever is thrown by move-construction or move-assignment of elements.
   *
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws sentinel_vector_length_error */
  template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void prepend(Iterator it, Sentinel iend) {
    this->insert(cbegin(), std::move(it), std::move(iend));
  }

  /** @brief Inserts the given range at the beginning of the vector. */
  template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void prepend(Iterator it, Sentinel iend, unchecked_t)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && (
        std::forward_iterator<Iterator>
        ? noexcept(*it) && noexcept(++it)
        && noexcept(util::next_until(it, iend, 0))
        && noexcept(util::distance(it, iend))
        && std::is_nothrow_copy_constructible_v<Iterator>
        && std::is_nothrow_copy_constructible_v<Sentinel>
        : std::is_nothrow_move_constructible_v<T>
        && std::is_nothrow_destructible_v<T>
      )) {
    this->insert(cbegin(), std::move(it), std::move(iend), unchecked);
  }

  /** @brief Inserts the given element at the beginning of the vector. */
  void push_front(T t) noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_constructible_v<T>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_destructible_v<T>) {
    assert_if_noexcept(!full());
    assert_if_noexcept(t != S);
    this->emplace_front(std::move(t));
    assert_if_noexcept(!empty());
  }

  // Erasure ///////////////////////////////////////////////////////////////////

  /** @brief Deletes all elements in the vector. */
  void clear() noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    this->erase_after(cbegin());
  }

  /** @brief Erases the elements starting from the one specified by the first
   * iterator and ending immediately before the second iterator.
   *
   * @returns An iterator pointing to the first element past the erased range.
   *          If the range is at the end of the vector, returns a past-the-end
   *          iterator. */
  iterator erase(const_iterator it, const_iterator iend)
      noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(buf_ <= it);
    assert(it <= iend);
    assert(iend <= buf_ + N);
    assert_if_noexcept(it == iend || *(iend - 1) != S);

    std::size_t const pos_begin{util::asserted_cast<std::size_t>(it - cbegin())};
    std::size_t const pos_end{util::asserted_cast<std::size_t>(iend - cbegin())};
    std::size_t const range_size{pos_end - pos_begin};
    if(range_size == 0) {
      return begin() + pos_begin;
    }

    T* p{begin() + pos_end};
    while(p < buf_ + N && *p != S) {
      *(p - range_size) = std::move(*p);
      ++p;
    }

    this->erase_after(p - range_size);
    return begin() + pos_begin;
  }

  /** @brief Erases the element pointed to by the given iterator.
   *
   * @returns An iterator pointing to the element after the erased one. If the
   *          element was at the end of the vector, returns a past-the-end
   *          iterator. */
  iterator erase(const_iterator it)
      noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(buf_ <= it);
    assert(it < buf_ + N);
    assert_if_noexcept(*it != S);
    return this->erase(it, it + 1);
  }

  /** @brief Erases all elements from the given position to the end of the
   * vector. */
  void erase_after(const_iterator it)
      noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(buf_ <= it);
    assert(it <= buf_ + N);

    std::size_t const pos{util::asserted_cast<std::size_t>(it - cbegin())};
    T* p{std::find(buf_ + pos, buf_ + N, S)};
    while(p != it) {
      *--p = S;
    }
  }

  /** @brief Deletes the last element. */
  void pop_back() noexcept(noexcept(std::declval<T const&>() != S)
      && noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_copy_assignable_v<T>) {
    assert_if_noexcept(!empty());
    this->erase_after(cend_common() - 1);
  }

  /** @brief Erases all elements from the beginning of the vector to the given
   * position (not including the element pointed to by the iterator). */
  void erase_before(const_iterator it)
      noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    this->erase(cbegin(), it);
  }

  /** @brief Deletes the first element. */
  void pop_front() noexcept(noexcept(std::declval<T const&>() != S)
      && std::is_nothrow_copy_assignable_v<T>) {
    assert_if_noexcept(!empty());
    this->erase_before(cbegin() + 1);
  }

  // Resizing //////////////////////////////////////////////////////////////////

  /** @brief Changes the size of the vector to the given size, padding with
   * default-constructed elements if needed. */
  void resize(size_type new_size)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_default_constructible_v<T>
      && std::is_nothrow_copy_assignable_v<T>
      && std::is_nothrow_destructible_v<T>)
      requires std::is_default_constructible_v<T> {
    assert(new_size <= N);
    this->resize(new_size, T{});
  }

  /** @brief Changes the size of the vector to the given size, padding with
   * copies of the given element if needed. */
  void resize(size_type new_size, T const& t)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(new_size <= N);
    if(new_size == N || buf_[new_size] == S) {
      for(T* cur{end_common()}; cur != buf_ + new_size; ++cur) {
        *cur = t;
      }
    } else {
      T* cur{std::find(buf_ + new_size, buf_ + N, S)};
      do {
        *--cur = S;
      } while(cur != buf_ + new_size);
    }
  }

  // Replacement ///////////////////////////////////////////////////////////////
private:
  template<bool Checked, std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  iterator replace_impl(const_iterator it_old, const_iterator iend_old,
      Iterator it_new, Sentinel iend_new) noexcept(!Checked
      && noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && (
        std::forward_iterator<Iterator>
        ? noexcept(*it_new) && noexcept(++it_new)
        && noexcept(util::next_until(it_new, iend_new, 0))
        && noexcept(util::distance(it_new, iend_new))
        && std::is_nothrow_copy_constructible_v<Iterator>
        && std::is_nothrow_copy_constructible_v<Sentinel>
        : std::is_nothrow_move_constructible_v<T>
        && std::is_nothrow_destructible_v<T>
      )) {
    assert(buf_ <= it_old);
    assert(it_old <= iend_old);
    assert(iend_old <= buf_ + N);
    if constexpr(std::forward_iterator<Iterator>) {
      std::size_t const pos{util::asserted_cast<std::size_t>(it_old - buf_)};

      T* last{std::find(buf_ + pos, buf_ + N, S)};
      std::size_t const old_size{util::asserted_cast<std::size_t>(last - buf_)};

      using range_diff_type = std::iter_difference_t<Iterator>;
      auto const [sep, split] = util::next_until(it_new, iend_new, old_size - pos);
      range_diff_type const new_range_size{sep == iend_new ? split
        : split + util::distance(sep, iend_new)};
      std::size_t const old_range_size{util::asserted_cast<std::size_t>(iend_old - it_old)};
      std::size_t const max_allowed_new_range_size{N - (old_size - old_range_size)};

      assert(new_range_size >= 0);
      if constexpr(Checked) {
        if(
          max_allowed_new_range_size <= std::numeric_limits<range_diff_type>::max()
          && new_range_size > static_cast<range_diff_type>(max_allowed_new_range_size)
        ) {
          throw sentinel_vector_length_error{};
        }
      } else {
        assert(
          max_allowed_new_range_size > std::numeric_limits<range_diff_type>::max()
          || new_range_size <= static_cast<range_diff_type>(max_allowed_new_range_size)
        );
      }
      auto const new_range_size_u{static_cast<std::size_t>(new_range_size)};

      if(new_range_size_u > old_range_size) {
        // The vector increases in size.
        std::size_t const new_size{old_size - old_range_size
          + new_range_size_u};
        std::size_t const size_increase{new_size - old_size};

        // There are two cases: either the newly inserted range overlaps the
        // end of the old vector or it does not. In the first case, we execute
        // steps 1, 2, and 4. In the second case, we execute steps 2, 3, and
        // 4.

        // 1. Insert the trailing part of the new range that extends past the
        // end of the original vector (if such part exists).
        for(auto iit{sep}; iit != iend_new; ++iit) {
          *last++ = *iit;
        }

        // 2. Move-construct the old trailing elements past the end.
        while(util::asserted_cast<std::size_t>(last - buf_) < new_size) {
          *last = std::move(*(last - size_increase));
          ++last;
        }

        // 3. Move-assign old elements within the old vector (if necessary).
        for(std::size_t i{old_size - 1}; i >= pos + new_range_size_u; --i) {
          buf_[i] = std::move(buf_[i - size_increase]);
        }

        // 4. Insert the initial part of the new range into the part of the
        // buffer previously occupied by the original vector.
        for(std::size_t i{pos}; it_new != sep; ++it_new, ++i) {
          buf_[i] = *it_new;
        }
      } else {
        // The vector either decreases in size or stays the same size.
        std::size_t i{pos};
        for(; it_new != iend_new; ++i, ++it_new) {
          buf_[i] = *it_new;
        }
        erase(begin() + i, iend_old);
      }

      return buf_ + pos;
    } else {
      inplace_vector<T, N> new_range{};
      for(auto&& t: std::ranges::subrange{std::move(it_new), std::move(iend_new)}) {
        if(!new_range.try_push_back(std::forward<decltype(t)>(t))) {
          throw sentinel_vector_length_error{};
        }
      }

      auto move_view{new_range | std::views::transform([](T& t) noexcept {
        return std::move(t);
      })};

      return this->replace(it_old, iend_old,
        move_view.begin(), move_view.end(), unchecked);
    }
  }

public:
  /** @brief Replaces the given range within this vector with the other provided
   * range.
   *
   * May also throw:
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the construction or assignment of elements from
   *   the result of dereferencing an iterator;
   * - whatever is thrown by move-construction or move-assignment of elements;
   * - whatever is thrown by the destruction of elements.
   *
   * @returns An iterator to the beginning of the inserted range.
   * @throws std::bad_alloc
   * @throws std::length_error
   * @throws sentinel_vector_length_error */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  iterator replace(const_iterator it_old, const_iterator iend_old,
      Iterator it_new, Sentinel iend_new) {
    assert(buf_ <= it_old);
    assert(it_old <= iend_old);
    assert(iend_old <= buf_ + N);
    return this->replace_impl<true>(it_old, iend_old, std::move(it_new),
      std::move(iend_new));
  }

  /** @brief Replaces the given range within this vector with the other provided
   * range.
   *
   * @returns An iterator to the beginning of the inserted range. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  iterator replace(const_iterator it_old, const_iterator iend_old,
      Iterator it_new, Sentinel iend_new, unchecked_t)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && (
        std::forward_iterator<Iterator>
        ? noexcept(*it_new) && noexcept(++it_new)
        && noexcept(util::next_until(it_new, iend_new, 0))
        && noexcept(util::distance(it_new, iend_new))
        && std::is_nothrow_copy_constructible_v<Iterator>
        && std::is_nothrow_copy_constructible_v<Sentinel>
        : std::is_nothrow_move_constructible_v<T>
        && std::is_nothrow_destructible_v<T>
      )) {
    assert(buf_ <= it_old);
    assert(it_old <= iend_old);
    assert(iend_old <= buf_ + N);
    return this->replace_impl<false>(it_old, iend_old, std::move(it_new),
      std::move(iend_new));
  }

  /** @brief Replaces the given range within this vector with the provided
   * initializer list.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator replace(const_iterator it_old, const_iterator iend_old,
      std::initializer_list<T> il) {
    assert(buf_ <= it_old);
    assert(it_old <= iend_old);
    assert(iend_old <= buf_ + N);
    assert(il.size() <= N - util::asserted_cast<std::size_t>(iend_old - it_old));
    return this->replace(it_old, iend_old, il.begin(), il.end());
  }

  /** @brief Replaces the given range within this vector with the provided
   * initializer list.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator replace(const_iterator it_old, const_iterator iend_old,
      std::initializer_list<T> il, unchecked_t)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(buf_ <= it_old);
    assert(it_old <= iend_old);
    assert(iend_old <= buf_ + N);
    assert(il.size() <= N - util::asserted_cast<std::size_t>(iend_old - it_old));
    return this->replace(it_old, iend_old, il.begin(), il.end(), unchecked);
  }

  /** @brief Replaces the given range within this vector with the given number
   * of copies of the provided element.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator replace(const_iterator it_old, const_iterator iend_old,
      size_type count, T const& t) {
    assert(buf_ <= it_old);
    assert(it_old <= iend_old);
    assert(iend_old <= buf_ + N);
    assert(count <= N - util::asserted_cast<std::size_t>(iend_old - it_old));
    auto const view{util::repeat(t, count)};
    return this->replace(it_old, iend_old, view.begin(), view.end());
  }

  /** @brief Replaces the given range within this vector with the given number
   * of copies of the provided element.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator replace(const_iterator it_old, const_iterator iend_old,
      size_type count, T const& t, unchecked_t)
      noexcept(noexcept(std::declval<T const&>() == S)
      && std::is_nothrow_move_assignable_v<T>
      && std::is_nothrow_copy_assignable_v<T>) {
    assert(buf_ <= it_old);
    assert(it_old <= iend_old);
    assert(iend_old <= buf_ + N);
    assert(count <= N - util::asserted_cast<std::size_t>(iend_old - it_old));
    auto const view{util::repeat(t, count)};
    return this->replace(it_old, iend_old, view.begin(), view.end(), unchecked);
  }
};

template<typename T, std::size_t N, T S>
void swap(sentinel_vector<T, N, S>& a, sentinel_vector<T, N, S>& b)
    noexcept(noexcept(a.swap(b))) {
  a.swap(b);
}

// Comparison //////////////////////////////////////////////////////////////////

template<typename T, std::size_t N, T S>
[[nodiscard]] bool operator==(
  sentinel_vector<T, N, S> const& a,
  sentinel_vector<T, N, S> const& b
) noexcept(noexcept(std::declval<T const&>() == std::declval<T const&>())) {
  return std::ranges::equal(a, b);
}

template<typename T, std::size_t N, T S>
[[nodiscard]] std::strong_ordering operator<=>(
  sentinel_vector<T, N, S> const& a,
  sentinel_vector<T, N, S> const& b
) noexcept(
  noexcept(std::declval<T const&>() <=> std::declval<T const&>())
  && noexcept(std::declval<T const&>() == S)
) {
  auto it_a{a.cbegin()};
  auto it_b{b.cbegin()};
  while(it_a != a.cend() && it_b != b.cend()) {
    if(auto const cmp{*it_b <=> *it_a}; cmp != 0) {
      return cmp;
    }
    ++it_a;
    ++it_b;
  }
  if(it_a != a.cend()) {
    return std::strong_ordering::greater;
  }
  if(it_b != b.cend()) {
    return std::strong_ordering::less;
  }
  return std::strong_ordering::equivalent;
}

} // util

#endif
