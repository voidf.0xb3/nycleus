#ifndef UTIL_SCOPE_GUARD_HPP_INCLUDED_UXXMSYWPLIK904NPR1LCH0VU0
#define UTIL_SCOPE_GUARD_HPP_INCLUDED_UXXMSYWPLIK904NPR1LCH0VU0

#include <concepts>
#include <functional>
#include <optional>
#include <type_traits>
#include <utility>

namespace util {

/** @brief Executes arbitrary code when leaving the scope. */
template<std::invocable Function>
requires std::destructible<Function>
class scope_guard {
private:
  std::optional<Function> handler_;

public:
  explicit constexpr scope_guard(Function handler)
    noexcept(std::is_nothrow_move_constructible_v<Function>):
    handler_{std::move(handler)} {}

  scope_guard(scope_guard const&) = delete;
  scope_guard(scope_guard&&) = delete;
  scope_guard& operator=(scope_guard const&) = delete;
  scope_guard& operator=(scope_guard&&) = delete;

  constexpr ~scope_guard() noexcept(std::is_nothrow_invocable_v<Function>) {
    if(handler_) {
      std::invoke(*handler_);
    }
  }

  /** @brief Disables the scope guard. */
  constexpr void reset() noexcept { handler_.reset(); }
};

} // util

#endif
