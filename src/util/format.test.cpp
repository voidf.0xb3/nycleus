#include <boost/test/unit_test.hpp>

#include "util/fixed_string.hpp"
#include "util/format.hpp"
#include <iterator>
#include <ranges>
#include <utility>

using namespace std::string_view_literals;

static_assert(std::output_iterator<util::output_counting_iterator, char8_t>);

namespace {

struct test_formattable {
  std::u8string_view str;
};

} // (anonymous)

template<::util::fixed_string S>
struct util::formatter<test_formattable, S> {
  struct type {
    static constexpr auto format(
      ::std::output_iterator<char8_t> auto out,
      test_formattable value
    ) {
      out = ::std::ranges::copy(value.str, ::std::move(out)).out;
      *out++ = u8':';
      return ::std::ranges::copy(::util::fixed_string_sv<S>,
        ::std::move(out)).out;
    }

    static constexpr ::util::output_counting_iterator format(
      ::util::output_counting_iterator out,
      test_formattable value
    ) {
      out.add(value.str.size());
      out.add(1);
      out.add(S.size());
      return out;
    }
  };
};

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(format)

BOOST_AUTO_TEST_CASE(literal) {
  auto const result{util::format<u8"abc">()};
  BOOST_TEST((result == u8"abc"));
}

BOOST_AUTO_TEST_CASE(escaped) {
  auto const result{util::format<u8"{{abc}}">()};
  BOOST_TEST((result == u8"{abc}"));
}

BOOST_AUTO_TEST_CASE(implicit_pos) {
  auto const result{util::format<u8"abc{}def{}ghi">(u8"123"sv, 456)};
  BOOST_TEST((result == u8"abc123def456ghi"));
}

BOOST_AUTO_TEST_CASE(explicit_pos) {
  auto const result{util::format<u8"abc{1}def{0}ghi">(u8"123"sv, 456)};
  BOOST_TEST((result == u8"abc456def123ghi"));
}

BOOST_AUTO_TEST_CASE(implicit_custom) {
  auto const result{util::format<u8"abc{}def{:---}ghi">(
    test_formattable(u8"123"sv),
    test_formattable(u8"456"sv)
  )};
  BOOST_TEST((result == u8"abc123:def456:---ghi"));
}

BOOST_AUTO_TEST_CASE(explicit_custom) {
  auto const result{util::format<u8"abc{1}def{0:---}ghi">(
    test_formattable(u8"123"sv),
    test_formattable(u8"456"sv)
  )};
  BOOST_TEST((result == u8"abc456:def123:---ghi"));
}

BOOST_AUTO_TEST_CASE(join_empty) {
  std::vector<test_formattable> vec;
  auto const result{util::format<u8"[{:---}]">(util::format_join{vec})};
  BOOST_TEST((result == u8"[]"));
}

BOOST_AUTO_TEST_CASE(join_one) {
  std::vector<test_formattable> vec{
    test_formattable{u8"123"sv}
  };
  auto const result{util::format<u8"[{:---}]">(
    util::format_join{vec, u8";"sv})};
  BOOST_TEST((result == u8"[123:---]"));
}

BOOST_AUTO_TEST_CASE(join_many) {
  std::vector<test_formattable> vec{
    test_formattable{u8"123"sv},
    test_formattable{u8"456"sv},
    test_formattable{u8"789"sv}
  };
  auto const result{util::format<u8"[{:---}]">(
    util::format_join{vec, u8";"sv})};
  BOOST_TEST((result == u8"[123:---;456:---;789:---]"));
}

BOOST_AUTO_TEST_SUITE_END() // format
BOOST_AUTO_TEST_SUITE_END() // test_util
