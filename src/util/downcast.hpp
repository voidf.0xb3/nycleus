#ifndef UTIL_DOWNCAST_HPP_INCLUDED_AS46EY1VD2MJ4UD15F93GOCS0
#define UTIL_DOWNCAST_HPP_INCLUDED_AS46EY1VD2MJ4UD15F93GOCS0

#include <cassert>
#include <memory>
#include <type_traits>

namespace util {

/** @brief Takes a reference to an object of a polymorphic base class and casts
 * it to the specified derived class.
 *
 * This asserts that the downcast is allowed, but does not otherwise verify its
 * validity. If the downcast is invalid, the behavior is undefined (in release
 * mode). */
template<typename T, typename U>
requires std::is_lvalue_reference_v<T>
  && std::is_base_of_v<std::remove_cvref_t<U>, std::remove_cvref_t<T>>
  && std::is_polymorphic_v<std::remove_cvref_t<U>>
[[nodiscard]] T downcast(U& value) noexcept {
  assert(dynamic_cast<std::add_pointer_t<T>>(std::addressof(value)));
  return static_cast<T>(value);
}

/** @brief Takes a pointer to an object of a polymorphic base class and casts it
 * to the specified derived class.
 *
 * The pointer must not be null.
 *
 * This asserts that the downcast is allowed, but does not otherwise verify its
 * validity. If the downcast is invalid, the behavior is undefined (in release
 * mode). */
template<typename T, typename U>
requires std::is_pointer_v<T>
  && std::is_base_of_v<std::remove_cvref_t<U>,
    std::remove_cv_t<std::remove_pointer_t<T>>>
  && std::is_polymorphic_v<std::remove_cvref_t<U>>
[[nodiscard]] T downcast(U* value) noexcept {
  assert(dynamic_cast<T>(value));
  return static_cast<T>(value);
}

} // util

#endif
