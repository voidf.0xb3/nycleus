#include <boost/test/unit_test.hpp>

#include "util/ranges.hpp"
#include <cstdint>
#include <string_view>
#include <vector>

using namespace std::string_view_literals;

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(ranges)

BOOST_AUTO_TEST_CASE(find_last) {
  std::vector<std::uint64_t> vec{1, 2, 3, 1, 3, 2};

  auto const result1{util::find_last(vec, 1u)};
  BOOST_TEST((result1.begin() == vec.cbegin() + 3));
  BOOST_TEST((result1.end() == vec.cend()));

  auto const result2{util::find_last(vec, 10u)};
  BOOST_TEST((result2.begin() == vec.cend()));
  BOOST_TEST((result2.end() == vec.cend()));

  auto const result3{util::find_last_if(vec,
    [](std::uint64_t x) noexcept { return x % 2 == 1; })};
  BOOST_TEST((result3.begin() == vec.cbegin() + 4));
  BOOST_TEST((result3.end() == vec.cend()));

  auto const result4{util::find_last_if(vec,
    [](std::uint64_t x) noexcept { return x > 10; })};
  BOOST_TEST((result4.begin() == vec.cend()));
  BOOST_TEST((result4.end() == vec.cend()));

  auto const result5{util::find_last_if_not(vec,
    [](std::uint64_t x) noexcept { return x % 2 == 0; })};
  BOOST_TEST((result5.begin() == vec.cbegin() + 4));
  BOOST_TEST((result5.end() == vec.cend()));

  auto const result6{util::find_last_if_not(vec,
    [](std::uint64_t x) noexcept { return x < 10; })};
  BOOST_TEST((result6.begin() == vec.cend()));
  BOOST_TEST((result6.end() == vec.cend()));
}

BOOST_AUTO_TEST_CASE(starts_with) {
  BOOST_TEST(util::starts_with(U"foobar"sv, U"foo"sv));
  BOOST_TEST(!util::starts_with(U"foobar"sv, U"xyz"sv));
  BOOST_TEST(!util::starts_with(U"foo"sv, U"foobar"sv));
}

BOOST_AUTO_TEST_CASE(ranges_to) {
  std::uint64_t input[]{1, 2, 3};
  auto const output{util::range_to<std::vector<std::uint64_t>>(input)};
  BOOST_TEST(input == output, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(ranges_to_deduced) {
  std::uint64_t input[]{1, 2, 3};
  auto const output{util::range_to<std::vector>(input)};
  BOOST_TEST(input == output, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(ranges_to_pipe) {
  std::uint64_t input[]{1, 2, 3};
  auto const output{input | util::range_to<std::vector<std::uint64_t>>()};
  BOOST_TEST(input == output, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(ranges_to_pipe_deduced) {
  std::uint64_t input[]{1, 2, 3};
  auto const output{input | util::range_to<std::vector>()};
  BOOST_TEST(input == output, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // ranges
BOOST_AUTO_TEST_SUITE_END() // test_util
