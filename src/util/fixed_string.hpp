#ifndef UTIL_FIXED_STRING_HPP_INCLUDED_XW2Q4TBV1NAIPYYAWD97B3LMP
#define UTIL_FIXED_STRING_HPP_INCLUDED_XW2Q4TBV1NAIPYYAWD97B3LMP

#include "util/u8compat.hpp"
#include <compare>
#include <cstddef>
#include <string_view>

namespace util {

/** @brief A helper class used as a way to pass strings as template arguments.
 *
 * Simply declare a non-type template parameter constrained to be a
 * specialization of fixed_string and pass a string literal as an argument. */
template<std::size_t N>
struct fixed_string {
  /** @brief A buffer containing the null-terminated string.
   *
   * To avoid duplication, access this buffer via `fixed_string_buf`. */
  char8_t buf[N + 1];

  /** @brief Constructs a string that is initialized with zeros.
   *
   * The buffer should be filled manually before the string is used. */
  constexpr fixed_string() noexcept {
    for(std::size_t i{0}; i < N + 1; ++i) {
      buf[i] = 0;
    }
  }

  constexpr fixed_string(cu8zstring init_str) noexcept {
    for(std::size_t i{0}; i < N; ++i) {
      buf[i] = init_str[i];
    }
    buf[N] = 0;
  }

  constexpr fixed_string(char8_t ch) noexcept requires (N == 1) {
    buf[0] = ch;
    buf[1] = 0;
  }

  [[nodiscard]] constexpr static std::size_t size() noexcept {
    return N;
  }

  /** @brief Accesses a code unit at the specified index. */
  [[nodiscard]] constexpr char8_t operator[](std::size_t index) const noexcept {
    return buf[index];
  }

  /** @brief Obtains a substring.
   *
   * Since everything is supposed to be done at compile time, the arguments are
   * passed as template arguments. */
  template<std::size_t Start, std::size_t Length = N - Start>
  requires (Start + Length <= N)
  [[nodiscard]] constexpr fixed_string<Length> substr() const noexcept {
    fixed_string<Length> result;
    for(std::size_t i{0}; i < Length; ++i) {
      result.buf[i] = buf[Start + i];
    }
    return result;
  }

  /** @brief A simpler syntax for a common case of obtaining a substring that
   * drops the first code unit. */
  [[nodiscard]] constexpr fixed_string<N - 1> tail() const noexcept
      requires (N >= 1) {
    return substr<1>();
  }
};

template<std::size_t N>
fixed_string(char8_t const (&)[N]) -> fixed_string<N - 1>;

fixed_string(char8_t) -> fixed_string<1>;

/** @brief A way to extract a buffer from a fixed_string such that every string
 * gets only a single buffer.
 *
 * If you have multiple copies of the same fixed_string, directly accessing each
 * one's buffer will create many distinct buffers. Instead, pass them as
 * the argument to this template, and all occurences of the same string will
 * give the same buffer. */
template<fixed_string S>
constexpr cu8zstring fixed_string_buf{S.buf};

/** @brief Converts a fixed_string into a u8string_view. */
template<fixed_string S>
constexpr std::u8string_view fixed_string_sv{fixed_string_buf<S>};

template<std::size_t N, std::size_t M>
[[nodiscard]] constexpr fixed_string<N + M> operator+(
  fixed_string<N> const& a,
  fixed_string<M> const& b
) noexcept {
  char8_t buf[N + M];
  for(std::size_t i{0}; i < N; ++i) {
    buf[i] = a[i];
  }
  for(std::size_t i{0}; i < M; ++i) {
    buf[N + i] = b[i];
  }
  return buf;
}

template<std::size_t N, std::size_t M>
[[nodiscard]] constexpr bool operator==(
  fixed_string<N> const& a,
  fixed_string<M> const& b
) noexcept {
  if constexpr(N != M) {
    return false;
  }
  for(std::size_t i{0}; i < N && i < M; ++i) {
    if(a[i] != b[i]) {
      return false;
    }
  }
  return true;
}

template<std::size_t N, std::size_t M>
[[nodiscard]] constexpr std::strong_ordering operator<=>(
  fixed_string<N> const& a,
  fixed_string<M> const& b
) noexcept {
  for(std::size_t i{0}; i < N && i < M; ++i) {
    if(auto const cmp{a[i] <=> b[i]}; cmp != 0) {
      return cmp;
    }
  }
  return N <=> M;
}

} // util

#endif
