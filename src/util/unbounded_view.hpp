#ifndef UTIL_UNBOUNDED_VIEW_HPP_INCLUDED_BP0TPO8J5HGWJGRIFS1Q7O7WP
#define UTIL_UNBOUNDED_VIEW_HPP_INCLUDED_BP0TPO8J5HGWJGRIFS1Q7O7WP

#include <iterator>
#include <ranges>
#include <type_traits>
#include <utility>

namespace util {

template<std::input_or_output_iterator T>
struct unreachable_sentinel {
  [[nodiscard]] constexpr friend bool operator==(
    T const&,
    unreachable_sentinel
  ) noexcept {
    return false;
  }
};

template<std::input_iterator It>
class unbounded_view: public std::ranges::view_interface<unbounded_view<It>> {
private:
  It it_;

public:
  constexpr explicit unbounded_view(It it)
    noexcept(std::is_nothrow_move_constructible_v<It>): it_{std::move(it)} {}

  [[nodiscard]] constexpr It begin() const
      noexcept(std::is_nothrow_copy_constructible_v<It>) {
    return it_;
  }

  [[nodiscard]] constexpr unreachable_sentinel<It> end() const noexcept {
    return {};
  }
};

} // util

template<std::input_iterator It>
constexpr bool std::ranges::enable_borrowed_range<util::unbounded_view<It>>{
  true};

#endif
