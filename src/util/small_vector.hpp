#ifndef UTIL_SMALL_VECTOR_HPP_INCLUDED_XDFBU0XPVXSMVZ6S4M9G5ZR3U
#define UTIL_SMALL_VECTOR_HPP_INCLUDED_XDFBU0XPVXSMVZ6S4M9G5ZR3U

#include "util/iterator.hpp"
#include "util/iterator_facade.hpp"
#include "util/overflow.hpp"
#include "util/repeat_view.hpp"
#include "util/scope_guard.hpp"
#include "util/type_traits.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <cassert>
#include <compare>
#include <concepts>
#include <cstddef>
#include <initializer_list>
#include <iterator>
#include <limits>
#include <memory>
#include <new>
#include <ranges>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace util {

namespace detail_ {

/** @brief A utility type providing storage for one small_vector element. */
template<typename T>
union small_vector_holder {
  T t;
  small_vector_holder() noexcept {}
  small_vector_holder(small_vector_holder const&) = delete;
  small_vector_holder& operator=(small_vector_holder const&) = delete;
  ~small_vector_holder() noexcept {}
};

/** @brief The rebound allocator for small_vector. */
template<typename T, typename Allocator>
using small_vector_allocator = std::allocator_traits<Allocator>
  ::template rebind_alloc<small_vector_holder<T>>;

} // detail_

/** @brief A vector that has a preallocated space for a certain number of
 * elements (similarly to small-string optimization).
 *
 * The element type must be move-insertible. Further, its destructor must not
 * throw exceptions.
 *
 * The actual in-place capacity may be larger than requested, if that does not
 * increase the size of the object. The actual small capacity can be found
 * through the `small_capacity` static member variable.
 *
 * If a vector grows larger than the value returned by max_size(), the behavior
 * is undefined. (Though we will probably run out of memory long before that.)
 *
 * Concerning undefined behavior with allocators: Unfortunately, prior to C++20,
 * an allocator-aware vector-like container is impossible to implement without
 * technically undefined behavior (pointer arithmetic outside of an array
 * object). Thus, if the allocator is not std::allocator, this implementation
 * has technically undefined behavior that, in practice, works on all compilers.
 * The undefined behavior is such that it automatically ceases to be undefined
 * upon upgrade to C++20.
 *
 * Exception safety guarantees: unless otherwise specified, every member
 * function has a weak exception safety guarantee, except that some elements
 * can be in a moved-from state, and if the exception was thrown by some
 * function provided by the element type, then the states of the element(s)
 * affected by that function depend on that function's exception safety
 * guarantees. Member documentation calls this "standard guarantee".
 *
 * Move safety guarantees: the moved-from vector will be in a valid but
 * unspecified state, except that some elements may be in the moved-from
 * state.
 *
 * Iterator invalidation: Insertion/emplacement, erasure, and replacement
 * operations invalidate at least all iterators except those pointing to
 * elements before the place where the operation occured. All operations that
 * change vector capacity invalidate all iterators. */
template<typename T, std::size_t N, typename Allocator = std::allocator<T>>
class small_vector {
protected:
  using alloc = detail_::small_vector_allocator<T, Allocator>;
  using alloc_traits = std::allocator_traits<alloc>;

public:
  using allocator_type = Allocator;

  static_assert(is_maybe_nothrow_eraseable_v<alloc, T>,
    "util::small_vector: element type's erasure must not throw exceptions");
  static_assert(is_maybe_move_insertible_v<alloc, T>,
    "util::small_vector: element type must be move-insertible");

  using value_type = T;
  using size_type = std::common_type_t<std::size_t,
    typename alloc_traits::size_type>;
  using difference_type = std::common_type_t<std::ptrdiff_t,
    typename alloc_traits::difference_type>;
  using reference = T&;
  using const_reference = T const&;
  using pointer = alloc_traits::pointer;
  using const_pointer = alloc_traits::const_pointer;

private:
  size_type size_;
  size_type capacity_;

  [[no_unique_address]] alloc alloc_;

public:
  /** @brief Returns a copy of the allocator. */
  [[nodiscard]] Allocator get_allocator() const noexcept {
    return alloc_;
  }

private:
  using holder = detail_::small_vector_holder<T>;

  /** @brief Allocates a buffer for the given number of elements. */
  [[nodiscard]] pointer allocate(size_type count)
      noexcept(noexcept(alloc_traits::allocate(alloc_, count))) {
    pointer ptr{alloc_traits::allocate(alloc_, count)};
    for(size_type i{0}; i < count; ++i) {
      ::new(std::addressof(ptr[i])) holder{};
    }
    return ptr;
  }

  /** @brief Deallocates the given buffer containing the given number of
   * elements. */
  void deallocate(pointer ptr, size_type count) noexcept {
    for(size_type i{0}; i < count; ++i) {
      ptr[i].~holder();
    }
    alloc_traits::deallocate(alloc_, std::move(ptr), count);
  }

  /** @brief Uses the allocator to construct an element at the given address
   * with the given arguments. */
  template<typename... Args>
  requires is_maybe_insertible_v<alloc, T, Args&&...>
  void construct_el(T* t, Args&&... args)
      noexcept(is_maybe_nothrow_insertible_v<alloc, T, Args&&...>) {
    alloc_traits::construct(alloc_, t, std::forward<Args>(args)...);
  }

  /** @brief Uses the allocator to destroy the given element. */
  void destroy_el(T* t) noexcept {
    alloc_traits::destroy(alloc_, t);
  }

  /** @brief Determines whether the reallocation does a copy instead of a move.
   *
   * This happens iff the allocator is std::allocator, the elements are
   * copy-constructible, and the elements' move construction throws. */
  static constexpr bool realloc_does_copy{
    std::same_as<Allocator, std::allocator<T>>
    && std::is_copy_constructible_v<T>
    && !std::is_nothrow_move_constructible_v<T>};

  /** @brief Determines whether the reallocation is non-throwing. */
  static constexpr bool realloc_is_nothrow{realloc_does_copy
    ? std::is_nothrow_copy_constructible_v<T>
    : is_maybe_nothrow_move_insertible_v<alloc, T>};

  /** @brief Returns either an lvalue or an rvalue reference to the argument,
   * depending on whether reallocation does a copy. */
  [[nodiscard]] static constexpr decltype(auto) maybe_move(T& t) noexcept {
    if constexpr(realloc_does_copy) {
      return static_cast<T const&>(t);
    } else {
      return static_cast<T&&>(t);
    }
  }

public:
  // NOLINTBEGIN(bugprone-sizeof-expression)
  static constexpr size_type small_capacity{std::max({N,
    sizeof(pointer) / sizeof(T), static_cast<std::size_t>(1)})};
  // NOLINTEND(bugprone-sizeof-expression)

private:
  union {
    pointer buf_ptr_;
    holder buf_[small_capacity];
  };

  /** @brief Returns a raw pointer to the currently used buffer. */
  [[nodiscard]] holder* get_buf() noexcept {
    return capacity_ <= small_capacity ? buf_ : std::addressof(*buf_ptr_);
  }

  /** @brief Returns a raw pointer to the currently used buffer. */
  [[nodiscard]] holder const* get_buf() const noexcept {
    return capacity_ <= small_capacity ? buf_ : std::addressof(*buf_ptr_);
  }

  /** @brief The factor by which vector capacity is increased on every buffer
   * growth. */
  static constexpr size_type growth_factor{2};

  // Construction & destruction ////////////////////////////////////////////////

  /** @brief Destroys the vector's elements and its allocated storage, if any.
   *
   * This is factored out of the destructor and the exception handlers of
   * various constructors. */
  void destroy() noexcept {
    clear();
    shrink_to_fit();
  }

public:
  /** @brief Constructs an empty vector. */
  explicit small_vector(Allocator allocator = Allocator{}) noexcept:
    size_{0}, capacity_{small_capacity}, alloc_{std::move(allocator)} {}

  /** @brief Constructs a vector containing the given number of
   * default-inserted elements.
   *
   * The element type must be default-insertible.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by the element's default insertion. */
  explicit small_vector(size_type count, Allocator allocator = Allocator{})
      noexcept(is_maybe_nothrow_default_insertible_v<alloc, T>
      && noexcept(allocate(count)))
      requires is_maybe_default_insertible_v<alloc, T>:
      small_vector{std::move(allocator)} {
    this->reserve(count);
    try {
      for(size_type i{0}; i < count; ++i) {
        emplace_back();
      }
    } catch(...) {
      destroy();
      throw;
    }
  }

  /** @brief Constructs a vector containing the elements from the given range.
   *
   * The element type must be constructible with the rvalue result of
   * dereferencing a source iterator as the only insertion argument.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the insertion of elements from the result of
   *   dereferencing an iterator;
   * - if the iterator is not multipass and the element type's move insertion
   *   can throw, then:
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise. */
  template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
  requires is_maybe_insertible_v<alloc, T, std::iter_reference_t<Iterator>&&>
  small_vector(Iterator it, Sentinel iend,
      Allocator allocator = Allocator{})
      noexcept(std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>
      && noexcept(this->append(std::declval<Iterator>(),
      std::declval<Sentinel>()))): small_vector{std::move(allocator)} {
    try {
      this->append(std::move(it), std::move(iend));
    } catch(...) {
      destroy();
      throw;
    }
  }

private:
  struct repeat_tag {};

  explicit small_vector(repeat_tag, util::repeat_view<T, size_type> view)
  noexcept(noexcept(allocate(size_type{})) && is_maybe_nothrow_copy_insertible_v<alloc, T>)
  requires is_maybe_copy_insertible_v<alloc, T>:
  small_vector{view.begin(), view.end()} {}

public:
  /** @brief Constructs a vector containing the given number of elements inserted as copies of
   * the given one.
   *
   * The element type must be copy-insertible.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by the element's copy insertion. */
  small_vector(size_type count, T const& t)
  noexcept(noexcept(allocate(count)) && is_maybe_nothrow_copy_insertible_v<alloc, T>)
  requires is_maybe_copy_insertible_v<alloc, T>:
  small_vector{repeat_tag{}, util::repeat(t, count)} {}

  /** @brief Constructs a vector from the given initializer list.
   *
   * The element type must be copy-insertible.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by the element's copy insertion. */
  small_vector(std::initializer_list<T> il)
    noexcept(noexcept(allocate(0))
    && is_maybe_nothrow_copy_insertible_v<alloc, T>)
    requires is_maybe_copy_insertible_v<alloc, T>:
    small_vector{il.begin(), il.end()} {}

  /** @brief Copy-constructs a vector.
   *
   * The element type must be copy-insertible.
   *
   * May throw:
   * - whatever is thrown by the allocation;
   * - whatever is thrown by an element's copy insertion. */
  small_vector(small_vector const& other, Allocator allocator)
    noexcept(noexcept(allocate(0))
    && is_maybe_nothrow_copy_insertible_v<alloc, T>)
    requires is_maybe_copy_insertible_v<alloc, T>:
    small_vector{other.cbegin(), other.cend(), std::move(allocator)} {}

  /** @brief Copy-constructs a vector, selecting an allocator in a default
   * manner.
   *
   * The element type must be copy-insertible.
   *
   * May throw:
   * - whatever is thrown by the selection of an allocator;
   * - whatever is thrown by the allocation;
   * - whatever is thrown by an element's copy insertion. */
  small_vector(small_vector const& other)
    noexcept(noexcept(allocate(0))
    && is_maybe_nothrow_copy_insertible_v<alloc, T>
    && noexcept(alloc_traits::select_on_container_copy_construction(
    other.alloc_))): small_vector{other,
    alloc_traits::select_on_container_copy_construction(other.alloc_)} {}

private:
  /** @brief Implements move construction by moving the elements individually.
   *
   * This cannot be implemented as a call to assign(), because the element type
   * might not be move-assignable. */
  void move_construct_elements(small_vector&& other)
      noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_move_insertible_v<alloc, T>) {
    assert(other.size() <= capacity_);
    try {
      this->append_impl<false>(std::move_iterator{other.begin()},
        std::move_iterator{other.end()});
    } catch(...) {
      destroy();
      throw;
    }
  }

  /** @brief Implements move construction, allowing stealing the other vector's
   * elements. */
  void move_construct_steal(small_vector&& other)
      noexcept(is_maybe_nothrow_move_insertible_v<alloc, T>) {
    if(other.capacity_ > small_capacity) {
      size_ = other.size_;
      capacity_ = other.capacity_;
      ::new(std::addressof(buf_ptr_)) pointer{std::move(other.buf_ptr_)};
      other.buf_ptr_.~pointer();
      other.size_ = 0;
      other.capacity_ = small_capacity;
    } else {
      this->move_construct_elements(std::move(other));
    }
  }

public:
  /** @brief Move-constructs a vector with the given custom allocator. */
  small_vector(small_vector&& other, Allocator allocator)
      noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_move_insertible_v<alloc, T>):
      size_{0}, capacity_{small_capacity}, alloc_{std::move(allocator)} {
    if constexpr(alloc_traits::is_always_equal::value) {
      this->move_construct_steal(other);
    } else {
      if(alloc_ == other.alloc_) {
        this->move_construct_steal(other);
      } else {
        reserve(other.size());
        this->move_construct_elements(other);
      }
    }
  }

  /** @brief Move-constructs a vector. */
  small_vector(small_vector&& other)
      noexcept(is_maybe_nothrow_move_insertible_v<alloc, T>):
      size_{0}, capacity_{small_capacity}, alloc_{other.alloc_} {
    this->move_construct_steal(std::move(other));
  }

  ~small_vector() noexcept {
    destroy();
  }

  // Assignment ////////////////////////////////////////////////////////////////
private:
  /** @brief A helper function for assign() which does not check whether
   * reallocation is necessary in advance.
   *
   * If a reallocation does become necessary, the behavior depends on the value
   * of the Checked template parameter: if it is true, a (less efficient)
   * reallocation is performed anyway; if it is false, the behavior is
   * undefined.
   *
   * The element type must be insertible with the rvalue result of
   * dereferencing a source iterator as the sole argument and assignable from
   * the same.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by inserting or assigning the elements from the result
   *   of dereferencing the iterators.
   * - on reallocation, if the element type's move insertion can throw:
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is not std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise. */
  template<bool Checked, std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel>
  requires is_maybe_insertible_v<alloc, T, std::iter_reference_t<Iterator>&&>
    && std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void assign_norealloc(Iterator it, Sentinel iend)
      noexcept((!Checked || (noexcept(allocate(0)) && realloc_is_nothrow))
      && is_maybe_nothrow_insertible_v<alloc, T,
        std::iter_reference_t<Iterator>&&>
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>) {
    // First, assign elements into existing storage.
    iterator it_old{begin()};
    iterator const iend_old{end()};
    while(it != iend && it_old != iend_old) {
      *it_old = *it;
      ++it;
      ++it_old;
    }

    if(it_old != iend_old) {
      // If there are old elements left over, get rid of them.
      erase(it_old, iend_old);
    } else {
      // If there are more elements to be added, add them.
      this->append_impl<Checked>(std::move(it), std::move(iend));
    }
  }

public:
  /** @brief Replaces the content of the vector with the given range.
   *
   * The element type must be constructible with the rvalue result of
   * dereferencing a source iterator as the sole argument and assignable from
   * the same.
   *
   * As always, if the resulting vector size is greater than max_size(), the
   * behavior is undefined. Also, if the given range's size does not fit in its
   * difference type, the behavior is undefined.
   *
   * May also throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by constructing or assigning the elements
   *   from the result of dereferencing the iterators.
   * - if the iterator is not multipass and the element type's move insertion
   *   can throw, then:
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise. */
  template<std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
  requires is_maybe_insertible_v<alloc, T, std::iter_reference_t<Iterator>&&>
    && std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void assign(Iterator it, Sentinel iend) noexcept(noexcept(allocate(0))
      && (std::forward_iterator<Iterator> || realloc_is_nothrow)
      && is_maybe_nothrow_insertible_v<alloc, T,
        std::iter_reference_t<Iterator>&&>
      && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>) {
    if constexpr(std::forward_iterator<Iterator>) {
      using range_diff_type = std::iter_difference_t<Iterator>;
      range_diff_type const new_size{util::distance(it, iend)};

      assert(new_size >= 0);
      size_type const new_size_u{static_cast<size_type>(new_size)};
      assert(max_size() > new_size_u);

      if(new_size_u > capacity_) {
        // We need to reallocate. Create and attach a new buffer.
        clear();
        pointer new_buf{allocate(new_size_u)};
        if(capacity_ > small_capacity) {
          this->deallocate(std::move(buf_ptr_), capacity_);
          buf_ptr_ = new_buf;
        } else {
          ::new(std::addressof(buf_ptr_)) pointer{std::move(new_buf)};
        }
        capacity_ = new_size_u;

        // Fill the new buffer.
        for(; it != iend; ++it) {
          this->push_back(*it);
        }
      } else {
        // We do not need to reallocate.
        this->assign_norealloc<false>(std::move(it), std::move(iend));
      }
    } else {
      // We don't know the range size in advance.
      this->assign_norealloc<true>(std::move(it), std::move(iend));
    }
  }

  /** @brief Replaces the content of the vector with the given number of copies
   * of the given object.
   *
   * The element type must be copy-insertible and copy-assignable.
   *
   * May throw:
   * - whatever is thrown by the allocation.
   * - whatever is thrown by copy-inserting or copy-assigning the elements. */
  void assign(size_type count, T const& t) noexcept(noexcept(allocate(count))
      && is_maybe_nothrow_copy_insertible_v<alloc, T>
      && std::is_nothrow_copy_assignable_v<T>)
      requires is_maybe_copy_insertible_v<alloc, T>
      && std::is_copy_assignable_v<T> {
    auto const view{util::repeat(t, count)};
    this->assign(view.begin(), view.end());
  }

  /** @brief Replaces the content of the vector with the given initializer list.
   *
   * The element type must be copy-insertion and copy-assignable.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by copy-inserting or copy-assiging the elements. */
  void assign(std::initializer_list<T> il) noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_copy_insertible_v<alloc, T>
      && std::is_nothrow_copy_assignable_v<T>)
      requires is_maybe_copy_insertible_v<alloc, T>
      && std::is_copy_assignable_v<T> {
    this->assign(il.begin(), il.end());
  }

  /** @brief Replaces the content of the vector with the given initializer list.
   *
   * The element type must be copy-insertible and copy-assignable.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by copy-inserting or copy-assigning the elements. */
  small_vector& operator=(std::initializer_list<T> il)
      noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_copy_insertible_v<alloc, T>
      && std::is_nothrow_copy_assignable_v<T>)
      requires is_maybe_copy_insertible_v<alloc, T>
      && std::is_copy_assignable_v<T> {
    this->assign(il);
    return *this;
  }

  /** @brief Copy-assigns the vector.
   *
   * The element type must be copy-insertible and copy-assignable.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by copy-inserting or copy-assigning the elements. */
  small_vector& operator=(small_vector const& other)
      noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_copy_insertible_v<alloc, T>
      && std::is_nothrow_copy_assignable_v<T>)
      requires is_maybe_copy_insertible_v<alloc, T>
      && std::is_copy_assignable_v<T> {
    if(this == &other) {
      return *this;
    }
    if constexpr(alloc_traits::propagate_on_container_copy_assignment::value) {
      if constexpr(!alloc_traits::is_always_equal::value) {
        if(alloc_ != other.alloc_ && capacity_ > small_capacity) {
          // Get rid of the buffer, using the old allocator.
          clear();
          shrink_to_fit();
        }
      }
      alloc_ = other.alloc_;
    }
    this->assign(other.cbegin(), other.cend());
    return *this;
  }

private:
  /** @brief Implements move assignment by stealing the other vector's buffer.
   */
  void move_assign_steal(small_vector&& other) noexcept {
    assert(other.capacity_ > small_capacity);
    clear();
    shrink_to_fit();
    alloc_ = other.alloc_;
    size_ = other.size_;
    capacity_ = other.capacity_;
    ::new(std::addressof(buf_ptr_)) pointer{std::move(other.buf_ptr_)};
    other.buf_ptr_.~pointer();
    other.size_ = 0;
    other.capacity_ = small_capacity;
  }

public:
  /** @brief Move-assigns the vector.
   *
   * The element type must be move-assignable. */
  small_vector& operator=(small_vector&& other)
      noexcept(std::is_nothrow_move_constructible_v<T>
      && std::is_nothrow_move_assignable_v<T>)
      requires std::is_move_assignable_v<T> {
    if constexpr(alloc_traits::propagate_on_container_move_assignment::value) {
      if(other.capacity_ > small_capacity) {
        this->move_assign_steal(std::move(other));
      } else {
        if constexpr(!alloc_traits::is_always_equal::value) {
          if(alloc_ != other.alloc_ && capacity_ > small_capacity) {
            clear();
            shrink_to_fit();
          }
        }
        alloc_ = other.alloc_;
        this->assign_norealloc<false>(std::move_iterator{other.begin()},
          std::move_iterator{other.end()});
      }
    } else {
      if(other.capacity_ > small_capacity) {
        if constexpr(alloc_traits::is_always_equal::value) {
          this->move_assign_steal(other);
        } else {
          if(alloc_ == other.alloc_) {
            this->move_assign_steal(other);
          } else {
            this->assign(std::move_iterator{other.begin()},
              std::move_iterator{other.end()});
          }
        }
      } else {
        this->assign_norealloc<false>(std::move_iterator{other.begin()},
          std::move_iterator{other.end()});
      }
    }
    return *this;
  }

  // Swap //////////////////////////////////////////////////////////////////////
private:
  /** @brief Implements swapping two big vectors when allocators are
   * incompatible. This vector must not be longer than the other one. */
  void swap_big_big_incompat(small_vector& other)
      noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_move_insertible_v<alloc, T>
      && std::is_nothrow_swappable_v<T>)
      requires std::is_swappable_v<T> {
    assert(capacity_ > small_capacity);
    assert(other.capacity_ > small_capacity);
    assert(size_ <= other.size_);
    using std::swap;

    if(other.size_ <= capacity_) {
      iterator const other_trail{std::swap_ranges(begin(), end(),
        other.begin())};
      this->append_impl<false>(std::move_iterator{other_trail},
        std::move_iterator{other.end()});
      other.erase(other_trail, other.cend());
    } else {
      size_type const new_size{other.size_};
      pointer new_buf{allocate(new_size)};

      // Move other's elements to the new buffer.
      for(size_type i{0}; i < new_size; ++i) {
        try {
          this->construct_el(std::addressof(new_buf[i].t),
            std::move(other.buf_ptr_[i].t));
        } catch(...) {
          while(i > 0) {
            this->destroy_el(std::addressof(new_buf[--i].t));
          }
          this->deallocate(std::move(new_buf), new_size);
          throw;
        }
      }

      // "Move" this one's elements into other. This is actually a swap, because
      // the element type doesn't have to be move-assignable.
      for(size_type i{0}; i < size_; ++i) {
        try {
          swap(buf_ptr_[i].t, other.buf_ptr_[i].t);
        } catch(...) {
          for(size_type j{0}; j < new_size; ++j) {
            this->destroy_el(std::addressof(new_buf[j].t));
          }
          this->deallocate(std::move(new_buf), new_size);
          throw;
        }
      }

      // Destroy the extra elements in the other's buffer.
      for(size_type i{size_}; i < other.size_; ++i) {
        other.destroy_el(std::addressof(other.buf_ptr_[i].t));
      }
      other.size_ = size_;

      // Get rid of our old buffer and install the new one.
      clear();
      shrink_to_fit();
      ::new(std::addressof(buf_ptr_)) pointer{std::move(new_buf)};
      size_ = capacity_ = new_size;
    }
  }

  /** @brief Implements swapping two big vectors. */
  void swap_big_big(small_vector& other)
      noexcept(alloc_traits::propagate_on_container_swap::value
      || alloc_traits::is_always_equal::value || (noexcept(allocate(0))
      && is_maybe_nothrow_move_insertible_v<alloc, T>
      && std::is_nothrow_swappable_v<T>))
      requires std::is_swappable_v<T> {
    assert(capacity_ > small_capacity);
    assert(other.capacity_ > small_capacity);
    using std::swap;

    if constexpr(alloc_traits::propagate_on_container_swap::value
        || alloc_traits::is_always_equal::value) {
      if constexpr(alloc_traits::propagate_on_container_swap::value) {
        swap(alloc_, other.alloc_);
      }
      swap(buf_ptr_, other.buf_ptr_);
      swap(size_, other.size_);
      swap(capacity_, other.capacity_);
    } else {
      if(alloc_ == other.alloc_) {
        swap(buf_ptr_, other.buf_ptr_);
        swap(size_, other.size_);
        swap(capacity_, other.capacity_);
      } else {
        if(size_ > other.size_) {
          other.swap_big_big_incompat(*this);
        } else {
          this->swap_big_big_incompat(other);
        }
      }
    }
  }

  /** @brief Implements swapping this small vector with the other big vector. */
  void swap_small_big(small_vector& other)
      noexcept(std::is_nothrow_swappable_v<T>
      && is_maybe_nothrow_move_insertible_v<alloc, T> &&
      (alloc_traits::propagate_on_container_swap::value
      || alloc_traits::is_always_equal::value || noexcept(allocate(0))))
      requires std::is_swappable_v<T> {
    assert(capacity_ == small_capacity);
    assert(other.capacity_ > small_capacity);

    pointer other_buf_ptr{std::move(other.buf_ptr_)};
    size_type const other_size{other.size_};
    size_type const other_capacity{other.capacity_};

    other.buf_ptr_.~pointer();
    other.size_ = 0;
    other.capacity_ = small_capacity;

    try {
      other.assign_norealloc<false>(std::move_iterator{begin()},
        std::move_iterator{end()});
    } catch(...) {
      for(size_type i{0}; i < other_size; ++i) {
        other.destroy_el(std::addressof(other_buf_ptr[i].t));
      }
      other.deallocate(std::move(other_buf_ptr), other_capacity);
      throw;
    }
    clear();

    if constexpr(alloc_traits::propagate_on_container_swap::value
        || alloc_traits::is_always_equal::value) {
      // We can adopt the other vector's buffer.
      if constexpr(alloc_traits::propagate_on_container_swap::value) {
        using std::swap;
        swap(alloc_, other.alloc_);
      }
      ::new(std::addressof(buf_ptr_)) pointer{std::move(other_buf_ptr)};
      size_ = other_size;
      capacity_ = other_capacity;
    } else {
      if(alloc_ == other.alloc_) {
        // We can adopt the other vector's buffer.
        ::new(std::addressof(buf_ptr_)) pointer{std::move(other_buf_ptr)};
        size_ = other_size;
        capacity_ = other_capacity;
      } else {
        scope_guard guard{[&]() noexcept {
          for(size_type i{0}; i < other_size; ++i) {
            other.destroy_el(std::addressof(other_buf_ptr[i]));
          }
          other.deallocate(std::move(other_buf_ptr));
        }};

        // Move the elements from the other vector's buffer.
        reserve(other_size);
        for(size_type i{0}; i < other_size; ++i) {
          this->emplace_back_impl<false>(std::move(other_buf_ptr[i].t));
        }
      }
    }
  }

  /** @brief Implements swapping two small vectors; this vector must not be
   * longer than the other vector. */
  void swap_small_small(small_vector& other)
      noexcept(std::is_nothrow_swappable_v<T>
      && is_maybe_nothrow_move_insertible_v<alloc, T>)
      requires std::is_swappable_v<T> {
    assert(capacity_ == small_capacity);
    assert(other.capacity_ == small_capacity);
    assert(size_ <= other.size_);

    if constexpr(alloc_traits::propagate_on_container_swap::value) {
      using std::swap;
      swap(alloc_, other.alloc_);
    }
    iterator const other_trail{std::swap_ranges(begin(), end(), other.begin())};
    this->append_impl<false>(std::move_iterator{other_trail},
      std::move_iterator{other.end()});
    other.erase(other_trail, other.cend());
  }

public:
  /** @brief Swaps the two vectors.
   *
   * The element type must be swappable. */
  void swap(small_vector& other) noexcept(std::is_nothrow_swappable_v<T>
      && is_maybe_nothrow_move_insertible_v<alloc, T> &&
      (alloc_traits::propagate_on_container_swap::value
      || alloc_traits::is_always_equal::value || noexcept(allocate(0))))
      requires std::is_swappable_v<T> {
    if(capacity_ > small_capacity) {
      if(other.capacity_ > small_capacity) {
        // Both vectors are big.
        this->swap_big_big(other);
      } else {
        // This vector is big, the other one is small.
        other.swap_small_big(*this);
      }
    } else {
      if(other.capacity_ > small_capacity) {
        // This vector is small, the other one is big.
        this->swap_small_big(other);
      } else {
        // Both vectors are small.
        if(size_ > other.size_) {
          other.swap_small_small(*this);
        } else {
          this->swap_small_small(other);
        }
      }
    }
  }

  // Element access ////////////////////////////////////////////////////////////
  /** @brief Accesses the element at the given index.
   *
   * @throws std::out_of_range If the index is out of range. */
  [[nodiscard]] T& at(size_type index) {
    if(index >= size_) {
      throw std::out_of_range{"small_vector::at: index out of range"};
    }
    return operator[](index);
  }

  /** @brief Accesses the element at the given index.
   *
   * @throws std::out_of_range If the index is out of range. */
  [[nodiscard]] T const& at(size_type index) const {
    if(index >= size_) {
      throw std::out_of_range{"small_vector::at: index out of range"};
    }
    return operator[](index);
  }

  /** @brief Accesses the element at the given index.
   *
   * If the index is out of range, the behavior is undefined. */
  [[nodiscard]] T& operator[](size_type index) noexcept {
    assert(index < size());
    return get_buf()[index].t;
  }

  /** @brief Accesses the element at the given index.
   *
   * If the index is out of range, the behavior is undefined. */
  [[nodiscard]] T const& operator[](size_type index) const noexcept {
    assert(index < size());
    return get_buf()[index].t;
  }

  /** @brief Accesses the first element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] T& front() noexcept {
    assert(!empty());
    return operator[](0);
  }

  /** @brief Accesses the first element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] T const& front() const noexcept {
    assert(!empty());
    return operator[](0);
  }

  /** @brief Accesses the last element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] T& back() noexcept {
    assert(!empty());
    return operator[](size() - 1);
  }

  /** @brief Accesses the last element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] T const& back() const noexcept {
    assert(!empty());
    return operator[](size() - 1);
  }

  // Iterators /////////////////////////////////////////////////////////////////

private:
  class const_iterator_core;

  class iterator_core {
    friend class const_iterator_core;

  private:
    holder* ptr_;

  protected:
    [[nodiscard]] T& dereference() const noexcept {
      return ptr_->t;
    }

    void advance(std::ptrdiff_t off) noexcept {
      ptr_ += off;
    }

    [[nodiscard]] std::ptrdiff_t distance_to(iterator_core other)
        const noexcept {
      return other.ptr_ - ptr_;
    }

  private:
    friend class small_vector;
    explicit iterator_core(holder* ptr) noexcept: ptr_{ptr} {}

  public:
    iterator_core() noexcept = default;
  };

public:
  using iterator = iterator_facade<iterator_core>;

private:
  class const_iterator_core {
  private:
    holder const* ptr_;

  protected:
    [[nodiscard]] T const& dereference() const noexcept {
      return ptr_->t;
    }

    void advance(std::ptrdiff_t off) noexcept {
      ptr_ += off;
    }

    [[nodiscard]] std::ptrdiff_t distance_to(const_iterator_core other)
        const noexcept {
      return other.ptr_ - ptr_;
    }

  private:
    friend class small_vector;
    explicit const_iterator_core(holder const* ptr) noexcept: ptr_{ptr} {}

  public:
    const_iterator_core() noexcept = default;
    const_iterator_core(iterator other) noexcept:
      const_iterator_core{other.ptr_} {}
  };

public:
  using const_iterator = iterator_facade<const_iterator_core>;

  // Iterators have to be constructed through local variables, because otherwise
  // the private constructor will be inaccessible due to Clang bug 48545
  // https://bugs.llvm.org/show_bug.cgi?id=48545
  [[nodiscard]] iterator begin() noexcept {
    iterator it{get_buf()};
    return it;
  }
  [[nodiscard]] const_iterator begin() const noexcept {
    const_iterator it{get_buf()};
    return it;
  }
  [[nodiscard]] const_iterator cbegin() const noexcept { return begin(); }

  [[nodiscard]] iterator end() noexcept {
    iterator it{get_buf() + size_};
    return it;
  }
  [[nodiscard]] const_iterator end() const noexcept {
    const_iterator it{get_buf() + size_};
    return it;
  }
  [[nodiscard]] const_iterator cend() const noexcept { return end(); }

  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  [[nodiscard]] reverse_iterator rbegin() noexcept {
    return reverse_iterator{end()};
  }
  [[nodiscard]] const_reverse_iterator rbegin() const noexcept {
    return const_reverse_iterator{end()};
  }
  [[nodiscard]] const_reverse_iterator crbegin() const noexcept {
    return rbegin();
  }

  [[nodiscard]] reverse_iterator rend() noexcept {
    return reverse_iterator{begin()};
  }
  [[nodiscard]] const_reverse_iterator rend() const noexcept {
    return const_reverse_iterator{begin()};
  }
  [[nodiscard]] const_reverse_iterator crend() const noexcept { return rend(); }

  // Size & capacity ///////////////////////////////////////////////////////////

  [[nodiscard]] size_type size() const noexcept { return size_; }
  [[nodiscard]] bool empty() const noexcept { return size() == 0; }
  [[nodiscard]] size_type capacity() const noexcept { return capacity_; }

  /** @brief Returns the maximum possible size of a vector. */
  [[nodiscard]] constexpr size_type max_size() const noexcept {
    // The size of the array, in bytes, must always be representable as a value
    // of both size_type and difference_type.
    constexpr size_type base_result{
      (sizeof(difference_type) <= sizeof(size_type)
      ? static_cast<size_type>(std::numeric_limits<difference_type>::max())
      : std::numeric_limits<size_type>::max()) / sizeof(T)};
    static_assert(small_capacity <= base_result);
    return std::max(std::min(base_result, alloc_traits::max_size(alloc_)),
      small_capacity);
  }

private:
  /** @brief Switches to the new buffer, as part of either reserve() or
   * shrink_to_fit().
   *
   * For thrown exceptions and exception safety guarantees, see those two
   * functions.
   *
   * It is the caller's responsibility:
   * - if an exception is thrown, to destroy the new buffer or reinstall the old
   *   buffer;
   * - in case of success, to install the new buffer and/or to destroy the old
   *   one. */
  void change_buffer(holder* old_buf, holder* new_buf)
      noexcept(realloc_is_nothrow) {
    if constexpr(realloc_is_nothrow) {
      // Copy or move elements to the new buffer
      for(size_type i{0}; i < size_; ++i) {
        this->construct_el(std::addressof(new_buf[i].t),
          this->maybe_move(old_buf[i].t));
        this->destroy_el(std::addressof(old_buf[i].t));
      }
    } else {
      // Copy or move elements to the new buffer
      for(size_type i{0}; i < size_; ++i) {
        try {
          this->construct_el(std::addressof(new_buf[i].t),
            this->maybe_move(old_buf[i].t));
        } catch(...) {
          while(i > 0) {
            --i;
            this->destroy_el(std::addressof(new_buf[i].t));
          }
          throw;
        }
      }

      // Destroy elements from the old buffer
      {
        size_type i{size_};
        while(i > 0) {
          --i;
          this->destroy_el(std::addressof(old_buf[i].t));
        }
      }
    }
  }

public:
  /** @brief Reserves enough capacity to store the given number of elements.
   *
   * May also throw:
   * - whatever is thrown by allocation;
   * - if the element type's move insertion can throw;
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise.
   *
   * Exception safety guarantee: If move insertion throws and either the
   * allocator is not std::allocator or the element type is not
   * copy-constructible, standard guarantee. In all other circumstances, strong
   * guarantee.
   *
   * @throws std::length_error If the requested size is larger than maximum. */
  void reserve(size_type new_capacity) {
    if(new_capacity <= capacity_) {
      return;
    }
    if(new_capacity > max_size()) {
      throw std::length_error{"util::small_vector maximum length exceeded"};
    }

    holder* const old_buf{get_buf()};
    pointer new_buf{allocate(new_capacity)};
    try {
      change_buffer(old_buf, std::addressof(*new_buf));
    } catch(...) {
      this->deallocate(std::move(new_buf), new_capacity);
      throw;
    }

    if(capacity_ == small_capacity) {
      ::new(std::addressof(buf_ptr_)) pointer{std::move(new_buf)};
    } else {
      this->deallocate(std::move(buf_ptr_), capacity_);
      buf_ptr_ = new_buf;
    }
    capacity_ = new_capacity;

    assert(capacity() >= new_capacity);
  }

  /** @brief Reduces the vector's capacity as much as possible.
   *
   * May throw:
   * - whatever is thrown by the allocation;
   * - if the element type's move insertion can throw:
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is not std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise.
   *
   * Exception safety guarantee: If move insertion throws and either the
   * allocator is not std::allocator or the element type is not
   * copy-constructible, standard guarantee. In all other circumstances, strong
   * guarantee. */
  void shrink_to_fit() noexcept(noexcept(allocate(0)) && realloc_is_nothrow) {
    size_type const new_capacity{size_ <= small_capacity
      ? small_capacity : size_};
    if(new_capacity == capacity_) {
      return;
    }

    assert(capacity_ > small_capacity);
    pointer old_buf{buf_ptr_};

    if(new_capacity == small_capacity) {
      buf_ptr_.~pointer();
      try {
        change_buffer(std::addressof(*old_buf), buf_);
      } catch(...) {
        ::new(std::addressof(buf_ptr_)) pointer{old_buf};
        throw;
      }
    } else {
      pointer new_buf{allocate(new_capacity)};
      try {
        change_buffer(std::addressof(*old_buf), std::addressof(*new_buf));
      } catch(...) {
        this->deallocate(std::move(new_buf), new_capacity);
        throw;
      }
      buf_ptr_ = std::move(new_buf);
    }

    this->deallocate(std::move(old_buf), capacity_);
    capacity_ = new_capacity;
  }

  // Emplacement ///////////////////////////////////////////////////////////////

  /** @brief Emplaces an element immediately before the given one.
   *
   * The element type must be move-assignable.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by the emplaced element's insertion;
   * - whatever is thrown by an element's move assignment or insertion;
   * - on reallocation, if the element type's move insertion can throw;
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise.
   *
   * Exception safety guarantees
   * ===========================
   * On reallocation, if an element's move insertion throws, standard guarantee;
   * if the allocator is std::allocator an element's copy constructor throws,
   * strong guarantee.
   *
   * If the emplaced element's insertion throws, then standard guarantee,
   * except that there is a strong guarantee if:
   * - move insertion is non-throwing; or
   * - the allocator is std::allocator and the element type is
   *   copy-constructible.
   *
   * In other situations, standard guarantee.
   *
   * @returns An iterator to the newly emplaced element. */
  template<typename... Args>
  requires is_maybe_insertible_v<alloc, T, Args&&...>
    && std::is_move_assignable_v<T>
  iterator emplace(const_iterator pos_it, Args&&... args)
      noexcept(noexcept(allocate(0)) && realloc_is_nothrow
      && is_maybe_nothrow_insertible_v<alloc, T, Args&&...>
      && is_maybe_nothrow_move_insertible_v<alloc, T>
      && std::is_nothrow_move_assignable_v<T>) {
    assert(size() < max_size());
    if(pos_it == cend()) {
      this->emplace_back(std::forward<Args>(args)...);
      return end() - 1;
    }
    size_type const pos{util::asserted_cast<size_type>(pos_it - cbegin())};

    if(size_ == capacity_) {
      // We need to reallocate
      holder* const old_buf{get_buf()};
      size_type const new_capacity{max_size() / growth_factor < capacity_
        ? max_size() : capacity_ * growth_factor};
      pointer new_buf{allocate(new_capacity)};

      if constexpr(realloc_is_nothrow
          && is_maybe_nothrow_insertible_v<alloc, T, Args&&...>) {
        // Copy or move the preceding elements to the new buffer
        for(size_type i{0}; i < pos; ++i) {
          this->construct_el(std::addressof(new_buf[i].t),
            this->maybe_move(old_buf[i].t));
          this->destroy_el(std::addressof(old_buf[i].t));
        }

        // Emplace the element
        this->construct_el(std::addressof(new_buf[pos].t),
          std::forward<Args>(args)...);

        // Copy or move the following elements to the new buffer
        for(size_type i{pos}; i < size_; ++i) {
          this->construct_el(std::addressof(new_buf[i + 1].t),
            this->maybe_move(old_buf[i].t));
          this->destroy_el(std::addressof(old_buf[i].t));
        }
      } else {
        // Copy or move the preceding elements to the new buffer
        for(size_type i{0}; i < pos; ++i) {
          try {
            this->construct_el(std::addressof(new_buf[i].t),
              this->maybe_move(old_buf[i].t));
          } catch(...) {
            while(i > 0) {
              --i;
              this->destroy_el(std::addressof(new_buf[i].t));
            }
            this->deallocate(std::move(new_buf), new_capacity);
            throw;
          }
        }

        // Emplace the element
        try {
          this->construct_el(std::addressof(new_buf[pos].t),
            std::forward<Args>(args)...);
        } catch(...) {
          if constexpr(is_maybe_nothrow_move_insertible_v<alloc, T>) {
            // If the move is non-throwing, move the rest of the old buffer and
            // provide the strong guarantee.
            for(size_type i{pos}; i < size_; ++i) {
              this->construct_el(std::addressof(new_buf[i].t),
                std::move(old_buf[i].t));
            }
            if(capacity_ > small_capacity) {
              this->deallocate(std::move(buf_ptr_), capacity_);
              buf_ptr_ = std::move(new_buf);
            } else {
              ::new(std::addressof(buf_ptr_)) pointer{std::move(new_buf)};
            }
            capacity_ = new_capacity;
          } else {
            // If the move is throwing, we can keep the old buffer. Either we've
            // been copying and can thus provide the strong guarantee, or there
            // is no copy and therefore can't do much.
            size_type i{size_};
            while(i > 0) {
              --i;
              this->destroy_el(std::addressof(new_buf[i].t));
            }
            this->deallocate(std::move(new_buf), new_capacity);
          }
          throw;
        }

        // Copy or move the following elements to the new buffer
        for(size_type i{pos}; i < size_; ++i) {
          try {
            this->construct_el(std::addressof(new_buf[i + 1].t),
              this->maybe_move(old_buf[i].t));
          } catch(...) {
            ++i; // New buffer has one more element than the old one!
            while(i > 0) {
              --i;
              this->destroy_el(std::addressof(new_buf[i].t));
            }
            this->deallocate(std::move(new_buf), new_capacity);
            throw;
          }
        }

        // Destroy elements from the old buffer
        {
          size_type i{size_};
          while(i > 0) {
            --i;
            this->destroy_el(std::addressof(old_buf[i].t));
          }
        }
      }

      // Install the new buffer
      if(capacity_ > small_capacity) {
        this->deallocate(std::move(buf_ptr_), capacity_);
        buf_ptr_ = std::move(new_buf);
      } else {
        ::new(std::addressof(buf_ptr_)) pointer{std::move(new_buf)};
      }
      capacity_ = new_capacity;

      ++size_;
    } else {
      // We do not need to reallocate. (Note that we're not emplacing at the
      // end.)

      // Construct the new element first. If copy/move construction can throw,
      // let this throw first
      alignas(T) std::byte new_el[sizeof(T)];
      this->construct_el(reinterpret_cast<T*>(&new_el),
        std::forward<Args>(args)...);
      scope_guard const guard{[&new_el, this]() noexcept {
        this->destroy_el(std::launder(reinterpret_cast<T*>(&new_el)));
      }};
      holder* const buf{get_buf()};

      // Copy or move the last element
      this->construct_el(std::addressof(buf[size_].t),
        std::move(buf[size_ - 1].t));
      ++size_;

      // Move-assign intermediate elements
      for(size_type i{size_ - 2}; i > pos; --i) {
        buf[i].t = std::move(buf[i - 1].t);
      }

      // Move the new element into place
      buf[pos].t = std::move(*std::launder(reinterpret_cast<T*>(&new_el)));
    }
    return begin() + pos;
  }

private:
  /** @brief Implements emplace_back with an extra template parameter.
   *
   * If there is not enough capacity, behavior depends on the Checked parameter:
   * if it is true, a reallocation occurs, just like with emplace_back; if it is
   * false, the behavior is undefined. */
  template<bool Checked, typename... Args>
  requires is_maybe_insertible_v<alloc, T, Args&&...>
  T& emplace_back_impl(Args&&... args)
      noexcept(is_maybe_nothrow_insertible_v<alloc, T, Args&&...>
      && (!Checked || (noexcept(allocate(0)) && realloc_is_nothrow))) {
    if constexpr(Checked) {
      assert(size() < max_size());
      if(size_ == capacity_) {
        reserve(max_size() / growth_factor < capacity_ ? max_size()
          : capacity_ * growth_factor);
      }
    } else {
      assert(size() < capacity());
    }

    T& result{get_buf()[size_].t};
    this->construct_el(std::addressof(result), std::forward<Args>(args)...);
    ++size_;
    return result;
  }

public:
  /** @brief Emplaces an element at the end of the vector.
   *
   * May throw:
   * - whatever is thrown by the allocation;
   * - whatever is thrown by the emplaced element's insertion;
   * - if the element type's move insertion can throw;
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise.
   *
   * Exception safety guarantees: If an element's move insertion throws (except
   * that of the newly emplaced element), standard guarantee. In all other
   * circumstances, strong guarantee (except that the capacity may have
   * changed).
   *
   * @returns A reference to the newly emplaced element. */
  template<typename... Args>
  requires is_maybe_insertible_v<alloc, T, Args&&...>
  T& emplace_back(Args&&... args) noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_insertible_v<alloc, T, Args&&...>
      && realloc_is_nothrow) {
    assert(size() < max_size());
    return this->emplace_back_impl<true>(std::forward<Args>(args)...);
  }

  /** @brief Emplaces an element at the beginning of the vector.
   *
   * The element type must be move-assignable.
   *
   * See emplace() for thrown exceptions and exception safety guarantees.
   *
   * @returns A reference to the newly emplaced element. */
  template<typename... Args>
  requires is_maybe_insertible_v<alloc, T, Args&&...>
    && std::is_move_assignable_v<T>
  T& emplace_front(Args&&... args)
      noexcept(noexcept(allocate(0)) && realloc_is_nothrow
      && is_maybe_nothrow_insertible_v<alloc, T, Args&&...>
      && is_maybe_nothrow_move_insertible_v<alloc, T>
      && std::is_nothrow_move_assignable_v<T>) {
    assert(size() < max_size());
    return *this->emplace(cbegin(), std::forward<Args>(args)...);
  }

  // Insertion /////////////////////////////////////////////////////////////////

  /** @brief Inserts the given range immediately before the element pointed to
   * by the given iterator.
   *
   * The element type must be move-assignable. It must also be insertible
   * with an rvalue result of dereferencing a source iterator as the sole
   * argument. Further, it must be assignable from the rvalue result of
   * dereferencing a source iterator.
   *
   * As always, if the resulting vector size is greater than max_size(), the
   * behavior is undefined. Also, if the given range's size does not fit in its
   * difference type, the behavior is undefined.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the insertion or assignment of elements from
   *   the result of dereferencing an iterator;
   * - whatever is thrown by move-insertion or move-assignment of elements;
   * - on reallocation, if the element type's move insertion can throw;
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise.
   *
   * @returns An iterator to the beginning of the inserted range. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::is_move_assignable_v<T>
    && is_maybe_insertible_v<alloc, T, std::iter_reference_t<Iterator>&&>
    && std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  iterator insert(const_iterator pos_it, Iterator it, Sentinel iend)
      noexcept(noexcept(this->replace(pos_it, pos_it, it, iend))
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>) {
    return this->replace(pos_it, pos_it, std::move(it), std::move(iend));
  }

  /** @brief Inserts the provided initializer list immediately before the
   * element pointed to by the given iterator.
   *
   * The element type must be move-assignable, copy-insertible, and
   * copy-assignable.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by copy- or move-insertion or assignment of elements.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator insert(const_iterator pos, std::initializer_list<T> il)
      noexcept(noexcept(allocate(0))
      && realloc_is_nothrow && is_maybe_nothrow_copy_insertible_v<alloc, T>
      && std::is_nothrow_copy_assignable_v<T>)
      requires is_maybe_copy_insertible_v<alloc, T>
      && std::is_copy_assignable_v<T> && std::is_move_assignable_v<T> {
    assert(il.size() <= static_cast<std::make_unsigned_t<std::ptrdiff_t>>(
      std::numeric_limits<std::ptrdiff_t>::max()));
    assert(max_size() >= il.size());
    assert(max_size() - il.size() >= size());
    return this->insert(pos, il.begin(), il.end());
  }

  /** @brief Inserts the given number of copies of the provided element
   * immediately before the element pointed to by the given iterator.
   *
   * The element type must be move-assignable, copy-insertible, and
   * copy-assignable.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by copy- or move-insertion or assignment of elements.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator insert(const_iterator pos, size_type count, T const& t)
      noexcept(noexcept(allocate(0)) && realloc_is_nothrow
      && is_maybe_nothrow_copy_insertible_v<alloc, T>
      && std::is_nothrow_copy_assignable_v<T>)
      requires is_maybe_copy_insertible_v<alloc, T>
      && std::is_copy_assignable_v<T> && std::is_move_assignable_v<T> {
    assert(max_size() >= count);
    assert(max_size() - count >= size());
    auto const view{util::repeat(t, count)};
    return this->insert(pos, view.begin(), view.end());
  }

  /** @brief Inserts the given element immediately before the element pointed to
   * by the given iterator.
   *
   * The element type must be move-assignable.
   *
   * See emplace() for thrown exceptions and exception safety guarantees. (The
   * new element is move-constructed from the one passed to this function.)
   *
   * @returns An iterator to the newly inserted element. */
  iterator insert(const_iterator pos_it, T t)
      noexcept(noexcept(allocate(0)) && realloc_is_nothrow
      && is_maybe_nothrow_move_insertible_v<alloc, T>
      && std::is_nothrow_move_assignable_v<T>)
      requires std::is_move_assignable_v<T> {
    assert(size() < max_size());
    return this->emplace(pos_it, std::move(t));
  }

private:
  /** @brief Implements append with an extra template parameter.
   *
   * If a reallocation is necessary, the behavior depends on the template
   * parameter: if it is true, a reallocation occurs (same as append); if it is
   * false, the behavior is undefined. */
  template<bool Checked, std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires is_maybe_insertible_v<alloc, T, std::iter_reference_t<Iterator>&&>
  void append_impl(Iterator it, Sentinel iend)
  noexcept(
    noexcept(allocate(0))
    && is_maybe_nothrow_insertible_v<alloc, T, std::iter_reference_t<Iterator>&&>
    && noexcept(it != iend) && noexcept(*it) && noexcept(++it)
    && (!Checked || (
      realloc_is_nothrow
      && (!std::forward_iterator<Iterator> || noexcept(util::distance(it, iend)))
    ))
  ) {
    assert(size() < max_size());
    constexpr bool is_multipass{std::forward_iterator<Iterator>};
    if constexpr(Checked && is_multipass) {
      using range_diff_type = std::iter_difference_t<Iterator>;
      range_diff_type const range_size{util::distance(it, iend)};
      assert(range_size >= 0);
      size_type const range_size_u{static_cast<size_type>(range_size)};
      assert(max_size() - size() >= range_size_u);
      this->reserve(size() + range_size_u);
    }

    for(; it != iend; ++it) {
      this->emplace_back_impl<Checked && !is_multipass>(*it);
    }
  }

public:
  /** @brief Inserts the given range at the end of the vector.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the insertion of elements from the result of
   *   dereferencing an iterator;
   * - if the element type's move insertion can throw, then:
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise.
   *
   * As always, if the resulting vector size is greater than max_size(), the
   * behavior is undefined. Also, if the given range's size does not fit in its
   * difference type, the behavior is undefined.
   *
   * Exception safety guarantees: strong guarantee if the range has one element
   * or if a copy constructor throws during reallocation and the allocator is
   * std::allocator, standard guarantee otherwise. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires is_maybe_insertible_v<alloc, T, std::iter_reference_t<Iterator>&&>
  void append(Iterator it, Sentinel iend) noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_insertible_v<alloc, T,
        std::iter_reference_t<Iterator>&&>
      && std::is_nothrow_move_constructible_v<Iterator>
      && std::is_nothrow_move_constructible_v<Sentinel>
      && noexcept(it != iend) && noexcept(*it) && noexcept(++it)
      && realloc_is_nothrow
      && (!std::forward_iterator<Iterator>
      || noexcept(util::distance(it, iend)))) {
    assert(size() < max_size());
    this->append_impl<true>(std::move(it), std::move(iend));
  }

  /** @brief Inserts the given element at the end of the vector.
   *
   * See emplace_back() for thrown exceptions and exception safety guarantees.
   * (The new element is move-constructed from the one passed to this function.)
   */
  void push_back(T t) noexcept(noexcept(allocate(0))
      && is_maybe_nothrow_move_insertible_v<alloc, T> && realloc_is_nothrow) {
    assert(size() < max_size());
    this->emplace_back(std::move(t));
    assert(size() > 0);
  }

  /** @brief Inserts the given range at the beginning of the vector.
   *
   * The element type must be move-assignable. It must also be insertible
   * with an rvalue result of dereferencing a source iterator as the sole
   * argument. Further, it must be assignable from the rvalue result of
   * dereferencing a source iterator.
   *
   * As always, if the resulting vector size is greater than max_size(), the
   * behavior is undefined. Also, if the given range's size does not fit in its
   * difference type, the behavior is undefined.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the insertion or assignment of elements from
   *   the result of dereferencing an iterator;
   * - whatever is thrown by move-insertion or move-assignment of elements;
   * - on reallocation, if the element type's move insertion can throw;
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::is_move_assignable_v<T>
    && is_maybe_insertible_v<alloc, T, std::iter_reference_t<Iterator>&&>
    && std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  void prepend(Iterator it, Sentinel iend)
      noexcept(noexcept(this->insert(cbegin(), std::move(it),
      std::move(iend)))) {
    this->insert(cbegin(), std::move(it), std::move(iend));
  }

  /** @brief Inserts the given element at the beginning of the vector.
   *
   * The element type must be move-assignable.
   *
   * See emplace_front() for thrown exceptions and exception safety guarantees.
   * (The new element is move-constructed from the one passed to this function.)
   */
  void push_front(T t) noexcept(noexcept(allocate(0)) && realloc_is_nothrow
      && is_maybe_nothrow_move_insertible_v<alloc, T>
      && std::is_nothrow_move_assignable_v<T>)
      requires std::is_move_assignable_v<T> {
    assert(size() < max_size());
    this->emplace_front(std::move(t));
    assert(size() > 0);
  }

  // Erasure ///////////////////////////////////////////////////////////////////

  /** @brief Deletes all elements in the vector. */
  void clear() noexcept {
    this->erase_after(cbegin());
  }

  /** @brief Erases the elements starting from the one specified by the first
   * iterator and ending immediately before the second iterator.
   *
   * The element type must be move-assignable.
   *
   * @returns An iterator pointing to the first element past the erased range.
   *          If the range is at the end of the vector, returns a past-the-end
   *          iterator. */
  iterator erase(const_iterator it, const_iterator iend)
      noexcept(std::is_nothrow_move_assignable_v<T>)
      requires std::is_move_assignable_v<T> {
    assert(it <= iend);

    holder* const buf{get_buf()};
    size_type const pos_begin{util::asserted_cast<size_type>(it - cbegin())};
    size_type const pos_end{util::asserted_cast<size_type>(iend - cbegin())};
    size_type const range_size{pos_end - pos_begin};
    if(range_size == 0) {
      return begin() + pos_begin;
    }

    for(size_type i{pos_end}; i < size_; ++i) {
      buf[i - range_size].t = std::move(buf[i].t);
    }

    for(size_type i{size_ - range_size}; i < size_; ++i) {
      this->destroy_el(std::addressof(buf[i].t));
    }

    size_ -= range_size;
    return begin() + pos_begin;
  }

  /** @brief Erases the element pointed to by the given iterator.
   *
   * The element type must be move-assignable.
   *
   * @returns An iterator pointing to the element after the erased one. If the
   *          element was at the end of the vector, returns a past-the-end
   *          iterator. */
  iterator erase(const_iterator it)
      noexcept(std::is_nothrow_move_assignable_v<T>)
      requires std::is_move_assignable_v<T> {
    assert(it != cend());
    return erase(it, it + 1);
  }

  /** @brief Erases all elements from the given position to the end of the
   * vector. */
  void erase_after(const_iterator it) noexcept {
    holder* const buf{get_buf()};
    size_type const new_size{util::asserted_cast<size_type>(it - begin())};
    for(size_type i{new_size}; i < size_; ++i) {
      this->destroy_el(std::addressof(buf[i].t));
    }
    size_ = new_size;
  }

  /** @brief Deletes the last element. */
  void pop_back() noexcept {
    assert(size() > 0);
    --size_;
    this->destroy_el(std::addressof(get_buf()[size_].t));
  }

  /** @brief Erases all elements from the beginning of the vector to the given
   * position (not including the element pointed to by the iterator).
   *
   * The element type must be move-assignable. */
  void erase_before(const_iterator it)
      noexcept(std::is_nothrow_move_assignable_v<T>)
      requires std::is_move_assignable_v<T> {
    this->erase(cbegin(), it);
  }

  /** @brief Deletes the first element.
   *
   * The element type must be move-assignable. */
  void pop_front() noexcept(std::is_nothrow_move_assignable_v<T>)
      requires std::is_move_assignable_v<T> {
    assert(size() > 0);
    erase(cbegin());
  }

  // Resizing //////////////////////////////////////////////////////////////////

  /** @brief Changes the size of the vector to the given size, padding with
   * default-constructed elements if needed.
   *
   * The element type must be default-insertible.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by the element type's default insertion;
   * - if the element type's move insertion can throw;
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise. */
  void resize(size_type new_size)
      noexcept(noexcept(allocate(0)) && realloc_is_nothrow
      && is_maybe_nothrow_default_insertible_v<alloc, T>)
      requires is_maybe_default_insertible_v<alloc, T> {
    assert(new_size <= max_size());
    if(new_size < size_) {
      this->erase_after(cbegin() + new_size);
    } else {
      reserve(new_size);
      while(size_ < new_size) {
        emplace_back_impl<false>();
      }
    }
  }

  /** @brief Changes the size of the vector to the given size, padding with
   * copies of the given element if needed.
   *
   * The element type must be copy-insertible.
   *
   * May also throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by the element type's copy insertion;
   * - if the element type's move insertion can throw;
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise. */
  void resize(size_type new_size, T const& t)
      noexcept(noexcept(allocate(0)) && realloc_is_nothrow
      && is_maybe_nothrow_copy_insertible_v<alloc, T>)
      requires is_maybe_copy_insertible_v<alloc, T> {
    assert(new_size <= max_size());
    if(new_size < size_) {
      this->erase_after(cbegin() + new_size);
    } else {
      reserve(new_size);
      while(size_ < new_size) {
        this->emplace_back_impl<false>(t);
      }
    }
  }

  // Replacement ///////////////////////////////////////////////////////////////

  /** @brief Replaces the given range within this vector with the other provided
   * range.
   *
   * The element type must be move-assignable. It must also be insertible
   * with an rvalue result of dereferencing a source iterator as the sole
   * argument. Further, it must be assignable from the rvalue result of
   * dereferencing a source iterator.
   *
   * As always, if the resulting vector size is greater than max_size(), the
   * behavior is undefined. Also, if the given range's size does not fit in its
   * difference type, the behavior is undefined.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by various operations on the iterator;
   * - whatever is thrown by the insertion or assignment of elements from
   *   the result of dereferencing an iterator;
   * - whatever is thrown by move-insertion or move-assignment of elements;
   * - on reallocation, if the element type's move insertion can throw;
   *   - whatever is thrown by an element's copy constructor, if it exists and
   *     the allocator is std::allocator;
   *   - whatever is thrown by an element's move insertion, otherwise.
   *
   * @returns An iterator to the beginning of the inserted range. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::is_move_assignable_v<T>
    && is_maybe_insertible_v<
      detail_::small_vector_allocator<T, Allocator>, T,
      std::iter_reference_t<Iterator>&&>
    && std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
  iterator replace(const_iterator it_old, const_iterator iend_old,
    Iterator it_new, Sentinel iend_new)
    noexcept(noexcept(allocate(0)) && realloc_is_nothrow
    && is_maybe_nothrow_insertible_v<alloc, T,
      std::iter_reference_t<Iterator>&&>
    && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
    && noexcept(it_new != iend_new) && noexcept(*it_new) && noexcept(++it_new)
    && (!std::forward_iterator<Iterator>
    || (std::is_nothrow_copy_constructible_v<Iterator>
    && std::is_nothrow_copy_constructible_v<Sentinel>
    && noexcept(util::next_until(it_new, iend_new, 0))
    && noexcept(util::distance(it_new, iend_new)))));

  /** @brief Replaces the given range within this vector with the provided
   * initializer list.
   *
   * The element type must be move-assignable, copy-insertible, and
   * copy-assignable.
   *
   * As always, if the resulting vector size is greater than max_size(), the
   * behavior is undefined. Also, if the given range's size does not fit in its
   * difference type, the behavior is undefined.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by copy- or move-insertion or assignment of elements.
   *
   * @returns An iterator to the beginning of the inserted range. */
  iterator replace(const_iterator it_old, const_iterator iend_old,
      std::initializer_list<T> il) noexcept(noexcept(allocate(0))
      && realloc_is_nothrow && is_maybe_nothrow_copy_insertible_v<alloc, T>
      && std::is_nothrow_copy_assignable_v<T>)
      requires std::is_move_assignable_v<T>
      && is_maybe_copy_insertible_v<alloc, T> && std::is_copy_assignable_v<T> {
    assert(it_old <= iend_old);
    return this->replace(it_old, iend_old, il.begin(), il.end());
  }

  /** @brief Replaces the given range within this vector with the given number
   * of copies of the provided element.
   *
   * The element type must be move-assignable, copy-insertible, and
   * copy-assignable.
   *
   * As always, if the resulting vector size is greater than max_size(), the
   * behavior is undefined. Also, if the given range's size does not fit in its
   * difference type, the behavior is undefined.
   *
   * May throw:
   * - whatever is thrown by allocation;
   * - whatever is thrown by copy- or move-insertion or assignment of elements.
   *
   * @returns An iterator to the beginning of the inserted range.
   * @throws std::bad_alloc */
  iterator replace(const_iterator it_old, const_iterator iend_old,
      size_type count, T const& t) noexcept(noexcept(allocate(0))
      && realloc_is_nothrow && is_maybe_nothrow_copy_insertible_v<alloc, T>
      && std::is_nothrow_copy_assignable_v<T>)
      requires std::is_move_assignable_v<T>
      && is_maybe_copy_insertible_v<alloc, T> && std::is_copy_assignable_v<T> {
    assert(it_old <= iend_old);
    auto const view{util::repeat(t, count)};
    return this->replace(it_old, iend_old, view.begin(), view.end());
  }
};

template<typename T, std::size_t N, typename Allocator>
template<std::input_iterator Iterator,
  std::sentinel_for<Iterator> Sentinel>
requires std::is_move_assignable_v<T>
  && is_maybe_insertible_v<detail_::small_vector_allocator<T, Allocator>,
    T, std::iter_reference_t<Iterator>&&>
  && std::is_assignable_v<T&, std::iter_reference_t<Iterator>&&>
typename small_vector<T, N, Allocator>::iterator
    small_vector<T, N, Allocator>::replace(
    const_iterator it_old, const_iterator iend_old,
    Iterator it_new, Sentinel iend_new)
    noexcept(noexcept(allocate(0)) && realloc_is_nothrow
    && is_maybe_nothrow_insertible_v<alloc, T,
      std::iter_reference_t<Iterator>&&>
    && std::is_nothrow_assignable_v<T&, std::iter_reference_t<Iterator>&&>
    && noexcept(it_new != iend_new) && noexcept(*it_new) && noexcept(++it_new)
    && (!std::forward_iterator<Iterator>
    || (std::is_nothrow_copy_constructible_v<Iterator>
    && std::is_nothrow_copy_constructible_v<Sentinel>
    && noexcept(util::next_until(it_new, iend_new, 0))
    && noexcept(util::distance(it_new, iend_new))))) {
  assert(it_old <= iend_old);
  size_type const pos{util::asserted_cast<size_type>(it_old - cbegin())};
  if constexpr(std::forward_iterator<Iterator>) {
    using range_diff_type = std::iter_difference_t<Iterator>;
    using range_ref = std::iter_reference_t<Iterator>;
    auto const [sep, split] = util::next_until(it_new, iend_new, size_ - pos);
    range_diff_type const new_range_size{sep == iend_new ? split
      : split + util::distance(sep, iend_new)};
    size_type const old_range_size{util::asserted_cast<size_type>(iend_old - it_old)};

    assert(new_range_size >= 0);
    size_type const new_range_size_u{static_cast<size_type>(new_range_size)};
    assert(max_size() >= new_range_size_u);
    assert(max_size() - new_range_size_u >= size_ - old_range_size);

    if(new_range_size_u > old_range_size) {
      // The vector increases in size.
      if(size_ + new_range_size_u - old_range_size > capacity_) {
        // We need to reallocate
        holder* const old_buf{get_buf()};
        size_type const new_capacity{size_
          + new_range_size_u - old_range_size};
        pointer new_buf{allocate(new_capacity)};

        if constexpr(is_maybe_nothrow_insertible_v<alloc, T, range_ref&&>
            && realloc_is_nothrow) {
          // Delete the old range
          for(size_type i{pos}; i < pos + old_range_size; ++i) {
            this->destroy_el(std::addressof(old_buf[i].t));
          }

          // Copy or move the preceding elements to the new buffer
          for(size_type i{0}; i < pos; ++i) {
            this->construct_el(std::addressof(new_buf[i].t),
              this->maybe_move(old_buf[i].t));
            this->destroy_el(std::addressof(old_buf[i].t));
          }

          // Emplace the elements
          for(size_type i{pos}; it_new != iend_new; ++i, ++it_new) {
            this->construct_el(std::addressof(new_buf[i].t), *it_new);
          }

          // Copy or move the following elements to the new buffer
          for(size_type i{pos + old_range_size}; i < size_; ++i) {
            this->construct_el(std::addressof(new_buf[i - old_range_size
              + new_range_size_u].t), this->maybe_move(old_buf[i].t));
            this->destroy_el(std::addressof(old_buf[i].t));
          }
        } else {
          // Copy or move the preceding elements to the new buffer
          for(size_type i{0}; i < pos; ++i) {
            try {
              this->construct_el(std::addressof(new_buf[i].t),
                this->maybe_move(old_buf[i].t));
            } catch(...) {
              while(i > 0) {
                --i;
                this->destroy_el(std::addressof(new_buf[i].t));
              }
              this->deallocate(std::move(new_buf), new_capacity);
              throw;
            }
          }

          // Emplace the new elements
          for(size_type i{pos}; it_new != iend_new; ++i, ++it_new) {
            try {
              this->construct_el(std::addressof(new_buf[i].t), *it_new);
            } catch(...) {
              while(i > 0) {
                --i;
                this->destroy_el(std::addressof(new_buf[i].t));
              }
              this->deallocate(std::move(new_buf), new_capacity);
              throw;
            }
          }

          // Copy or move the following elements to the new buffer
          for(size_type i{pos + old_range_size}; i < size_; ++i) {
            try {
              this->construct_el(std::addressof(new_buf[i - old_range_size
                + new_range_size_u].t), this->maybe_move(old_buf[i].t));
            } catch(...) {
              // i used to be an index into the old buffer; now it's an index
              // into the new buffer
              i = i - old_range_size + new_range_size_u;
              while(i > 0) {
                --i;
                this->destroy_el(std::addressof(new_buf[i].t));
              }
              this->deallocate(std::move(new_buf), new_capacity);
              throw;
            }
          }

          // Destroy elements from the old buffer
          {
            size_type i{size_};
            while(i > 0) {
              --i;
              this->destroy_el(std::addressof(old_buf[i].t));
            }
          }
        }

        // Install the new buffer
        if(capacity_ > small_capacity) {
          this->deallocate(std::move(old_buf), capacity_);
          buf_ptr_ = std::move(new_buf);
        } else {
          ::new(std::addressof(buf_ptr_)) pointer{std::move(new_buf)};
        }
        capacity_ = new_capacity;

        size_ = size_ - old_range_size + new_range_size_u;
      } else {
        // We do not need to reallocate.
        holder* const buf{get_buf()};
        size_type const old_size{size_};
        size_type const new_size{size_ - old_range_size + new_range_size_u};
        size_type const size_increase{new_size - old_size};

        // There are two cases: either the newly inserted range overlaps the
        // end of the old vector or it does not. In the first case, we execute
        // steps 1, 2, and 4. In the second case, we execute steps 2, 3, and
        // 4.

        // 1. Insert the trailing part of the new range that extends past the
        // end of the original vector (if such part exists).
        for(auto iit{sep}; iit != iend_new; ++iit) {
          this->construct_el(std::addressof(buf[size_].t), *iit);
          ++size_;
        }

        // 2. Move-construct the old trailing elements past the end.
        while(size_ < new_size) {
          this->construct_el(std::addressof(buf[size_].t),
            std::move(buf[size_ - size_increase].t));
          ++size_;
        }

        // 3. Move-assign old elements within the old vector (if necessary).
        for(size_type i{old_size - 1}; i >= pos + new_range_size_u; --i) {
          buf[i].t = std::move(buf[i - size_increase].t);
        }

        // 4. Insert the initial part of the new range into the part of the
        // buffer previously occupied by the original vector.
        for(size_type i{pos}; it_new != sep; ++i, ++it_new) {
          buf[i].t = *it_new;
        }
      }
    } else {
      // The vector either decreases in size or stays the same size.
      holder* const buf{get_buf()};
      size_type i{pos};
      for(; it_new != iend_new; ++i, ++it_new) {
        buf[i].t = *it_new;
      }
      this->erase(begin() + i, iend_old);
    }

    return begin() + pos;
  } else {
    small_vector<T, N, Allocator> new_range{std::move(it_new),
      std::move(iend_new), alloc_};
    auto move_view{new_range | std::views::transform([](T& t) noexcept {
      return std::move(t);
    })};
    return this->replace(it_old, iend_old, move_view.begin(), move_view.end());
  }
}

// Comparison ////////////////////////////////////////////////////////////////

template<typename T, std::size_t N, typename Allocator>
[[nodiscard]] bool operator==(small_vector<T, N, Allocator> const& a,
    small_vector<T, N, Allocator> const& b)
    noexcept(noexcept(std::declval<T const&>() == std::declval<T const&>())) {
  if(a.size() != b.size()) {
    return false;
  }
  auto it_a{a.cbegin()};
  auto it_b{b.cbegin()};
  while(it_a != a.cend()) {
    if(*it_a++ != *it_b++) {
      return false;
    }
  }
  return true;
}

template<typename T, std::size_t N, typename Allocator>
[[nodiscard]] std::strong_ordering operator<=>(
  small_vector<T, N, Allocator> const& a,
  small_vector<T, N, Allocator> const& b
) noexcept(noexcept(std::declval<T const&>() <=> std::declval<T const&>())) {
  auto it_a{a.cbegin()};
  auto it_b{b.cbegin()};
  while(it_a != a.cend() && it_b != b.cend()) {
    if(auto const cmp{*it_a <=> *it_b}; cmp != 0) {
      return cmp;
    }
    ++it_a;
    ++it_b;
  }
  if(it_a != a.cend()) {
    return std::strong_ordering::greater;
  }
  if(it_b != b.cend()) {
    return std::strong_ordering::less;
  }
  return std::strong_ordering::equivalent;
}

template<typename T, std::size_t N, typename Allocator>
void swap(small_vector<T, N, Allocator>& a, small_vector<T, N, Allocator>& b)
    noexcept(noexcept(a.swap(b))) requires std::is_swappable_v<T> {
  a.swap(b);
}

} // util

#endif
