#ifndef UTIL_COW_HPP_INCLUDED_RT58B7SR2V6AKG6BXGIU3HL2K
#define UTIL_COW_HPP_INCLUDED_RT58B7SR2V6AKG6BXGIU3HL2K

#include "util/non_null_ptr.hpp"
#include "util/scope_guard.hpp"
#include <atomic>
#include <cstdint>
#include <memory>
#include <type_traits>
#include <utility>

namespace util {

/** @brief Contains a shared object that is copied whenever it needs to be
 * modified.
 *
 * Thread safety guarantees: different COW instances that point to the same
 * object can be safely used from multiple threads without synchronization. The
 * same COW object cannot be accessed from multiple threads without
 * synchronization. (The underlying object may also have its own thread safety
 * guarantees.) */
template<typename T>
class cow {
  static_assert(std::is_copy_constructible_v<T>);

private:
  struct holder {
    std::atomic_uintptr_t refs{1};
    union { T t; };

    holder() noexcept {}
    holder(holder const&) = delete;
    holder& operator=(holder const&) = delete;
    ~holder() noexcept {}
  };

  non_null_ptr<holder> holder_;

public:
  /** @brief Constructs a new COW object, with the given arguments forwarded to
   * the underlying object's constructor.
   *
   * May also throw whatever is thrown by constructing the underlying object.
   *
   * @throws std::bad_alloc */
  template<typename... Args>
  requires std::is_constructible_v<T, Args&&...>
  explicit cow(Args&&... args): holder_{new holder} {
    util::scope_guard guard{[this]() noexcept { delete holder_; }};
    ::new(std::addressof(holder_->t)) T{std::forward<Args>(args)...};
    guard.reset();
  }

  cow(cow const& other) noexcept: holder_{other.holder_} {
    // This increment does not need stronger memory ordering requirements,
    // because the current thread already holds a reference to the object, and
    // therefore another thread won't destroy or modify the object under us.
    // Since atomic modification order is consistent with intra-thread program
    // order, by the time that reference is released and the reference counter
    // is decremented, this increment will already have occured; thus, any
    // thread that no longer "sees" the old reference will have already "seen"
    // this reference.
    holder_->refs.fetch_add(1, std::memory_order_relaxed);
  }

private:
  void release() noexcept(std::is_nothrow_destructible_v<T>) {
    // Every instance's use of the underlying object must end before the object
    // is destroyed or before another instance assumes exclusive control over
    // the object. Thus, decrementing the reference counter must synchronize
    // with the last decrement or with the read of 1 inside `modify()`; all
    // decrements (save for the last one) must have release semantics and the
    // last decrement must have acquire semantics. Since both decrements occur
    // here, this decrement must have both acquire and release semantics.
    if(holder_->refs.fetch_sub(1, std::memory_order_acq_rel) == 1) {
      util::scope_guard const guard{[this]() noexcept { delete holder_; }};
      holder_->t.~T();
    }
  }

public:
  ~cow() noexcept(std::is_nothrow_destructible_v<T>) {
    release();
  }

  cow& operator=(cow const& other)
      noexcept(std::is_nothrow_destructible_v<T>) {
    // This scope guard executes even if old object's destructor threw an
    // exception. The exception is still propagated.
    util::scope_guard const guard{[this, &other]() noexcept {
      holder_ = other.holder_;

      // `holder_` can only be nullptr if the destruction of the underlying
      // object throws an exception.
      if constexpr(!std::is_nothrow_destructible_v<T>) {
        if(holder_ == nullptr) {
          return *this;
        }
      }

      // See copy constructor for justification of the relaxed memory order.
      holder_->refs.fetch_add(1, std::memory_order_relaxed);
    }};

    release();
    return *this;
  }

  void swap(cow& other) noexcept {
    std::swap(holder_, other.holder_);
  }

  [[nodiscard]] T const& operator*() const noexcept {
    return holder_->t;
  }

  [[nodiscard]] T const* operator->() const noexcept {
    return std::addressof(holder_->t);
  }

  /** @brief Returns a mutable reference to the underlying object for
   * modification.
   *
   * After this returns, this COW instance is the exclusive owner of the
   * underlying object. The returned reference must be discarded before the COW
   * instance is manipulated.
   *
   * May also throw whatever is thrown by the destruction or copy-construction
   * of the underlying object.
   *
   * @throws std::bad_alloc */
  [[nodiscard]] T& modify() {
    // If we assume exclusive control over the object, then all previous users'
    // usage must end before this returns. This means that the decrements of
    // their reference count must synchronize with this read. Those decrements
    // therefore have release semantics, and this read has acquire semantics.
    if(use_count(std::memory_order_acquire) != 1) {
      non_null_ptr<holder> new_holder{new holder};

      try {
        ::new(std::addressof(new_holder->t)) T{holder_->t};
      } catch(...) {
        delete new_holder.get();
        throw;
      }

      try {
        release();
      } catch(...) {
        holder_ = new_holder;
        throw;
      }

      holder_ = new_holder;
    }
    return holder_->t;
  }

  [[nodiscard]] std::uintptr_t use_count(
    std::memory_order order = std::memory_order_relaxed
  ) const noexcept {
    return holder_->refs.load(order);
  }
};

template<typename T>
cow(T&&) -> cow<std::decay_t<T>>;

template<typename T>
void swap(cow<T>& a, cow<T>& b) noexcept {
  a.swap(b);
}

} // util

#endif
