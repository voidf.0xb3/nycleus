#ifndef UTIL_STACKFUL_CORO_HPP_INCLUDED_A1WSP2L2TS2IIROAZ0MN2PYD6
#define UTIL_STACKFUL_CORO_HPP_INCLUDED_A1WSP2L2TS2IIROAZ0MN2PYD6

#include "util/overflow.hpp"
#include "util/ptr_with_flags.hpp"
#include "util/stackful_coro_fwd.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <cassert>
#include <concepts>
#include <coroutine>
#include <cstddef>
#include <exception>
#include <functional>
#include <memory>
#include <type_traits>
#include <utility>
#include <variant>

namespace util::stackful_coro {

/** @brief A stackful coroutine processor owns the stack where coroutine frames
 * are kept and manages their execution.
 *
 * A stackful coroutine is a coroutine whose return type is a specialization of
 * `result` and whose first argument is of type `context`. To start execution of
 * a stackful coroutine, one needs to create an instance of a specialization of
 * this class template, with the same template argument as the one passed to the
 * `result` template (i. e. the coroutine's "real" return type), and then one
 * needs to call its `start` member function, passing the coroutine to call and
 * all the arguments to that coroutine, apart from the first one. (The processor
 * will directly invoke the coroutine, supplying in particular a `context`
 * argument.)
 *
 * The processor will execute the coroutines and will return control to its
 * caller when the first coroutine returns or throws an exception, or when some
 * coroutine suspends execution of the entire stack. These two situations can
 * be distinguished by calling `done`; if it returns false, the execution has
 * been suspended and can be resumed by calling `resume`.
 *
 * After the first coroutine returns, if its return type is something other than
 * void, the return value can be obtained by calling `get_result`. If it throws
 * an exception, that exception will be thrown out of `start` or `resume`.
 *
 * The coroutine can call other coroutines on the same stack. This is done by
 * directly invoking the other coroutine, passing as the first argument the
 * `context` object that the caller coroutine received itself. The returned
 * `result` object must be immediately passed to the `co_await` operator. Doing
 * anything else with that object causes undefined behavior. The `co_await`
 * operator produces the value returned by the called coroutine, or throws the
 * exception thrown by the called coroutine.
 *
 * To suspend execution of the stack, a coroutine calls the `suspend` member
 * function of the context argument and passes the result to the `co_await`
 * operator. Doing anything else with the return value of the `suspend` function
 * causes undefined behavior.
 *
 * Note that each coroutine call may require allocation of more stack space. If
 * this allocation fails, `std::bad_alloc` is thrown. This means that coroutines
 * should not be marked as `noexcept`.
 *
 * Note that passing exceptions between coroutines or throwing them out of the
 * coroutine processor uses std::exception_ptr internally. Therefore, any thrown
 * exception can turn into an std::bad_alloc or std::bad_exception, according to
 * the specification of std::current_exception.
 *
 * Due to technical limitations, coroutines should not have parameters, local
 * variables, or temporaries with alignment requirements greater than
 * __STDCPP_DEFAULT_NEW_ALIGNMENT__, and none of their destructors are allowed
 * to throw exceptions. Furthermore, coroutine parameters must not throw
 * exceptions when moved. Otherwise the behavior is undefined.
 *
 * It is possible to make a tail call. To do that, a `tail_call` member function
 * of the context argument is made, passing to it a tail call callback. The
 * return value of that call must be passed to `co_await`, and the result of
 * that must be passed to `co_return`.
 *
 * The callback must be move-constructible, as well as destructible without
 * throwing exceptions. The callback is invoked with a context object as the
 * only argument and is expected to return a coroutine result object with the
 * same result type as that of the caller. The callback is expected to invoke
 * the callee coroutine, passing the context argument, and immediately return
 * the returned result object. (The callback is not expected to be a coroutine
 * and is not expected to `co_await` the result object.)
 *
 * The callback is invoked after the caller's frame has already been destroyed,
 * so the callback MUST NOT capture the caller's local variables by reference
 * (unless those variables are themselves references). */
template<coro_result = void>
class processor;

namespace detail_ {

/** @brief The base class of promise types, containing all code that does not
 * depend on whether the coroutine returns void. */
template<coro_result>
class promise_base;

/** @brief The promise type, containing specific code for the case of returning
 * void and for the case of returning something else. */
template<coro_result>
class promise_type;

// Tail calls //////////////////////////////////////////////////////////////////

/** @brief The base class for a callback holder. */
class tail_call_holder_base {
public:
  virtual ~tail_call_holder_base() noexcept = default;
  virtual void invoke(context) noexcept = 0;
};

/** @brief The holder that is created inside the buffer to contain the callback,
 * as well as a reference to the associated result object. */
template<coro_result T, std::move_constructible Callback>
requires std::is_invocable_r_v<result<T>, Callback, context>
class tail_call_holder final: public tail_call_holder_base {
private:
  result<T>& result_;
  Callback callback_;

public:
  explicit tail_call_holder(result<T>& result_ref, Callback&& callback)
    noexcept(std::is_nothrow_move_constructible_v<Callback>):
    result_{result_ref}, callback_{std::move(callback)} {}

private:
  virtual ~tail_call_holder() noexcept override = default;

  /** @brief Invokes the callback.
   *
   * Assigns the returned value to the referred result object. If an exception
   * is thrown, it is also assigned to that object; therefore, this function
   * never throws exceptions. */
  virtual void invoke(context) noexcept override;
};

/** @brief A pointer to the buffer holding a tail call callback, as well as the
 * suspended flag. */
class tail_call_buffer {
private:
  ptr_with_flags<std::byte, 1> ptr_{nullptr};

  static_assert(__STDCPP_DEFAULT_NEW_ALIGNMENT__ >= 2);

  /** @brief The minimum size of the buffer. */
  static constexpr std::size_t initial_buf_size{128};

  /** @brief The factor by which we expand the buffer if a callback does not
   * fit. */
  static constexpr std::size_t growth_factor{2};

  template<coro_result>
  friend class result;

  /** @brief A structure that is placed at the beginning of the buffer. */
  struct buffer_header {
    /** @brief The size of the buffer, not including the header. */
    std::size_t size;
    /** @brief A pointer to the holder contained in the buffer, or a null
     * pointer if there is no held callback. */
    tail_call_holder_base* holder_ptr;
  };

  static_assert(alignof(buffer_header) <= __STDCPP_DEFAULT_NEW_ALIGNMENT__);

  /** @brief Returns a reference to the header structure in the currently
   * allocated buffer.
   *
   * The buffer must exist. */
  [[nodiscard]] buffer_header& get_header() const noexcept {
    assert(ptr_);
    return *std::launder(reinterpret_cast<buffer_header*>(
      static_cast<std::byte*>(ptr_)));
  }

  /** @brief Returns a pointer to the currently contained holder.
   *
   * If there is no callback stored, returns a null pointer. */
  [[nodiscard]] tail_call_holder_base* get_holder() const noexcept {
    assert(ptr_);
    return get_header().holder_ptr;
  }

  /** @brief Creates a buffer of the specified size, not counting the header
   * size.
   *
   * There must not be a buffer already existing.
   *
   * @throws std::bad_alloc */
  void create_buffer(std::size_t);

  /** @brief Destroys the current buffer.
   *
   * There must be an existing buffer. */
  void destroy_buffer() noexcept;

public:
  /** @brief Moves the given callback into the buffer along with the specified
   * target result object. */
  template<coro_result T, std::move_constructible Callback>
  requires std::is_invocable_r_v<result<T>, Callback, context>
  void emplace(result<T>& result_ref,
      std::remove_cvref_t<Callback>&& callback) {
    // This will never be called while suspended, so don't bother preserving the
    // suspended bit.
    assert(!suspended());

    auto const make_base{[&]() noexcept {
      void* ptr{ptr_ + sizeof(buffer_header)};
      void* const adjusted{std::align(alignof(tail_call_holder<T, Callback>),
        sizeof(tail_call_holder<T, Callback>), ptr, get_header().size)};
      return static_cast<std::byte*>(adjusted);
    }};

    std::byte* base{nullptr};
    std::size_t new_buf_size{initial_buf_size};
    if(ptr_) {
      base = make_base();
      if(!base) {
        new_buf_size = saturating_mul(get_header().size, growth_factor);
        destroy_buffer();
      }
    }

    if(!base) {
      std::size_t desired_size{sizeof(tail_call_holder<T, Callback>)};

      constexpr std::size_t extra_space{[]() noexcept {
        if constexpr(alignof(tail_call_holder<T, Callback>)
            <= __STDCPP_DEFAULT_NEW_ALIGNMENT__) {
          // If the callback's alignment is less than or equal to the allocation
          // alignment, the extra space that we need is exactly known and is
          // calculated as follows:
          return alignof(tail_call_holder<T, Callback>)
            - (sizeof(buffer_header) % alignof(tail_call_holder<T, Callback>));
        } else {
          // If the callback's alignment is greater than the allocation
          // alignment, the extra space that we need depends on the alignment of
          // the buffer with respect to the higher alignment requirement; said
          // alignment is a multiple of __STDCPP_DEFAULT_NEW_ALIGNMENT__. It
          // needs to be shifted by the size of the header; the maximum extra
          // space that we could possibly need is calculated as follows:
          if constexpr(sizeof(buffer_header)
              % __STDCPP_DEFAULT_NEW_ALIGNMENT__ == 0) {
            return alignof(tail_call_holder<T, Callback>)
              - __STDCPP_DEFAULT_NEW_ALIGNMENT__;
          } else {
            return alignof(tail_call_holder<T, Callback>)
              - sizeof(buffer_header) % __STDCPP_DEFAULT_NEW_ALIGNMENT__;
          }
        }
      }()};

      desired_size = saturating_add(desired_size, extra_space);

      create_buffer(std::max(desired_size, new_buf_size));
      base = make_base();
    }

    assert(base);
    get_header().holder_ptr = new(base) tail_call_holder<T, Callback>{
      result_ref, std::move(callback)};
  }

  /** @brief Checks if the buffer contains a callback. */
  [[nodiscard]] bool has_callback() const noexcept;

  /** @brief Calls the contained callback, then destroys it. */
  void invoke(context) noexcept;

  tail_call_buffer() noexcept = default;

  /** @brief Destroys the buffer.
   *
   * Important: the buffer must not contain a callback! If there is a callback,
   * the behavior is undefined! This is fine, since this class is an
   * implementation detail of the stackful coroutine mechanism, and we make sure
   * to use this class only in such a way that this restriction holds. */
  ~tail_call_buffer() noexcept;

  tail_call_buffer(tail_call_buffer const&) = delete;
  tail_call_buffer(tail_call_buffer&&) = delete;
  tail_call_buffer& operator=(tail_call_buffer const&) = delete;
  tail_call_buffer& operator=(tail_call_buffer&&) = delete;

  [[nodiscard]] bool suspended() const noexcept {
    return ptr_.get_flag<0>();
  }

  void set_suspended(bool new_value) noexcept {
    ptr_.set_flag<0>(new_value);
  }
};

// Processor base //////////////////////////////////////////////////////////////

/** @brief The base of `processor` that consists of all the stuff that does not
 * need to be a template.
 *
 * The chunks are organized in a linked list, with the header containing
 * pointers to the next and the previous ones. The coroutine frames closer to
 * the bottom of the stack are placed in the chunks closer to the beginning of
 * the linked list, and within the same chunk, closer to the beginning of the
 * chunk (in the lower addresses).
 *
 * Each frame is allocated with alignment of at least
 * `__STDCPP_DEFAULT_NEW_ALIGNMENT__`. Along with each frame we put an integer,
 * of type `std::size_t`, immediately before the point where the next frame
 * would start; the integer contains the offset from the beginning of the chunk
 * to the beginning of the corresponding frame. This lets us find the beginning
 * of that frame and restore the old offset when we pop a frame. */
class processor_base {
private:
  /** @brief This structure is placed at the beginning of each stack chunk. */
  struct chunk_header {
    /** @brief The size of this chunk. */
    std::size_t const size;

    /** @brief A pointer to the previous chunk, or a null pointer for the first
     * chunk. */
    std::byte* prev;

    /** @brief A pointer to the next chunk, or a null pointer for the first
     * chunk. */
    std::byte* next;

    /** @brief An offset from the beginning of the chunk to the beginning of the
     * unallocated part of the chunk. */
    std::size_t top_offset;
  };

  /** @brief The size of the chunk that is first allocated when the processor is
   * created. */
  static constexpr std::size_t initial_chunk_size{4096};

  /** @brief The factor by which we multiply the size of the previous chunk to
   * get the size of the next chunk to allocate. */
  static constexpr std::size_t growth_factor{2};

  /** @brief The offset into the chunk to the beginning of memory that can be
   * used for allocating frames.
   *
   * This is the size of the chunk header rounded up to the next alignment
   * point. */
  static constexpr std::size_t initial_offset{
    (sizeof(chunk_header) + __STDCPP_DEFAULT_NEW_ALIGNMENT__ - 1)
    & ~(__STDCPP_DEFAULT_NEW_ALIGNMENT__ - 1)};

  static_assert(alignof(chunk_header) <= __STDCPP_DEFAULT_NEW_ALIGNMENT__);
  static_assert(alignof(std::size_t) <= __STDCPP_DEFAULT_NEW_ALIGNMENT__);
  static_assert(initial_offset >= sizeof(chunk_header));

  /** @brief A pointer to the chunk containing the top frame, or the first chunk
   * if there are no frames. */
  std::byte* cur_chunk_;

  /** @brief The handle to the coroutine at the top of the stack, or a null
   * handle if there are no coroutines. */
  std::coroutine_handle<> top_frame_{};

  /** @brief A pointer to the tail call buffer; also stores the suspended flag.
   */
  tail_call_buffer tail_call_;

  /** @brief This flag is used to keep track of whether new frame allocation was
   * elided.
   *
   * When a frame is allocated, this is set to true. Once a promise object is
   * constructed within that frame, it reads and remembers this value and sets
   * this to false. When a coroutine ends its execution, that remembered value
   * is used to check if a frame needs to be freed. */
  bool frame_allocated_{false};

  friend class util::stackful_coro::context;
  template<coro_result>
  friend class util::stackful_coro::detail_::promise_base;
  template<coro_result>
  friend class util::stackful_coro::detail_::promise_type;

  /** @brief Allocates a new chunk of the given size.
   *
   * The chunk is not immediately attached to any chunk list; that is to say,
   * its previous and next chunk pointers are null.
   *
   * @throws std::bad_alloc */
  [[nodiscard]] static std::byte* new_chunk(std::size_t);

  /** @brief Given a pointer to a chunk, returns a pointer to that chunk's
   * header. */
  [[nodiscard]] static chunk_header* get_chunk_header(std::byte*) noexcept;

  /** @brief Allocates a new frame of the given size and returns a pointer to
   * it.
   *
   * The frame may be allocated inside an existing chunk, or a new chunk may
   * have to be created.
   *
   * @throws std::bad_alloc */
  [[nodiscard]] std::byte* push_frame(std::size_t);

  /** @brief Deallocates the memory allocated for the top frame.
   *
   * This just deallocates memory; the contained frame has to have already been
   * destroyed. */
  void pop_frame() noexcept;

protected:
  /** @throws std::bad_alloc */
  processor_base();

public:
  processor_base(processor_base const&) = delete;
  processor_base(processor_base&&) = delete;
  processor_base& operator=(processor_base const&) = delete;
  processor_base& operator=(processor_base&&) = delete;

protected:
  ~processor_base() noexcept;

  /** @brief Runs the main loop, continuously resuming the top coroutine on the
   * stack until control needs to be returned to the original caller/resumer. */
  void loop() noexcept;
};

} // detail_

// Context /////////////////////////////////////////////////////////////////////

class context {
private:
  detail_::processor_base* processor_;

  template<coro_result>
  friend class util::stackful_coro::detail_::promise_base;

  template<coro_result>
  friend class util::stackful_coro::detail_::promise_type;

  friend class util::stackful_coro::detail_::processor_base;

  template<coro_result>
  friend class util::stackful_coro::processor;

  struct suspend_type {
    suspend_type() noexcept = default;
    suspend_type(suspend_type const&) = delete;
    suspend_type(suspend_type&&) = delete;
    suspend_type& operator=(suspend_type const&) = delete;
    suspend_type& operator=(suspend_type&&) = delete;
    ~suspend_type() noexcept = default;
  };

  explicit context(detail_::processor_base& proc) noexcept: processor_{&proc} {}

public:
  [[nodiscard]] suspend_type suspend() noexcept {
    processor_->tail_call_.set_suspended(true);
    return {};
  }

private:
  template<coro_result T, std::move_constructible Callback>
  requires std::is_invocable_r_v<result<T>, Callback&&, context>
  struct [[nodiscard("the return value of context::tail_call must be passed "
      "to co_await")]] tail_call_token {
    Callback& callback;
  };

public:
  template<std::move_constructible Callback>
  requires std::invocable<Callback&&, context>
    && specialization_of<std::invoke_result_t<Callback&&, context>, result>
  [[nodiscard]] auto tail_call(Callback&& callback) noexcept {
    return tail_call_token<
      typename std::invoke_result_t<Callback&&, context>::result_type,
      std::remove_cvref_t<Callback>
    >{callback};
  }
};

// Result //////////////////////////////////////////////////////////////////////

namespace detail_ {

/** @brief A simple wrapper for the returned value, which may be a reference or
 * void. */
template<coro_result T>
struct value_holder {
  T t_;

  explicit value_holder(T&& t)
    noexcept(std::is_nothrow_move_constructible_v<T>): t_{std::forward<T>(t)} {}

  value_holder(value_holder const&) = delete;
  value_holder(value_holder&&) = delete;
  value_holder& operator=(value_holder const&) = delete;
  value_holder& operator=(value_holder&&) = delete;
  ~value_holder() noexcept = default;
};

template<>
struct value_holder<void> {};

} // detail_

template<coro_result T>
class [[nodiscard("stackful coroutine result should be passed to co_await "
    "or returned from a tail call callback")]] result {
private:
  /** @brief A backpointer to the promise that will store the result in this
   * object.
   *
   * This is used to adjust the promise's pointer to the result object when the
   * result object is moved. */
  detail_::promise_base<T>* promise_;

  /** @brief The value returned or the exception thrown by the callee. */
  std::variant<std::monostate, detail_::value_holder<T>,
    std::exception_ptr> data_;

  template<coro_result U, std::move_constructible Callback>
  requires std::is_invocable_r_v<result<U>, Callback, context>
  friend class util::stackful_coro::detail_::tail_call_holder;

  template<coro_result>
  friend class util::stackful_coro::processor;

  template<coro_result>
  friend class util::stackful_coro::detail_::promise_base;

  template<coro_result>
  friend class util::stackful_coro::detail_::promise_type;

  result() noexcept: promise_{nullptr}, data_{} {}

  explicit result(detail_::promise_base<T>& promise) noexcept:
    promise_{&promise} {}

public:
  using result_type = T;

  result(result const&) = delete;
  result& operator=(result const&) = delete;

  ~result() noexcept = default;

  /** @brief Move-constructs the object. The object must be newly constructed
   * and not contain returned data.
   *
   * The result object must be movable for the coroutine machinery to work,
   * because it stores the result of `get_return_object` in a local variable and
   * later moves it to return it. But apart from that and the one move
   * assignment in processor::start, we seek to avoid moving result objects (the
   * only valid action on a result object returned from a coroutine is to
   * co_await it). So we assume in the following code that a result object that
   * is being moved does not contain a returned value or a thrown exception. */
  result(result&& other) noexcept: promise_{other.promise_} {
    assert(std::holds_alternative<std::monostate>(other.data_));
    assert(other.promise_);
    assert(other.promise_->result_ == &other);

    other.promise_ = nullptr;
    promise_->result_ = this;
  }

  /** @brief Move-assigned the object. The source object must be newly
   * constructed and not contain returned data. The destination object may
   * contain data, which will be destroyed.
   *
   * See the move constructor documentation for rationale. */
  result& operator=(result&& other) noexcept {
    assert(std::holds_alternative<std::monostate>(other.data_));
    assert(other.promise_);
    assert(other.promise_->result_ == &other);

    data_.template emplace<std::monostate>();
    promise_ = other.promise_;
    other.promise_ = nullptr;
    promise_->result_ = this;

    return *this;
  }

  [[nodiscard]] bool await_ready() const noexcept {
    return false;
  }

  template<coro_result U>
  void await_suspend(std::coroutine_handle<detail_::promise_type<U>>)
    const noexcept {}

  T await_resume() {
    return std::visit(overloaded{
      [](std::monostate) noexcept -> T { unreachable(); },

      [](detail_::value_holder<T>& data)
          noexcept(std::is_nothrow_move_constructible_v<T>) -> T {
        if constexpr(std::same_as<T, void>) {
          return;
        } else {
          return std::forward<T>(data.t_);
        }
      },

      [](std::exception_ptr e) -> T {
        assert(e);
        std::rethrow_exception(e);
      }
    }, data_);
  }
};

namespace detail_ {

template<coro_result T, std::move_constructible Callback>
requires std::is_invocable_r_v<result<T>, Callback, context>
void tail_call_holder<T, Callback>::invoke(context ctx) noexcept {
  try {
    result_ = std::invoke(callback_, ctx);
  } catch(...) {
    result_.data_.template emplace<std::exception_ptr>(
      std::current_exception());
  }
}

// Promise /////////////////////////////////////////////////////////////////////

template<coro_result T>
class promise_base {
private:
  /** @brief A pointer to the result object that will contain the returned value
   * or the thrown exception. */
  result<T>* result_;

  /** @brief A reference to the coroutine processor. */
  processor_base& proc_;

  /** @brief This coroutine's caller. */
  std::coroutine_handle<> caller_;

  /** @brief Whether the frame was allocated on the processor's stack, and
   * therefore needs to be deallocated.
   *
   * If this is false, the allocation was elided. */
  bool frame_allocated_;

  friend class result<T>;

protected:
  [[nodiscard]] processor_base& get_processor() const noexcept { return proc_; }
  [[nodiscard]] result<T>*& get_result_ptr() noexcept { return result_; }

public:
  /** @throws std::bad_alloc */
  template<typename... Args>
  [[nodiscard]] static void* operator new(
    std::size_t size,
    context const& ctx,
    Args&&...
  ) {
    return ctx.processor_->push_frame(size);
  }

  /** @throws std::bad_alloc */
  template<typename C, typename... Args>
  requires (!std::same_as<std::remove_cvref_t<C>, context>)
  [[nodiscard]] static void* operator new(
    std::size_t size,
    C&&,
    context const& ctx,
    Args&&...
  ) {
    return ctx.processor_->push_frame(size);
  }

  // NOLINTNEXTLINE(misc-new-delete-overloads)
  static void operator delete(void*) noexcept {}

  explicit promise_base(context const& ctx) noexcept:
      proc_{*ctx.processor_}, caller_{proc_.top_frame_},
      frame_allocated_{proc_.frame_allocated_} {
    proc_.frame_allocated_ = false;
  }

  ~promise_base() noexcept {
    if(result_ && caller_) {
      caller_.destroy();
    }
  }

  promise_base(promise_base const&) = delete;
  promise_base(promise_base&&) = delete;
  promise_base& operator=(promise_base const&) = delete;
  promise_base& operator=(promise_base&&) = delete;

  [[nodiscard]] result<T> get_return_object() noexcept {
    result<T> ret{*this};
    result_ = &ret;
    return ret;
  }

  [[nodiscard]] std::suspend_always initial_suspend() const noexcept {
    return {};
  }

  template<coro_result U>
  [[nodiscard]] result<U>&& await_transform(result<U>&& callee) const noexcept {
    return std::move(callee);
  }

  [[nodiscard]] std::suspend_always await_transform(context::suspend_type)
      const noexcept {
    return {};
  }

protected:
  void pop_frame() noexcept {
    if(frame_allocated_) {
      proc_.pop_frame();
    }
    proc_.top_frame_ = caller_;
  }

public:
  void unhandled_exception() noexcept {
    result_->data_.template emplace<std::exception_ptr>(
      std::current_exception());
    pop_frame();
    result_ = nullptr;
  }

  [[nodiscard]] std::suspend_never final_suspend() const noexcept { return {}; }
};

template<coro_result T>
class promise_type: public promise_base<T> {
private:
  using base = promise_base<T>;

  [[nodiscard]] std::coroutine_handle<promise_type> get_handle() noexcept {
    return std::coroutine_handle<promise_type>::from_promise(*this);
  }

public:
  explicit promise_type(context const& ctx) noexcept: base{ctx} {
    base::get_processor().top_frame_ = get_handle();
  }

  template<typename Arg, typename... Args>
  explicit promise_type(context const& ctx, Arg&&, Args&&...) noexcept:
    promise_type{ctx} {}

  template<typename C, typename... Args>
  requires (!std::same_as<std::remove_cvref_t<C>, context>)
  explicit promise_type(C&&, context const& ctx, Args&&...) noexcept:
    promise_type{ctx} {}

  void return_value(T value)
      noexcept(std::is_nothrow_move_constructible_v<T>) {
    base::get_result_ptr()->data_.template emplace<detail_::value_holder<T>>(
      std::forward<T>(value));
    base::pop_frame();
    base::get_result_ptr() = nullptr;
  }

private:
  template<std::move_constructible Callback>
  requires std::is_invocable_r_v<result<T>, Callback&&, context>
  struct [[nodiscard("the result of awaiting the return value of "
      "context::tail_call must be passed to co_return")]] tail_call_returnable {
    Callback& callback;
  };

public:
  template<std::move_constructible Callback>
  requires std::is_invocable_r_v<result<T>, Callback&&, context>
  void return_value(tail_call_returnable<Callback> returnable) {
    base::get_processor().tail_call_.template emplace<T, Callback>(
      *base::get_result_ptr(), std::move(returnable.callback));
    base::pop_frame();
    base::get_result_ptr() = nullptr;
  }

private:
  template<std::move_constructible Callback>
  requires std::is_invocable_r_v<result<T>, Callback&&, context>
  struct tail_call_awaitable {
    Callback& callback;

    [[nodiscard]] bool await_ready() const noexcept { return true; }
    void await_suspend(std::coroutine_handle<>) const noexcept {
      unreachable();
    }
    [[nodiscard]] auto await_resume() const noexcept {
      return tail_call_returnable<Callback>{callback};
    }
  };

public:
  template<std::move_constructible Callback>
  requires std::is_invocable_r_v<result<T>, Callback&&, context>
  [[nodiscard]] auto await_transform(
    context::tail_call_token<T, Callback> token
  ) noexcept {
    return tail_call_awaitable<Callback>{token.callback};
  }

  using base::await_transform;
};

template<>
class promise_type<void>: public promise_base<void> {
private:
  [[nodiscard]] std::coroutine_handle<promise_type> get_handle() noexcept {
    return std::coroutine_handle<promise_type>::from_promise(*this);
  }

public:
  explicit promise_type(context const& ctx) noexcept: promise_base<void>{ctx} {
    get_processor().top_frame_
      = std::coroutine_handle<promise_type>::from_promise(*this);
  }

  template<typename Arg, typename... Args>
  explicit promise_type(context const& ctx, Arg&&, Args&&...) noexcept:
    promise_type{ctx} {}

  template<typename C, typename... Args>
  requires (!std::same_as<std::remove_cvref_t<C>, context>)
  explicit promise_type(C&&, context const& ctx, Args&&...) noexcept:
    promise_type{ctx} {}

  void return_void() noexcept {
    if(!get_result_ptr()) {
      assert(get_processor().tail_call_.has_callback());
      return;
    }

    get_result_ptr()->data_.emplace<detail_::value_holder<void>>();
    pop_frame();
    get_result_ptr() = nullptr;
  }

private:
  template<std::move_constructible Callback>
  requires std::is_invocable_r_v<result<void>, Callback&&, context>
  struct tail_call_awaitable {
    Callback& callback;
    promise_type& promise;

    [[nodiscard]] bool await_ready() const noexcept { return true; }
    void await_suspend(std::coroutine_handle<>) const noexcept {
      unreachable();
    }
    void await_resume() const {
      promise.get_processor().tail_call_.template emplace<void, Callback>(
        *promise.get_result_ptr(), std::move(callback));
      promise.pop_frame();
      promise.get_result_ptr() = nullptr;
    }
  };

public:
  template<std::move_constructible Callback>
  requires std::is_invocable_r_v<result<void>, Callback&&, context>
  [[nodiscard]] auto await_transform(
    context::tail_call_token<void, Callback> token
  ) noexcept {
    return tail_call_awaitable<Callback>{token.callback, *this};
  }

  using promise_base<void>::await_transform;
};

} // detail_

} // util::stackful_coro

template<
  util::stackful_coro::coro_result T,
  util::stackful_coro::coro_param... Args
>
struct std::coroutine_traits<
  util::stackful_coro::result<T>,
  util::stackful_coro::context,
  Args...
> {
  using promise_type = ::util::stackful_coro::detail_::promise_type<T>;
};

template<
  util::stackful_coro::coro_result T,
  typename C,
  util::stackful_coro::coro_param... Args
>
requires (!std::same_as<std::remove_cvref_t<C>, util::stackful_coro::context>)
  && std::is_class_v<std::remove_cvref_t<C>>
struct std::coroutine_traits<
  util::stackful_coro::result<T>,
  C,
  util::stackful_coro::context,
  Args...
> {
  using promise_type = ::util::stackful_coro::detail_::promise_type<T>;
};

// Processor (public API) //////////////////////////////////////////////////////

namespace util::stackful_coro {

template<coro_result T>
class processor: private detail_::processor_base {
private:
  result<T> result_;

public:
  /** @throws std::bad_alloc */
  processor() = default;

  processor(processor const&) = delete;
  processor(processor&&) = delete;
  processor& operator=(processor const&) = delete;
  processor& operator=(processor&&) = delete;

  ~processor() noexcept = default;

  /** @brief After `start` or `resume` returns, this can be used to check if the
   * execution was completed (in which case true is returned) or suspended (in
   * which case false is returned). */
  [[nodiscard]] bool done() const noexcept {
    return !std::holds_alternative<std::monostate>(result_.data_);
  }

  /** @brief After execution is completed, this can be used to obtain the return
   * value of the first coroutine. */
  [[nodiscard]] T get_result() noexcept(std::is_nothrow_move_constructible_v<T>)
      requires (!std::same_as<T, void>) {
    assert(std::holds_alternative<detail_::value_holder<T>>(result_.data_));
    return std::forward<T>(
      std::get<detail_::value_holder<T>>(result_.data_).t_);
  }

private:
  /** @brief Checks if the called coroutine threw an exception, and if so,
   * rethrows it. */
  void unwrap_exception() {
    if(std::exception_ptr* const result_exception{
        std::get_if<std::exception_ptr>(&result_.data_)}) {
      assert(*result_exception);
      std::exception_ptr const e{std::move(*result_exception)};
      *result_exception = nullptr;
      std::rethrow_exception(e);
    }
  }

public:
  /** @brief Resumes suspended execution. Must only be called while execution is
   * suspended. */
  void resume() {
    assert(!done());
    loop();
    unwrap_exception();
  }

  /** @brief Calls the given coroutine.
   *
   * The first argument is the coroutine to call. Subsequent arguments are
   * passed to the coroutine after the initial context argument. */
  template<typename Coro, typename... Args>
  requires std::is_invocable_r_v<result<T>, Coro&&, context, Args&&...>
  void start(Coro&& callee, Args&&... args) {
    result_ = std::invoke(std::forward<Coro>(callee), context{*this},
      std::forward<Args>(args)...);
    loop();
    unwrap_exception();
  }
};

} // util::stackful_coro

#endif
