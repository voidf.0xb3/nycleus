#include <boost/test/unit_test.hpp>

#include "util/inplace_vector.hpp"
#include "single_pass_view.hpp"
#include <algorithm>
#include <compare>
#include <cstdint>
#include <new>
#include <ranges>
#include <stdexcept>
#include <string>
#include <utility>

///////////////////////////////////////////////////////////////////////////////////////////////
// Non-trivial element types //////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

namespace {

struct inplace_vector_fixture {
  util::inplace_vector<std::string, 8> vec{"0", "1", "2", "3"};
};

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_FIXTURE_TEST_SUITE(inplace_vector, inplace_vector_fixture)

// Iteration //////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(iteration)

BOOST_AUTO_TEST_CASE(forward) {
  auto it{vec.begin()};
  auto iend{vec.end()};
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "0");
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "1");
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "2");
  --it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "1");
  it += 2;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "3");
  ++it;
  BOOST_TEST((it == iend));
}

BOOST_AUTO_TEST_CASE(reverse) {
  std::string arr[]{"3", "2", "1", "0"};
  BOOST_TEST(std::equal(vec.rbegin(), vec.rend(),
    std::begin(arr), std::end(arr)));
}

BOOST_AUTO_TEST_CASE(interop) {
  BOOST_TEST((vec.begin() + vec.size() == vec.cend()));
  BOOST_TEST((vec.cbegin() + vec.size() == vec.end()));
}

BOOST_AUTO_TEST_CASE(indexing_read) {
  auto it{vec.begin()};
  BOOST_TEST(it[1] == "1");
}

BOOST_AUTO_TEST_CASE(indexing_write) {
  auto it{vec.begin()};
  it[1] = "179";
  std::string values[]{"0", "179", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // iteration

// Construction ///////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(construct)

BOOST_AUTO_TEST_CASE(empty) {
  util::inplace_vector<std::string, 8> vec;
  BOOST_TEST(vec.size() == 0);
  BOOST_TEST(vec.empty());
}

BOOST_AUTO_TEST_CASE(copy) {
  std::string values[]{"0", "1", "2", "3"};
  util::inplace_vector<std::string, 8> other{vec};
  BOOST_TEST(vec == other, boost::test_tools::per_element());
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(move) {
  std::string values[]{"0", "1", "2", "3"};
  util::inplace_vector<std::string, 8> other{std::move(vec)};
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range) {
  std::string values[]{"0", "1", "2", "3"};
  util::inplace_vector<std::string, 8> new_vec{std::begin(values), std::end(values)};
  BOOST_TEST(new_vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_overlong) {
  std::string values[]{"0", "1", "2", "3"};
  BOOST_CHECK_THROW((util::inplace_vector<std::string, 3>{
    std::begin(values), std::end(values)}), std::bad_alloc);
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  std::string values[]{"0", "1", "2", "3"};
  auto view{values | tests::single_pass};
  util::inplace_vector<std::string, 8> new_vec{view.begin(), view.end()};
  BOOST_TEST(new_vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_default) {
  util::inplace_vector<std::string, 16> new_vec(5);
  std::string arr[]{"", "", "", "", ""};
  BOOST_TEST(new_vec == arr, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill) {
  util::inplace_vector<std::string, 16> new_vec(5, "179");
  std::string arr[]{"179", "179", "179", "179", "179"};
  BOOST_TEST(new_vec == arr, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // construct

// Swap ///////////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(swap) {
  std::string values[]{"0", "1", "2", "3"};
  std::string values_other[]{"100", "101", "102"};
  util::inplace_vector<std::string, 8> other{"100", "101", "102"};
  vec.swap(other);
  BOOST_TEST(other == values, boost::test_tools::per_element());
  BOOST_TEST(vec == values_other, boost::test_tools::per_element());
}

// Assignment /////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(assign)

BOOST_AUTO_TEST_CASE(op_copy) {
  std::string values[]{"0", "1", "2", "3"};
  util::inplace_vector<std::string, 8> other;
  other = vec;
  BOOST_TEST(vec == other, boost::test_tools::per_element());
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_move) {
  std::string values[]{"0", "1", "2", "3"};
  util::inplace_vector<std::string, 8> other;
  other = std::move(vec);
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(self_assign) {
  std::string values[]{"0", "1", "2", "3"};
  vec = vec;
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_il) {
  vec = {"100", "101", "102"};
  std::string arr[]{"100", "101", "102"};
  BOOST_TEST(vec == arr, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_shrink) {
  std::string values[]{"100", "101", "102"};
  vec.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_equal) {
  std::string values[]{"100", "101", "102", "103"};
  vec.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_extend) {
  std::string values[]{"100", "101", "102", "103", "104"};
  vec.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_overlong) {
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109"};
  BOOST_CHECK_THROW(vec.assign(std::begin(values), std::end(values)), std::bad_alloc);
}

BOOST_AUTO_TEST_CASE(il_extend) {
  vec.assign({"100", "101", "102", "103", "104"});
  std::string values[]{"100", "101", "102", "103", "104"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_extend) {
  vec.assign(5, "100");
  std::string values[]{"100", "100", "100", "100", "100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // assign

// Indexing ///////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(indexing)

BOOST_AUTO_TEST_CASE(at_unchecked) {
  BOOST_TEST(vec[2] == "2");
}

BOOST_AUTO_TEST_CASE(at) {
  BOOST_TEST(vec.at(2) == "2");
}

BOOST_AUTO_TEST_CASE(at_out_of_range) {
  BOOST_CHECK_THROW(static_cast<void>(vec.at(80)), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(front) {
  BOOST_TEST(vec.front() == "0");
}

BOOST_AUTO_TEST_CASE(front_assign) {
  vec.front() = "179";
  std::string values[]{"179", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back) {
  BOOST_TEST(vec.back() == "3");
}

BOOST_AUTO_TEST_CASE(back_assign) {
  vec.back() = "179";
  std::string values[]{"0", "1", "2", "179"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // indexing

// Comparison /////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(compare)

BOOST_AUTO_TEST_CASE(equal) {
  util::inplace_vector<std::string, 8> vec_equal{"0", "1", "2", "3"};
  util::inplace_vector<std::string, 8> vec_different_size{"100", "101", "102", "103"};
  util::inplace_vector<std::string, 8> vec_different_content{"101", "102", "103"};
  BOOST_TEST(vec == vec_equal);
  BOOST_TEST(vec != vec_different_size);
  BOOST_TEST(vec != vec_different_content);
}

BOOST_AUTO_TEST_CASE(three_way) {
  util::inplace_vector<std::string, 8> vec_prefix{"0", "1", "2"};
  util::inplace_vector<std::string, 8> vec_less{"0", "1", "2", "", "100"};
  util::inplace_vector<std::string, 8> vec_equal{"0", "1", "2", "3"};
  util::inplace_vector<std::string, 8> vec_greater{"0", "1", "2", "4", ""};
  util::inplace_vector<std::string, 8> vec_extension{"0", "1", "2", "3", "4"};
  BOOST_TEST(((vec_prefix <=> vec) == std::strong_ordering::less));
  BOOST_TEST(((vec_less <=> vec) == std::strong_ordering::less));
  BOOST_TEST(((vec_equal <=> vec) == std::strong_ordering::equal));
  BOOST_TEST(((vec_greater <=> vec) == std::strong_ordering::greater));
  BOOST_TEST(((vec_extension <=> vec) == std::strong_ordering::greater));
}

BOOST_AUTO_TEST_SUITE_END() // compare

// Emplacement ////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(emplace)

BOOST_AUTO_TEST_CASE(into) {
  auto it{vec.emplace(vec.cbegin() + 1, "100")};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(into_endlineup) {
  auto it{vec.emplace(vec.cend() - 1, "100")};
  BOOST_TEST((it == vec.end() - 2));
  std::string values[]{"0", "1", "2", "100", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(into_atend) {
  auto it{vec.emplace(vec.cend(), "100")};
  BOOST_TEST((it == vec.end() - 1));
  std::string values[]{"0", "1", "2", "3", "100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back) {
  std::string& el{vec.emplace_back("100")};
  BOOST_TEST((&el == &vec.back()));
  std::string values[]{"0", "1", "2", "3", "100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back_empty) {
  util::inplace_vector<std::string, 8> vec;
  std::string& el{vec.emplace_back("100")};
  BOOST_TEST((&el == &vec.back()));
  std::string values[]{"100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back_try) {
  std::string* const el{vec.try_emplace_back("100")};
  BOOST_TEST((el == &vec.back()));
  std::string values[]{"0", "1", "2", "3", "100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back_try_fail) {
  util::inplace_vector<std::string, 8> vec{"0", "1", "2", "3", "4", "5", "6", "7"};
  std::string* const el{vec.try_emplace_back("100")};
  BOOST_TEST((el == nullptr));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // emplace

// Insertion //////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(insert)

BOOST_AUTO_TEST_CASE(one) {
  auto it{vec.insert(vec.cbegin() + 1, "100")};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_endlineup) {
  auto it{vec.insert(vec.cend() - 1, "100")};
  BOOST_TEST((it == vec.end() - 2));
  std::string values[]{"0", "1", "2", "100", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_atend) {
  auto it{vec.insert(vec.cend(), "100")};
  BOOST_TEST((it == vec.end() - 1));
  std::string values[]{"0", "1", "2", "3", "100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_empty) {
  std::string arr[]{""};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::begin(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endahead) {
  std::string arr[]{"100", "101"};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "101", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endlineup) {
  std::string arr[]{"100", "101", "102"};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endoverlap) {
  std::string arr[]{"100", "101", "102", "103"};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_overlong) {
  std::string arr[]{"100", "101", "102", "103", "104", "105", "106"};
  BOOST_CHECK_THROW(vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr)),
    std::bad_alloc);
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  std::string arr[]{"100", "101", "102", "103"};
  auto view{arr | tests::single_pass};
  auto it{vec.insert(vec.begin() + 1, view.begin(), view.end())};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_endoverlap) {
  auto it{vec.insert(vec.begin() + 1, {"100", "101", "102", "103"})};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_endoverlap) {
  auto it{vec.insert(vec.begin() + 1, 4, "100")};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "100", "100", "100", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_empty) {
  vec.append_range(std::ranges::subrange<std::string const*>{nullptr, nullptr});
  std::string values[]{"0", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append) {
  std::string arr[]{"100", "101"};
  vec.append_range(arr);
  std::string values[]{"0", "1", "2", "3", "100", "101"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_single_pass) {
  std::string arr[]{"100", "101", "102", "103"};
  vec.append_range(arr | tests::single_pass);
  std::string values[]{"0", "1", "2", "3", "100", "101", "102", "103"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_try) {
  std::string arr[]{"100", "101"};
  auto const it{vec.try_append_range(arr)};
  BOOST_TEST((it == std::end(arr)));
  std::string values[]{"0", "1", "2", "3", "100", "101"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_try_fail) {
  std::string arr[]{"100", "101", "102", "103", "104", "105"};
  auto const it{vec.try_append_range(arr)};
  BOOST_TEST((it == std::end(arr) - 2));
  std::string values[]{"0", "1", "2", "3", "100", "101", "102", "103"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back) {
  auto& el{vec.push_back("255")};
  BOOST_TEST((&el == &vec.back()));
  std::string values[]{"0", "1", "2", "3", "255"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back_empty) {
  util::inplace_vector<std::string, 8> vec;
  auto& el{vec.push_back("255")};
  BOOST_TEST((&el == &vec.front()));
  std::string values[]{"255"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back_try) {
  auto* const el{vec.try_push_back("255")};
  BOOST_TEST((el == &vec.back()));
  std::string values[]{"0", "1", "2", "3", "255"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back_try_fail) {
  util::inplace_vector<std::string, 8> vec{"0", "1", "2", "3", "4", "5", "6", "7"};
  auto* const el{vec.try_push_back("255")};
  BOOST_TEST((el == nullptr));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // insert

// Erasure ////////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(erase)

BOOST_AUTO_TEST_CASE(one) {
  auto it{vec.erase(vec.cbegin() + 1)};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_atend) {
  auto it{vec.erase(vec.cend() - 1)};
  BOOST_TEST((it == vec.end()));
  std::string values[]{"0", "1", "2"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endahead) {
  auto it{vec.erase(vec.cbegin() + 1, vec.cbegin() + 3)};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endlineup) {
  auto it{vec.erase(vec.cbegin() + 1, vec.cend())};
  BOOST_TEST((it == vec.end()));
  std::string values[]{"0"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(clear) {
  vec.clear();
  BOOST_TEST(vec.size() == 0);
  BOOST_TEST(vec.empty());
}

BOOST_AUTO_TEST_CASE(clear_empty) {
  util::inplace_vector<std::string, 8> vec;
  vec.clear();
  BOOST_TEST(vec.size() == 0);
  BOOST_TEST(vec.empty());
}

BOOST_AUTO_TEST_CASE(pop_back) {
  vec.pop_back();
  std::string values[]{"0", "1", "2"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pop_back_one) {
  util::inplace_vector<std::string, 8> vec_one{"0"};
  vec_one.pop_back();
  BOOST_TEST(vec_one.size() == 0);
  BOOST_TEST(vec_one.empty());
}

BOOST_AUTO_TEST_SUITE_END() // erase

// Resizing ///////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(resize)

BOOST_AUTO_TEST_CASE(shrink) {
  vec.resize(2);
  std::string values[]{"0", "1"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(expand_default) {
  vec.resize(7);
  std::string values[]{"0", "1", "2", "3", "", "", ""};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(expand_copy) {
  vec.resize(7, "100");
  std::string values[]{"0", "1", "2", "3", "100", "100", "100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // resize

BOOST_AUTO_TEST_SUITE_END() // inplace_vector
BOOST_AUTO_TEST_SUITE_END() // test_util

///////////////////////////////////////////////////////////////////////////////////////////////
// Trivial element types //////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

#define CT_TEST(...) do { if(!(__VA_ARGS__)) { return false; } } while(false)

namespace {

namespace trivial_types {

static_assert(std::is_trivially_copyable_v<util::inplace_vector<std::uint8_t, 8>>);

struct fixture {
  util::inplace_vector<std::uint8_t, 8> vec{0, 1, 2, 3};
};

// Iteration //////////////////////////////////////////////////////////////////////////////////

namespace iteration {

constexpr bool forward(fixture fix) {
  auto it{fix.vec.begin()};
  auto iend{fix.vec.end()};
  CT_TEST(it != iend);
  CT_TEST(*it == 0);
  ++it;
  CT_TEST(it != iend);
  CT_TEST(*it == 1);
  ++it;
  CT_TEST(it != iend);
  CT_TEST(*it == 2);
  --it;
  CT_TEST(it != iend);
  CT_TEST(*it == 1);
  it += 2;
  CT_TEST(it != iend);
  CT_TEST(*it == 3);
  ++it;
  CT_TEST(it == iend);
  return true;
}
static_assert(forward({}));

constexpr bool reverse(fixture fix) {
  std::uint8_t arr[]{3, 2, 1, 0};
  CT_TEST(std::equal(fix.vec.rbegin(), fix.vec.rend(), std::begin(arr), std::end(arr)));
  return true;
}
static_assert(reverse({}));

constexpr bool interop(fixture fix) {
  CT_TEST(fix.vec.begin() + fix.vec.size() == fix.vec.cend());
  CT_TEST(fix.vec.cbegin() + fix.vec.size() == fix.vec.end());
  return true;
}
static_assert(interop({}));

constexpr bool indexing_read(fixture fix) {
  auto it{fix.vec.begin()};
  CT_TEST(it[1] == 1);
  return true;
}
static_assert(indexing_read({}));

constexpr bool indexing_write(fixture fix) {
  auto it{fix.vec.begin()};
  it[1] = 179;
  std::uint8_t values[]{0, 179, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(indexing_write({}));

} // iteration

// Construction ///////////////////////////////////////////////////////////////////////////////

namespace construct {

constexpr bool empty() {
  util::inplace_vector<std::uint8_t, 8> vec;
  CT_TEST(vec.size() == 0);
  CT_TEST(vec.empty());
  return true;
}
static_assert(empty());

constexpr bool copy(fixture fix) {
  std::uint8_t values[4]{0, 1, 2, 3};
  util::inplace_vector<std::uint8_t, 8> other{fix.vec};
  CT_TEST(std::ranges::equal(fix.vec, other));
  CT_TEST(std::ranges::equal(other, values));
  return true;
}
static_assert(copy({}));

constexpr bool move(fixture fix) {
  std::uint8_t values[4]{0, 1, 2, 3};
  util::inplace_vector<std::uint8_t, 8> other{std::move(fix.vec)};
  CT_TEST(std::ranges::equal(other, values));
  return true;
}
static_assert(move({}));

constexpr bool range() {
  std::uint8_t values[4]{0, 1, 2, 3};
  util::inplace_vector<std::uint8_t, 8> new_vec{std::begin(values), std::end(values)};
  CT_TEST(std::ranges::equal(new_vec, values));
  return true;
}
static_assert(range());

constexpr bool range_single_pass() {
  std::uint8_t values[4]{0, 1, 2, 3};
  auto view{values | tests::single_pass};
  util::inplace_vector<std::uint8_t, 8> new_vec{view.begin(), view.end()};
  CT_TEST(std::ranges::equal(new_vec, values));
  return true;
}
static_assert(range_single_pass());

constexpr bool fill_default() {
  util::inplace_vector<std::uint8_t, 16> new_vec(5);
  CT_TEST(new_vec.size() == 5);
  return true;
}
static_assert(fill_default());

constexpr bool fill() {
  util::inplace_vector<std::uint8_t, 16> new_vec(5, 179);
  std::uint8_t arr[]{179, 179, 179, 179, 179};
  CT_TEST(std::ranges::equal(new_vec, arr));
  return true;
}
static_assert(fill());

} // construct

// Swapping ///////////////////////////////////////////////////////////////////////////////////

namespace swap {

constexpr bool swap(fixture fix) {
  std::uint8_t values[4]{0, 1, 2, 3};
  std::uint8_t values_other[]{100, 101, 102};
  util::inplace_vector<std::uint8_t, 8> other{100, 101, 102};
  fix.vec.swap(other);
  CT_TEST(std::ranges::equal(other, values));
  CT_TEST(std::ranges::equal(fix.vec, values_other));
  return true;
}
static_assert(swap({}));

} // swap

// Assignment /////////////////////////////////////////////////////////////////////////////////

namespace assign {

constexpr bool op_copy(fixture fix) {
  std::uint8_t values[]{0, 1, 2, 3};
  util::inplace_vector<std::uint8_t, 8> other;
  other = fix.vec;
  CT_TEST(std::ranges::equal(fix.vec, other));
  CT_TEST(std::ranges::equal(other, values));
  return true;
}
static_assert(op_copy({}));

constexpr bool op_move(fixture fix) {
  std::uint8_t values[]{0, 1, 2, 3};
  util::inplace_vector<std::uint8_t, 8> other;
  other = std::move(fix.vec);
  CT_TEST(std::ranges::equal(other, values));
  return true;
}
static_assert(op_move({}));

constexpr bool self_assign(fixture fix) {
  std::uint8_t values[]{0, 1, 2, 3};
  fix.vec = fix.vec;
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(self_assign({}));

constexpr bool op_il(fixture fix) {
  fix.vec = {100, 101, 102};
  std::uint8_t values[]{100, 101, 102};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(op_il({}));

constexpr bool range_shrink(fixture fix) {
  std::uint8_t values[]{100, 101, 102};
  fix.vec.assign(std::begin(values), std::end(values));
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_shrink({}));

constexpr bool range_equal(fixture fix) {
  std::uint8_t values[]{100, 101, 102, 103};
  fix.vec.assign(std::begin(values), std::end(values));
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_equal({}));

constexpr bool range_extend(fixture fix) {
  std::uint8_t values[]{100, 101, 102, 103, 104};
  fix.vec.assign(std::begin(values), std::end(values));
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_extend({}));

constexpr bool il_extend(fixture fix) {
  std::uint8_t values[]{100, 101, 102, 103, 104};
  fix.vec.assign({100, 101, 102, 103, 104});
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(il_extend({}));

constexpr bool fill_extend(fixture fix) {
  fix.vec.assign(5, 100);
  std::uint8_t values[]{100, 100, 100, 100, 100};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(fill_extend({}));

} // assign

// Indexing ///////////////////////////////////////////////////////////////////////////////////

namespace indexing {

constexpr bool at_unchecked(fixture fix) {
  CT_TEST(fix.vec[2] == 2);
  return true;
}
static_assert(at_unchecked({}));

constexpr bool at(fixture fix) {
  CT_TEST(fix.vec.at(2) == 2);
  return true;
}
static_assert(at({}));

constexpr bool front(fixture fix) {
  CT_TEST(fix.vec.front() == 0);
  return true;
}
static_assert(front({}));

constexpr bool front_assign(fixture fix) {
  fix.vec.front() = 179;
  std::uint8_t values[]{179, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(front_assign({}));

constexpr bool back(fixture fix) {
  CT_TEST(fix.vec.back() == 3);
  return true;
}
static_assert(back({}));

constexpr bool back_assign(fixture fix) {
  fix.vec.back() = 179;
  std::uint8_t values[]{0, 1, 2, 179};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(back_assign({}));

} // indexing

// Comparison /////////////////////////////////////////////////////////////////////////////////

namespace compare {

constexpr bool equal(fixture fix) {
  util::inplace_vector<std::uint8_t, 8> vec_equal{0, 1, 2, 3};
  util::inplace_vector<std::uint8_t, 8> vec_different_size{100, 101, 102, 103};
  util::inplace_vector<std::uint8_t, 8> vec_different_content{101, 102, 103};
  CT_TEST(fix.vec == vec_equal);
  CT_TEST(fix.vec != vec_different_size);
  CT_TEST(fix.vec != vec_different_content);
  return true;
}
static_assert(equal({}));

constexpr bool three_way(fixture fix) {
  util::inplace_vector<std::uint8_t, 8> vec_prefix{0, 1, 2};
  util::inplace_vector<std::uint8_t, 8> vec_less{0, 1, 2, 0, 100};
  util::inplace_vector<std::uint8_t, 8> vec_equal{0, 1, 2, 3};
  util::inplace_vector<std::uint8_t, 8> vec_greater{0, 1, 2, 4, 0};
  util::inplace_vector<std::uint8_t, 8> vec_extension{0, 1, 2, 3, 4};
  CT_TEST((vec_prefix <=> fix.vec) == std::strong_ordering::less);
  CT_TEST((vec_less <=> fix.vec) == std::strong_ordering::less);
  CT_TEST((vec_equal <=> fix.vec) == std::strong_ordering::equal);
  CT_TEST((vec_greater <=> fix.vec) == std::strong_ordering::greater);
  CT_TEST((vec_extension <=> fix.vec) == std::strong_ordering::greater);
  return true;
}
static_assert(three_way({}));

} // compare

// Emplacement ////////////////////////////////////////////////////////////////////////////////

namespace emplace {

constexpr bool into(fixture fix) {
  auto it{fix.vec.emplace(fix.vec.cbegin() + 1, 100)};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 100, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(into({}));

constexpr bool into_endlineup(fixture fix) {
  auto it{fix.vec.emplace(fix.vec.cend() - 1, 100)};
  CT_TEST(it == fix.vec.end() - 2);
  std::uint8_t values[]{0, 1, 2, 100, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(into_endlineup({}));

constexpr bool into_atend(fixture fix) {
  auto it{fix.vec.emplace(fix.vec.cend(), 100)};
  CT_TEST(it == fix.vec.end() - 1);
  std::uint8_t values[]{0, 1, 2, 3, 100};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(into_atend({}));

constexpr bool back(fixture fix) {
  std::uint8_t& el{fix.vec.emplace_back(100)};
  CT_TEST(&el == &fix.vec.back());
  std::uint8_t values[]{0, 1, 2, 3, 100};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(back({}));

constexpr bool back_empty() {
  util::inplace_vector<std::uint8_t, 8> vec;
  std::uint8_t& el{vec.emplace_back(100)};
  CT_TEST(&el == &vec.back());
  std::uint8_t values[]{100};
  CT_TEST(std::ranges::equal(vec, values));
  return true;
}
static_assert(back_empty());

constexpr bool back_try(fixture fix) {
  std::uint8_t* const el{fix.vec.try_emplace_back(100)};
  CT_TEST(el == &fix.vec.back());
  std::uint8_t values[]{0, 1, 2, 3, 100};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(back_try({}));

constexpr bool back_try_fail() {
  util::inplace_vector<std::uint8_t, 8> vec{0, 1, 2, 3, 4, 5, 6, 7};
  std::uint8_t* const el{vec.try_emplace_back(100)};
  CT_TEST(el == nullptr);
  std::uint8_t values[]{0, 1, 2, 3, 4, 5, 6, 7};
  CT_TEST(std::ranges::equal(vec, values));
  return true;
}
static_assert(back_try_fail());

} // emplace

// Insertion //////////////////////////////////////////////////////////////////////////////////

namespace insert {

constexpr bool one(fixture fix) {
  auto it{fix.vec.insert(fix.vec.cbegin() + 1, 100)};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 100, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(one({}));

constexpr bool one_endlineup(fixture fix) {
  auto it{fix.vec.insert(fix.vec.cend() - 1, 100)};
  CT_TEST(it == fix.vec.end() - 2);
  std::uint8_t values[]{0, 1, 2, 100, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(one_endlineup({}));

constexpr bool one_atend(fixture fix) {
  auto it{fix.vec.insert(fix.vec.cend(), 100)};
  CT_TEST(it == fix.vec.end() - 1);
  std::uint8_t values[]{0, 1, 2, 3, 100};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(one_atend({}));

constexpr bool range_empty(fixture fix) {
  std::uint8_t arr[]{0};
  auto it{fix.vec.insert(fix.vec.begin() + 1, std::begin(arr), std::begin(arr))};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_empty({}));

constexpr bool range_endahead(fixture fix) {
  std::uint8_t arr[]{100, 101};
  auto it{fix.vec.insert(fix.vec.begin() + 1, std::begin(arr), std::end(arr))};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 100, 101, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_endahead({}));

constexpr bool range_endlineup(fixture fix) {
  std::uint8_t arr[]{100, 101, 102};
  auto it{fix.vec.insert(fix.vec.begin() + 1, std::begin(arr), std::end(arr))};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 100, 101, 102, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_endlineup({}));

constexpr bool range_endoverlap(fixture fix) {
  std::uint8_t arr[]{100, 101, 102, 103};
  auto it{fix.vec.insert(fix.vec.begin() + 1, std::begin(arr), std::end(arr))};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 100, 101, 102, 103, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_endoverlap({}));

constexpr bool range_single_pass(fixture fix) {
  std::uint8_t arr[]{100, 101, 102, 103};
  auto view{arr | tests::single_pass};
  auto it{fix.vec.insert(fix.vec.begin() + 1, view.begin(), view.end())};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 100, 101, 102, 103, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_single_pass({}));

constexpr bool il_endoverlap(fixture fix) {
  auto it{fix.vec.insert(fix.vec.begin() + 1, {100, 101, 102, 103})};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 100, 101, 102, 103, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(il_endoverlap({}));

constexpr bool fill_endoverlap(fixture fix) {
  auto it{fix.vec.insert(fix.vec.begin() + 1, 4, 100)};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 100, 100, 100, 100, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(fill_endoverlap({}));

constexpr bool append_empty(fixture fix) {
  fix.vec.append_range(std::ranges::subrange<std::uint8_t const*>{nullptr, nullptr});
  std::uint8_t values[]{0, 1, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(append_empty({}));

constexpr bool append(fixture fix) {
  std::uint8_t arr[]{100, 101};
  fix.vec.append_range(arr);
  std::uint8_t values[]{0, 1, 2, 3, 100, 101};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(append({}));

constexpr bool append_single_pass(fixture fix) {
  std::uint8_t arr[]{100, 101, 102, 103};
  fix.vec.append_range(arr | tests::single_pass);
  std::uint8_t values[]{0, 1, 2, 3, 100, 101, 102, 103};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(append_single_pass({}));

constexpr bool append_try(fixture fix) {
  std::uint8_t arr[]{100, 101};
  auto const it{fix.vec.try_append_range(arr)};
  CT_TEST(it == std::end(arr));
  std::uint8_t values[]{0, 1, 2, 3, 100, 101};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(append_try({}));

constexpr bool append_try_fail(fixture fix) {
  std::uint8_t arr[]{100, 101, 102, 103, 104, 105};
  auto const it{fix.vec.try_append_range(arr)};
  CT_TEST(it == std::end(arr) - 2);
  std::uint8_t values[]{0, 1, 2, 3, 100, 101, 102, 103};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(append_try_fail({}));

constexpr bool push_back(fixture fix) {
  auto& el{fix.vec.push_back(255)};
  CT_TEST(&el == &fix.vec.back());
  std::uint8_t values[]{0, 1, 2, 3, 255};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(push_back({}));

constexpr bool push_back_empty() {
  util::inplace_vector<std::uint8_t, 8> vec;
  auto& el{vec.push_back(255)};
  CT_TEST(&el == &vec.front());
  std::uint8_t values[]{255};
  CT_TEST(std::ranges::equal(vec, values));
  return true;
}
static_assert(push_back_empty());

constexpr bool push_back_try(fixture fix) {
  auto* const el{fix.vec.try_push_back(255)};
  CT_TEST(el == &fix.vec.back());
  std::uint8_t values[]{0, 1, 2, 3, 255};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(push_back_try({}));

constexpr bool push_back_try_fail() {
  util::inplace_vector<std::uint8_t, 8> vec{0, 1, 2, 3, 4, 5, 6, 7};
  auto* const el{vec.try_push_back(255)};
  CT_TEST(el == nullptr);
  std::uint8_t values[]{0, 1, 2, 3, 4, 5, 6, 7};
  CT_TEST(std::ranges::equal(vec, values));
  return true;
}
static_assert(push_back_try_fail());

} // insert

// Erasure ////////////////////////////////////////////////////////////////////////////////////

namespace erase {

constexpr bool one(fixture fix) {
  auto it{fix.vec.erase(fix.vec.cbegin() + 1)};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 2, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(one({}));

constexpr bool one_atend(fixture fix) {
  auto it{fix.vec.erase(fix.vec.cend() - 1)};
  CT_TEST(it == fix.vec.end());
  std::uint8_t values[]{0, 1, 2};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(one_atend({}));

constexpr bool range_endahead(fixture fix) {
  auto it{fix.vec.erase(fix.vec.cbegin() + 1, fix.vec.cbegin() + 3)};
  CT_TEST(it == fix.vec.begin() + 1);
  std::uint8_t values[]{0, 3};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_endahead({}));

constexpr bool range_endlineup(fixture fix) {
  auto it{fix.vec.erase(fix.vec.cbegin() + 1, fix.vec.cend())};
  CT_TEST(it == fix.vec.end());
  std::uint8_t values[]{0};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(range_endlineup({}));

constexpr bool clear(fixture fix) {
  fix.vec.clear();
  CT_TEST(fix.vec.size() == 0);
  CT_TEST(fix.vec.empty());
  return true;
}
static_assert(clear({}));

constexpr bool pop_back(fixture fix) {
  fix.vec.pop_back();
  std::uint8_t values[]{0, 1, 2};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(pop_back({}));

constexpr bool pop_back_one() {
  util::inplace_vector<std::uint8_t, 8> vec_one{0};
  vec_one.pop_back();
  CT_TEST(vec_one.size() == 0);
  CT_TEST(vec_one.empty());
  return true;
}
static_assert(pop_back_one());

} // erase

// Resizing ///////////////////////////////////////////////////////////////////////////////////

namespace resize {

constexpr bool shrink(fixture fix) {
  fix.vec.resize(2);
  std::uint8_t values[]{0, 1};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(shrink({}));

constexpr bool expand_default(fixture fix) {
  fix.vec.resize(7);
  CT_TEST(fix.vec.size() == 7);
  return true;
}
static_assert(expand_default({}));

constexpr bool expand_copy(fixture fix) {
  fix.vec.resize(7, 100);
  std::uint8_t values[]{0, 1, 2, 3, 100, 100, 100};
  CT_TEST(std::ranges::equal(fix.vec, values));
  return true;
}
static_assert(expand_copy({}));

} // resize

} // trivial_types

///////////////////////////////////////////////////////////////////////////////////////////////
// Zero capacity //////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

namespace zero_capacity {

static_assert(std::is_trivially_copyable_v<util::inplace_vector<std::string, 0>>);

struct fixture {
  util::inplace_vector<std::string, 0> vec;
};

constexpr bool iteration(fixture fix) {
  auto const it{fix.vec.begin()};
  auto const iend{fix.vec.end()};
  CT_TEST(it == iend);
  return true;
}
static_assert(iteration({}));

constexpr bool is_empty(fixture fix) {
  CT_TEST(fix.vec.empty());
  CT_TEST(fix.vec.size() == 0);
  return true;
}
static_assert(is_empty({}));

constexpr bool construct_copy(fixture fix) {
  util::inplace_vector<std::string, 0> other(fix.vec);
  CT_TEST(other.empty());
  CT_TEST(other.size() == 0);
  return true;
}
static_assert(construct_copy({}));

constexpr bool construct_move(fixture fix) {
  util::inplace_vector<std::string, 0> other(std::move(fix.vec));
  CT_TEST(other.empty());
  CT_TEST(other.size() == 0);
  return true;
}
static_assert(construct_move({}));

constexpr bool swap(fixture fix) {
  util::inplace_vector<std::string, 0> other;
  fix.vec.swap(other);
  CT_TEST(fix.vec.empty());
  CT_TEST(fix.vec.size() == 0);
  CT_TEST(other.empty());
  CT_TEST(other.size() == 0);
  return true;
}
static_assert(swap({}));

constexpr bool assign_copy(fixture fix) {
  util::inplace_vector<std::string, 0> other;
  other = fix.vec;
  CT_TEST(other.empty());
  CT_TEST(other.size() == 0);
  return true;
}
static_assert(assign_copy({}));

constexpr bool assign_move(fixture fix) {
  util::inplace_vector<std::string, 0> other;
  other = std::move(fix.vec);
  CT_TEST(other.empty());
  CT_TEST(other.size() == 0);
  return true;
}
static_assert(assign_move({}));

constexpr bool assign_self(fixture fix) {
  fix.vec = fix.vec;
  CT_TEST(fix.vec.empty());
  CT_TEST(fix.vec.size() == 0);
  return true;
}
static_assert(assign_self({}));

constexpr bool compare_equal(fixture fix) {
  util::inplace_vector<std::string, 0> other;
  CT_TEST(fix.vec == other);
  return true;
}
static_assert(compare_equal({}));

constexpr bool compare_three_way(fixture fix) {
  util::inplace_vector<std::string, 0> other;
  CT_TEST((fix.vec <=> other) == std::strong_ordering::equal);
  return true;
}
static_assert(compare_three_way({}));

constexpr bool emplace_back_try(fixture fix) {
  std::string* const el{fix.vec.try_emplace_back("")};
  CT_TEST(el == nullptr);
  CT_TEST(fix.vec.empty());
  CT_TEST(fix.vec.size() == 0);
  return true;
}
static_assert(emplace_back_try({}));

constexpr bool append_try(fixture fix) {
  std::string arr[]{"100", "101"};
  auto const it{fix.vec.try_append_range(arr)};
  CT_TEST(it == std::begin(arr));
  CT_TEST(fix.vec.empty());
  CT_TEST(fix.vec.size() == 0);
  return true;
}
static_assert(append_try({}));

constexpr bool push_back_try(fixture fix) {
  std::string* const el{fix.vec.try_push_back("")};
  CT_TEST(el == nullptr);
  CT_TEST(fix.vec.empty());
  CT_TEST(fix.vec.size() == 0);
  return true;
}
static_assert(push_back_try({}));

constexpr bool clear(fixture fix) {
  fix.vec.clear();
  CT_TEST(fix.vec.empty());
  CT_TEST(fix.vec.size() == 0);
  return true;
}
static_assert(clear({}));

} // zero_capacity

} // (anonymous)
