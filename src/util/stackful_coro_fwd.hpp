#ifndef UTIL_STACKFUL_CORO_HPP_INCLUDED_HPP_G5H0RFPJ9V5RVVRWGKBOX3ZKE
#define UTIL_STACKFUL_CORO_HPP_INCLUDED_HPP_G5H0RFPJ9V5RVVRWGKBOX3ZKE

#include "util/type_traits.hpp"
#include <concepts>
#include <type_traits>

namespace util::stackful_coro {

/** @brief The concept that a coroutine actual return type (the template
 * argument to `result` below) must satisfy.
 *
 * The coroutine return type can be:
 * - void;
 * - a move-constructible value; or
 * - a reference. */
template<typename T>
concept coro_result = std::move_constructible<T> || std::same_as<T, void>;

template<typename T>
concept coro_param = std::is_nothrow_move_constructible_v<T>
  && std::is_nothrow_destructible_v<T>
  && (!object<T> || alignof(T) <= __STDCPP_DEFAULT_NEW_ALIGNMENT__);

/** @brief A value that is passed to all stackful coroutines as the first
 * argument.
 *
 * When calling other coroutines on the same stack, this argument must be passed
 * to the callees. It also has a `suspend` member function that can be used to
 * suspend the stack. */
class context;

/** @brief The return value of a stackful coroutine.
 *
 * A stackful coroutine should only be directly called from another stackful
 * coroutine that uses the same stack, and the only valid use for the returned
 * result object is to immediately co_await it. */
template<coro_result = void>
class result;

} // util::stackful_coro

#endif
