#ifndef UTIL_CONCAT_VIEW_HPP_INCLUDED_IWHXYR8RAIGEPD53SJD145WAL
#define UTIL_CONCAT_VIEW_HPP_INCLUDED_IWHXYR8RAIGEPD53SJD145WAL

#include "util/iterator_facade.hpp"
#include "util/type_traits.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <iterator>
#include <ranges>
#include <tuple>
#include <type_traits>
#include <utility>
#include <variant>

namespace util {

namespace detail_::concat_view {

template<typename... Views>
using reference_t = std::common_reference_t<std::ranges::range_reference_t<Views>...>;

template<typename... Views>
using value_t = std::common_type_t<std::ranges::range_value_t<Views>...>;

template<typename... Views>
using rvalue_reference_t = std::common_reference_t<
  std::ranges::range_rvalue_reference_t<Views>...>;

template<typename... Views>
using size_t = std::common_type_t<std::ranges::range_size_t<Views>...>;

template<typename... Views>
using diff_t = std::common_type_t<std::ranges::range_difference_t<Views>...>;

template<typename... Views>
concept concatable
  = requires {
    typename reference_t<Views...>;
    typename value_t<Views...>;
    typename rvalue_reference_t<Views...>;
  }
  && std::common_reference_with<reference_t<Views...>&&, value_t<Views...>&>
  && std::common_reference_with<reference_t<Views...>&&, rvalue_reference_t<Views...>&&>
  && std::common_reference_with<rvalue_reference_t<Views...>&&, value_t<Views...> const&>;

template<typename... Views>
struct is_common: std::false_type {};
template<typename View>
struct is_common<View>: std::bool_constant<std::ranges::common_range<View>> {};
template<typename View, typename... Views>
struct is_common<View, Views...>: is_common<Views...> {};
template<typename... Views>
constexpr bool is_common_v{is_common<Views...>::value};

template<std::size_t I, typename T>
struct indexed {
  T value;
};

template<typename Seq, typename... T>
struct make_indexed_variant_impl;

template<std::size_t... Is, typename... Ts>
struct make_indexed_variant_impl<std::index_sequence<Is...>, Ts...> {
  using type = std::variant<indexed<Is, Ts>...>;
};

template<typename... Ts>
using make_indexed_variant = make_indexed_variant_impl<
  std::make_index_sequence<sizeof...(Ts)>, Ts...>::type;

} // detail_::concat_view

/** @brief An implementation of std::ranges::concat_view from C++26.
 *
 * This view produces the elements obtained from concatenating several input views.
 *
 * The view is a forward range iff all original views are forward ranges.
 *
 * The view is a bidirectional range iff all original views are bidirectional common ranges.
 *
 * The view is a random-access range iff all original views are random-access common ranges.
 *
 * The view is a common range iff the last original view is a common range.
 *
 * The view is a sized range iff all original views are sized ranges. */
template<std::ranges::input_range... Views>
requires (std::ranges::view<Views> && ...) && (sizeof...(Views) > 0)
  && detail_::concat_view::concatable<Views...>
class concat_view: public std::ranges::view_interface<concat_view<Views...>> {
private:
  std::tuple<Views...> views_;

  template<bool Const, std::size_t From, std::size_t To>
  requires (From <= To) && (To <= sizeof...(Views))
  [[nodiscard]] constexpr detail_::concat_view::diff_t<maybe_const<Const, Views>...>
  count_size_as_diff() const
  {
    if constexpr(From < To) {
      return static_cast<detail_::concat_view::diff_t<maybe_const<Const, Views>...>>(
        std::ranges::size(std::get<From>(views_)))
        + this->count_size_as_diff<Const, From + 1, To>();
    } else {
      return 0;
    }
  }

  struct make_begin {};
  struct make_end {};

  template<bool Const>
  class iterator_core {
  private:
    maybe_const<Const, concat_view>* view_;
    detail_::concat_view::make_indexed_variant<
      std::ranges::iterator_t<maybe_const<Const, Views>>...> it_;

    template<std::size_t I>
    using iter_t = std::ranges::iterator_t<maybe_const<Const,
      std::tuple_element_t<I, std::tuple<Views...>>>>;
    template<std::size_t I>
    using indexed_iter_t = detail_::concat_view::indexed<I, iter_t<I>>;

    template<std::size_t I>
    constexpr void check_range_end() {
      assert(it_.index() == I);
      if constexpr(I != sizeof...(Views) - 1) {
        if(std::get<I>(it_).value == std::ranges::end(std::get<I>(view_->views_))) {
          it_ = indexed_iter_t<I + 1>{std::ranges::begin(std::get<I + 1>(view_->views_))};
          check_range_end<I + 1>();
        }
      }
    }

    friend class concat_view;

    constexpr explicit iterator_core(make_begin, maybe_const<Const, concat_view>& view):
      view_{&view},
      it_{std::in_place_index<0>, std::ranges::begin(std::get<0>(view_->views_))}
    {
      check_range_end<0>();
    }

    constexpr explicit iterator_core(make_end, maybe_const<Const, concat_view>& view):
      view_{&view},
      it_{
        std::in_place_index<sizeof...(Views) - 1>,
        std::ranges::end(std::get<sizeof...(Views) - 1>(view_->views_))} {}

  public:
    constexpr iterator_core() = default;

    constexpr iterator_core(iterator_core<!Const> other)
    noexcept((... && (
      std::is_nothrow_convertible_v<std::ranges::iterator_t<Views>,
        std::ranges::iterator_t<Views const>>
      && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<Views const>>
    )))
    requires (Const && ... && std::convertible_to<std::ranges::iterator_t<Views>,
      std::ranges::iterator_t<Views const>>):
    view_{other.view_},
    it_{std::visit([]<std::size_t I>(indexed_iter_t<I> value) {
      return decltype(it_){std::in_place_index<I>, std::move(value.value)};
    }, std::move(other.it_))} {}

  protected:
    using value_type = detail_::concat_view::value_t<maybe_const<Const, Views>...>;
    using difference_type = detail_::concat_view::diff_t<maybe_const<Const, Views>...>;

    static constexpr bool is_single_pass{(...
      || !std::ranges::forward_range<maybe_const<Const, Views>>)};

    [[nodiscard]] constexpr decltype(auto) dereference() const
    noexcept((... && noexcept(*std::declval<
      std::ranges::iterator_t<maybe_const<Const, Views>> const&>())))
    {
      assert(!it_.valueless_by_exception());
      return std::visit([]<std::size_t I>(indexed_iter_t<I> const& value)
          -> detail_::concat_view::reference_t<maybe_const<Const, Views>...> {
        return *value.value;
      }, it_);
    }

    constexpr void increment()
    noexcept((... && (
      noexcept(++std::declval<std::ranges::iterator_t<maybe_const<Const, Views>>&>())
      && noexcept(std::ranges::begin(std::declval<Views&>()))
      && noexcept(std::ranges::end(std::declval<Views&>()))
      && is_nothrow_equality_comparable_with_v<std::ranges::iterator_t<Views&>,
        std::ranges::sentinel_t<Views&>>
      && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<Views&>>
    )))
    {
      assert(!it_.valueless_by_exception());
      std::visit([&]<std::size_t I>(indexed_iter_t<I>& value) {
        ++value.value;
        check_range_end<I>();
      }, it_);
    }

    [[nodiscard]] constexpr bool equal_to(iterator_core const& other) const
    noexcept((... && is_nothrow_equality_comparable_v<
      std::ranges::iterator_t<maybe_const<Const, Views>>>))
    {
      assert(!it_.valueless_by_exception());
      assert(!other.it_.valueless_by_exception());
      return std::visit([]<std::size_t I, std::size_t J>(
        indexed_iter_t<I> const& a,
        indexed_iter_t<J> const& b
      ) {
        if constexpr(I == J) {
          return a.value == b.value;
        } else {
          return false;
        }
      }, it_, other.it_);
    }

    [[nodiscard]] constexpr bool equal_to(std::default_sentinel_t) const
    noexcept(
      noexcept(std::ranges::end(std::declval<maybe_const<Const,
        std::tuple_element_t<sizeof...(Views) - 1, std::tuple<Views...>>>&>()))
      && is_nothrow_equality_comparable_with_v<
        std::ranges::iterator_t<maybe_const<Const,
          std::tuple_element_t<sizeof...(Views) - 1, std::tuple<Views...>>>&>,
        std::ranges::sentinel_t<maybe_const<Const,
          std::tuple_element_t<sizeof...(Views) - 1, std::tuple<Views...>>>&>
      >
    )
    {
      constexpr std::size_t last{sizeof...(Views) - 1};
      if(auto const* const it{std::get_if<last>(&it_)}) {
        return *it == std::ranges::end(std::get<last>(view_->views_));
      } else {
        return false;
      }
    }

  private:
    template<std::size_t I>
    requires
      (... && std::ranges::bidirectional_range<maybe_const<Const, Views>>)
      && (... && std::ranges::common_range<maybe_const<Const, Views>>)
    constexpr void decrement_impl(iter_t<I>& it) {
      if constexpr(I != 0) {
        if(it == std::ranges::begin(std::get<I>(view_->views_))) {
          it_ = indexed_iter_t<I - 1>{std::ranges::end(std::get<I - 1>(view_->views_))};
          decrement_impl<I - 1>(std::get<I - 1>(it_).value);
          return;
        }
      }
      --it;
    }

  public:
    constexpr void decrement()
    noexcept((... && (
      noexcept(--std::declval<std::ranges::iterator_t<maybe_const<Const, Views>>&>())
      && noexcept(std::ranges::begin(std::declval<Views&>()))
      && noexcept(std::ranges::end(std::declval<Views&>()))
      && is_nothrow_equality_comparable_v<std::ranges::iterator_t<Views&>>
      && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<Views&>>
    )))
    requires
      (... && std::ranges::bidirectional_range<maybe_const<Const, Views>>)
      && (... && std::ranges::common_range<maybe_const<Const, Views>>)
    {
      assert(!it_.valueless_by_exception());
      std::visit([&]<std::size_t I>(indexed_iter_t<I>& value) {
        this->decrement_impl<I>(value.value);
      }, it_);
    }

  private:
    struct distance_visitor {
      maybe_const<Const, concat_view>& view;

      template<std::size_t I, std::size_t J>
      [[nodiscard]] difference_type operator()(
        indexed_iter_t<I> const& a,
        indexed_iter_t<J> const& b
      )
      requires
        (... && std::ranges::random_access_range<maybe_const<Const, Views>>)
        && (... && std::ranges::common_range<maybe_const<Const, Views>>)
      {
        if constexpr(I == J) {
          return b.value - a.value;
        } else if constexpr(I > J) {
          return -(*this)(b, a);
        } else {
          return (std::ranges::end(std::get<I>(view.views_)) - a.value)
            + view.template count_size_as_diff<Const, I + 1, J>()
            + (b.value - std::ranges::begin(std::get<J>(view.views_)));
        }
      }
    };

  protected:
    [[nodiscard]] constexpr difference_type distance_to(iterator_core other) const
    noexcept((... && (
      noexcept(std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>()
        - std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>())
      && noexcept(std::ranges::begin(std::declval<Views&>()))
      && noexcept(std::ranges::end(std::declval<Views&>()))
      && noexcept(std::ranges::size(std::declval<Views&>()))
    )))
    requires
      (... && std::ranges::random_access_range<maybe_const<Const, Views>>)
      && (... && std::ranges::common_range<maybe_const<Const, Views>>)
    {
      assert(view_ == other.view_);
      return std::visit(distance_visitor{*view_}, it_, other.it_);
    }

    [[nodiscard]] constexpr difference_type distance_to(std::default_sentinel_t) const
    noexcept((... && (
      noexcept(std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>()
        - std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>())
      && noexcept(std::ranges::end(std::declval<Views&>()))
      && noexcept(std::ranges::size(std::declval<Views&>()))
    )))
    requires
      (... && std::ranges::random_access_range<maybe_const<Const, Views>>)
      && (... && std::ranges::common_range<maybe_const<Const, Views>>)
    {
      return std::visit([&]<std::size_t I>(indexed_iter_t<I> const& value) -> difference_type {
        return (std::ranges::end(std::get<I>(view_->views_)) - value.value)
          + view_->template count_size_as_diff<Const, I, sizeof...(Views)>();
      }, it_);
    }

    constexpr void advance(difference_type offset)
    noexcept((... && (
      noexcept(std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>()
        - std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>())
      && noexcept(std::ranges::begin(std::declval<Views&>()))
      && noexcept(std::ranges::end(std::declval<Views&>()))
      && noexcept(std::declval<std::ranges::iterator_t<maybe_const<Const, Views>>&>()
        += offset)
      && std::is_nothrow_move_constructible_v<
        std::ranges::iterator_t<maybe_const<Const, Views>>>
    )))
    requires
      (... && std::ranges::random_access_range<maybe_const<Const, Views>>)
      && (... && std::ranges::common_range<maybe_const<Const, Views>>)
    {
      while(offset != 0) {
        std::visit([&]<std::size_t I>(indexed_iter_t<I>& value) {
          if(offset > 0) {
            if constexpr(I < sizeof...(Views) - 1) {
              difference_type const dist{
                std::ranges::end(std::get<I>(view_->views_)) - value.value};
              if(dist < offset) {
                offset -= dist;
                it_ = indexed_iter_t<I + 1>{
                  std::ranges::begin(std::get<I + 1>(view_->views_))};
                return;
              }
            }
          } else {
            if constexpr(I > 0) {
              difference_type const dist{
                std::ranges::begin(std::get<I>(view_->views_)) - value.value};
              if(dist > offset) {
                offset -= dist;
                it_ = indexed_iter_t<I - 1>{std::ranges::end(std::get<I - 1>(view_->views_))};
                return;
              }
            }
          }
          value.value += offset;
          if(offset > 0) {
            check_range_end<I>();
          }
          offset = 0;
        }, it_);
      }
    }

    [[nodiscard]] friend constexpr decltype(auto) iter_move(iterator_core const& it)
    noexcept((... && noexcept(std::ranges::iter_move(
      std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>()))))
    {
      assert(!it.it_.valueless_by_exception());
      return std::visit([]<std::size_t I>(indexed_iter_t<I> const& value)
          -> detail_::concat_view::rvalue_reference_t<maybe_const<Const, Views>...> {
        return std::ranges::iter_move(value.value);
      }, it.it_);
    }

    friend constexpr void iter_swap(iterator_core const& a, iterator_core const& b)
    noexcept((
      std::is_nothrow_swappable_v<
        detail_::concat_view::reference_t<maybe_const<Const, Views>...>>
      && ... && noexcept(std::ranges::iter_swap(
        std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>(),
        std::declval<std::ranges::iterator_t<maybe_const<Const, Views>> const&>()
      ))
    ))
    requires
      (std::swappable<detail_::concat_view::reference_t<maybe_const<Const, Views>...>>
      && ... && std::indirectly_swappable<std::ranges::iterator_t<maybe_const<Const, Views>>>)
    {
      assert(!a.it_.valueless_by_exception());
      assert(!b.it_.valueless_by_exception());
      std::visit([]<std::size_t I, std::size_t J>(
        indexed_iter_t<I> const& a,
        indexed_iter_t<J> const& b
      ) {
        if constexpr(std::same_as<iter_t<I>, iter_t<J>>) {
          std::ranges::iter_swap(a.value, b.value);
        } else {
          std::ranges::swap(*a.value, *b.value);
        }
      }, a.it_, b.it_);
    }
  };

  using iterator = util::iterator_facade<iterator_core<false>>;
  using const_iterator = util::iterator_facade<iterator_core<true>>;

public:
  constexpr concat_view() = default;

  constexpr concat_view(Views... views)
  noexcept((... && std::is_nothrow_move_constructible_v<Views>)):
  views_{std::move(views)...} {}

  [[nodiscard]] constexpr iterator begin()
  noexcept((... && (
    noexcept(std::ranges::begin(std::declval<Views&>()))
    && noexcept(std::ranges::end(std::declval<Views&>()))
    && is_nothrow_equality_comparable_with_v<std::ranges::iterator_t<Views&>,
      std::ranges::sentinel_t<Views&>>
    && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<Views&>>
  )))
  requires (!(... && (std::ranges::range<Views const>
    && std::same_as<std::ranges::iterator_t<Views>, std::ranges::iterator_t<Views const>>
    && std::same_as<std::ranges::sentinel_t<Views>, std::ranges::sentinel_t<Views const>>)))
  {
    return iterator{make_begin{}, *this};
  }

  [[nodiscard]] constexpr const_iterator begin() const
  noexcept((... && (
    noexcept(std::ranges::begin(std::declval<Views const&>()))
    && noexcept(std::ranges::end(std::declval<Views const&>()))
    && is_nothrow_equality_comparable_v<std::ranges::iterator_t<Views const&>>
    && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<Views const&>>
  )))
  requires (... && std::ranges::range<Views const>)
    && detail_::concat_view::concatable<Views const...>
  {
    return const_iterator{make_begin{}, *this};
  }

private:
  [[nodiscard]] static consteval bool end_noexcept() noexcept {
    if constexpr(detail_::concat_view::is_common_v<Views...>) {
      using last_view_t = std::tuple_element_t<sizeof...(Views) - 1, std::tuple<Views...>>&;
      return noexcept(std::ranges::end(std::declval<last_view_t>()))
        && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<last_view_t>>;
    } else {
      return true;
    }
  }

public:
  [[nodiscard]] constexpr auto end()
  noexcept(end_noexcept())
  requires (!(... && (std::ranges::range<Views const>
    && std::same_as<std::ranges::iterator_t<Views>, std::ranges::iterator_t<Views const>>
    && std::same_as<std::ranges::sentinel_t<Views>, std::ranges::sentinel_t<Views const>>)))
  {
    if constexpr(detail_::concat_view::is_common_v<Views...>) {
      return iterator{make_end{}, *this};
    } else {
      return std::default_sentinel;
    }
  }

private:
  [[nodiscard]] static consteval bool end_const_noexcept() noexcept {
    if constexpr(detail_::concat_view::is_common_v<Views const...>) {
      using last_view_t = std::tuple_element_t<sizeof...(Views) - 1,
        std::tuple<Views...>> const&;
      return noexcept(std::ranges::end(std::declval<last_view_t>()))
        && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<last_view_t>>;
    } else {
      return true;
    }
  }

public:
  [[nodiscard]] constexpr auto end() const
  requires (... && std::ranges::range<Views const>)
    && detail_::concat_view::concatable<Views const...>
  {
    if constexpr(detail_::concat_view::is_common_v<Views const...>) {
      return const_iterator{make_end{}, *this};
    } else {
      return std::default_sentinel;
    }
  }

  [[nodiscard]] constexpr auto size()
  noexcept((... && noexcept(std::ranges::size(std::declval<Views&>()))))
  requires (... && std::ranges::sized_range<Views>)
  {
    return std::apply([](Views&... views) -> detail_::concat_view::size_t<Views...> {
      return (static_cast<detail_::concat_view::size_t<Views...>>(
        std::ranges::size(views)) + ...);
    }, views_);
  }

  [[nodiscard]] constexpr auto size() const
  noexcept((... && noexcept(std::ranges::size(std::declval<Views const&>()))))
  requires (... && std::ranges::sized_range<Views const>)
  {
    return std::apply([](Views const&... views)
        -> detail_::concat_view::size_t<Views const...> {
      return (static_cast<detail_::concat_view::size_t<Views const...>>(
        std::ranges::size(views)) + ...);
    }, views_);
  }
};

template<typename... Ts>
concat_view(Ts...) -> concat_view<std::views::all_t<Ts>...>;

namespace detail_::concat_view {

struct niebloid {
  template<std::ranges::viewable_range Range>
  [[nodiscard]] constexpr auto operator()(Range&& range) const {
    return std::views::all(std::forward<Range>(range));
  }

  template<std::ranges::viewable_range... Ranges>
  requires (sizeof...(Ranges) >= 2) && concatable<std::views::all_t<Ranges>...>
  [[nodiscard]] constexpr auto operator()(Ranges&&... ranges) const
  noexcept((... && std::is_nothrow_move_constructible_v<std::views::all_t<Ranges>>))
  {
    return util::concat_view<std::views::all_t<Ranges>...>{
      std::views::all(std::forward<Ranges>(ranges))...};
  }
};

} // detail_::concat_view

inline constexpr detail_::concat_view::niebloid concat;

} // util

#endif
