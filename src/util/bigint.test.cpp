#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "util/bigint.hpp"
#include "util/util.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <iomanip>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>

using namespace util::bigint_literals;
using namespace std::string_literals;
using namespace std::string_view_literals;

namespace {

static_assert(std::regular<util::bigint>);

util::bigint const pos_small{0x3210_big};
util::bigint const pos_medium{0x3210'FEDCBA9876543210_big};
util::bigint const pos_big{
  0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210_big};
util::bigint const neg_small{-pos_small};
util::bigint const neg_medium{-pos_medium};
util::bigint const neg_big{-pos_big};

util::bigint const pos_big_sq{0x9CAA4AF'1235A1DF76F1407C'EEDCCF4EE417DC4A'\
CB83FCBE513E1468'983AC7B67E97789A'BB939A471170DCCC'DEEC6CD7A44A4100_big};

// Additive test data //////////////////////////////////////////////////////////

util::bigint const add_test_data_a[]{
  0x0_big,
  0x0_big,
  0x0_big,
  0x0_big,

  pos_small,
  pos_small,
  pos_small,
  pos_small,

  pos_medium,
  pos_medium,
  pos_medium,
  pos_medium,

  pos_big,
  pos_big,
  pos_big,
  pos_big,

  0x10'FFFFFFFFFFFFFFFF'0000000000000000_big,
  0xFFFFFFFFFFFFFFFF'0000000000000000_big,

  pos_big,
  pos_big,
  pos_medium
};

util::bigint const add_test_data_b[]{
  0x0_big,
  pos_small,
  pos_medium,
  pos_big,

  0x0_big,
  pos_small,
  pos_medium,
  pos_big,

  0x0_big,
  pos_small,
  pos_medium,
  pos_big,

  0x0_big,
  pos_small,
  pos_medium,
  pos_big,

  0x1'0000000000000000_big,
  0x1'0000000000000000_big,

  neg_small,
  neg_medium,
  neg_small
};

util::bigint const add_test_data_sum[]{
  0x0_big,
  pos_small,
  pos_medium,
  pos_big,

  pos_small,
  0x6420_big,
  0x3210'FEDCBA9876546420_big,
  0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876546420_big,

  pos_medium,
  0x3210'FEDCBA9876546420_big,
  0x6421'FDB97530ECA86420_big,
  0x3210'FEDCBA9876543210'FEDCBA9876546421'FDB97530ECA86420_big,

  pos_big,
  0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876546420_big,
  0x3210'FEDCBA9876543210'FEDCBA9876546421'FDB97530ECA86420_big,
  0x6421'FDB97530ECA86421'FDB97530ECA86421'FDB97530ECA86420_big,

  0x11'0000000000000000'0000000000000000_big,
  0x1'0000000000000000'0000000000000000_big,

  0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876540000_big,
  0x3210'FEDCBA9876543210'FEDCBA9876540000'0000000000000000_big,
  0x3210FEDCBA9876540000_big
};

static_assert(sizeof(add_test_data_a) == sizeof(add_test_data_sum));
static_assert(sizeof(add_test_data_b) == sizeof(add_test_data_sum));

auto const add_test_data{boost::unit_test::data::make(add_test_data_a)
  ^ boost::unit_test::data::make(add_test_data_b)
  ^ boost::unit_test::data::make(add_test_data_sum)};

// Multiplicative test data ////////////////////////////////////////////////////

util::bigint const mul_test_data_a[]{
  0x0_big,
  pos_small,
  pos_medium,
  pos_big,

  pos_small,
  pos_medium,
  pos_big,

  pos_medium,
  pos_big,

  pos_big
};

util::bigint const mul_test_data_b[]{
  0x0_big,
  0x0_big,
  0x0_big,
  0x0_big,

  pos_small,
  pos_small,
  pos_small,

  pos_medium,
  pos_medium,

  pos_big
};

util::bigint const mul_test_data_product[]{
  0x0_big,
  0x0_big,
  0x0_big,
  0x0_big,

  0x9CA4100_big,
  0x9CA72D7'0A3D70A3D70A4100_big,
  0x9CA72D7'0A3D70A3D70A72D7'0A3D70A3D70A72D7'0A3D70A3D70A4100_big,

  0x9CAA4AF'1235A1DF76F0DCCC'DEEC6CD7A44A4100_big,
  0x9CAA4AF'1235A1DF76F10EA4'E6E49E1344310EA4'E6E49E134430DCCC'\
DEEC6CD7A44A4100_big,

  pos_big_sq
};

static_assert(sizeof(mul_test_data_a) == sizeof(mul_test_data_product));
static_assert(sizeof(mul_test_data_b) == sizeof(mul_test_data_product));

auto const mul_test_data{boost::unit_test::data::make(mul_test_data_a)
  ^ boost::unit_test::data::make(mul_test_data_b)
  ^ boost::unit_test::data::make(mul_test_data_product)};

// Division test data //////////////////////////////////////////////////////////

util::bigint const div_test_data_a[]{
  pos_small,
  pos_small,
  pos_small,

  pos_medium,
  pos_medium,
  pos_medium,

  pos_big,
  pos_big,
  pos_big
};

util::bigint const div_test_data_b[]{
  pos_small,
  pos_medium,
  pos_big,

  pos_small,
  pos_medium,
  pos_big,

  pos_small,
  pos_medium,
  pos_big
};

util::bigint const div_test_data_quotient[]{
  0x1_big,
  0x0_big,
  0x0_big,

  0x1'00051743F78A319F_big,
  0x1_big,
  0x0_big,

  0x1'00051743F78A319F'33CB9013116D6E06'C0BD2E46D7E63D20_big,
  0x1'0000000000000000'0005172A0D492BE5_big,
  0x1_big
};

util::bigint const div_test_data_remainder[]{
  0x0_big,
  pos_small,
  pos_small,

  0xA20_big,
  0x0_big,
  pos_medium,

  0x2010_big,
  0xC59'07F68929620AB9C0_big,
  0x0_big
};

static_assert(sizeof(div_test_data_a) == sizeof(div_test_data_quotient));
static_assert(sizeof(div_test_data_b) == sizeof(div_test_data_quotient));
static_assert(sizeof(div_test_data_remainder)
  == sizeof(div_test_data_quotient));

auto const div_test_data{boost::unit_test::data::make(div_test_data_a)
  ^ boost::unit_test::data::make(div_test_data_b)
  ^ boost::unit_test::data::make(div_test_data_quotient)
  ^ boost::unit_test::data::make(div_test_data_remainder)};

// Bitwise test data ///////////////////////////////////////////////////////////

util::bigint const bit_small{0xC396_big};
util::bigint const bit_medium{0xA284'081D3848C5775C8A_big};
util::bigint const bit_big{
  0x9F25'BEC823731225B33D'9C4A4194951D1259'4CC16C37F406710B_big};

util::bigint const bit_test_data_a[]{
  bit_small,
  bit_small,
  bit_small,

  bit_medium,
  bit_medium,

  bit_big
};

util::bigint const bit_test_data_b[]{
  bit_small,
  bit_medium,
  bit_big,

  bit_medium,
  bit_big,

  bit_big
};

util::bigint const bit_test_data_and[]{
  bit_small,
  0x4082_big,
  0x4102_big,

  bit_medium,
  0x200'08012800C406500A_big,

  bit_big
};

util::bigint const bit_test_data_or[]{
  bit_small,
  0xA284'081D3848C577DF9E_big,
  0x9F25'BEC823731225B33D'9C4A4194951D1259'4CC16C37F406F39F_big,

  bit_medium,
  0x9F25'BEC823731225B33D'9C4A4194951DB2DD'4CDD7C7FF5777D8B_big,

  bit_big
};

util::bigint const bit_test_data_not_and[]{
  0x0_big,
  0xA284'081D3848C5771C08_big,
  0x9F25'BEC823731225B33D'9C4A4194951D1259'4CC16C37F4063009_big,

  0x0_big,
  0x9F25'BEC823731225B33D'9C4A4194951D1059'44C0443730002101_big,

  0x0_big
};

util::bigint const bit_test_data_not_or[]{
  -0x1_big,
  -0x8315_big,
  -0x8295_big,

  -0x1_big,
  -0xA084'001C104801710C81_big,

  -0x1_big
};

util::bigint const bit_test_data_xor[]{
  0x0_big,
  0xA284'081D3848C5779F1C_big,
  0x9F25'BEC823731225B33D'9C4A4194951D1259'4CC16C37F406B29D_big,

  0x0_big,
  0x9F25'BEC823731225B33D'9C4A4194951DB0DD'44DC547F31712D81_big,

  0x0_big
};

static_assert(sizeof(bit_test_data_a) == sizeof(bit_test_data_b));
static_assert(sizeof(bit_test_data_a) == sizeof(bit_test_data_and));
static_assert(sizeof(bit_test_data_a) == sizeof(bit_test_data_or));
static_assert(sizeof(bit_test_data_a) == sizeof(bit_test_data_not_and));
static_assert(sizeof(bit_test_data_a) == sizeof(bit_test_data_not_or));
static_assert(sizeof(bit_test_data_a) == sizeof(bit_test_data_xor));

auto const bit_test_data{boost::unit_test::data::make(bit_test_data_a)
  ^ boost::unit_test::data::make(bit_test_data_b)
  ^ boost::unit_test::data::make(bit_test_data_and)
  ^ boost::unit_test::data::make(bit_test_data_or)
  ^ boost::unit_test::data::make(bit_test_data_not_and)
  ^ boost::unit_test::data::make(bit_test_data_not_or)};

auto const xor_test_data{boost::unit_test::data::make(bit_test_data_a)
  ^ boost::unit_test::data::make(bit_test_data_b)
  ^ boost::unit_test::data::make(bit_test_data_xor)};

// Bitwise left shift test data ////////////////////////////////////////////////

util::bigint const pos_bigger{
  0xBA9876543210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210_big};
util::bigint const neg_bigger{-pos_bigger};

util::bigint const lsh_test_data_a[]{
  0x0_big,
  0x0_big,
  0x0_big,
  0x0_big,

  pos_bigger,
  pos_bigger,
  pos_bigger,
  pos_bigger,
  pos_bigger,
  pos_bigger,
  pos_bigger,
  pos_bigger,

  neg_bigger,
  neg_bigger,
  neg_bigger,
  neg_bigger,
  neg_bigger,
  neg_bigger,
  neg_bigger,
  neg_bigger
};

std::size_t const lsh_test_data_off[]{
  0,
  10,
  64,
  100,

  0,
  10,
  64,
  100,
  256,
  260,
  320,
  324,

  0,
  10,
  64,
  100,
  256,
  260,
  320,
  324
};

util::bigint const lsh_test_data_result[]{
  0x0_big,
  0x0_big,
  0x0_big,
  0x0_big,

  pos_bigger,
  0x2EA61D950C843FB'72EA61D950C843FB'72EA61D950C843FB'72EA61D950C84000_big,
  0xBA9876543210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210'\
0000000000000000_big,
  0xBA987'6543210FEDCBA987'6543210FEDCBA987'6543210FEDCBA987'6543210000000000'\
0000000000000000_big,
  0xBA9876543210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210'\
0000000000000000'0000000000000000'0000000000000000'0000000000000000_big,
  0xB'A9876543210F'EDCBA9876543210F'EDCBA9876543210F'EDCBA98765432100'\
0000000000000000'0000000000000000'0000000000000000'0000000000000000_big,
  0xBA9876543210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210'\
0000000000000000'0000000000000000'0000000000000000'0000000000000000'\
0000000000000000_big,
  0xB'A9876543210F'EDCBA9876543210F'EDCBA9876543210F'EDCBA98765432100'\
0000000000000000'0000000000000000'0000000000000000'0000000000000000'\
0000000000000000_big,

  neg_bigger,
  -0x2EA61D950C843FB'72EA61D950C843FB'72EA61D950C843FB'72EA61D950C84000_big,
  -0xBA9876543210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210'\
0000000000000000_big,
  -0xBA987'6543210FEDCBA987'6543210FEDCBA987'6543210FEDCBA987'6543210000000000'\
0000000000000000_big,
  -0xBA9876543210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210'\
0000000000000000'0000000000000000'0000000000000000'0000000000000000_big,
  -0xB'A9876543210F'EDCBA9876543210F'EDCBA9876543210F'EDCBA98765432100'\
0000000000000000'0000000000000000'0000000000000000'0000000000000000_big,
  -0xBA9876543210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210'\
0000000000000000'0000000000000000'0000000000000000'0000000000000000'\
0000000000000000_big,
  -0xB'A9876543210F'EDCBA9876543210F'EDCBA9876543210F'EDCBA98765432100'\
0000000000000000'0000000000000000'0000000000000000'0000000000000000'\
0000000000000000_big
};

static_assert(sizeof(lsh_test_data_a) == sizeof(lsh_test_data_result));
static_assert(sizeof(lsh_test_data_off) / sizeof(std::size_t)
  == sizeof(lsh_test_data_result) / sizeof(util::bigint));

auto const lsh_test_data{boost::unit_test::data::make(lsh_test_data_a)
  ^ boost::unit_test::data::make(lsh_test_data_off)
  ^ boost::unit_test::data::make(lsh_test_data_result)};

// Bitwise right shift test data ///////////////////////////////////////////////

util::bigint const rsh_test_data_a[]{
  0x0_big,
  0x0_big,
  0x0_big,
  0x0_big,

  pos_big,
  pos_big,
  pos_big,
  pos_big,
  pos_big,
  pos_big,

  neg_big,
  neg_big,
  neg_big,
  neg_big,
  neg_big,
  neg_big
};

std::size_t const rsh_test_data_off[]{
  0,
  10,
  64,
  100,

  0,
  10,
  64,
  100,
  256,
  260,

  0,
  10,
  64,
  100,
  256,
  260,
};

util::bigint const rsh_test_data_result[]{
  0x0_big,
  0x0_big,
  0x0_big,
  0x0_big,

  pos_big,
  0xC'843FB72EA61D950C'843FB72EA61D950C'843FB72EA61D950C_big,
  0x3210'FEDCBA9876543210'FEDCBA9876543210_big,
  0x3210FEDCBA9'876543210FEDCBA9_big,
  0x0_big,
  0x0_big,

  neg_big,
  -0xC'843FB72EA61D950C'843FB72EA61D950C'843FB72EA61D950D_big,
  -0x3210'FEDCBA9876543210'FEDCBA9876543211_big,
  -0x3210FEDCBA9'876543210FEDCBAA_big,
  -0x1_big,
  -0x1_big
};

static_assert(sizeof(rsh_test_data_a) == sizeof(rsh_test_data_result));
static_assert(sizeof(rsh_test_data_off) / sizeof(std::size_t)
  == sizeof(rsh_test_data_result) / sizeof(util::bigint));

auto const rsh_test_data{boost::unit_test::data::make(rsh_test_data_a)
  ^ boost::unit_test::data::make(rsh_test_data_off)
  ^ boost::unit_test::data::make(rsh_test_data_result)};

}

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(bigint)

// Basic operations ////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(basic)

BOOST_AUTO_TEST_CASE(construct) {
  util::bigint copy_small{pos_small};
  BOOST_TEST(copy_small == pos_small);

  util::bigint move_small{std::move(copy_small)};
  BOOST_TEST(move_small == pos_small);

  util::bigint copy_medium{pos_medium};
  BOOST_TEST(copy_medium == pos_medium);

  util::bigint move_medium{std::move(copy_medium)};
  BOOST_TEST(move_medium == pos_medium);

  util::bigint copy_big{pos_big};
  BOOST_TEST(copy_big == pos_big);

  util::bigint move_big{std::move(copy_big)};
  BOOST_TEST(move_big == pos_big);
}

BOOST_AUTO_TEST_CASE(assign) {
  util::bigint num1;
  util::bigint num2;

  {
    num1 = pos_small;
    BOOST_TEST(num1 == pos_small);

    num2 = std::move(num1);
    BOOST_TEST(num2 == pos_small);
  }

  {
    num1 = pos_medium;
    BOOST_TEST(num1 == pos_medium);

    num2 = std::move(num1);
    BOOST_TEST(num2 == pos_medium);
  }

  {
    num1 = pos_big;
    BOOST_TEST(num1 == pos_big);

    num2 = std::move(num1);
    BOOST_TEST(num2 == pos_big);
  }
}

BOOST_AUTO_TEST_CASE(size) {
  BOOST_TEST((0x0_big).size() == 0);
  BOOST_TEST(pos_small.size() == 2);
  BOOST_TEST(pos_medium.size() == 10);
  BOOST_TEST(pos_big.size() == 26);
  BOOST_TEST(neg_small.size() == 2);
  BOOST_TEST(neg_medium.size() == 10);
  BOOST_TEST(neg_big.size() == 26);
}

BOOST_AUTO_TEST_CASE(capacity) {
  BOOST_TEST((0x0_big).capacity() >= 0);
  BOOST_TEST(pos_small.capacity() >= 2);
  BOOST_TEST(pos_medium.capacity() >= 10);
  BOOST_TEST(pos_big.capacity() >= 26);
  BOOST_TEST(neg_small.capacity() >= 2);
  BOOST_TEST(neg_medium.capacity() >= 10);
  BOOST_TEST(neg_big.capacity() >= 26);
}

BOOST_AUTO_TEST_CASE(reserve) {
  {
    util::bigint num{0x0_big};
    num.reserve(20);
    num.reserve(15);
    BOOST_TEST(num.capacity() >= 20);
  }
  {
    util::bigint num{pos_small};
    num.reserve(20);
    num.reserve(15);
    BOOST_TEST(num.capacity() >= 20);
  }
  {
    util::bigint num{pos_medium};
    num.reserve(20);
    num.reserve(15);
    BOOST_TEST(num.capacity() >= 20);
  }
  {
    util::bigint num{pos_big};
    num.reserve(20);
    num.reserve(15);
    BOOST_TEST(num.capacity() >= 26);
  }
  {
    util::bigint num{neg_small};
    num.reserve(20);
    num.reserve(15);
    BOOST_TEST(num.capacity() >= 20);
  }
  {
    util::bigint num{neg_medium};
    num.reserve(20);
    num.reserve(15);
    BOOST_TEST(num.capacity() >= 20);
  }
  {
    util::bigint num{neg_big};
    num.reserve(20);
    num.reserve(15);
    BOOST_TEST(num.capacity() >= 26);
  }
}

BOOST_AUTO_TEST_CASE(shrink_to_fit) {
  {
    util::bigint num{0x0_big};
    std::size_t const old_capacity{num.capacity()};
    num.reserve(100);
    num.shrink_to_fit();
    BOOST_TEST(num.capacity() <= old_capacity);
  }
  {
    util::bigint num{pos_small};
    std::size_t const old_capacity{num.capacity()};
    num.reserve(100);
    num.shrink_to_fit();
    BOOST_TEST(num.capacity() <= old_capacity);
  }
  {
    util::bigint num{pos_medium};
    std::size_t const old_capacity{num.capacity()};
    num.reserve(100);
    num.shrink_to_fit();
    BOOST_TEST(num.capacity() <= old_capacity);
  }
  {
    util::bigint num{pos_big};
    std::size_t const old_capacity{num.capacity()};
    num.reserve(100);
    num.shrink_to_fit();
    BOOST_TEST(num.capacity() <= old_capacity);
  }
  {
    util::bigint num{neg_small};
    std::size_t const old_capacity{num.capacity()};
    num.reserve(100);
    num.shrink_to_fit();
    BOOST_TEST(num.capacity() <= old_capacity);
  }
  {
    util::bigint num{neg_medium};
    std::size_t const old_capacity{num.capacity()};
    num.reserve(100);
    num.shrink_to_fit();
    BOOST_TEST(num.capacity() <= old_capacity);
  }
  {
    util::bigint num{neg_big};
    std::size_t const old_capacity{num.capacity()};
    num.reserve(100);
    num.shrink_to_fit();
    BOOST_TEST(num.capacity() <= old_capacity);
  }
}

BOOST_AUTO_TEST_CASE(op_indexing) {
  BOOST_TEST((0x0_big)[0] == 0);
  BOOST_TEST((0x0_big)[100] == 0);

  BOOST_TEST(pos_small[0] == 0x10);
  BOOST_TEST(pos_small[1] == 0x32);
  BOOST_TEST(pos_small[2] == 0);
  BOOST_TEST(pos_small[100] == 0);

  BOOST_TEST(pos_medium[0] == 0x10);
  BOOST_TEST(pos_medium[1] == 0x32);
  BOOST_TEST(pos_medium[2] == 0x54);
  BOOST_TEST(pos_medium[3] == 0x76);
  BOOST_TEST(pos_medium[4] == 0x98);
  BOOST_TEST(pos_medium[5] == 0xBA);
  BOOST_TEST(pos_medium[6] == 0xDC);
  BOOST_TEST(pos_medium[7] == 0xFE);
  BOOST_TEST(pos_medium[8] == 0x10);
  BOOST_TEST(pos_medium[9] == 0x32);
  BOOST_TEST(pos_medium[10] == 0);
  BOOST_TEST(pos_medium[100] == 0);

  BOOST_TEST(pos_big[0] == 0x10);
  BOOST_TEST(pos_big[1] == 0x32);
  BOOST_TEST(pos_big[2] == 0x54);
  BOOST_TEST(pos_big[3] == 0x76);
  BOOST_TEST(pos_big[4] == 0x98);
  BOOST_TEST(pos_big[5] == 0xBA);
  BOOST_TEST(pos_big[6] == 0xDC);
  BOOST_TEST(pos_big[7] == 0xFE);
  BOOST_TEST(pos_big[8] == 0x10);
  BOOST_TEST(pos_big[9] == 0x32);
  BOOST_TEST(pos_big[10] == 0x54);
  BOOST_TEST(pos_big[11] == 0x76);
  BOOST_TEST(pos_big[12] == 0x98);
  BOOST_TEST(pos_big[13] == 0xBA);
  BOOST_TEST(pos_big[14] == 0xDC);
  BOOST_TEST(pos_big[15] == 0xFE);
  BOOST_TEST(pos_big[16] == 0x10);
  BOOST_TEST(pos_big[17] == 0x32);
  BOOST_TEST(pos_big[18] == 0x54);
  BOOST_TEST(pos_big[19] == 0x76);
  BOOST_TEST(pos_big[20] == 0x98);
  BOOST_TEST(pos_big[21] == 0xBA);
  BOOST_TEST(pos_big[22] == 0xDC);
  BOOST_TEST(pos_big[23] == 0xFE);
  BOOST_TEST(pos_big[24] == 0x10);
  BOOST_TEST(pos_big[25] == 0x32);
  BOOST_TEST(pos_big[26] == 0);
  BOOST_TEST(pos_big[100] == 0);

  BOOST_TEST(neg_small[0] == 0x10);
  BOOST_TEST(neg_small[1] == 0x32);
  BOOST_TEST(neg_small[2] == 0);
  BOOST_TEST(neg_small[100] == 0);

  BOOST_TEST(neg_medium[0] == 0x10);
  BOOST_TEST(neg_medium[1] == 0x32);
  BOOST_TEST(neg_medium[2] == 0x54);
  BOOST_TEST(neg_medium[3] == 0x76);
  BOOST_TEST(neg_medium[4] == 0x98);
  BOOST_TEST(neg_medium[5] == 0xBA);
  BOOST_TEST(neg_medium[6] == 0xDC);
  BOOST_TEST(neg_medium[7] == 0xFE);
  BOOST_TEST(neg_medium[8] == 0x10);
  BOOST_TEST(neg_medium[9] == 0x32);
  BOOST_TEST(neg_medium[10] == 0);
  BOOST_TEST(neg_medium[100] == 0);

  BOOST_TEST(neg_big[0] == 0x10);
  BOOST_TEST(neg_big[1] == 0x32);
  BOOST_TEST(neg_big[2] == 0x54);
  BOOST_TEST(neg_big[3] == 0x76);
  BOOST_TEST(neg_big[4] == 0x98);
  BOOST_TEST(neg_big[5] == 0xBA);
  BOOST_TEST(neg_big[6] == 0xDC);
  BOOST_TEST(neg_big[7] == 0xFE);
  BOOST_TEST(neg_big[8] == 0x10);
  BOOST_TEST(neg_big[9] == 0x32);
  BOOST_TEST(neg_big[10] == 0x54);
  BOOST_TEST(neg_big[11] == 0x76);
  BOOST_TEST(neg_big[12] == 0x98);
  BOOST_TEST(neg_big[13] == 0xBA);
  BOOST_TEST(neg_big[14] == 0xDC);
  BOOST_TEST(neg_big[15] == 0xFE);
  BOOST_TEST(neg_big[16] == 0x10);
  BOOST_TEST(neg_big[17] == 0x32);
  BOOST_TEST(neg_big[18] == 0x54);
  BOOST_TEST(neg_big[19] == 0x76);
  BOOST_TEST(neg_big[20] == 0x98);
  BOOST_TEST(neg_big[21] == 0xBA);
  BOOST_TEST(neg_big[22] == 0xDC);
  BOOST_TEST(neg_big[23] == 0xFE);
  BOOST_TEST(neg_big[24] == 0x10);
  BOOST_TEST(neg_big[25] == 0x32);
  BOOST_TEST(neg_big[26] == 0);
  BOOST_TEST(neg_big[100] == 0);
}

BOOST_AUTO_TEST_CASE(twos_compl) {
  BOOST_TEST((0x0_big).twos_compl_at(0) == 0);
  BOOST_TEST((0x0_big).twos_compl_at(100) == 0);

  BOOST_TEST(pos_medium.twos_compl_at(0) == 0x10);
  BOOST_TEST(pos_medium.twos_compl_at(1) == 0x32);
  BOOST_TEST(pos_medium.twos_compl_at(2) == 0x54);
  BOOST_TEST(pos_medium.twos_compl_at(3) == 0x76);
  BOOST_TEST(pos_medium.twos_compl_at(4) == 0x98);
  BOOST_TEST(pos_medium.twos_compl_at(5) == 0xBA);
  BOOST_TEST(pos_medium.twos_compl_at(6) == 0xDC);
  BOOST_TEST(pos_medium.twos_compl_at(7) == 0xFE);
  BOOST_TEST(pos_medium.twos_compl_at(8) == 0x10);
  BOOST_TEST(pos_medium.twos_compl_at(9) == 0x32);
  BOOST_TEST(pos_medium.twos_compl_at(10) == 0);
  BOOST_TEST(pos_medium.twos_compl_at(100) == 0);

  {
    util::bigint const num{0x3210'0000000000000000_big};
    BOOST_TEST(num.twos_compl_at(0) == 0);
    BOOST_TEST(num.twos_compl_at(1) == 0);
    BOOST_TEST(num.twos_compl_at(2) == 0);
    BOOST_TEST(num.twos_compl_at(3) == 0);
    BOOST_TEST(num.twos_compl_at(4) == 0);
    BOOST_TEST(num.twos_compl_at(5) == 0);
    BOOST_TEST(num.twos_compl_at(6) == 0);
    BOOST_TEST(num.twos_compl_at(7) == 0);
    BOOST_TEST(num.twos_compl_at(8) == 0x10);
    BOOST_TEST(num.twos_compl_at(9) == 0x32);
    BOOST_TEST(num.twos_compl_at(10) == 0);
    BOOST_TEST(num.twos_compl_at(100) == 0);
  }

  BOOST_TEST(neg_medium.twos_compl_at(0) == 0xF0);
  BOOST_TEST(neg_medium.twos_compl_at(1) == 0xCD);
  BOOST_TEST(neg_medium.twos_compl_at(2) == 0xAB);
  BOOST_TEST(neg_medium.twos_compl_at(3) == 0x89);
  BOOST_TEST(neg_medium.twos_compl_at(4) == 0x67);
  BOOST_TEST(neg_medium.twos_compl_at(5) == 0x45);
  BOOST_TEST(neg_medium.twos_compl_at(6) == 0x23);
  BOOST_TEST(neg_medium.twos_compl_at(7) == 0x01);
  BOOST_TEST(neg_medium.twos_compl_at(8) == 0xEF);
  BOOST_TEST(neg_medium.twos_compl_at(9) == 0xCD);
  BOOST_TEST(neg_medium.twos_compl_at(10) == 0xFF);
  BOOST_TEST(neg_medium.twos_compl_at(100) == 0xFF);

  {
    util::bigint const num{-0x3210'0000000000000000_big};
    BOOST_TEST(num.twos_compl_at(0) == 0);
    BOOST_TEST(num.twos_compl_at(1) == 0);
    BOOST_TEST(num.twos_compl_at(2) == 0);
    BOOST_TEST(num.twos_compl_at(3) == 0);
    BOOST_TEST(num.twos_compl_at(4) == 0);
    BOOST_TEST(num.twos_compl_at(5) == 0);
    BOOST_TEST(num.twos_compl_at(6) == 0);
    BOOST_TEST(num.twos_compl_at(7) == 0);
    BOOST_TEST(num.twos_compl_at(8) == 0xF0);
    BOOST_TEST(num.twos_compl_at(9) == 0xCD);
    BOOST_TEST(num.twos_compl_at(10) == 0xFF);
    BOOST_TEST(num.twos_compl_at(100) == 0xFF);
  }
}

BOOST_AUTO_TEST_CASE(twos_compl_precomputed) {
  {
    util::bigint const num{0x0_big};
    util::bigint::lnz_t const lnz{num.lowest_non_zero()};
    BOOST_TEST(num.twos_compl_at(0, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(100, lnz) == 0);
  }

  {
    util::bigint::lnz_t const lnz{pos_medium.lowest_non_zero()};
    BOOST_TEST(pos_medium.twos_compl_at(0, lnz) == 0x10);
    BOOST_TEST(pos_medium.twos_compl_at(1, lnz) == 0x32);
    BOOST_TEST(pos_medium.twos_compl_at(2, lnz) == 0x54);
    BOOST_TEST(pos_medium.twos_compl_at(3, lnz) == 0x76);
    BOOST_TEST(pos_medium.twos_compl_at(4, lnz) == 0x98);
    BOOST_TEST(pos_medium.twos_compl_at(5, lnz) == 0xBA);
    BOOST_TEST(pos_medium.twos_compl_at(6, lnz) == 0xDC);
    BOOST_TEST(pos_medium.twos_compl_at(7, lnz) == 0xFE);
    BOOST_TEST(pos_medium.twos_compl_at(8, lnz) == 0x10);
    BOOST_TEST(pos_medium.twos_compl_at(9, lnz) == 0x32);
    BOOST_TEST(pos_medium.twos_compl_at(10, lnz) == 0);
    BOOST_TEST(pos_medium.twos_compl_at(100, lnz) == 0);
  }

  {
    util::bigint const num{0x3210'0000000000000000_big};
    util::bigint::lnz_t const lnz{num.lowest_non_zero()};
    BOOST_TEST(num.twos_compl_at(0, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(1, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(2, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(3, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(4, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(5, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(6, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(7, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(8, lnz) == 0x10);
    BOOST_TEST(num.twos_compl_at(9, lnz) == 0x32);
    BOOST_TEST(num.twos_compl_at(10, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(100, lnz) == 0);
  }

  {
    util::bigint::lnz_t const lnz{neg_medium.lowest_non_zero()};
    BOOST_TEST(neg_medium.twos_compl_at(0, lnz) == 0xF0);
    BOOST_TEST(neg_medium.twos_compl_at(1, lnz) == 0xCD);
    BOOST_TEST(neg_medium.twos_compl_at(2, lnz) == 0xAB);
    BOOST_TEST(neg_medium.twos_compl_at(3, lnz) == 0x89);
    BOOST_TEST(neg_medium.twos_compl_at(4, lnz) == 0x67);
    BOOST_TEST(neg_medium.twos_compl_at(5, lnz) == 0x45);
    BOOST_TEST(neg_medium.twos_compl_at(6, lnz) == 0x23);
    BOOST_TEST(neg_medium.twos_compl_at(7, lnz) == 0x01);
    BOOST_TEST(neg_medium.twos_compl_at(8, lnz) == 0xEF);
    BOOST_TEST(neg_medium.twos_compl_at(9, lnz) == 0xCD);
    BOOST_TEST(neg_medium.twos_compl_at(10, lnz) == 0xFF);
    BOOST_TEST(neg_medium.twos_compl_at(100, lnz) == 0xFF);
  }

  {
    util::bigint const num{-0x3210'0000000000000000_big};
    util::bigint::lnz_t const lnz{num.lowest_non_zero()};
    BOOST_TEST(num.twos_compl_at(0, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(1, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(2, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(3, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(4, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(5, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(6, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(7, lnz) == 0);
    BOOST_TEST(num.twos_compl_at(8, lnz) == 0xF0);
    BOOST_TEST(num.twos_compl_at(9, lnz) == 0xCD);
    BOOST_TEST(num.twos_compl_at(10, lnz) == 0xFF);
    BOOST_TEST(num.twos_compl_at(100, lnz) == 0xFF);
  }
}

BOOST_AUTO_TEST_SUITE_END() // basic

// Built-in integers to bigint conversion //////////////////////////////////////

BOOST_AUTO_TEST_SUITE(conversion)

BOOST_AUTO_TEST_CASE(zero) {
  util::bigint const bignum{0};
  std::uint64_t const smallnum{bignum};
  BOOST_TEST(smallnum == 0);
}

BOOST_AUTO_TEST_CASE(small_pos) {
  util::bigint const bignum{100};
  std::uint64_t const smallnum{bignum};
  BOOST_TEST(smallnum == 100);
}

BOOST_AUTO_TEST_CASE(medium_pos) {
  util::bigint const bignum{static_cast<std::uint64_t>(1) << 48};
  std::uint64_t const smallnum{bignum};
  BOOST_TEST(smallnum == (static_cast<std::uint64_t>(1) << 48));
}

BOOST_AUTO_TEST_CASE(small_neg) {
  util::bigint const bignum{-100};
  std::int64_t const smallnum{bignum};
  BOOST_TEST(smallnum == -100);
}

BOOST_AUTO_TEST_CASE(medium_neg) {
  util::bigint const bignum{-(static_cast<std::int64_t>(1) << 48)};
  std::int64_t const smallnum{bignum};
  BOOST_TEST(smallnum == -(static_cast<std::int64_t>(1) << 48));
}

BOOST_AUTO_TEST_SUITE_END() // conversion

// Comparison //////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(compare)

BOOST_AUTO_TEST_CASE(is_negative) {
  BOOST_TEST(!pos_small.is_negative());
  BOOST_TEST(!pos_medium.is_negative());
  BOOST_TEST(!pos_big.is_negative());
  BOOST_TEST(!(0x0_big).is_negative());
  BOOST_TEST(neg_small.is_negative());
  BOOST_TEST(neg_medium.is_negative());
  BOOST_TEST(neg_big.is_negative());
}

BOOST_AUTO_TEST_CASE(eq) {
  BOOST_TEST(0x0_big == 0x0_big);
  BOOST_TEST(pos_small == pos_small);
  BOOST_TEST(pos_medium == pos_medium);
  BOOST_TEST(pos_big == pos_big);
  BOOST_TEST(neg_small == neg_small);
  BOOST_TEST(neg_medium == neg_medium);
  BOOST_TEST(neg_big == neg_big);

  util::bigint const num{neg_big};
  BOOST_TEST(neg_big == num);

  BOOST_TEST(pos_small != pos_big);
  BOOST_TEST(pos_small != neg_small);
}

BOOST_AUTO_TEST_CASE(rel) {
  BOOST_TEST(pos_small >= pos_small);
  BOOST_TEST(pos_small < pos_medium);
  BOOST_TEST(pos_small < pos_big);

  BOOST_TEST(pos_medium > pos_small);
  BOOST_TEST(pos_medium >= pos_medium);
  BOOST_TEST(pos_medium < pos_big);

  BOOST_TEST(pos_big > pos_small);
  BOOST_TEST(pos_big > pos_medium);
  BOOST_TEST(pos_big <= pos_big);

  BOOST_TEST(neg_small <= neg_small);
  BOOST_TEST(neg_small > neg_medium);
  BOOST_TEST(neg_small > neg_big);

  BOOST_TEST(neg_medium < neg_small);
  BOOST_TEST(neg_medium <= neg_medium);
  BOOST_TEST(neg_medium > neg_big);

  BOOST_TEST(neg_big < neg_small);
  BOOST_TEST(neg_big < neg_medium);
  BOOST_TEST(neg_big >= neg_big);

  BOOST_TEST(neg_small < pos_big);
  BOOST_TEST(neg_medium < pos_medium);
  BOOST_TEST(neg_big < pos_small);
}

BOOST_AUTO_TEST_SUITE_END() // compare

// String to bigint conversion /////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(from_string)

BOOST_AUTO_TEST_CASE(decimal) {
  #define BIGINT_FROM_STRING_TEST(str_type, str_prefix) \
    { \
      str_type const str{str_prefix##"0foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 1)); \
    } \
    { \
      str_type const str{str_prefix##"0012816foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str); \
      if(num) { \
        BOOST_TEST(*num == pos_small); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 7)); \
    } \
    { \
      str_type const str{str_prefix##"+236431836807206106575376foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str); \
      if(num) { \
        BOOST_TEST(*num == pos_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 25)); \
    } \
    { \
      str_type const str{str_prefix##"80453585044221152298169170068268828533" \
        "249754330668494132359696foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str); \
      if(num) { \
        BOOST_TEST(*num == pos_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 62)); \
    } \
    { \
      str_type const str{str_prefix##"-00foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 3)); \
    } \
    { \
      str_type const str{str_prefix##"-0012816foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str); \
      if(num) { \
        BOOST_TEST(*num == neg_small); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 8)); \
    } \
    { \
      str_type const str{str_prefix##"-236431836807206106575376foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str); \
      if(num) { \
        BOOST_TEST(*num == neg_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 25)); \
    } \
    { \
      str_type const str{str_prefix##"-80453585044221152298169170068268828533" \
        "249754330668494132359696foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str); \
      if(num) { \
        BOOST_TEST(*num == neg_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 63)); \
    }

  BIGINT_FROM_STRING_TEST(std::string, )
  BIGINT_FROM_STRING_TEST(std::u8string, u8)
  BIGINT_FROM_STRING_TEST(std::wstring, L)
  BIGINT_FROM_STRING_TEST(std::u16string, u)
  BIGINT_FROM_STRING_TEST(std::u32string, U)

  #undef BIGINT_FROM_STRING_TEST
}

BOOST_AUTO_TEST_CASE(base7) {
  #define BIGINT_FROM_STRING_TEST(str_type, str_prefix) \
    { \
      str_type const str{str_prefix##"0789"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 7); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 1)); \
    } \
    { \
      str_type const str{str_prefix##"0052236789"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 7); \
      if(num) { \
        BOOST_TEST(*num == pos_small); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 7)); \
    } \
    { \
      str_type const str{str_prefix##"+3412051606452316212413020530789"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 7); \
      if(num) { \
        BOOST_TEST(*num == pos_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 29)); \
    } \
    { \
      str_type const str{str_prefix##"14304316024101642462620405155045615036" \
        "446554461033431135065121140162246346789"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 7); \
      if(num) { \
        BOOST_TEST(*num == pos_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 74)); \
    } \
    { \
      str_type const str{str_prefix##"-00foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 7); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 3)); \
    } \
    { \
      str_type const str{str_prefix##"-0052236789"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 7); \
      if(num) { \
        BOOST_TEST(*num == neg_small); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 8)); \
    } \
    { \
      str_type const str{str_prefix##"-3412051606452316212413020530789"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 7); \
      if(num) { \
        BOOST_TEST(*num == neg_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 29)); \
    } \
    { \
      str_type const str{str_prefix##"-14304316024101642462620405155045615036" \
        "446554461033431135065121140162246346789"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 7); \
      if(num) { \
        BOOST_TEST(*num == neg_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 75)); \
    }

  BIGINT_FROM_STRING_TEST(std::string, )
  BIGINT_FROM_STRING_TEST(std::u8string, u8)
  BIGINT_FROM_STRING_TEST(std::wstring, L)
  BIGINT_FROM_STRING_TEST(std::u16string, u)
  BIGINT_FROM_STRING_TEST(std::u32string, U)

  #undef BIGINT_FROM_STRING_TEST
}

BOOST_AUTO_TEST_CASE(octal) {
  #define BIGINT_FROM_STRING_TEST(str_type, str_prefix) \
    { \
      str_type const str{str_prefix##"089"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 8); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 1)); \
    } \
    { \
      str_type const str{str_prefix##"003102089"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 8); \
      if(num) { \
        BOOST_TEST(*num == pos_small); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 7)); \
    } \
    { \
      str_type const str{str_prefix##"+6204177334565141662503102089"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 8); \
      if(num) { \
        BOOST_TEST(*num == pos_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 27)); \
    } \
    { \
      str_type const str{str_prefix##"31020775562724607312414410376671352303" \
        "545206204177334565141662503102089"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 8); \
      if(num) { \
        BOOST_TEST(*num == pos_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 69)); \
    } \
    { \
      str_type const str{str_prefix##"-00foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 8); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 3)); \
    } \
    { \
      str_type const str{str_prefix##"-003102089"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 8); \
      if(num) { \
        BOOST_TEST(*num == neg_small); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 8)); \
    } \
    { \
      str_type const str{str_prefix##"-6204177334565141662503102089"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 8); \
      if(num) { \
        BOOST_TEST(*num == neg_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 27)); \
    } \
    { \
      str_type const str{str_prefix##"-31020775562724607312414410376671352303" \
        "545206204177334565141662503102089"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 8); \
      if(num) { \
        BOOST_TEST(*num == neg_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 70)); \
    }

  BIGINT_FROM_STRING_TEST(std::string, )
  BIGINT_FROM_STRING_TEST(std::u8string, u8)
  BIGINT_FROM_STRING_TEST(std::wstring, L)
  BIGINT_FROM_STRING_TEST(std::u16string, u)
  BIGINT_FROM_STRING_TEST(std::u32string, U)

  #undef BIGINT_FROM_STRING_TEST
}

BOOST_AUTO_TEST_CASE(base21) {
  #define BIGINT_FROM_STRING_TEST(str_type, str_prefix) \
    { \
      str_type const str{str_prefix##"0xyz"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 21); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 1)); \
    } \
    { \
      str_type const str{str_prefix##"001816xyz"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 21); \
      if(num) { \
        BOOST_TEST(*num == pos_small); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 6)); \
    } \
    { \
      str_type const str{str_prefix##"+7i5eCgeAG3g9eFFhF0xyz"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 21); \
      if(num) { \
        BOOST_TEST(*num == pos_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 19)); \
    } \
    { \
      str_type const str{ \
        str_prefix##"c2a7fEJCe1A06gJGb64biAj83Jd1D1k53c31Fj6498ibDd6xyz"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 21); \
      if(num) { \
        BOOST_TEST(*num == pos_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 47)); \
    } \
    { \
      str_type const str{str_prefix##"-00zoo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 21); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 3)); \
    } \
    { \
      str_type const str{str_prefix##"-001816xyz"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 21); \
      if(num) { \
        BOOST_TEST(*num == neg_small); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 7)); \
    } \
    { \
      str_type const str{str_prefix##"-7i5eCgeAG3g9eFFhF0xyz"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 21); \
      if(num) { \
        BOOST_TEST(*num == neg_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 19)); \
    } \
    { \
      str_type const str{ \
        str_prefix##"-c2a7fEJCe1A06gJGb64biAj83Jd1D1k53c31Fj6498ibDd6xyz"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 21); \
      if(num) { \
        BOOST_TEST(*num == neg_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 48)); \
    }

  BIGINT_FROM_STRING_TEST(std::string, )
  BIGINT_FROM_STRING_TEST(std::u8string, u8)
  BIGINT_FROM_STRING_TEST(std::wstring, L)
  BIGINT_FROM_STRING_TEST(std::u16string, u)
  BIGINT_FROM_STRING_TEST(std::u32string, U)

  #undef BIGINT_FROM_STRING_TEST
}

BOOST_AUTO_TEST_CASE(base_detection) {
  #define BIGINT_FROM_STRING_TEST(str_type, str_prefix) \
    { \
      str_type const str{str_prefix##"+236431836807206106575376foo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 0); \
      if(num) { \
        BOOST_TEST(*num == pos_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 25)); \
    } \
    { \
      str_type const str{str_prefix##"+06204177334565141662503102089"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 0); \
      if(num) { \
        BOOST_TEST(*num == pos_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 28)); \
    } \
    { \
      str_type const str{str_prefix##"+0x003210fEdCbA9876543210xyz"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 0); \
      if(num) { \
        BOOST_TEST(*num == pos_medium); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 25)); \
    } \
    { \
      str_type const str{str_prefix##"-0xzoo"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 0); \
      if(num) { \
        BOOST_TEST(*num == 0x0_big); \
      } else { \
        BOOST_ERROR("num does not have a value"); \
      } \
      BOOST_TEST((it == str.cbegin() + 2)); \
    }

  BIGINT_FROM_STRING_TEST(std::string, )
  BIGINT_FROM_STRING_TEST(std::u8string, u8)
  BIGINT_FROM_STRING_TEST(std::wstring, L)
  BIGINT_FROM_STRING_TEST(std::u16string, u)
  BIGINT_FROM_STRING_TEST(std::u32string, U)

  #undef BIGINT_FROM_STRING_TEST
}

BOOST_AUTO_TEST_CASE(failtail) {
  #define BIGINT_FROM_STRING_TEST(str_type, str_prefix) \
    { \
      str_type const str{str_prefix##"this is not a number"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 0); \
      BOOST_TEST(!num); \
      BOOST_TEST((it == str.cbegin())); \
    } \
    { \
      str_type const str{str_prefix##"+this is not a number"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 0); \
      BOOST_TEST(!num); \
      BOOST_TEST((it == str.cbegin())); \
    } \
    { \
      str_type const str{str_prefix##"-this is not a number"}; \
      auto const [num, it] = util::bigint::maybe_from_string(str, 0); \
      BOOST_TEST(!num); \
      BOOST_TEST((it == str.cbegin())); \
    }

  BIGINT_FROM_STRING_TEST(std::string, )
  BIGINT_FROM_STRING_TEST(std::u8string, u8)
  BIGINT_FROM_STRING_TEST(std::wstring, L)
  BIGINT_FROM_STRING_TEST(std::u16string, u)
  BIGINT_FROM_STRING_TEST(std::u32string, U)

  #undef BIGINT_FROM_STRING_TEST
}

BOOST_AUTO_TEST_CASE(literal_dec) {
  BOOST_TEST(0_big == util::bigint{});
  BOOST_TEST(12816_big == pos_small);
  BOOST_TEST(236431836807206106575376_big == pos_medium);
  BOOST_TEST(80453585044221152298169170068268828533249754330668494132359696_big
    == pos_big);

  BOOST_TEST(236'431'836'807'206'106'575'376_big == pos_medium);
}

BOOST_AUTO_TEST_CASE(literal_oct) {
  BOOST_TEST(00_big == util::bigint{});
  BOOST_TEST(031020_big == pos_small);
  BOOST_TEST(062041773345651416625031020_big == pos_medium);
  BOOST_TEST(
    0310207755627246073124144103766713523035452062041773345651416625031020_big
    == pos_big);

  BOOST_TEST(062'04177334'56514166'25031020_big == pos_medium);
}

BOOST_AUTO_TEST_CASE(literal_hex) {
  BOOST_TEST(0x0_big == util::bigint{});

  {
    util::bigint const bignum{0xbAdC0dE_big};
    std::uint64_t const smallnum{bignum};
    BOOST_TEST(smallnum == 0xBADC0DE);
  }

  {
    util::bigint const bignum{0xBa'd'c0D'e_big};
    std::uint64_t const smallnum{bignum};
    BOOST_TEST(smallnum == 0xBADC0DE);
  }
}

BOOST_AUTO_TEST_CASE(stream_input) {
  {
    util::bigint num;
    std::istringstream stream{"  000  foo"};
    stream >> num;
    BOOST_TEST(num == 0x0_big);
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "  foo");
  }
  {
    util::bigint num;
    std::istringstream stream{"  -0012816  foo"};
    stream >> num;
    BOOST_TEST(num == neg_small);
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "  foo");
  }
  {
    util::bigint num;
    std::istringstream stream{
      "  +80453585044221152298169170068268828533249754330668494132359696  foo"};
    stream >> num;
    BOOST_TEST(num == pos_big);
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "  foo");
  }
  {
    util::bigint num;
    std::istringstream stream{"  -00062041773345651416625031020  foo"};
    stream >> std::oct >> num;
    BOOST_TEST(num == neg_medium);
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "  foo");
  }
  {
    util::bigint num;
    std::istringstream stream{"  31020  foo"};
    stream >> std::oct >> num;
    BOOST_TEST(num == pos_small);
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "  foo");
  }
  {
    util::bigint num;
    std::istringstream stream{"  +3210  foo"};
    stream >> std::hex >> num;
    BOOST_TEST(num == pos_small);
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "  foo");
  }
  {
    util::bigint num;
    std::istringstream stream{"  -0x3210fEdCbA9876543210  foo"};
    stream >> std::hex >> num;
    BOOST_TEST(num == neg_medium);
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "  foo");
  }
  {
    util::bigint num;
    std::istringstream stream{
      "  0X3210fEdCbA9876543210fEdCbA9876543210fEdCbA9876543210  foo"};
    stream >> std::hex >> num;
    BOOST_TEST(num == pos_big);
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "  foo");
  }
  {
    util::bigint num;
    std::istringstream stream{"  0x???"};
    stream >> std::hex >> num;
    BOOST_TEST(stream.rdstate() == std::ios_base::failbit);
    stream.clear();
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "???");
  }
  {
    util::bigint num;
    std::istringstream stream{"  -0X???"};
    stream >> std::hex >> num;
    BOOST_TEST(stream.rdstate() == std::ios_base::failbit);
    stream.clear();
    std::string str;
    std::getline(stream, str);
    BOOST_TEST(str == "???");
  }

  {
    util::bigint num;
    std::wistringstream stream{L"  000  foo"};
    stream >> num;
    BOOST_TEST(num == 0x0_big);
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"  foo"));
  }
  {
    util::bigint num;
    std::wistringstream stream{L"  -0012816  foo"};
    stream >> num;
    BOOST_TEST(num == neg_small);
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"  foo"));
  }
  {
    util::bigint num;
    std::wistringstream stream{
      L"  +80453585044221152298169170068268828533249754330668494132359696 foo"};
    stream >> num;
    BOOST_TEST(num == pos_big);
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L" foo"));
  }
  {
    util::bigint num;
    std::wistringstream stream{L"  -00062041773345651416625031020  foo"};
    stream >> std::oct >> num;
    BOOST_TEST(num == neg_medium);
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"  foo"));
  }
  {
    util::bigint num;
    std::wistringstream stream{L"  31020  foo"};
    stream >> std::oct >> num;
    BOOST_TEST(num == pos_small);
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"  foo"));
  }
  {
    util::bigint num;
    std::wistringstream stream{L"  +3210  foo"};
    stream >> std::hex >> num;
    BOOST_TEST(num == pos_small);
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"  foo"));
  }
  {
    util::bigint num;
    std::wistringstream stream{L"  -0x3210fEdCbA9876543210  foo"};
    stream >> std::hex >> num;
    BOOST_TEST(num == neg_medium);
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"  foo"));
  }
  {
    util::bigint num;
    std::wistringstream stream{
      L"  0X3210fEdCbA9876543210fEdCbA9876543210fEdCbA9876543210  foo"};
    stream >> std::hex >> num;
    BOOST_TEST(num == pos_big);
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"  foo"));
  }
  {
    util::bigint num;
    std::wistringstream stream{L"  0x???"};
    stream >> std::hex >> num;
    BOOST_TEST(stream.rdstate() == std::ios_base::failbit);
    stream.clear();
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"???"));
  }
  {
    util::bigint num;
    std::wistringstream stream{L"  -0X???"};
    stream >> std::hex >> num;
    BOOST_TEST(stream.rdstate() == std::ios_base::failbit);
    stream.clear();
    std::wstring str;
    std::getline(stream, str);
    BOOST_TEST((str == L"???"));
  }
}

BOOST_AUTO_TEST_CASE(bytes) {
  {
    std::vector<std::uint8_t> const input{};
    util::bigint const output{util::bigint::from_bytes(input)};
    BOOST_TEST(output == 0_big);
  }
  {
    std::vector<std::uint8_t> const input{0x00, 0x00, 0x00, 0x00};
    util::bigint const output{util::bigint::from_bytes(input)};
    BOOST_TEST(output == 0_big);
  }
  {
    std::vector<std::uint8_t> const input{0x10, 0x32, 0x00, 0x00};
    util::bigint const output{util::bigint::from_bytes(input)};
    BOOST_TEST(output == pos_small);
  }
  {
    std::vector<std::uint8_t> const input{
      0x10, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE, 0x10, 0x32};
    util::bigint const output{util::bigint::from_bytes(input)};
    BOOST_TEST(output == pos_medium);
  }
  {
    std::vector<std::uint8_t> const input{
      0x10, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE,
      0x10, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE,
      0x10, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE, 0x10, 0x32};
    util::bigint const output{util::bigint::from_bytes(input)};
    BOOST_TEST(output == pos_big);
  }
  {
    std::vector<std::uint8_t> const input{
      0x10, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE,
      0x10, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE,
      0x10, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE};
    util::bigint const output{util::bigint::from_bytes(input)};
    BOOST_TEST(output
      == 0xFEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543210_big);
  }
}

BOOST_AUTO_TEST_SUITE_END() // from_string

// Bigint to string conversion /////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(to_string)

BOOST_AUTO_TEST_CASE(decimal) {
  BOOST_TEST((0x0_big).to_string() == "0");
  BOOST_TEST(pos_small.to_string() == "12816");
  BOOST_TEST(pos_medium.to_string() == "236431836807206106575376");
  BOOST_TEST(pos_big.to_string()
    == "80453585044221152298169170068268828533249754330668494132359696");
  BOOST_TEST(neg_small.to_string() == "-12816");
  BOOST_TEST(neg_medium.to_string() == "-236431836807206106575376");
  BOOST_TEST(neg_big.to_string()
    == "-80453585044221152298169170068268828533249754330668494132359696");

  BOOST_TEST(((0x0_big).to_wstring() == L"0"));
  BOOST_TEST((pos_small.to_wstring() == L"12816"));
  BOOST_TEST((pos_medium.to_wstring() == L"236431836807206106575376"));
  BOOST_TEST((pos_big.to_wstring()
    == L"80453585044221152298169170068268828533249754330668494132359696"));
  BOOST_TEST((neg_small.to_wstring() == L"-12816"));
  BOOST_TEST((neg_medium.to_wstring() == L"-236431836807206106575376"));
  BOOST_TEST((neg_big.to_wstring()
    == L"-80453585044221152298169170068268828533249754330668494132359696"));

  BOOST_TEST(((0x0_big).to_u8string() == u8"0"));
  BOOST_TEST((pos_small.to_u8string() == u8"12816"));
  BOOST_TEST((pos_medium.to_u8string() == u8"236431836807206106575376"));
  BOOST_TEST((pos_big.to_u8string()
    == u8"80453585044221152298169170068268828533249754330668494132359696"));
  BOOST_TEST((neg_small.to_u8string() == u8"-12816"));
  BOOST_TEST((neg_medium.to_u8string() == u8"-236431836807206106575376"));
  BOOST_TEST((neg_big.to_u8string()
    == u8"-80453585044221152298169170068268828533249754330668494132359696"));

  BOOST_TEST(((0x0_big).to_u16string() == u"0"));
  BOOST_TEST((pos_small.to_u16string() == u"12816"));
  BOOST_TEST((pos_medium.to_u16string() == u"236431836807206106575376"));
  BOOST_TEST((pos_big.to_u16string()
    == u"80453585044221152298169170068268828533249754330668494132359696"));
  BOOST_TEST((neg_small.to_u16string() == u"-12816"));
  BOOST_TEST((neg_medium.to_u16string() == u"-236431836807206106575376"));
  BOOST_TEST((neg_big.to_u16string()
    == u"-80453585044221152298169170068268828533249754330668494132359696"));

  BOOST_TEST(((0x0_big).to_u32string() == U"0"));
  BOOST_TEST((pos_small.to_u32string() == U"12816"));
  BOOST_TEST((pos_medium.to_u32string() == U"236431836807206106575376"));
  BOOST_TEST((pos_big.to_u32string()
    == U"80453585044221152298169170068268828533249754330668494132359696"));
  BOOST_TEST((neg_small.to_u32string() == U"-12816"));
  BOOST_TEST((neg_medium.to_u32string() == U"-236431836807206106575376"));
  BOOST_TEST((neg_big.to_u32string()
    == U"-80453585044221152298169170068268828533249754330668494132359696"));
}

BOOST_AUTO_TEST_CASE(base21) {
  BOOST_TEST((0x0_big).to_string(21) == "0");
  BOOST_TEST(pos_small.to_string(21) == "1816");
  BOOST_TEST(pos_medium.to_string(21) == "7I5ECGEAG3G9EFFHF0");
  BOOST_TEST(pos_big.to_string(21)
    == "C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6");
  BOOST_TEST(neg_small.to_string(21) == "-1816");
  BOOST_TEST(neg_medium.to_string(21) == "-7I5ECGEAG3G9EFFHF0");
  BOOST_TEST(neg_big.to_string(21)
    == "-C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6");

  BOOST_TEST(((0x0_big).to_wstring(21) == L"0"));
  BOOST_TEST((pos_small.to_wstring(21) == L"1816"));
  BOOST_TEST((pos_medium.to_wstring(21) == L"7I5ECGEAG3G9EFFHF0"));
  BOOST_TEST((pos_big.to_wstring(21)
    == L"C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6"));
  BOOST_TEST((neg_small.to_wstring(21) == L"-1816"));
  BOOST_TEST((neg_medium.to_wstring(21) == L"-7I5ECGEAG3G9EFFHF0"));
  BOOST_TEST((neg_big.to_wstring(21)
    == L"-C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6"));

  BOOST_TEST(((0x0_big).to_u8string(21) == u8"0"));
  BOOST_TEST((pos_small.to_u8string(21) == u8"1816"));
  BOOST_TEST((pos_medium.to_u8string(21) == u8"7I5ECGEAG3G9EFFHF0"));
  BOOST_TEST((pos_big.to_u8string(21)
    == u8"C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6"));
  BOOST_TEST((neg_small.to_u8string(21) == u8"-1816"));
  BOOST_TEST((neg_medium.to_u8string(21) == u8"-7I5ECGEAG3G9EFFHF0"));
  BOOST_TEST((neg_big.to_u8string(21)
    == u8"-C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6"));

  BOOST_TEST(((0x0_big).to_u16string(21) == u"0"));
  BOOST_TEST((pos_small.to_u16string(21) == u"1816"));
  BOOST_TEST((pos_medium.to_u16string(21) == u"7I5ECGEAG3G9EFFHF0"));
  BOOST_TEST((pos_big.to_u16string(21)
    == u"C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6"));
  BOOST_TEST((neg_small.to_u16string(21) == u"-1816"));
  BOOST_TEST((neg_medium.to_u16string(21) == u"-7I5ECGEAG3G9EFFHF0"));
  BOOST_TEST((neg_big.to_u16string(21)
    == u"-C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6"));

  BOOST_TEST(((0x0_big).to_u32string(21) == U"0"));
  BOOST_TEST((pos_small.to_u32string(21) == U"1816"));
  BOOST_TEST((pos_medium.to_u32string(21) == U"7I5ECGEAG3G9EFFHF0"));
  BOOST_TEST((pos_big.to_u32string(21)
    == U"C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6"));
  BOOST_TEST((neg_small.to_u32string(21) == U"-1816"));
  BOOST_TEST((neg_medium.to_u32string(21) == U"-7I5ECGEAG3G9EFFHF0"));
  BOOST_TEST((neg_big.to_u32string(21)
    == U"-C2A7FEJCE1A06GJGB64BIAJ83JD1D1K53C31FJ6498IBDD6"));
}

BOOST_AUTO_TEST_CASE(bigbase) {
  auto const digits{[](util::bigint const& num) {
    std::string result{"["};
    result += num.to_string(16);
    result += ']';
    return result;
  }};

  util::bigint const base{0x1'0000000000000001_big};

  BOOST_TEST((0x0_big).to_string(base, digits, "-"sv) == "[0]");
  BOOST_TEST(pos_small.to_string(base, digits, "-"sv) == "[3210]");
  BOOST_TEST(pos_medium.to_string(base, digits, "-"sv)
    == "[3210][FEDCBA9876540000]");
  BOOST_TEST(pos_big.to_string(base, digits, "-"sv)
    == "[3210][FEDCBA9876539BDF][123456789AC6421][FEDCBA9876540000]");
  BOOST_TEST(neg_small.to_string(base, digits, "-"sv) == "-[3210]");
  BOOST_TEST(neg_medium.to_string(base, digits, "-"sv)
    == "-[3210][FEDCBA9876540000]");
  BOOST_TEST(neg_big.to_string(base, digits, "-"sv)
    == "-[3210][FEDCBA9876539BDF][123456789AC6421][FEDCBA9876540000]");
}

BOOST_AUTO_TEST_CASE(custom_digits_base3) {
  auto digits{[](util::bigint const& x) noexcept {
    assert(0 <= x && x < 3);
    if(x == 0) {
      return "v"sv;
    } else if(x == 1) {
      return "--"sv;
    } else if(x == 2) {
      return "^^^"sv;
    } else {
      util::unreachable();
    }
  }};

  BOOST_TEST((0x0_big).to_string(3, digits, "@@"sv)
    == "v");
  BOOST_TEST(pos_small.to_string(3, digits, "@@"sv)
    == "--^^^^^^--^^^v^^^vv");
  BOOST_TEST(pos_medium.to_string(3, digits, "@@"sv)
    == "^^^^^^^^^^^^vvv^^^--v--v^^^--v^^^^^^--v----^^^----------vv--^^^^^^----^"
    "^^^^^--vv----vv--^^^----vv");
  BOOST_TEST(pos_big.to_string(3, digits, "@@"sv)
    == "^^^v^^^----v^^^v----^^^--v----^^^^^^vv^^^^^^^^^v----^^^^^^v^^^v^^^^^^v-"
    "-v--^^^^^^^^^^^^^^^vv^^^^^^----vvv--^^^^^^v^^^^^^--vvv^^^--^^^--v^^^^^^v^^"
    "^^^^--------v^^^v^^^----vvv^^^^^^v^^^vv----^^^^^^v----vvv--v^^^^^^v^^^--^^"
    "^^^^v--^^^----^^^^^^--^^^--^^^^^^^^^^^^v----^^^^^^^^^vv");
  BOOST_TEST(neg_small.to_string(3, digits, "@@"sv)
    == "@@--^^^^^^--^^^v^^^vv");
  BOOST_TEST(neg_medium.to_string(3, digits, "@@"sv)
    == "@@^^^^^^^^^^^^vvv^^^--v--v^^^--v^^^^^^--v----^^^----------vv--^^^^^^---"
    "-^^^^^^--vv----vv--^^^----vv");
  BOOST_TEST(neg_big.to_string(3, digits, "@@"sv)
    == "@@^^^v^^^----v^^^v----^^^--v----^^^^^^vv^^^^^^^^^v----^^^^^^v^^^v^^^^^^"
    "v--v--^^^^^^^^^^^^^^^vv^^^^^^----vvv--^^^^^^v^^^^^^--vvv^^^--^^^--v^^^^^^v"
    "^^^^^^--------v^^^v^^^----vvv^^^^^^v^^^vv----^^^^^^v----vvv--v^^^^^^v^^^--"
    "^^^^^^v--^^^----^^^^^^--^^^--^^^^^^^^^^^^v----^^^^^^^^^vv");
}

BOOST_AUTO_TEST_CASE(custom_digits_base4) {
  auto digits{[](util::bigint const& x) noexcept {
    assert(0 <= x && x < 4);
    if(x == 0) {
      return "v"sv;
    } else if(x == 1) {
      return "--"sv;
    } else if(x == 2) {
      return "^^^"sv;
    } else if(x == 3) {
      return "?"sv;
    } else {
      util::unreachable();
    }
  }};

  BOOST_TEST((0x0_big).to_string(4, digits, "@@"sv)
    == "v");
  BOOST_TEST(pos_small.to_string(4, digits, "@@"sv)
    == "?v^^^v--vv");
  BOOST_TEST(pos_medium.to_string(4, digits, "@@"sv)
    == "?v^^^v--vv???^^^?--?v^^^?^^^^^^^^^--^^^v--?--^^^------vv?v^^^v--vv");
  BOOST_TEST(pos_big.to_string(4, digits, "@@"sv)
    == "?v^^^v--vv???^^^?--?v^^^?^^^^^^^^^--^^^v--?--^^^------vv?v^^^v--vv???^^"
    "^?--?v^^^?^^^^^^^^^--^^^v--?--^^^------vv?v^^^v--vv???^^^?--?v^^^?^^^^^^^^"
    "^--^^^v--?--^^^------vv?v^^^v--vv");
  BOOST_TEST(neg_small.to_string(4, digits, "@@"sv)
    == "@@?v^^^v--vv");
  BOOST_TEST(neg_medium.to_string(4, digits, "@@"sv)
    == "@@?v^^^v--vv???^^^?--?v^^^?^^^^^^^^^--^^^v--?--^^^------vv?v^^^v--vv");
  BOOST_TEST(neg_big.to_string(4, digits, "@@"sv)
    == "@@?v^^^v--vv???^^^?--?v^^^?^^^^^^^^^--^^^v--?--^^^------vv?v^^^v--vv???"
    "^^^?--?v^^^?^^^^^^^^^--^^^v--?--^^^------vv?v^^^v--vv???^^^?--?v^^^?^^^^^^"
    "^^^--^^^v--?--^^^------vv?v^^^v--vv");
}

BOOST_AUTO_TEST_CASE(stream_output) {
  {
    std::ostringstream s;
    s << 0x0_big;
    BOOST_TEST(s.str() == "0");
  }
  {
    std::ostringstream s;
    s << pos_small;
    BOOST_TEST(s.str() == "12816");
  }
  {
    std::ostringstream s;
    s << pos_medium;
    BOOST_TEST(s.str() == "236431836807206106575376");
  }
  {
    std::ostringstream s;
    s << pos_big;
    BOOST_TEST(s.str()
      == "80453585044221152298169170068268828533249754330668494132359696");
  }
  {
    std::ostringstream s;
    s << neg_small;
    BOOST_TEST(s.str() == "-12816");
  }
  {
    std::ostringstream s;
    s << neg_medium;
    BOOST_TEST(s.str() == "-236431836807206106575376");
  }
  {
    std::ostringstream s;
    s << neg_big;
    BOOST_TEST(s.str()
      == "-80453585044221152298169170068268828533249754330668494132359696");
  }

  {
    std::wostringstream s;
    s << 0x0_big;
    BOOST_TEST((s.str() == L"0"));
  }
  {
    std::wostringstream s;
    s << pos_small;
    BOOST_TEST((s.str() == L"12816"));
  }
  {
    std::wostringstream s;
    s << pos_medium;
    BOOST_TEST((s.str() == L"236431836807206106575376"));
  }
  {
    std::wostringstream s;
    s << pos_big;
    BOOST_TEST((s.str()
      == L"80453585044221152298169170068268828533249754330668494132359696"));
  }
  {
    std::wostringstream s;
    s << neg_small;
    BOOST_TEST((s.str() == L"-12816"));
  }
  {
    std::wostringstream s;
    s << neg_medium;
    BOOST_TEST((s.str() == L"-236431836807206106575376"));
  }
  {
    std::wostringstream s;
    s << neg_big;
    BOOST_TEST((s.str()
      == L"-80453585044221152298169170068268828533249754330668494132359696"));
  }
}

BOOST_AUTO_TEST_CASE(stream_output_fancy1) {
  {
    std::ostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill('*') << 0x0_big;
    BOOST_TEST(s.str() == "+0********");
  }
  {
    std::ostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill('*') << pos_small;
    BOOST_TEST(s.str() == "+031020***");
  }
  {
    std::ostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill('*') << pos_medium;
    BOOST_TEST(s.str() == "+062041773345651416625031020");
  }
  {
    std::ostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill('*') << pos_big;
    BOOST_TEST(s.str() == "+031020775562724607312414410376671352303545206204177"
      "3345651416625031020");
  }
  {
    std::ostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill('*') << neg_small;
    BOOST_TEST(s.str() == "-031020***");
  }
  {
    std::ostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill('*') << neg_medium;
    BOOST_TEST(s.str() == "-062041773345651416625031020");
  }
  {
    std::ostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill('*') << neg_big;
    BOOST_TEST(s.str() == "-031020775562724607312414410376671352303545206204177"
      "3345651416625031020");
  }

  {
    std::wostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill(L'*') << 0x0_big;
    BOOST_TEST((s.str() == L"+0********"));
  }
  {
    std::wostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill(L'*') << pos_small;
    BOOST_TEST((s.str() == L"+031020***"));
  }
  {
    std::wostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill(L'*') << pos_medium;
    BOOST_TEST((s.str() == L"+062041773345651416625031020"));
  }
  {
    std::wostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill(L'*') << pos_big;
    BOOST_TEST((s.str() == L"+0310207755627246073124144103766713523035452062041"
      "773345651416625031020"));
  }
  {
    std::wostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill(L'*') << neg_small;
    BOOST_TEST((s.str() == L"-031020***"));
  }
  {
    std::wostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill(L'*') << neg_medium;
    BOOST_TEST((s.str() == L"-062041773345651416625031020"));
  }
  {
    std::wostringstream s;
    s << std::left << std::showbase << std::showpos << std::setw(10) << std::oct
      << std::setfill(L'*') << neg_big;
    BOOST_TEST((s.str() == L"-0310207755627246073124144103766713523035452062041"
      "773345651416625031020"));
  }
}

BOOST_AUTO_TEST_CASE(stream_output_fancy2) {
  {
    std::ostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill('*') << 0x0_big;
    BOOST_TEST(s.str() == "+0x******0");
  }
  {
    std::ostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill('*') << pos_small;
    BOOST_TEST(s.str() == "+0x***3210");
  }
  {
    std::ostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill('*') << pos_medium;
    BOOST_TEST(s.str() == "+0x3210fedcba9876543210");
  }
  {
    std::ostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill('*') << pos_big;
    BOOST_TEST(s.str()
      == "+0x3210fedcba9876543210fedcba9876543210fedcba9876543210");
  }
  {
    std::ostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill('*') << neg_small;
    BOOST_TEST(s.str() == "-0x***3210");
  }
  {
    std::ostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill('*') << neg_medium;
    BOOST_TEST(s.str() == "-0x3210fedcba9876543210");
  }
  {
    std::ostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill('*') << neg_big;
    BOOST_TEST(s.str()
      == "-0x3210fedcba9876543210fedcba9876543210fedcba9876543210");
  }

  {
    std::wostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill(L'*') << 0x0_big;
    BOOST_TEST((s.str() == L"+0x******0"));
  }
  {
    std::wostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill(L'*') << pos_small;
    BOOST_TEST((s.str() == L"+0x***3210"));
  }
  {
    std::wostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill(L'*') << pos_medium;
    BOOST_TEST((s.str() == L"+0x3210fedcba9876543210"));
  }
  {
    std::wostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill(L'*') << pos_big;
    BOOST_TEST((s.str()
      == L"+0x3210fedcba9876543210fedcba9876543210fedcba9876543210"));
  }
  {
    std::wostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill(L'*') << neg_small;
    BOOST_TEST((s.str() == L"-0x***3210"));
  }
  {
    std::wostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill(L'*') << neg_medium;
    BOOST_TEST((s.str() == L"-0x3210fedcba9876543210"));
  }
  {
    std::wostringstream s;
    s << std::internal << std::showbase << std::showpos << std::setw(10)
      << std::hex << std::nouppercase << std::setfill(L'*') << neg_big;
    BOOST_TEST((s.str()
      == L"-0x3210fedcba9876543210fedcba9876543210fedcba9876543210"));
  }
}

BOOST_AUTO_TEST_SUITE_END() // to_string

// Additive arithmetic /////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(additive)

BOOST_AUTO_TEST_CASE(negate) {
  {
    util::bigint num{-pos_small};
    num.negate();
    BOOST_TEST(num == pos_small);
  }

  {
    util::bigint num{-pos_medium};
    num.negate();
    BOOST_TEST(num == pos_medium);
  }

  {
    util::bigint num{-pos_big};
    num.negate();
    BOOST_TEST(num == pos_big);
  }

  BOOST_TEST(-util::bigint{0} == util::bigint{0});
}

BOOST_DATA_TEST_CASE(add, add_test_data, a, b, sum) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_sum{-sum};

  BOOST_TEST(a + b == sum);
  BOOST_TEST(b + a == sum);
  BOOST_TEST(minus_a + minus_b == minus_sum);
  BOOST_TEST(minus_b + minus_a == minus_sum);

  BOOST_TEST(sum + minus_a == b);
  BOOST_TEST(minus_a + sum == b);
  BOOST_TEST(minus_sum + a == minus_b);
  BOOST_TEST(a + minus_sum == minus_b);

  BOOST_TEST(sum + minus_b == a);
  BOOST_TEST(minus_b + sum == a);
  BOOST_TEST(minus_sum + b == minus_a);
  BOOST_TEST(b + minus_sum == minus_a);
}

BOOST_AUTO_TEST_CASE(add_self) {
  BOOST_TEST(pos_small + pos_small == 0x6420_big);
  BOOST_TEST(pos_medium + pos_medium == 0x6421'FDB97530ECA86420_big);
  BOOST_TEST(pos_big + pos_big
    == 0x6421'FDB97530ECA86421'FDB97530ECA86421'FDB97530ECA86420_big);

  BOOST_TEST(neg_small + neg_small == -0x6420_big);
  BOOST_TEST(neg_medium + neg_medium == -0x6421'FDB97530ECA86420_big);
  BOOST_TEST(neg_big + neg_big
    == -0x6421'FDB97530ECA86421'FDB97530ECA86421'FDB97530ECA86420_big);
}

BOOST_DATA_TEST_CASE(add_assign, add_test_data, a, b, sum) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_sum{-sum};

  {
    util::bigint num{a};
    num += b;
    BOOST_TEST(num == sum);
  }
  {
    util::bigint num{b};
    num += a;
    BOOST_TEST(num == sum);
  }
  {
    util::bigint num{minus_a};
    num += minus_b;
    BOOST_TEST(num == minus_sum);
  }
  {
    util::bigint num{minus_b};
    num += minus_a;
    BOOST_TEST(num == minus_sum);
  }

  {
    util::bigint num{sum};
    num += minus_a;
    BOOST_TEST(num == b);
  }
  {
    util::bigint num{minus_a};
    num += sum;
    BOOST_TEST(num == b);
  }
  {
    util::bigint num{minus_sum};
    num += a;
    BOOST_TEST(num == minus_b);
  }
  {
    util::bigint num{a};
    num += minus_sum;
    BOOST_TEST(num == minus_b);
  }

  {
    util::bigint num{sum};
    num += minus_b;
    BOOST_TEST(num == a);
  }
  {
    util::bigint num{minus_b};
    num += sum;
    BOOST_TEST(num == a);
  }
  {
    util::bigint num{minus_sum};
    num += b;
    BOOST_TEST(num == minus_a);
  }
  {
    util::bigint num{b};
    num += minus_sum;
    BOOST_TEST(num == minus_a);
  }
}

BOOST_AUTO_TEST_CASE(add_assign_self) {
  {
    util::bigint num{pos_small};
    num += num;
    BOOST_TEST(num == 0x6420_big);
  }

  {
    util::bigint num{pos_medium};
    num += num;
    BOOST_TEST(num == 0x6421'FDB97530ECA86420_big);
  }

  {
    util::bigint num{pos_big};
    num += num;
    BOOST_TEST(num
      == 0x6421'FDB97530ECA86421'FDB97530ECA86421'FDB97530ECA86420_big);
  }

  {
    util::bigint num{neg_small};
    num += num;
    BOOST_TEST(num == -0x6420_big);
  }

  {
    util::bigint num{neg_medium};
    num += num;
    BOOST_TEST(num == -0x6421'FDB97530ECA86420_big);
  }

  {
    util::bigint num{neg_big};
    num += num;
    BOOST_TEST(num
      == -0x6421'FDB97530ECA86421'FDB97530ECA86421'FDB97530ECA86420_big);
  }
}

BOOST_DATA_TEST_CASE(subtract, add_test_data, a, b, sum) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_sum{-sum};

  BOOST_TEST(sum - a == b);
  BOOST_TEST(sum - b == a);
  BOOST_TEST(minus_sum - minus_a == minus_b);
  BOOST_TEST(minus_sum - minus_b == minus_a);

  BOOST_TEST(b - minus_a == sum);
  BOOST_TEST(a - minus_b == sum);
  BOOST_TEST(minus_b - a == minus_sum);
  BOOST_TEST(minus_a - b == minus_sum);
}

BOOST_AUTO_TEST_CASE(subtract_self) {
  BOOST_TEST(pos_small - pos_small == 0);
  BOOST_TEST(pos_medium - pos_medium == 0);
  BOOST_TEST(pos_big - pos_big == 0);

  BOOST_TEST(neg_small - neg_small == 0);
  BOOST_TEST(neg_medium - neg_medium == 0);
  BOOST_TEST(neg_big - neg_big == 0);
}

BOOST_DATA_TEST_CASE(subtract_assign, add_test_data, a, b, sum) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_sum{-sum};

  {
    util::bigint num{sum};
    num -= a;
    BOOST_TEST(num == b);
  }
  {
    util::bigint num{sum};
    num -= b;
    BOOST_TEST(num == a);
  }
  {
    util::bigint num{minus_sum};
    num -= minus_a;
    BOOST_TEST(num == minus_b);
  }
  {
    util::bigint num{minus_sum};
    num -= minus_b;
    BOOST_TEST(num == minus_a);
  }

  {
    util::bigint num{minus_b};
    num -= a;
    BOOST_TEST(num == minus_sum);
  }
  {
    util::bigint num{minus_a};
    num -= b;
    BOOST_TEST(num == minus_sum);
  }
}

BOOST_AUTO_TEST_CASE(subtract_assign_self) {
  {
    util::bigint num{pos_small};
    num -= num;
    BOOST_TEST(num == 0);
  }

  {
    util::bigint num{pos_medium};
    num -= num;
    BOOST_TEST(num == 0);
  }

  {
    util::bigint num{pos_big};
    num -= num;
    BOOST_TEST(num == 0);
  }

  {
    util::bigint num{neg_small};
    num -= num;
    BOOST_TEST(num == 0);
  }

  {
    util::bigint num{neg_medium};
    num -= num;
    BOOST_TEST(num == 0);
  }

  {
    util::bigint num{neg_big};
    num -= num;
    BOOST_TEST(num == 0);
  }
}

BOOST_AUTO_TEST_CASE(increment) {
  {
    util::bigint num{pos_small};
    BOOST_TEST(++num == 0x3211_big);
    BOOST_TEST(num++ == 0x3211_big);
    BOOST_TEST(num == 0x3212_big);
  }

  {
    util::bigint num{pos_medium};
    BOOST_TEST(++num == 0x3210'FEDCBA9876543211_big);
    BOOST_TEST(num++ == 0x3210'FEDCBA9876543211_big);
    BOOST_TEST(num == 0x3210'FEDCBA9876543212_big);
  }

  {
    util::bigint num{pos_big};
    BOOST_TEST(++num
      == 0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543211_big);
    BOOST_TEST(num++
      == 0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543211_big);
    BOOST_TEST(num
      == 0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA9876543212_big);
  }

  {
    util::bigint num{0x10'FFFFFFFFFFFFFFFF_big};
    BOOST_TEST(++num == 0x11'0000000000000000_big);
    BOOST_TEST(num++ == 0x11'0000000000000000_big);
    BOOST_TEST(num == 0x11'0000000000000001_big);
  }
}

BOOST_AUTO_TEST_CASE(decrement) {
  {
    util::bigint num{pos_small};
    BOOST_TEST(--num == 0x320F_big);
    BOOST_TEST(num-- == 0x320F_big);
    BOOST_TEST(num == 0x320E_big);
  }

  {
    util::bigint num{pos_medium};
    BOOST_TEST(--num == 0x3210'FEDCBA987654320F_big);
    BOOST_TEST(num-- == 0x3210'FEDCBA987654320F_big);
    BOOST_TEST(num == 0x3210'FEDCBA987654320E_big);
  }

  {
    util::bigint num{pos_big};
    BOOST_TEST(--num
      == 0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA987654320F_big);
    BOOST_TEST(num--
      == 0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA987654320F_big);
    BOOST_TEST(num
      == 0x3210'FEDCBA9876543210'FEDCBA9876543210'FEDCBA987654320E_big);
  }

  {
    util::bigint num{0x11'0000000000000000_big};
    BOOST_TEST(--num == 0x10'FFFFFFFFFFFFFFFF_big);
    BOOST_TEST(num-- == 0x10'FFFFFFFFFFFFFFFF_big);
    BOOST_TEST(num == 0x10'FFFFFFFFFFFFFFFE_big);
  }
}

BOOST_AUTO_TEST_SUITE_END() // additive

// Multiplicative operations ///////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(multiplicative)

BOOST_DATA_TEST_CASE(multiply, mul_test_data, a, b, product) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_product{-product};

  BOOST_TEST(a * b == product);
  BOOST_TEST(b * a == product);

  BOOST_TEST(a * minus_b == minus_product);
  BOOST_TEST(minus_b * a == minus_product);

  BOOST_TEST(minus_a * b == minus_product);
  BOOST_TEST(b * minus_a == minus_product);

  BOOST_TEST(minus_a * minus_b == product);
  BOOST_TEST(minus_b * minus_a == product);
}

BOOST_AUTO_TEST_CASE(multiply_self) {
  BOOST_TEST(pos_small * pos_small == 0x9CA4100_big);
  BOOST_TEST(pos_medium * pos_medium
    == 0x9CAA4AF'1235A1DF76F0DCCC'DEEC6CD7A44A4100_big);
  BOOST_TEST(pos_big * pos_big == pos_big_sq);

  BOOST_TEST(neg_small * neg_small == 0x9CA4100_big);
  BOOST_TEST(neg_medium * neg_medium
    == 0x9CAA4AF'1235A1DF76F0DCCC'DEEC6CD7A44A4100_big);
  BOOST_TEST(neg_big * neg_big == pos_big_sq);
}

BOOST_DATA_TEST_CASE(multiply_assign, mul_test_data, a, b, product) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_product{-product};

  {
    util::bigint num{a};
    num *= b;
    BOOST_TEST(num == product);
  }
  {
    util::bigint num{b};
    num *= a;
    BOOST_TEST(num == product);
  }

  {
    util::bigint num{a};
    num *= minus_b;
    BOOST_TEST(num == minus_product);
  }
  {
    util::bigint num{minus_b};
    num *= a;
    BOOST_TEST(num == minus_product);
  }

  {
    util::bigint num{minus_a};
    num *= b;
    BOOST_TEST(num == minus_product);
  }
  {
    util::bigint num{b};
    num *= minus_a;
    BOOST_TEST(num == minus_product);
  }

  {
    util::bigint num{minus_a};
    num *= minus_b;
    BOOST_TEST(num == product);
  }
  {
    util::bigint num{minus_b};
    num *= minus_a;
    BOOST_TEST(num == product);
  }
}

BOOST_AUTO_TEST_CASE(multiply_assign_self) {
  {
    util::bigint num{pos_small};
    num *= num;
    BOOST_TEST(num == 0x9CA4100_big);
  }
  {
    util::bigint num{pos_medium};
    num *= num;
    BOOST_TEST(num == 0x9CAA4AF'1235A1DF76F0DCCC'DEEC6CD7A44A4100_big);
  }
  {
    util::bigint num{pos_big};
    num *= num;
    BOOST_TEST(num == pos_big_sq);
  }

  {
    util::bigint num{neg_small};
    num *= num;
    BOOST_TEST(num == 0x9CA4100_big);
  }
  {
    util::bigint num{neg_medium};
    num *= num;
    BOOST_TEST(num == 0x9CAA4AF'1235A1DF76F0DCCC'DEEC6CD7A44A4100_big);
  }
  {
    util::bigint num{neg_big};
    num *= num;
    BOOST_TEST(num == pos_big_sq);
  }
}

BOOST_DATA_TEST_CASE(divmod_exact, mul_test_data, a, b, product) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_product{-product};

  if(a != 0) {
    BOOST_TEST(product / a == b);
    BOOST_TEST(product % a == 0);

    BOOST_TEST(minus_product / a == minus_b);
    BOOST_TEST(minus_product % a == 0);

    BOOST_TEST(product / minus_a == minus_b);
    BOOST_TEST(product % minus_a == 0);

    BOOST_TEST(minus_product / minus_a == b);
    BOOST_TEST(minus_product % minus_a == 0);
  }
  if(b != 0) {
    BOOST_TEST(product / b == a);
    BOOST_TEST(product % b == 0);

    BOOST_TEST(minus_product / b == minus_a);
    BOOST_TEST(minus_product % b == 0);

    BOOST_TEST(product / minus_b == minus_a);
    BOOST_TEST(product % minus_b == 0);

    BOOST_TEST(minus_product / minus_b == a);
    BOOST_TEST(minus_product % minus_b == 0);
  }
}

BOOST_AUTO_TEST_CASE(divmod_self) {
  BOOST_TEST(pos_small / pos_small == 1);
  BOOST_TEST(pos_medium / pos_medium == 1);
  BOOST_TEST(pos_big / pos_big == 1);

  BOOST_TEST(neg_small / neg_small == 1);
  BOOST_TEST(neg_medium / neg_medium == 1);
  BOOST_TEST(neg_big / neg_big == 1);

  BOOST_TEST(pos_small % pos_small == 0);
  BOOST_TEST(pos_medium % pos_medium == 0);
  BOOST_TEST(pos_big % pos_big == 0);

  BOOST_TEST(neg_small % neg_small == 0);
  BOOST_TEST(neg_medium % neg_medium == 0);
  BOOST_TEST(neg_big % neg_big == 0);
}

BOOST_DATA_TEST_CASE(divmod_assign_exact, mul_test_data, a, b, product) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_product{-product};

  if(a != 0) {
    {
      util::bigint num{product};
      num /= a;
      BOOST_TEST(num == b);
    }
    {
      util::bigint num{product};
      num %= a;
      BOOST_TEST(num == 0);
    }

    {
      util::bigint num{minus_product};
      num /= a;
      BOOST_TEST(num == minus_b);
    }
    {
      util::bigint num{minus_product};
      num %= a;
      BOOST_TEST(num == 0);
    }

    {
      util::bigint num{product};
      num /= minus_a;
      BOOST_TEST(num == minus_b);
    }
    {
      util::bigint num{product};
      num %= minus_a;
      BOOST_TEST(num == 0);
    }

    {
      util::bigint num{minus_product};
      num /= minus_a;
      BOOST_TEST(num == b);
    }
    {
      util::bigint num{minus_product};
      num %= minus_a;
      BOOST_TEST(num == 0);
    }
  }
  if(b != 0) {
    {
      util::bigint num{product};
      num /= b;
      BOOST_TEST(num == a);
    }
    {
      util::bigint num{product};
      num %= b;
      BOOST_TEST(num == 0);
    }

    {
      util::bigint num{minus_product};
      num /= b;
      BOOST_TEST(num == minus_a);
    }
    {
      util::bigint num{minus_product};
      num %= b;
      BOOST_TEST(num == 0);
    }

    {
      util::bigint num{product};
      num /= minus_b;
      BOOST_TEST(num == minus_a);
    }
    {
      util::bigint num{product};
      num %= minus_b;
      BOOST_TEST(num == 0);
    }

    {
      util::bigint num{minus_product};
      num /= minus_b;
      BOOST_TEST(num == a);
    }
    {
      util::bigint num{minus_product};
      num %= minus_b;
      BOOST_TEST(num == 0);
    }
  }
}

BOOST_AUTO_TEST_CASE(divmod_assign_self) {
  {
    util::bigint num{pos_small};
    num /= num;
    BOOST_TEST(num == 1);
  }
  {
    util::bigint num{pos_medium};
    num /= num;
    BOOST_TEST(num == 1);
  }
  {
    util::bigint num{pos_big};
    num /= num;
    BOOST_TEST(num == 1);
  }

  {
    util::bigint num{neg_small};
    num /= num;
    BOOST_TEST(num == 1);
  }
  {
    util::bigint num{neg_medium};
    num /= num;
    BOOST_TEST(num == 1);
  }
  {
    util::bigint num{neg_big};
    num /= num;
    BOOST_TEST(num == 1);
  }

  {
    util::bigint num{pos_small};
    num %= num;
    BOOST_TEST(num == 0);
  }
  {
    util::bigint num{pos_medium};
    num %= num;
    BOOST_TEST(num == 0);
  }
  {
    util::bigint num{pos_big};
    num %= num;
    BOOST_TEST(num == 0);
  }

  {
    util::bigint num{neg_small};
    num %= num;
    BOOST_TEST(num == 0);
  }
  {
    util::bigint num{neg_medium};
    num %= num;
    BOOST_TEST(num == 0);
  }
  {
    util::bigint num{neg_big};
    num %= num;
    BOOST_TEST(num == 0);
  }
}

BOOST_DATA_TEST_CASE(divmod_raw_exact, mul_test_data, a, b, product) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_product{-product};

  if(a != 0) {
    {
      util::bigint num{product};
      num.divide(a, nullptr);
      BOOST_TEST(num == 0);
    }
    {
      util::bigint num{product};
      util::bigint q;
      num.divide(a, &q);
      BOOST_TEST(num == 0);
      BOOST_TEST(q == b);
    }

    {
      util::bigint num{minus_product};
      num.divide(a, nullptr);
      BOOST_TEST(num == 0);
    }
    {
      util::bigint num{minus_product};
      util::bigint q;
      num.divide(a, &q);
      BOOST_TEST(num == 0);
      BOOST_TEST(q == minus_b);
    }

    {
      util::bigint num{product};
      num.divide(minus_a, nullptr);
      BOOST_TEST(num == 0);
    }
    {
      util::bigint num{product};
      util::bigint q;
      num.divide(minus_a, &q);
      BOOST_TEST(num == 0);
      BOOST_TEST(q == minus_b);
    }

    {
      util::bigint num{minus_product};
      num.divide(minus_a, nullptr);
      BOOST_TEST(num == 0);
    }
    {
      util::bigint num{minus_product};
      util::bigint q;
      num.divide(minus_a, &q);
      BOOST_TEST(num == 0);
      BOOST_TEST(q == b);
    }
  }
  if(b != 0) {
    {
      util::bigint num{product};
      num.divide(b, nullptr);
      BOOST_TEST(num == 0);
    }
    {
      util::bigint num{product};
      util::bigint q;
      num.divide(b, &q);
      BOOST_TEST(num == 0);
      BOOST_TEST(q == a);
    }

    {
      util::bigint num{minus_product};
      num.divide(b, nullptr);
      BOOST_TEST(num == 0);
    }
    {
      util::bigint num{minus_product};
      util::bigint q;
      num.divide(b, &q);
      BOOST_TEST(num == 0);
      BOOST_TEST(q == minus_a);
    }

    {
      util::bigint num{product};
      num.divide(minus_b, nullptr);
      BOOST_TEST(num == 0);
    }
    {
      util::bigint num{product};
      util::bigint q;
      num.divide(minus_b, &q);
      BOOST_TEST(num == 0);
      BOOST_TEST(q == minus_a);
    }

    {
      util::bigint num{minus_product};
      num.divide(minus_b, nullptr);
      BOOST_TEST(num == 0);
    }
    {
      util::bigint num{minus_product};
      util::bigint q;
      num.divide(minus_b, &q);
      BOOST_TEST(num == 0);
      BOOST_TEST(q == a);
    }
  }
}

BOOST_DATA_TEST_CASE(divmod, div_test_data, a, b, quotient, remainder) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_quotient{-quotient};
  util::bigint const minus_remainder{-remainder};

  BOOST_TEST(a / b == quotient);
  BOOST_TEST(a % b == remainder);

  BOOST_TEST(a / minus_b == minus_quotient);
  BOOST_TEST(a % minus_b == remainder);

  BOOST_TEST(minus_a / b == minus_quotient);
  BOOST_TEST(minus_a % b == minus_remainder);

  BOOST_TEST(minus_a / minus_b == quotient);
  BOOST_TEST(minus_a % minus_b == minus_remainder);
}

BOOST_DATA_TEST_CASE(divmod_assign, div_test_data, a, b, quotient, remainder) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_quotient{-quotient};
  util::bigint const minus_remainder{-remainder};

  {
    util::bigint num{a};
    num /= b;
    BOOST_TEST(num == quotient);
  }
  {
    util::bigint num{a};
    num %= b;
    BOOST_TEST(num == remainder);
  }

  {
    util::bigint num{a};
    num /= minus_b;
    BOOST_TEST(num == minus_quotient);
  }
  {
    util::bigint num{a};
    num %= minus_b;
    BOOST_TEST(num == remainder);
  }

  {
    util::bigint num{minus_a};
    num /= b;
    BOOST_TEST(num == minus_quotient);
  }
  {
    util::bigint num{minus_a};
    num %= b;
    BOOST_TEST(num == minus_remainder);
  }

  {
    util::bigint num{minus_a};
    num /= minus_b;
    BOOST_TEST(num == quotient);
  }
  {
    util::bigint num{minus_a};
    num %= minus_b;
    BOOST_TEST(num == minus_remainder);
  }
}

BOOST_DATA_TEST_CASE(divmod_raw, div_test_data, a, b, quotient, remainder) {
  util::bigint const minus_a{-a};
  util::bigint const minus_b{-b};
  util::bigint const minus_quotient{-quotient};
  util::bigint const minus_remainder{-remainder};

  {
    util::bigint num{a};
    num.divide(b, nullptr);
    BOOST_TEST(num == remainder);
  }
  {
    util::bigint num{a};
    util::bigint q;
    num.divide(b, &q);
    BOOST_TEST(num == remainder);
    BOOST_TEST(q == quotient);
  }

  {
    util::bigint num{a};
    num.divide(minus_b, nullptr);
    BOOST_TEST(num == remainder);
  }
  {
    util::bigint num{a};
    util::bigint q;
    num.divide(minus_b, &q);
    BOOST_TEST(num == remainder);
    BOOST_TEST(q == minus_quotient);
  }

  {
    util::bigint num{minus_a};
    num.divide(b, nullptr);
    BOOST_TEST(num == minus_remainder);
  }
  {
    util::bigint num{minus_a};
    util::bigint q;
    num.divide(b, &q);
    BOOST_TEST(num == minus_remainder);
    BOOST_TEST(q == minus_quotient);
  }

  {
    util::bigint num{minus_a};
    num.divide(minus_b, nullptr);
    BOOST_TEST(num == minus_remainder);
  }
  {
    util::bigint num{minus_a};
    util::bigint q;
    num.divide(minus_b, &q);
    BOOST_TEST(num == minus_remainder);
    BOOST_TEST(q == quotient);
  }
}

BOOST_DATA_TEST_CASE(divmod_inverse, div_test_data, a, b, quotient, remainder) {
  util::bigint result{quotient};
  result *= b;
  result += remainder;
  BOOST_TEST(result == a);
}

BOOST_AUTO_TEST_SUITE_END() // multiplicative

// Bitwise operations //////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(bitwise)

BOOST_AUTO_TEST_CASE(invert) {
  {
    util::bigint num{~bit_small};
    BOOST_TEST(num.is_negative());
    num.bit_invert();
    BOOST_TEST(!num.is_negative());
    BOOST_TEST(num == bit_small);
  }

  {
    util::bigint num{~bit_medium};
    BOOST_TEST(num.is_negative());
    num.bit_invert();
    BOOST_TEST(!num.is_negative());
    BOOST_TEST(num == bit_medium);
  }

  {
    util::bigint num{~bit_big};
    BOOST_TEST(num.is_negative());
    num.bit_invert();
    BOOST_TEST(!num.is_negative());
    BOOST_TEST(num == bit_big);
  }
}

BOOST_DATA_TEST_CASE(and_copy, bit_test_data, a, b, and_result, or_result,
    not_and_result, not_or_result) {
  util::bigint const not_a{~a};
  util::bigint const not_b{~b};

  BOOST_TEST((a & b) == and_result);
  BOOST_TEST((b & a) == and_result);

  BOOST_TEST((not_a & b) == not_and_result);
  BOOST_TEST((b & not_a) == not_and_result);

  // a & ~b == ~(~a | b)
  BOOST_TEST((a & not_b) == ~not_or_result);
  BOOST_TEST((not_b & a) == ~not_or_result);

  // ~a & ~b == ~(a | b)
  BOOST_TEST((not_a & not_b) == ~or_result);
  BOOST_TEST((not_b & not_a) == ~or_result);
}

BOOST_AUTO_TEST_CASE(and_self) {
  BOOST_TEST((bit_small & bit_small) == bit_small);
  BOOST_TEST((bit_medium & bit_medium) == bit_medium);
  BOOST_TEST((bit_big & bit_big) == bit_big);
}

BOOST_DATA_TEST_CASE(and_assign, bit_test_data, a, b, and_result, or_result,
    not_and_result, not_or_result) {
  util::bigint const not_a{~a};
  util::bigint const not_b{~b};

  {
    util::bigint num{a};
    num &= b;
    BOOST_TEST(num == and_result);
  }
  {
    util::bigint num{b};
    num &= a;
    BOOST_TEST(num == and_result);
  }

  {
    util::bigint num{not_a};
    num &= b;
    BOOST_TEST(num == not_and_result);
  }
  {
    util::bigint num{b};
    num &= not_a;
    BOOST_TEST(num == not_and_result);
  }

  // a & ~b == ~(~a | b)
  {
    util::bigint num{a};
    num &= not_b;
    BOOST_TEST(num == ~not_or_result);
  }
  {
    util::bigint num{not_b};
    num &= a;
    BOOST_TEST(num == ~not_or_result);
  }

  // ~a & ~b == ~(a | b)
  {
    util::bigint num{not_a};
    num &= not_b;
    BOOST_TEST(num == ~or_result);
  }
  {
    util::bigint num{not_b};
    num &= not_a;
    BOOST_TEST(num == ~or_result);
  }
}

BOOST_AUTO_TEST_CASE(and_assign_self) {
  {
    util::bigint num{bit_small};
    num &= num;
    BOOST_TEST(num == bit_small);
  }
  {
    util::bigint num{bit_medium};
    num &= num;
    BOOST_TEST(num == bit_medium);
  }
  {
    util::bigint num{bit_big};
    num &= num;
    BOOST_TEST(num == bit_big);
  }
}

BOOST_DATA_TEST_CASE(or_copy, bit_test_data, a, b, and_result, or_result,
    not_and_result, not_or_result) {
  util::bigint const not_a{~a};
  util::bigint const not_b{~b};

  BOOST_TEST((a | b) == or_result);
  BOOST_TEST((b | a) == or_result);

  BOOST_TEST((not_a | b) == not_or_result);
  BOOST_TEST((b | not_a) == not_or_result);

  // a | ~b == ~(~a & b)
  BOOST_TEST((a | not_b) == ~not_and_result);
  BOOST_TEST((not_b | a) == ~not_and_result);

  // ~a | ~b == ~(a & b)
  BOOST_TEST((not_a | not_b) == ~and_result);
  BOOST_TEST((not_b | not_a) == ~and_result);
}

BOOST_AUTO_TEST_CASE(or_self) {
  BOOST_TEST((bit_small | bit_small) == bit_small);
  BOOST_TEST((bit_medium | bit_medium) == bit_medium);
  BOOST_TEST((bit_big | bit_big) == bit_big);
}

BOOST_DATA_TEST_CASE(or_assign, bit_test_data, a, b, and_result, or_result,
    not_and_result, not_or_result) {
  util::bigint const not_a{~a};
  util::bigint const not_b{~b};

  {
    util::bigint num{a};
    num |= b;
    BOOST_TEST(num == or_result);
  }
  {
    util::bigint num{b};
    num |= a;
    BOOST_TEST(num == or_result);
  }

  {
    util::bigint num{not_a};
    num |= b;
    BOOST_TEST(num == not_or_result);
  }
  {
    util::bigint num{b};
    num |= not_a;
    BOOST_TEST(num == not_or_result);
  }

  // a | ~b == ~(~a & b)
  {
    util::bigint num{a};
    num |= not_b;
    BOOST_TEST(num == ~not_and_result);
  }
  {
    util::bigint num{not_b};
    num |= a;
    BOOST_TEST(num == ~not_and_result);
  }

  // ~a | ~b == ~(a & b)
  {
    util::bigint num{not_a};
    num |= not_b;
    BOOST_TEST(num == ~and_result);
  }
  {
    util::bigint num{not_b};
    num |= not_a;
    BOOST_TEST(num == ~and_result);
  }
}

BOOST_AUTO_TEST_CASE(or_assign_self) {
  {
    util::bigint num{bit_small};
    num |= num;
    BOOST_TEST(num == bit_small);
  }
  {
    util::bigint num{bit_medium};
    num |= num;
    BOOST_TEST(num == bit_medium);
  }
  {
    util::bigint num{bit_big};
    num |= num;
    BOOST_TEST(num == bit_big);
  }
}

BOOST_DATA_TEST_CASE(xor_copy, xor_test_data, a, b, xor_result) {
  util::bigint const not_a{~a};
  util::bigint const not_b{~b};

  BOOST_TEST((a ^ b) == xor_result);
  BOOST_TEST((b ^ a) == xor_result);

  BOOST_TEST((not_a ^ b) == ~xor_result);
  BOOST_TEST((b ^ not_a) == ~xor_result);

  BOOST_TEST((a ^ not_b) == ~xor_result);
  BOOST_TEST((not_b ^ a) == ~xor_result);

  BOOST_TEST((not_a ^ not_b) == xor_result);
  BOOST_TEST((not_b ^ not_a) == xor_result);
}

BOOST_AUTO_TEST_CASE(xor_self) {
  BOOST_TEST((bit_small ^ bit_small) == 0x0_big);
  BOOST_TEST((bit_medium ^ bit_medium) == 0x0_big);
  BOOST_TEST((bit_big ^ bit_big) == 0x0_big);
}

BOOST_DATA_TEST_CASE(xor_assign, xor_test_data, a, b, xor_result) {
  util::bigint const not_a{~a};
  util::bigint const not_b{~b};

  {
    util::bigint num{a};
    num ^= b;
    BOOST_TEST(num == xor_result);
  }
  {
    util::bigint num{b};
    num ^= a;
    BOOST_TEST(num == xor_result);
  }

  {
    util::bigint num{not_a};
    num ^= b;
    BOOST_TEST(num == ~xor_result);
  }
  {
    util::bigint num{b};
    num ^= not_a;
    BOOST_TEST(num == ~xor_result);
  }

  {
    util::bigint num{a};
    num ^= not_b;
    BOOST_TEST(num == ~xor_result);
  }
  {
    util::bigint num{not_b};
    num ^= a;
    BOOST_TEST(num == ~xor_result);
  }

  {
    util::bigint num{not_a};
    num ^= not_b;
    BOOST_TEST(num == xor_result);
  }
  {
    util::bigint num{not_b};
    num ^= not_a;
    BOOST_TEST(num == xor_result);
  }
}

BOOST_AUTO_TEST_CASE(xor_assign_self) {
  {
    util::bigint num{bit_small};
    num ^= num;
    BOOST_TEST(num == 0x0_big);
  }
  {
    util::bigint num{bit_medium};
    num ^= num;
    BOOST_TEST(num == 0x0_big);
  }
  {
    util::bigint num{bit_big};
    num ^= num;
    BOOST_TEST(num == 0x0_big);
  }
}

BOOST_DATA_TEST_CASE(lsh, lsh_test_data, a, off, result) {
  BOOST_TEST((a << off) == result);
}

BOOST_DATA_TEST_CASE(lsh_assign, lsh_test_data, a, off, result) {
  util::bigint num{a};
  num <<= off;
  BOOST_TEST(num == result);
}

BOOST_DATA_TEST_CASE(rsh, rsh_test_data, a, off, result) {
  BOOST_TEST((a >> off) == result);
}

BOOST_DATA_TEST_CASE(rsh_assign, rsh_test_data, a, off, result) {
  util::bigint num{a};
  num >>= off;
  BOOST_TEST(num == result);
}

BOOST_AUTO_TEST_SUITE_END() // bitwise

BOOST_AUTO_TEST_SUITE_END() // bigint
BOOST_AUTO_TEST_SUITE_END() // test_util
