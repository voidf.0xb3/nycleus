#ifndef UTIL_SHARED_GUARDED_VAR_HPP_INCLUDED_2GD9PH21QYGVE9RF50X5EYVAK
#define UTIL_SHARED_GUARDED_VAR_HPP_INCLUDED_2GD9PH21QYGVE9RF50X5EYVAK

#include "util/non_null_ptr.hpp"
#include <mutex>
#include <shared_mutex>
#include <type_traits>
#include <utility>

namespace util {

/** @brief A variable guarded by a shared_mutex.
 *
 * This ensures that the variable can only be accessed while the lock is held by
 * only allowing access via the lock guard objects. The shared_guard object
 * allows read-only access, and the unique_guard object allows write access. */
template<typename T>
class shared_guarded_var {
  static_assert(std::is_destructible_v<T>);

private:
  T value_;
  mutable std::shared_mutex mutex_;

public:
  /** @brief Constructs the guarded variable.
   *
   * Can throw whatever is thrown by the construction of the underlying object.
   */
  template<typename... Args>
  requires std::is_constructible_v<T, Args&&...>
  explicit shared_guarded_var(Args&&... args)
    noexcept(std::is_nothrow_constructible_v<T, Args&&...>):
    value_{std::forward<Args>(args)...} {}

  shared_guarded_var(shared_guarded_var const&) = delete;
  shared_guarded_var& operator=(shared_guarded_var const&) = delete;
  ~shared_guarded_var() = default;

  class shared_guard {
  private:
    non_null_ptr<T const> value_;
    std::shared_lock<std::shared_mutex> guard_;

    friend class shared_guarded_var;

    explicit shared_guard(T const& value, std::shared_mutex& m) noexcept:
      value_{&value}, guard_{m} {}

  public:
    shared_guard(shared_guard const&) = delete;
    shared_guard(shared_guard&&) noexcept = default;
    shared_guard& operator=(shared_guard const&) = delete;
    shared_guard& operator=(shared_guard&&) noexcept = default;
    ~shared_guard() noexcept = default;

    /** @brief Retrieves the underlying lock guard object.
     *
     * Care must be taken when working directly with the lock guard to avoid
     * unguarded access to the variable. */
    std::shared_lock<std::shared_mutex>& get_guard() noexcept {
      return guard_;
    }

    [[nodiscard]] T const& operator*() const noexcept {
      return *value_;
    }

    [[nodiscard]] T const* operator->() const noexcept {
      return value_.get();
    }
  };

  [[nodiscard]] shared_guard lock_shared() const noexcept {
    return shared_guard{value_, mutex_};
  }

  class unique_guard {
  private:
    non_null_ptr<T> value_;
    std::unique_lock<std::shared_mutex> guard_;

    friend class shared_guarded_var;

    explicit unique_guard(T& value, std::shared_mutex& m) noexcept:
      value_{&value}, guard_{m} {}

  public:
    unique_guard(unique_guard const&) = delete;
    unique_guard(unique_guard&&) noexcept = default;
    unique_guard& operator=(unique_guard const&) = delete;
    unique_guard& operator=(unique_guard&&) noexcept = default;
    ~unique_guard() noexcept = default;

    /** @brief Retrieves the underlying lock guard object.
     *
     * Care must be taken when working directly with the lock guard to avoid
     * unguarded access to the variable. */
    std::unique_lock<std::shared_mutex>& get_guard() noexcept {
      return guard_;
    }

    [[nodiscard]] T& operator*() const noexcept {
      return *value_;
    }

    [[nodiscard]] T* operator->() const noexcept {
      return value_.get();
    }
  };

  [[nodiscard]] unique_guard lock_unique() noexcept {
    return unique_guard{value_, mutex_};
  }
};

} // util

#endif
