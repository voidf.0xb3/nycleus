#ifndef UTIL_FS_PATH_HASH_HPP_INCLUDED_8WKX9MHR4HVR9U86HOKXLJQOH
#define UTIL_FS_PATH_HASH_HPP_INCLUDED_8WKX9MHR4HVR9U86HOKXLJQOH

#include <concepts>
#include <cstddef>
#include <filesystem>
#include <string>
#include <string_view>
#include <utility>

namespace util {

/** @brief Converts the given path to a narrow-character string containing its
 * native form.
 *
 * On platforms with narrow-character file names, this is simply the native
 * string. On platforms with wide-character file names, the string is the byte
 * representation of the native string.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<std::same_as<std::filesystem::path::value_type> NativeChar
  = std::filesystem::path::value_type>
[[nodiscard]] std::string path_to_string(std::filesystem::path const& path) {
  if constexpr(std::same_as<NativeChar, char>) {
    return std::basic_string<NativeChar>{path.native()};
  } else {
    std::basic_string<NativeChar> const& str{path.native()};
    return std::string{reinterpret_cast<char const*>(str.data()),
      str.size() * sizeof(wchar_t)};
  }
}

/** @brief Converts the given narrow-character string containing a path's native
 * form to a path.
 *
 * On platforms with narrow-character file names, the string is simply taken as
 * the native string. On platforms with wide-character file names, the string is
 * interpreted as the byte representation of the native string; if the string's
 * length is not a multiple of the wide character size, the extra bytes at the
 * end are ignored.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<std::same_as<std::filesystem::path::value_type> NativeChar
  = std::filesystem::path::value_type>
[[nodiscard]] std::filesystem::path string_to_path(std::string&& str) {
  if constexpr(std::same_as<NativeChar, char>) {
    return std::filesystem::path{std::basic_string<NativeChar>{std::move(str)},
      std::filesystem::path::native_format};
  } else {
    return std::filesystem::path{std::basic_string<NativeChar>{
      reinterpret_cast<wchar_t const*>(str.data()),
      str.size() / sizeof(wchar_t)},
      std::filesystem::path::native_format};
  }
}

} // util

#endif
