#ifndef UTIL_THREAD_POOL_HPP_INCLUDED_V1ZBP3P4CYV0FT3URLQHW9ZCD
#define UTIL_THREAD_POOL_HPP_INCLUDED_V1ZBP3P4CYV0FT3URLQHW9ZCD

#include "util/enumerate_view.hpp"
#include "util/guarded_var.hpp"
#include "util/iterator.hpp"
#include "util/nums.hpp"
#include "util/overflow.hpp"
#include "util/util.hpp"
#include <concepts>
#include <cstdint>
#include <exception>
#include <functional>
#include <new>
#include <ranges>
#include <stdexcept>
#include <stop_token>
#include <thread>
#include <utility>
#include <vector>

namespace util {

/** @brief Runs a thread for each range element.
 *
 * Each thread calls the supplied function, passing it a stop token and a
 * reference to the corresponding element of the range.
 *
 * The stop token is used to cancel the threads. This happens in the following
 * cases:
 * - If a thread throws an exception, all other threads are cancelled.
 * - If an exception is thrown while iterating over the range, while moving a
 *   range element's value, while allocating memory, or while starting a thread,
 *   all threads that have already been started are cancelled.
 *
 * If a thread throws an exception, the exception is rethrown from this
 * function. If multiple threads throw exceptions, one of them (chosen
 * arbitrarily) is thrown from this function and all others are discarded.
 *
 * @throws std::bad_alloc - If memory could not be allocated for the threads.
 *                        - If a thread throws an exception, but memory cannot
 *                          be allocated for a copy of that exception while
 *                          passing it to another thread.
 * @throws std::bad_exception If a thread throws an exception, but the exception
 *                            cannot be passed to another thread.
 * @throws std::system_error If a thread cannot be started. */
template<
  std::ranges::input_range Range,
  std::invocable<
    std::stop_token,
    std::ranges::range_reference_t<Range>
  > Fn
>
void run_in_threads(
  Range&& range,
  Fn&& fn
) {
  util::guarded_var<std::exception_ptr> exception;
  std::vector<std::jthread> threads;

  if constexpr(std::ranges::sized_range<Range>) {
    try {
      threads.reserve(util::throwing_cast<std::vector<std::jthread>::size_type>(
        std::ranges::size(range)));
    } catch(std::overflow_error const&) {
      std::throw_with_nested(std::bad_alloc{});
    }
  }

  using ref_t = std::ranges::range_reference_t<Range>;

  for(ref_t ref: range) {
    struct ref_box {
      ref_t ref;
    };

    threads.emplace_back([
      &fn,
      ref = ref_box{std::forward<ref_t>(ref)},
      &exception,
      &threads
    ](std::stop_token stop) mutable noexcept {
      try {
        std::invoke(fn, std::move(stop), std::forward<ref_t>(ref.ref));
      } catch(...) {
        {
          auto const e{exception.lock()};
          if(*e) {
            return;
          }
          *e = std::current_exception();
        }
        for(auto& thread: threads) {
          thread.request_stop();
        }
      }
    });
  }

  for(auto& thread: threads) {
    thread.join();
  }

  if(auto const e{exception.unlocked()}; *e) {
    std::rethrow_exception(std::move(*e));
  }
}

/** @brief Processes each range element on a thread pool.
 *
 * One thread is launched for each element of the threads range. The supplied
 * function is called once for each element of the second range. Each invocation
 * occurs on one of the aforementioned threads. The function is called with
 * three arguments: a stop token; the element of the threads range corresponding
 * to the thread on which the function is invoked; and the element of the second
 * range for with the function is invoked.
 *
 * The stop token is used to cancel the threads. This happens in the following
 * cases:
 * - If a thread throws an exception, all other threads are cancelled.
 * - If an exception is thrown while iterating over the range, while moving a
 *   range element's value, while allocating memory, or while starting a thread,
 *   all threads that have already been started are cancelled.
 *
 * If a thread throws an exception, the exception is rethrown from this
 * function. If multiple threads throw exceptions, one of them (chosen
 * arbitrarily) is thrown from this function and all others are discarded.
 *
 * @throws std::bad_alloc - If memory could not be allocated for the threads.
 *                        - If a thread throws an exception, but memory cannot
 *                          be allocated for a copy of that exception while
 *                          passing it to another thread.
 * @throws std::bad_exception If a thread throws an exception, but the exception
 *                            cannot be passed to another thread.
 * @throws std::system_error If a thread cannot be started. */
template<
  std::ranges::forward_range ThreadsRange,
  std::ranges::forward_range Range,
  std::invocable<
    std::stop_token,
    std::ranges::range_reference_t<ThreadsRange>,
    std::ranges::range_reference_t<Range>
  > Fn
>
requires
  std::ranges::sized_range<ThreadsRange>
  && std::ranges::sized_range<Range>
void run_on_thread_pool(
  ThreadsRange&& threads_range,
  Range&& range,
  Fn&& fn
) {
  // TODO: Work-stealing thread pool
  auto const num_threads{std::ranges::size(threads_range)};
  auto const range_size{std::ranges::size(range)};
  auto const els_per_thread{range_size / num_threads};
  auto const els_per_thread_rem{range_size % num_threads};
  auto next{std::ranges::begin(range)};
  using diff_t = std::ranges::range_difference_t<Range>;

  util::guarded_var<std::exception_ptr> exception;
  std::vector<std::jthread> threads;

  try {
    threads.reserve(util::throwing_cast<std::vector<std::jthread>::size_type>(
      num_threads));
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }

  for(auto&& [thread_index_s, thread_ref]: threads_range | util::enumerate) {
    auto const thread_index{[&]() noexcept {
      if constexpr(std::integral<
          std::ranges::range_difference_t<ThreadsRange>>) {
        return util::make_unsigned(thread_index_s);
      } else {
        return static_cast<std::uintmax_t>(thread_index_s);
      }
    }()};

    assert_if_noexcept(util::make_unsigned(util::distance(next,
      std::ranges::end(range))) >= els_per_thread);
    auto subrange_end{next};
    util::advance(subrange_end, static_cast<diff_t>(els_per_thread));
    if(thread_index < els_per_thread_rem) {
      assert_if_noexcept(subrange_end != std::ranges::end(range));
      ++subrange_end;
    }

    std::ranges::subrange const thread_subrange{next, subrange_end};
    next = subrange_end;

    using thread_ref_t = std::ranges::range_reference_t<ThreadsRange>;

    struct ref_box {
      thread_ref_t ref;
    };

    threads.emplace_back([
      &fn,
      thread_ref = ref_box{std::forward<thread_ref_t>(thread_ref)},
      thread_subrange,
      &exception,
      &threads
    ](std::stop_token stop) mutable noexcept {
      try {
        for(std::ranges::range_reference_t<Range> ref: thread_subrange) {
          if(stop.stop_requested()) {
            return;
          }
          std::invoke(
            fn,
            std::move(stop),
            std::forward<thread_ref_t>(thread_ref.ref),
            std::forward<std::ranges::range_reference_t<Range>>(ref)
          );
        }
      } catch(...) {
        {
          auto const e{exception.lock()};
          if(*e) {
            return;
          }
          *e = std::current_exception();
        }
        for(auto& thread: threads) {
          thread.request_stop();
        }
      }
    });
  }

  for(auto& thread: threads) {
    thread.join();
  }

  if(auto const e{exception.unlocked()}; *e) {
    std::rethrow_exception(std::move(*e));
  }
}

} // util

#endif
