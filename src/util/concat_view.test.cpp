#include <boost/test/unit_test.hpp>

#include "single_pass_view.hpp"
#include "util/concat_view.hpp"
#include "util/safe_int.hpp"
#include <forward_list>
#include <list>
#include <ranges>
#include <span>
#include <utility>
#include <vector>

namespace {

using single_pass = decltype(util::concat(
  std::declval<std::vector<util::uint64_t> const&>() | tests::single_pass,
  std::declval<std::span<util::uint64_t>>()
));
static_assert(std::ranges::input_range<single_pass>);
static_assert(!std::ranges::forward_range<single_pass>);
static_assert(std::ranges::sized_range<single_pass>);
static_assert(std::ranges::common_range<single_pass>);
static_assert(std::ranges::view<single_pass>);

using multipass = decltype(util::concat(
  std::declval<std::vector<util::uint64_t> const&>(),
  std::declval<std::span<util::uint64_t>>(),
  std::declval<std::forward_list<util::uint64_t>>()
));
static_assert(std::ranges::forward_range<multipass>);
static_assert(!std::ranges::bidirectional_range<multipass>);
static_assert(!std::ranges::sized_range<multipass>);
static_assert(std::ranges::common_range<multipass>);
static_assert(std::ranges::view<multipass>);

using bidi = decltype(util::concat(
  std::declval<std::vector<util::uint64_t> const&>(),
  std::declval<std::span<util::uint64_t>>(),
  std::declval<std::list<util::uint64_t> const&>()
));
static_assert(std::ranges::bidirectional_range<bidi>);
static_assert(!std::ranges::random_access_range<bidi>);
static_assert(std::ranges::sized_range<bidi>);
static_assert(std::ranges::common_range<bidi>);
static_assert(std::ranges::view<bidi>);

using random_access = decltype(util::concat(
  std::declval<std::vector<util::uint64_t> const&>(),
  std::declval<std::span<util::uint64_t>>()
));
static_assert(std::ranges::random_access_range<random_access>);
static_assert(!std::ranges::contiguous_range<random_access>);
static_assert(std::ranges::sized_range<random_access>);
static_assert(std::ranges::common_range<random_access>);
static_assert(std::ranges::view<random_access>);

} // (anonymous)

// static ...

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(concat_view)

BOOST_AUTO_TEST_SUITE(single_pass)

BOOST_AUTO_TEST_CASE(one) {
  std::vector<util::uint64_t> vec{100, 101, 102};
  auto const view{util::concat(vec | tests::single_pass)};
  BOOST_TEST(view == vec, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(two) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec2{200, 201, 202};
  auto const view{util::concat(vec1 | tests::single_pass, std::span{vec2})};
  util::uint64_t values[]{100, 101, 102, 200, 201, 202};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(three) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec2{200, 201, 202};
  std::vector<util::uint64_t> vec3{300, 301, 302};
  auto const view{util::concat(vec1 | tests::single_pass, std::span{vec2}, vec3)};
  util::uint64_t values[]{100, 101, 102, 200, 201, 202, 300, 301, 302};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_empty) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec3{300, 301, 302};
  auto const view{util::concat(vec1 | tests::single_pass, std::span<util::uint64_t>{}, vec3)};
  util::uint64_t values[]{100, 101, 102, 300, 301, 302};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(two_empty) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec4{400, 401, 402};
  auto const view{util::concat(vec1 | tests::single_pass, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, vec4)};
  util::uint64_t values[]{100, 101, 102, 400, 401, 402};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(three_empty) {
  auto const view{util::concat(std::span<util::uint64_t>{} | tests::single_pass,
    std::span<util::uint64_t>{}, std::span<util::uint64_t>{}, std::span<util::uint64_t>{})};
  BOOST_TEST(view.empty());
}

BOOST_AUTO_TEST_SUITE_END() // single_pass

BOOST_AUTO_TEST_SUITE(forward)

BOOST_AUTO_TEST_CASE(one) {
  std::vector<util::uint64_t> vec{100, 101, 102};
  auto const view{util::concat(vec)};
  BOOST_TEST(view == vec, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(two) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec2{200, 201, 202};
  auto const view{util::concat(vec1, std::span{vec2})};
  util::uint64_t values[]{100, 101, 102, 200, 201, 202};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(three) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec2{200, 201, 202};
  std::vector<util::uint64_t> vec3{300, 301, 302};
  auto const view{util::concat(vec1, std::span{vec2}, vec3)};
  util::uint64_t values[]{100, 101, 102, 200, 201, 202, 300, 301, 302};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_empty) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec3{300, 301, 302};
  auto const view{util::concat(vec1, std::span<util::uint64_t>{}, vec3)};
  util::uint64_t values[]{100, 101, 102, 300, 301, 302};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(two_empty) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec4{400, 401, 402};
  auto const view{util::concat(vec1, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, vec4)};
  util::uint64_t values[]{100, 101, 102, 400, 401, 402};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(three_empty) {
  auto const view{util::concat(std::span<util::uint64_t>{}, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, std::span<util::uint64_t>{})};
  BOOST_TEST(view.empty());
}

BOOST_AUTO_TEST_SUITE_END() // forward

BOOST_AUTO_TEST_SUITE(reverse)

BOOST_AUTO_TEST_CASE(one) {
  std::vector<util::uint64_t> vec{100, 101, 102};
  auto const view{util::concat(vec) | std::views::reverse};
  util::uint64_t values[]{102, 101, 100};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(two) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec2{200, 201, 202};
  auto const view{util::concat(vec1, std::span{vec2}) | std::views::reverse};
  util::uint64_t values[]{202, 201, 200, 102, 101, 100};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(three) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec2{200, 201, 202};
  std::vector<util::uint64_t> vec3{300, 301, 302};
  auto const view{util::concat(vec1, std::span{vec2}, vec3) | std::views::reverse};
  util::uint64_t values[]{302, 301, 300, 202, 201, 200, 102, 101, 100};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_empty) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec3{300, 301, 302};
  auto const view{util::concat(vec1, std::span<util::uint64_t>{}, vec3) | std::views::reverse};
  util::uint64_t values[]{302, 301, 300, 102, 101, 100};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(two_empty) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec4{400, 401, 402};
  auto const view{util::concat(vec1, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, vec4) | std::views::reverse};
  util::uint64_t values[]{402, 401, 400, 102, 101, 100};
  BOOST_TEST(view == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(three_empty) {
  auto const view{util::concat(std::span<util::uint64_t>{}, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, std::span<util::uint64_t>{}) | std::views::reverse};
  BOOST_TEST(view.empty());
}

BOOST_AUTO_TEST_SUITE_END() // reverse

BOOST_AUTO_TEST_SUITE(random_access)

BOOST_AUTO_TEST_CASE(forward) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec4{400, 401, 402};
  auto const view{util::concat(vec1, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, vec4)};

  auto it{view.begin()};
  it += 3;
  BOOST_TEST(*it == util::uint64_t{400});
  it += 2;
  BOOST_TEST(*it == util::uint64_t{402});
  BOOST_TEST(it[-2] == util::uint64_t{400});
  it -= 3;
  BOOST_TEST(*it == util::uint64_t{102});
  BOOST_TEST(it[2] == util::uint64_t{401});
}

BOOST_AUTO_TEST_CASE(random_access_reverse) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec4{400, 401, 402};
  auto const view{util::concat(vec1, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, vec4) | std::views::reverse};

  auto it{view.begin()};
  it += 3;
  BOOST_TEST(*it == util::uint64_t{102});
  it += 2;
  BOOST_TEST(*it == util::uint64_t{100});
  BOOST_TEST(it[-2] == util::uint64_t{102});
  it -= 3;
  BOOST_TEST(*it == util::uint64_t{400});
  BOOST_TEST(it[2] == util::uint64_t{101});
}

BOOST_AUTO_TEST_SUITE_END() // random_access

BOOST_AUTO_TEST_SUITE(size)

BOOST_AUTO_TEST_CASE(one) {
  std::vector<util::uint64_t> vec{100, 101, 102};
  auto const view{util::concat(vec)};
  BOOST_TEST(view.size() == 3);
}

BOOST_AUTO_TEST_CASE(two) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec2{200, 201, 202};
  auto const view{util::concat(vec1, std::span{vec2})};
  BOOST_TEST(view.size() == 6);
}

BOOST_AUTO_TEST_CASE(three) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec2{200, 201, 202};
  std::vector<util::uint64_t> vec3{300, 301, 302};
  auto const view{util::concat(vec1, std::span{vec2}, vec3)};
  BOOST_TEST(view.size() == 9);
}

BOOST_AUTO_TEST_CASE(one_empty) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec3{300, 301, 302};
  auto const view{util::concat(vec1, std::span<util::uint64_t>{}, vec3)};
  BOOST_TEST(view.size() == 6);
}

BOOST_AUTO_TEST_CASE(two_empty) {
  std::vector<util::uint64_t> vec1{100, 101, 102};
  std::vector<util::uint64_t> vec4{400, 401, 402};
  auto const view{util::concat(vec1, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, vec4)};
  BOOST_TEST(view.size() == 6);
}

BOOST_AUTO_TEST_CASE(three_empty) {
  auto const view{util::concat(std::span<util::uint64_t>{}, std::span<util::uint64_t>{},
    std::span<util::uint64_t>{}, std::span<util::uint64_t>{})};
  BOOST_TEST(view.size() == 0);
}

BOOST_AUTO_TEST_SUITE_END() // size

BOOST_AUTO_TEST_SUITE_END() // concat_view
BOOST_AUTO_TEST_SUITE_END() // test_util
