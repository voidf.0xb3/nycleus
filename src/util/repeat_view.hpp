#ifndef UTIL_REPEAT_VIEW_HPP_INCLUDED_HGTTVA3DVVUTQBDTW8Q61LITV
#define UTIL_REPEAT_VIEW_HPP_INCLUDED_HGTTVA3DVVUTQBDTW8Q61LITV

#include "util/iterator_facade.hpp"
#include "util/nums.hpp"
#include "util/type_traits.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <iterator>
#include <optional>
#include <ranges>
#include <tuple>
#include <type_traits>
#include <utility>

namespace util {

namespace detail_ {

template<std::move_constructible T>
class movable_box {
private:
  std::optional<T> t_;

public:
  constexpr movable_box() noexcept(std::is_nothrow_default_constructible_v<T>)
    requires std::default_initializable<T>: t_{std::in_place} {}

  constexpr explicit movable_box(T const& t) noexcept(std::is_nothrow_copy_constructible_v<T>)
    requires std::copy_constructible<T>: t_{t} {}

  constexpr explicit movable_box(T&& t) noexcept(std::is_nothrow_move_constructible_v<T>):
    t_{std::move(t)} {}

  constexpr movable_box(movable_box const& other) = default;

  constexpr movable_box(movable_box&& other) = default;

  constexpr ~movable_box() noexcept = default;

  constexpr movable_box& operator=(movable_box const& other)
  requires std::copy_constructible<T>
  {
    if(this != &other) {
      if constexpr(std::is_copy_assignable_v<T>) {
        t_ = other.t_;
      } else {
        if(other.t_) {
          t_.emplace(*other.t_);
        } else {
          t_.reset();
        }
      }
    }
    return *this;
  }

  constexpr movable_box& operator=(movable_box&& other)
  requires std::copy_constructible<T>
  {
    if(this != &other) {
      if constexpr(std::is_move_assignable_v<T>) {
        t_ = std::move(other.t_);
      } else {
        if(other.t_) {
          t_.emplace(std::move(*other.t_));
        } else {
          t_.reset();
        }
      }
    }
    return *this;
  }

  constexpr T const& get() const noexcept {
    return *t_;
  }
};

template<std::move_constructible T>
requires
  std::copyable<T>
  || std::is_nothrow_copy_constructible_v<T> && std::is_nothrow_move_constructible_v<T>
  || (!std::is_copy_constructible_v<T> && (
    std::movable<T>
    || std::is_nothrow_move_constructible_v<T>
  ))
class movable_box<T> {
private:
  union { T t_; };

public:
  constexpr movable_box() noexcept(std::is_nothrow_default_constructible_v<T>)
    requires std::default_initializable<T>: t_{} {}

  constexpr explicit movable_box(T const& t) noexcept(std::is_nothrow_copy_constructible_v<T>)
    requires std::copy_constructible<T>: t_{t} {}

  constexpr explicit movable_box(T&& t) noexcept(std::is_nothrow_move_constructible_v<T>):
    t_{std::move(t)} {}

  constexpr movable_box(movable_box const& other)
    noexcept(std::is_nothrow_copy_constructible_v<T>)
    requires std::copy_constructible<T>: t_{other.t_} {}

  constexpr movable_box(movable_box&& other)
    noexcept(std::is_nothrow_move_constructible_v<T>): t_{std::move(other.t_)} {}

  constexpr ~movable_box() noexcept {
    t_.~T();
  }

  constexpr movable_box& operator=(movable_box const& other)
  requires std::copy_constructible<T>
  {
    if(this != &other) {
      if constexpr(std::is_copy_assignable_v<T>) {
        t_ = other.t_;
      } else {
        t_.~T();
        std::construct_at(&t_, other.t_);
      }
    }
    return *this;
  }

  constexpr movable_box& operator=(movable_box&& other) {
    if(this != &other) {
      if constexpr(std::is_move_assignable_v<T>) {
        t_ = std::move(other.t_);
      } else {
        t_.~T();
        std::construct_at(&t_, std::move(other.t_));
      }
    }
    return *this;
  }

  constexpr T const& get() const noexcept {
    return t_;
  }
};

template<typename Bound>
struct repeat_view_diff_type {
  using type = std::ranges::range_difference_t<std::ranges::iota_view<Bound>>;
};

template<>
struct repeat_view_diff_type<std::unreachable_sentinel_t> {
  using type = std::ptrdiff_t;
};

} // detail_

/** @brief An implementation of std::views::repeat_view from C++23.
 *
 * This is a view that produces copies of a specified value. It is possible to specify the
 * total number of items in the range. Alternatively, the number can be omitted, in which case
 * the range is infinite. */
template<std::move_constructible T, std::semiregular Bound = std::unreachable_sentinel_t>
requires util::object<T> && std::same_as<T, std::remove_cvref_t<T>>
  && (std::integral<Bound> || std::same_as<Bound, std::unreachable_sentinel_t>)
class repeat_view: public std::ranges::view_interface<repeat_view<T, Bound>> {
private:
  detail_::movable_box<T> value_;
  [[no_unique_address]] Bound bound_;

  class iterator_core {
  private:
    using index_type = std::conditional_t<std::same_as<Bound, std::unreachable_sentinel_t>,
      std::ptrdiff_t, Bound>;
    T const* value_;
    index_type current_;

    friend class repeat_view<T, Bound>;
    constexpr explicit iterator_core(T const& value, index_type bound) noexcept:
      value_{&value}, current_{bound} {}

  public:
    constexpr iterator_core() noexcept: value_{nullptr} {}

  public:
    using difference_type = detail_::repeat_view_diff_type<Bound>::type;

    constexpr T const& dereference() const noexcept {
      return *value_;
    }

    constexpr void advance(difference_type offset) noexcept {
      current_ += offset;
    }

    constexpr difference_type distance_to(iterator_core other) const noexcept {
      return static_cast<difference_type>(other.current_)
        - static_cast<difference_type>(current_);
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  constexpr repeat_view() requires std::default_initializable<T> = default;

  constexpr repeat_view(T const& value, Bound bound = {})
  noexcept(std::is_nothrow_copy_constructible_v<T>)
  requires std::copy_constructible<T>: value_{value}, bound_{bound}
  {
    if constexpr(!std::same_as<Bound, std::unreachable_sentinel_t>) {
      assert(bound_ >= 0);
    }
  }

  constexpr repeat_view(T&& value, Bound bound = {})
  noexcept(std::is_nothrow_move_constructible_v<T>): value_{std::move(value)}, bound_{bound}
  {
    if constexpr(!std::same_as<Bound, std::unreachable_sentinel_t>) {
      assert(bound_ >= 0);
    }
  }

  template<typename... TArgs, typename... BoundArgs>
  requires std::constructible_from<T, TArgs...> && std::constructible_from<Bound, BoundArgs...>
  constexpr repeat_view(
    std::piecewise_construct_t,
    std::tuple<TArgs...> t_args,
    std::tuple<BoundArgs...> bound_args = std::tuple<>{}
  )
  noexcept(
    std::is_nothrow_constructible_v<T, TArgs...>
    && std::is_nothrow_constructible_v<T, BoundArgs...>
  ):
    value_{std::make_from_tuple(std::move(t_args))},
    bound_{std::make_from_tuple(std::move(bound_args))}
  {
    if constexpr(!std::same_as<Bound, std::unreachable_sentinel_t>) {
      assert(bound_ >= 0);
    }
  }

  constexpr iterator begin() const noexcept {
    return iterator{value_.get(), {}};
  }

  constexpr iterator end() const noexcept
  requires (!std::same_as<Bound, std::unreachable_sentinel_t>)
  {
    return iterator{value_.get(), bound_};
  }

  constexpr std::unreachable_sentinel_t end() const noexcept {
    return std::unreachable_sentinel;
  }

  constexpr auto size() const noexcept
  requires (!std::same_as<Bound, std::unreachable_sentinel_t>)
  {
    if constexpr(std::signed_integral<T>) {
      return util::make_unsigned(bound_);
    } else {
      return bound_;
    }
  }
};

template<typename T, typename Bound = std::unreachable_sentinel_t>
repeat_view(T, Bound = {}) -> repeat_view<T, Bound>;

namespace detail_ {

struct repeat_call {
  template<typename T, typename Bound = std::unreachable_sentinel_t>
  constexpr auto operator()(T&& value, Bound bound = {}) const noexcept {
    return repeat_view{std::forward<T>(value), bound};
  }
};

} // detail_

inline constexpr detail_::repeat_call repeat;

} // util

#endif
