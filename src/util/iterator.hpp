#ifndef UTIL_ITERATOR_HPP_INCLUDED_5Z9YT692B0WJ1SCW85EZX8157
#define UTIL_ITERATOR_HPP_INCLUDED_5Z9YT692B0WJ1SCW85EZX8157

#include "util/iterator_facade.hpp"
#include "util/type_traits.hpp"
#include "util/u8compat.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <iterator>
#include <limits>
#include <type_traits>
#include <utility>

namespace util {

/** @brief An output iterator that discards everything sent to it. */
struct null_output_iterator {
  using difference_type = std::ptrdiff_t;
  using value_type = void;
  using pointer = void;
  using reference = void;
  using iterator_category = std::output_iterator_tag;

  [[nodiscard]] null_output_iterator& operator*() noexcept { return *this; }
  template<typename T>
  null_output_iterator& operator=(T&&) noexcept { return *this; }
  null_output_iterator& operator++() noexcept { return *this; }
  null_output_iterator operator++(int) noexcept { return *this; }
};

// Iterator utilities //////////////////////////////////////////////////////////

namespace detail_ {

/** @brief Helper function to write the exception specification of
 * util::advance. */
template<std::input_or_output_iterator Iterator>
[[nodiscard]] constexpr bool advance_noexcept() noexcept {
  if constexpr(std::random_access_iterator<Iterator>) {
    return noexcept(std::declval<Iterator&>()
      += std::declval<std::iter_difference_t<Iterator>>());
  } else if constexpr(std::bidirectional_iterator<Iterator>) {
    return noexcept(++std::declval<Iterator&>())
      && noexcept(--std::declval<Iterator&>());
  } else {
    return noexcept(++std::declval<Iterator&>());
  }
}

}

/** @brief A reimplementation of std::advance that has a functioning exception
 * specification. */
template<std::input_or_output_iterator Iterator>
void advance(Iterator& it, std::iter_difference_t<Iterator> off)
    noexcept(detail_::advance_noexcept<Iterator>()) {
  if constexpr(std::random_access_iterator<Iterator>) {
    it += off;
  } else {
    while(off > 0) {
      ++it;
      --off;
    }
    if constexpr(std::bidirectional_iterator<Iterator>) {
      while(off < 0) {
        --it;
        ++off;
      }
    }
  }
}

namespace detail_ {

/** @brief Helper function to write the exception specification of
 * util::distance. */
template<std::input_or_output_iterator Iterator,
  std::sentinel_for<Iterator> Sentinel>
[[nodiscard]] constexpr bool distance_noexcept() noexcept {
  if constexpr(std::sized_sentinel_for<Sentinel, Iterator>) {
    return noexcept(std::declval<Sentinel>() - std::declval<Iterator>());
  } else {
    return noexcept(std::declval<Iterator>() != std::declval<Sentinel>())
      && noexcept(++std::declval<Iterator&>());
  }
}

}

/** @brief A reimplementation of std::distance that allows for heterogeneous
 * sentinels. */
template<std::input_or_output_iterator Iterator,
  std::sentinel_for<Iterator> Sentinel = Iterator>
[[nodiscard]] auto distance(Iterator it, Sentinel iend)
    noexcept(detail_::distance_noexcept<Iterator, Sentinel>()) {
  if constexpr(std::sized_sentinel_for<Sentinel, Iterator>) {
    return iend - it;
  } else {
    std::iter_difference_t<Iterator> result{0};
    while(it != iend) {
      ++it;
      ++result;
    }
    return result;
  }
}

namespace detail_ {

/** @brief Helper function to write exception specification for
 * util::next_until. */
template<std::input_or_output_iterator Iterator,
  std::sentinel_for<Iterator> Sentinel>
[[nodiscard]] constexpr bool next_until_noexcept() noexcept {
  if constexpr(std::sized_sentinel_for<Sentinel, Iterator>) {
    return noexcept(std::declval<Sentinel>() - std::declval<Iterator>())
      && noexcept(util::advance(std::declval<Iterator&>(),
      std::declval<std::iter_difference_t<Iterator>>()));
  } else {
    return noexcept(std::declval<Iterator>() != std::declval<Sentinel>())
      && noexcept(++std::declval<Iterator&>());
  }
}

}

/** @brief Advances the given iterator by the given number of steps or until the
 * end of the range, whichever comes first.
 *
 * @returns A pair consisting of:
 *          - the resulting iterator; and
 *          - the distance between the provided iterator and the resulting one,
 *            which may be less than the distance given to the function, if the
 *            end is reached first. */
template<std::input_or_output_iterator Iterator,
  std::sentinel_for<Iterator> Sentinel = Iterator>
std::pair<Iterator, std::iter_difference_t<Iterator>> next_until(Iterator it,
    Sentinel iend, std::iter_difference_t<Iterator> dist)
    noexcept(detail_::next_until_noexcept<Iterator, Sentinel>()
    && std::is_nothrow_move_constructible_v<Iterator>) {
  assert(dist >= 0);
  if constexpr(std::sized_sentinel_for<Sentinel, Iterator>) {
    std::iter_difference_t<Iterator> const size{iend - it};
    if(dist > size) {
      dist = size;
    }
    util::advance(it, dist);
    return {std::move(it), dist};
  } else {
    std::iter_difference_t<Iterator> current_dist{0};
    while(it != iend && current_dist < dist) {
      ++it;
      ++current_dist;
    }
    return {std::move(it), current_dist};
  }
}

/** @brief A concept testing if a type is a valid legacy (pre-C++20) iterator
 * type. */
template<typename T>
concept legacy_iterator = std::derived_from<
  typename std::iterator_traits<T>::iterator_category,
  std::input_iterator_tag>;

}

#endif
