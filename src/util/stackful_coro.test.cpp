#include <boost/test/unit_test.hpp>

#include "util/stackful_coro.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <cstdint>
#include <exception>
#include <vector>

namespace {

namespace coro = util::stackful_coro;

// Basic coroutines ////////////////////////////////////////////////////////////

coro::result<std::uint64_t> basic_coro_0(coro::context, std::uint64_t x) {
  co_return 100 * (x + 1);
}

coro::result<> basic_coro_1(coro::context ctx,
    std::vector<std::uint64_t>& log) {
  log.push_back(0);
  log.push_back(co_await basic_coro_0(ctx, 0));
  log.push_back(2);
  log.push_back(co_await basic_coro_0(ctx, 1));
  log.push_back(4);
}

// Coroutines returning references /////////////////////////////////////////////

coro::result<std::uint64_t&> ret_ref_coro_0(coro::context,
    std::uint64_t& result) {
  co_return result;
}

coro::result<> ret_ref_coro_1(coro::context ctx) {
  std::uint64_t x{0};
  std::uint64_t& result{co_await ret_ref_coro_0(ctx, x)};
  BOOST_TEST(x == 0);
  result = 179;
  BOOST_TEST(x == 179);
}

// Suspending coroutines ///////////////////////////////////////////////////////

coro::result<> suspend_coro_0(coro::context ctx, std::uint64_t& x,
    std::uint64_t&) {
  BOOST_TEST(x == 0);
  co_await ctx.suspend();
  BOOST_TEST(x == 179);
  x = 179179;
}

coro::result<> suspend_coro_1(coro::context ctx, std::uint64_t& x) {
  std::uint64_t y{100};
  co_await suspend_coro_0(ctx, x, y);
  BOOST_TEST(y == 100);
}

// Recursive coroutines ////////////////////////////////////////////////////////

coro::result<std::uint64_t> factorial_coro(coro::context ctx, std::uint64_t x) {
  if(x == 0) {
    // Just to check that recursive coroutines suspend correctly
    co_await ctx.suspend();
    co_return 1;
  } else {
    co_return x * co_await factorial_coro(ctx, x - 1);
  }
}

// Deeply nested coroutines ////////////////////////////////////////////////////

coro::result<std::uint64_t> deep_coro(coro::context ctx, std::uint64_t num) {
  if(num == 0) {
    co_return 0;
  } else {
    co_return num + co_await deep_coro(ctx, num - 1);
  }
}

// Destroying suspended coroutines /////////////////////////////////////////////

coro::result<> deep_suspending_coro(coro::context ctx, std::uint64_t num) {
  if(num == 0) {
    co_return;
  }
  co_await deep_suspending_coro(ctx, num - 1);
  if(num == 50) {
    co_await ctx.suspend();
    BOOST_REQUIRE(false);
    util::unreachable();
  }
  BOOST_REQUIRE(num < 50);
}

// Tail calls with returned values /////////////////////////////////////////////

coro::result<std::uint64_t> tail_call_coro_0(coro::context, std::uint64_t x) {
  co_return x + 79;
}

coro::result<std::uint64_t> tail_call_coro_1(coro::context ctx,
    std::uint64_t x) {
  co_return co_await ctx.tail_call([x](coro::context ctx) {
    return tail_call_coro_0(ctx, 100 * x);
  });
}

coro::result<> tail_call_coro_2(coro::context ctx) {
  std::uint64_t const result{co_await tail_call_coro_1(ctx, 1)};
  BOOST_TEST(result == 179);
}

// Tail calls without returned values //////////////////////////////////////////

coro::result<> tail_call_void_coro_0(coro::context, std::uint64_t& x) {
  x += 79;
  co_return; // make this a coroutine
}

coro::result<> tail_call_void_coro_1(coro::context ctx, std::uint64_t& x) {
  x *= 100;
  co_return co_await ctx.tail_call([&x](coro::context ctx) {
    return tail_call_void_coro_0(ctx, x);
  });
}

coro::result<> tail_call_void_coro_2(coro::context ctx) {
  std::uint64_t x{1};
  co_await tail_call_void_coro_1(ctx, x);
  BOOST_TEST(x == 179);
}

// Throwing coroutines /////////////////////////////////////////////////////////

struct coro_test_exception_0: public std::exception {
  [[nodiscard]] virtual gsl::czstring what() const noexcept override {
    return "coro_test_exception_0";
  }
};

struct coro_test_exception_1: public std::exception {
  [[nodiscard]] virtual gsl::czstring what() const noexcept override {
    return "coro_test_exception_1";
  }
};

coro::result<> throw_coro_0(coro::context) {
  throw coro_test_exception_0{};
  co_return; // make this a coroutine
}

coro::result<> throw_coro_1(coro::context ctx) {
  co_await throw_coro_0(ctx);
  BOOST_REQUIRE(false);
  util::unreachable();
}

coro::result<> throw_coro_2(coro::context ctx) {
  try {
    co_await throw_coro_1(ctx);
    BOOST_REQUIRE(false);
    util::unreachable();
  } catch(coro_test_exception_0 const&) {
    throw coro_test_exception_1{};
  } catch(...) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }
}

// Throwing from move construction of return values ////////////////////////////

struct throw_on_move {
  std::uint64_t counter;

  explicit throw_on_move(std::uint64_t counter) noexcept: counter{counter} {}

  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
  throw_on_move(throw_on_move const&) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }

  // NOLINTBEGIN(bugprone-exception-escape)
  // NOLINTBEGIN(performance-noexcept-move-constructor)
  throw_on_move(throw_on_move&& other): counter{other.counter} {
  // NOLINTEND(performance-noexcept-move-constructor)
  // NOLINTEND(bugprone-exception-escape)
    if(counter == 0) {
      throw coro_test_exception_0{};
    } else {
      --counter;
    }
  }

  throw_on_move& operator=(throw_on_move const&) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }

  // NOLINTNEXTLINE(performance-noexcept-move-constructor)
  throw_on_move& operator=(throw_on_move&&) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }

  ~throw_on_move() = default;
};

coro::result<throw_on_move> throw_from_ret_move_coro_0(coro::context,
    std::uint64_t counter) {
  co_return throw_on_move{counter};
}

coro::result<bool> throw_from_ret_move_coro_1(coro::context ctx,
    std::uint64_t counter) {
  try {
    co_await throw_from_ret_move_coro_0(ctx, counter);
  } catch(coro_test_exception_0 const&) {
    co_return true;
  } catch(...) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }
  co_return false;
}

// Throwing from tail call callbacks with returned values //////////////////////

coro::result<std::uint64_t> throw_from_tail_coro_0(coro::context ctx) {
  try {
    co_return co_await ctx.tail_call([](coro::context)
        -> coro::result<std::uint64_t> {
      throw coro_test_exception_0{};
    });
  } catch(...) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }
}

coro::result<> throw_from_tail_coro_1(coro::context ctx) {
  try {
    co_await throw_from_tail_coro_0(ctx);
    BOOST_REQUIRE(false);
    util::unreachable();
  } catch(coro_test_exception_0 const&) {
    co_return;
  } catch(...) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }
}

// Throwing from tail call callbacks without returned values ///////////////////

coro::result<> throw_from_tail_void_coro_0(coro::context ctx) {
  try {
    co_return co_await ctx.tail_call([](coro::context) -> coro::result<> {
      throw coro_test_exception_0{};
    });
  } catch(...) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }
}

coro::result<> throw_from_tail_void_coro_1(coro::context ctx) {
  try {
    co_await throw_from_tail_void_coro_0(ctx);
    BOOST_REQUIRE(false);
    util::unreachable();
  } catch(coro_test_exception_0 const&) {
    co_return;
  } catch(...) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }
}

// Throwing from move of tail call callbacks with returned values //////////////

coro::result<std::uint64_t> throw_from_tail_move_coro_0(coro::context) {
  co_return 0;
}

struct throw_on_move_tail_callback: throw_on_move {
  explicit throw_on_move_tail_callback(std::uint64_t counter) noexcept:
    throw_on_move{counter} {}

  coro::result<std::uint64_t> operator()(coro::context ctx) {
    return throw_from_tail_move_coro_0(ctx);
  }
};

coro::result<std::uint64_t> throw_from_tail_move_coro_1(coro::context ctx,
    std::uint64_t counter) {
  co_return co_await ctx.tail_call(throw_on_move_tail_callback{counter});
}

coro::result<bool> throw_from_tail_move_coro_2(coro::context ctx,
    std::uint64_t counter) {
  try {
    co_await throw_from_tail_move_coro_1(ctx, counter);
    co_return false;
  } catch(coro_test_exception_0 const&) {
    co_return true;
  } catch(...) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }
}

// Throwing from move of tail call callbacks without returned values ///////////

coro::result<> throw_from_tail_void_move_coro_0(coro::context) {
  co_return; // make this a coroutine
}

struct throw_on_move_tail_void_callback: throw_on_move {
  explicit throw_on_move_tail_void_callback(std::uint64_t counter) noexcept:
    throw_on_move{counter} {}

  coro::result<> operator()(coro::context ctx) {
    return throw_from_tail_void_move_coro_0(ctx);
  }
};

coro::result<> throw_from_tail_void_move_coro_1(coro::context ctx,
    std::uint64_t counter) {
  co_return co_await ctx.tail_call(throw_on_move_tail_void_callback{counter});
}

coro::result<bool> throw_from_tail_void_move_coro_2(coro::context ctx,
    std::uint64_t counter) {
  try {
    co_await throw_from_tail_void_move_coro_1(ctx, counter);
    co_return false;
  } catch(coro_test_exception_0 const&) {
    co_return true;
  } catch(...) {
    BOOST_REQUIRE(false);
    util::unreachable();
  }
}

} // (anonymous)

// Tests ///////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(stackful_coro)

BOOST_AUTO_TEST_CASE(basic) {
  std::vector<std::uint64_t> log;
  coro::processor<> proc;

  proc.start(basic_coro_1, log);
  BOOST_REQUIRE(proc.done());

  std::uint64_t const test[]{0, 100, 2, 200, 4};
  BOOST_TEST(log == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(ret_ref) {
  {
    coro::processor<std::uint64_t&> proc;

    std::uint64_t x{0};
    proc.start(ret_ref_coro_0, x);
    BOOST_REQUIRE(proc.done());

    std::uint64_t& result{proc.get_result()};

    BOOST_TEST(x == 0);
    result = 179;
    BOOST_TEST(x == 179);
  }

  {
    coro::processor<> proc;
    proc.start(ret_ref_coro_1);
    BOOST_TEST(proc.done());
  }
}

BOOST_AUTO_TEST_CASE(suspend) {
  std::uint64_t x{0};
  coro::processor<> proc;

  proc.start(suspend_coro_1, x);
  BOOST_REQUIRE(!proc.done());

  x = 179;
  proc.resume();
  BOOST_REQUIRE(proc.done());

  BOOST_TEST(x == 179179);
}

BOOST_AUTO_TEST_CASE(recursion) {
  coro::processor<std::uint64_t> proc;

  proc.start(factorial_coro, 5);
  BOOST_REQUIRE(!proc.done());

  proc.resume();
  BOOST_REQUIRE(proc.done());
  BOOST_TEST(proc.get_result() == 120);
}

BOOST_AUTO_TEST_CASE(deep) {
  coro::processor<std::uint64_t> proc;
  proc.start(deep_coro, 100);
  BOOST_REQUIRE(proc.done());
  BOOST_TEST(proc.get_result() == 5050);
}

BOOST_AUTO_TEST_CASE(tail_call_value) {
  {
    coro::processor<std::uint64_t> proc;
    proc.start(tail_call_coro_1, 1);
    BOOST_REQUIRE(proc.done());
    BOOST_TEST(proc.get_result() == 179);
  }

  {
    coro::processor<> proc;
    proc.start(tail_call_coro_2);
    BOOST_TEST(proc.done());
  }
}

BOOST_AUTO_TEST_CASE(tail_call_void) {
  {
    std::uint64_t x{1};
    coro::processor<> proc;
    proc.start(tail_call_void_coro_1, x);
    BOOST_REQUIRE(proc.done());
    BOOST_TEST(x == 179);
  }

  {
    coro::processor<> proc;
    proc.start(tail_call_void_coro_2);
    BOOST_TEST(proc.done());
  }
}

BOOST_AUTO_TEST_CASE(destroy_while_suspended) {
  coro::processor<> proc;
  proc.start(deep_suspending_coro, 100);
  BOOST_TEST(!proc.done());
}

BOOST_AUTO_TEST_CASE(throwing) {
  coro::processor<> proc;
  BOOST_CHECK_THROW(proc.start(throw_coro_2), coro_test_exception_1);
}

BOOST_AUTO_TEST_CASE(throw_from_ret_move) {
  constexpr std::uint64_t max_counter{10};

  for(std::uint64_t i{0}; i < max_counter; ++i) {
    coro::processor<throw_on_move> proc;
    try {
      proc.start(throw_from_ret_move_coro_0, i);
      BOOST_REQUIRE(proc.done());
      break;
    } catch(coro_test_exception_0 const&) {
    }
  }

  for(std::uint64_t i{0}; i < max_counter; ++i) {
    coro::processor<bool> proc;
    proc.start(throw_from_ret_move_coro_1, i);
    BOOST_REQUIRE(proc.done());
    if(!proc.get_result()) {
      break;
    }
  }
}

BOOST_AUTO_TEST_CASE(throw_from_tail_call_value) {
  {
    coro::processor<std::uint64_t> proc;
    BOOST_CHECK_THROW(proc.start(throw_from_tail_coro_0),
      coro_test_exception_0);
  }

  {
    coro::processor<> proc;
    proc.start(throw_from_tail_coro_1);
    BOOST_TEST(proc.done());
  }
}

BOOST_AUTO_TEST_CASE(throw_from_tail_call_void) {
  {
    coro::processor<> proc;
    BOOST_CHECK_THROW(proc.start(throw_from_tail_void_coro_0),
      coro_test_exception_0);
  }

  {
    coro::processor<> proc;
    proc.start(throw_from_tail_void_coro_1);
    BOOST_TEST(proc.done());
  }
}

BOOST_AUTO_TEST_CASE(throw_from_tail_value_move) {
  constexpr std::uint64_t max_counter{10};

  for(std::uint64_t i{0}; i < max_counter; ++i) {
    coro::processor<std::uint64_t> proc;
    try {
      proc.start(throw_from_tail_move_coro_1, i);
      BOOST_TEST(proc.done());
      break;
    } catch(coro_test_exception_0 const&) {
    }
  }

  for(std::uint64_t i{0}; i < max_counter; ++i) {
    coro::processor<bool> proc;
    proc.start(throw_from_tail_move_coro_2, i);
    BOOST_REQUIRE(proc.done());
    if(!proc.get_result()) {
      break;
    }
  }
}

BOOST_AUTO_TEST_CASE(throw_from_tail_void_move) {
  constexpr std::uint64_t max_counter{10};

  for(std::uint64_t i{0}; i < max_counter; ++i) {
    coro::processor<> proc;
    try {
      proc.start(throw_from_tail_void_move_coro_1, i);
      BOOST_TEST(proc.done());
      break;
    } catch(coro_test_exception_0 const&) {
    }
  }

  for(std::uint64_t i{0}; i < max_counter; ++i) {
    coro::processor<bool> proc;
    proc.start(throw_from_tail_void_move_coro_2, i);
    BOOST_REQUIRE(proc.done());
    if(!proc.get_result()) {
      break;
    }
  }
}

BOOST_AUTO_TEST_SUITE_END() // stackful_coro
BOOST_AUTO_TEST_SUITE_END() // test_util
