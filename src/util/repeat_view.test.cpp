#include <boost/test/unit_test.hpp>

#include "util/repeat_view.hpp"
#include "util/ranges.hpp"
#include <algorithm>
#include <array>
#include <cstdint>
#include <ranges>
#include <string>
#include <vector>

static_assert(std::ranges::random_access_range<util::repeat_view<std::string, int>>);
static_assert(std::ranges::view<util::repeat_view<std::string, int>>);
static_assert(std::ranges::random_access_range<util::repeat_view<std::string>>);
static_assert(std::ranges::view<util::repeat_view<std::string>>);

static_assert(std::ranges::equal(util::repeat(100, 5), std::array{100, 100, 100, 100, 100}));
static_assert(std::ranges::equal(util::repeat(100) | std::views::take(5),
  std::array{100, 100, 100, 100, 100}));

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(repeat_view)

BOOST_AUTO_TEST_CASE(finite) {
  std::string values[]{"100", "100", "100", "100", "100"};
  auto const view{util::repeat(std::string{"100"}, 5)};
  auto const vec{view | util::range_to<std::vector>()};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(infinite) {
  std::string values[]{"100", "100", "100", "100", "100"};
  auto const view{util::repeat(std::string{"100"}) | std::views::take(5)};
  auto const vec{view | util::range_to<std::vector>()};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // repeat_view
BOOST_AUTO_TEST_SUITE_END() // test_util
