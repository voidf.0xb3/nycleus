#ifndef UTIL_ENUMERATE_VIEW_HPP_INCLUDED_DTBTWJTZSN5IZNV2TJC1Z5Z7V
#define UTIL_ENUMERATE_VIEW_HPP_INCLUDED_DTBTWJTZSN5IZNV2TJC1Z5Z7V

#include "util/fancy_tuple.hpp"
#include "util/iterator_facade.hpp"
#include "util/range_adaptor_closure.hpp"
#include <concepts>
#include <iterator>
#include <ranges>
#include <type_traits>
#include <tuple>
#include <utility>

namespace util {

/** @brief An implementation of std::enumerate_view from C++23. */
template<std::ranges::view V>
requires std::ranges::input_range<V>
  && std::move_constructible<std::ranges::range_reference_t<V>>
  && std::move_constructible<std::ranges::range_rvalue_reference_t<V>>
class enumerate_view: public std::ranges::view_interface<enumerate_view<V>> {
private:
  V view_;

  template<bool Const>
  class iterator_core {
  private:
    using base_view = std::conditional_t<Const, V const, V>;

    std::ranges::iterator_t<base_view> it_;
    std::ranges::range_difference_t<base_view> pos_;

    friend class enumerate_view;
    constexpr explicit iterator_core(
      std::ranges::iterator_t<base_view> it,
      std::ranges::range_difference_t<base_view> pos
    ): it_{std::move(it)}, pos_{pos} {}

  protected:
    using difference_type = std::ranges::range_difference_t<base_view>;

    using value_type = fancy_tuple<difference_type,
      std::ranges::range_value_t<base_view>>;

  public:
    iterator_core() = default;

    constexpr iterator_core(iterator_core<!Const> other) requires Const
      && std::convertible_to<std::ranges::iterator_t<V>,
      std::ranges::iterator_t<V const>>:
      it_{std::move(other).base()}, pos_{other.index()} {}

    [[nodiscard]] constexpr std::ranges::iterator_t<base_view> const& base()
        const& noexcept {
      return it_;
    }

    [[nodiscard]] constexpr std::ranges::iterator_t<base_view> base() && {
      return std::move(it_);
    }

    [[nodiscard]] constexpr difference_type index() const noexcept {
      return pos_;
    }

  protected:
    static constexpr bool is_single_pass{
      !std::ranges::forward_range<base_view>};

    [[nodiscard]] constexpr fancy_tuple<difference_type,
        std::ranges::range_reference_t<base_view>> dereference() const {
      return {pos_, *it_};
    }

    constexpr void increment() {
      ++it_;
      ++pos_;
    }

    [[nodiscard]] constexpr bool equal_to(iterator_core const& other)
        const noexcept {
      return pos_ == other.pos_;
    }

    constexpr void decrement()
        requires std::ranges::bidirectional_range<base_view> {
      --it_;
      --pos_;
    }

    [[nodiscard]] constexpr difference_type distance_to(
      iterator_core const& other
    ) const noexcept {
      return other.pos_ - pos_;
    }

    constexpr void advance(difference_type offset)
        requires std::ranges::random_access_range<base_view> {
      it_ += offset;
      pos_ += offset;
    }

    [[nodiscard]] constexpr fancy_tuple<difference_type,
        std::ranges::range_reference_t<base_view>>
        at(difference_type offset) const
        requires std::ranges::random_access_range<base_view> {
      return {pos_ + offset, it_[offset]};
    }

    [[nodiscard]] friend constexpr fancy_tuple<difference_type,
        std::ranges::range_rvalue_reference_t<base_view>>
        iter_move(iterator_core const& it)
        noexcept(noexcept(std::ranges::iter_move(it.it_))
        && std::is_nothrow_move_constructible_v<
        std::ranges::range_rvalue_reference_t<base_view>>) {
      return {it.pos_, std::ranges::iter_move(it.it_)};
    }
  };

  template<bool Const>
  using iterator = util::iterator_facade<iterator_core<Const>>;

  template<bool Const>
  class sentinel {
  private:
    using base_view = std::conditional_t<Const, V const, V>;

    std::ranges::sentinel_t<base_view> iend_;

    friend class enumerate_view;
    constexpr explicit sentinel(std::ranges::sentinel_t<base_view> const& iend):
      iend_{std::move(iend)} {}

  public:
    sentinel() = default;

    constexpr sentinel(sentinel<false> const& other) requires Const
      && std::convertible_to<std::ranges::sentinel_t<V>,
      std::ranges::sentinel_t<V const>>:
      iend_{std::move(other).base()} {}

    [[nodiscard]] constexpr std::ranges::sentinel_t<base_view> const& base()
        const& noexcept {
      return iend_;
    }

    [[nodiscard]] constexpr std::ranges::sentinel_t<base_view> base()
        && noexcept {
      return std::move(iend_);
    }

    [[nodiscard]] friend constexpr bool operator==(
      iterator<Const> const& a,
      sentinel const& b
    ) {
      return a.base() == b.base();
    }

    template<bool OtherConst>
    [[nodiscard]] friend constexpr std::ranges::range_difference_t<
        std::conditional_t<OtherConst, V const, V>> operator-(
      iterator<OtherConst> const& a,
      sentinel const& b
    ) requires std::sized_sentinel_for<
      std::ranges::sentinel_t<base_view>,
      std::ranges::iterator_t<std::conditional_t<OtherConst, V const, V>>
    > {
      return a.base() - b.base();
    }

    template<bool OtherConst>
    [[nodiscard]] friend constexpr std::ranges::range_difference_t<
        std::conditional_t<OtherConst, V const, V>> operator-(
      sentinel const& a,
      iterator<OtherConst> const& b
    ) requires std::sized_sentinel_for<
      std::ranges::sentinel_t<base_view>,
      std::ranges::iterator_t<std::conditional_t<OtherConst, V const, V>>
    > {
      return a.base() - b.base();
    }
  };

public:
  constexpr enumerate_view() = default;

  constexpr explicit enumerate_view(V view): view_{std::move(view)} {}

  [[nodiscard]] constexpr V base() const& requires std::copy_constructible<V> {
    return view_;
  }

  [[nodiscard]] constexpr V base() && {
    return std::move(view_);
  }

  [[nodiscard]] constexpr auto begin()
  requires (!std::ranges::range<V const>
    || !std::same_as<std::ranges::iterator_t<V>,
      std::ranges::iterator_t<V const>>
    || !std::same_as<std::ranges::sentinel_t<V>,
      std::ranges::sentinel_t<V const>>)
  {
    return iterator<false>{std::ranges::begin(view_), 0};
  }

  [[nodiscard]] constexpr auto begin() const
  requires std::ranges::input_range<V const>
    && std::move_constructible<std::ranges::range_reference_t<V const>>
    && std::move_constructible<
      std::ranges::range_rvalue_reference_t<V const>>
  {
    return iterator<true>{std::ranges::begin(view_), 0};
  }

  [[nodiscard]] constexpr auto end()
  requires (!std::ranges::range<V const>
    || !std::same_as<std::ranges::iterator_t<V>,
      std::ranges::iterator_t<V const>>
    || !std::same_as<std::ranges::sentinel_t<V>,
      std::ranges::sentinel_t<V const>>)
  {
    if constexpr(std::ranges::common_range<V> && std::ranges::sized_range<V>) {
      return iterator<false>{std::ranges::end(view_), std::ranges::size(view_)};
    } else {
      return sentinel<false>{std::ranges::end(view_)};
    }
  }

  [[nodiscard]] constexpr auto end() const
  requires std::ranges::input_range<V const>
    && std::move_constructible<std::ranges::range_reference_t<V const>>
    && std::move_constructible<
      std::ranges::range_rvalue_reference_t<V const>>
  {
    if constexpr(std::ranges::common_range<V const>
        && std::ranges::sized_range<V const>) {
      return iterator<true>{std::ranges::end(view_),
        std::ranges::distance(view_)};
    } else {
      return sentinel<true>{std::ranges::end(view_)};
    }
  }

  [[nodiscard]] constexpr auto size() requires std::ranges::sized_range<V> {
    return std::ranges::size(view_);
  }

  [[nodiscard]] constexpr auto size() const
      requires std::ranges::sized_range<V const> {
    return std::ranges::size(view_);
  }
};

template<typename R>
enumerate_view(R&&) -> enumerate_view<std::views::all_t<R>>;

} // util

template<::std::ranges::view V>
requires ::std::ranges::input_range<V>
  && ::std::move_constructible<::std::ranges::range_reference_t<V>>
  && ::std::move_constructible<::std::ranges::range_rvalue_reference_t<V>>
constexpr bool std::ranges::enable_borrowed_range<::util::enumerate_view<V>>{
  ::std::ranges::enable_borrowed_range<V>};

namespace util {

namespace detail_ {

struct enumerate_t: public range_adaptor_closure<enumerate_t> {
  template<std::ranges::viewable_range R>
  [[nodiscard]] constexpr auto operator()(R&& range) const {
    return enumerate_view{std::views::all_t<R>{std::forward<R>(range)}};
  }
};

} // detail_

inline constexpr detail_::enumerate_t enumerate;

} // util

#endif
