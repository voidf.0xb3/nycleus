#ifndef UTIL_STATIC_TRIE_HPP_INCLUDED_189RB8O5DJ6Y7IQ9LERVJ7B4V
#define UTIL_STATIC_TRIE_HPP_INCLUDED_189RB8O5DJ6Y7IQ9LERVJ7B4V

#include "util/fixed_string.hpp"
#include "util/type_traits.hpp"
#include "util/u8compat.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <concepts>
#include <iterator>
#include <ranges>
#include <span>
#include <string_view>
#include <type_traits>
#include <utility>

namespace util::static_trie {

// Static trie configuration ///////////////////////////////////////////////////

/** @brief A concept for a valid static trie.
 *
 * Matches iff the type is:
 * - void; or
 * - a move-constructible value type; or
 * - a reference type (which is always move-constructible). */
template<typename State>
concept state = std::same_as<State, void>
  || std::is_move_constructible_v<State>;

namespace detail_ {

/** @brief A helper type to store a reference to the state, if one exists. */
template<state State>
struct state_ref {
  State&& state;

  constexpr state_ref(State&& state) noexcept:
    state{std::forward<State>(state)} {}
  constexpr state_ref(state_ref const& other) noexcept:
    state{std::forward<State>(other.state)} {}
  constexpr state_ref(state_ref&& other) noexcept: state_ref{other} {}
  state_ref& operator=(state_ref const& other) = delete;
  state_ref& operator=(state_ref&&) = delete;
  ~state_ref() noexcept = default;
};
template<>
struct state_ref<void> {};

/** @brief A metafunction that determines the return type of the given fail
 * handler type's basic hook.
 *
 * This metafunction is SFINAE-friendly: if the hook does not exist, the
 * metafunction does not have a `type` member type. This allows it to be used as
 * a detector for the hook. */
template<typename FailHandler, state State>
struct fail_handler_basic_hook {};

template<typename FailHandler, state State>
requires requires { FailHandler::fail(std::declval<State&&>()); }
struct fail_handler_basic_hook<FailHandler, State> {
  using type = decltype(FailHandler::fail(std::declval<State&&>()));
};

template<typename FailHandler>
requires requires { FailHandler::fail(); }
struct fail_handler_basic_hook<FailHandler, void> {
  using type = decltype(FailHandler::fail());
};

template<typename FailHandler, typename State>
using fail_handler_basic_hook_t
  = fail_handler_basic_hook<FailHandler, State>::type;

/** @brief A metafunction that determines the return type of the given fail
 * handler type's advanced hook.
 *
 * This metafunction is SFINAE-friendly: if the hook does not exist, the
 * metafunction does not have a `type` member type. This allows it to be used as
 * a detector for the hook. */
template<typename FailHandler, typename State, typename = void>
struct fail_handler_adv_hook {};

template<typename FailHandler, state State>
requires requires { FailHandler::fail(std::declval<State&&>(),
  std::span<std::u8string_view const>{}); }
struct fail_handler_adv_hook<FailHandler, State> {
  using type = decltype(FailHandler::fail(std::declval<State&&>(),
    std::declval<std::span<std::u8string_view const>>()));
};

template<typename FailHandler>
requires requires { FailHandler::fail(std::span<std::u8string_view const>{}); }
struct fail_handler_adv_hook<FailHandler, void> {
  using type = decltype(FailHandler::fail(
    std::declval<std::span<std::u8string_view const>>()));
};

template<typename FailHandler, typename State>
using fail_handler_adv_hook_t = fail_handler_adv_hook<FailHandler, State>::type;

} // detail_

/** @brief A metafunction that checks if the given type is a valid fail handler
 * for the given state.
 *
 * Produces true iff the type has either a basic or an advanced fail handler
 * hook. */
template<typename FailHandler, typename State>
concept fail_handler = state<State>
  && (requires {
    typename detail_::fail_handler_basic_hook_t<FailHandler, State>;
  } || requires {
    typename detail_::fail_handler_adv_hook_t<FailHandler, State>;
  });

namespace detail_ {

/** @brief A helper concept to check if the fail handler has an advanced hook.
 */
template<typename FailHandler, typename State>
concept has_adv_hook = fail_handler<FailHandler, State>
  && requires {
    typename detail_::fail_handler_adv_hook_t<FailHandler, State>;
  };

} // detail_

/** @brief A metafunction that determines the given fail handler's result type.
 *
 * This metafunction is SFINAE-friendly: if the type is not a fail handler, the
 * `type` member type does not exist. */
template<typename FailHandler, state State>
struct fail_handler_result {};
template<typename FailHandler, state State>
using fail_handler_result_t = fail_handler_result<FailHandler, State>::type;

template<typename FailHandler, state State>
requires detail_::has_adv_hook<FailHandler, State>
struct fail_handler_result<FailHandler, State> {
  using type = detail_::fail_handler_adv_hook_t<FailHandler, State>;
};

template<typename FailHandler, state State>
requires (!detail_::has_adv_hook<FailHandler, State>) && requires {
  typename detail_::fail_handler_basic_hook_t<FailHandler, State>;
}
struct fail_handler_result<FailHandler, State> {
  using type = detail_::fail_handler_basic_hook_t<FailHandler, State>;
};

namespace detail_ {

template<state State, fail_handler<State> FailHandler>
static constexpr bool fail_handler_noexcept() noexcept {
  if constexpr(has_adv_hook<FailHandler, State>) {
    if constexpr(std::same_as<void, State>) {
      return noexcept(FailHandler::fail(
        std::declval<std::span<std::u8string_view const>>()));
    } else {
      return noexcept(FailHandler::fail(std::declval<State>(),
        std::declval<std::span<std::u8string_view const>>()));
    }
  } else {
    if constexpr(std::same_as<void, State>) {
      return noexcept(FailHandler::fail());
    } else {
      return noexcept(FailHandler::fail(std::declval<State>()));
    }
  }
}

} // detail_

/** @brief A metafunction that determines the return type of the given entry
 * handler's match hook.
 *
 * This metafunction is SFINAE-friendly: if the hook does not exist, the
 * metafunction does not have a `type` member type. This allows it to be used as
 * a detector for the hook. */
template<typename Handler, state State>
struct entry_handler_result {};

template<typename Handler, state State>
requires requires { Handler::match(std::declval<State&&>()); }
struct entry_handler_result<Handler, State> {
  using type = decltype(Handler::match(std::declval<State&&>()));
};

template<typename Handler>
requires requires { Handler::match(); }
struct entry_handler_result<Handler, void> {
  using type = decltype(Handler::match());
};

template<typename Handler, state State>
using entry_handler_result_t = entry_handler_result<Handler, State>::type;

/** @brief A metafunction that determines the return type of the given entry
 * handler's partial match hook.
 *
 * This metafunction is SFINAE-friendly: if the hook does not exist, the
 * metafunction does not have a `type` member type. This allows it to be used as
 * a detector for the hook. */
template<typename Handler, state State>
struct entry_handler_partial_match_result {};

template<typename Handler, state State>
requires requires { Handler::partial_match(std::declval<State&&>()); }
struct entry_handler_partial_match_result<Handler, State> {
  using type = decltype(Handler::partial_match(std::declval<State&&>()));
};

template<typename Handler>
requires requires { Handler::partial_match(); }
struct entry_handler_partial_match_result<Handler, void> {
  using type = decltype(Handler::partial_match());
};

template<typename Handler, state State>
using entry_handler_partial_match_result_t
  = entry_handler_partial_match_result<Handler, State>::type;

/** @brief A metafunction that determines the return type of the given entry
 * handler's prefix match hook.
 *
 * This metafunction is SFINAE-friendly: if the hook does not exist, the
 * metafunction does not have a `type` member type. This allows it to be used as
 * a detector for the hook. */
template<typename Handler, state State, std::input_iterator Iterator>
requires std::same_as<std::iter_value_t<Iterator>, char8_t>
struct entry_handler_prefix_match_result {};

template<typename Handler, state State, std::input_iterator Iterator>
requires std::same_as<std::iter_value_t<Iterator>, char8_t> && requires {
  Handler::prefix_match(std::declval<State&&>(), std::declval<Iterator>());
}
struct entry_handler_prefix_match_result<Handler, State, Iterator> {
  using type = decltype(Handler::prefix_match(std::declval<State&&>(),
    std::declval<Iterator>()));
};

template<typename Handler, std::input_iterator Iterator>
requires std::same_as<std::iter_value_t<Iterator>, char8_t> && requires {
  Handler::prefix_match(std::declval<Iterator>());
}
struct entry_handler_prefix_match_result<Handler, void, Iterator> {
  using type = decltype(Handler::prefix_match(std::declval<Iterator>()));
};

template<typename Handler, state State, std::input_iterator Iterator>
requires std::same_as<std::iter_value_t<Iterator>, char8_t>
using entry_handler_prefix_match_result_t
  = entry_handler_prefix_match_result<Handler, State, Iterator>::type;

/** @brief A metafunction to check if the given type is a valid entry handler.
 *
 * Produces true iff:
 * - the type has the entry handler hook; and either
 *   - the type has no partial match hook; or
 *   - the partial match hook has the same return type as the main hook. */
template<typename Handler, typename State>
concept entry_handler = state<State> && requires {
  typename entry_handler_result_t<Handler, State>;
} && (!requires {
  typename entry_handler_partial_match_result_t<Handler, State>;
} || std::same_as<
  entry_handler_partial_match_result_t<Handler, State>,
  entry_handler_result_t<Handler, State>
>);

namespace detail_ {

template<typename Handler, typename State>
concept has_partial_match = entry_handler<Handler, State>
  && requires {
    typename entry_handler_partial_match_result_t<Handler, State>;
  };

template<typename Handler, typename State, typename Iterator>
concept has_prefix_match = entry_handler<Handler, State>
  && std::input_iterator<Iterator>
  && std::same_as<std::iter_value_t<Iterator>, char8_t>
  && requires {
    typename entry_handler_prefix_match_result_t<Handler, State, Iterator>;
  };

} // detail

/** @brief A helper type passed to the trie building metacode to indicate a trie
 * entry. */
template<fixed_string Key, typename Handler>
struct entry {
  using handler = Handler;
};

namespace detail_ {

/** @brief A metafunction that determines whether the given key is used by any
 * of the given entries. */
template<fixed_string Key, typename... Entries>
struct is_key_present;
template<fixed_string Key, typename... Entries>
constexpr bool is_key_present_v{is_key_present<Key, Entries...>::value};
template<fixed_string Key>
struct is_key_present<Key>: std::false_type {};
template<fixed_string Key, typename Handler, typename... Entries>
struct is_key_present<Key, entry<Key, Handler>, Entries...>: std::true_type {};
template<fixed_string Key, typename Entry, typename... Entries>
struct is_key_present<Key, Entry, Entries...>:
  is_key_present<Key, Entries...> {};

/** @brief A metafunction that determines whether all the given entries have
 * distinct keys. */
template<typename... Entries>
struct keys_are_distinct;
template<typename... Entries>
constexpr bool keys_are_distinct_v{keys_are_distinct<Entries...>::value};
template<>
struct keys_are_distinct<>: std::true_type {};
template<fixed_string Key, typename Handler, typename... Entries>
struct keys_are_distinct<entry<Key, Handler>, Entries...>: std::bool_constant<
  !is_key_present_v<Key, Entries...>
  && keys_are_distinct_v<Entries...>
> {};

// Static trie nodes ///////////////////////////////////////////////////////////

/** @brief A combination of a key list and an offset.
 *
 * This is used as a template parameter for trie nodes to indicate a list of
 * keys of entries in this subtrie. This is used to construct the span to pass
 * to the fail handler's advanced hook. The intent is that the key list is the
 * same type for all nodes in a trie, to ensure a single allocation of the key
 * array.
 *
 * Instead of a key list reference, the supplied type can be void. During trie
 * construction, the parameter is void, until after the third, optional, marking
 * stage, which associates the nodes with their associated key list references.
 */
template<typename KeyList, std::size_t I>
struct key_list_ref {
  static constexpr std::span<std::u8string_view const> to_span(std::size_t size)
      noexcept {
    return {KeyList::keys + I, size};
  }
};

/** @brief A metafunction to add an offset to a key list reference's index.
 *
 * If given void, produces void. */
template<typename KLRef, std::size_t Off>
struct klref_add;
template<typename KLRef, std::size_t Off>
using klref_add_t = klref_add<KLRef, Off>::type;
template<std::size_t Off>
struct klref_add<void, Off> {
  using type = void;
};
template<typename KeyList, std::size_t I, std::size_t Off>
struct klref_add<key_list_ref<KeyList, I>, Off> {
  using type = key_list_ref<KeyList, I + Off>;
};

/** @brief A metafunction to subtract an offset from a key list reference's
 * index.
 *
 * If given void, produces void. */
template<typename KLRef, std::size_t Off>
struct klref_sub;
template<typename KLRef, std::size_t Off>
using klref_sub_t = klref_sub<KLRef, Off>::type;
template<std::size_t Off>
struct klref_sub<void, Off> {
  using type = void;
};
template<typename KeyList, std::size_t I, std::size_t Off>
struct klref_sub<key_list_ref<KeyList, I>, Off> {
  using type = key_list_ref<KeyList, I - Off>;
};

/** @brief A basic node of the trie. */
template<typename KLRef, typename... Edges>
struct node;

/** @brief A node that corresponds to an entry. */
template<typename KLRef, typename Handler, typename... Edges>
struct node_accept;

/** @brief A collapsed node that represents a chain of nodes, each with exactly
 * one outgoing edge. */
template<typename KLRef, fixed_string Run, typename NextNode>
struct node_collapsed;

/** @brief An edge going out of a node. */
template<char8_t C, typename Node>
struct edge {
  static constexpr char8_t ch{C};
  using node = Node;
};

template<typename KLRef>
struct node<KLRef> {
  static constexpr std::size_t num_entries{0};

  template<state State, fail_handler<State> FailHandler>
  static constexpr decltype(auto) fail(state_ref<State> state)
      noexcept(fail_handler_noexcept<State, FailHandler>()) {
    if constexpr(has_adv_hook<FailHandler, State>) {
      if constexpr(std::same_as<void, State>) {
        return FailHandler::fail(std::span<std::u8string_view const>{});
      } else {
        return FailHandler::fail(std::forward<State>(state.state),
          std::span<std::u8string_view const>{});
      }
    } else {
      if constexpr(std::same_as<void, State>) {
        return FailHandler::fail();
      } else {
        return FailHandler::fail(std::forward<State>(state.state));
      }
    }
  }

  // A partial match function here is unnecessary: it can only be invoked from a
  // `node_collapsed`, but a node with no edges can only appear at the root of
  // an empty trie.

  template<
    state State,
    fail_handler<State> FailHandler,
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel
  >
  requires std::same_as<std::iter_value_t<Iterator>, char8_t>
  static constexpr decltype(auto) lookup_impl(state_ref<State> state,
      Iterator, Sentinel)
      noexcept(fail_handler_noexcept<State, FailHandler>()) {
    return node::fail<State, FailHandler>(state);
  }

  template<
    state State,
    fail_handler<State> FailHandler,
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel
  >
  requires std::same_as<std::iter_value_t<Iterator>, char8_t>
  static constexpr decltype(auto) lookup(state_ref<State> state,
      Iterator, Sentinel)
      noexcept(fail_handler_noexcept<State, FailHandler>()) {
    // Since this function is only called on a "complete" node (a node that
    // actually occured in the trie, without removed edges), and a node with no
    // outgoing edges can only occur as the root of an empty trie, then that is
    // the only situation in which this can actually be called.
    return node::fail<State, FailHandler>(state);
  }
};

template<typename KLRef, typename Edge, typename... Edges>
struct node<KLRef, Edge, Edges...> {
  static constexpr std::size_t num_entries{(Edge::node::num_entries
    + ... + Edges::node::num_entries)};

  template<state State, fail_handler<State> FailHandler>
  static constexpr bool partial_match_noexcept() noexcept {
    if constexpr(sizeof...(Edges) == 0) {
      return noexcept(Edge::node::template partial_match<State, FailHandler>(
        std::declval<state_ref<State>>()));
    } else {
      return fail_handler_noexcept<State, FailHandler>();
    }
  }

  template<state State, fail_handler<State> FailHandler>
  static constexpr decltype(auto) partial_match(state_ref<State> state)
      noexcept(partial_match_noexcept<State, FailHandler>()) {
    if constexpr(sizeof...(Edges) == 0) {
      return Edge::node::template partial_match<State, FailHandler>(state);
    } else {
      if constexpr(has_adv_hook<FailHandler, State>) {
        if constexpr(std::same_as<void, State>) {
          return FailHandler::fail(KLRef::to_span(num_entries));
        } else {
          return FailHandler::fail(std::forward<State>(state.state),
            KLRef::to_span(num_entries));
        }
      } else {
        if constexpr(std::same_as<void, State>) {
          return FailHandler::fail();
        } else {
          return FailHandler::fail(std::forward<State>(state.state));
        }
      }
    }
  }

  template<
    state State,
    fail_handler<State> FailHandler,
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel
  >
  requires std::same_as<std::iter_value_t<Iterator>, char8_t>
  static constexpr decltype(auto) lookup_impl(state_ref<State> state,
      Iterator it, Sentinel iend) noexcept(noexcept(*it) && noexcept(++it)
      && noexcept(Edge::node::template lookup<State, FailHandler>(state,
      std::move(it), std::move(iend)))
      && noexcept(node<klref_add_t<KLRef, Edge::node::num_entries>, Edges...>
      ::template lookup_impl<State, FailHandler>(
      state, std::move(it), std::move(iend)))) {
    assert_if_noexcept(it != iend);
    if(*it == Edge::ch) {
      ++it;
      return Edge::node::template lookup<State, FailHandler>(state,
        std::move(it), std::move(iend));
    } else {
      return node<klref_add_t<KLRef, Edge::node::num_entries>, Edges...>
        ::template lookup_impl<State, FailHandler>(
        state, std::move(it), std::move(iend));
    }
  }

  template<
    state State,
    fail_handler<State> FailHandler,
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel
  >
  requires std::same_as<std::iter_value_t<Iterator>, char8_t>
  static constexpr decltype(auto) lookup(state_ref<State> state,
      Iterator it, Sentinel iend) noexcept(noexcept(it == iend)
      && fail_handler_noexcept<State, FailHandler>()
      && noexcept(node::lookup_impl<State, FailHandler>(state,
      std::move(it), std::move(iend)))) {
    if(it == iend) {
      // This cannot be a partial match: for a partial match, the node needs to
      // have one outgoing edge, but such nodes are converted to
      // `node_collapsed` during optimization.
      if constexpr(has_adv_hook<FailHandler, State>) {
        if constexpr(std::same_as<void, State>) {
          return FailHandler::fail(KLRef::to_span(num_entries));
        } else {
          return FailHandler::fail(std::forward<State>(state.state),
            KLRef::to_span(num_entries));
        }
      } else {
        if constexpr(std::same_as<void, State>) {
          return FailHandler::fail();
        } else {
          return FailHandler::fail(std::forward<State>(state.state));
        }
      }
    } else {
      return node::lookup_impl<State, FailHandler>(state,
        std::move(it), std::move(iend));
    }
  }
};

template<typename KLRef, typename Handler, typename... Edges>
struct node_accept {
  static constexpr std::size_t num_entries{(1
    + ... + Edges::node::num_entries)};

  template<state State, fail_handler<State> FailHandler>
  [[nodiscard]] static constexpr bool partial_match_noexcept() noexcept {
    if constexpr(sizeof...(Edges) == 0 && has_partial_match<Handler, State>) {
      if constexpr(std::same_as<void, State>) {
        return noexcept(Handler::partial_match());
      } else {
        return noexcept(Handler::partial_match(std::declval<State>()));
      }
    } else {
      return fail_handler_noexcept<State, FailHandler>();
    }
  }

  template<state State, fail_handler<State> FailHandler>
  static constexpr decltype(auto) partial_match(state_ref<State> state)
      noexcept(partial_match_noexcept<State, FailHandler>()) {
    if constexpr(sizeof...(Edges) == 0 && has_partial_match<Handler, State>) {
      if constexpr(std::same_as<void, State>) {
        return Handler::partial_match();
      } else {
        return Handler::partial_match(std::forward<State>(state.state));
      }
    } else {
      if constexpr(has_adv_hook<FailHandler, State>) {
        if constexpr(std::same_as<void, State>) {
          return FailHandler::fail(KLRef::to_span(num_entries));
        } else {
          return FailHandler::fail(std::forward<State>(state.state),
            KLRef::to_span(num_entries));
        }
      } else {
        if constexpr(std::same_as<void, State>) {
          return FailHandler::fail();
        } else {
          return FailHandler::fail(std::forward<State>(state.state));
        }
      }
    }
  }

  template<
    state State,
    fail_handler<State> FailHandler,
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel
  >
  requires entry_handler<Handler, State>
    && std::same_as<std::iter_value_t<Iterator>, char8_t>
  [[nodiscard]] static constexpr bool match_noexcept() noexcept {
    bool match_hook_noexcept{false};
    if constexpr(std::same_as<void, State>) {
      match_hook_noexcept = noexcept(Handler::match());
    } else {
      match_hook_noexcept = noexcept(Handler::match(std::declval<State>()));
    }

    bool next_noexcept{noexcept(
      node<klref_add_t<KLRef, 1>, Edges...>
      ::template lookup<State, FailHandler>(
      std::declval<state_ref<State>>(), std::declval<Iterator>(),
      std::declval<Sentinel>())
    )};

    if constexpr(has_prefix_match<Handler, State, Iterator>) {
      if constexpr(std::same_as<
        entry_handler_prefix_match_result_t<Handler, State, Iterator>,
        entry_handler_result_t<Handler, State>
      >) {
        if constexpr(std::same_as<void, State>) {
          next_noexcept = noexcept(Handler::prefix_match(
            std::declval<Iterator>()));
        } else {
          next_noexcept = noexcept(Handler::prefix_match(std::declval<State>(),
            std::declval<Iterator>()));
        }
      }
    }

    return match_hook_noexcept && next_noexcept;
  }

  template<
    state State,
    fail_handler<State> FailHandler,
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel
  >
  requires entry_handler<Handler, State>
    && std::same_as<std::iter_value_t<Iterator>, char8_t>
  static constexpr decltype(auto) lookup(state_ref<State> state,
      Iterator it, Sentinel iend) noexcept(noexcept(it == iend)
      && match_noexcept<State, FailHandler, Iterator, Sentinel>()) {
    if(it == iend) {
      if constexpr(std::same_as<void, State>) {
        return Handler::match();
      } else {
        return Handler::match(std::forward<State>(state.state));
      }
    }

    if constexpr(has_prefix_match<Handler, State, Iterator>) {
      if constexpr(std::same_as<
        entry_handler_prefix_match_result_t<Handler, State, Iterator>,
        entry_handler_result_t<Handler, State>
      >) {
        if constexpr(std::same_as<void, State>) {
          return Handler::prefix_match(std::move(it));
        } else {
          return Handler::prefix_match(std::forward<State>(state.state),
            std::move(it));
        }
      }
    }

    return node<klref_add_t<KLRef, 1>, Edges...>
      ::template lookup<State, FailHandler>(
      state, std::move(it), std::move(iend));
  }
};

template<typename KLRef, fixed_string Run, typename NextNode>
struct node_collapsed {
  static constexpr std::size_t num_entries{NextNode::num_entries};

  // A partial match function here is unnecessary: it can only be invoked from a
  // `node_collapsed`, but its child cannot be another `node_collapsed`.

  template<
    state State,
    fail_handler<State> FailHandler,
    std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel
  >
  requires std::same_as<std::iter_value_t<Iterator>, char8_t>
  static constexpr decltype(auto) lookup(state_ref<State> state,
      Iterator it, Sentinel iend) noexcept(noexcept(++it)
      && noexcept(it == iend) && noexcept(*it != std::declval<char8_t&>())
      && noexcept(NextNode::template partial_match<State, FailHandler>(state))
      && fail_handler_noexcept<State, FailHandler>()
      && noexcept(NextNode::template lookup<State, FailHandler>(state,
      std::move(it), std::move(iend)))) {
    for(std::size_t i{0}; i < Run.size(); ++i, ++it) {
      if(it == iend) {
        return NextNode::template partial_match<State, FailHandler>(state);
      }
      if(*it != util::fixed_string_buf<Run>[i]) {
        if constexpr(has_adv_hook<FailHandler, State>) {
          if constexpr(std::same_as<void, State>) {
            return FailHandler::fail(std::span<std::u8string_view const>{});
          } else {
            return FailHandler::fail(std::forward<State>(state.state),
              std::span<std::u8string_view const>{});
          }
        } else {
          if constexpr(std::same_as<void, State>) {
            return FailHandler::fail();
          } else {
            return FailHandler::fail(std::forward<State>(state.state));
          }
        }
      }
    }
    return NextNode::template lookup<State, FailHandler>(state,
      std::move(it), std::move(iend));
  }
};

// Static trie construction ////////////////////////////////////////////////////

/** @brief A helper type for temporarily stashing away trie edges while adding
 * a new entry to a trie. */
template<typename... Edges>
struct edge_stash {};

/** @brief A metafunction which adds a entry to a trie.
 * @tparam Node The root of the trie to modify.
 * @tparam Entry The entry to add.
 * @tparam Stash Optionally, a pack of edges (in an edge_stash specialization)
 *               that have been temporarily stashed away and need to be added
 *               to the node after it is modified. */
template<typename Node, typename Entry, typename Stash = edge_stash<>>
struct add_entry_to_trie;
template<typename Node, typename Entry, typename Stash = edge_stash<>>
using add_entry_to_trie_t = add_entry_to_trie<Node, Entry, Stash>::type;

// To add a entry for an empty string, convert a non-accepting node into an
// accepting one. (If the node is already accepting, this indicates a duplicate
// string, so this specialization is deliberately omitted.)
template<typename... Edges, typename Handler, typename... Stash>
struct add_entry_to_trie<node<void, Edges...>,
    entry<u8"", Handler>, edge_stash<Stash...>> {
  using type = node_accept<void, Handler, Stash..., Edges...>;
};

// If we find an edge labeled with the entry's first character, recurse into
// the node it points to.
template<typename NextNode, typename... Edges,
  typename Handler, char8_t C, fixed_string S, typename... Stash>
requires (C == S[0])
struct add_entry_to_trie<
  node<void, edge<C, NextNode>, Edges...>,
  entry<S, Handler>,
  edge_stash<Stash...>
> {
  using type = node<
    void,
    Stash...,
    edge<S[0], add_entry_to_trie_t<NextNode, entry<S.tail(), Handler>>>,
    Edges...
  >;
};

// Same for accepting nodes.
template<typename OldHandler, typename NextNode, typename... Edges,
  typename Handler, char8_t C, fixed_string S, typename... Stash>
requires (C == S[0])
struct add_entry_to_trie<
  node_accept<void, OldHandler, edge<C, NextNode>, Edges...>,
  entry<S, Handler>,
  edge_stash<Stash...>
> {
  using type = node_accept<
    void,
    OldHandler,
    Stash...,
    edge<S[0], add_entry_to_trie_t<NextNode, entry<S.tail(), Handler>>>,
    Edges...
  >;
};

// If the edge is labeled with a different character, stash it away and proceed
// to the next edge.
template<typename Edge, typename... Edges, typename Handler, fixed_string S,
  typename... Stash>
requires (S.size() >= 1)
struct add_entry_to_trie<
  node<void, Edge, Edges...>,
  entry<S, Handler>,
  edge_stash<Stash...>
> {
  using type = add_entry_to_trie_t<node<void, Edges...>,
    entry<S, Handler>, edge_stash<Stash..., Edge>>;
};

// Same for accepting nodes.
template<typename OldHandler, typename Edge, typename... Edges,
  typename NewHandler, fixed_string S, typename... Stash>
requires (S.size() >= 1)
struct add_entry_to_trie<
  node_accept<void, OldHandler, Edge, Edges...>,
  entry<S, NewHandler>,
  edge_stash<Stash...>
> {
  using type = add_entry_to_trie_t<node_accept<void, OldHandler, Edges...>,
    entry<S, NewHandler>, edge_stash<Stash..., Edge>>;
};

// If we ran out of edges, add a new one.
template<typename Handler, fixed_string S, typename... Stash>
requires (S.size() >= 1)
struct add_entry_to_trie<node<void>, entry<S, Handler>, edge_stash<Stash...>> {
  using type = node<void, Stash..., edge<S[0],
    add_entry_to_trie_t<node<void>, entry<S.tail(), Handler>>>>;
};

// Same for accepting nodes.
template<typename OldHandler, typename NewHandler, fixed_string S,
  typename... Stash>
requires (S.size() >= 1)
struct add_entry_to_trie<node_accept<void, OldHandler>,
    entry<S, NewHandler>, edge_stash<Stash...>> {
  using type = node_accept<void, OldHandler, Stash..., edge<S[0],
    add_entry_to_trie_t<node<void>, entry<S.tail(), NewHandler>>>>;
};

/** @brief A metafunction which converts a pack of entries into an unoptimized
 * static trie based on those entries.
 *
 * This should be fed into `optimize_trie` before using.
 *
 * Produces the trie's root node (a specialization of node or node_accept). */
template<typename... Entries>
struct build_basic_trie;
template<typename... Entries>
using build_basic_trie_t = build_basic_trie<Entries...>::type;

template<>
struct build_basic_trie<> {
  using type = node<void>;
};

template<typename Entry, typename... Entries>
struct build_basic_trie<Entry, Entries...> {
  using type = add_entry_to_trie_t<build_basic_trie_t<Entries...>, Entry>;
};

// Static trie optimization ////////////////////////////////////////////////////

/** @brief Creates a trie node containing the same entries as the given one,
 * but with an additional prefixed character.
 *
 * If the trie is rooted at a collapsed node, extends the node's common prefix
 * string. Otherwise, creates a new collapsed node. */
template<char8_t C, typename Node>
struct prepend_to_trie;
template<char8_t C, typename Node>
using prepend_to_trie_t = prepend_to_trie<C, Node>::type;

template<char8_t C, typename... Edges>
struct prepend_to_trie<C, node<void, Edges...>> {
  using type = node_collapsed<void, C, node<void, Edges...>>;
};

template<char8_t C, typename Handler, typename... Edges>
struct prepend_to_trie<C, node_accept<void, Handler, Edges...>> {
  using type = node_collapsed<void, C, node_accept<void, Handler, Edges...>>;
};

template<char8_t C, fixed_string S, typename NextNode>
struct prepend_to_trie<C, node_collapsed<void, S, NextNode>> {
  using type = node_collapsed<void, fixed_string{C} + S, NextNode>;
};

/** @brief Optimizes the trie by collapsing chains of non-branching edges into
 * single collapsed nodes. */
template<typename Node>
struct optimize_trie;
template<typename Node>
using optimize_trie_t = optimize_trie<Node>::type;

// By default, don't collapse anything, but recurse into all subtries.
template<char8_t... EdgeC, typename... NextNodes>
struct optimize_trie<node<void, edge<EdgeC, NextNodes>...>> {
  using type = node<void, edge<EdgeC, optimize_trie_t<NextNodes>>...>;
};

// Never collapse accepting nodes, but recurse into all subtries.
template<typename Handler, char8_t... EdgeC, typename... NextNodes>
struct optimize_trie<node_accept<void, Handler, edge<EdgeC, NextNodes>...>> {
  using type = node_accept<void, Handler,
    edge<EdgeC, optimize_trie_t<NextNodes>>...>;
};

// Collapse non-accepting nodes that only have one edge.
template<char8_t EdgeC, typename NextNode>
struct optimize_trie<node<void, edge<EdgeC, NextNode>>> {
  using type = prepend_to_trie_t<EdgeC, optimize_trie_t<NextNode>>;
};

// Static trie key lists ///////////////////////////////////////////////////////

/** @brief A helper type representing a list of keys in a subtrie.
 *
 * The order of the keys is significant: this is the order in which they are
 * laid out in the generated key array. */
template<fixed_string... Keys>
struct key_list {
  static constexpr std::u8string_view keys[sizeof...(Keys)]{
    fixed_string_buf<Keys>...};
};

/** @brief A metafunction that concatenates key lists. */
template<typename... KeyLists>
struct key_list_concat;
template<typename... KeyLists>
using key_list_concat_t = key_list_concat<KeyLists...>::type;
template<>
struct key_list_concat<> {
  using type = key_list<>;
};
template<typename KeyList>
struct key_list_concat<KeyList> {
  using type = KeyList;
};
template<fixed_string... Keys1, fixed_string... Keys2>
struct key_list_concat<key_list<Keys1...>, key_list<Keys2...>> {
  using type = key_list<Keys1..., Keys2...>;
};
template<typename KeyList1, typename KeyList2, typename KeyList3,
  typename... KeyLists>
struct key_list_concat<KeyList1, KeyList2, KeyList3, KeyLists...> {
  using type = key_list_concat_t<key_list_concat_t<KeyList1, KeyList2>,
    KeyList3, KeyLists...>;
};

/** @brief A metafunction that prepends the given string to every key in the
 * given key list. */
template<fixed_string Prefix, typename KeyList>
struct key_list_prepend;
template<fixed_string Prefix, typename KeyList>
using key_list_prepend_t = key_list_prepend<Prefix, KeyList>::type;
template<fixed_string Prefix, fixed_string... Keys>
struct key_list_prepend<Prefix, key_list<Keys...>> {
  using type = key_list<(Prefix + Keys)...>;
};

/** @brief A metafunction that produces a list of all keys in the given subtrie.
 */
template<typename Node>
struct key_list_of;
template<typename Node>
using key_list_of_t = key_list_of<Node>::type;

template<char8_t... Cs, typename... NextNodes>
struct key_list_of<node<void, edge<Cs, NextNodes>...>> {
  using type = key_list_concat_t<
    key_list_prepend_t<Cs, key_list_of_t<NextNodes>>...
  >;
};

template<typename Handler, char8_t... Cs, typename... NextNodes>
struct key_list_of<node_accept<void, Handler, edge<Cs, NextNodes>...>> {
  using type = key_list_concat_t<
    key_list<u8"">,
    key_list_prepend_t<Cs, key_list_of_t<NextNodes>>...
  >;
};

template<fixed_string Prefix, typename NextNode>
struct key_list_of<node_collapsed<void, Prefix, NextNode>> {
  using type = key_list_prepend_t<Prefix, key_list_of_t<NextNode>>;
};

// Static trie marking /////////////////////////////////////////////////////////

/** @brief A metafunction that attaches key list references to a completed trie.
 */
template<typename KLRef, typename Node, typename Stash = edge_stash<>>
struct mark_trie;
template<typename KLRef, typename Node, typename Stash = edge_stash<>>
using mark_trie_t = mark_trie<KLRef, Node, Stash>::type;

template<typename KLRef, typename... StashedEdges>
struct mark_trie<KLRef, node<void>, edge_stash<StashedEdges...>> {
  using type = node<
    klref_sub_t<KLRef, (0 + ... + StashedEdges::node::num_entries)>,
    StashedEdges...
  >;
};

template<typename KLRef, char8_t C, typename NextNode,
  typename... Edges, typename... StashedEdges>
struct mark_trie<
  KLRef,
  node<void, edge<C, NextNode>, Edges...>,
  edge_stash<StashedEdges...>
>: mark_trie<
  klref_add_t<KLRef, NextNode::num_entries>,
  node<void, Edges...>,
  edge_stash<StashedEdges..., edge<C, mark_trie_t<KLRef, NextNode>>>
> {};

template<typename KLRef, typename Handler, typename... StashedEdges>
struct mark_trie<KLRef, node_accept<void, Handler>,
    edge_stash<StashedEdges...>> {
  using type = node_accept<
    klref_sub_t<KLRef, (0 + ... + StashedEdges::node::num_entries)>,
    Handler,
    StashedEdges...
  >;
};

template<typename KLRef, typename Handler, char8_t C, typename NextNode,
  typename... Edges, typename... StashedEdges>
struct mark_trie<
  KLRef,
  node_accept<void, Handler, edge<C, NextNode>, Edges...>,
  edge_stash<StashedEdges...>
>: mark_trie<
  klref_add_t<KLRef, NextNode::num_entries>,
  node_accept<void, Handler, Edges...>,
  edge_stash<StashedEdges..., edge<C,
    mark_trie_t<klref_add_t<KLRef, 1>, NextNode>>>
> {};

template<typename KLRef, fixed_string Prefix, typename NextNode>
struct mark_trie<KLRef, node_collapsed<void, Prefix, NextNode>> {
  using type = node_collapsed<KLRef, Prefix, mark_trie_t<KLRef, NextNode>>;
};

// Static trie public interface ////////////////////////////////////////////////

/** @brief The class type used as the public interface.
 *
 * `RootNode` is the root node of the trie, which is one of `node`,
 * `node_accept`, or `node_collapsed`. The trie is constructed via three steps,
 * the last of which is optional:
 *
 * 1. Building the basic trie out of the trie entries. The basic trie is
 *    constructed out of `node` and `node_accept` types; there are no
 *    `node_collapsed` nodes yet.
 *
 *    At this stage, regular (non-accepting) nodes must have a non-empty list of
 *    edges. This means that any subtrie corresponds to a non-empty set of
 *    entries. The exception is the degenerate case of an empty trie, which is
 *    represented as a single empty `node`.
 *
 * 2. Optimizing the resulting trie. This involves finding chains of nodes where
 *    each node, except for the last, has exactly one child, which is the next
 *    node in the chain. Then each such chain is replaced with a
 *    `node_collapsed` node.
 *
 *    At this stage, regular (non-accepting) nodes must have at least two
 *    outgoing edges each. Any other node will have been collapsed into a
 *    `node_collapsed`.
 *
 * 3. Optionally, marking the trie with key list references. This is done iff
 *    the fail handler uses an advanced hook. Otherwise, the key list references
 *    stay at `void`, which is what they are during the first two steps.
 *
 * The node types implement the lookup process via a number of static member
 * functions:
 *
 * - `lookup`: Performs the lookup in the subtrie. In `node` and `node_accept`,
 *   this checks whether the input is over, and dispatches to `lookup_impl` or
 *   `partial_match`, or to various handler hooks. In `node_collapsed`, this
 *   dispatches to `lookup` or `partial_match` on the child node.
 *
 * - `lookup_impl`: In a `node`, is used to loop over outgoing edges. This is
 *   called on node types that are not actually present in the actual trie,
 *   gradually stripping edges until a match is found or no edges are left.
 *
 * - `partial_match`: Called if we've run out of input, but there's a
 *   possibility that there's a single entry that we can match. This will call
 *   either a partial match hook on an entry handler or a fail handler hook. */
template<state State, fail_handler<State> FailHandler, typename RootNode>
struct trie_interface {
  using marked_root_node = std::conditional_t<
    has_adv_hook<FailHandler, State>,
    mark_trie_t<key_list_ref<key_list_of_t<RootNode>, 0>, RootNode>,
    RootNode
  >;

  template<typename Range>
  requires std::ranges::input_range<Range&&>
    && std::same_as<std::ranges::range_value_t<Range&&>, char8_t>
  static constexpr decltype(auto) lookup(State&& state, Range&& input)
      noexcept(noexcept(marked_root_node::template lookup<State, FailHandler>(
      state_ref<State>{std::forward<State>(state)},
      std::ranges::begin(input), std::ranges::end(input)))) {
    return marked_root_node::template lookup<State, FailHandler>(
      state_ref<State>{std::forward<State>(state)},
      std::ranges::begin(input), std::ranges::end(input));
  }
};

template<fail_handler<void> FailHandler, typename RootNode>
struct trie_interface<void, FailHandler, RootNode> {
  using marked_root_node = std::conditional_t<
    has_adv_hook<FailHandler, void>,
    mark_trie_t<key_list_ref<key_list_of_t<RootNode>, 0>, RootNode>,
    RootNode
  >;

  template<typename Range>
  requires std::ranges::input_range<Range&&>
    && std::same_as<std::ranges::range_value_t<Range&&>, char8_t>
  static constexpr decltype(auto) lookup(Range&& input)
      noexcept(noexcept(marked_root_node::template lookup<void, FailHandler>(
      state_ref<void>{}, std::ranges::begin(input), std::ranges::end(input)))) {
    return marked_root_node::template lookup<void, FailHandler>(
      state_ref<void>{}, std::ranges::begin(input), std::ranges::end(input));
  }
};

} // detail_

/** @brief A metafunction which produces a static trie type. The main public
 * interface for the static trie functionality.
 *
 * The resulting type encodes the static trie and has a static member function
 * that can be used to perform lookup. The trie is encoded in the data type; no
 * objects are actually created. The behavior upon a match is customized by
 * providing various handler types, which must implement public static hook
 * member functions.
 *
 * The first argument is the state type, which is passed to all handler hooks
 * and carries arbitrary information that those hooks may need access to. It
 * must be void, or a move-constructible value type, or a reference type.
 *
 * The second argument is the fail handler type, which contains a hook to be
 * called in the event of a failed match. There are two kinds of fail handler
 * hooks: basic and advanced. (If both are present, the advanced hook takes
 * priority.) The advanced hook is described later. The basic hook is called
 * `fail` and accepts the state as its only argument, or if the state type is
 * void, accepts no arguments.
 *
 * The remaining arguments are trie entries, which must be specializations of
 * the `entry` template. The first argument to that template is a string naming
 * the entry's key (all of which must be distinct, obviously). The second
 * argument is the entry handler, which contains hooks to be called in the event
 * of a match.
 *
 * The main hook in an entry handler is a match hook, which is called `match`
 * and accepts the state as its only argument, or if the state type is void,
 * accepts no arguments. It is called when the lookup input matches the entry's
 * key exactly.
 *
 * Optionally, an entry handler can have a partial match hook, called
 * `partial_match`. It likewise accepts the state as its only argument, or if
 * the state type is void, accepts no arguments. It is called when the lookup
 * input does not exactly match any entry's key, but is a prefix of exactly one
 * entry's key. If this hook does not exist, then the fail handler's hook is
 * called, as usual.
 *
 * If the input does not exactly match any entry's key, it may be useful to
 * determine all the keys for which the input is a prefix. For that, an advanced
 * fail handler hook can be used. It has the same name and signature as the
 * basic hook, except that it accepts one additional argument of type
 * `std::span<std::u8string_view const>`, which is passed a range of entry keys
 * that are partially matched by the input. Both the range and the strings are
 * immutable and are statically allocated.
 *
 * An entry handler can also have a prefix match hook, called `prefix_match`. It
 * accepts two arguments: the state and an iterator to the input range, or if
 * the state type is void, it accepts one argument: just the iterator. If the
 * handler has a prefix match hook for the iterator type of the supplied range
 * and the hook has the same return type as the main match hook, it is called if
 * the entry's key matches a prefix of the input. If there are multiple entries
 * that match this condition, the one with the shortest key is chosen. (Note
 * that whether the prefix match hook exists depends on the range type that is
 * supplied as input.)
 *
 * The trie type itself has a single public static member function: `lookup`. It
 * accepts two arguments: the state and a range of chars, or if the state type
 * is void, it accepts the range of chars as the only argument. The range is
 * used as the lookup key; the function performs the lookup and calls one of the
 * handler hook functions.
 *
 * The return types of all handler hooks (the fail handler hook, the match entry
 * hooks, and, if present, the partial match entry hooks) must all be the same;
 * then the return type of the `lookup` interface function is also that type,
 * and it returns whatever is returned by the called hook function. */
template<state State, fail_handler<State> FailHandler, typename... Entries>
struct trie {
  // static_assert((... && specialization_of<Entries, entry>));
  static_assert((... && entry_handler<typename Entries::handler, State>));

  static_assert(detail_::keys_are_distinct_v<Entries...>,
    "static trie entries must have distinct keys");

  static_assert((... && std::same_as<
    fail_handler_result_t<FailHandler, State>,
    entry_handler_result_t<typename Entries::handler, State>
  >),
    "all static trie handler return types (including the fail handler return"
    " type) must be the same");

  using type = detail_::trie_interface<State, FailHandler,
    detail_::optimize_trie_t<detail_::build_basic_trie_t<Entries...>>>;
};
template<typename State, typename FailHandler, typename... Entries>
using trie_t = trie<State, FailHandler, Entries...>::type;

} // util::static_trie

#endif
