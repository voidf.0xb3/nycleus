#ifndef UTIL_OVERFLOW_HPP_INCLUDED_3E1JZVE558UHOLNKAYAJ0653E
#define UTIL_OVERFLOW_HPP_INCLUDED_3E1JZVE558UHOLNKAYAJ0653E

#include "util/safe_int.hpp"
#include "util/util.hpp"
#include <cassert>
#include <concepts>
#include <limits>
#include <optional>
#include <stdexcept>
#include <type_traits>

namespace util {

// Checked operations /////////////////////////////////////////////////////////////////////////

namespace detail_ {

/** @brief Adds two numbers, returning nothing on overflow. */
template<true_unsigned_integer T>
[[nodiscard]] constexpr std::optional<T> checked_add_impl(T a, T b) noexcept {
  if(auto const result{safe_int<T>::wrap(a).checked_add(safe_int<T>::wrap(b))}) {
    return result->unwrap();
  } else {
    return std::nullopt;
  }
}

/** @brief Multiplies two numbers, returning nothing on overflow. */
template<true_unsigned_integer T>
[[nodiscard]] constexpr std::optional<T> checked_mul_impl(T a, T b) noexcept {
  if(auto const result{safe_int<T>::wrap(a).checked_mul(safe_int<T>::wrap(b))}) {
    return result->unwrap();
  } else {
    return std::nullopt;
  }
}

template<typename T, typename U>
using overflow_result_t = std::conditional_t<std::same_as<T, void>, U, T>;

} // detail_

/** @brief Adds one or more numbers, returning nothing on overflow.
 *
 * The first template argument determines the result type. All function arguments are converted
 * to that type, and addition is done in that type.
 *
 * If the first template argument is void (which it is by default), all function arguments must
 * be of the same type, and that is the result type. */
template<typename T = void, true_unsigned_integer U, true_unsigned_integer... Us>
requires true_unsigned_integer<T> || (std::same_as<T, void> && ... && std::same_as<U, Us>)
[[nodiscard]] constexpr std::optional<detail_::overflow_result_t<T, U>>
checked_add(U op, Us... ops) noexcept {
  if constexpr(std::same_as<T, void>) {
    return checked_add<U>(op, ops...);
  } else {
    if constexpr(sizeof...(Us) == 0) {
      return std::optional<T>{op};
    } else {
      std::optional<T> const partial_result{checked_add<T>(ops...)};
      if(partial_result) {
        return detail_::checked_add_impl<T>(op, *partial_result);
      } else {
        return std::optional<T>{};
      }
    }
  }
}

/** @brief Multiplies one or more numbers, returning nothing on overflow.
 *
 * The first template argument determines the result type. All function arguments are converted
 * to that type, and addition is done in that type.
 *
 * If the first template argument is void (which it is by default), all function arguments must
 * be of the same type, and that is the result type. */
template<typename T = void, true_unsigned_integer U, true_unsigned_integer... Us>
requires true_unsigned_integer<T> || (std::same_as<T, void> && ... && std::same_as<U, Us>)
[[nodiscard]] constexpr std::optional<detail_::overflow_result_t<T, U>>
checked_mul(U op, Us... ops) noexcept {
  if constexpr(std::same_as<T, void>) {
    return checked_mul<U>(op, ops...);
  } else {
    if constexpr(sizeof...(Us) == 0) {
      return std::optional<T>{op};
    } else {
      std::optional<T> const partial_result{checked_mul<T>(ops...)};
      if(partial_result) {
        return detail_::checked_mul_impl<T>(op, *partial_result);
      } else {
        return std::optional<T>{};
      }
    }
  }
}

/** @brief Whether a cast from U to T is safe from overflow.
 *
 * A cast is safe if it preserves signedness and does not reduce size. Also, a cast from
 * unsigned to signed is safe if the size strictly increases. All other casts can overflow. */
template<true_integer T, true_integer U>
constexpr bool cast_is_safe{
  sizeof(T) >= sizeof(U) && std::signed_integral<T> == std::signed_integral<U>
  || sizeof(T) > sizeof(U) && std::signed_integral<T> && std::unsigned_integral<U>
};

/** @brief Casts the given number to the type T, returning nothing on overflow. */
template<true_integer T, true_integer U>
[[nodiscard]] constexpr std::optional<T> checked_cast(U u) noexcept {
  if(auto const result{safe_int<U>::wrap(u).template checked_cast<T>()}) {
    return result->unwrap();
  } else {
    return std::nullopt;
  }
}

// Asserted operations ////////////////////////////////////////////////////////////////////////

/** @brief Adds one or more numbers and asserts that there is no overflow. */
template<typename T = void, true_unsigned_integer U, true_unsigned_integer... Us>
requires true_unsigned_integer<T> || (std::same_as<T, void> && ... && std::same_as<U, Us>)
[[nodiscard]] constexpr detail_::overflow_result_t<T, U>
asserted_add(U op, Us... ops) noexcept {
  auto const result{checked_add<T>(op, ops...)};
  assert(result);
  return *result;
}

/** @brief Multiplies two numbers and asserts that there is no overflow. */
template<typename T = void, true_unsigned_integer U, true_unsigned_integer... Us>
requires true_unsigned_integer<T> || (std::same_as<T, void> && ... && std::same_as<U, Us>)
[[nodiscard]] constexpr detail_::overflow_result_t<T, U>
asserted_mul(U op, Us... ops) noexcept {
  auto const result{checked_mul<T>(op, ops...)};
  assert(result);
  return *result;
}

/** @brief Casts the given number to the type T and asserts that there is no overflow. */
template<true_integer T, true_integer U>
[[nodiscard]] constexpr T asserted_cast(U u) noexcept {
  auto const result{checked_cast<T>(u)};
  assert(result);
  return *result;
}

// Throwing operations ////////////////////////////////////////////////////////////////////////

/** @brief Adds one or more numbers, throwing an exception on overflow.
 * @throws std::overflow_error In case of overflow.
 * @throws std::bad_alloc If the exception object could not be allocated. */
template<typename T = void, true_unsigned_integer U, true_unsigned_integer... Us>
requires true_unsigned_integer<T> || (std::same_as<T, void> && ... && std::same_as<U, Us>)
constexpr detail_::overflow_result_t<T, U> throwing_add(U op, Us... ops) {
  if(auto const result{checked_add<T>(op, ops...)}) {
    return *result;
  }
  throw std::overflow_error{"util::throwing_add"};
}

/** @brief Multiplies two numbers, throwing an exception on overflow.
 * @throws std::overflow_error In case of overflow.
 * @throws std::bad_alloc If the exception object could not be allocated. */
template<typename T = void, true_unsigned_integer U, true_unsigned_integer... Us>
requires true_unsigned_integer<T> || (std::same_as<T, void> && ... && std::same_as<U, Us>)
constexpr detail_::overflow_result_t<T, U> throwing_mul(U op, Us... ops) {
  if(auto const result{checked_mul<T>(op, ops...)}) {
    return *result;
  }
  throw std::overflow_error{"util::throwing_mul"};
}

/** @brief Casts the given number to the type T, throwing an exception on overflow.
 * @throws std::overflow_error In case of overflow.
 * @throws std::bad_alloc If the exception object could not be allocated. */
template<true_integer T, true_integer U>
constexpr T throwing_cast(U u) noexcept(cast_is_safe<T, U>) {
  if(auto const result{checked_cast<T>(u)}) {
    return *result;
  }

  if constexpr(cast_is_safe<T, U>) {
    unreachable();
  } else {
    throw std::overflow_error{"util::throwing_cast"};
  }
}

// Saturating operations //////////////////////////////////////////////////////////////////////

/** @brief Adds two numbers, returning the maximum value on overflow. */
template<typename T = void, true_unsigned_integer U, true_unsigned_integer... Us>
requires true_unsigned_integer<T> || (std::same_as<T, void> && ... && std::same_as<U, Us>)
[[nodiscard]] constexpr detail_::overflow_result_t<T, U>
saturating_add(U op, Us... ops) noexcept {
  if(auto const result{checked_add<T>(op, ops...)}) {
    return *result;
  }
  return std::numeric_limits<detail_::overflow_result_t<T, U>>::max();
}

/** @brief Multiplies two numbers, returning the maximum value on overflow. */
template<typename T = void, true_unsigned_integer U, true_unsigned_integer... Us>
requires true_unsigned_integer<T> || (std::same_as<T, void> && ... && std::same_as<U, Us>)
[[nodiscard]] constexpr detail_::overflow_result_t<T, U>
saturating_mul(U op, Us... ops) noexcept {
  if(auto const result{checked_mul<T>(op, ops...)}) {
    return *result;
  }
  return std::numeric_limits<detail_::overflow_result_t<T, U>>::max();
}

/** @brief Casts the given number to the type T, returning the maximum possible value on
 * overflow from above or the minimum possible value on overflow from below. */
template<true_unsigned_integer T, true_unsigned_integer U>
[[nodiscard]] constexpr T saturating_cast(U u) noexcept {
  if(auto const result{checked_cast<T>(u)}) {
    return *result;
  }

  if constexpr(cast_is_safe<T, U>) {
    unreachable();
  } else {
    if(u > std::numeric_limits<T>::max()) {
      return std::numeric_limits<T>::max();
    } else {
      assert(u < std::numeric_limits<T>::min());
      return std::numeric_limits<T>::min();
    }
  }
}

} // util

#endif
