#ifndef UTIL_PTR_WITH_FLAGS_HPP_INCLUDED_8TLEQDRSZOB5TSJGSSAFU1PV4
#define UTIL_PTR_WITH_FLAGS_HPP_INCLUDED_8TLEQDRSZOB5TSJGSSAFU1PV4

#include "util/type_traits.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <utility>

// NOLINTBEGIN(performance-no-int-to-ptr)

namespace util {

/** @brief A pointer that stores additional information in its least significant
 * bits (which would otherwise be zero due to alignment requirements).
 * @tparam T the pointee type
 * @tparam N the number of flags */
template<object T, std::size_t N>
class ptr_with_flags {
private:
  std::uintptr_t value_;

  static constexpr std::uintptr_t ptr_mask{
    ~((static_cast<std::uintptr_t>(1) << N) - 1)};

  friend struct std::hash<ptr_with_flags>;

public:
  /** @brief Initializes the pointer to the given value and the flags to false.
   */
  ptr_with_flags(T* value) noexcept:
      value_{reinterpret_cast<std::uintptr_t>(value)} {
    assert((value_ & ~ptr_mask) == 0);
  }

  /** @brief Initializes the pointer to a null pointer and the flags to false.
   */
  ptr_with_flags() noexcept: ptr_with_flags{nullptr} {}

private:
  void init_flags(std::index_sequence<>) noexcept {}

  template<
    std::size_t Index, std::size_t... Indexes,
    std::convertible_to<bool> Flag, std::convertible_to<bool>... Flags>
  requires (sizeof...(Indexes) == sizeof...(Flags)) && (sizeof...(Flags) < N)
  void init_flags(
    std::index_sequence<Index, Indexes...>,
    Flag value,
    Flags... values
  ) {
    this->set_flag<Index>(value);
    this->init_flags(std::index_sequence<Indexes...>{}, values...);
  }

public:
  /** @brief Initializes the pointer and the flags to the given values. */
  template<std::convertible_to<bool>... Ts>
  requires (sizeof...(Ts) == N)
  ptr_with_flags(T* value, Ts... flags): ptr_with_flags{value} {
    this->init_flags(std::make_index_sequence<N>{}, flags...);
  }

  /** @brief Extracts the pointer value. */
  [[nodiscard]] T* get_ptr() const noexcept {
    return reinterpret_cast<T*>(value_ & ptr_mask);
  }

  /** @brief Extracts the pointer value. */
  [[nodiscard]] operator T*() const noexcept {
    return get_ptr();
  }

  /** @brief Changes the pointer value. */
  void set_ptr(T* new_value) noexcept {
    std::uintptr_t const as_int{reinterpret_cast<std::uintptr_t>(new_value)};
    assert((as_int & ~ptr_mask) == 0);
    value_ = as_int | (value_ & ~ptr_mask);
    assert(get_ptr() == new_value);
  }

  [[nodiscard]] T& operator*() const noexcept {
    return *get_ptr();
  }

  [[nodiscard]] T& operator[](std::size_t offset) const noexcept {
    return get_ptr()[offset];
  }

  [[nodiscard]] T* operator->() const noexcept {
    return get_ptr();
  }

  /** @brief Extracts the flag at the given index. */
  template<std::size_t Index>
  requires (Index < N)
  [[nodiscard]] bool get_flag() const noexcept {
    return (value_ & (static_cast<std::uintptr_t>(1) << Index)) != 0;
  }

  /** @brief Changes the value of the flag at the given index. */
  template<std::size_t Index>
  requires (Index < N)
  void set_flag(bool new_value) noexcept {
    if(new_value) {
      value_ |= static_cast<std::uintptr_t>(1) << Index;
    } else {
      value_ &= ~(static_cast<std::uintptr_t>(1) << Index);
    }
    assert(get_flag<Index>() == new_value);
  }

  [[nodiscard]] bool operator==(ptr_with_flags other) const noexcept {
    return get_ptr() == other.get_ptr()
      && (value_ & ~ptr_mask) == (other.value_ & ~ptr_mask);
  }
};

} // util

template<util::object T, std::size_t N>
struct std::hash<util::ptr_with_flags<T, N>> {
  [[nodiscard]] ::std::size_t operator()(::util::ptr_with_flags<T, N> ptr) {
    return ::std::hash<::std::uintptr_t>{}(ptr.value_);
  }
};

// NOLINTEND(performance-no-int-to-ptr)

#endif
