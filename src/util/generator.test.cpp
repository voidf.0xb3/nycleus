#include <boost/test/unit_test.hpp>

#include "util/generator.hpp"
#include "util/ranges.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <concepts>
#include <cstdint>
#include <exception>
#include <iterator>
#include <ranges>
#include <vector>

namespace {

static_assert(std::ranges::input_range<util::generator<std::uint64_t>>);
static_assert(!std::ranges::forward_range<util::generator<std::uint64_t>>);
static_assert(std::ranges::view<util::generator<std::uint64_t>>);
static_assert(std::same_as<
  std::ranges::range_value_t<util::generator<std::uint64_t>>, std::uint64_t>);
static_assert(std::same_as<std::ranges::range_reference_t<
  util::generator<std::uint64_t>>, std::uint64_t&&>);

util::generator<std::uint64_t> test_basic() {
  co_yield 1;
  co_yield 2;
  co_yield 5;
}

util::generator<std::uint64_t> test_empty() {
  co_return;
}

util::generator<std::uint64_t> test_nested_generator_2() {
  co_yield 100;
}

util::generator<std::uint64_t> test_nested_generator_1() {
  co_yield 10;
  co_yield util::elements_of{test_nested_generator_2()};
  co_yield 20;
  co_yield util::elements_of{test_nested_generator_2()};
}

util::generator<std::uint64_t> test_nested_generator_0() {
  co_yield 1;
  co_yield util::elements_of{test_nested_generator_1()};
  co_yield 2;
  co_yield util::elements_of{test_nested_generator_2()};
  co_yield 3;
}

util::generator<std::uint64_t> test_nested_range() {
  co_yield 1;
  co_yield util::elements_of{std::views::iota(100) | std::views::take(2)};
  co_yield 2;
}

util::generator<std::uint64_t> test_nested_empty_1() {
  co_return;
}

util::generator<std::uint64_t> test_nested_empty_0() {
  co_yield 1;
  co_yield util::elements_of{test_nested_empty_1()};
  co_yield 2;
  co_yield util::elements_of{test_nested_empty_1()};
}

class test_exception_t: public std::exception {
  [[nodiscard]] virtual gsl::czstring what() const noexcept override {
    return "generator test_exception_t";
  }
};

util::generator<std::uint64_t> test_exception() {
  co_yield 1;
  throw test_exception_t{};
}

util::generator<std::uint64_t> test_exception_nested_catch_1() {
  co_yield 10;
  throw test_exception_t{};
}

util::generator<std::uint64_t> test_exception_nested_catch_0() {
  co_yield 1;
  std::uint64_t value{0};
  try {
    co_yield util::elements_of{test_exception_nested_catch_1()};
    BOOST_TEST(false);
  } catch(test_exception_t const&) {
    value = 2;
  }
  co_yield value;
}

util::generator<std::uint64_t> test_exception_nested_propagate_2() {
  co_yield 100;
  throw test_exception_t{};
}

util::generator<std::uint64_t> test_exception_nested_propagate_1() {
  co_yield 10;
  co_yield util::elements_of{test_exception_nested_propagate_2()};
  BOOST_TEST(false);
}

util::generator<std::uint64_t> test_exception_nested_propagate_0() {
  co_yield 1;
  std::uint64_t value{0};
  try {
    co_yield util::elements_of{test_exception_nested_propagate_1()};
    BOOST_TEST(false);
  } catch(test_exception_t const&) {
    value = 2;
  }
  co_yield value;
}

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(generator)

BOOST_AUTO_TEST_CASE(basic) {
  auto generated{test_basic()};

  auto const output{generated | util::range_to<std::vector>()};
  std::uint64_t const test[]{1, 2, 5};
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(empty) {
  auto generated{test_empty()};

  auto const output{generated | util::range_to<std::vector>()};
  BOOST_TEST(output.empty());
}

BOOST_AUTO_TEST_CASE(nested_generator) {
  auto generated{test_nested_generator_0()};

  auto const output{generated | util::range_to<std::vector>()};
  std::uint64_t const test[]{1, 10, 100, 20, 100, 2, 100, 3};
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(nested_range) {
  auto generated{test_nested_range()};

  auto const output{generated | util::range_to<std::vector>()};
  std::uint64_t const test[]{1, 100, 101, 2};
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(nested_empty) {
  auto generated{test_nested_empty_0()};

  auto const output{generated | util::range_to<std::vector>()};
  std::uint64_t const test[]{1, 2};
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(exception) {
  auto generated{test_exception()};

  std::vector<std::uint64_t> output;
  BOOST_CHECK_THROW(std::ranges::copy(generated, std::back_inserter(output)),
    test_exception_t);
  std::uint64_t const test[]{1};
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(exception_nested_catch) {
  auto generated{test_exception_nested_catch_0()};

  auto const output{generated | util::range_to<std::vector>()};
  std::uint64_t const test[]{1, 10, 2};
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(exception_nested_propagate) {
  auto generated{test_exception_nested_propagate_0()};

  auto const output{generated | util::range_to<std::vector>()};
  std::uint64_t const test[]{1, 10, 100, 2};
  BOOST_TEST(output == test, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // generator
BOOST_AUTO_TEST_SUITE_END() // test_util
