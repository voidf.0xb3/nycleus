#ifndef UTIL_BIGINT_HPP_INCLUDED_2AAJT5LHZ9RJ7T4C3WZQJ7TQI
#define UTIL_BIGINT_HPP_INCLUDED_2AAJT5LHZ9RJ7T4C3WZQJ7TQI

#include "util/config.hpp"
#include "util/iterator_facade.hpp"
#include "util/nums.hpp"
#include "util/overflow.hpp"
#include "util/small_vector.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include "util/zstring_view.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <bit>
#include <cassert>
#include <compare>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <ios>
#include <istream>
#include <iterator>
#include <limits>
#include <optional>
#include <ostream>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>

namespace util {

/** @brief An arbitrarily large integer.
 *
 * The size of these integers is limited only by available memory. Internally,
 * the numbers are stored in a sign-and-magnitude representation.
 *
 * Division results are rounded towards zero. The remainder is chosen so that,
 * given numbers <var>A</var> and <var>B</var>, their rounded quotient
 * <var>Q</var>, and the remainder <var>R</var>, it is the case that
 * <var>Q</var> * <var>B</var> + <var>R</var> = <var>A</var>.
 *
 * All operations that throw exceptions have strong exception safety guarantees.
 *
 * There is a strong move safety guarantee: a moved-from object has the value 0.
 *
 * Bitwise operations operate on the numbers' two's complement representations,
 * infinitely extended with the sign bit. */
class bigint {
private:
  #if HAVE_UINT128_T
    using limb = std::uint64_t;
    using two_limbs = __uint128_t;
  #else
    using limb = std::uint32_t;
    using two_limbs = std::uint64_t;
  #endif

  static_assert(sizeof(two_limbs) == 2 * sizeof(limb));
  static_assert(std::numeric_limits<std::size_t>::max() / 8
    >= sizeof(two_limbs));

  static limb asserted_cast_to_limb(two_limbs value) noexcept {
    assert(value <= std::numeric_limits<limb>::max());
    return static_cast<limb>(value);
  }

  static constexpr std::size_t small_capacity{
    (24 + sizeof(limb) - 1) / sizeof(limb)};

  /** @brief The number's magnitude, stored in a large base in little-endian
   * order.
   *
   * Leading zeros are allowed (and do not affect the value). Empty vector
   * indicates zero. */
  small_vector<limb, small_capacity> limbs_;

  /** @brief Whether the number is negative.
   *
   * Note that this is false if the number is zero. */
  bool neg_;

  // TODO: Expression templates

public:
  /** @brief Initializes the number to zero. */
  bigint() noexcept: limbs_{}, neg_{false} {}

  bigint(bigint const&) = default;

  bigint(bigint&& other) noexcept:
      limbs_{std::move(other.limbs_)}, neg_{other.neg_} {
    other.limbs_.clear();
    other.neg_ = false;
  }

  bigint& operator=(bigint const&) = default;

  bigint& operator=(bigint&& other) noexcept {
    limbs_ = std::move(other.limbs_);
    neg_ = other.neg_;

    other.limbs_.clear();
    other.neg_ = false;

    return *this;
  }

  ~bigint() = default;

  // Conversions to and from small integers ////////////////////////////////////

  template<std::unsigned_integral T>
  bigint(T t) noexcept(sizeof(T) <= sizeof(limb) * small_capacity):
      limbs_{}, neg_{false} {
    if constexpr(sizeof(T) <= sizeof(limb)) {
      limbs_.push_back(t);
    } else {
      limbs_.reserve((sizeof(T) + sizeof(limb) - 1) / sizeof(limb));
      while(t > 0) {
        limbs_.push_back(static_cast<limb>(t));
        t >>= (sizeof(limb) * 8);
      }
    }
  }

  template<std::signed_integral T>
  bigint(T t) noexcept(sizeof(T) <= sizeof(limb) * small_capacity):
      limbs_{}, neg_{t < 0} {
    std::make_unsigned_t<T> mag{
      static_cast<std::make_unsigned_t<T>>(t < 0 ? -t : t)};
    static_assert(sizeof(T) == sizeof(mag));

    if constexpr(sizeof(mag) <= sizeof(limb)) {
      limbs_.push_back(mag);
    } else {
      limbs_.reserve((sizeof(T) + sizeof(limb) - 1) / sizeof(limb));
      while(mag > 0) {
        limbs_.push_back(static_cast<limb>(mag));
        mag >>= sizeof(limb) * 8;
      }
    }
  }

  /** @brief Converts the object to a built-in integral type.
   *
   * If this object's value does not fit into the target type, the result is
   * produced from the least significant bits of the value's two's complement
   * representation. */
  template<std::integral T>
  requires (!std::same_as<T, bool>)
  [[nodiscard]] explicit operator T() const noexcept {
    constexpr std::size_t num_limbs{
      (sizeof(T) + sizeof(limb) - 1) / sizeof(limb)};
    std::size_t limbs_added{0};

    using intermediate_t = std::make_unsigned_t<T>;
    intermediate_t result{0};

    if(neg_) {
      bool adding_one{true};
      while(limbs_added < num_limbs && limbs_added < limbs_.size()) {
        limb const next_limb{~limbs_[limbs_added] + (adding_one ? 1 : 0)};
        if(next_limb != 0) {
          adding_one = false;
          result |= static_cast<intermediate_t>(next_limb)
            << (limbs_added * sizeof(limb) * 8);
        }
        ++limbs_added;
      }
      if(limbs_added < num_limbs) {
        // We need to cast twice due to integer promotion rules
        result |= static_cast<intermediate_t>(~static_cast<intermediate_t>(0))
          << (limbs_added * sizeof(limb) * 8);
      }
    } else {
      while(limbs_added < num_limbs && limbs_added < limbs_.size()) {
        result |= static_cast<intermediate_t>(limbs_[limbs_added])
          << (limbs_added * sizeof(limb) * 8);
        ++limbs_added;
      }
    }

    return static_cast<T>(result);
  }

  // Low-level access //////////////////////////////////////////////////////////

  /** @brief Returns the number of bytes actually allocated for the number. */
  [[nodiscard]] std::size_t capacity() const noexcept {
    return limbs_.capacity() * sizeof(limb);
  }

private:
  /** @brief Returns the number of limbs that carry significant information. */
  [[nodiscard]] std::size_t significant_limbs() const noexcept;

public:
  /** @brief Returns the number of bytes that actually store meaningful
   * information.
   *
   * This excludes bytes that store leading zeros. */
  [[nodiscard]] std::size_t size() const noexcept;

  /** @throws std::bad_alloc */
  void reserve(std::size_t);

  /** @throws std::bad_alloc */
  void shrink_to_fit();

  /** @brief Returns a byte of the magnitude represented as an unsigned integer
   * infinitely padded with zeros. */
  [[nodiscard]] std::uint8_t operator[](std::size_t) const noexcept;

  class lnz_t {
  private:
    std::size_t value_;

    explicit lnz_t(std::size_t value) noexcept: value_{value} {}

    friend class bigint;
  };

  /** @brief Returns the index of the least significant non-zero byte of the
   * magnitude.
   *
   * Can be used to optimize the two's complement access functions.
   *
   * The result is provided as an opaque value of type `lnz_t`, which can only
   * be used as the second argument to `twos_compl_at` called on a number equal
   * to this one. */
  [[nodiscard]] lnz_t lowest_non_zero() const noexcept;

  /** @brief Returns a byte of the infinite two's complement representation.
   *
   * If the number is positive or zero, this operates in constant time. But if
   * the number is negative, it takes linear time to find the least significant
   * non-zero byte of the magnitude. To avoid this, the index can be precomputed
   * via `lowest_non_zero` and passed into the other overload. */
  [[nodiscard]] std::uint8_t twos_compl_at(std::size_t) const noexcept;

  /** @brief Returns a byte of the infinite two's complement representation,
   * given a precomputed index of the least significant non-zero byte of the
   * magnitude.
   *
   * If the number is negative, the precomputed index can be used to obtain the
   * result in constant time, rather than linear.
   *
   * @param lnz The value returned by `lowest_non_zero`. */
  [[nodiscard]] std::uint8_t twos_compl_at(std::size_t, lnz_t lnz) const noexcept;

  // Comparison ////////////////////////////////////////////////////////////////

  friend bool operator==(bigint const&, bigint const&) noexcept;
  friend std::strong_ordering operator<=>(bigint const&,
    bigint const&) noexcept;

  [[nodiscard]] bool is_negative() const noexcept { return neg_; }

  // Arithmetic ////////////////////////////////////////////////////////////////
private:
  /** @brief Adds the magnitudes of the given numbers and sets this number's
   * magnitude to the result.
   *
   * Ignores the operands' signs and does not change this number's sign.
   *
   * This object must not be the same as either of the terms.
   *
   * @throws std::bad_alloc */
  void add_mags(bigint const&, bigint const&);

  /** @brief Adds the given number's magnitude to this one's and sets this
   * number's magnitude to the result.
   *
   * Ignores the operand's sign and does not change this number's sign.
   *
   * This object may be the same as the operand.
   *
   * @throws std::bad_alloc */
  void add_mag(bigint const&);

  /** @brief Replaces the magnitude with its two's complement inverse.
   *
   * The magnitude must not be zero. */
  void invert_mag() noexcept;

  /** @brief Subtracts the magnitudes of the given numbers and sets this
   * number's magnitude to the magnitude of the result.
   *
   * Ignores the operands' signs. If the result is zero, sets this number's sign
   * to false; if the result is negative, flips this number's sign; otherwise,
   * does not change this number's sign.
   *
   * This object must not be the same as either of the terms.
   *
   * @returns Whether the resulting magnitude is zero.
   * @throws std::bad_alloc */
  void sub_mags(bigint const&, bigint const&);

  /** @brief Subtracts the given number's magnitude from this one's and sets
   * this number's magnitude to the result.
   *
   * Ignores the operands' signs. If the result is zero, sets this number's sign
   * to false; if the result is negative, flips this number's sign; otherwise,
   * does not change this number's sign.
   *
   * This object may be the same as the operand.
   *
   * @returns Whether the resulting magnitude is zero.
   * @throws std::bad_alloc */
  void sub_mag(bigint const&);

public:
  /** @throws std::bad_alloc */
  [[nodiscard]] bigint operator+() const {
    return *this;
  }

  /** @throws std::bad_alloc */
  bigint& operator+=(bigint const&);

  /** @throws std::bad_alloc */
  friend bigint operator+(bigint const&, bigint const&);

  /** @throws std::bad_alloc */
  bigint& operator++() {
    return *this += 1;
  }

  /** @throws std::bad_alloc */
  bigint operator++(int) {
    bigint result{*this};
    ++*this;
    return result;
  }

  /** @brief Flips the number's sign. */
  void negate() noexcept;

  /** @throws std::bad_alloc */
  [[nodiscard]] bigint operator-() const & {
    bigint result{*this};
    result.negate();
    return result;
  }

  /** @throws std::bad_alloc */
  [[nodiscard]] bigint operator-() && {
    negate();
    bigint result{std::move(*this)};
    return result;
  }

  /** @throws std::bad_alloc */
  bigint& operator-=(bigint const&);

  /** @throws std::bad_alloc */
  friend bigint operator-(bigint const&, bigint const&);

  /** @throws std::bad_alloc */
  friend bigint operator-(bigint const&, bigint&&);

  /** @throws std::bad_alloc */
  bigint& operator--() {
    return *this -= 1;
  }

  /** @throws std::bad_alloc */
  bigint operator--(int) {
    bigint result{*this};
    --*this;
    return result;
  }

  /** @throws std::bad_alloc */
  bigint& operator*=(bigint const&);

  /** @throws std::bad_alloc */
  friend bigint operator*(bigint const&, bigint const&);

private:
  /** @brief An implementation of `divide` where the divisor fits into one limb.
   * @throws std::bad_alloc */
  void divide_by_limb(limb divisor, bool divisor_neg, bigint* quotient);

public:
  /** @brief Performs division, calculating both the quotient and the remainder.
   *
   * This object is treated as the dividend. After the operation, it holds the
   * remainder.
   *
   * The divisor may be the same object as this one. However, the quotient
   * destination, if present, must not be the same as either this object or the
   * divisor.
   *
   * @param divisor The divisor.
   * @param result A pointer to the bigint that will contain the quotient. If
   *               nullptr is passed, the quotient is not calculated.
   * @throws std::bad_alloc */
  void divide(bigint const& divisor, bigint* quotient);

  /** @throws std::bad_alloc */
  bigint& operator/=(bigint const& other);

  /** @throws std::bad_alloc */
  bigint& operator%=(bigint const& other) {
    this->divide(other, nullptr);
    return *this;
  }

  // Limb iterators ////////////////////////////////////////////////////////////
private:
  class limb_iterator_core {
  private:
    bigint const* bigint_;
    std::size_t index_;
    bool flip_;

    friend class bigint;
    limb_iterator_core(bigint const* b, std::size_t i, bool f) noexcept:
      bigint_{b}, index_{i}, flip_{f} {}

  public:
    limb_iterator_core() noexcept = default;

  protected:
    [[nodiscard]] limb dereference() const noexcept {
      return flip_ ? ~bigint_->limbs_[index_] : bigint_->neg_
        ? ~bigint_->limbs_[index_] + 1 : bigint_->limbs_[index_];
    }

    [[nodiscard]] std::ptrdiff_t distance_to(limb_iterator_core const& other)
        const noexcept {
      return static_cast<std::ptrdiff_t>(other.index_ - index_);
    }

    void increment() noexcept {
      if(bigint_->neg_ && bigint_->limbs_[index_] != 0) {
        flip_ = true;
      }
      ++index_;
    }
  };

  using limb_iterator = iterator_facade<limb_iterator_core>;
  static_assert(std::forward_iterator<limb_iterator>);
  static_assert(std::sized_sentinel_for<limb_iterator, limb_iterator>);

  /** @brief Returns a limb_iterator to the beginning of this bigint.
   *
   * Since limbs are an implementation detail, this is a private member. It is
   * used in the implementation of bitwise operators. */
  [[nodiscard]] limb_iterator limbs_begin() const noexcept {
    limb_iterator it{this, 0, false};
    return it;
  }

  /** @brief Returns a limb_iterator to the end of this bigint. */
  [[nodiscard]] limb_iterator limbs_end() const noexcept {
    limb_iterator it{this, limbs_.size(), neg_};
    return it;
  }

  // Bitwise operations ////////////////////////////////////////////////////////

  /** @brief Decreases the magnitude by one.
   *
   * Used for implementing bitwise operations. */
  void mag_decrement() noexcept;

public:
  /** @brief Performs bitwise negation of the number's two's complement
   * representation.
   * @throws std::bad_alloc */
  void bit_invert() {
    if(neg_) {
      mag_decrement();
      neg_ = false;
    } else {
      this->add_mag(1);
      neg_ = true;
    }
  }

  /** @throws std::bad_alloc */
  [[nodiscard]] bigint operator~() const &;

  /** @throws std::bad_alloc */
  [[nodiscard]] bigint operator~() && {
    bit_invert();
    bigint result{std::move(*this)};
    return result;
  }

  /** @throws std::bad_alloc */
  bigint& operator&=(bigint const&);

  /** @throws std::bad_alloc */
  friend bigint operator&(bigint const&, bigint const&);

  /** @throws std::bad_alloc */
  bigint& operator|=(bigint const&);

  /** @throws std::bad_alloc */
  friend bigint operator|(bigint const&, bigint const&);

  /** @throws std::bad_alloc */
  bigint& operator^=(bigint const&);

  /** @throws std::bad_alloc */
  friend bigint operator^(bigint const&, bigint const&);

  /** @throws std::bad_alloc */
  bigint& operator<<=(std::size_t);

  /** @throws std::bad_alloc */
  friend bigint operator<<(bigint const&, std::size_t);

  bigint& operator>>=(std::size_t) noexcept;

  /** @throws std::bad_alloc */
  friend bigint operator>>(bigint const&, std::size_t);

  // Conversion to string //////////////////////////////////////////////////////

  /** @brief Produces uppercase digits for conversion to strings, in the basic
   * execution encoding. */
  [[nodiscard]] static std::ranges::single_view<char> digits_upper(
    bigint const& digit
  ) noexcept;
  /** @brief Produces lowercase digits for conversion to strings, in the basic
   * execution encoding. */
  [[nodiscard]] static std::ranges::single_view<char> digits_lower(
    bigint const& digit
  ) noexcept;
  /** @brief Produces uppercase digits for conversion to strings, in the wide
   * execution encoding. */
  [[nodiscard]] static std::ranges::single_view<wchar_t> digits_upper_w(
    bigint const& digit
  ) noexcept;
  /** @brief Produces lowercase digits for conversion to strings, in the wide
   * execution encoding. */
  [[nodiscard]] static std::ranges::single_view<wchar_t> digits_lower_w(
    bigint const& digit
  ) noexcept;
  /** @brief Produces uppercase digits for conversion to strings, in UTF-8. */
  [[nodiscard]] static std::ranges::single_view<char8_t> digits_upper_u8(
    bigint const& digit
  ) noexcept;
  /** @brief Produces lowercase digits for conversion to strings, in UTF-8. */
  [[nodiscard]] static std::ranges::single_view<char8_t> digits_lower_u8(
    bigint const& digit
  ) noexcept;
  /** @brief Produces uppercase digits for conversion to strings, in UTF-16. */
  [[nodiscard]] static std::ranges::single_view<char16_t> digits_upper_u16(
    bigint const& digit
  ) noexcept;
  /** @brief Produces lowercase digits for conversion to strings, in UTF-16. */
  [[nodiscard]] static std::ranges::single_view<char16_t> digits_lower_u16(
    bigint const& digit
  ) noexcept;
  /** @brief Produces uppercase digits for conversion to strings, in UTF-32. */
  [[nodiscard]] static std::ranges::single_view<char32_t> digits_upper_u32(
    bigint const& digit
  ) noexcept;
  /** @brief Produces lowercase digits for conversion to strings, in UTF-32. */
  [[nodiscard]] static std::ranges::single_view<char32_t> digits_lower_u32(
    bigint const& digit
  ) noexcept;

  /** @brief Converts the number to a string.
   *
   * @param base The base of the representation. Must be at least 2.
   * @param digits A function specifying the representation digits. It is called
   *               with a bigint not less than 0 and less than base, and must
   *               return a range of characters used to represent the digit.
   * @param minus A string view that is prefixed to the string if the number is
   *              negative.
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<typename Char, typename Traits = std::char_traits<Char>,
    std::invocable<bigint const&> Digits, typename Minus>
  requires
    std::ranges::input_range<std::invoke_result_t<Digits&&, bigint const&>>
    && std::same_as<Char, std::ranges::range_value_t<
      std::invoke_result_t<Digits&&, bigint const&>>>
    && std::ranges::input_range<Minus&&> && std::same_as<Char,
      std::ranges::range_value_t<Minus&&>>
  [[nodiscard]] std::basic_string<Char, Traits> to_basic_string(
    bigint const& base,
    Digits&& digits,
    Minus&& minus
  ) const;

  /** @brief Converts the number to a string in the basic execution encoding.
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<std::invocable<bigint const&> Digits = decltype(digits_upper),
    typename Minus = std::string_view&&>
  requires
    std::ranges::input_range<std::invoke_result_t<Digits&&, bigint const&>>
    && std::same_as<char, std::ranges::range_value_t<
      std::invoke_result_t<Digits&&, bigint const&>>>
    && std::ranges::input_range<Minus&&> && std::same_as<char,
      std::ranges::range_value_t<Minus&&>>
  [[nodiscard]] std::string to_string(
    bigint const& base = 10,
    Digits&& digits = digits_upper,
    Minus&& minus = std::string_view{"-"}
  ) const {
    return this->to_basic_string<char>(base, std::forward<Digits>(digits),
      std::forward<Minus>(minus));
  }

  /** @brief Converts the number to a string in UTF-8.
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<std::invocable<bigint const&> Digits = decltype(digits_upper_u8),
    typename Minus = std::u8string_view&&>
  requires
    std::ranges::input_range<std::invoke_result_t<Digits&&, bigint const&>>
    && std::same_as<char8_t, std::ranges::range_value_t<
      std::invoke_result_t<Digits&&, bigint const&>>>
    && std::ranges::input_range<Minus&&> && std::same_as<char8_t,
      std::ranges::range_value_t<Minus&&>>
  [[nodiscard]] std::u8string to_u8string(
    bigint const& base = 10,
    Digits&& digits = digits_upper_u8,
    Minus&& minus = std::u8string_view{u8"-"}
  ) const {
    return this->to_basic_string<char8_t>(base,
      std::forward<Digits>(digits), std::forward<Minus>(minus));
  }

  /** @brief Converts the number to a string in the wide execution encoding.
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<std::invocable<bigint const&> Digits = decltype(digits_upper_w),
    typename Minus = std::wstring_view&&>
  requires
    std::ranges::input_range<std::invoke_result_t<Digits&&, bigint const&>>
    && std::same_as<wchar_t, std::ranges::range_value_t<
      std::invoke_result_t<Digits&&, bigint const&>>>
    && std::ranges::input_range<Minus&&> && std::same_as<wchar_t,
      std::ranges::range_value_t<Minus&&>>
  [[nodiscard]] std::wstring to_wstring(
    bigint const& base = 10,
    Digits&& digits = digits_upper_w,
    Minus&& minus = std::wstring_view{L"-"}
  ) const {
    return this->to_basic_string<wchar_t>(base, std::forward<Digits>(digits),
      std::forward<Minus>(minus));
  }

  /** @brief Converts the number to a string in UTF-16.
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<std::invocable<bigint const&> Digits = decltype(digits_upper_u16),
    typename Minus = std::u16string_view&&>
  requires
    std::ranges::input_range<std::invoke_result_t<Digits&&, bigint const&>>
    && std::same_as<char16_t, std::ranges::range_value_t<
      std::invoke_result_t<Digits&&, bigint const&>>>
    && std::ranges::input_range<Minus&&> && std::same_as<char16_t,
      std::ranges::range_value_t<Minus&&>>
  [[nodiscard]] std::u16string to_u16string(
    bigint const& base = 10,
    Digits&& digits = digits_upper_u16,
    Minus&& minus = std::u16string_view{u"-"}
  ) const {
    return this->to_basic_string<char16_t>(base, std::forward<Digits>(digits),
      std::forward<Minus>(minus));
  }

  /** @brief Converts the number to a string in the UTF-32.
   * @throws std::bad_alloc
   * @throws std::length_error */
  template<std::invocable<bigint const&> Digits = decltype(digits_upper_u32),
    typename Minus = std::u32string_view&&>
  requires
    std::ranges::input_range<std::invoke_result_t<Digits&&, bigint const&>>
    && std::same_as<char32_t, std::ranges::range_value_t<
      std::invoke_result_t<Digits&&, bigint const&>>>
    && std::ranges::input_range<Minus&&> && std::same_as<char32_t,
      std::ranges::range_value_t<Minus&&>>
  [[nodiscard]] std::u32string to_u32string(
    bigint const& base = 10,
    Digits&& digits = digits_upper_u32,
    Minus&& minus = std::u32string_view{U"-"}
  ) const {
    return this->to_basic_string<char32_t>(base, std::forward<Digits>(digits),
      std::forward<Minus>(minus));
  }

  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] explicit operator std::string() const {
    return to_string();
  }
  /** @throws std::bad_alloc
  * @throws std::length_error */
  [[nodiscard]] explicit operator std::u8string() const {
    return to_u8string();
  }
  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] explicit operator std::wstring() const {
    return to_wstring();
  }
  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] explicit operator std::u16string() const {
    return to_u16string();
  }
  /** @throws std::bad_alloc
   * @throws std::length_error */
  [[nodiscard]] explicit operator std::u32string() const {
    return to_u32string();
  }

  // Conversion from string ////////////////////////////////////////////////////

  /** @brief Parses a string to create a bigint.
   *
   * The accepted syntax consists of an optional sign, `+` or `-`, followed by
   * digits. Digits with values 10 and greater are English letters, accepted in
   * both upper and lower case. There may be extra characters after the number,
   * which are disregarded.
   *
   * If `base` is 0, the actual base is autodetected as follows: if there is a
   * prefix `0x` or `0X` in front of the number, after the optional sign, then
   * base 16 is assumed. Otherwise, if the number has leading zeros, base 8 is
   * assumed. Otherwise, base 10 is assumed. (If base 16 is specified
   * explicitly, the prefix is not accepted.)
   *
   * If the range is single-pass, its iterator is advanced as follows:
   * - If the base is autodetected and there is a hexadecimal prefix that is not
   *   followed by a valid digit, the iterator is advanced past the end of the
   *   prefix. (In this case, the decoded number is zero, and the `x` or `X` is
   *   not part of it.)
   * - Otherwise, if there is a valid integer encoded, the iterator is advanced
   *   just past the end of the number.
   * - Otherwise, if the string begins with `+` or `-`, the iterator is advanced
   *   once.
   * - Otherwise, the iterator is not advanced at all.
   *
   * @returns A pair consisting of:
   *          - the parsed number, if any;
   *          - if the range is multipass, an iterator just past the end of the
   *            number; otherwise, an iterator to the point in the range to
   *            which it was advanced, as described above.
   * @throws std::bad_alloc */
  template<typename S, bool SkipApos = false>
  requires std::ranges::input_range<S&&>
    && one_of<std::ranges::range_value_t<S&&>,
    char, char8_t, wchar_t, char16_t, char32_t>
  [[nodiscard]] static std::pair<
    std::optional<bigint>,
    std::ranges::iterator_t<S&&>
  > maybe_from_string(S&&, std::uint8_t base = 10);

  /** @brief Parses a string to create a bigint.
   *
   * Just like `maybe_from_string`, except that if there is no valid number
   * encoded in the string, an exception is thrown. Therefore, the first return
   * value is a bigint and not an optional.
   *
   * @throws std::invalid_argument If there is no valid number representation.
   * @throws std::bad_alloc */
  template<typename S>
  requires std::ranges::input_range<S&&>
    && one_of<std::ranges::range_value_t<S&&>,
    char, char8_t, wchar_t, char16_t, char32_t>
  [[nodiscard]] static std::pair<
    bigint,
    std::ranges::iterator_t<S&&>
  > from_string(S&& str, std::uint8_t base = 10) {
    assert((2 <= base && base <= 36) || base == 0);
    auto [num, it] = maybe_from_string(std::forward<S>(str), base);
    if(!num) {
      throw std::invalid_argument{"util::bigint::from_string: parse error"};
    }
    return {std::move(*num), std::move(it)};
  }

  /** @throws std::invalid_argument
   * @throws std::bad_alloc */
  explicit bigint(std::string const& str): bigint{from_string(str).first} {}
  /** @throws std::invalid_argument
  * @throws std::bad_alloc */
  explicit bigint(std::u8string const& str): bigint{from_string(str).first} {}
  /** @throws std::invalid_argument
   * @throws std::bad_alloc */
  explicit bigint(std::wstring const& str): bigint{from_string(str).first} {}
  /** @throws std::invalid_argument
   * @throws std::bad_alloc */
  explicit bigint(std::u16string const& str): bigint{from_string(str).first} {}
  /** @throws std::invalid_argument
   * @throws std::bad_alloc */
  explicit bigint(std::u32string const& str): bigint{from_string(str).first} {}

  // Conversion from bytes /////////////////////////////////////////////////////

  /** @brief Constructs a bigint from a range of bytes.
   *
   * The constructed bigint will always be non-negative. The bytes are
   * considered to go in little-endian order.
   *
   * May also throw whatever is thrown by various operations on the range and
   * its iterators.
   *
   * @throws std::bad_alloc */
  template<typename S>
  requires std::ranges::input_range<S&&>
    && std::same_as<std::uint8_t, std::ranges::range_value_t<S&&>>
  [[nodiscard]] static bigint from_bytes(S&&);
};

// Binary operators implemented out-of-body ////////////////////////////////////

[[nodiscard]] bool operator==(bigint const&, bigint const&) noexcept;
[[nodiscard]] std::strong_ordering operator<=>(bigint const&, bigint const&)
  noexcept;

#define BIGINT_BINARY_OPS(op) \
\
  [[nodiscard]] bigint operator op(bigint const&, bigint const&); \
\
  /** @throws std::bad_alloc */ \
  [[nodiscard]] inline bigint operator op(bigint&& a, bigint const& b) { \
    bigint result{std::move(a)}; \
    result op##= b; \
    return result; \
  } \
\
  /** @throws std::bad_alloc */ \
  [[nodiscard]] inline bigint operator op(bigint const& a, bigint&& b) { \
    bigint result{std::move(b)}; \
    result op##= a; \
    return result; \
  } \
\
  /** @throws std::bad_alloc */ \
  [[nodiscard]] inline bigint operator op(bigint&& a, bigint&& b) { \
    bigint result{std::move(a)}; \
    result op##= b; \
    return result; \
  }

BIGINT_BINARY_OPS(+)
BIGINT_BINARY_OPS(*)
BIGINT_BINARY_OPS(&)
BIGINT_BINARY_OPS(|)
BIGINT_BINARY_OPS(^)

#undef BIGINT_BINARY_OPS

[[nodiscard]] bigint operator-(bigint const&, bigint const&);

/** @throws std::bad_alloc */
[[nodiscard]] inline bigint operator-(bigint&& a, bigint const& b) {
  bigint result{std::move(a)};
  result -= b;
  return result;
}

/** @throws std::bad_alloc */
[[nodiscard]] inline bigint operator-(bigint const& a, bigint&& b) {
  bigint result{std::move(b)};
  result -= a;
  result.negate();
  return result;
}

/** @throws std::bad_alloc */
[[nodiscard]] inline bigint operator-(bigint&& a, bigint&& b) {
  bigint result{std::move(a)};
  result -= b;
  return result;
}

/** @throws std::bad_alloc */
[[nodiscard]] inline bigint operator/(bigint&& a, bigint const& b) {
  assert(b != 0);
  bigint result;
  a.divide(b, &result);
  return result;
}

/** @throws std::bad_alloc */
[[nodiscard]] inline bigint operator/(bigint const& a, bigint const& b) {
  assert(b != 0);
  return bigint{a} / b;
}

inline bigint& bigint::operator/=(bigint const& other) {
  assert(other != 0);
  return *this = *this / other;
}

/** @throws std::bad_alloc */
[[nodiscard]] inline bigint operator%(bigint const& a, bigint const& b) {
  assert(b != 0);
  bigint result{a};
  result %= b;
  return result;
}

/** @throws std::bad_alloc */
[[nodiscard]] inline bigint operator%(bigint&& a, bigint const& b) {
  assert(b != 0);
  bigint result{std::move(a)};
  result %= b;
  return result;
}

[[nodiscard]] bigint operator<<(bigint const&, std::size_t);

/** @throws std::bad_alloc */
[[nodiscard]] inline bigint operator<<(bigint&& x, std::size_t off) {
  x <<= off;
  return std::move(x);
}

[[nodiscard]] bigint operator>>(bigint const&, std::size_t);

[[nodiscard]] inline bigint operator>>(bigint&& x, std::size_t off) noexcept {
  x >>= off;
  return std::move(x);
}

// Bigint to string conversions out of body ////////////////////////////////////

inline std::ranges::single_view<char> bigint::digits_upper(
  bigint const& digit
) noexcept {
  assert(digit < 36);
  std::uint8_t const digit_small{digit};

  if(digit_small < 10) {
    return std::views::single(static_cast<char>('0' + digit_small));
  }

  switch(digit_small) {
    case 10: return std::views::single('A');
    case 11: return std::views::single('B');
    case 12: return std::views::single('C');
    case 13: return std::views::single('D');
    case 14: return std::views::single('E');
    case 15: return std::views::single('F');
    case 16: return std::views::single('G');
    case 17: return std::views::single('H');
    case 18: return std::views::single('I');
    case 19: return std::views::single('J');
    case 20: return std::views::single('K');
    case 21: return std::views::single('L');
    case 22: return std::views::single('M');
    case 23: return std::views::single('N');
    case 24: return std::views::single('O');
    case 25: return std::views::single('P');
    case 26: return std::views::single('Q');
    case 27: return std::views::single('R');
    case 28: return std::views::single('S');
    case 29: return std::views::single('T');
    case 30: return std::views::single('U');
    case 31: return std::views::single('V');
    case 32: return std::views::single('W');
    case 33: return std::views::single('X');
    case 34: return std::views::single('Y');
    case 35: return std::views::single('Z');
    default: unreachable();
  }
}

inline std::ranges::single_view<char> bigint::digits_lower(
  bigint const& digit
) noexcept {
  assert(digit < 36);
  std::uint8_t const digit_small{digit};

  if(digit_small < 10) {
    return std::views::single(static_cast<char>('0' + digit_small));
  }

  switch(digit_small) {
    case 10: return std::views::single('a');
    case 11: return std::views::single('b');
    case 12: return std::views::single('c');
    case 13: return std::views::single('d');
    case 14: return std::views::single('e');
    case 15: return std::views::single('f');
    case 16: return std::views::single('g');
    case 17: return std::views::single('h');
    case 18: return std::views::single('i');
    case 19: return std::views::single('j');
    case 20: return std::views::single('k');
    case 21: return std::views::single('l');
    case 22: return std::views::single('m');
    case 23: return std::views::single('n');
    case 24: return std::views::single('o');
    case 25: return std::views::single('p');
    case 26: return std::views::single('q');
    case 27: return std::views::single('r');
    case 28: return std::views::single('s');
    case 29: return std::views::single('t');
    case 30: return std::views::single('u');
    case 31: return std::views::single('v');
    case 32: return std::views::single('w');
    case 33: return std::views::single('x');
    case 34: return std::views::single('y');
    case 35: return std::views::single('z');
    default: unreachable();
  }
}

inline std::ranges::single_view<wchar_t> bigint::digits_upper_w(
  bigint const& digit
) noexcept {
  assert(digit < 36);
  std::uint8_t const digit_small{digit};

  if(digit_small < 10) {
    return std::views::single(static_cast<wchar_t>(L'0' + digit_small));
  }

  switch(digit_small) {
    case 10: return std::views::single(L'A');
    case 11: return std::views::single(L'B');
    case 12: return std::views::single(L'C');
    case 13: return std::views::single(L'D');
    case 14: return std::views::single(L'E');
    case 15: return std::views::single(L'F');
    case 16: return std::views::single(L'G');
    case 17: return std::views::single(L'H');
    case 18: return std::views::single(L'I');
    case 19: return std::views::single(L'J');
    case 20: return std::views::single(L'K');
    case 21: return std::views::single(L'L');
    case 22: return std::views::single(L'M');
    case 23: return std::views::single(L'N');
    case 24: return std::views::single(L'O');
    case 25: return std::views::single(L'P');
    case 26: return std::views::single(L'Q');
    case 27: return std::views::single(L'R');
    case 28: return std::views::single(L'S');
    case 29: return std::views::single(L'T');
    case 30: return std::views::single(L'U');
    case 31: return std::views::single(L'V');
    case 32: return std::views::single(L'W');
    case 33: return std::views::single(L'X');
    case 34: return std::views::single(L'Y');
    case 35: return std::views::single(L'Z');
    default: unreachable();
  }
}

inline std::ranges::single_view<wchar_t> bigint::digits_lower_w(
  bigint const& digit
) noexcept {
  assert(digit < 36);
  std::uint8_t const digit_small{digit};

  if(digit_small < 10) {
    return std::views::single(static_cast<wchar_t>(L'0' + digit_small));
  }

  switch(digit_small) {
    case 10: return std::views::single(L'a');
    case 11: return std::views::single(L'b');
    case 12: return std::views::single(L'c');
    case 13: return std::views::single(L'd');
    case 14: return std::views::single(L'e');
    case 15: return std::views::single(L'f');
    case 16: return std::views::single(L'g');
    case 17: return std::views::single(L'h');
    case 18: return std::views::single(L'i');
    case 19: return std::views::single(L'j');
    case 20: return std::views::single(L'k');
    case 21: return std::views::single(L'l');
    case 22: return std::views::single(L'm');
    case 23: return std::views::single(L'n');
    case 24: return std::views::single(L'o');
    case 25: return std::views::single(L'p');
    case 26: return std::views::single(L'q');
    case 27: return std::views::single(L'r');
    case 28: return std::views::single(L's');
    case 29: return std::views::single(L't');
    case 30: return std::views::single(L'u');
    case 31: return std::views::single(L'v');
    case 32: return std::views::single(L'w');
    case 33: return std::views::single(L'x');
    case 34: return std::views::single(L'y');
    case 35: return std::views::single(L'z');
    default: unreachable();
  }
}

inline std::ranges::single_view<char8_t> bigint::digits_upper_u8(
  bigint const& digit
) noexcept {
  assert(digit < 36);

  if(digit < 10) {
    return std::views::single(static_cast<char8_t>(u8'0' + digit));
  } else {
    return std::views::single(
      static_cast<char8_t>(u8'A' + (digit - 10)));
  }
}

inline std::ranges::single_view<char8_t> bigint::digits_lower_u8(
  bigint const& digit
) noexcept {
  assert(digit < 36);

  if(digit < 10) {
    return std::views::single(static_cast<char8_t>(u8'0' + digit));
  } else {
    return std::views::single(
      static_cast<char8_t>(u8'a' + (digit - 10)));
  }
}

inline std::ranges::single_view<char16_t> bigint::digits_upper_u16(
  bigint const& digit
) noexcept {
  assert(digit < 36);

  if(digit < 10) {
    return std::views::single(static_cast<char16_t>(u'0' + digit));
  } else {
    return std::views::single(static_cast<char16_t>(u'A' + (digit - 10)));
  }
}

inline std::ranges::single_view<char16_t> bigint::digits_lower_u16(
  bigint const& digit
) noexcept {
  assert(digit < 36);

  if(digit < 10) {
    return std::views::single(static_cast<char16_t>(u'0' + digit));
  } else {
    return std::views::single(static_cast<char16_t>(u'a' + (digit - 10)));
  }
}

inline std::ranges::single_view<char32_t> bigint::digits_upper_u32(
  bigint const& digit
) noexcept {
  assert(digit < 36);

  if(digit < 10) {
    return std::views::single(static_cast<char32_t>(U'0' + digit));
  } else {
    return std::views::single(static_cast<char32_t>(U'A' + (digit - 10)));
  }
}

inline std::ranges::single_view<char32_t> bigint::digits_lower_u32(
  bigint const& digit
) noexcept {
  assert(digit < 36);

  if(digit < 10) {
    return std::views::single(static_cast<char32_t>(U'0' + digit));
  } else {
    return std::views::single(static_cast<char32_t>(U'a' + (digit - 10)));
  }
}

template<typename Char, typename Traits, std::invocable<bigint const&> Digits,
  typename Minus>
requires std::ranges::input_range<std::invoke_result_t<Digits&&, bigint const&>>
  && std::same_as<Char, std::ranges::range_value_t<
  std::invoke_result_t<Digits&&, bigint const&>>>
  && std::ranges::input_range<Minus&&> && std::same_as<Char,
    std::ranges::range_value_t<Minus&&>>
std::basic_string<Char, Traits> bigint::to_basic_string(
  bigint const& base,
  Digits&& digits,
  Minus&& minus
) const {
  assert(base >= 2);

  using digit_t = std::invoke_result_t<Digits&&, bigint const&>;

  std::basic_string<Char, Traits> result;

  if((bigint{1} << (sizeof(limb) * 8)) >= base) {
    // If the base is such that each limb is exactly a digit, then this value
    // will be zero. Otherwise, this will be the actual base value.
    limb const small_base{static_cast<limb>(base)};

    std::size_t const this_size{limbs_.size()};

    if(this_size == 0) {
      if(neg_) {
        if constexpr(specialization_of<digit_t, std::ranges::single_view>
            && std::ranges::sized_range<Minus&&>) {
          result.reserve(std::ranges::size(minus) + 1);
        }
        std::ranges::copy(minus, std::back_inserter(result));
      }
      std::ranges::copy(std::invoke(digits, 0), std::back_inserter(result));
      return result;
    }

    if(std::numeric_limits<std::size_t>::max() / (sizeof(limb) * 8) >= this_size
        && (small_base == 0 || std::has_single_bit(small_base))) {
      std::uint8_t const log_base{static_cast<std::uint8_t>(small_base == 0
        ? sizeof(limb) * 8 : std::countr_zero(small_base))};
      limb const mask{small_base - 1};
      std::size_t const bit_size{this_size * sizeof(limb) * 8};

      std::size_t pos{bit_size - (bit_size % log_base)};
      if(pos == bit_size) {
        pos -= log_base;
      }

      limb digit{(limbs_[pos / (sizeof(limb) * 8)]
        >> (pos % (sizeof(limb) * 8))) & mask};

      auto extract_digit{[&]() noexcept {
        std::size_t const limb_off{pos / (sizeof(limb) * 8)};
        std::size_t const sublimb_off{pos % (sizeof(limb) * 8)};

        digit = (limbs_[limb_off] >> sublimb_off) & mask;

        std::size_t const bits_that_fit{sizeof(limb) * 8 - sublimb_off};
        if(bits_that_fit < log_base) {
          digit |= (limbs_[limb_off + 1] & (mask >> bits_that_fit))
            << bits_that_fit;
        }
      }};

      while(digit == 0 && pos > 0) {
        pos -= log_base;
        extract_digit();
      }

      if constexpr(specialization_of<digit_t, std::ranges::single_view>) {
        std::size_t const num_digits{pos / log_base + 1};
        if(neg_) {
          if constexpr(std::ranges::sized_range<Minus&&>) {
            result.reserve(std::ranges::size(minus) + num_digits);
          }
        } else {
          result.reserve(num_digits);
        }
      }

      if(neg_) {
        std::ranges::copy(minus, std::back_inserter(result));
      }

      for(;;) {
        std::ranges::copy(std::invoke(digits, digit),
          std::back_inserter(result));

        if(pos == 0) {
          break;
        }

        pos -= log_base;
        extract_digit();
      }

      return result;
    }
  }

  bigint a{*this};
  bigint b;
  bigint* cur{&a};
  a.neg_ = false;

  auto append_digit{[&]() {
    // Since we produce digits from least to most significant, we need to
    // reverse the string at the end. But in that case, we need to append the
    // digit's characters in reverse order.
    auto&& digit_chars{std::invoke(digits, *cur)};
    if constexpr(std::ranges::bidirectional_range<digit_t>) {
      std::ranges::copy(digit_chars | std::views::reverse,
        std::back_inserter(result));
    } else {
      std::basic_string<Char, Traits> digit;
      std::ranges::copy(digit_chars, std::back_inserter(result));
      result.append(digit.crbegin(), digit.crend());
    }
  }};

  while(*cur >= base) {
    bigint* const other{cur == &a ? &b : &a};
    cur->divide(base, other);
    append_digit();
    cur = other;
  }

  append_digit();

  if(is_negative()) {
    if constexpr(std::ranges::bidirectional_range<Minus&&>) {
      std::ranges::copy(minus | std::views::reverse,
        std::back_inserter(result));
    } else {
      std::basic_string<Char, Traits> digit;
      std::ranges::copy(minus, std::back_inserter(digit));
      result.append(digit.crbegin(), digit.crend());
    }
  }

  std::reverse(result.begin(), result.end());

  return result;
}

extern template std::string bigint::to_basic_string<char,
  std::char_traits<char>, decltype(bigint::digits_upper),
  std::string_view&&>(bigint const&, decltype(bigint::digits_upper)&&,
  std::string_view&&) const;
extern template std::wstring bigint::to_basic_string<wchar_t,
  std::char_traits<wchar_t>, decltype(bigint::digits_upper_w),
  std::wstring_view&&>(bigint const&, decltype(bigint::digits_upper_w)&&,
  std::wstring_view&&) const;
extern template std::u16string bigint::to_basic_string<char16_t,
  std::char_traits<char16_t>, decltype(bigint::digits_upper_u16),
  std::u16string_view&&>(bigint const&, decltype(bigint::digits_upper_u16)&&,
  std::u16string_view&&) const;
extern template std::u32string bigint::to_basic_string<char32_t,
  std::char_traits<char32_t>, decltype(bigint::digits_upper_u32),
  std::u32string_view&&>(bigint const&, decltype(bigint::digits_upper_u32)&&,
  std::u32string_view&&) const;

/** @brief Outputs a bigint through an output stream.
 * @throws std::bad_alloc
 * @throws std::ios_base::failure */
template<typename Char, typename Traits>
requires one_of<Char, char, wchar_t>
std::basic_ostream<Char, Traits>& operator<<(
  std::basic_ostream<Char, Traits>& os,
  bigint const& num
) {
  typename std::basic_ostream<Char, Traits>::sentry const s{os};
  if(!s) {
    return os;
  }
  if(os.rdbuf() == nullptr) {
    os.setstate(std::ios_base::badbit);
    return os;
  }

  std::uint8_t const base{static_cast<std::uint8_t>(
    (os.flags() & std::ios_base::basefield) == std::ios_base::oct ? 8
    : (os.flags() & std::ios_base::basefield) == std::ios_base::dec ? 10 : 16)};

  auto const digits{[&os]() noexcept {
    if constexpr(std::same_as<Char, char>) {
      if((os.flags() & std::ios_base::uppercase) == 0) {
        return bigint::digits_lower;
      } else {
        return bigint::digits_upper;
      }
    } else {
      if((os.flags() & std::ios_base::uppercase) == 0) {
        return bigint::digits_lower_w;
      } else {
        return bigint::digits_upper_w;
      }
    }
  }()};
  auto const minus{[]() noexcept {
    // This is an empty string view, because the minus sign is produced
    // differently here
    if constexpr(std::same_as<Char, char>) {
      return std::string_view{};
    } else {
      return std::wstring_view{};
    }
  }()};

  using ustreamsize_t = std::make_unsigned_t<std::streamsize>;

  std::basic_string<Char, Traits> const str{num.to_basic_string<Char>(base,
    digits, minus)};
  ustreamsize_t const num_padding{[&]() noexcept -> ustreamsize_t {
    if(os.width() < 0 || str.size() >= util::asserted_cast<ustreamsize_t>(os.width())) {
      return 0;
    }

    ustreamsize_t result{static_cast<ustreamsize_t>(
      os.width() - str.size())};
    if(result > 0 && (num.is_negative()
        || (os.flags() & std::ios_base::showpos) != 0)) {
      --result;
    }

    if((os.flags() & std::ios_base::showbase) != 0) {
      if(base == 8) {
        if(result > 0 && num != 0) {
          --result;
        }
      } else if(base == 16) {
        if(result == 1) {
          result = 0;
        } else if(result > 1) {
          result -= 2;
        }
      }
    }

    return result;
  }()};

  if((os.flags() & std::ios_base::adjustfield) == std::ios_base::right) {
    for(std::size_t i{0}; i < num_padding; ++i) {
      if(Traits::eq_int_type(os.rdbuf()->sputc(os.fill()), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }
  }

  if(num.is_negative()) {
    if constexpr(std::same_as<Char, char>) {
      if(Traits::eq_int_type(os.rdbuf()->sputc('-'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    } else {
      if(Traits::eq_int_type(os.rdbuf()->sputc(L'-'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }
  } else if((os.flags() & std::ios_base::showpos) != 0) {
    if constexpr(std::same_as<Char, char>) {
      if(Traits::eq_int_type(os.rdbuf()->sputc('+'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    } else {
      if(Traits::eq_int_type(os.rdbuf()->sputc(L'+'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }
  }

  if(base == 16 && (os.flags() & std::ios_base::showbase) != 0) {
    if constexpr(std::same_as<Char, char>) {
      if(Traits::eq_int_type(os.rdbuf()->sputc('0'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    } else {
      if(Traits::eq_int_type(os.rdbuf()->sputc(L'0'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }

    if((os.flags() & std::ios_base::uppercase) != 0) {
      if constexpr(std::same_as<Char, char>) {
      if(Traits::eq_int_type(os.rdbuf()->sputc('X'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    } else {
      if(Traits::eq_int_type(os.rdbuf()->sputc(L'X'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }
    } else {
      if constexpr(std::same_as<Char, char>) {
        if(Traits::eq_int_type(os.rdbuf()->sputc('x'), Traits::eof())) {
          os.setstate(std::ios_base::badbit);
          return os;
        }
      } else {
        if(Traits::eq_int_type(os.rdbuf()->sputc(L'x'), Traits::eof())) {
          os.setstate(std::ios_base::badbit);
          return os;
        }
      }
    }
  }

  if((os.flags() & std::ios_base::adjustfield) == std::ios_base::internal) {
    for(std::size_t i{0}; i < num_padding; ++i) {
      if(Traits::eq_int_type(os.rdbuf()->sputc(os.fill()), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }
  }

  if(base == 8 && (os.flags() & std::ios_base::showbase) != 0 && num != 0) {
    if constexpr(std::same_as<Char, char>) {
      if(Traits::eq_int_type(os.rdbuf()->sputc('0'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    } else {
      if(Traits::eq_int_type(os.rdbuf()->sputc(L'0'), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }
  }

  if(str.size() <= util::make_unsigned(
      std::numeric_limits<std::streamsize>::max())) {
    std::streamsize const written_count{
      os.rdbuf()->sputn(str.data(), str.size())};
    assert(written_count >= 0);
    if(util::asserted_cast<ustreamsize_t>(written_count) != str.size()) {
      os.setstate(std::ios_base::badbit);
      return os;
    }
  } else {
    for(Char ch: str) {
      if(Traits::eq_int_type(os.rdbuf()->sputc(ch), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }
  }

  if((os.flags() & std::ios_base::adjustfield) == std::ios_base::left) {
    for(std::size_t i{0}; i < num_padding; ++i) {
      if(Traits::eq_int_type(os.rdbuf()->sputc(os.fill()), Traits::eof())) {
        os.setstate(std::ios_base::badbit);
        return os;
      }
    }
  }

  os.width(0);
  return os;
}

extern template std::ostream& operator<<(std::ostream&, bigint const&);
extern template std::wostream& operator<<(std::wostream&, bigint const&);

// String to bigint conversions out of body ////////////////////////////////////

template<typename S, bool SkipApos>
requires std::ranges::input_range<S&&>
  && one_of<std::ranges::range_value_t<S&&>,
  char, char8_t, wchar_t, char16_t, char32_t>
std::pair<
  std::optional<bigint>,
  std::ranges::iterator_t<S&&>
> bigint::maybe_from_string(S&& str, std::uint8_t base) {
  assert(base >= 2 && base <= 36 || base == 0);
  using char_t = std::ranges::range_value_t<S&&>;

  constexpr auto plus_sign{[]() noexcept {
    if constexpr(std::same_as<char_t, char8_t>) {
      return u8'+';
    } else if constexpr(std::same_as<char_t, char>) {
      return '+';
    } else if constexpr(std::same_as<char_t, wchar_t>) {
      return L'+';
    } else if constexpr(std::same_as<char_t, char16_t>) {
      return u'+';
    } else {
      return U'+';
    }
  }()};
  constexpr auto minus_sign{[]() noexcept {
    if constexpr(std::same_as<char_t, char8_t>) {
      return u8'-';
    } else if constexpr(std::same_as<char_t, char>) {
      return '-';
    } else if constexpr(std::same_as<char_t, wchar_t>) {
      return L'-';
    } else if constexpr(std::same_as<char_t, char16_t>) {
      return u'-';
    } else {
      return U'-';
    }
  }()};
  constexpr auto zero{[]() noexcept {
    if constexpr(std::same_as<char_t, char8_t>) {
      return u8'0';
    } else if constexpr(std::same_as<char_t, char>) {
      return '0';
    } else if constexpr(std::same_as<char_t, wchar_t>) {
      return L'0';
    } else if constexpr(std::same_as<char_t, char16_t>) {
      return u'0';
    } else {
      return U'0';
    }
  }()};
  constexpr auto x_lower{[]() noexcept {
    if constexpr(std::same_as<char_t, char8_t>) {
      return u8'x';
    } else if constexpr(std::same_as<char_t, char>) {
      return 'x';
    } else if constexpr(std::same_as<char_t, wchar_t>) {
      return L'x';
    } else if constexpr(std::same_as<char_t, char16_t>) {
      return u'x';
    } else {
      return U'x';
    }
  }()};
  constexpr auto x_upper{[]() noexcept {
    if constexpr(std::same_as<char_t, char8_t>) {
      return u8'X';
    } else if constexpr(std::same_as<char_t, char>) {
      return 'X';
    } else if constexpr(std::same_as<char_t, wchar_t>) {
      return L'X';
    } else if constexpr(std::same_as<char_t, char16_t>) {
      return u'X';
    } else {
      return U'X';
    }
  }()};
  constexpr auto apos{[]() noexcept {
    if constexpr(std::same_as<char_t, char8_t>) {
      return u8'\'';
    } else if constexpr(std::same_as<char_t, char>) {
      return '\'';
    } else if constexpr(std::same_as<char_t, wchar_t>) {
      return L'\'';
    } else if constexpr(std::same_as<char_t, char16_t>) {
      return u'\'';
    } else {
      return U'\'';
    }
  }()};

  std::uint8_t real_base{base == 0 ? static_cast<std::uint8_t>(10) : base};

  auto parse_digit{[&](char_t ch) noexcept {
    std::uint8_t result;
    if(ch >= zero && ch <= zero + 9) {
      result = util::asserted_cast<std::uint8_t>(ch - zero);
    } else {
      if constexpr(std::same_as<char_t, wchar_t>) {
        switch(ch) {
          case L'A': case L'a': result = 10; break;
          case L'B': case L'b': result = 11; break;
          case L'C': case L'c': result = 12; break;
          case L'D': case L'd': result = 13; break;
          case L'E': case L'e': result = 14; break;
          case L'F': case L'f': result = 15; break;
          case L'G': case L'g': result = 16; break;
          case L'H': case L'h': result = 17; break;
          case L'I': case L'i': result = 18; break;
          case L'J': case L'j': result = 19; break;
          case L'K': case L'k': result = 20; break;
          case L'L': case L'l': result = 21; break;
          case L'M': case L'm': result = 22; break;
          case L'N': case L'n': result = 23; break;
          case L'O': case L'o': result = 24; break;
          case L'P': case L'p': result = 25; break;
          case L'Q': case L'q': result = 26; break;
          case L'R': case L'r': result = 27; break;
          case L'S': case L's': result = 28; break;
          case L'T': case L't': result = 29; break;
          case L'U': case L'u': result = 30; break;
          case L'V': case L'v': result = 31; break;
          case L'W': case L'w': result = 32; break;
          case L'X': case L'x': result = 33; break;
          case L'Y': case L'y': result = 34; break;
          case L'Z': case L'z': result = 35; break;
          default: result = 255; break;
        }
      } else if constexpr(std::same_as<char_t, char>) {
        switch(ch) {
          case 'A': case 'a': result = 10; break;
          case 'B': case 'b': result = 11; break;
          case 'C': case 'c': result = 12; break;
          case 'D': case 'd': result = 13; break;
          case 'E': case 'e': result = 14; break;
          case 'F': case 'f': result = 15; break;
          case 'G': case 'g': result = 16; break;
          case 'H': case 'h': result = 17; break;
          case 'I': case 'i': result = 18; break;
          case 'J': case 'j': result = 19; break;
          case 'K': case 'k': result = 20; break;
          case 'L': case 'l': result = 21; break;
          case 'M': case 'm': result = 22; break;
          case 'N': case 'n': result = 23; break;
          case 'O': case 'o': result = 24; break;
          case 'P': case 'p': result = 25; break;
          case 'Q': case 'q': result = 26; break;
          case 'R': case 'r': result = 27; break;
          case 'S': case 's': result = 28; break;
          case 'T': case 't': result = 29; break;
          case 'U': case 'u': result = 30; break;
          case 'V': case 'v': result = 31; break;
          case 'W': case 'w': result = 32; break;
          case 'X': case 'x': result = 33; break;
          case 'Y': case 'y': result = 34; break;
          case 'Z': case 'z': result = 35; break;
          default: result = 255; break;
        }
      } else {
        constexpr auto a_upper{[]() noexcept {
          if constexpr(std::same_as<char_t, char>) {
            return u8'A';
          } else if constexpr(std::same_as<char_t, char16_t>) {
            return u'A';
          } else {
            return U'A';
          }
        }()};
        constexpr auto a_lower{[]() noexcept {
          if constexpr(std::same_as<char_t, char>) {
            return u8'a';
          } else if constexpr(std::same_as<char_t, char16_t>) {
            return u'a';
          } else {
            return U'a';
          }
        }()};

        if(a_lower <= ch && ch <= a_lower + 25) {
          result = util::asserted_cast<std::uint8_t>(ch - a_lower + 10);
        } else if(a_upper <= ch && ch <= a_upper + 25) {
          result = util::asserted_cast<std::uint8_t>(ch - a_upper + 10);
        } else {
          result = 255;
        }
      }
    }

    if(result >= real_base) {
      result = 255;
    }
    return result;
  }};

  std::ranges::iterator_t<S&&> it{std::ranges::begin(str)};
  std::ranges::sentinel_t<S&&> const iend{std::ranges::end(str)};

  if(it == iend) {
    return {std::nullopt, std::move(it)};
  }

  char_t const sign{*it};
  char_t next_char;
  if(sign == plus_sign || sign == minus_sign) {
    if(++it == iend) {
      if constexpr(std::ranges::forward_range<S&&>) {
        return {std::nullopt, std::move(it)};
      } else {
        return {std::nullopt, std::ranges::begin(str)};
      }
    }
    next_char = *it;
  } else {
    next_char = sign;
  }

  std::uint8_t next_digit{parse_digit(next_char)};

  if(base == 0 && next_digit == 0) {
    real_base = 8;

    if(++it == iend) {
      return {0, std::move(it)};
    }
    next_char = *it;

    std::ranges::iterator_t<S&&> const iold{std::ranges::forward_range<S&&>
      ? it : std::ranges::iterator_t<S&&>{}};

    if(next_char == x_upper || next_char == x_lower) {
      real_base = 16;
      if(++it == iend) {
        if constexpr(std::ranges::forward_range<S&&>) {
          return {0, std::move(iold)};
        } else {
          return {0, std::move(it)};
        }
      }
      next_char = *it;
      next_digit = parse_digit(next_char);

      if(next_digit == 255) {
        if constexpr(std::ranges::forward_range<S&&>) {
          return {0, std::move(iold)};
        } else {
          return {0, std::move(it)};
        }
      }
    } else {
      next_digit = parse_digit(next_char);
    }
  }

  if(next_digit == 255) {
    if constexpr(std::ranges::forward_range<S&&>) {
      return {std::nullopt, std::ranges::begin(str)};
    } else {
      return {std::nullopt, std::move(it)};
    }
  }

  std::uint8_t const log_base{static_cast<std::uint8_t>(
    std::countr_zero(real_base))};

  bigint result;

  auto common_algo{[&]() -> std::pair<std::optional<bigint>,
      std::ranges::iterator_t<S&&>> {
    do {
      if(std::has_single_bit(real_base)) {
        result <<= log_base;
        result |= next_digit;
      } else {
        result *= real_base;
        result += next_digit;
      }

      do {
        if(++it == iend) {
          if(sign == minus_sign) {
            result.negate();
          }
          return {std::move(result), std::move(it)};
        }

        next_char = *it;
      } while(SkipApos && next_char == apos);
      next_digit = parse_digit(next_char);
    } while(next_digit != 255);

    if(sign == minus_sign) {
      result.negate();
    }

    return {std::move(result), std::move(it)};
  }};

  if(std::ranges::forward_range<S&&> && std::has_single_bit(real_base)) {
    using num_t = std::common_type_t<std::make_unsigned_t<
      std::ranges::range_difference_t<S&&>>, std::size_t>;
    num_t num_digits{1};

    {
      std::ranges::iterator_t<S&&> ifwd{it};
      for(;;) {
        ++ifwd;
        if(ifwd == iend) {
          break;
        }
        if(parse_digit(*ifwd) != 255) {
          if(num_digits >= std::numeric_limits<num_t>::max()) {
            return common_algo();
          }
          ++num_digits;
        } else if(!SkipApos || *ifwd != apos) {
          break;
        }
      }
    }

    if(std::numeric_limits<num_t>::max() / log_base < num_digits) {
      return common_algo();
    }
    result.limbs_.resize(num_digits * log_base / (sizeof(limb) * 8)
      + (num_digits * log_base % (sizeof(limb) * 8) == 0 ? 0 : 1), 0);

    num_t pos{(num_digits - 1) * log_base};
    do {
      num_t const limb_off{pos / (sizeof(limb) * 8)};
      num_t const sublimb_off{pos % (sizeof(limb) * 8)};
      if(sublimb_off + log_base <= sizeof(limb) * 8) {
        assert(result.limbs_.size() > limb_off);
        result.limbs_[limb_off]
          |= (static_cast<limb>(next_digit) << sublimb_off);
      } else {
        assert(result.limbs_.size() > limb_off + 1);
        std::uint8_t const mask{static_cast<std::uint8_t>((1
          << (sizeof(limb) * 8 - sublimb_off)) - 1)};
        result.limbs_[limb_off] |= static_cast<limb>(next_digit & mask)
          << sublimb_off;
        result.limbs_[limb_off + 1] |= static_cast<limb>(next_digit & ~mask);
      }

      pos -= log_base;
      do {
        if(++it == iend) {
          if(sign == minus_sign) {
            result.negate();
          }
          return {std::move(result), std::move(it)};
        }

        next_char = *it;
      } while(SkipApos && next_char == apos);
      next_digit = parse_digit(next_char);
    } while(next_digit != 255);

    if(sign == minus_sign) {
      result.negate();
    }

    return {std::move(result), std::move(it)};
  } else {
    return common_algo();
  }
}

namespace bigint_literals {

/** @throws std::bad_alloc */
inline bigint operator""_big(gsl::czstring str) {
  auto [num, it] = bigint::maybe_from_string<util::zstring_view, true>(
    util::zstring_view{str}, 0);
  assert(num);
  assert(*it == '\0');
  return std::move(*num);
}

} // bigint_literals

/** @brief Inputs a bigint through an input stream.
 *
 * The stream's base setting determines the input base. In base 16, the "0x" or
 * "0X" prefix is optionally recognized.
 *
 * @throws std::ios_base::failure
 * @throws std::bad_alloc */
template<typename Char, typename Traits>
requires one_of<Char, char, wchar_t>
std::basic_istream<Char, Traits>& operator>>(
  std::basic_istream<Char, Traits>& is,
  bigint& num
) {
  typename std::basic_istream<Char, Traits>::sentry const s{is};
  if(!s) {
    return is;
  }
  if(is.rdbuf() == nullptr) {
    is.setstate(std::ios_base::badbit);
    return is;
  }

  std::uint8_t const base{static_cast<std::uint8_t>(
    (is.flags() & std::ios_base::basefield) == std::ios_base::hex ? 16 :
    (is.flags() & std::ios_base::basefield) == std::ios_base::oct ? 8 : 10)};
  bool should_negate{false};

  if(base == 16) {
    // Eat the "0x" or "0X" prefix, if it exists

    auto const plus_sign{[]() noexcept {
      if constexpr(std::same_as<Char, char>) {
        return '+';
      } else {
        return L'+';
      }
    }()};
    auto const minus_sign{[]() noexcept {
      if constexpr(std::same_as<Char, char>) {
        return '-';
      } else {
        return L'-';
      }
    }()};
    auto const zero{[]() noexcept {
      if constexpr(std::same_as<Char, char>) {
        return '0';
      } else {
        return L'0';
      }
    }()};
    auto const x_upper{[]() noexcept {
      if constexpr(std::same_as<Char, char>) {
        return 'X';
      } else {
        return L'X';
      }
    }()};
    auto const x_lower{[]() noexcept {
      if constexpr(std::same_as<Char, char>) {
        return 'x';
      } else {
        return L'x';
      }
    }()};

    typename Traits::int_type ch{is.rdbuf()->sbumpc()};
    if(Traits::eq_int_type(ch, Traits::eof())) {
      is.setstate(std::ios_base::failbit);
      return is;
    }

    if(Traits::eq(Traits::to_char_type(ch), plus_sign)
        || Traits::eq(Traits::to_char_type(ch), minus_sign)) {
      if(Traits::eq(Traits::to_char_type(ch), minus_sign)) {
        should_negate = true;
      }

      ch = is.rdbuf()->sbumpc();
      if(Traits::eq_int_type(ch, Traits::eof())) {
        is.setstate(std::ios_base::failbit);
        return is;
      }
    }

    if(Traits::eq(Traits::to_char_type(ch), zero)) {
      ch = is.rdbuf()->sbumpc();
      if(Traits::eq_int_type(ch, Traits::eof())) {
        is.setstate(std::ios_base::failbit);
        return is;
      }

      if(!Traits::eq(Traits::to_char_type(ch), x_upper)
          && !Traits::eq(Traits::to_char_type(ch), x_lower)) {
        if(Traits::eq_int_type(Traits::eof(), is.rdbuf()->sungetc())) {
          is.setstate(std::ios_base::failbit);
          return is;
        }

        num = 0;
        return is;
      }
    } else {
      if(Traits::eq_int_type(Traits::eof(), is.rdbuf()->sungetc())) {
        is.setstate(std::ios_base::failbit);
        return is;
      }
    }
  }

  auto [result, it] = bigint::maybe_from_string(std::ranges::subrange{
    std::istreambuf_iterator<Char>{is.rdbuf()},
    std::istreambuf_iterator<Char>{}}, base);

  if(!result) {
    is.setstate(std::ios_base::failbit);
    return is;
  }

  num = std::move(*result);
  if(should_negate) {
    num.negate();
  }
  return is;
}

extern template std::istream& operator>>(std::istream&, bigint&);
extern template std::wistream& operator>>(std::wistream&, bigint&);

// Bytes to bigint conversion out of body //////////////////////////////////////

template<typename S>
requires std::ranges::input_range<S&&>
  && std::same_as<std::uint8_t, std::ranges::range_value_t<S&&>>
bigint bigint::from_bytes(S&& str) {
  bigint result;

  if constexpr(std::ranges::sized_range<S&&>) {
    result.limbs_.reserve(std::ranges::size(str) / sizeof(limb));
  }

  std::size_t i{0};
  limb new_limb{0};
  for(std::uint8_t const byte: str) {
    new_limb |= (static_cast<limb>(byte) << ((i % sizeof(limb)) * 8));
    if(++i % sizeof(limb) == 0) {
      result.limbs_.push_back(new_limb);
      new_limb = 0;
    }
  }
  if(i % sizeof(limb) != 0) {
    result.limbs_.push_back(new_limb);
  }

  return result;
}

} // util

// Limits //////////////////////////////////////////////////////////////////////

template<>
class std::numeric_limits<util::bigint> {
public:
  static constexpr bool is_specialized{true};
  static constexpr bool is_signed{true};
  static constexpr bool is_integer{true};
  static constexpr bool is_exact{true};
  static constexpr bool has_infinity{false};
  static constexpr bool has_quiet_NaN{false};
  static constexpr bool has_signaling_Nan{false};
  static constexpr bool has_denorm{false};
  static constexpr bool has_denorm_loss{false};
  static constexpr ::std::float_round_style round_style{
    ::std::round_toward_zero};
  static constexpr bool is_iec559{false};
  static constexpr bool is_bounded{false};
  static constexpr bool is_modulo{false};
  static constexpr int digits{0};
  static constexpr int digits10{0};
  static constexpr int max_digits10{0};
  static constexpr int radix{2};
  static constexpr int min_exponent{0};
  static constexpr int min_exponent10{0};
  static constexpr int max_exponent{0};
  static constexpr int max_exponent10{0};
  static constexpr bool traps{true};
  static constexpr bool tinyness_before{false};

  [[nodiscard]] static ::util::bigint min() noexcept { return 0; }
  [[nodiscard]] static ::util::bigint lowest() noexcept { return 0; }
  [[nodiscard]] static ::util::bigint max() noexcept { return 0; }
  [[nodiscard]] static ::util::bigint epsilon() noexcept { return 0; }
  [[nodiscard]] static ::util::bigint round_error() noexcept { return 0; }
  [[nodiscard]] static ::util::bigint infinity() noexcept { return 0; }
  [[nodiscard]] static ::util::bigint quiet_NaN() noexcept { return 0; }
  [[nodiscard]] static ::util::bigint signaling_NaN() noexcept { return 0; }
  [[nodiscard]] static ::util::bigint denorm_min() noexcept { return 0; }
};

#endif
