#ifndef UTIL_FUNCTION_HPP_INCLUDED_90W9ZGM0W595YM447HYQAZ2K5
#define UTIL_FUNCTION_HPP_INCLUDED_90W9ZGM0W595YM447HYQAZ2K5

#include "util/type_traits.hpp"
#include <gsl/gsl>
#include <cstddef>
#include <exception>
#include <functional>
#include <new>
#include <type_traits>
#include <typeinfo>
#include <utility>

namespace util {

/** @brief An exception class that wraps exceptions thrown by util::function's
 * target. */
class function_target_exception: public std::exception {
public:
  [[nodiscard]] virtual gsl::czstring what() const noexcept override;
};

namespace detail_ {

template<typename T>
struct function_param_types {};
template<typename R, typename A>
struct function_param_types<R(A)> {
  using argument_type [[deprecated]] = A;
};
template<typename R, typename A, typename B>
struct function_param_types<R(A, B)> {
  using first_argument_type [[deprecated]] = A;
  using second_argument_type [[deprecated]] = B;
};

template<typename T>
struct function_deduced_sig {};
template<typename T>
using function_deduced_sig_t = function_deduced_sig<T>::type;

template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...)> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) &> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) const> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) const &> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) volatile> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) volatile &> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) const volatile> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) const volatile &> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) noexcept> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) & noexcept> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) const noexcept> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) const & noexcept> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) volatile noexcept> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) volatile & noexcept> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) const volatile noexcept> {
  using type = R(Args...);
};
template<typename R, typename C, typename... Args>
struct function_deduced_sig<R (C::*)(Args...) const volatile & noexcept> {
  using type = R(Args...);
};

} // detail_

/** @brief A wrapper for std::function that throws properly wrapped exceptions.
 *
 * This class mimics the std::function API, except that whenever the underlying
 * API throws an exception, it is wrapped in a function_target_exception before
 * being thrown out of here. */
template<typename>
class function;

template<typename R, typename... Args>
function(R (*)(Args...)) -> function<R(Args...)>;
template<typename F,
  typename Sig = detail_::function_deduced_sig_t<decltype(&F::operator())>>
function(F) -> function<Sig>;

template<typename R, typename... Args>
class function<R(Args...)>: public detail_::function_param_types<R(Args...)> {
private:
  std::function<R(Args...)> fn_;

public:
  using result_type = R;

  function() noexcept = default;
  function(std::nullptr_t) noexcept: fn_{nullptr} {}

  function(function const& other) try: fn_{other.fn_} {
  } catch(std::bad_alloc const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(function_target_exception{});
  }

  function(function&& other)
      noexcept(std::is_nothrow_move_constructible_v<std::function<R(Args...)>>)
      try: fn_{std::move(other.fn_)} {
  } catch(std::bad_alloc const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(function_target_exception{});
  }

  function(std::function<R(Args...)> other)
      noexcept(std::is_nothrow_move_constructible_v<R(Args...)>)
      try: fn_{std::move(other)} {
  } catch(std::bad_alloc const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(function_target_exception{});
  }

  template<typename F>
  requires
    (!one_of<std::remove_cvref_t<F>, function, std::function<R(Args...)>>)
    && std::is_invocable_r_v<R, std::remove_cvref_t<F>&, Args...>
    && std::is_copy_constructible_v<F> && std::is_nothrow_destructible_v<F>
  function(F&& f) noexcept(function_pointer<std::remove_cvref_t<F>>
      || specialization_of<std::remove_cvref_t<F>, std::reference_wrapper>)
      try: fn_{std::forward<F>(f)} {
  } catch(std::bad_alloc const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(function_target_exception{});
  }

  function& operator=(function const& other) {
    try {
      fn_ = other.fn_;
    } catch(std::bad_alloc const&) {
      throw;
    } catch(...) {
      std::throw_with_nested(function_target_exception{});
    }
    return *this;
  }

  function& operator=(function&& other) {
    try {
      fn_ = std::move(other.fn_);
    } catch(std::bad_alloc const&) {
      throw;
    } catch(...) {
      std::throw_with_nested(function_target_exception{});
    }
    return *this;
  }

  function& operator=(std::nullptr_t) noexcept {
    fn_ = nullptr;
    return *this;
  }

  function& operator=(std::function<R(Args...)> other) {
    try {
      fn_ = std::move(other);
    } catch(std::bad_alloc const&) {
      throw;
    } catch(...) {
      std::throw_with_nested(function_target_exception{});
    }
    return *this;
  }

  template<typename F>
  requires
    (!one_of<std::remove_cvref_t<F>, function, std::function<R(Args...)>>)
    && std::is_invocable_r_v<R, std::remove_cvref_t<F>&, Args...>
    && std::is_copy_constructible_v<F> && std::is_nothrow_destructible_v<F>
  function& operator=(F&& f) noexcept(function_pointer<std::remove_cvref_t<F>>
      || specialization_of<std::remove_cvref_t<F>, std::reference_wrapper>) {
    try {
      fn_ = std::forward<F>(f);
    } catch(std::bad_alloc const&) {
      throw;
    } catch(...) {
      std::throw_with_nested(function_target_exception{});
    }
    return *this;
  }

  void swap(function& other) noexcept {
    fn_.swap(other.fn_);
  }

  ~function() noexcept = default;

  [[nodiscard]] explicit operator bool() const noexcept {
    return static_cast<bool>(fn_);
  }

  R operator()(Args... args) {
    try {
      return fn_(std::forward<Args>(args)...);
    } catch(std::bad_alloc const&) {
      throw;
    } catch(std::bad_function_call const&) {
      throw;
    } catch(...) {
      std::throw_with_nested(function_target_exception{});
    }
  }

  [[nodiscard]] std::type_info const& target_type() const noexcept {
    return fn_.target_type();
  }

  template<typename T>
  [[nodiscard]] T* target() noexcept {
    return fn_.template target<T>();
  }
  template<typename T>
  [[nodiscard]] T const* target() const noexcept {
    return fn_.template target<T>();
  }

  [[nodiscard]] std::function<R(Args...)>& base() & noexcept {
    return fn_;
  }
  [[nodiscard]] std::function<R(Args...)>&& base() && noexcept {
    return fn_;
  }
  [[nodiscard]] std::function<R(Args...)> const& base() const& noexcept {
    return fn_;
  }

  [[nodiscard]] friend bool operator==(
    function<R(Args...)> const& f,
    std::nullptr_t
  ) noexcept {
    return f.fn_ == nullptr;
  }
  [[nodiscard]] friend bool operator==(
    std::nullptr_t,
    function<R(Args...)> const& f
  ) noexcept {
    return nullptr == f.fn_;
  }
};

template<typename R, typename... Args>
void swap(function<R(Args...)>& a, function<R(Args...)>& b) noexcept {
  a.swap(b);
}

} // util

#endif
