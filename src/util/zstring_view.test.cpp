#include <boost/test/unit_test.hpp>

#include "util/zstring_view.hpp"
#include <algorithm>
#include <concepts>
#include <ranges>
#include <string_view>

using namespace std::string_view_literals;

static_assert(std::ranges::contiguous_range<util::u8zstring_view>);
static_assert(std::ranges::view<util::u8zstring_view>);
static_assert(std::same_as<std::ranges::range_value_t<util::u8zstring_view>,
  char8_t>);

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(zstring_view)

BOOST_AUTO_TEST_CASE(basic) {
  BOOST_TEST(std::ranges::equal(util::u8zstring_view{u8"foo"}, u8"foo"sv));
}

BOOST_AUTO_TEST_SUITE_END() // zstring_view
BOOST_AUTO_TEST_SUITE_END() // test_util
