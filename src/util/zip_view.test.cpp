#include <boost/test/unit_test.hpp>

#include "util/fancy_tuple.hpp"
#include "util/zip_view.hpp"
#include <algorithm>
#include <concepts>
#include <cstdint>
#include <iterator>
#include <ranges>
#include <string>
#include <vector>

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(zip_view)

BOOST_AUTO_TEST_CASE(zip_view) {
  std::uint64_t const nums[2]{100, 200};
  std::vector<std::string> const strs{"one", "two", "three"};

  auto const view{util::zip(nums, strs)};
  using view_t = decltype(view);
  static_assert(std::ranges::random_access_range<view_t>);
  static_assert(std::same_as<std::ranges::range_value_t<view_t>,
    util::fancy_tuple<std::uint64_t, std::string>>);
  static_assert(std::same_as<std::ranges::range_reference_t<view_t>,
    util::fancy_tuple<std::uint64_t const&, std::string const&>>);
  static_assert(std::ranges::common_range<view_t>);

  std::vector<std::tuple<std::uint64_t, std::string>> result;
  std::ranges::copy(view, std::back_inserter(result));

  std::tuple<std::uint64_t, std::string> test[]{{100, "one"}, {200, "two"}};
  BOOST_TEST(std::ranges::equal(result, test));
}

BOOST_AUTO_TEST_SUITE_END() // zip_view
BOOST_AUTO_TEST_SUITE_END() // test_util
