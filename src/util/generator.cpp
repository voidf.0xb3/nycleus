#include "util/generator.hpp"

static_assert(sizeof(util::detail_::generator_allocation_unit)
  == __STDCPP_DEFAULT_NEW_ALIGNMENT__);
static_assert(alignof(util::detail_::generator_allocation_unit)
  == __STDCPP_DEFAULT_NEW_ALIGNMENT__);
