#ifndef UTIL_INTRUSIVE_FORWARD_LIST_HPP_INCLUDED_8H31TFMKEZY772UMAC1K8BH2N
#define UTIL_INTRUSIVE_FORWARD_LIST_HPP_INCLUDED_8H31TFMKEZY772UMAC1K8BH2N

#include "util/iterator_facade.hpp"
#include <cassert>
#include <concepts>
#include <functional>
#include <iterator>
#include <type_traits>
#include <utility>

namespace util {

/** @brief The base class for types that can be elements of an intrusive forward
 * list. */
class intrusive_forward_list_hook;

/** @brief A singly-linked list consisting of externally supplied elements.
 *
 * Unlike an ordinary linked list, the intrusive list does not allocate its
 * elements and does not control their lifetimes. Instead, client code is
 * expected to derive the element type from `intrusive_list_hook`, which is then
 * used to connect the element to the list.
 *
 * If a list element is destroyed before the list object is destroyed, the
 * behavior is undefined. If the list object is destroyed while it has elements,
 * the element objects remain alive (but are no longer part of any list).
 *
 * An iterator to an element is invalidated only when that element ceases to be
 * an element of its containing list or when the list object is destroyed. */
template<std::derived_from<intrusive_forward_list_hook> T>
class intrusive_forward_list;

class intrusive_forward_list_hook {
private:
  intrusive_forward_list_hook* next;

  template<std::derived_from<intrusive_forward_list_hook> T>
  friend class intrusive_forward_list;

public:
  intrusive_forward_list_hook() noexcept = default;
  ~intrusive_forward_list_hook() noexcept = default;

  // List elements can be copyable and movable, but copying and/or moving them
  // does not affect their list memberships.

  intrusive_forward_list_hook(intrusive_forward_list_hook const&) noexcept {}
  intrusive_forward_list_hook(intrusive_forward_list_hook&&) noexcept {}
  // NOLINTNEXTLINE(bugprone-unhandled-self-assignment)
  intrusive_forward_list_hook& operator=(intrusive_forward_list_hook const&)
      noexcept {
    return *this;
  }
  intrusive_forward_list_hook& operator=(intrusive_forward_list_hook&&)
      noexcept {
    return *this;
  }
};

template<std::derived_from<intrusive_forward_list_hook> T>
class intrusive_forward_list {
private:
  intrusive_forward_list_hook hook_;

  using ifl = intrusive_forward_list;

  [[nodiscard]] static intrusive_forward_list_hook& hook(T& t) noexcept {
    return t;
  }

  [[nodiscard]] static intrusive_forward_list_hook const& hook(T const& t)
      noexcept {
    return t;
  }

public:
  using value_type = T;

  /** @brief Creates an empty list. */
  intrusive_forward_list() noexcept {
    hook_.next = nullptr;
  }

  /** @brief Creates a list with the given elements.
   *
   * If iteration over the range throws an exception, the elements are not
   * destroyed in any way; if that is undesirable, you can create an empty list
   * and then assign the range later. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T&>
  explicit intrusive_forward_list(Iterator it, Sentinel iend)
      noexcept(noexcept(it != iend) && noexcept(++it) && noexcept(*it)):
      intrusive_forward_list{} {
    this->insert_after(before_begin(), std::move(it), std::move(iend));
  }

  /** @brief Destroys the list.
   *
   * The list's elements are not destroyed. If it is necessary to destroy them,
   * that needs to be done separately. */
  ~intrusive_forward_list() noexcept = default;

  intrusive_forward_list(intrusive_forward_list const&) = delete;
  intrusive_forward_list& operator=(intrusive_forward_list const&) = delete;

  void swap(intrusive_forward_list& other) noexcept {
    std::swap(hook_.next, other.hook_.next);
  }

  /** @brief Move-constructs the list.
   *
   * The moved-from list becomes empty. */
  intrusive_forward_list(intrusive_forward_list&& other) noexcept:
      intrusive_forward_list{} {
    this->swap(other);
  }

  /** @brief Move-assigns the list.
   *
   * The old content of this list is assigned to the other list. (In other
   * words, this is equivalent to a swap.) */
  intrusive_forward_list& operator=(intrusive_forward_list&& other) noexcept {
    this->swap(other);
    return *this;
  }

  [[nodiscard]] bool empty() const noexcept {
    return hook_.next == nullptr;
  }

  // Element access ////////////////////////////////////////////////////////////

  [[nodiscard]] T& front() noexcept {
    assert(!empty());
    return static_cast<T&>(*hook_.next);
  }

  [[nodiscard]] T const& front() const noexcept {
    assert(!empty());
    return static_cast<T const&>(*hook_.next);
  }

private:
  class const_iterator_core;

  class iterator_core {
  private:
    intrusive_forward_list_hook* cur_;

    explicit iterator_core(intrusive_forward_list_hook* cur)
      noexcept: cur_{cur} {}

    friend class intrusive_forward_list;
    friend class const_iterator_core;

  public:
    iterator_core() noexcept = default;

  protected:
    [[nodiscard]] T& dereference() const noexcept {
      assert(cur_);
      return static_cast<T&>(*cur_);
    }

    [[nodiscard]] bool equal_to(iterator_core other) const noexcept {
      return cur_ == other.cur_;
    }

    void increment() noexcept {
      assert(cur_);
      cur_ = cur_->next;
    }
  };

public:
  using iterator = util::iterator_facade<iterator_core>;
  static_assert(std::forward_iterator<iterator>);

private:
  class const_iterator_core {
  private:
    intrusive_forward_list_hook const* cur_;

    explicit const_iterator_core(intrusive_forward_list_hook const* cur)
      noexcept: cur_{cur} {}

    friend class intrusive_forward_list;

  public:
    const_iterator_core() noexcept = default;
    const_iterator_core(iterator other) noexcept: cur_{other.cur_} {}

  protected:
    [[nodiscard]] T const& dereference() const noexcept {
      assert(cur_);
      return static_cast<T const&>(*cur_);
    }

    [[nodiscard]] bool equal_to(const_iterator_core other) const noexcept {
      return cur_ == other.cur_;
    }

    void increment() noexcept {
      assert(cur_);
      cur_ = cur_->next;
    }
  };

public:
  using const_iterator = util::iterator_facade<const_iterator_core>;
  static_assert(std::forward_iterator<const_iterator>);

  /** @brief Returns an iterator to before the beginning of the list.
   *
   * The iterator cannot be dereferenced. Incrementing the iterator produces an
   * iterator to the first element (or a past-the-end iterator for an empty
   * list). This can be passed to various insertion or erasure functions to
   * indicate a position at the beginning of the list. */
  [[nodiscard]] iterator before_begin() noexcept { return iterator{&hook_}; }

  /** @brief Returns an iterator to before the beginning of the list.
   *
   * The iterator cannot be dereferenced. Incrementing the iterator produces an
   * iterator to the first element (or a past-the-end iterator for an empty
   * list). This can be passed to various insertion or erasure functions to
   * indicate a position at the beginning of the list. */
  [[nodiscard]] const_iterator before_begin() const noexcept {
    return const_iterator{&hook_};
  }

  /** @brief Returns an iterator to before the beginning of the list.
   *
   * The iterator cannot be dereferenced. Incrementing the iterator produces an
   * iterator to the first element (or a past-the-end iterator for an empty
   * list). This can be passed to various insertion or erasure functions to
   * indicate a position at the beginning of the list. */
  [[nodiscard]] const_iterator cbefore_begin() const noexcept {
    return before_begin();
  }

  [[nodiscard]] iterator begin() noexcept {
    return iterator{hook_.next};
  }
  [[nodiscard]] const_iterator begin() const noexcept {
    return const_iterator{hook_.next};
  }
  [[nodiscard]] const_iterator cbegin() const noexcept {
    return begin();
  }

  [[nodiscard]] iterator end() noexcept {
    return iterator{nullptr};
  }
  [[nodiscard]] const_iterator end() const noexcept {
    return const_iterator{nullptr};
  }
  [[nodiscard]] const_iterator cend() const noexcept {
    return end();
  }

  /** @brief Given an object, return an iterator to that object.
   *
   * There is no need to refer to the list object itself. If the object is not
   * an element of any list, the behavior is undefined. */
  [[nodiscard]] static iterator iterator_to(T& value) noexcept {
    return iterator{value};
  }

  /** @brief Given an object, return a constant iterator to that object.
   *
   * There is no need to refer to the list object itself. If the object is not
   * an element of any list, the behavior is undefined. */
  [[nodiscard]] static const_iterator iterator_to(T const& value) noexcept {
    return const_iterator{value};
  }

  /** @brief Given an object, return a constant iterator to that object.
   *
   * There is no need to refer to the list object itself. If the object is not
   * an element of any list, the behavior is undefined. */
  [[nodiscard]] static const_iterator const_iterator_to(T const& value)
      noexcept {
    return ifl::iterator_to(value);
  }

  // Assignment ////////////////////////////////////////////////////////////////

  /** @brief Replaces the content of the list with the given range.
   *
   * The amount of time does not depend on the number of elements that are in
   * the list prior to the call (but does depend on the number of new elements).
   *
   * The old elements are not destroyed in any way. If a range operation throws
   * an exception, the elements that have been successfully retrieved from the
   * range are added to the list. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T&>
  void assign(Iterator it, Sentinel iend)
      noexcept(noexcept(it != iend) && noexcept(++it) && noexcept(*it)) {
    clear();
    this->insert_after(before_begin(), std::move(it), std::move(iend));
  }

  /** @brief Disposes of the old elements of the list and replaces them with the
   * given range.
   *
   * If the disposer throws an exception, the elements removed from the list are
   * those and only those that have been passed to the disposer (this includes
   * the element passed to the throwing invocation). The supplied range is then
   * untouched.
   *
   * If a range operation throws an exception, the elements that have been
   * successfully retrieved from the range are added to the list. */
  template<std::invocable<T&> Disposer, std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T&>
  void dispose_and_assign(Disposer&& disposer, Iterator it, Sentinel iend)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>
      && noexcept(it != iend) && noexcept(++it) && noexcept(*it)) {
    this->clear_and_dispose(std::forward<Disposer>(disposer));
    this->insert_after(before_begin(), std::move(it), std::move(iend));
  }

  // Insertion /////////////////////////////////////////////////////////////////

  /** @brief Inserts the given element into the list immediately after the
   * element referred to by the iterator (or at the beginning, if given a
   * before-the-beginning iterator).
   * @returns An iterator to the newly inserted element. */
  static iterator insert_after(iterator pos, T& value) noexcept {
    assert(pos.cur_);
    ifl::hook(value).next = pos.cur_->next;
    pos.cur_->next = &ifl::hook(value);
    return std::next(pos);
  }

  /** @brief Inserts the given elements into the list immediately after the
   * element referred to by the iterator (or at the beginning, if given a
   * before-the-beginning iterator).
   *
   * If a range operation throws an exception, the elements that have been
   * successfully retrieved from the range are added to the list.
   *
   * @returns An iterator to the last inserted element, or `pos` if the range
   *          is empty. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T&>
  static iterator insert_after(
    iterator pos,
    Iterator it,
    Sentinel iend
  ) noexcept(noexcept(it != iend) && noexcept(++it) && noexcept(*it)) {
    assert(pos.cur_);
    intrusive_forward_list_hook* const old_next{pos.cur_->next};

    try {
      while(it != iend) {
        T& value{*it};
        pos.cur_->next = &ifl::hook(value);
        ++pos;
        ++it;
      }
    } catch(...) {
      pos.cur_->next = old_next;
      throw;
    }

    pos.cur_->next = old_next;
    return pos;
  }

  /** @brief Inserts the given element at the beginning of the list. */
  void push_front(T& value) noexcept {
    this->insert_after(before_begin(), value);
  }

  /** @brief Adds all the elements from the given list and adds them to another
   * list immediately after the element referred to by the provided iterator (or
   * at the beginning, if given a before-the-beginning iterator).
   *
   * If the elements are added at the end of the destination list, this takes
   * constant time.
   *
   * Iterators referring to the moved elements are still valid and are now
   * iterators into the destination list.
   *
   * The source and the destination lists must be distinct. */
  static void splice_after(iterator pos, intrusive_forward_list& src) noexcept {
    return ifl::splice_after(pos, src.before_begin(), src.end());
  }

  /** @brief Removes the given range from one list and adds it to another list
   * at the provided position (immediately after `pos`).
   *
   * The range does not include the elements pointed to by `it` or `iend`. (`it`
   * may be a before-the-beginning iterator, and `iend` may be a past-the-end
   * iterator.)
   *
   * If the elements were originally at the end of the source list and are added
   * at the end of the destination list, this takes constant time.
   *
   * Iterators referring to the moved elements are still valid and are now
   * iterators into the destination list.
   *
   * The source and the destination lists may be the same list; but if the `pos`
   * iterator points to one of the elements in the range or is the same as `it`,
   * the behavior is undefined.
   *
   * @returns An iterator to the first spliced element, or `pos` if the range is
   *          empty. */
  static void splice_after(iterator pos, iterator it, iterator iend) noexcept {
    assert(pos.cur_);
    assert(it.cur_);

    if(pos.cur_->next == nullptr && iend.cur_ == nullptr) {
      pos.cur_->next = it.cur_->next;
      it.cur_->next = nullptr;
      return;
    }

    iterator last{it};
    while(std::next(last) != iend) {
      ++last;
    }

    last.cur_->next = pos.cur_->next;
    pos.cur_->next = it.cur_->next;
    it.cur_->next = iend.cur_;
  }

  // Erasure ///////////////////////////////////////////////////////////////////

  /** @brief Removes all elements from the list.
   *
   * This takes constant time. The elements are not destroyed in any way. */
  void clear() noexcept {
    this->erase_after(before_begin(), end());
  }

  /** @brief Removes all elements from the list and disposes of them.
   *
   * If the disposer throws an exception, the elements removed from the list are
   * those and only those that have been passed to the disposer (this includes
   * the element passed to the throwing invocation). */
  template<std::invocable<T&> Disposer>
  void clear_and_dispose(Disposer&& disposer)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    this->erase_and_dispose_after(before_begin(), end(),
      std::forward<Disposer>(disposer));
  }

  /** @brief Removes from the list the element immediately after the one
   * referred to by the iterator.
   * @returns An iterator to the next element after the one removed, or to the
   *          end of the list if this is the last element. */
  static iterator erase_after(iterator it) noexcept {
    assert(it.cur_);
    return ifl::erase_after(it, std::next(it, 2));
  }

  /** @brief Removes from the list the element immediately after the one
   * referred to by the iterator and disposes of it.
   *
   * If the disposer throws an exception, the element is removed anyway.
   *
   * @returns An iterator to the next element after the one removed, or to the
   *          end of the list if this is the last element. */
  template<std::invocable<T&> Disposer>
  static iterator erase_and_dispose_after(iterator it, Disposer&& disposer)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    assert(it.cur_);
    return ifl::erase_and_dispose_after(it, std::next(it, 2),
      std::forward<Disposer>(disposer));
  }

  /** @brief Removes from the list the given range of elements.
   *
   * The range does not include the elements pointed to by `it` or `iend`. (`it`
   * may be a before-the-beginning iterator, and `iend` may be a past-the-end
   * iterator.)
   *
   * This takes constant time. The elements are not destroyed in any way.
   *
   * @returns An iterator to the next element after the ones removed, or to the
   *          end of the list if the elements were at the end. */
  static iterator erase_after(iterator it, iterator iend) noexcept {
    assert(it.cur_);
    it.cur_->next = iend.cur_;
    return iend;
  }

  /** @brief Removes from the list the given range of elements and disposes of
   * them.
   *
   * The range does not include the elements pointed to by `it` or `iend`. (`it`
   * may be a before-the-beginning iterator, and `iend` may be a past-the-end
   * iterator.)
   *
   * If the disposer throws an exception, the elements removed from the list are
   * those and only those that have been passed to the disposer (this includes
   * the element passed to the throwing invocation).
   *
   * @returns An iterator to the next element after the ones removed, or to the
   *          end of the list if the elements were at the end. */
  template<std::invocable<T&> Disposer>
  static iterator erase_and_dispose_after(iterator it, iterator iend,
      Disposer&& disposer) noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    assert(it.cur_);
    if constexpr(std::is_nothrow_invocable_v<Disposer, T&>) {
      iterator cur{std::next(it)};
      ifl::erase_after(it, iend);
      while(cur != iend) {
        iterator const next_it{std::next(cur)};
        std::invoke(std::forward<Disposer>(disposer), *cur);
        cur = next_it;
      }
    } else {
      while(std::next(it) != iend) {
        T& el{*std::next(it)};
        ifl::erase_after(it);
        std::invoke(std::forward<Disposer>(disposer), el);
      }
    }
    return iend;
  }

  /** @brief Removes the first element from the list.
   *
   * The element is not destroyed in any way. */
  void pop_front() noexcept {
    assert(!empty());
    this->erase_after(before_begin());
  }

  /** @brief Removes the first element from the list and disposes of it.
   *
   * If the disposer throws an exception, the element is removed anyway. */
  template<std::invocable<T&> Disposer>
  void pop_front_and_dispose(Disposer&& disposer)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    assert(!empty());
    this->erase_and_dispose_after(before_begin(),
      std::forward<Disposer>(disposer));
  }
};

template<std::derived_from<intrusive_forward_list_hook> T>
void swap(intrusive_forward_list<T>& a, intrusive_forward_list<T>& b) noexcept {
  a.swap(b);
}

} // util

#endif
