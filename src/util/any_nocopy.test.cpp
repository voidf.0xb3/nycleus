#include <boost/test/unit_test.hpp>

#include "util/any_nocopy.hpp"
#include "util/safe_int.hpp"
#include <any>
#include <cstdint>
#include <typeinfo>
#include <utility>

namespace {

struct any_nocopy_fixture {
  util::any_nocopy any_empty{};
  util::any_nocopy any_uint64{util::uint64_t{179}};
  util::any_nocopy any_string{std::u8string{u8"test string"}};
};

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(any_nocopy_nofixture)

BOOST_AUTO_TEST_CASE(make_empty) {
  util::any_nocopy const a{};
  BOOST_TEST(!a.has_value());
}

BOOST_AUTO_TEST_CASE(make_uint64) {
  util::any_nocopy const a{util::uint64_t{179}};
  BOOST_TEST(a.has_value());
}

BOOST_AUTO_TEST_CASE(make_string) {
  util::any_nocopy const a{std::u8string{u8"test string"}};
  BOOST_TEST(a.has_value());
}

BOOST_AUTO_TEST_SUITE_END() // any_nocopy_nofixture

////////////////////////////////////////////////////////////////////////////////

BOOST_FIXTURE_TEST_SUITE(any_nocopy, any_nocopy_fixture)

BOOST_AUTO_TEST_CASE(type_empty) {
  BOOST_TEST((any_empty.type() == typeid(void)));
}

BOOST_AUTO_TEST_CASE(type_uint64) {
  BOOST_TEST((any_uint64.type() == typeid(util::uint64_t)));
}

BOOST_AUTO_TEST_CASE(type_string) {
  BOOST_TEST((any_string.type() == typeid(std::u8string)));
}

////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(cast_uint64) {
  BOOST_TEST(util::any_cast<util::uint64_t>(any_uint64) == util::uint64_t{179});
  BOOST_TEST(any_uint64.as<util::uint64_t>() == util::uint64_t{179});
}

BOOST_AUTO_TEST_CASE(cast_string) {
  BOOST_TEST((util::any_cast<std::u8string>(any_string)
    == std::u8string{u8"test string"}));
  BOOST_TEST((any_string.as<std::u8string>()
    == std::u8string{u8"test string"}));
}

BOOST_AUTO_TEST_CASE(ref_cast0_uint64) {
  util::any_cast<util::uint64_t&>(any_uint64) = 72;
  BOOST_TEST(util::any_cast<util::uint64_t>(any_uint64) == util::uint64_t{72});
}

BOOST_AUTO_TEST_CASE(ref_cast0_string) {
  util::any_cast<std::u8string&>(any_string) = u8"other string";
  BOOST_TEST((util::any_cast<std::u8string>(any_string) == u8"other string"));
}

BOOST_AUTO_TEST_CASE(ref_cast1_uint64) {
  any_uint64.as<util::uint64_t&>() = 72;
  BOOST_TEST(any_uint64.as<util::uint64_t>() == util::uint64_t{72});
}

BOOST_AUTO_TEST_CASE(ref_cast1_string) {
  any_string.as<std::u8string&>() = u8"other string";
  BOOST_TEST((any_string.as<std::u8string>() == u8"other string"));
}

BOOST_AUTO_TEST_CASE(test_type) {
  BOOST_TEST(any_uint64.has<util::uint64_t>());
  BOOST_TEST(!any_uint64.has<std::u8string>());

  BOOST_TEST(!any_string.has<util::uint64_t>());
  BOOST_TEST(any_string.has<std::u8string>());
}

////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(unsafe_cast_uint64) {
  BOOST_TEST(util::any_cast_unsafe<util::uint64_t>(any_uint64)
    == util::uint64_t{179});
  BOOST_TEST(any_uint64.unsafe_as<util::uint64_t>() == util::uint64_t{179});
}

BOOST_AUTO_TEST_CASE(unsafe_cast_string) {
  BOOST_TEST((util::any_cast_unsafe<std::u8string>(any_string)
    == std::u8string{u8"test string"}));
  BOOST_TEST((any_string.unsafe_as<std::u8string>()
    == std::u8string{u8"test string"}));
}

BOOST_AUTO_TEST_CASE(unsafe_ref_cast0_uint64) {
  util::any_cast_unsafe<util::uint64_t&>(any_uint64) = 72;
  BOOST_TEST(util::any_cast_unsafe<util::uint64_t>(any_uint64)
    == util::uint64_t{72});
}

BOOST_AUTO_TEST_CASE(unsafe_ref_cast0_string) {
  util::any_cast_unsafe<std::u8string&>(any_string) = u8"other string";
  BOOST_TEST((util::any_cast_unsafe<std::u8string>(any_string)
    == u8"other string"));
}

BOOST_AUTO_TEST_CASE(unsafe_ref_cast1_uint64) {
  any_uint64.unsafe_as<util::uint64_t&>() = 72;
  BOOST_TEST(any_uint64.unsafe_as<util::uint64_t>() == util::uint64_t{72});
}

BOOST_AUTO_TEST_CASE(unsafe_ref_cast1_string) {
  any_string.unsafe_as<std::u8string&>() = u8"other string";
  BOOST_TEST((any_string.unsafe_as<std::u8string>() == u8"other string"));
}

////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(bad_cast_uint64) {
  BOOST_CHECK_THROW(static_cast<void>(util::any_cast<std::u8string>(any_uint64)),
    std::bad_any_cast);
  BOOST_CHECK_THROW(static_cast<void>(any_uint64.as<std::u8string>()),
    std::bad_any_cast);
}

BOOST_AUTO_TEST_CASE(bad_cast_string) {
  BOOST_CHECK_THROW(static_cast<void>(util::any_cast<util::uint64_t>(any_string)),
    std::bad_any_cast);
  BOOST_CHECK_THROW(static_cast<void>(any_string.as<util::uint64_t>()),
    std::bad_any_cast);
}

BOOST_AUTO_TEST_CASE(bad_empty_cast_uint64) {
  BOOST_CHECK_THROW(static_cast<void>(util::any_cast<std::u8string>(any_empty)),
    std::bad_any_cast);
  BOOST_CHECK_THROW(static_cast<void>(any_empty.as<std::u8string>()),
    std::bad_any_cast);
}

BOOST_AUTO_TEST_CASE(bad_empty_cast_string) {
  BOOST_CHECK_THROW(static_cast<void>(util::any_cast<util::uint64_t>(any_empty)),
    std::bad_any_cast);
  BOOST_CHECK_THROW(static_cast<void>(any_empty.as<util::uint64_t>()),
    std::bad_any_cast);
}

////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(make_move_empty) {
  util::any_nocopy const a{std::move(any_empty)};
  BOOST_TEST(!a.has_value());
  BOOST_TEST(!any_empty.has_value());
}

BOOST_AUTO_TEST_CASE(make_move_uint64) {
  util::any_nocopy a{std::move(any_uint64)};
  BOOST_TEST(!any_uint64.has_value());
  BOOST_TEST(a.as<util::uint64_t>() == util::uint64_t{179});
}

BOOST_AUTO_TEST_CASE(make_move_string) {
  util::any_nocopy a{std::move(any_string)};
  BOOST_TEST(!any_string.has_value());
  BOOST_TEST((a.as<std::u8string>() == u8"test string"));
}

BOOST_AUTO_TEST_CASE(reset_empty) {
  any_empty.reset();
  BOOST_TEST(!any_empty.has_value());
}

BOOST_AUTO_TEST_CASE(reset_uint64) {
  any_uint64.reset();
  BOOST_TEST(!any_uint64.has_value());
}

BOOST_AUTO_TEST_CASE(reset_string) {
  any_string.reset();
  BOOST_TEST(!any_string.has_value());
}

BOOST_AUTO_TEST_CASE(assign_empty) {
  any_string = std::move(any_empty);
  BOOST_TEST(!any_string.has_value());
}

BOOST_AUTO_TEST_CASE(assign_uint64) {
  any_empty = std::move(any_uint64);
  BOOST_TEST(any_empty.as<util::uint64_t>() == util::uint64_t{179});
  BOOST_TEST(!any_uint64.has_value());
}

BOOST_AUTO_TEST_CASE(assign_string) {
  any_empty = std::move(any_string);
  BOOST_TEST((any_empty.as<std::u8string>() == u8"test string"));
  BOOST_TEST(!any_string.has_value());
}

BOOST_AUTO_TEST_CASE(assign_move_uint64) {
  any_empty = util::uint64_t{179};
  BOOST_TEST(any_empty.as<util::uint64_t>() == util::uint64_t{179});
}

BOOST_AUTO_TEST_CASE(assign_move_string) {
  any_empty = std::u8string{u8"test string"};
  BOOST_TEST((any_empty.as<std::u8string>() == u8"test string"));
}

BOOST_AUTO_TEST_CASE(emplace_uint64) {
  any_empty.emplace<util::uint64_t>(util::uint64_t{179});
  BOOST_TEST(any_empty.as<util::uint64_t>() == util::uint64_t{179});
}

BOOST_AUTO_TEST_CASE(emplace_string) {
  any_empty.emplace<std::u8string>(u8"other test string");
  BOOST_TEST((any_empty.as<std::u8string>() == u8"other test string"));
}

BOOST_AUTO_TEST_SUITE_END() // any_nocopy
BOOST_AUTO_TEST_SUITE_END() // test_util
