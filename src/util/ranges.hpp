#ifndef UTIL_RANGES_HPP_INCLUDED_EF8XTYWQE1IU43ES9QDP15ANH
#define UTIL_RANGES_HPP_INCLUDED_EF8XTYWQE1IU43ES9QDP15ANH

#include "util/iterator.hpp"
#include "util/range_adaptor_closure.hpp"
#include "util/type_traits.hpp"
#include <concepts>
#include <cstddef>
#include <functional>
#include <iterator>
#include <ranges>
#include <tuple>
#include <type_traits>
#include <utility>

namespace util {

// find_last ///////////////////////////////////////////////////////////////////

namespace detail_ {

template<
  std::forward_iterator Iterator,
  std::sentinel_for<Iterator> Sentinel,
  typename Proj
>
static constexpr bool find_last_noexcept_impl() noexcept {
  if constexpr(std::bidirectional_iterator<Iterator>
      && std::same_as<Sentinel, Iterator>) {
    return std::is_nothrow_copy_constructible_v<Iterator>
      && noexcept(std::declval<Iterator>() != std::declval<Iterator>())
      && noexcept(--std::declval<Iterator&>())
      && std::is_nothrow_invocable_v<Proj, std::iter_reference_t<Iterator>>
      && std::is_nothrow_move_constructible_v<Iterator>;
  } else {
    return std::is_nothrow_default_constructible_v<Iterator>
      && noexcept(std::declval<Iterator>() != std::declval<Sentinel>())
      && std::is_nothrow_invocable_v<Proj, std::iter_reference_t<Iterator>>
      && std::is_nothrow_copy_assignable_v<Iterator>
      && noexcept(++std::declval<Iterator&>())
      && noexcept(std::declval<Iterator>() == std::declval<Iterator>())
      && std::is_nothrow_move_constructible_v<Iterator>;
  }
}

struct find_last_if_t {
  template<
    std::forward_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel,
    typename Proj = std::identity,
    std::indirect_unary_predicate<std::projected<Iterator, Proj>> Pred
  >
  [[nodiscard]] constexpr std::ranges::subrange<Iterator> operator()(
    Iterator it,
    Sentinel iend,
    Pred pred,
    Proj proj = {}
  ) const noexcept(
    detail_::find_last_noexcept_impl<Iterator, Sentinel, Proj>()
    && std::is_nothrow_invocable_v<Pred,
      std::invoke_result_t<Proj, std::iter_reference_t<Iterator>>>
  ) {
    if constexpr(std::bidirectional_iterator<Iterator>
        && std::same_as<Sentinel, Iterator>) {
      Iterator cur{iend};
      while(cur != it) {
        --cur;
        if(std::invoke(pred, std::invoke(proj, *cur))) {
          return std::ranges::subrange{std::move(cur), std::move(iend)};
        }
      }
      Iterator result{iend};
      return std::ranges::subrange{std::move(result), std::move(iend)};
    } else {
      Iterator result;
      while(it != iend) {
        if(std::invoke(pred, std::invoke(proj, *it))) {
          result = it;
        }
        ++it;
      }
      if(result == Iterator{}) {
        result = it;
      }
      return std::ranges::subrange{std::move(*result), std::move(it)};
    }
  }

  template<
    std::ranges::forward_range Range,
    typename Proj = std::identity,
    std::indirect_unary_predicate<
      std::projected<std::ranges::iterator_t<Range>, Proj>> Pred
  >
  [[nodiscard]] constexpr std::ranges::borrowed_subrange_t<Range> operator()(
    Range&& range,
    Pred pred,
    Proj proj = {}
  ) const noexcept(
    detail_::find_last_noexcept_impl<std::ranges::iterator_t<Range>,
      std::ranges::sentinel_t<Range>, Proj>()
    && std::is_nothrow_invocable_v<Pred,
      std::invoke_result_t<Proj, std::ranges::range_reference_t<Range>>>
  ) {
    return this->operator()(std::ranges::begin(range), std::ranges::end(range),
      std::forward<Pred>(pred), std::forward<Proj>(proj));
  }
};

} // detail_

/** @brief An implementation of std::ranges::find_last_if from C++23. */
inline constexpr detail_::find_last_if_t find_last_if;

namespace detail_ {

struct find_last_if_not_t {
  template<
    std::forward_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel,
    typename Proj = std::identity,
    std::indirect_unary_predicate<std::projected<Iterator, Proj>> Pred
  >
  [[nodiscard]] constexpr std::ranges::subrange<Iterator> operator()(
    Iterator it,
    Sentinel iend,
    Pred pred,
    Proj proj = {}
  ) const noexcept(
    detail_::find_last_noexcept_impl<Iterator, Sentinel, Proj>()
    && std::is_nothrow_invocable_v<Pred,
      std::invoke_result_t<Proj, std::iter_reference_t<Iterator>>>
  ) {
    return find_last_if(std::move(it), std::move(iend),
      [pred = std::move(pred)]<typename T>(T&& t) -> bool {
        return !pred(std::forward<T>(t));
      }, std::move(proj));
  }

  template<
    std::ranges::forward_range Range,
    typename Proj = std::identity,
    std::indirect_unary_predicate<
      std::projected<std::ranges::iterator_t<Range>, Proj>> Pred
  >
  [[nodiscard]] constexpr std::ranges::borrowed_subrange_t<Range> operator()(
    Range&& range,
    Pred pred,
    Proj proj = {}
  ) const noexcept(
    detail_::find_last_noexcept_impl<std::ranges::iterator_t<Range>,
      std::ranges::sentinel_t<Range>, Proj>()
    && std::is_nothrow_invocable_v<Pred,
      std::invoke_result_t<Proj, std::ranges::range_reference_t<Range>>>
  ) {
    return this->operator()(std::ranges::begin(range), std::ranges::end(range),
      std::forward<Pred>(pred), std::forward<Proj>(proj));
  }
};

} // detail_

/** @brief An implementation of std::ranges::find_last_if_not from C++23. */
inline constexpr detail_::find_last_if_not_t find_last_if_not;

namespace detail_ {

struct find_last_t {
  template<
    std::forward_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel,
    typename T,
    typename Proj = std::identity
  >
  requires std::indirect_binary_predicate<std::ranges::equal_to,
    std::projected<Iterator, Proj>, const T*>
  [[nodiscard]] constexpr std::ranges::subrange<Iterator> operator()(
    Iterator it,
    Sentinel iend,
    T const& t,
    Proj proj = {}
  ) const noexcept(
    detail_::find_last_noexcept_impl<Iterator, Sentinel, Proj>()
    && noexcept(std::declval<std::invoke_result_t<Proj,
      std::iter_reference_t<Iterator>>>() == std::declval<T const&>())
  ) {
    return find_last_if(std::move(it), std::move(iend),
      [&t]<typename U>(U&& u) {
        return std::forward<U>(u) == t;
      }, std::move(proj));
  }

  template<
    std::ranges::forward_range Range,
    typename T,
    typename Proj = std::identity
  >
  requires std::indirect_binary_predicate<std::ranges::equal_to,
    std::projected<std::ranges::iterator_t<Range>, Proj>, const T*>
  [[nodiscard]] constexpr std::ranges::borrowed_subrange_t<Range> operator()(
    Range&& range,
    T const& t,
    Proj proj = {}
  ) const noexcept(
    detail_::find_last_noexcept_impl<std::ranges::iterator_t<Range>,
      std::ranges::sentinel_t<Range>, Proj>()
    && noexcept(std::declval<std::invoke_result_t<Proj,
      std::ranges::range_reference_t<Range>>>() == std::declval<T const&>())
  ) {
    return this->operator()(std::ranges::begin(range), std::ranges::end(range),
      t, std::forward<Proj>(proj));
  }
};

} // detail_

/** @brief An implementation of std::ranges::find_last from C++23. */
inline constexpr detail_::find_last_t find_last;

// starts_with /////////////////////////////////////////////////////////////////

namespace detail_ {

struct starts_with_t {
  template<
    std::ranges::input_range Haystack,
    std::ranges::input_range Needle,
    typename Pred = std::ranges::equal_to,
    typename HaystackProj = std::identity,
    typename NeedleProj = std::identity
  >
  requires std::indirectly_comparable<
    std::ranges::iterator_t<Haystack>,
    std::ranges::iterator_t<Needle>,
    Pred,
    HaystackProj,
    NeedleProj
  >
  [[nodiscard]] constexpr bool operator()(
    Haystack&& haystack,
    Needle&& needle,
    Pred pred = {},
    HaystackProj haystack_proj = {},
    NeedleProj needle_proj = {}
  ) const noexcept(
    noexcept(std::ranges::begin(haystack))
    && noexcept(std::ranges::end(haystack))
    && noexcept(std::ranges::begin(needle))
    && noexcept(std::ranges::end(needle))
    && is_nothrow_equality_comparable_with_v<std::ranges::iterator_t<Haystack>,
      std::ranges::sentinel_t<Haystack>>
    && is_nothrow_equality_comparable_with_v<std::ranges::iterator_t<Needle>,
      std::ranges::sentinel_t<Needle>>
    && noexcept(*std::declval<std::ranges::iterator_t<Needle> const&>())
    && noexcept(*std::declval<std::ranges::iterator_t<Haystack> const&>())
    && std::is_nothrow_invocable_v<HaystackProj,
      std::ranges::range_reference_t<Haystack>>
    && std::is_nothrow_invocable_v<NeedleProj,
      std::ranges::range_reference_t<Needle>>
    && std::is_nothrow_invocable_v<Pred,
      std::projected<std::ranges::iterator_t<Needle>, NeedleProj>,
      std::projected<std::ranges::iterator_t<Haystack>, HaystackProj>>
  ) {
    if constexpr(std::ranges::sized_range<Haystack>
        && std::ranges::sized_range<Needle>) {
      if(std::ranges::size(haystack) < std::ranges::size(needle)) {
        return false;
      }
    }

    auto haystack_it{std::ranges::begin(haystack)};
    auto const haystack_iend{std::ranges::end(haystack)};
    auto needle_it{std::ranges::begin(needle)};
    auto const needle_iend{std::ranges::end(needle)};

    for(;;) {
      if(needle_it == needle_iend) {
        return true;
      }
      if(haystack_it == haystack_iend) {
        return false;
      }
      if(!std::invoke(pred, std::invoke(needle_proj, *needle_it),
          std::invoke(haystack_proj, *haystack_it))) {
        return false;
      }
      ++haystack_it;
      ++needle_it;
    }
  }
};

} // detail_

/** @brief An implementation of std::ranges::starts_with from C++23. */
inline constexpr detail_::starts_with_t starts_with;

// range_to ////////////////////////////////////////////////////////////////////

namespace detail_ {

template<typename C, typename Ref>
concept container_insertible = requires(C& container, Ref&& reference) {
  container.push_back(std::forward<Ref>(reference));
} || requires(C& container, Ref&& reference) {
  container.insert(container.end(), std::forward<Ref>(reference));
};

template<typename C>
concept reservable_container = std::ranges::sized_range<C>
  && requires(C& container, std::ranges::range_size_t<C> n) {
    container.reserve(n);
    { container.capacity() } -> std::same_as<std::ranges::range_size_t<C>>;
    { container.max_size() } -> std::same_as<std::ranges::range_size_t<C>>;
  };

} // detail_

/** @brief An implementation of std::ranges::to from C++23.
 *
 * The tag-based construction (via std::from_range) is not implemented, since
 * the tag does not exist before C++23, nor do the tag-based constructors of
 * standard library containers. */
template<typename C, std::ranges::input_range R, typename... Args>
requires (!std::ranges::view<C>)
[[nodiscard]] constexpr C range_to(R&& range, Args&&... args) {
  if constexpr(
    std::ranges::input_range<C>
    && !std::convertible_to<std::ranges::range_reference_t<R>,
      std::ranges::range_value_t<C>>
  ) {
    return util::range_to<C>(range
      | std::views::transform([]<typename T>(T&& t) constexpr {
        return util::range_to<std::ranges::range_value_t<C>>(std::forward<T>(t));
      }), std::forward<Args>(args)...);
  } else if constexpr(std::constructible_from<C, R, Args...>) {
    return C(std::forward<R>(range), std::forward<Args>(args)...);
  } else if constexpr(
    std::ranges::common_range<R>
    && legacy_iterator<std::ranges::iterator_t<R>>
    && std::constructible_from<C, std::ranges::iterator_t<R>,
      std::ranges::sentinel_t<R>, Args...>
  ) {
    return C(std::ranges::begin(range), std::ranges::end(range),
      std::forward<Args>(args)...);
  } else if constexpr(std::constructible_from<C, Args...>
      && detail_::container_insertible<C, std::ranges::range_reference_t<R>>) {
    C container(std::forward<Args>(args)...);
    if constexpr(std::ranges::sized_range<R>
        && detail_::reservable_container<C>) {
      container.reserve(static_cast<std::ranges::range_size_t<C>>(
        std::ranges::size(range)));
    }
    std::ranges::copy(range, [&]() constexpr {
      if constexpr(requires {
        container.push_back(std::declval<std::ranges::range_reference_t<R>>());
      }) {
        return std::back_inserter(container);
      } else {
        return std::inserter(container, container.end());
      }
    }());
    return container;
  }
}

namespace detail_ {

template<std::ranges::input_range R>
struct synthetic_iterator {
  using iterator_category = std::input_iterator_tag;
  using value_type = std::ranges::range_value_t<R>;
  using difference_type = std::ptrdiff_t;
  using pointer = std::add_pointer_t<std::ranges::range_reference_t<R>>;
  using reference = std::ranges::range_reference_t<R>;
  reference operator*() const;
  pointer operator->() const;
  synthetic_iterator& operator++();
  synthetic_iterator operator++(int);
  bool operator==(synthetic_iterator const&) const;
};

} // detail_

/** @brief An implementation of std::ranges::to from C++23.
 *
 * The tag-based construction (via std::from_range) is not implemented, since
 * the tag does not exist before C++23, nor do the tag-based constructors of
 * standard library containers. */
template<template<typename...> typename C, std::ranges::input_range R,
  typename... Args>
[[nodiscard]] constexpr auto range_to(R&& range, Args&&... args) {
  if constexpr(requires { C(std::declval<R>(), std::declval<Args>()...); }) {
    return util::range_to<decltype(
      C(std::declval<R>(), std::declval<Args>()...)
    )>(std::forward<R>(range), std::forward<Args>(args)...);
  } else {
    using iter = detail_::synthetic_iterator<R>;
    if constexpr(requires {
      C(std::declval<iter>(), std::declval<iter>(), std::declval<Args>()...);
    }) {
      return util::range_to<decltype(
        C(std::declval<iter>(), std::declval<iter>(), std::declval<Args>()...)
      )>(std::forward<R>(range), std::forward<Args>(args)...);
    }
  }
}

namespace detail_ {

template<typename C, typename... Args>
requires (!std::ranges::view<C>)
class range_to_adaptor:
    public util::range_adaptor_closure<range_to_adaptor<C, Args...>> {
private:
  std::tuple<Args...> args_;

public:
  explicit range_to_adaptor(Args&&... args):
    args_{std::forward<Args>(args)...} {}

  template<std::ranges::input_range R>
  [[nodiscard]] constexpr C operator()(R&& range) {
    return std::apply([&](Args... args) constexpr -> C {
      return util::range_to<C>(std::forward<R>(range),
        std::forward<Args>(args)...);
    }, args_);
  }
};

} // detail_

/** @brief An implementation of std::ranges::to from C++23.
 *
 * The tag-based construction (via std::from_range) is not implemented, since
 * the tag does not exist before C++23, nor do the tag-based constructors of
 * standard library containers. */
template<typename C, typename... Args>
requires (!std::ranges::view<C>)
[[nodiscard]] constexpr auto range_to(Args&&... args) {
  return detail_::range_to_adaptor<C, Args...>{std::forward<Args>(args)...};
}

namespace detail_ {

template<template<typename...> typename C, typename... Args>
class range_to_adaptor_deduced:
    public util::range_adaptor_closure<range_to_adaptor_deduced<C, Args...>> {
private:
  std::tuple<Args...> args_;

public:
  explicit range_to_adaptor_deduced(Args&&... args):
    args_{std::forward<Args>(args)...} {}

  template<std::ranges::input_range R>
  [[nodiscard]] constexpr auto operator()(R&& range) {
    return std::apply([&](Args... args) constexpr {
      return util::range_to<C>(std::forward<R>(range),
        std::forward<Args>(args)...);
    }, args_);
  }
};

} // detail_

/** @brief An implementation of std::ranges::to from C++23.
 *
 * The tag-based construction (via std::from_range) is not implemented, since
 * the tag does not exist before C++23, nor do the tag-based constructors of
 * standard library containers. */
template<template<typename...> typename C, typename... Args>
[[nodiscard]] constexpr auto range_to(Args&&... args) {
  return detail_::range_to_adaptor_deduced<C, Args...>{
    std::forward<Args>(args)...};
}

} // util

#endif
