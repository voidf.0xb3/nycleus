#include "util/any_nocopy.hpp"
#include <gsl/gsl>

gsl::czstring util::any_value_exception::what() const noexcept {
  return "util::any_value_exception";
}
