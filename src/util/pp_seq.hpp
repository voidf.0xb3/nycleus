#ifndef UTIL_PP_SEQ_HPP_INCLUDED_BA1ZYFOPYZYV39XVICLTLM0O5
#define UTIL_PP_SEQ_HPP_INCLUDED_BA1ZYFOPYZYV39XVICLTLM0O5

// Preprocessor tools for auto-generating code based on sequences specified as
// preprocessor macros.
//
// A sequence is defined as a three-argument macro with arguments x, y, and z
// and expansion of the form: z(...) y x(...) y x(...) ...; that is to say, the
// first sequence element is passed as an argument to the macro whose name is
// passed as z; all subsequent elements are passed as arguments to individual
// invocations of the macro whose name is passed as x; and between each pair of
// adjacent sequence elements, there is an expansion of the argument y.
//
// The following macros can be used to work with such sequences as follows:

// PP_SEQ_APPLY is given a sequence, a macro, and a separator. The macro is
// invoked with each sequence element as the only argument, with the given
// separator interspersed.

#define PP_SEQ_APPLY(seq, x, y) seq(x, y, x)

// PP_SEQ_FOR_EACH is given a sequence and a macro. The macro is invoked with
// each sequence element as the only argument.

#define PP_SEQ_FOR_EACH(seq, macro) PP_SEQ_APPLY(seq, macro, )

// PP_SEQ_LIST is given a sequence and a macro. The macro is invoked with each
// sequence element as the only argument, and the expansions are separated with
// commas.

#define PP_SEQ_COMMA ,
#define PP_SEQ_LIST(seq, macro) seq(macro, PP_SEQ_COMMA, macro)

// PP_SEQ_FIRST is given a sequence and expands to the first element of the
// sequence.

#define PP_SEQ_DROP(x)
#define PP_SEQ_ID(x) x
#define PP_SEQ_FIRST(seq) seq(PP_SEQ_DROP, , PP_SEQ_ID)

#endif
