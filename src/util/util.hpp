#ifndef UTIL_UTIL_HPP_INCLUDED_TZ9GL9IXBNZ9GEMUVYX50OU8Z
#define UTIL_UTIL_HPP_INCLUDED_TZ9GL9IXBNZ9GEMUVYX50OU8Z

#include <cassert>

#ifdef __cpp_lib_unreachable
  #include <utility>
#endif

#define assert_if_noexcept(expr) do { \
  if constexpr(noexcept(expr)) { \
    assert(expr); \
  } \
} while(false)

namespace util {

/** @brief Helper class for use with std::visit. */
template<typename... T>
struct overloaded: T... { using T::operator()...; };

/** @brief Base class for hierarchies (to remove copy & move operations). */
struct hier_base {
  hier_base() noexcept = default;
  hier_base(hier_base const&) = delete;
  hier_base(hier_base&&) = delete;
  hier_base& operator=(hier_base const&) = delete;
  hier_base& operator=(hier_base&&) = delete;
  ~hier_base() noexcept = default;
};

/** @brief An implementation of std::unreachable from C++23. */
[[noreturn]] inline void unreachable() noexcept {
  assert(false);
  #ifdef __cpp_lib_unreachable
    std::unreachable();
  #elif defined(__GNUC__)
    __builtin_unreachable();
  #elif defined(_MSC_VER)
    __assume(false);
  #endif
}

} // util

#endif
