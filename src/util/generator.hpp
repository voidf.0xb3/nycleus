#ifndef UTIL_GENERATOR_HPP_INCLUDED_D1H0OKU3FZLDNXCGK253WO8BQ
#define UTIL_GENERATOR_HPP_INCLUDED_D1H0OKU3FZLDNXCGK253WO8BQ

#include "util/iterator_facade.hpp"
#include "util/nums.hpp"
#include <cassert>
#include <concepts>
#include <coroutine>
#include <cstddef>
#include <exception>
#include <memory>
#include <new>
#include <ranges>
#include <type_traits>

namespace util {

/** @brief An implementation of std::ranges::elements_of from C++23, for use
 * with the implementation of std::generator from C++23. */
template<typename Range, typename Allocator = std::allocator<std::byte>>
struct elements_of {
  [[no_unique_address]] Range range;
  [[no_unique_address]] Allocator allocator{};
};

template<typename R, typename Allocator = std::allocator<std::byte>>
elements_of(R&&, Allocator = Allocator{}) -> elements_of<R&&, Allocator>;

/** @brief An implementation of std::generator from C++23. */
template<typename Ref, typename Value = void, typename Allocator = void>
class generator;

namespace detail_ {

template<typename Yielded>
class generator_promise_base {
private:
  /** @brief The coroutine above this one in the stack (the callee).
   *
   * If, when the coroutine is suspended, the yielded pointer and the exception
   * pointer are both null, and this handle is not null, this means that the
   * coroutine got suspended as a result of yielding another generator. If this
   * is also null, then the coroutine reached its end.
   *
   * The callee is owned by this coroutine and is destroyed by this one. */
  generator_promise_base* callee_{nullptr};

  /** @brief The coroutine below this one in the stack (the caller). */
  generator_promise_base* caller_{nullptr};

  /** @brief A pointer to the last yielded value.
   *
   * If, when the coroutine is suspended, this pointer is not null, this means
   * that the coroutine is suspended as a result of yielding a value. In this
   * case, the exception pointer and the callee handle must both be null. */
  std::add_pointer_t<Yielded> value_;

  /** @brief An exception pointer that is used to pass exceptions to and from
   * generators.
   *
   * This pointer serves two purposes. Firstly, if, when the coroutine is
   * suspended, the yielded pointer is null, and the exception pointer is not
   * null, this means that the generator threw the given exception. The
   * exception needs to be propagated to the caller generator or, if this
   * generator is at the bottom of the stack, to the code iterating over the
   * generator's produced range. In this case, the callee handle must also be
   * null.
   *
   * Secondly, if a coroutine suspends while yielding another generator and,
   * upon resumption, observes that this pointer is not null, that means that
   * the called generator threw an exception, so this exception needs to be
   * rethrown from the co_yield expression. */
  std::exception_ptr exception_{nullptr};

  template<typename, typename, typename>
  friend class util::generator;

public:
  generator_promise_base() = default;

  generator_promise_base(generator_promise_base const&) = delete;
  generator_promise_base(generator_promise_base&&) = delete;
  generator_promise_base& operator=(generator_promise_base const&) = delete;
  generator_promise_base& operator=(generator_promise_base&&) = delete;

protected:
  ~generator_promise_base() noexcept = default;

public:
  /** @brief Resumes the coroutine.
   *
   * Uses virtual dispatch to select the correct promise type and convert it to
   * a coroutine handle. */
  virtual void resume() = 0;

  /** @brief Destroys the coroutine frame and all frames above it on the stack.
   *
   * Uses virtual dispatch to select the correct promise type and convert it to
   * a coroutine handle.
   *
   * Since destroying the coroutine frame also destroys its promise object,
   * after this function returns, the promise object it's called on will no
   * longer exist. */
  virtual void destroy() noexcept = 0;

  [[nodiscard]] static auto initial_suspend() noexcept {
    return std::suspend_always{};
  }

  [[nodiscard]] auto final_suspend() noexcept {
    value_ = nullptr;
    return std::suspend_always{};
  }

  void await_transform() = delete;

  void return_void() noexcept {
    value_ = nullptr;
  }

  void unhandled_exception() noexcept {
    value_ = nullptr;
    exception_ = std::current_exception();
    assert(exception_);
  }
};

struct alignas(__STDCPP_DEFAULT_NEW_ALIGNMENT__) generator_allocation_unit {
  char storage[__STDCPP_DEFAULT_NEW_ALIGNMENT__];
};

/** @brief A type-erased wrapper for a generator’s allocator.
 *
 * If a generator type’s allocator parameter is void, then that generator can
 * be allocated with any allocator type, and deallocation must be done using the
 * same allocator type. This requires storing the allocator in a type-erased
 * box and using virtual dispatch to call appropriate deallocation code. */
class generator_allocator_wrapper_base {
protected:
  ~generator_allocator_wrapper_base() noexcept = default;

public:
  virtual void deallocate(void* memory, std::size_t base_size) noexcept = 0;
};

template<typename Allocator>
class generator_allocator_wrapper final:
    public generator_allocator_wrapper_base {
private:
  [[no_unique_address]] Allocator allocator_;

  virtual void deallocate(void* memory, std::size_t base_size) noexcept
      override {
    Allocator allocator{allocator_};
    this->~generator_allocator_wrapper();

    std::size_t const aligned_size{next_multiple(base_size,
      __STDCPP_DEFAULT_NEW_ALIGNMENT__)};
    std::size_t const real_size{aligned_size
      + sizeof(generator_allocator_wrapper)};

    std::allocator_traits<Allocator>::deallocate(allocator,
      static_cast<generator_allocation_unit*>(memory),
      div_ceil(real_size, __STDCPP_DEFAULT_NEW_ALIGNMENT__));
  }

public:
  explicit generator_allocator_wrapper(Allocator const& allocator) noexcept:
    allocator_{allocator} {}
};

/** @brief An implementation of generator allocation and deallocation logic for
 * the given allocator type (which may be void). */
template<typename Allocator>
struct generator_allocator_traits {
  using rebound_t = std::allocator_traits<Allocator>
    ::template rebind_alloc<generator_allocation_unit>;
  using traits = std::allocator_traits<rebound_t>;

  static_assert(std::is_pointer_v<typename traits::pointer>);

  static constexpr bool can_omit_allocator{std::default_initializable<rebound_t>
    && traits::is_always_equal::value};

  /** @brief Given the size of an allocated coroutine frame, returns the offset
   * from its start to the position where the allocator is stored. */
  [[nodiscard]] static std::size_t align_for_allocator(std::size_t base_size)
      noexcept {
    return next_multiple(base_size, alignof(rebound_t));
  }

  /** @brief Returns the amount of memory actually allocated, including the
   * memory for storing the allocator itself.
   *
   * This does not take into account the possibility that the allocator may be
   * omitted. */
  [[nodiscard]] static std::size_t get_real_size(std::size_t base_size)
      noexcept {
    return align_for_allocator(base_size) + sizeof(rebound_t);
  }

  template<typename GivenAllocator>
  requires std::convertible_to<GivenAllocator const&, Allocator>
  [[nodiscard]] static void* allocate(
    std::size_t size,
    GivenAllocator const& allocator
  ) {
    rebound_t rebound{Allocator(allocator)};
    if constexpr(can_omit_allocator) {
      return traits::allocate(rebound,
        div_ceil(size, __STDCPP_DEFAULT_NEW_ALIGNMENT__));
    } else {
      void* const memory{traits::allocator(rebound,
        div_ceil(get_real_size(size), __STDCPP_DEFAULT_NEW_ALIGNMENT__))};
      ::new(static_cast<std::byte*>(memory) + align_for_allocator(size))
        rebound_t{rebound};
      return memory;
    }
  }

  [[nodiscard]] static void* allocate(std::size_t size) {
    return generator_allocator_traits::allocate(size, Allocator());
  }

  static void deallocate(void* memory, std::size_t size) noexcept {
    if constexpr(can_omit_allocator) {
      rebound_t allocator;
      traits::deallocate(allocator,
        static_cast<generator_allocation_unit*>(memory),
        div_ceil(size, __STDCPP_DEFAULT_NEW_ALIGNMENT__));
    } else {
      rebound_t* const stored_allocator{reinterpret_cast<rebound_t*>(
        static_cast<std::byte*>(memory) + align_for_allocator(size))};
      rebound_t allocator{stored_allocator};
      stored_allocator->~rebound_t();
      traits::deallocate(allocator, memory,
        div_ceil(get_real_size(size), __STDCPP_DEFAULT_NEW_ALIGNMENT__));
    }
  }
};

template<>
struct generator_allocator_traits<void> {
  template<typename GivenAllocator>
  [[nodiscard]] static void* allocate(std::size_t base_size,
      GivenAllocator const& allocator) {
    // Rebind the allocator
    using rebound_t = std::allocator_traits<GivenAllocator>
      ::template rebind_alloc<generator_allocation_unit>;
    rebound_t rebound{allocator};

    using wrapper = generator_allocator_wrapper<rebound_t>;

    static_assert(std::is_pointer_v<
      typename std::allocator_traits<rebound_t>::pointer>);

    // Calculate the real allocation size
    std::size_t const aligned_size{next_multiple(base_size,
      __STDCPP_DEFAULT_NEW_ALIGNMENT__)};
    std::size_t const real_size{aligned_size + sizeof(wrapper)};

    // Allocate
    void* const memory{std::allocator_traits<rebound_t>::allocate(rebound,
      div_ceil(real_size, __STDCPP_DEFAULT_NEW_ALIGNMENT__))};

    // Store the allocator wrapper
    [[maybe_unused]] wrapper& w{
      *new(static_cast<std::byte*>(memory) + aligned_size) wrapper{rebound}};
    assert(&w == &static_cast<generator_allocator_wrapper_base&>(w));

    return memory;
  }

  [[nodiscard]] static void* allocate(std::size_t size) {
    return generator_allocator_traits<void>::allocate(size,
      std::allocator<void>{});
  }

  static void deallocate(void* memory, std::size_t base_size) noexcept {
    std::size_t const aligned_size{next_multiple(base_size,
      __STDCPP_DEFAULT_NEW_ALIGNMENT__)};
    reinterpret_cast<generator_allocator_wrapper_base*>(
      static_cast<std::byte*>(memory) + aligned_size)
      ->deallocate(memory, base_size);
  }
};

} // detail_

template<typename Ref, typename Value, typename Allocator>
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class generator:
    public std::ranges::view_interface<generator<Ref, Value, Allocator>> {
private:
  using value = std::conditional_t<std::is_void_v<Value>,
    std::remove_cvref_t<Ref>, Value>;
  using reference = std::conditional_t<std::is_void_v<Value>, Ref&&, Ref>;
  using rvalue_reference = std::conditional_t<std::is_reference_v<reference>,
    std::remove_reference_t<reference>&&, reference>;

  static_assert(std::is_object_v<value>
    && std::same_as<value, std::remove_cv_t<value>>);
  static_assert(std::is_reference_v<reference> || std::is_object_v<reference>
    && std::same_as<reference, std::remove_cv_t<reference>>
    && std::copy_constructible<reference>);
  static_assert(std::common_reference_with<reference&&, value&>);
  static_assert(std::common_reference_with<reference&&, rvalue_reference&&>);
  static_assert(std::common_reference_with<rvalue_reference&&, value const&>);

  template<typename, typename, typename>
  friend class util::generator;

public:
  using yielded = std::conditional_t<std::is_reference_v<reference>,
    reference, reference const&>;

  class promise_type;

private:
  promise_type* bottom_;

  explicit generator(promise_type& bottom) noexcept: bottom_{&bottom} {}

public:
  // NOLINTNEXTLINE(cppcoreguidelines-virtual-class-destructor)
  class promise_type final: public detail_::generator_promise_base<yielded> {
  public:
    virtual void resume() override {
      std::coroutine_handle<promise_type>::from_promise(*this).resume();
    }

    virtual void destroy() noexcept override {
      if(this->callee_) {
        this->callee_->destroy();
      }
      std::coroutine_handle<promise_type>::from_promise(*this).destroy();
    }

    [[nodiscard]] generator get_return_object() noexcept {
      return generator{*this};
    }

    [[nodiscard]] auto yield_value(yielded value) noexcept {
      this->value_ = std::addressof(value);
      return std::suspend_always{};
    }

    [[nodiscard]] auto yield_value(
      std::remove_reference_t<yielded> const& value
    )
    noexcept(std::is_nothrow_copy_constructible_v<std::remove_cvref_t<yielded>>)
    requires std::is_rvalue_reference_v<yielded>
      && std::constructible_from<std::remove_cvref_t<yielded>,
      std::remove_reference_t<yielded> const&>
    {
      class awaitable {
      private:
        std::remove_cvref_t<yielded> value_;

      public:
        explicit awaitable(std::remove_reference_t<yielded> const& value)
          noexcept(std::is_nothrow_copy_constructible_v<
          std::remove_cvref_t<yielded>>): value_{value} {}

        [[nodiscard]] static bool await_ready() noexcept { return false; }

        void await_suspend(std::coroutine_handle<promise_type> h) noexcept {
          h.promise().value_ = std::addressof(value_);
        }

        static void await_resume() noexcept {}
      };

      return awaitable{value};
    }

    template<typename OtherRef, typename OtherValue, typename OtherAllocator,
      typename RangeAllocator>
    requires std::same_as<typename generator<OtherRef, OtherValue,
      OtherAllocator>::yielded, yielded>
    [[nodiscard]] auto yield_value(elements_of<generator<OtherRef, OtherValue,
        OtherAllocator>&&, RangeAllocator> range) noexcept {
      assert(range.range.bottom_);
      this->callee_ = range.range.bottom_;
      range.range.bottom_ = nullptr;

      this->callee_->caller_ = this;

      this->value_ = nullptr;

      class awaitable {
      private:
        promise_type& promise_;

      public:
        explicit awaitable(promise_type& promise) noexcept: promise_{promise} {}

        [[nodiscard]] static bool await_ready() noexcept { return false; }

        static void await_suspend(std::coroutine_handle<promise_type>)
          noexcept {}

        void await_resume() const {
          std::exception_ptr ex{promise_.exception_};
          if(ex) {
            promise_.exception_ = nullptr;
            std::rethrow_exception(std::move(ex));
          }
        }
      };
      return awaitable{*this};
    }

    template<typename Range, typename RangeAllocator>
    requires std::convertible_to<std::ranges::range_reference_t<Range>, yielded>
    [[nodiscard]] auto yield_value(elements_of<Range, RangeAllocator> range) {
      auto nested{[](
        std::allocator_arg_t,
        RangeAllocator,
        std::ranges::iterator_t<Range> it,
        std::ranges::sentinel_t<Range> iend
      ) -> generator<yielded, std::ranges::range_value_t<Range>,
          RangeAllocator> {
        while(it != iend) {
          co_yield static_cast<yielded>(*it);
          ++it;
        }
      }};
      return yield_value(elements_of{nested(
        std::allocator_arg,
        range.allocator,
        std::ranges::begin(range.range),
        std::ranges::end(range.range)
      )});
    }

  private:
    using traits = detail_::generator_allocator_traits<Allocator>;

  public:
    [[nodiscard]] void* operator new(std::size_t size)
    requires std::same_as<Allocator, void>
      || std::default_initializable<Allocator>
    {
      return traits::allocate(size);
    }

    template<typename GivenAllocator, typename... Args>
    requires std::same_as<Allocator, void>
      || std::convertible_to<GivenAllocator const&, Allocator>
    [[nodiscard]] void* operator new(
      std::size_t size,
      std::allocator_arg_t,
      GivenAllocator const& allocator,
      Args&&...
    ) {
      return traits::allocate(size, allocator);
    }

    template<typename This, typename GivenAllocator, typename... Args>
    requires std::same_as<Allocator, void>
      || std::convertible_to<GivenAllocator const&, Allocator>
    [[nodiscard]] void* operator new(
      std::size_t size,
      This const&,
      std::allocator_arg_t,
      GivenAllocator const& allocator,
      Args&&...
    ) {
      return traits::allocate(size, allocator);
    }

    void operator delete(void* ptr, std::size_t size) noexcept {
      traits::deallocate(ptr, size);
    }
  };

private:
  class iterator_core {
  private:
    /** @brief A handle to the top coroutine on the generator's stack.
     *
     * We would want to store a pointer to its corresponding generator. However,
     * if the generator is moved, we want its associated iterators to turn into
     * iterators into the newly constructed generator. Therefore, we instead
     * keep a pointer into the generator's state that is moved when the
     * generator is moved. */
    detail_::generator_promise_base<yielded>* top_;

    friend class generator;
    explicit iterator_core(promise_type& top) noexcept: top_{&top} {
      increment();
    }

  public:
    iterator_core() = default;

  protected:
    using value_type = value;

    using difference_type = std::ptrdiff_t;

    static constexpr bool is_single_pass{true};

    [[nodiscard]] reference dereference() const
        noexcept(std::is_nothrow_copy_constructible_v<reference>) {
      return static_cast<reference>(*top_->value_);
    }

    void increment() {
      for(;;) {
        top_->resume();
        if(top_->value_) {
          // We're yielding the next value
          assert(!top_->callee_);
          assert(!top_->exception_);
          return;
        } else if(top_->exception_) {
          // We're propagating an exception...
          assert(!top_->callee_);

          if(top_->caller_) {
            // ...to the caller
            auto const old_top = top_;
            top_ = top_->caller_;
            assert(top_->callee_ == old_top);
            top_->callee_ = nullptr;
            top_->exception_ = std::move(old_top->exception_);
            old_top->destroy();
          } else {
            // ...to the iterator's client
            std::rethrow_exception(std::move(top_->exception_));
          }
        } else if(top_->callee_) {
          // We're calling another generator
          top_ = top_->callee_;
        } else if(top_->caller_) {
          // We're returning to the caller generator
          auto const old_top = top_;
          top_ = top_->caller_;
          assert(top_->callee_ == old_top);
          top_->callee_ = nullptr;
          old_top->destroy();
        } else {
          // We've reached the end of the range
          return;
        }
      }
    }

    [[nodiscard]] bool equal_to(std::default_sentinel_t) const noexcept {
      return !top_->value_;
    }
  };

  using iterator = util::iterator_facade<iterator_core>;

public:
  generator() noexcept: bottom_{} {}

  generator(generator const&) = default;

  generator(generator&& other) noexcept: bottom_{other.bottom_} {
    other.bottom_ = nullptr;
  }

  generator& operator=(generator other) noexcept {
    std::swap(bottom_, other.bottom_);
    return *this;
  }

  ~generator() noexcept {
    if(bottom_) {
      bottom_->destroy();
    }
  }

  [[nodiscard]] auto begin() const noexcept {
    assert(bottom_);
    return iterator{*bottom_};
  }

  [[nodiscard]] auto end() const noexcept { return std::default_sentinel; }
};

} // util

#endif
