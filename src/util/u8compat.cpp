#include "util/u8compat.hpp"
#include <algorithm>
#include <iterator>
#include <string>

std::u8string util::as_u8char_s(std::string_view str) {
  std::u8string result;
  result.reserve(str.size());
  std::ranges::copy(str, std::back_inserter(result));
  return result;
}
