#include <boost/test/unit_test.hpp>

#include "util/ptr_with_flags.hpp"
#include <cstdint>
#include <string>

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(ptr_with_flags)

BOOST_AUTO_TEST_CASE(init_ptr) {
  std::uint64_t const x{0};
  util::ptr_with_flags<std::uint64_t const, 2> const p{&x};

  std::uint64_t const* const p_raw{p};
  BOOST_TEST(p_raw == &x);
}

BOOST_AUTO_TEST_CASE(set_ptr) {
  std::uint64_t const x{0};
  std::uint64_t const y{0};
  util::ptr_with_flags<std::uint64_t const, 2> p{&x};

  p.set_ptr(&y);
  std::uint64_t const* const p_raw{p};
  BOOST_TEST(p_raw == &y);
}

BOOST_AUTO_TEST_CASE(init_flags) {
  std::uint64_t const x{0};
  util::ptr_with_flags<std::uint64_t const, 2> const p{&x};

  BOOST_TEST(!p.get_flag<0>());
  BOOST_TEST(!p.get_flag<1>());
}

BOOST_AUTO_TEST_CASE(set_flags) {
  std::uint64_t const x{0};
  util::ptr_with_flags<std::uint64_t const, 2> p{&x};

  p.set_flag<0>(true);
  BOOST_TEST(p.get_flag<0>());
  BOOST_TEST(!p.get_flag<1>());

  p.set_flag<0>(false);
  BOOST_TEST(!p.get_flag<0>());
  BOOST_TEST(!p.get_flag<1>());

  p.set_flag<1>(false);
  BOOST_TEST(!p.get_flag<0>());
  BOOST_TEST(!p.get_flag<1>());
}

BOOST_AUTO_TEST_CASE(flags_preserve_ptr) {
  std::uint64_t const x{0};
  util::ptr_with_flags<std::uint64_t const, 2> p{&x};

  p.set_flag<0>(true);
  std::uint64_t const* const p_raw{p};
  BOOST_TEST(p_raw == &x);
}

BOOST_AUTO_TEST_CASE(ptr_preserves_flags) {
  std::uint64_t const x{0};
  std::uint64_t const y{0};
  util::ptr_with_flags<std::uint64_t const, 2> p{&x};

  p.set_flag<0>(true);
  p.set_ptr(&y);
  BOOST_TEST(p.get_flag<0>());
  BOOST_TEST(!p.get_flag<1>());
}

BOOST_AUTO_TEST_CASE(deref) {
  std::uint64_t x{0};
  util::ptr_with_flags<std::uint64_t, 2> p{&x};
  p.set_flag<0>(true);

  *p = 179;
  BOOST_TEST(x == 179);
}

BOOST_AUTO_TEST_CASE(arrow) {
  std::string str{"hello"};
  util::ptr_with_flags<std::string, 2> p{&str};
  p.set_flag<0>(true);

  p->clear();
  BOOST_TEST(str.empty());
}

BOOST_AUTO_TEST_SUITE_END() // ptr_with_flags
BOOST_AUTO_TEST_SUITE_END() // test_util
