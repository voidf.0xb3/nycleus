#ifndef UTIL_ANY_NOCOPY_HPP_INCLUDED_69ZAIIPRXPTYYTELP9T2O5MIU
#define UTIL_ANY_NOCOPY_HPP_INCLUDED_69ZAIIPRXPTYYTELP9T2O5MIU

#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <any>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <exception>
#include <memory>
#include <new>
#include <type_traits>
#include <typeinfo>
#include <utility>

namespace util {

/** @brief An exception class that wraps exceptions thrown by any_nocopy's
 * contained value. */
class any_value_exception: public std::exception {
public:
  any_value_exception() noexcept = default;
  [[nodiscard]] virtual gsl::czstring what() const noexcept override;
};

/** @brief A version of std::any that works with non-copyable types.
 *
 * The type must still be nothrow-destructible.
 *
 * Bizarrely, std::any's member functions are allowed to throw *literally
 * anything*, because they throw whatever is thrown by the contained value's
 * constructors. This deficiency is corrected in this class by wrapping the
 * contained value's exceptions (except for bad_alloc) in any_value_exception.
 */
class any_nocopy {
private:
  /** @brief The minimum acceptable size of the local storage. */
  static constexpr std::size_t min_local_size{16};

  /** @brief The size of local storage.
   *
   * It is designed to be at least as large as the minimum required size, but
   * also to use up the object's space most efficiently, leaving no padding. */
  static constexpr std::size_t local_size{min_local_size
    + (alignof(std::max_align_t) - 1
    - min_local_size % alignof(std::max_align_t))};

  alignas(std::max_align_t) std::byte storage_[local_size];

  enum class state_t: std::uint8_t { empty, local, nonlocal };
  state_t state_;

  struct holder_base {
    virtual ~holder_base() noexcept = default;

    /** @brief Moves the contained value into the given local storage.
     *
     * After this call, the contained value is in a moved-from state. Can only
     * be called if the contained value's move constructor does not throw. */
    virtual void move(std::byte*) noexcept = 0;

    /** @brief Determines the type of the contained value. */
    [[nodiscard]] virtual std::type_info const& type() const noexcept = 0;
  };

  template<typename T>
  struct holder final: holder_base {
    static_assert(std::is_nothrow_destructible_v<T>,
      "util::any_nocopy cannot hold types with throwing destructors");
    static_assert(std::same_as<T, std::decay_t<T>>);

    T t;

    template<typename... Args>
    requires std::is_constructible_v<T, Args&&...>
    holder(Args&&... args)
      noexcept(std::is_nothrow_constructible_v<T, Args...>):
      t{std::forward<Args>(args)...} {}

    virtual void move(std::byte* storage) noexcept override;

    [[nodiscard]] virtual std::type_info const& type() const noexcept override {
      return typeid(T);
    }
  };

  /** @brief Whether the given type should be held in local storage. */
  template<typename T>
  static constexpr bool is_local{local_size >= sizeof(holder<T>)
    && std::is_nothrow_move_constructible_v<T>
    && alignof(holder<T>) <= alignof(std::max_align_t)};

  static_assert(local_size >= sizeof(std::unique_ptr<holder_base>),
    "util::any_nocopy::local_size too small for nonlocal storage");
  static_assert(is_local<void*>,
    "util::any_nocopy does not store void* locally");

  /** @brief Retrieves the value holder. */
  [[nodiscard]] holder_base const& get_holder() const noexcept {
    assert(has_value());
    switch(state_) {
      case state_t::empty:
        unreachable();

      case state_t::local:
        return *std::launder(reinterpret_cast<holder_base const*>(storage_));

      case state_t::nonlocal:
        return **std::launder(reinterpret_cast<
          std::unique_ptr<holder_base> const*>(storage_));

      default: unreachable();
    }
  }

  /** @brief Retrieves the value holder. */
  [[nodiscard]] holder_base& get_holder() noexcept {
    assert(has_value());
    return const_cast<holder_base&>(static_cast<any_nocopy const*>(this)
      ->get_holder());
  }

public:
  /** @brief Creates an empty object. */
  any_nocopy() noexcept: state_{state_t::empty} {}

  any_nocopy(any_nocopy const&) = delete;

  /** @brief Move-constructs an object.
   *
   * After the call, the other object is empty. */
  any_nocopy(any_nocopy&& other) noexcept: state_{state_t::empty} {
    *this = std::move(other);
  }

  /** @brief Constructs a new object with a contained value of the given type,
   * constructed with the given arguments.
   *
   * @see util::make_any_nocopy
   *
   * @throws std::bad_alloc
   * @throws any_value_exception */
  template<typename T, typename... Args>
  requires std::is_constructible_v<std::decay_t<T>, Args&&...>
  any_nocopy(std::in_place_type_t<T>, Args&&... args): state_{state_t::empty} {
    this->emplace<T>(std::forward<Args>(args)...);
  }

private:
  template<typename T>
  struct is_value_constructible: std::false_type {};
  template<typename T>
  requires (!std::same_as<std::decay_t<T>, any_nocopy>)
  struct is_value_constructible<T>:
    std::bool_constant<std::is_constructible_v<std::decay_t<T>, T&&>> {};
  template<typename T>
  static constexpr bool is_value_constructible_v{
    is_value_constructible<T>::value};

public:
  /** @brief Constructs a new object with the given contained value.
   *
   * @throws std::bad_alloc
   * @throws any_value_exception */
  template<typename T>
  requires (!std::same_as<std::decay_t<T>, any_nocopy>)
    && (!util::specialization_of<std::decay_t<T>, std::in_place_type_t>)
    && is_value_constructible_v<T>
  any_nocopy(T&& t): any_nocopy{std::in_place_type<T>, std::forward<T>(t)} {}

  ~any_nocopy() noexcept {
    reset();
  }

  any_nocopy& operator=(any_nocopy const&) = delete;

  /** @brief Moves an object.
   *
   * After the call, the other object is empty. */
  any_nocopy& operator=(any_nocopy&& other) noexcept {
    reset();
    state_ = other.state_;
    switch(state_) {
      case state_t::empty:
        return *this;

      case state_t::local:
        other.get_holder().move(storage_);
        other.reset();
        return *this;

      case state_t::nonlocal:
        new(storage_) std::unique_ptr<holder_base>{std::move(*std::launder(
          reinterpret_cast<std::unique_ptr<holder_base>*>(other.storage_)))};
        other.reset();
        return *this;

      default: unreachable();
    }
  }

  /** @brief Sets the object to contain the given value.
   *
   * @throws std::bad_alloc
   * @throws any_value_exception */
  template<typename T>
  requires (!std::same_as<std::decay_t<T>, any_nocopy>)
    && is_value_constructible_v<T>
  any_nocopy& operator=(T&& t) {
    this->emplace<T>(std::forward<T>(t));
    return *this;
  }

  /** @brief Sets the object to contain a value of the given type, constructed
   * in-place with the given arguments.
   *
   * @throws std::bad_alloc
   * @throws any_value_exception */
  template<typename T, typename... Args>
    requires std::is_constructible_v<std::decay_t<T>, Args&&...>
  std::decay_t<T>& emplace(Args&&... args) {
    reset();
    try {
      if constexpr(is_local<std::decay_t<T>>) {
        auto const p{new(storage_)
          holder<std::decay_t<T>>{std::forward<Args>(args)...}};

        // We assume that the holder_base subobject is laid out at the beginning
        // of the holder<T> object. Strictly speaking, this is not required by
        // the standard, so we need to check for this. This really should be
        // done with a static_assert, but offsetof() may not work with
        // non-standard-layout types.
        assert(reinterpret_cast<std::intptr_t>(p)
          == reinterpret_cast<std::intptr_t>(static_cast<holder_base*>(p)));

        state_ = state_t::local;
        return p->t;
      } else {
        auto const p{new holder<std::decay_t<T>>{std::forward<Args>(args)...}};
        new(storage_) std::unique_ptr<holder_base>{p};
        state_ = state_t::nonlocal;
        return p->t;
      }
    } catch(std::bad_alloc const&) {
      throw;
    } catch(...) {
      std::throw_with_nested(any_value_exception{});
    }
  }

  /** @brief Destroys the held value, making the object empty. */
  void reset() noexcept {
    switch(state_) {
      case state_t::empty:
        return;

      case state_t::local:
        get_holder().~holder_base();
        break;

      case state_t::nonlocal: {
        using std::unique_ptr;
        std::launder(reinterpret_cast<unique_ptr<holder_base>*>(storage_))
          ->~unique_ptr();
        break;
      }
    }
    state_ = state_t::empty;
  }

  /** @brief Swaps the two objects. */
  void swap(any_nocopy& other) noexcept {
    std::swap(*this, other);
  }

  /** @brief Determines whether the object has a value. */
  [[nodiscard]] bool has_value() const noexcept {
    return state_ != state_t::empty;
  }

  /** @brief Determines the type of the contained value.
   *
   * If empty, returns typeid(void). */
  [[nodiscard]] std::type_info const& type() const noexcept {
    if(has_value()) {
      return get_holder().type();
    } else {
      return typeid(void);
    }
  }

  template<typename T>
  friend T const* any_cast(any_nocopy const*) noexcept;

  /** @brief Safely retrieves the contained value.
   *
   * @throws std::bad_any_cast if the value is not of the correct type. */
  template<typename T>
  requires std::is_constructible_v<T, std::remove_cvref_t<T> const&>
  [[nodiscard]] T as() const&;

  /** @brief Safely retrieves the contained value.
   *
   * @throws std::bad_any_cast if the value is not of the correct type. */
  template<typename T>
  requires std::is_constructible_v<T, std::remove_cvref_t<T>&>
  [[nodiscard]] T as() &;

  /** @brief Safely retrieves the contained value.
   *
   * @throws std::bad_any_cast if the value is not of the correct type. */
  template<typename T>
  requires std::is_constructible_v<T, std::remove_cvref_t<T>&&>
  [[nodiscard]] T as() &&;

  /** @brief Determines whether the contained value has the specified type. */
  template<typename T>
  [[nodiscard]] bool has() const noexcept;

  template<typename T>
  friend T const* any_cast_unsafe(any_nocopy const*) noexcept;

  /** @brief Unsafely retrieves the contained value.
   *
   * If the value is not of the correct type, undefined behavior. */
  template<typename T>
  requires std::is_constructible_v<T, std::remove_cvref_t<T> const&>
  [[nodiscard]] T unsafe_as() const& noexcept;

  /** @brief Unsafely retrieves the contained value.
   *
   * If the value is not of the correct type, undefined behavior. */
  template<typename T>
  requires std::is_constructible_v<T, std::remove_cvref_t<T>&>
  [[nodiscard]] T unsafe_as() & noexcept;

  /** @brief Unsafely retrieves the contained value.
   *
   * If the value is not of the correct type, undefined behavior. */
  template<typename T>
  requires std::is_constructible_v<T, std::remove_cvref_t<T>&&>
  [[nodiscard]] T unsafe_as() && noexcept;
};

template<typename T>
void any_nocopy::holder<T>::move(std::byte* storage) noexcept {
  if constexpr(is_local<T>) {
    ::new(storage) holder{std::move(t)};
  } else {
    unreachable();
  }
}

/** @brief Helper function to create a new any_nocopy with a given value. */
template<typename T, typename... Args>
requires std::is_constructible_v<std::decay_t<T>, Args&&...>
[[nodiscard]] any_nocopy make_any_nocopy(Args&&... args) {
  return any_nocopy{std::in_place_type<T>, std::forward<Args>(args)...};
}

////////////////////////////////////////////////////////////////////////////////

/** @brief Safely retrieves the contained value.
 *
 * If the value is not of the correct type, returns a null pointer. */
template<typename T>
[[nodiscard]] T const* any_cast(any_nocopy const* op) noexcept {
  if(!op || !op->has_value()) {
    return nullptr;
  }
  if(auto p{dynamic_cast<any_nocopy::holder<std::decay_t<T>> const*>(
      &op->get_holder())}) {
    return &p->t;
  } else {
    return nullptr;
  }
}

/** @brief Safely retrieves the contained value.
 *
 * If the value is not of the correct type, returns a null pointer. */
template<typename T>
[[nodiscard]] T* any_cast(any_nocopy* op) noexcept {
  return const_cast<T*>(any_cast<T>(static_cast<any_nocopy const*>(op)));
}

/** @brief Safely retrieves the contained value.
 *
 * @throws std::bad_any_cast if the value is not of the correct type. */
template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T> const&>
[[nodiscard]] T any_cast(any_nocopy const& op) {
  auto p{any_cast<std::remove_cvref_t<T>>(&op)};
  if(p == nullptr) {
    throw std::bad_any_cast{};
  }
  return static_cast<T>(*p);
}

template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T> const&>
T any_nocopy::as() const& {
  return any_cast<T>(*this);
}

/** @brief Safely retrieves the contained value.
 *
 * @throws std::bad_any_cast if the value is not of the correct type. */
template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T>&>
[[nodiscard]] T any_cast(any_nocopy& op) {
  auto p{any_cast<std::remove_cvref_t<T>>(&op)};
  if(p == nullptr) {
    throw std::bad_any_cast{};
  }
  return static_cast<T>(*p);
}

template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T>&>
T any_nocopy::as() & {
  return any_cast<T>(*this);
}

/** @brief Safely retrieves the contained value.
 *
 * @throws std::bad_any_cast if the value is not of the correct type. */
template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T>&&>
[[nodiscard]] T any_cast(any_nocopy&& op) {
  auto p{any_cast<std::remove_cvref_t<T>>(&op)};
  if(p == nullptr) {
    throw std::bad_any_cast{};
  }
  return static_cast<T>(std::move(*p));
}

template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T>&&>
T any_nocopy::as() && {
  return any_cast<T>(std::move(*this));
}

template<typename T>
bool any_nocopy::has() const noexcept {
  return any_cast<std::remove_cvref_t<T>>(this) != nullptr;
}

////////////////////////////////////////////////////////////////////////////////

/** @brief Unsafely retrieves the contained value.
 *
 * If the value is not of the correct type, undefined behavior. */
template<typename T>
[[nodiscard]] T const* any_cast_unsafe(any_nocopy const* op) noexcept {
  assert(op);
  assert(typeid(std::decay_t<T>) == op->type());
  return &static_cast<any_nocopy::holder<std::decay_t<T>> const*>(
    &op->get_holder())->t;
}

/** @brief Unsafely retrieves the contained value.
 *
 * If the value is not of the correct type, undefined behavior. */
template<typename T>
[[nodiscard]] T* any_cast_unsafe(any_nocopy* op) noexcept {
  assert(op);
  assert(typeid(std::decay_t<T>) == op->type());
  return const_cast<T*>(any_cast_unsafe<T>(static_cast<any_nocopy const*>(op)));
}

/** @brief Unsafely retrieves the contained value.
 *
 * If the value is not of the correct type, undefined behavior. */
template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T> const&>
[[nodiscard]] T any_cast_unsafe(any_nocopy const& op) noexcept {
  assert(typeid(std::decay_t<T>) == op.type());
  return static_cast<T>(*any_cast_unsafe<std::remove_cvref_t<T>>(&op));
}

template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T> const&>
T any_nocopy::unsafe_as() const& noexcept {
  return any_cast_unsafe<T>(*this);
}

/** @brief Unsafely retrieves the contained value.
 *
 * If the value is not of the correct type, undefined behavior. */
template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T>&>
[[nodiscard]] T any_cast_unsafe(any_nocopy& op) noexcept {
  assert(typeid(std::decay_t<T>) == op.type());
  return static_cast<T>(*any_cast_unsafe<std::remove_cvref_t<T>>(&op));
}

template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T>&>
T any_nocopy::unsafe_as() & noexcept {
  return any_cast_unsafe<T>(*this);
}

/** @brief Unsafely retrieves the contained value.
 *
 * If the value is not of the correct type, undefined behavior. */
template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T>&&>
[[nodiscard]] T any_cast_unsafe(any_nocopy&& op) noexcept {
  assert(typeid(std::decay_t<T>) == op.type());
  return static_cast<T>(std::move(
    *any_cast_unsafe<std::remove_cvref_t<T>>(&op)));
}

template<typename T>
requires std::is_constructible_v<T, std::remove_cvref_t<T>&&>
T any_nocopy::unsafe_as() && noexcept {
  return any_cast_unsafe<T>(std::move(*this));
}

} // util

#endif
