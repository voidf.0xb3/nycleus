#ifndef UTIL_ITERATOR_FACADE_HPP_INCLUDED_RIDY8CQUYVT9OBVY1GR5NX6HC
#define UTIL_ITERATOR_FACADE_HPP_INCLUDED_RIDY8CQUYVT9OBVY1GR5NX6HC

#include <compare>
#include <concepts>
#include <iterator>
#include <memory>
#include <type_traits>
#include <utility>

namespace util {

namespace detail_ {

/** @brief A helper proxy class for operator->() of iterators whose reference
 * type is not a true reference. */
template<typename T>
class arrow_proxy {
  static_assert(std::is_move_constructible_v<T>);

private:
  T t_;

public:
  explicit constexpr arrow_proxy(T&& t)
    noexcept(std::is_nothrow_move_constructible_v<T>): t_{std::move(t)} {}

  [[nodiscard]] constexpr T* operator->() noexcept {
    return std::addressof(t_);
  }
};

} // detail_

/** @brief A tool for automatically implementing readable iterators from a
 * handful of basic operations.
 *
 * The iterator facade is parametrized by an *iterator core* type, from which it
 * inherits and which supplies a number of *hooks* that define iterator
 * behavior. The facade inherits all constructors and assignment operators from
 * the core, and also all other members that are normally inherited. If a core
 * assignment operator returns an lvalue reference to the core type, the
 * facade's assignment operator instead returns an lvalue reference to the
 * facade itself.
 *
 * Note that this is different from the way iterator_facade is implemented in
 * Boost.Iterator: this class does not use CRTP, but rather the inverse of it.
 * Instead of having the core inherit from the facade, the facade inherits from
 * the core; the actual iterator type is the facade class itself. This design
 * allows the iterator to automatically determine a lot more characteristics of
 * the iterator, since it can actually reflect on the core type.
 *
 * At a minimum, the core must be move-constructible and move-assignable, and it
 * must implement the dereference() function hook and either the increment()
 * function hook or its replacement.
 *
 * The hooks should not be made public, to ensure encapsulation; instead, it is
 * enough to make them protected.
 *
 * Function hooks
 * ==============
 * - reference dereference() const: Required. This hook is called to implement
 *   the dereferencing of an iterator. Its return type is taken to be the
 *   iterator's reference type.
 *
 * - pointer arrow() const: Optional. This hook can be used to provide a custom
 *   implementation for operator->(). If it is not present, that operator is
 *   instead implemented as follows:
 *   - If the reference type is a true reference, the operator returns the
 *     address of the referred-to object.
 *   - Otherwise, if the reference type is move-constructible, the operator
 *     returns a proxy object with the necessary semantics.
 *   - Otherwise, the operator is not implemented and the pointer type is void.
 *
 * - void increment(): Required (but can be replaced with advance(), see below).
 *   This hook is used to increment the iterator.
 *
 * - auto sp_postfix_increment(): Optional. If the iterator is single-pass, this
 *   hook is used to implement the postfix increment operator. If the iterator
 *   is not single-pass, this hook is not used. More information about this
 *   operator's implementation is found below.
 *
 * - bool equal_to(Other const&) const: For forward iterators, this hook is
 *   required to be implemented for comparing to other instances of the same
 *   core; optional in all other circumstances. This is used to compare the
 *   iterator to other objects. Note that since the iterator is derived from the
 *   core, you can (indeed, must) accept an argument of the core type itself in
 *   order to be able to compare the iterator to itself.
 *
 * - void decrement(): Required for bidirectional iterators. This hook is used
 *   to decrement the iterator.
 *
 * - auto distance_to(Other const&) const: For random-access iterators, this
 *   hook is required to be implemented for comparing to other instances of the
 *   same core; optional in all other circumstances. This is used to determine
 *   the distance from this iterator to another iterator or a sentinel. Note
 *   that since the iterator is derived from the core, you can (indeed, must)
 *   accept an argument of the core type itself in order to be able to compare
 *   the iterator to itself. The result type of such a comparison is used as the
 *   iterator's difference type.
 *
 * - void advance(difference_type): Required for random-access iterators. This
 *   hook is used to move the iterator forward or backward by any amount.
 *
 * - reference at(difference_type) const: Optional. Can be used to provide a
 *   custom implementation of operator[]. More information about this operator's
 *   implementation is found below.
 *
 * Reimplementations of function hooks
 * -----------------------------------
 * If the core has an advance() hook, the increment() and/or decrement() hook
 * may be absent; in that case, it is automatically reimplemented as advance(1)
 * or advance(-1), respectively, and then treated as if it were explicitly
 * present. Likewise, if the core has a distance_to() hook with a particular
 * parameter, the equal_to() hook with the same parameter may be absent, in
 * which case it is reimplemented using the distance_to() hook by comparing the
 * distance to zero.
 *
 * This works even if the core does not otherwise satisfy the requirements of a
 * random-access iterator. (But in that case, why would you even have an
 * advance() or distance_to() hook??)
 *
 * Type hooks
 * ==========
 * - value_type: Sets the iterator's value type. If this is absent, the value
 *   type is determined by the reference type as follows:
 *   - If the reference type is a true reference, the value type is the
 *     reference's target type (without cv-qualifications).
 *   - Otherwise, the value type is the same as the reference type. This is a
 *     heuristic for iterators that return their referred-to elements by value.
 * - difference_type: Sets the iterator's difference type. If this is absent,
 *   the difference type is determined as follows:
 *   - If there exists a distance_to() hook for comparing to the core itself,
 *     its return type is the iterator's difference type.
 *   - Otherwise, the difference type is std::ptrdiff_t.
 * - legacy_category: Sets the iterator's legacy (pre-C++20) category tag. If
 *   this is absent, the tag is determined in the manner specified below.
 *
 * Flag hooks
 * ==========
 * There are also flag hooks; these are Boolean member variables which, if
 * absent, are treated as if they were false (not set).
 *
 * The following flag hooks are supported:
 * - is_single_pass: Marks the iterator as a single-pass iterator. Since this is
 *   a purely semantic requirement, the only way for the core to communicate
 *   this to the facade is with a hook. Note that even if this flag is not set,
 *   the iterator may not be a forward iterator if the syntactic requirements
 *   are not satisfied.
 * - is_contiguous: Marks the iterator as a contiguous iterator. Once again,
 *   since the requirements are mostly semantic (it makes perfect sense to have
 *   a random-access iterator whose reference type is an lvalue reference but
 *   which is not a contiguous iterator), the only way for the core to
 *   communicate this to the facade is with a hook. If the iterator does not
 *   satisfy the syntactic requirements for a contiguous iterator, this has no
 *   effect.
 *
 * Iterator tags
 * =============
 * The facade automatically determines the best iterator tags for the iterator,
 * based on the syntactic requirements that it satisfies (and some hints about
 * semantic requirements). This facility is primarily oriented toward C++20
 * iterator_concept, but also supports legacy (C++17 and earlier)
 * iterator_category. This means that the resulting iterator is always a C++20
 * iterator, but it may not be a legacy (readable) iterator.
 *
 * A few discrepancies between these three iterator categorization systems:
 * - C++17 requires all iterators to be copyable. C++20 allows single-pass
 *   iterators to be move-only. Thus, if the core is non-copyable, it is a C++20
 *   input_iterator, but not a legacy iterator.
 * - Legacy (readable) iterators must have operator->(), but C++20 iterators
 *   don't have to have one. If operator->() cannot be implemented, the iterator
 *   is not a legacy iterator.
 * - For legacy single-pass iterators, the expression `*it++` must be equivalent
 *   to dereferencing the iterator and using the result, followed by a
 *   prefix increment. For C++20 iterators, the postfix increment simply needs
 *   to be valid and have the same effect as a prefix increment, but there are
 *   no requirements imposed on its value. If the postfix increment cannot be
 *   implemented appropriately, the iterator is not a legacy iterator. See also
 *   the section below concerting the implementation of postfix increment for
 *   single-pass iterators. (For multipass iterators, both C++17 and C++20
 *   require `it++` to return a copy of the old value, so there's no discrepancy
 *   there.)
 * - If an iterator is not a legacy input iterator, it may still be an output
 *   iterator. This facility cannot determine that (because it cannot tell the
 *   types that can be written through the iterator). Therefore, if the iterator
 *   is not a legacy input iterator, its iterator_category is not set to a valid
 *   tag. If it is desired to set it to std::output_iterator_tag, that can be
 *   arranged using the iterator_category hook.
 *
 * Indexing operator
 * =================
 * For random-access iterators, the expression `i[n]` is required to be valid
 * and equal to `*(i + n)`. The problem is that the expression `i + n` creates a
 * temporary iterator, which is dereferenced and subsequently destroyed before
 * the reference is returned. The default implementation assumes that the
 * reference remains valid even after the iterator from which it was obtained is
 * destroyed. If that is not the case, a custom implementation is needed, which
 * may be provided via the at() hook.
 *
 * Postfix increment operator for single-pass iterators
 * ====================================================
 * When implementing the postfix increment operator for single-pass iterators,
 * one problem is that if the result of the increment is dereferenced and used
 * in some way, the use must occur before the increment, even though the
 * increment syntactically precedes the use. To overcome this, the postfix
 * increment is implemented as follows:
 * - If there is a `sp_postfix_increment` hook, it is used as the implementation
 *   of postfix increment, overriding any heuristics. When selecting
 *   `iterator_category`, it is assumed that the hook correctly satisfies the
 *   requirements necessary for the iterator to be a legacy iterator; if that is
 *   not the case, `iterator_category` can be explicitly overridden via the
 *   appropriate type hook.
 * - Otherwise, if the reference type is move-constructible (in particular, if
 *   it is a true reference), postfix increment dereferences the iterator, then
 *   increments it, then returns a proxy object wrapping the obtained reference.
 *   This assumes that the reference can still be used even after the iterator
 *   is incremented. If that assumption is false, the operator must be
 *   customized via the `sp_postfix_increment` hook, as described above.
 * - Otherwise, postfix increment simply increments the iterator and returns
 *   void. In this case, the iterator is not a legacy iterator.
 *
 * Note that if the iterator is a multipass iterator, a simpler implementation
 * is possible and, indeed, required, so the sp_postfix_increment() hook is
 * simply ignored in that case. */
template<typename Core>
class iterator_facade: public Core {
private:
  /** @brief A helper class to test for core function hooks.
   *
   * We can only check for protected members using an instance of the class that
   * does the check. This cannot be iterator_facade, because it must be complete
   * by the time we rely on the check. The checks have to be done inside
   * functions to ensure that the class is complete by the time the checks are
   * performed. We also calculate associated types, because they are needed to
   * check for some function hooks. */
  class access: public Core {
    // Checking for core function hooks ////////////////////////////////////////

  public:
    [[nodiscard]] static constexpr bool has_dereference() noexcept {
      return requires(access const& it) { it.Core::dereference(); };
    }

    [[nodiscard]] static constexpr bool has_arrow() noexcept {
      return requires(access const& it) { it.Core::arrow(); };
    }

    [[nodiscard]] static constexpr bool has_increment() noexcept {
      return requires(access& it) { it.Core::increment(); };
    }

    [[nodiscard]] static constexpr bool has_sp_postfix_increment() noexcept {
      return requires(access& it) { it.Core::sp_postfix_increment(); };
    }

    template<typename Other>
    [[nodiscard]] static constexpr bool has_equal_to() noexcept {
      return requires(access const& it, Other const& other) {
        it.Core::equal_to(other);
      };
    }

    [[nodiscard]] static constexpr bool has_decrement() noexcept {
      return requires(access& it) { it.Core::decrement(); };
    }

    template<typename Other>
    [[nodiscard]] static constexpr bool has_distance_to() noexcept {
      return requires(access const& it, Other const& other) {
        it.Core::distance_to(other);
      };
    }

    // Checking for core type hooks ////////////////////////////////////////////

    static constexpr bool has_value_type{requires {
      typename Core::value_type;
    }};

    static constexpr bool has_difference_type{requires {
      typename Core::difference_type;
    }};

    // Associated types ////////////////////////////////////////////////////////

    // These are calculated using functions that return an std::type_identity
    // tag that names the desired type. The iterator_facade class itself then
    // unwraps the tag to extract the type. (The functions are never actually
    // executed; they're just used as containers for if constexpr.) They need to
    // be present here, because later function hooks rely on difference_type
    // (and other types are also calculated here for consistency).

    [[nodiscard]] static auto reference_impl() noexcept {
      return std::type_identity<decltype(std::declval<access const&>()
        .Core::dereference())>{};
    }

    [[nodiscard]] static auto value_type_impl() noexcept {
      using reference = decltype(reference_impl())::type;
      if constexpr(has_value_type) {
        return std::type_identity<typename Core::value_type>{};
      } else if constexpr(std::is_reference_v<reference>) {
        return std::type_identity<std::remove_cvref_t<reference>>{};
      } else {
        return std::type_identity<reference>{};
      }
    }

    [[nodiscard]] static auto pointer_impl() noexcept {
      using reference = decltype(reference_impl())::type;
      if constexpr(has_arrow()) {
        return std::type_identity<decltype(std::declval<access const&>()
          .arrow())>{};
      } else if constexpr(std::is_reference_v<reference>) {
        return std::type_identity<std::remove_reference_t<reference>*>{};
      } else if constexpr(std::is_move_constructible_v<reference>) {
        return std::type_identity<detail_::arrow_proxy<reference>>{};
      } else {
        return std::type_identity<void>{};
      }
    }

    [[nodiscard]] static auto difference_type_impl() noexcept {
      if constexpr(has_difference_type) {
        return std::type_identity<typename Core::difference_type>{};
      } else if constexpr(has_distance_to<Core>()) {
        return std::type_identity<decltype(std::declval<access const&>()
          .distance_to(std::declval<Core const&>()))>{};
      } else {
        return std::type_identity<std::ptrdiff_t>{};
      }
    }

    // Checking for more core function hooks ///////////////////////////////////
    // (these hooks rely on the difference type)

    [[nodiscard]] static constexpr bool has_advance() noexcept {
      using diff_type = decltype(difference_type_impl())::type;
      return requires(access& it) {
        it.Core::advance(std::declval<diff_type>());
      };
    }

    [[nodiscard]] static constexpr bool has_at() noexcept {
      using diff_type = decltype(difference_type_impl())::type;
      return requires(access const& it) {
        it.Core::at(std::declval<diff_type>());
      };
    }
  };

public:
  using reference = decltype(access::reference_impl())::type;
  using value_type = decltype(access::value_type_impl())::type;
  using pointer = decltype(access::pointer_impl())::type;
  using difference_type = decltype(access::difference_type_impl())::type;

  // Dispatching to core function hooks ////////////////////////////////////////

private:
  static constexpr bool can_dereference{access::has_dereference()};
  [[nodiscard]] constexpr reference do_dereference() const
      noexcept(noexcept(Core::dereference())) requires can_dereference {
    return Core::dereference();
  }

  static constexpr bool can_arrow{access::has_arrow()};
  [[nodiscard]] constexpr pointer do_arrow() const
      noexcept(noexcept(Core::arrow())) requires can_arrow {
    return Core::arrow();
  }

  static constexpr bool can_increment{access::has_increment()
    || access::has_advance()};
  [[nodiscard]] static constexpr bool do_increment_noexcept() noexcept
      requires can_increment {
    if constexpr(access::has_increment()) {
      return noexcept(std::declval<iterator_facade&>().increment());
    } else {
      return noexcept(std::declval<iterator_facade&>()
        .advance(static_cast<difference_type>(1)));
    }
  }
  constexpr void do_increment() noexcept(do_increment_noexcept())
      requires can_increment {
    if constexpr(access::has_increment()) {
      Core::increment();
    } else {
      Core::advance(static_cast<difference_type>(1));
    }
  }

  static constexpr bool can_sp_postfix_increment{
    access::has_sp_postfix_increment()};
  [[nodiscard]] decltype(auto) do_sp_postfix_increment()
      noexcept(noexcept(Core::sp_postfix_increment()))
      requires can_sp_postfix_increment {
    return Core::sp_postfix_increment();
  }

  template<typename Other>
  static constexpr bool can_equal_to{access::template has_equal_to<Other>()
    || access::template has_distance_to<Other>()};
  template<typename Other>
  requires can_equal_to<Other>
  [[nodiscard]] static constexpr bool do_equal_to_noexcept() noexcept {
    if constexpr(access::template has_equal_to<Other>()) {
      return noexcept(std::declval<iterator_facade const&>()
        .equal_to(std::declval<Other const&>()));
    } else {
      return noexcept(std::declval<iterator_facade const&>()
        .distance_to(std::declval<Other const&>()));
    }
  }
  template<typename Other>
  requires can_equal_to<Other>
  [[nodiscard]] constexpr bool do_equal_to(Other const& other) const
      noexcept(do_equal_to_noexcept<Other>()) {
    if constexpr(access::template has_equal_to<Other>()) {
      return Core::equal_to(other);
    } else {
      return Core::distance_to(other) == 0;
    }
  }

  static constexpr bool can_decrement{access::has_decrement()
    || access::has_advance()};
  [[nodiscard]] static constexpr bool do_decrement_noexcept() noexcept
      requires can_decrement {
    if constexpr(access::has_decrement()) {
      return noexcept(std::declval<iterator_facade&>().decrement());
    } else {
      return noexcept(std::declval<iterator_facade&>()
        .advance(static_cast<difference_type>(1)));
    }
  }
  constexpr void do_decrement() noexcept(do_decrement_noexcept())
      requires can_decrement {
    if constexpr(access::has_decrement()) {
      Core::decrement();
    } else {
      Core::advance(static_cast<difference_type>(-1));
    }
  }

  static constexpr bool can_advance{access::has_advance()};
  constexpr void do_advance(difference_type off)
      noexcept(noexcept(Core::advance(off))) requires can_advance {
    Core::advance(off);
  }

  static constexpr bool can_at{access::has_at()};
  [[nodiscard]] constexpr reference do_at(difference_type off) const
      noexcept(noexcept(Core::at(off))) requires can_at {
    return Core::at(off);
  }

  template<typename Other>
  static constexpr bool can_distance_to{
    access::template has_distance_to<Other>()};
  template<typename Other>
  requires can_distance_to<Other>
  [[nodiscard]] constexpr difference_type do_distance_to(Other const& other)
      const noexcept(noexcept(Core::distance_to(other))) {
    return Core::distance_to(other);
  }

  // Checking for flag hooks ///////////////////////////////////////////////////

  [[nodiscard]] static constexpr bool check_is_contiguous() noexcept {
    if constexpr(requires { Core::is_contiguous; }) {
      return Core::is_contiguous;
    } else {
      return false;
    }
  }

  [[nodiscard]] static constexpr bool check_is_single_pass() noexcept {
    if constexpr(requires { Core::is_single_pass; }) {
      return Core::is_single_pass;
    } else {
      return false;
    }
  }

  // Calculation of iterator concept, category, and traversal //////////////////

public:
  using iterator_concept = std::conditional_t<
    can_equal_to<Core> && !check_is_single_pass()
      && std::is_copy_constructible_v<Core> && std::is_copy_assignable_v<Core>,
    std::conditional_t<
      can_decrement,
      std::conditional_t<
        can_advance && can_distance_to<Core>,
        std::conditional_t<
          std::is_lvalue_reference_v<reference> && check_is_contiguous(),
          std::contiguous_iterator_tag,
          std::random_access_iterator_tag
        >,
        std::bidirectional_iterator_tag
      >,
      std::forward_iterator_tag
    >,
    std::input_iterator_tag
  >;

private:
  template<typename C, typename = void>
  struct legacy_category_impl {
    using type = void;
  };
  template<typename C>
  struct legacy_category_impl<C, std::void_t<typename C::legacy_category>> {
    using type = C::legacy_category;
  };
  using legacy_category = legacy_category_impl<Core>::type;

public:
  using iterator_category = std::conditional_t<
    std::same_as<legacy_category, void>,
    std::conditional_t<
      can_arrow || std::is_move_constructible_v<reference>,
      std::conditional_t<
        std::is_base_of_v<std::forward_iterator_tag, iterator_concept>,
        std::conditional_t<
          std::is_lvalue_reference_v<reference>,
          iterator_concept,
          std::input_iterator_tag
        >,
        std::conditional_t<
          std::is_copy_constructible_v<Core> && std::is_copy_assignable_v<Core>
            && can_equal_to<Core> && (can_sp_postfix_increment
            || std::is_move_constructible_v<reference>),
          std::input_iterator_tag,
          void
        >
      >,
      void
    >,
    legacy_category
  >;

  static_assert(can_increment);
  static_assert(can_dereference);
  static_assert(std::movable<Core>);

  // Public iterator interface /////////////////////////////////////////////////

  using Core::Core;

  iterator_facade() = default;

  iterator_facade(iterator_facade const&) = default;
  iterator_facade(iterator_facade&&) = default;
  iterator_facade& operator=(iterator_facade const&) = default;
  iterator_facade& operator=(iterator_facade&&) = default;
  ~iterator_facade() = default;

  template<typename T>
  requires (!std::same_as<decltype(std::declval<Core&>()
    = std::declval<T&&>()), Core&>)
  // NOLINTBEGIN(cppcoreguidelines-c-copy-assignment-signature)
  // NOLINTBEGIN(misc-unconventional-assign-operator)
  constexpr decltype(auto) operator=(T&& t)
      noexcept(noexcept(static_cast<Core&>(*this) = std::forward<T>(t))) {
    // NOLINTEND(misc-unconventional-assign-operator)
    // NOLINTEND(cppcoreguidelines-c-copy-assignment-signature)
    return static_cast<Core&>(*this) = std::forward<T>(t);
  }

  template<typename T>
  requires std::same_as<decltype(std::declval<Core&>()
    = std::declval<T&&>()), Core&>
  // NOLINTBEGIN(cppcoreguidelines-c-copy-assignment-signature)
  // NOLINTBEGIN(misc-unconventional-assign-operator)
  constexpr iterator_facade& operator=(T&& t)
      noexcept(noexcept(static_cast<Core&>(*this) = std::forward<T>(t))) {
    // NOLINTEND(misc-unconventional-assign-operator)
    // NOLINTEND(cppcoreguidelines-c-copy-assignment-signature)
    static_cast<Core&>(*this) = std::forward<T>(t);
    return *this;
  }

  constexpr void swap(iterator_facade& other)
      noexcept(std::is_nothrow_swappable_v<Core>) {
    using std::swap;
    swap(static_cast<Core&>(*this), static_cast<Core&>(other));
  }

  [[nodiscard]] constexpr reference operator*() const
      noexcept(noexcept(do_dereference())) {
    return do_dereference();
  }

private:
  [[nodiscard]] static constexpr bool arrow_noexcept() noexcept
      requires can_arrow || std::is_reference_v<reference>
      || std::is_move_constructible_v<reference> {
    if constexpr(can_arrow) {
      return noexcept(std::declval<iterator_facade const&>().do_arrow());
    } else  {
      if constexpr(!noexcept(noexcept(std::declval<iterator_facade const&>()
          .do_dereference()))) {
        return false;
      }
      return std::is_reference_v<reference>
        || std::is_nothrow_move_constructible_v<reference>;
    }
  }

public:
  [[nodiscard]] constexpr pointer operator->() const noexcept(arrow_noexcept())
      requires can_arrow || std::is_reference_v<reference>
      || std::is_move_constructible_v<reference> {
    if constexpr(can_arrow) {
      return do_arrow();
    } else if constexpr(std::is_reference_v<reference>) {
      return std::addressof(**this);
    } else {
      return detail_::arrow_proxy{**this};
    }
  }

  constexpr iterator_facade& operator++()
      noexcept(noexcept(do_increment())) {
    do_increment();
    return *this;
  }

private:
  [[nodiscard]] static constexpr bool postfix_increment_noexcept() noexcept {
    if constexpr(std::is_base_of_v<std::forward_iterator_tag,
        iterator_concept>) {
      return std::is_nothrow_copy_constructible_v<Core>
        && noexcept(std::declval<iterator_facade&>().do_increment());
    } else if constexpr(can_sp_postfix_increment) {
      return noexcept(std::declval<iterator_facade&>()
        .do_sp_postfix_increment());
    } else if constexpr(std::is_move_constructible_v<reference>) {
      return noexcept(std::declval<iterator_facade&>().do_increment())
        && (std::is_reference_v<reference>
        || std::is_nothrow_move_constructible_v<reference>);
    } else {
      return noexcept(std::declval<iterator_facade&>().do_increment());
    }
  }

public:
  constexpr decltype(auto) operator++(int)
      noexcept(postfix_increment_noexcept()) {
    if constexpr(std::is_base_of_v<std::forward_iterator_tag,
        iterator_concept>) {
      iterator_facade copy{*this};
      ++*this;
      return copy;
    } else if constexpr(can_sp_postfix_increment) {
      return do_sp_postfix_increment();
    } else if constexpr(std::is_move_constructible_v<reference>) {
      class proxy {
      private:
        reference ref_;

      public:
        explicit constexpr proxy(reference&& ref)
          noexcept(std::is_reference_v<reference>
          || std::is_nothrow_move_constructible_v<reference>):
          ref_{std::forward<reference>(ref)} {}

        [[nodiscard]] constexpr reference operator*() &&
            noexcept(std::is_reference_v<reference>
            || std::is_nothrow_move_constructible_v<reference>) {
          return std::forward<reference>(ref_);
        }
      };

      proxy result{**this};
      ++*this;
      return result;
    } else {
      ++*this;
    }
  }

  constexpr iterator_facade& operator--() noexcept(noexcept(do_decrement()))
      requires can_decrement {
    do_decrement();
    return *this;
  }

  constexpr iterator_facade operator--(int) noexcept(noexcept(do_decrement())
      && std::is_nothrow_copy_constructible_v<Core>)
      requires can_decrement {
    iterator_facade copy{*this};
    --*this;
    return copy;
  }

  constexpr iterator_facade& operator+=(difference_type off)
      noexcept(noexcept(do_advance(off))) requires can_advance {
    do_advance(off);
    return *this;
  }

  [[nodiscard]] friend constexpr iterator_facade operator+(
    iterator_facade const& it,
    difference_type off
  )
  noexcept(
    noexcept(std::declval<iterator_facade&>().do_advance(off))
    && std::is_nothrow_copy_constructible_v<Core>
  )
  requires can_advance && std::is_copy_constructible_v<Core>
  {
    iterator_facade result{it};
    result += off;
    return result;
  }

  [[nodiscard]] friend constexpr iterator_facade operator+(
    difference_type off,
    iterator_facade const& it
  )
  noexcept(noexcept(it + off))
  requires can_advance && std::is_copy_constructible_v<Core>
  {
    return it + off;
  }

  constexpr iterator_facade& operator-=(difference_type off)
      noexcept(noexcept(*this += -off)) requires can_advance {
    return *this += -off;
  }

  [[nodiscard]] friend constexpr iterator_facade operator-(
    iterator_facade const& it,
    difference_type off
  )
  noexcept(noexcept(it + -off))
  requires can_advance && std::is_copy_constructible_v<Core>
  {
    return it + -off;
  }

  template<typename Other>
  requires can_distance_to<Other>
  [[nodiscard]] friend constexpr difference_type operator-(
    iterator_facade const& it,
    Other const& other
  ) noexcept(noexcept(it.do_distance_to(other))) {
    return -it.do_distance_to(other);
  }

  template<typename Other>
  requires can_distance_to<Other> && (!std::same_as<iterator_facade, Other>)
  [[nodiscard]] friend constexpr auto operator-(
    Other const& other,
    iterator_facade const& it
  ) noexcept(noexcept(it.do_distance_to(other))) {
    return it.do_distance_to(other);
  }

private:
  [[nodiscard]] static constexpr bool at_noexcept() noexcept
      requires can_at || (can_advance && std::is_copy_constructible_v<Core>) {
    if constexpr(can_at) {
      return noexcept(std::declval<iterator_facade const&>()
        .do_at(std::declval<difference_type>()));
    } else {
      return noexcept(*(std::declval<iterator_facade const&>()
        + std::declval<difference_type>()));
    }
  }

public:
  [[nodiscard]] constexpr reference operator[](difference_type off)
      const noexcept(at_noexcept())
      requires can_at || (can_advance && std::is_copy_constructible_v<Core>) {
    if constexpr(can_at) {
      return do_at(off);
    } else {
      return *(*this + off);
    }
  }

  template<typename Other>
  requires can_equal_to<Other>
  [[nodiscard]] friend constexpr bool operator==(
    iterator_facade const& a,
    Other const& b
  ) noexcept(noexcept(a.do_equal_to(b))) {
    return a.do_equal_to(b);
  }

  template<typename Other>
  requires can_distance_to<Other>
  [[nodiscard]] friend constexpr std::strong_ordering operator<=>(
    iterator_facade const& a,
    Other const& b
  ) {
    return a - b <=> 0;
  }
};

template<typename Core>
void constexpr swap(iterator_facade<Core>& a, iterator_facade<Core>& b)
    noexcept(std::is_nothrow_swappable_v<Core>) {
  a.swap(b);
}

} // util

#endif
