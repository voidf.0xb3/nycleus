#include <boost/test/unit_test.hpp>

#include "util/safe_int.hpp"
#include <stdexcept>

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(safe_int)

///////////////////////////////////////////////////////////////////////////////////////////////
// Unsigned integers //////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(unsigned_int)

// Addition ///////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(add)

BOOST_AUTO_TEST_CASE(checked_below) {
  auto const value{util::uint8_t{245}.checked_add(5)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{250});
}

BOOST_AUTO_TEST_CASE(checked_at) {
  auto const value{util::uint8_t{245}.checked_add(10)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(checked_above) {
  auto const value{util::uint8_t{245}.checked_add(20)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted_below) {
  auto const value{util::uint8_t{245} + util::uint8_t{5}};
  BOOST_TEST(value == util::uint8_t{250});
}

BOOST_AUTO_TEST_CASE(asserted_at) {
  auto const value{util::uint8_t{245} + util::uint8_t{10}};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(wrapping_below) {
  auto const value{util::uint8_t{245}.wrapping_add(5)};
  BOOST_TEST(value == util::uint8_t{250});
}

BOOST_AUTO_TEST_CASE(wrapping_at) {
  auto const value{util::uint8_t{245}.wrapping_add(10)};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(wrapping_above) {
  auto const value{util::uint8_t{245}.wrapping_add(20)};
  BOOST_TEST(value == util::uint8_t{9});
}

BOOST_AUTO_TEST_CASE(throwing_below) {
  auto const value{util::uint8_t{245}.throwing_add(5)};
  BOOST_TEST(value == util::uint8_t{250});
}

BOOST_AUTO_TEST_CASE(throwing_at) {
  auto const value{util::uint8_t{245}.throwing_add(10)};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(throwing_above) {
  BOOST_CHECK_THROW(static_cast<void>(util::uint8_t{245}.throwing_add(20)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(saturating_below) {
  auto const value{util::uint8_t{245}.saturating_add(5)};
  BOOST_TEST(value == util::uint8_t{250});
}

BOOST_AUTO_TEST_CASE(saturating_at) {
  auto const value{util::uint8_t{245}.saturating_add(10)};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(saturating_above) {
  auto const value{util::uint8_t{245}.saturating_add(20)};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_SUITE_END() // add

// Subtraction ////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(sub)

BOOST_AUTO_TEST_CASE(checked_above) {
  auto const value{util::uint8_t{10}.checked_sub(5)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{5});
}

BOOST_AUTO_TEST_CASE(checked_at) {
  auto const value{util::uint8_t{10}.checked_sub(10)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{0});
}

BOOST_AUTO_TEST_CASE(checked_below) {
  auto const value{util::uint8_t{10}.checked_sub(20)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted_above) {
  auto const value{util::uint8_t{10} - util::uint8_t{5}};
  BOOST_TEST(value == util::uint8_t{5});
}

BOOST_AUTO_TEST_CASE(asserted_at) {
  auto const value{util::uint8_t{10} - util::uint8_t{10}};
  BOOST_TEST(value == util::uint8_t{0});
}

BOOST_AUTO_TEST_CASE(wrapping_above) {
  auto const value{util::uint8_t{10}.wrapping_sub(5)};
  BOOST_TEST(value == util::uint8_t{5});
}

BOOST_AUTO_TEST_CASE(wrapping_at) {
  auto const value{util::uint8_t{10}.wrapping_sub(10)};
  BOOST_TEST(value == util::uint8_t{0});
}

BOOST_AUTO_TEST_CASE(wrapping_below) {
  auto const value{util::uint8_t{10}.wrapping_sub(20)};
  BOOST_TEST(value == util::uint8_t{246});
}

BOOST_AUTO_TEST_CASE(throwing_above) {
  auto const value{util::uint8_t{10}.throwing_sub(5)};
  BOOST_TEST(value == util::uint8_t{5});
}

BOOST_AUTO_TEST_CASE(throwing_at) {
  auto const value{util::uint8_t{10}.throwing_sub(10)};
  BOOST_TEST(value == util::uint8_t{0});
}

BOOST_AUTO_TEST_CASE(throwing_below) {
  BOOST_CHECK_THROW(static_cast<void>(util::uint8_t{10}.throwing_sub(20)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(saturating_above) {
  auto const value{util::uint8_t{10}.saturating_sub(5)};
  BOOST_TEST(value == util::uint8_t{5});
}

BOOST_AUTO_TEST_CASE(saturating_at) {
  auto const value{util::uint8_t{10}.saturating_sub(10)};
  BOOST_TEST(value == util::uint8_t{0});
}

BOOST_AUTO_TEST_CASE(saturating_below) {
  auto const value{util::uint8_t{10}.saturating_sub(20)};
  BOOST_TEST(value == util::uint8_t{0});
}

BOOST_AUTO_TEST_SUITE_END() // sub

// Multiplication /////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(mul)

BOOST_AUTO_TEST_CASE(checked_below) {
  auto const value{util::uint8_t{100}.checked_mul(2)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(checked_at) {
  auto const value{util::uint8_t{15}.checked_mul(17)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(checked_above) {
  auto const value{util::uint8_t{100}.checked_mul(3)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted_below) {
  auto const value{util::uint8_t{100} * util::uint8_t{2}};
  BOOST_TEST(value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(asserted_at) {
  auto const value{util::uint8_t{15} * util::uint8_t{17}};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(wrapping_below) {
  auto const value{util::uint8_t{100}.wrapping_mul(2)};
  BOOST_TEST(value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(wrapping_at) {
  auto const value{util::uint8_t{15}.wrapping_mul(17)};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(wrapping_above) {
  auto const value{util::uint8_t{100}.wrapping_mul(3)};
  BOOST_TEST(value == util::uint8_t{44});
}

BOOST_AUTO_TEST_CASE(throwing_below) {
  auto const value{util::uint8_t{100}.throwing_mul(2)};
  BOOST_TEST(value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(throwing_at) {
  auto const value{util::uint8_t{15}.throwing_mul(17)};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(throwing_above) {
  BOOST_CHECK_THROW(static_cast<void>(util::uint8_t{100}.throwing_mul(3)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(saturating_below) {
  auto const value{util::uint8_t{100}.saturating_mul(2)};
  BOOST_TEST(value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(saturating_at) {
  auto const value{util::uint8_t{15}.saturating_mul(17)};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_CASE(saturating_above) {
  auto const value{util::uint8_t{100}.saturating_mul(3)};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_SUITE_END() // mul

// Division ///////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(div)

BOOST_AUTO_TEST_CASE(checked) {
  auto const value{util::uint8_t{100}.checked_div(2)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{50});
}

BOOST_AUTO_TEST_CASE(asserted) {
  auto const value{util::uint8_t{100} / util::uint8_t{2}};
  BOOST_TEST(value == util::uint8_t{50});
}

BOOST_AUTO_TEST_CASE(wrapping) {
  auto const value{util::uint8_t{100}.wrapping_div(2)};
  BOOST_TEST(value == util::uint8_t{50});
}

BOOST_AUTO_TEST_CASE(throwing) {
  auto const value{util::uint8_t{100}.throwing_div(2)};
  BOOST_TEST(value == util::uint8_t{50});
}

BOOST_AUTO_TEST_CASE(saturating) {
  auto const value{util::uint8_t{100}.saturating_div(2)};
  BOOST_TEST(value == util::uint8_t{50});
}

BOOST_AUTO_TEST_SUITE_END() // div

// Conversion /////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(cast)

BOOST_AUTO_TEST_CASE(checked) {
  auto const value{util::uint16_t{200}.checked_cast<util::uint8_t>()};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(checked_fail) {
  auto const value{util::uint16_t{1000}.checked_cast<util::uint8_t>()};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted) {
  auto const value{util::uint16_t{200}.cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(throwing) {
  auto const value{util::uint16_t{200}.throwing_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(throwing_fail) {
  BOOST_CHECK_THROW(static_cast<void>(util::uint16_t{1000}.throwing_cast<util::uint8_t>()),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(lossy) {
  auto const value{util::uint16_t{200}.lossy_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(lossy_fail) {
  auto const value{util::uint16_t{1000}.lossy_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{232});
}

BOOST_AUTO_TEST_CASE(saturating) {
  auto const value{util::uint16_t{200}.saturating_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{200});
}

BOOST_AUTO_TEST_CASE(saturating_fail) {
  auto const value{util::uint16_t{1000}.saturating_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{255});
}

BOOST_AUTO_TEST_SUITE_END() // cast

BOOST_AUTO_TEST_SUITE_END() // unsigned_int

///////////////////////////////////////////////////////////////////////////////////////////////
// Signed integers ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(signed_int)

// Addition ///////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(add)

BOOST_AUTO_TEST_CASE(checked_below_top) {
  auto const value{util::int8_t{117}.checked_add(5)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(checked_at_top) {
  auto const value{util::int8_t{117}.checked_add(10)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(checked_above_top) {
  auto const value{util::int8_t{117}.checked_add(20)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(checked_above_bottom) {
  auto const value{util::int8_t{-118}.checked_add(-5)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(checked_at_bottom) {
  auto const value{util::int8_t{-118}.checked_add(-10)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(checked_below_bottom) {
  auto const value{util::int8_t{-118}.checked_add(-20)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted_below_top) {
  auto const value{util::int8_t{117} + util::int8_t{5}};
  BOOST_TEST(value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(asserted_at_top) {
  auto const value{util::int8_t{117} + util::int8_t{10}};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(asserted_above_bottom) {
  auto const value{util::int8_t{-118} + util::int8_t{-5}};
  BOOST_TEST(value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(asserted_at_bottom) {
  auto const value{util::int8_t{-118} + util::int8_t{-10}};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(wrapping_below_top) {
  auto const value{util::int8_t{117}.wrapping_add(5)};
  BOOST_TEST(value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(wrapping_at_top) {
  auto const value{util::int8_t{117}.wrapping_add(10)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(wrapping_above_top) {
  auto const value{util::int8_t{117}.wrapping_add(20)};
  BOOST_TEST(value == util::int8_t{-119});
}

BOOST_AUTO_TEST_CASE(wrapping_above_bottom) {
  auto const value{util::int8_t{-118}.wrapping_add(-5)};
  BOOST_TEST(value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(wrapping_at_bottom) {
  auto const value{util::int8_t{-118}.wrapping_add(-10)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(wrapping_below_bottom) {
  auto const value{util::int8_t{-118}.wrapping_add(-20)};
  BOOST_TEST(value == util::int8_t{118});
}

BOOST_AUTO_TEST_CASE(throwing_below_top) {
  auto const value{util::int8_t{117}.throwing_add(5)};
  BOOST_TEST(value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(throwing_at_top) {
  auto const value{util::int8_t{117}.throwing_add(10)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(throwing_above_top) {
  BOOST_CHECK_THROW(static_cast<void>(util::int8_t{117}.throwing_add(20)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(throwing_above_bottom) {
  auto const value{util::int8_t{-118}.throwing_add(-5)};
  BOOST_TEST(value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(throwing_at_bottom) {
  auto const value{util::int8_t{-118}.throwing_add(-10)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(throwing_below_bottom) {
  BOOST_CHECK_THROW(static_cast<void>(util::int8_t{-118}.throwing_add(-20)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(saturating_below_top) {
  auto const value{util::int8_t{117}.saturating_add(5)};
  BOOST_TEST(value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(saturating_at_top) {
  auto const value{util::int8_t{117}.saturating_add(10)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(saturating_above_top) {
  auto const value{util::int8_t{117}.saturating_add(20)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(saturating_above_bottom) {
  auto const value{util::int8_t{-118}.saturating_add(-5)};
  BOOST_TEST(value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(saturating_at_bottom) {
  auto const value{util::int8_t{-118}.saturating_add(-10)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(saturating_below_bottom) {
  auto const value{util::int8_t{-118}.saturating_add(-20)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_SUITE_END() // add

// Subtraction ////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(sub)

BOOST_AUTO_TEST_CASE(checked_below_top) {
  auto const value{util::int8_t{117}.checked_sub(-5)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(checked_at_top) {
  auto const value{util::int8_t{117}.checked_sub(-10)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(checked_above_top) {
  auto const value{util::int8_t{117}.checked_sub(-20)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(checked_above_bottom) {
  auto const value{util::int8_t{-118}.checked_sub(5)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(checked_at_bottom) {
  auto const value{util::int8_t{-118}.checked_sub(10)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(checked_below_bottom) {
  auto const value{util::int8_t{-118}.checked_sub(20)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted_below_top) {
  auto const value{util::int8_t{117} - util::int8_t{-5}};
  BOOST_TEST(value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(asserted_at_top) {
  auto const value{util::int8_t{117} - util::int8_t{-10}};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(asserted_above_bottom) {
  auto const value{util::int8_t{-118} - util::int8_t{5}};
  BOOST_TEST(value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(asserted_at_bottom) {
  auto const value{util::int8_t{-118} - util::int8_t{10}};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(wrapping_below_top) {
  auto const value{util::int8_t{117}.wrapping_sub(-5)};
  BOOST_TEST(value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(wrapping_at_top) {
  auto const value{util::int8_t{117}.wrapping_sub(-10)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(wrapping_above_top) {
  auto const value{util::int8_t{117}.wrapping_sub(-20)};
  BOOST_TEST(value == util::int8_t{-119});
}

BOOST_AUTO_TEST_CASE(wrapping_above_bottom) {
  auto const value{util::int8_t{-118}.wrapping_sub(5)};
  BOOST_TEST(value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(wrapping_at_bottom) {
  auto const value{util::int8_t{-118}.wrapping_sub(10)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(wrapping_below_bottom) {
  auto const value{util::int8_t{-118}.wrapping_sub(20)};
  BOOST_TEST(value == util::int8_t{118});
}

BOOST_AUTO_TEST_CASE(throwing_below_top) {
  auto const value{util::int8_t{117}.throwing_sub(-5)};
  BOOST_TEST(value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(throwing_at_top) {
  auto const value{util::int8_t{117}.throwing_sub(-10)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(throwing_above_top) {
  BOOST_CHECK_THROW(static_cast<void>(util::int8_t{117}.throwing_sub(-20)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(throwing_above_bottom) {
  auto const value{util::int8_t{-118}.throwing_sub(5)};
  BOOST_TEST(value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(throwing_at_bottom) {
  auto const value{util::int8_t{-118}.throwing_sub(10)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(throwing_below_bottom) {
  BOOST_CHECK_THROW(static_cast<void>(util::int8_t{-118}.throwing_sub(20)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(saturating_below_top) {
  auto const value{util::int8_t{117}.saturating_sub(-5)};
  BOOST_TEST(value == util::int8_t{122});
}

BOOST_AUTO_TEST_CASE(saturating_at_top) {
  auto const value{util::int8_t{117}.saturating_sub(-10)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(saturating_above_top) {
  auto const value{util::int8_t{117}.saturating_sub(-20)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(saturating_above_bottom) {
  auto const value{util::int8_t{-118}.saturating_sub(5)};
  BOOST_TEST(value == util::int8_t{-123});
}

BOOST_AUTO_TEST_CASE(saturating_at_bottom) {
  auto const value{util::int8_t{-118}.saturating_sub(10)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(saturating_below_bottom) {
  auto const value{util::int8_t{-118}.saturating_sub(20)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_SUITE_END() // sub

// Multiplication /////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(mul)

BOOST_AUTO_TEST_CASE(checked_below_top) {
  auto const value{util::int8_t{2}.checked_mul(50)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(checked_at_top) {
  auto const value{util::int8_t{-127}.checked_mul(-1)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(checked_above_top) {
  auto const value{util::int8_t{100}.checked_mul(3)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(checked_above_bottom) {
  auto const value{util::int8_t{-2}.checked_mul(50)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(checked_at_bottom) {
  auto const value{util::int8_t{-32}.checked_mul(4)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(checked_below_bottom) {
  auto const value{util::int8_t{3}.checked_mul(-100)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted_below_top) {
  auto const value{util::int8_t{2} * util::int8_t{50}};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(asserted_at_top) {
  auto const value{util::int8_t{-127} * util::int8_t{-1}};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(asserted_above_bottom) {
  auto const value{util::int8_t{-2} * util::int8_t{50}};
  BOOST_TEST(value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(asserted_at_bottom) {
  auto const value{util::int8_t{-32} * util::int8_t{4}};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(wrapping_below_top) {
  auto const value{util::int8_t{2}.wrapping_mul(50)};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(wrapping_at_top) {
  auto const value{util::int8_t{-127}.wrapping_mul(-1)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(wrapping_above_top) {
  auto const value{util::int8_t{100}.wrapping_mul(3)};
  BOOST_TEST(value == util::int8_t{44});
}

BOOST_AUTO_TEST_CASE(wrapping_above_bottom) {
  auto const value{util::int8_t{-2}.wrapping_mul(50)};
  BOOST_TEST(value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(wrapping_at_bottom) {
  auto const value{util::int8_t{-32}.wrapping_mul(4)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(wrapping_below_bottom) {
  auto const value{util::int8_t{3}.wrapping_mul(-100)};
  BOOST_TEST(value == util::int8_t{-44});
}

BOOST_AUTO_TEST_CASE(throwing_below_top) {
  auto const value{util::int8_t{2}.throwing_mul(50)};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(throwing_at_top) {
  auto const value{util::int8_t{-127}.throwing_mul(-1)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(throwing_above_top) {
  BOOST_CHECK_THROW(static_cast<void>(util::int8_t{100}.throwing_mul(3)), std::overflow_error);
}

BOOST_AUTO_TEST_CASE(throwing_above_bottom) {
  auto const value{util::int8_t{-2}.throwing_mul(50)};
  BOOST_TEST(value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(throwing_at_bottom) {
  auto const value{util::int8_t{-32}.throwing_mul(4)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(throwing_below_bottom) {
  BOOST_CHECK_THROW(static_cast<void>(util::int8_t{3}.throwing_mul(-100)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(saturating_below_top) {
  auto const value{util::int8_t{2}.saturating_mul(50)};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(saturating_at_top) {
  auto const value{util::int8_t{-127}.saturating_mul(-1)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(saturating_above_top) {
  auto const value{util::int8_t{100}.saturating_mul(3)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_CASE(saturating_above_bottom) {
  auto const value{util::int8_t{-2}.saturating_mul(50)};
  BOOST_TEST(value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(saturating_at_bottom) {
  auto const value{util::int8_t{-32}.saturating_mul(4)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(saturating_below_bottom) {
  auto const value{util::int8_t{3}.saturating_mul(-100)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_SUITE_END() // mul

// Division ///////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(div)

BOOST_AUTO_TEST_CASE(checked) {
  auto const value{util::int8_t{-100}.checked_div(-3)};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{33});
}

BOOST_AUTO_TEST_CASE(checked_fail) {
  auto const value{util::int8_t{-128}.checked_div(-1)};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted) {
  auto const value{util::int8_t{-100} / util::int8_t{-3}};
  BOOST_TEST(value == util::int8_t{33});
}

BOOST_AUTO_TEST_CASE(wrapping) {
  auto const value{util::int8_t{-100}.wrapping_div(-3)};
  BOOST_TEST(value == util::int8_t{33});
}

BOOST_AUTO_TEST_CASE(wrapping_fail) {
  auto const value{util::int8_t{-128}.wrapping_div(-1)};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(throwing) {
  auto const value{util::int8_t{-100}.throwing_div(-3)};
  BOOST_TEST(value == util::int8_t{33});
}

BOOST_AUTO_TEST_CASE(throwing_fail) {
  BOOST_CHECK_THROW(static_cast<void>(util::int8_t{-128}.throwing_div(-1)),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(saturating) {
  auto const value{util::int8_t{-100}.saturating_div(-3)};
  BOOST_TEST(value == util::int8_t{33});
}

BOOST_AUTO_TEST_CASE(saturating_fail) {
  auto const value{util::int8_t{-128}.saturating_div(-1)};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_SUITE_END() // div

// Negation ///////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(neg)

BOOST_AUTO_TEST_CASE(checked) {
  auto const value{util::int8_t{-100}.checked_neg()};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(checked_fail) {
  auto const value{util::int8_t{-128}.checked_neg()};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted) {
  auto const value{-util::int8_t{-100}};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(wrapping) {
  auto const value{util::int8_t{-100}.wrapping_neg()};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(wrapping_fail) {
  auto const value{util::int8_t{-128}.wrapping_neg()};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_CASE(throwing) {
  auto const value{util::int8_t{-100}.throwing_neg()};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(throwing_fail) {
  BOOST_CHECK_THROW(static_cast<void>(util::int8_t{-128}.throwing_neg()),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(saturating) {
  auto const value{util::int8_t{-100}.saturating_neg()};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(saturating_fail) {
  auto const value{util::int8_t{-128}.saturating_neg()};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_SUITE_END() // neg

// Conversion /////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(cast)

BOOST_AUTO_TEST_CASE(checked) {
  auto const value{util::int16_t{-100}.checked_cast<util::int8_t>()};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(checked_fail) {
  auto const value{util::int16_t{-1000}.checked_cast<util::int8_t>()};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted) {
  auto const value{util::int16_t{-100}.cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(throwing) {
  auto const value{util::int16_t{-100}.throwing_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(throwing_fail) {
  BOOST_CHECK_THROW(static_cast<void>(util::int16_t{-1000}.throwing_cast<util::int8_t>()),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(lossy) {
  auto const value{util::int16_t{-100}.lossy_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(lossy_fail) {
  auto const value{util::int16_t{-1000}.lossy_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{24});
}

BOOST_AUTO_TEST_CASE(saturating) {
  auto const value{util::int16_t{-100}.saturating_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{-100});
}

BOOST_AUTO_TEST_CASE(saturating_fail) {
  auto const value{util::int16_t{-1000}.saturating_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{-128});
}

BOOST_AUTO_TEST_SUITE_END() // cast

BOOST_AUTO_TEST_SUITE_END() // signed_int

///////////////////////////////////////////////////////////////////////////////////////////////
// Casts across signedness ////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

// From unsigned to signed ////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(cast_unsigned_to_signed)

BOOST_AUTO_TEST_CASE(checked) {
  auto const value{util::uint16_t{100}.checked_cast<util::int8_t>()};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(checked_fail) {
  auto const value{util::uint16_t{1000}.checked_cast<util::int8_t>()};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted) {
  auto const value{util::uint16_t{100}.cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(throwing) {
  auto const value{util::uint16_t{100}.throwing_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(throwing_fail) {
  BOOST_CHECK_THROW(static_cast<void>(util::uint16_t{1000}.throwing_cast<util::int8_t>()),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(lossy) {
  auto const value{util::uint16_t{100}.lossy_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(lossy_fail) {
  auto const value{util::uint16_t{1000}.lossy_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{-24});
}

BOOST_AUTO_TEST_CASE(saturating) {
  auto const value{util::uint16_t{100}.saturating_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{100});
}

BOOST_AUTO_TEST_CASE(saturating_fail) {
  auto const value{util::uint16_t{1000}.saturating_cast<util::int8_t>()};
  BOOST_TEST(value == util::int8_t{127});
}

BOOST_AUTO_TEST_SUITE_END() // cast_unsigned_to_signed

// From signed to unsigned ////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(cast_signed_to_unsigned)

BOOST_AUTO_TEST_CASE(checked) {
  auto const value{util::int16_t{100}.checked_cast<util::uint8_t>()};
  BOOST_REQUIRE(value.has_value());
  BOOST_TEST(*value == util::uint8_t{100});
}

BOOST_AUTO_TEST_CASE(checked_fail) {
  auto const value{util::int16_t{-10}.checked_cast<util::uint8_t>()};
  BOOST_TEST(!value.has_value());
}

BOOST_AUTO_TEST_CASE(asserted) {
  auto const value{util::int16_t{100}.cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{100});
}

BOOST_AUTO_TEST_CASE(throwing) {
  auto const value{util::int16_t{100}.throwing_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{100});
}

BOOST_AUTO_TEST_CASE(throwing_fail) {
  BOOST_CHECK_THROW(static_cast<void>(util::int16_t{-10}.throwing_cast<util::uint8_t>()),
    std::overflow_error);
}

BOOST_AUTO_TEST_CASE(lossy) {
  auto const value{util::int16_t{100}.lossy_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{100});
}

BOOST_AUTO_TEST_CASE(lossy_fail) {
  auto const value{util::int16_t{-10}.lossy_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{246});
}

BOOST_AUTO_TEST_CASE(saturating) {
  auto const value{util::int16_t{100}.saturating_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{100});
}

BOOST_AUTO_TEST_CASE(saturating_fail) {
  auto const value{util::int16_t{-10}.saturating_cast<util::uint8_t>()};
  BOOST_TEST(value == util::uint8_t{0});
}

BOOST_AUTO_TEST_SUITE_END() // cast_signed_to_unsigned

BOOST_AUTO_TEST_SUITE_END() // safe_int
BOOST_AUTO_TEST_SUITE_END() // test_util
