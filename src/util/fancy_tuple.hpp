#ifndef UTIL_FANCY_TUPLE_HPP_INCLUDED_56BI4UA26CGMFBN5W4X714Y4H
#define UTIL_FANCY_TUPLE_HPP_INCLUDED_56BI4UA26CGMFBN5W4X714Y4H

#include <concepts>
#include <cstddef>
#include <tuple>
#include <type_traits>
#include <utility>

namespace util {

// Zip-view tuple //////////////////////////////////////////////////////////////

/** @brief A wrapper around std::tuple for which we provide the specialization
 * of std::basic_common_reference that exists in C++23, as well as some
 * additional constructors.
 *
 * This is used as the value and reference type of certain views instead of
 * std::tuple, because they on the specialization of std::basic_common_reference
 * and certain constructors that only exist in C++23.
 *
 * A specialization of std::common_type from C++23 is also provided, since it's
 * there anyway. */
template<typename... Ts>
struct fancy_tuple: std::tuple<Ts...> {
  using base = std::tuple<Ts...>;

  using base::base;

private:
  struct private_ctor {};

  template<std::size_t... Is, typename... Us>
  constexpr explicit fancy_tuple(
    private_ctor,
    std::index_sequence<Is...>,
    std::tuple<Us...>& other
  ): base{std::get<Is>(other)...} {}

  template<std::size_t... Is, typename... Us>
  constexpr explicit fancy_tuple(
    private_ctor,
    std::index_sequence<Is...>,
    std::tuple<Us...> const&& other
  ): base{std::get<Is>(std::move(other))...} {}

public:
  template<typename... Us>
  requires ((sizeof...(Ts) == sizeof...(Us)) && ...
    && std::is_constructible_v<Ts, Us&>)
    && (
      sizeof...(Ts) != 1
      || !std::same_as<std::tuple_element_t<0, base>,
        std::tuple_element_t<0, std::tuple<Us...>>>
      && std::is_convertible_v<std::tuple<Us...>&,
        std::tuple_element_t<0, base>>
      && std::is_convertible_v<std::tuple_element_t<0, base>,
        std::tuple<Us...>&>
    )
  constexpr explicit((... || !std::is_convertible_v<Us&, Ts>))
    fancy_tuple(std::tuple<Us...>& other):
    fancy_tuple{private_ctor{},
    std::make_index_sequence<sizeof...(Ts)>{}, other} {}

  template<typename... Us>
  requires ((sizeof...(Ts) == sizeof...(Us)) && ...
    && std::is_constructible_v<Ts, Us const&&>)
    && (
      sizeof...(Ts) != 1
      || !std::same_as<std::tuple_element_t<0, base>,
        std::tuple_element_t<0, std::tuple<Us...>>>
      && std::is_convertible_v<std::tuple<Us...> const&&,
        std::tuple_element_t<0, base>>
      && std::is_convertible_v<std::tuple_element_t<0, base>,
        std::tuple<Us...> const&&>
    )
  constexpr explicit((... || !std::is_convertible_v<Us const&&, Ts>))
    fancy_tuple(std::tuple<Us...> const&& other):
    fancy_tuple{private_ctor{},
    std::make_index_sequence<sizeof...(Ts)>{}, std::move(other)} {}

  template<typename U1, typename U2>
  requires (sizeof...(Ts) == 2)
    && std::is_constructible_v<std::tuple_element_t<0, base>, U1&>
    && std::is_constructible_v<std::tuple_element_t<1, base>, U2&>
  constexpr explicit(
    !std::is_convertible_v<U1&, std::tuple_element_t<0, base>>
    || !std::is_convertible_v<U2&, std::tuple_element_t<1, base>>
  ) fancy_tuple(std::pair<U1, U2>& other): base{other.first, other.second} {}

  template<typename U1, typename U2>
  requires (sizeof...(Ts) == 2)
    && std::is_constructible_v<std::tuple_element_t<0, base>, U1 const&&>
    && std::is_constructible_v<std::tuple_element_t<1, base>, U2 const&&>
  constexpr explicit(
    !std::is_convertible_v<U1 const&&, std::tuple_element_t<0, base>>
    || !std::is_convertible_v<U2 const&&, std::tuple_element_t<1, base>>
  ) fancy_tuple(std::pair<U1, U2> const&& other):
    base{std::move(other.first), std::move(other.second)} {}

  constexpr fancy_tuple(fancy_tuple const&) = default;
  constexpr fancy_tuple(fancy_tuple&&) = default;

  constexpr fancy_tuple& operator=(fancy_tuple const&) = default;
  constexpr fancy_tuple& operator=(fancy_tuple&&) = default;

  constexpr ~fancy_tuple() = default;

  template<typename... Us>
  requires ((sizeof...(Us) == sizeof...(Ts)) && ...
    && std::is_assignable_v<Ts&, Us const&>)
  constexpr fancy_tuple& operator=(std::tuple<Us...> const& other) {
    base::operator=(other);
    return *this;
  }

  template<typename... Us>
  requires ((sizeof...(Us) == sizeof...(Ts)) && ...
    && std::is_assignable_v<Ts&, Us&&>)
  constexpr fancy_tuple& operator=(std::tuple<Us...>&& other) {
    base::operator=(std::move(other));
    return *this;
  }

  template<typename U1, typename U2>
  requires (sizeof...(Ts) == 2
    && std::is_assignable_v<std::tuple_element<0, base>&, U1 const&>
    && std::is_assignable_v<std::tuple_element<1, base>&, U2 const&>)
  constexpr fancy_tuple& operator=(std::pair<U1, U2> const& other) {
    base::operator=(other);
    return *this;
  }

  template<typename U1, typename U2>
  requires (sizeof...(Ts) == 2
    && std::is_assignable_v<std::tuple_element<0, base>&, U1&&>
    && std::is_assignable_v<std::tuple_element<1, base>&, U2&&>)
  constexpr fancy_tuple& operator=(std::pair<U1, U2>&& other) {
    base::operator=(std::move(other));
    return *this;
  }
};

} // util

template<typename... Ts>
struct std::tuple_size<::util::fancy_tuple<Ts...>>:
  ::std::tuple_size<::std::tuple<Ts...>> {};

template<std::size_t I, typename... Ts>
struct std::tuple_element<I, ::util::fancy_tuple<Ts...>>:
  ::std::tuple_element<I, ::std::tuple<Ts...>> {};

template<typename... Ts, typename... Us, template<typename> typename TQual,
  template<typename> typename UQual>
requires ((sizeof...(Ts) == sizeof...(Us)) && ...
  && requires { typename ::std::common_reference_t<Ts, Us>; })
struct std::basic_common_reference<::util::fancy_tuple<Ts...>,
    ::util::fancy_tuple<Us...>, TQual, UQual> {
  using type = ::util::fancy_tuple<
    ::std::common_reference_t<TQual<Ts>, UQual<Us>>...>;
};

template<typename... Ts, typename... Us>
requires ((sizeof...(Ts) == sizeof...(Us)) && ...
  && requires { typename ::std::common_type_t<Ts, Us>; })
struct std::common_type<::util::fancy_tuple<Ts...>,
    ::util::fancy_tuple<Us...>> {
  using type = ::util::fancy_tuple<::std::common_type_t<Ts, Us>...>;
};

#endif
