#include <boost/test/unit_test.hpp>

#include "util/cow.hpp"
#include <cstdint>
#include <memory>

namespace {

struct cow_test_obj {
  std::uintmax_t* counter;
  std::uintmax_t id;

  explicit cow_test_obj(std::uintmax_t* counter, std::uintmax_t id) noexcept:
      counter{counter}, id{id} {
    ++*counter;
  }
  cow_test_obj(cow_test_obj const& other) noexcept: counter{other.counter},
      id{other.id} {
    ++*counter;
  }
  cow_test_obj(cow_test_obj&& other) noexcept: counter{other.counter},
      id{other.id} {
    ++*counter;
  }
  ~cow_test_obj() noexcept {
    --*counter;
  }
  cow_test_obj& operator=(cow_test_obj const&) = delete;
  cow_test_obj& operator=(cow_test_obj&&) = delete;
};

}

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(cow)

BOOST_AUTO_TEST_CASE(simple) {
  std::uintmax_t counter{0};
  {
    util::cow<cow_test_obj> const cow1{&counter,
      static_cast<std::uintmax_t>(100)};
    BOOST_TEST(counter == 1);
    {
      auto cow2{cow1}; // NOLINT(performance-unnecessary-copy-initialization)
      BOOST_TEST(counter == 1);
      BOOST_TEST(cow1->id == 100);
      BOOST_TEST(cow2->id == 100);
    }
    BOOST_TEST(counter == 1);
  }
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_CASE(modify) {
  std::uintmax_t counter{0};
  {
    util::cow<cow_test_obj> const cow1{&counter,
      static_cast<std::uintmax_t>(100)};
    BOOST_TEST(counter == 1);
    {
      auto cow2{cow1};
      BOOST_TEST(counter == 1);
      {
        auto cow3{cow2};
        BOOST_TEST(counter == 1);
        BOOST_TEST(cow1->id == 100);
        BOOST_TEST(cow2->id == 100);
        BOOST_TEST(cow3->id == 100);

        cow2.modify().id = 200;
        BOOST_TEST(counter == 2);
        BOOST_TEST(cow1->id == 100);
        BOOST_TEST(cow2->id == 200);
        BOOST_TEST(cow3->id == 100);
      }
      BOOST_TEST(counter == 2);
    }
    BOOST_TEST(counter == 1);
  }
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_CASE(modify_exclsuive) {
  std::uintmax_t counter{0};
  auto cow1{std::make_unique<util::cow<cow_test_obj>>(&counter,
    static_cast<std::uintmax_t>(100))};
  BOOST_TEST(counter == 1);
  auto cow2{std::make_unique<util::cow<cow_test_obj>>(*cow1)};
  BOOST_TEST(counter == 1);
  cow1.reset();
  BOOST_TEST(counter == 1);
  cow2->modify().id = 200;
  BOOST_TEST(counter == 1);
  BOOST_TEST((*cow2)->id == 200);
  cow2.reset();
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_SUITE_END() // cow
BOOST_AUTO_TEST_SUITE_END() // test_util
