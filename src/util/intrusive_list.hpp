#ifndef UTIL_INTRUSIVE_LIST_HPP_INCLUDED_UED3B4KFY9WYZCA9ZI6Y5J9M7
#define UTIL_INTRUSIVE_LIST_HPP_INCLUDED_UED3B4KFY9WYZCA9ZI6Y5J9M7

#include "util/iterator_facade.hpp"
#include <cassert>
#include <concepts>
#include <functional>
#include <iterator>
#include <ranges>
#include <type_traits>
#include <utility>

namespace util {

/** @brief The base class for types that can be elements of an intrusive list.
 */
class intrusive_list_hook;

/** @brief A linked list consisting of externally supplied elements.
 *
 * Unlike an ordinary linked list, the intrusive list does not allocate its
 * elements and does not control their lifetimes. Instead, client code is
 * expected to derive the element type from `intrusive_list_hook`, which is then
 * used to connect the element to the list.
 *
 * If a list element is destroyed before the list object is destroyed, the
 * behavior is undefined. If the list object is destroyed while it has elements,
 * the element objects remain alive (but are no longer part of any list).
 *
 * An iterator to an element is invalidated only when that element ceases to be
 * an element of its containing list or when the list object is destroyed. */
template<std::derived_from<intrusive_list_hook> T>
class intrusive_list;

class intrusive_list_hook {
private:
  intrusive_list_hook* prev;
  intrusive_list_hook* next;

  template<std::derived_from<intrusive_list_hook> T>
  friend class intrusive_list;

  /** @brief Returns a true copy of this hook.
   *
   * This function is needed because the copy constructor has been changed below
   * and does not actually do a proper copy of the hook data. */
  [[nodiscard]] intrusive_list_hook clone() const noexcept {
    intrusive_list_hook result;
    result.prev = prev;
    result.next = next;
    return result;
  }

public:
  intrusive_list_hook() noexcept = default;
  ~intrusive_list_hook() noexcept = default;

  // List elements can be copyable and movable, but copying and/or moving them
  // does not affect their list memberships.

  intrusive_list_hook(intrusive_list_hook const&) noexcept {}
  intrusive_list_hook(intrusive_list_hook&&) noexcept {}
  // NOLINTNEXTLINE(bugprone-unhandled-self-assignment)
  intrusive_list_hook& operator=(intrusive_list_hook const&) noexcept {
    return *this;
  }
  intrusive_list_hook& operator=(intrusive_list_hook&&) noexcept {
    return *this;
  }
};

template<std::derived_from<intrusive_list_hook> T>
class intrusive_list {
private:
  // The hooks are actually origanized in a circular linked list, with this
  // special hook indicating the beginning and end. For an empty list, this hook
  // points to itself. This simplifies a lot of edge cases in the implemetation.
  intrusive_list_hook hook_;

  using il = intrusive_list;

  [[nodiscard]] static intrusive_list_hook& hook(T& t) noexcept {
    return t;
  }

  [[nodiscard]] static intrusive_list_hook const& hook(T const& t) noexcept {
    return t;
  }

public:
  using value_type = T;

  /** @brief Creates an empty list. */
  intrusive_list() noexcept {
    hook_.next = hook_.prev = &hook_;
  }

  /** @brief Creates a list with the given elements.
   *
   * If iteration over the range throws an exception, the elements are not
   * destroyed in any way; if that is undesirable, you can create an empty list
   * and then assign the range later. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T&>
  explicit intrusive_list(Iterator it, Sentinel iend)
      noexcept(noexcept(it != iend) && noexcept(++it) && noexcept(*it)):
      intrusive_list{} {
    this->insert(begin(), std::move(it), std::move(iend));
  }

  /** @brief Destroys the list.
   *
   * The list's elements are not destroyed. If it is necessary to destroy them,
   * that needs to be done separately. */
  ~intrusive_list() noexcept = default;

  intrusive_list(intrusive_list const&) = delete;
  intrusive_list& operator=(intrusive_list const&) = delete;

  void swap(intrusive_list& other) noexcept {
    intrusive_list_hook const old_hook{hook_.clone()};
    intrusive_list_hook const old_other_hook{other.hook_.clone()};

    old_hook.next->prev = &other.hook_;
    old_hook.prev->next = &other.hook_;
    old_other_hook.next->prev = &hook_;
    old_other_hook.prev->next = &hook_;

    std::swap(hook_.prev, other.hook_.prev);
    std::swap(hook_.next, other.hook_.next);
  }

  /** @brief Move-constructs the list.
   *
   * The moved-from list becomes empty. */
  intrusive_list(intrusive_list&& other) noexcept: intrusive_list{} {
    this->swap(other);
  }

  /** @brief Move-assigns the list.
   *
   * The old content of this list is assigned to the other list. (In other
   * words, this is equivalent to a swap.) */
  intrusive_list& operator=(intrusive_list&& other) noexcept {
    this->swap(other);
    return *this;
  }

  [[nodiscard]] bool empty() const noexcept {
    return hook_.next == &hook_;
  }

  // Element access ////////////////////////////////////////////////////////////

  [[nodiscard]] T const& front() const noexcept {
    assert(!empty());
    return static_cast<T const&>(*hook_.next);
  }

  [[nodiscard]] T& front() noexcept {
    assert(!empty());
    return static_cast<T&>(*hook_.next);
  }

  [[nodiscard]] T const& back() const noexcept {
    assert(!empty());
    return static_cast<T const&>(*hook_.prev);
  }

  [[nodiscard]] T& back() noexcept {
    assert(!empty());
    return static_cast<T&>(*hook_.prev);
  }

private:
  class const_iterator_core;

  class iterator_core {
  private:
    intrusive_list_hook* cur_;

    explicit iterator_core(intrusive_list_hook* cur) noexcept: cur_{cur} {}

    friend class intrusive_list;
    friend class const_iterator_core;

  public:
    iterator_core() noexcept = default;

  protected:
    [[nodiscard]] T& dereference() const noexcept {
      return static_cast<T&>(*cur_);
    }

    [[nodiscard]] bool equal_to(iterator_core other) const noexcept {
      return cur_ == other.cur_;
    }

    void increment() noexcept {
      cur_ = cur_->next;
    }

    void decrement() noexcept {
      cur_ = cur_->prev;
    }
  };

public:
  using iterator = iterator_facade<iterator_core>;
  static_assert(std::bidirectional_iterator<iterator>);

private:
  class const_iterator_core {
  private:
    intrusive_list_hook const* cur_;

    explicit const_iterator_core(intrusive_list_hook const* cur) noexcept:
      cur_{cur} {}

    friend class intrusive_list;

  public:
    const_iterator_core() noexcept = default;
    const_iterator_core(iterator other) noexcept: cur_{other.cur_} {}

  protected:
    [[nodiscard]] T const& dereference() const noexcept {
      return static_cast<T const&>(*cur_);
    }

    [[nodiscard]] bool equal_to(const_iterator_core other) const noexcept {
      return cur_ == other.cur_;
    }

    void increment() noexcept {
      cur_ = cur_->next;
    }

    void decrement() noexcept {
      cur_ = cur_->prev;
    }
  };

public:
  using const_iterator = iterator_facade<const_iterator_core>;
  static_assert(std::bidirectional_iterator<const_iterator>);

  [[nodiscard]] iterator begin() noexcept {
    return iterator{hook_.next};
  }
  [[nodiscard]] const_iterator begin() const noexcept {
    return const_iterator{hook_.next};
  }
  [[nodiscard]] const_iterator cbegin() const noexcept {
    return begin();
  }

  [[nodiscard]] iterator end() noexcept {
    return iterator{&hook_};
  }
  [[nodiscard]] const_iterator end() const noexcept {
    return const_iterator{&hook_};
  }
  [[nodiscard]] const_iterator cend() const noexcept {
    return end();
  }

  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  [[nodiscard]] reverse_iterator rbegin() noexcept {
    return reverse_iterator{end()};
  }
  [[nodiscard]] const_reverse_iterator rbegin() const noexcept {
    return const_reverse_iterator{end()};
  }
  [[nodiscard]] const_reverse_iterator crbegin() const noexcept {
    return rbegin();
  }

  [[nodiscard]] reverse_iterator rend() noexcept {
    return reverse_iterator{begin()};
  }
  [[nodiscard]] const_reverse_iterator rend() const noexcept {
    return const_reverse_iterator{begin()};
  }
  [[nodiscard]] const_reverse_iterator crend() const noexcept {
    return rend();
  }

  /** @brief Given an object, return an iterator to that object.
   *
   * There is no need to refer to the list object itself. If the object is not
   * an element of any list, the behavior is undefined. */
  [[nodiscard]] static iterator iterator_to(T& value) noexcept {
    return iterator{&value};
  }

  /** @brief Given an object, return a constant iterator to that object.
   *
   * There is no need to refer to the list object itself. If the object is not
   * an element of any list, the behavior is undefined. */
  [[nodiscard]] static const_iterator iterator_to(T const& value) noexcept {
    return const_iterator{&value};
  }

  /** @brief Given an object, return a constant iterator to that object.
   *
   * There is no need to refer to the list object itself. If the object is not
   * an element of any list, the behavior is undefined. */
  [[nodiscard]] static const_iterator const_iterator_to(T const& value)
      noexcept {
    return il::iterator_to(value);
  }

  // Assignment ////////////////////////////////////////////////////////////////

  /** @brief Replaces the content of the list with the given range.
   *
   * The amount of time does not depend on the number of elements that are in
   * the list prior to the call (but does depend on the number of new elements).
   *
   * The old elements are not destroyed in any way. If a range operation throws
   * an exception, the elements that have been successfully retrieved from the
   * range are added to the list. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T&>
  void assign(Iterator it, Sentinel iend)
      noexcept(noexcept(it != iend) && noexcept(++it) && noexcept(*it)) {
    clear();
    this->insert(begin(), std::move(it), std::move(iend));
  }

  /** @brief Disposes of the old elements of the list and replaces them with the
   * given range.
   *
   * If the disposer throws an exception, the elements removed from the list are
   * those and only those that have been passed to the disposer (this includes
   * the element passed to the throwing invocation). The supplied range is then
   * untouched.
   *
   * If a range operation throws an exception, the elements that have been
   * successfully retrieved from the range are added to the list. */
  template<std::invocable<T&> Disposer, std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T&>
  void dispose_and_assign(Disposer&& disposer, Iterator it, Sentinel iend)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>
      && noexcept(it != iend) && noexcept(++it) && noexcept(*it)) {
    this->clear_and_dispose(std::forward<Disposer>(disposer));
    this->insert(begin(), std::move(it), std::move(iend));
  }

  // Insertion /////////////////////////////////////////////////////////////////

  /** @brief Inserts the given element into the list immediately before the
   * element referred to by the iterator (or at the end, if given a past-the-end
   * iterator).
   * @returns An iterator to the newly inserted element. */
  static iterator insert(iterator pos, T& value) noexcept {
    il::hook(value).prev = pos.cur_->prev;
    il::hook(value).next = pos.cur_;
    pos.cur_->prev->next = &il::hook(value);
    pos.cur_->prev = &il::hook(value);
    return iterator{&value};
  }

  /** @brief Inserts the given elements into the list immediately before the
   * element referred to by the iterator (or at the end, if given a past-the-end
   * iterator).
   *
   * If a range operation throws an exception, the elements that have been
   * successfully retrieved from the range are added to the list.
   *
   * @returns An iterator to the first inserted element, or `pos` if the range
   *          is empty. */
  template<std::input_iterator Iterator,
    std::sentinel_for<Iterator> Sentinel = Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T&>
  iterator insert(iterator pos, Iterator it, Sentinel iend)
      noexcept(noexcept(it != iend) && noexcept(++it) && noexcept(*it)) {
    intrusive_list_hook& pre_first_hook{*pos.cur_->prev};

    if constexpr(noexcept(it != iend) && noexcept(++it) && noexcept(*it)) {
      intrusive_list_hook* last_hook{&pre_first_hook};

      while(it != iend) {
        T& value{*it};
        last_hook->next = &il::hook(value);
        il::hook(value).prev = last_hook;
        last_hook = &il::hook(value);
        ++it;
      }

      last_hook->next = pos.cur_;
      pos.cur_->prev = last_hook;
    } else {
      for(T& value: std::ranges::subrange{std::move(it), std::move(iend)}) {
        il::insert(pos, value);
      }
    }

    return iterator{pre_first_hook.next};
  }

  /** @brief Inserts the given element at the beginning of the list. */
  void push_front(T& value) noexcept {
    il::insert(begin(), value);
    assert(!empty());
  }

  /** @brief Inserts the given element at the end of the list. */
  void push_back(T& value) noexcept {
    il::insert(end(), value);
    assert(!empty());
  }

  /** @brief Adds all the elements from the given list and adds them to another
   * list at the provided position.
   *
   * This takes constant time and is therefore more efficient than manually
   * inserting the elements.
   *
   * Iterators referring to the moved elements are still valid and are now
   * iterators into the destination list.
   *
   * If the source list is the same as the destination list, then `pos` must be
   * `iend`; in that case, this has no effect.
   *
   * @returns An iterator to the first spliced element, or `pos` if the range is
   *          empty. */
  static iterator splice(iterator pos, intrusive_list& src) noexcept {
    return il::splice(pos, src.begin(), src.end());
  }

  /** @brief Removes the given range (`it` to `iend`) from one list and adds it
   * to another list at the provided position (`pos`).
   *
   * This takes constant time and is therefore more efficient than manually
   * removing and inserting the elements.
   *
   * Iterators referring to the moved elements are still valid and are now
   * iterators into the destination list.
   *
   * The source and the destination lists may be the same list; but if the `pos`
   * iterator points to one of the elements in the range, the behavior is
   * undefined. (If `pos == iend`, this has no effect.)
   *
   * @returns An iterator to the first spliced element, or `pos` if the range is
   *          empty. */
  static iterator splice(iterator pos, iterator it, iterator iend) noexcept {
    if(it == iend) {
      return pos;
    }

    intrusive_list_hook& last_hook{*iend.cur_->prev};

    // Remove the range from the source list
    it.cur_->prev->next = iend.cur_;
    iend.cur_->prev = it.cur_->prev;

    // Point the range to the destination list
    it.cur_->prev = pos.cur_->prev;
    last_hook.next = pos.cur_;

    // Point the destination list to the range
    pos.cur_->prev->next = it.cur_;
    pos.cur_->prev = &last_hook;

    return it;
  }

  // Erasure ///////////////////////////////////////////////////////////////////

  /** @brief Removes all elements from the list.
   *
   * This takes constant time. The elements are not destroyed in any way. */
  void clear() noexcept {
    hook_.next = hook_.prev = &hook_;
  }

  /** @brief Removes all elements from the list and disposes of them.
   *
   * If the disposer throws an exception, the elements removed from the list are
   * those and only those that have been passed to the disposer (this includes
   * the element passed to the throwing invocation). */
  template<std::invocable<T&> Disposer>
  void clear_and_dispose(Disposer&& disposer)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    this->erase_and_dispose(begin(), end(), std::forward<Disposer>(disposer));
  }

  /** @brief Removes from the list the element referred to by the iterator.
   * @returns An iterator to the next element after the one removed, or to the
   *          end of the list if this is the last element. */
  static iterator erase(iterator it) noexcept {
    return il::erase(it, std::next(it));
  }

  /** @brief Removes from the list the element referred to by the iterator and
   * disposes of it.
   *
   * If the disposer throws an exception, the element is removed anyway.
   *
   * @returns An iterator to the next element after the one removed, or to the
   *          end of the list if this is the last element. */
  template<std::invocable<T&> Disposer>
  static iterator erase_and_dispose(iterator it, Disposer&& disposer)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    return il::erase_and_dispose(it, std::next(it),
      std::forward<Disposer>(disposer));
  }

  /** @brief Removes from the list the given range of elements.
   *
   * This takes constant time. The elements are not destroyed in any way.
   *
   * @returns An iterator to the next element after the ones removed, or to the
   *          end of the list if the elements were at the end. */
  static iterator erase(iterator it, iterator iend) noexcept {
    it.cur_->prev->next = iend.cur_;
    iend.cur_->prev = it.cur_->prev;
    return iend;
  }

  /** @brief Removes from the list the given range of elements and disposes of
   * them.
   *
   * If the disposer throws an exception, the elements removed from the list are
   * those and only those that have been passed to the disposer (this includes
   * the element passed to the throwing invocation).
   *
   * @returns An iterator to the next element after the ones removed, or to the
   *          end of the list if the elements were at the end. */
  template<std::invocable<T&> Disposer>
  static iterator erase_and_dispose(iterator it, iterator iend,
      Disposer&& disposer) noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    if constexpr(std::is_nothrow_invocable_v<Disposer, T&>) {
      il::erase(it, iend);
      while(it != iend) {
        iterator const next_it{std::next(it)};
        std::invoke(std::forward<Disposer>(disposer), *it);
        it = next_it;
      }
    } else {
      while(it != iend) {
        iterator const next_it{il::erase(it, std::next(it))};
        std::invoke(std::forward<Disposer>(disposer), *it);
        it = next_it;
      }
    }
    return iend;
  }

  /** @brief Removes the first element from the list.
   *
   * The element is not destroyed in any way. */
  void pop_front() noexcept {
    assert(!empty());
    il::erase(begin());
  }

  /** @brief Removes the first element from the list and disposes of it.
   *
   * If the disposer throws an exception, the element is removed anyway. */
  template<std::invocable<T&> Disposer>
  void pop_front_and_dispose(Disposer&& disposer)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    assert(!empty());
    il::erase_and_dispose(begin(), std::forward<Disposer>(disposer));
  }

  /** @brief Removes the last element from the list.
   *
   * The element is not destroyed in any way. */
  void pop_back() noexcept {
    assert(!empty());
    il::erase(std::next(end(), -1));
  }

  /** @brief Removes the last element from the list and disposes of it.
   *
   * If the disposer throws an exception, the element is removed anyway. */
  template<std::invocable<T&> Disposer>
  void pop_back_and_dispose(Disposer&& disposer)
      noexcept(std::is_nothrow_invocable_v<Disposer, T&>) {
    assert(!empty());
    il::erase_and_dispose(std::next(end(), -1),
      std::forward<Disposer>(disposer));
  }
};

template<std::derived_from<intrusive_list_hook> T>
void swap(intrusive_list<T>& a, intrusive_list<T>& b) noexcept {
  a.swap(b);
}

} // util

#endif
