#ifndef UTIL_RANGE_ADAPTOR_CLOSURE_HPP_INCLUDED_G8UNV2RGSMP6CB7IVRGX7XPZX
#define UTIL_RANGE_ADAPTOR_CLOSURE_HPP_INCLUDED_G8UNV2RGSMP6CB7IVRGX7XPZX

#include "util/type_traits.hpp"
#include <concepts>
#include <functional>
#include <ranges>
#include <type_traits>
#include <utility>

namespace util {

/** @brief An implementation of `std::range_adaptor_closure` from C++23.
 *
 * If an object that is not a range type inherits from this class and implements
 * `operator()`, then appropriate `operator|` versions are added to turn this
 * type into a range adaptor. */
template<util::object T>
requires std::same_as<T, std::remove_cv_t<T>>
class range_adaptor_closure {};

template<typename T>
concept custom_range_adaptor = util::object<T>
  && std::is_base_of_v<range_adaptor_closure<T>, T>
  && !std::ranges::range<T>;

namespace detail_ {

template<util::object L, util::object R>
class range_adaptor_chain:
    public range_adaptor_closure<range_adaptor_chain<L, R>> {
private:
  L left_;
  R right_;

public:
  template<same_without_cvref<L> LL, same_without_cvref<R> RR>
  requires std::is_constructible_v<L, LL&&>
    && std::is_constructible_v<R, RR&&>
  explicit constexpr range_adaptor_chain(LL&& left, RR&& right)
    noexcept(std::is_nothrow_constructible_v<L, LL&&>
    && std::is_nothrow_constructible_v<R, RR&&>):
    left_{std::forward<LL>(left)}, right_{std::forward<RR>(right)} {}

  template<std::ranges::range Range>
  constexpr decltype(auto) operator()(Range&& range) &
      noexcept(noexcept(right_(left_(std::forward<Range>(range)))))
      requires requires { right_(left_(std::forward<Range>(range))); } {
    return right_(left_(std::forward<Range>(range)));
  }

  template<std::ranges::range Range>
  constexpr decltype(auto) operator()(Range&& range) const&
      noexcept(noexcept(right_(left_(std::forward<Range>(range)))))
      requires requires { right_(left_(std::forward<Range>(range))); } {
    return right_(left_(std::forward<Range>(range)));
  }

  template<std::ranges::range Range>
  constexpr decltype(auto) operator()(Range&& range) &&
      noexcept(noexcept(
        std::move(right_)(std::move(left_)(std::forward<Range>(range)))))
      requires requires {
        std::move(right_)(std::move(left_)(std::forward<Range>(range))); } {
    return std::move(right_)(std::move(left_)(std::forward<Range>(range)));
  }

  template<std::ranges::range Range>
  constexpr decltype(auto) operator()(Range&& range) const&&
      noexcept(noexcept(
        std::move(right_)(std::move(left_)(std::forward<Range>(range)))))
      requires requires {
        std::move(right_)(std::move(left_)(std::forward<Range>(range))); } {
    return std::move(right_)(std::move(left_)(std::forward<Range>(range)));
  }
};

} // detail_

template<std::ranges::range R, typename A>
requires custom_range_adaptor<std::remove_cvref_t<A>>
  && requires(R&& range, A&& adaptor) { adaptor(range); }
constexpr decltype(auto) operator|(R&& range, A&& adaptor)
    noexcept(noexcept(std::forward<A>(adaptor)(std::forward<R>(range)))) {
  return std::forward<A>(adaptor)(std::forward<R>(range));
}

template<typename L, typename R>
requires (custom_range_adaptor<std::remove_cvref_t<L>>
  || (!std::ranges::range<L> && custom_range_adaptor<std::remove_cvref_t<R>>))
  && std::is_constructible_v<L, L&&>
  && std::is_constructible_v<R, R&&>
constexpr auto operator|(L&& left, R&& right)
    noexcept(std::is_nothrow_constructible_v<L, L&&>
    && std::is_nothrow_constructible_v<R, R&&>) {
  return detail_::range_adaptor_chain<std::remove_cvref_t<L>,
    std::remove_cvref_t<R>>{std::forward<L>(left), std::forward<R>(right)};
}

} // util

#endif
