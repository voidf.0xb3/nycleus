#ifndef UTIL_TYPE_TRAITS_HPP_INCLUDED_N5ETD4O205VU0AFGQU8VRNNRF
#define UTIL_TYPE_TRAITS_HPP_INCLUDED_N5ETD4O205VU0AFGQU8VRNNRF

#include <concepts>
#include <cstddef>
#include <iterator>
#include <memory>
#include <tuple>
#include <type_traits>
#include <utility>

namespace util {

/** @brief Conditionally marks the given type as const based on a condition. */
template<bool Const, typename T>
using maybe_const = std::conditional_t<Const, T const, T>;

/** @brief A type trait to test if the given type is a specialization of the
 * given template with no non-type parameters. */
template<typename T, template<typename...> typename Template>
struct is_specialization: std::false_type {};
template<template<typename...> typename Template, typename... Args>
struct is_specialization<Template<Args...>, Template>: std::true_type {};
template<typename T, template<typename...> typename Template>
constexpr bool is_specialization_v{is_specialization<T, Template>::value};

/** @brief A concept to test if the given type is a specialization of the
 * given template with no non-type parameters. */
template<typename T, template<typename...> typename Template>
concept specialization_of = is_specialization_v<T, Template>;

/** @brief A helper type trait that combines multiple std::same_as uses into
 * one. */
template<typename T, typename... Us>
struct is_one_of: std::bool_constant<(... || std::same_as<T, Us>)> {};
template<typename T, typename... Us>
constexpr bool is_one_of_v{is_one_of<T, Us...>::value};

/** @brief A helper concept that combines multiple std::same_as uses into one.
 */
template<typename T, typename... Us>
concept one_of = is_one_of_v<T, Us...>;

/** @brief A type trait that checks whether all the given types are the same.
 *
 * If the pack is empty, produces true (vacuously). */
template<typename... Ts>
struct all_same: std::true_type {};
template<typename T, typename... Ts>
struct all_same<T, Ts...>: std::conjunction<std::is_same<T, Ts>...> {};
template<typename... Ts>
constexpr bool all_same_v{all_same<Ts...>::value};

/** @brief A type trait that checks whether the given type is a function
 * pointer. */
template<typename T>
struct is_function_pointer: std::bool_constant<std::is_pointer_v<T>
  && std::is_function_v<std::remove_pointer_t<T>>> {};
template<typename T>
constexpr bool is_function_pointer_v{is_function_pointer<T>::value};

/** @brief A concept that checks whether the given type is a function pointer.
 */
template<typename T>
concept function_pointer = is_function_pointer_v<T>;

/** @brief The standard library std::is_object trait, but as a concept. */
template<typename T>
concept object = std::is_object_v<T>;

/** @brief A concept for checking that the type T, after removing cvref, is the
 * same as the type U. */
template<typename T, typename U>
concept same_without_cvref = std::same_as<std::remove_cvref_t<T>, U>;

/** @brief Given a type that is a specialization of some template that has no
 * non-type template parameters, check if all of its template parameters satisfy
 * the given predicate.
 *
 * If T is not a specialization of such a template, produces false.
 *
 * The predicate must be publicly and unambiguously derived from either
 * std::true_type or std::false_type for any value of its template parameter.
 *
 * Example:
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * static_assert(all_are_v<std::variant<int, long>, std::is_integral>);
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
template<typename T, template<typename> typename Predicate>
struct all_are: std::false_type {};
template<template<typename...> typename Template, typename... Ts,
  template<typename> typename Predicate>
struct all_are<Template<Ts...>, Predicate>:
  std::conjunction<Predicate<Ts>...> {};
template<typename T, template<typename> typename Predicate>
constexpr bool all_are_v{all_are<T, Predicate>::value};

/** @brief Given a type that is a specialization of some template that has no
 * non-type template parameters, check if some of its template parameters
 * satisfy the given predicate.
 *
 * If T is not a specialization of such a template, produces false.
 *
 * The predicate must be publicly and unambiguously derived from either
 * std::true_type or std::false_type for any value of its template parameter.
 *
 * Example:
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * static_assert(some_are_v<std::variant<int, std::string>, std::is_integral>);
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
template<typename T, template<typename> typename Predicate>
struct some_are: std::false_type {};
template<template<typename...> typename Template, typename... Ts,
  template<typename> typename Predicate>
struct some_are<Template<Ts...>, Predicate>:
  std::disjunction<Predicate<Ts>...> {};
template<typename T, template<typename> typename Predicate>
constexpr bool some_are_v{some_are<T, Predicate>::value};

/** @brief Given a type that is a specialization of some template that has no
 * non-type template parameters, find the parameter that is equal to the given
 * one and return its index.
 *
 * There must be exactly one parameter equal to the given alternative.
 *
 * Example:
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * static_assert(variant_index_v<std::variant<std::string, int>, int> == 1);
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
template<typename T, typename Alternative>
struct variant_index {};
template<typename T, typename Alternative>
constexpr std::size_t variant_index_v{variant_index<T, Alternative>::value};
template<template<typename...> typename Template, typename... Ts,
  typename Alternative>
struct variant_index<Template<Alternative, Ts...>, Alternative>:
  std::integral_constant<std::size_t, 0> {};
template<template<typename...> typename Template, typename T, typename... Ts,
  typename Alternative>
requires (!std::same_as<T, Alternative>)
struct variant_index<Template<T, Ts...>, Alternative>:
  std::integral_constant<std::size_t,
  1 + variant_index_v<Template<Ts...>, Alternative>> {};

/** @brief A metafunction that concatenates tuples. */
template<typename... Tuples>
struct tuple_concat;
template<typename... Tuples>
using tuple_concat_t = tuple_concat<Tuples...>::type;
template<>
struct tuple_concat<> {
  using type = std::tuple<>;
};
template<typename... Ts>
struct tuple_concat<std::tuple<Ts...>> {
  using type = std::tuple<Ts...>;
};
template<typename... Ts, typename... Us>
struct tuple_concat<std::tuple<Ts...>, std::tuple<Us...>> {
  using type = std::tuple<Ts..., Us...>;
};
template<typename T0, typename T1, typename T2, typename... Ts>
struct tuple_concat<T0, T1, T2, Ts...>:
  tuple_concat<tuple_concat_t<T0, T1>, T2, Ts...> {};

/** @brief A metafunction that concatenates integer sequences.
 *
 * There must be at least one sequence in order to determine the integer type.
 */
template<typename Seq, typename... Seqs>
struct int_seq_concat;
template<typename Seq, typename... Seqs>
using int_seq_concat_t = int_seq_concat<Seq, Seqs...>::type;
template<typename T, T... Ints>
struct int_seq_concat<std::integer_sequence<T, Ints...>> {
  using type = std::integer_sequence<T, Ints...>;
};
template<typename T, T... Ints0, T... Ints1>
struct int_seq_concat<
  std::integer_sequence<T, Ints0...>,
  std::integer_sequence<T, Ints1...>
> {
  using type = std::integer_sequence<T, Ints0..., Ints1...>;
};
template<typename T0, typename T1, typename T2, typename... Ts>
struct int_seq_concat<T0, T1, T2, Ts...>:
  int_seq_concat<int_seq_concat_t<T0, T1>, T2, Ts...> {};

/** @brief A metafunction to check if comparing objects of the given type for
 * equality is noexcept. */
template<typename T>
struct is_nothrow_equality_comparable: std::bool_constant<
  noexcept(std::declval<T>() == std::declval<T>())
  && noexcept(std::declval<T>() != std::declval<T>())
> {};
template<typename T>
constexpr bool is_nothrow_equality_comparable_v{
  is_nothrow_equality_comparable<T>::value};

/** @brief A metafunction to check if comparing objects of the two given types
 * for equality is noexcept. */
template<typename T, typename U>
struct is_nothrow_equality_comparable_with: std::bool_constant<
  is_nothrow_equality_comparable_v<T>
  && is_nothrow_equality_comparable_v<U>
  && noexcept(std::declval<T>() == std::declval<U>())
  && noexcept(std::declval<T>() != std::declval<U>())
  && noexcept(std::declval<U>() == std::declval<T>())
  && noexcept(std::declval<U>() != std::declval<T>())
> {};
template<typename T, typename U>
constexpr bool is_nothrow_equality_comparable_with_v{
  is_nothrow_equality_comparable_with<T, U>::value};

// Allocator-related traits ////////////////////////////////////////////////////

/** @brief A type trait to test if a type is constructible with the given
 * arguments if the allocator is std::allocator.
 *
 * Unfortunately, due to the deficiencies of C++ design, it is impossible to
 * test whether a particular element type is insertible using a custom
 * allocator, because std::allocator_traits isn't SFINAE-friendly (and cannot be
 * required to be, as constructors might not be SFINAE-friendly). However,
 * testing for insertibility is useful. Therefore, in the most common case when
 * the allocator is std::allocator, this delegates to testing for
 * constructibility, but for custom allocators we have no choice but to always
 * return true. */
template<typename Allocator, typename T, typename... Args>
struct is_maybe_insertible: std::true_type {};
template<typename U, typename T, typename... Args>
struct is_maybe_insertible<std::allocator<U>, T, Args...>:
  std::is_constructible<T, Args...> {};
template<typename Allocator, typename T, typename... Args>
constexpr bool is_maybe_insertible_v{
  is_maybe_insertible<Allocator, T, Args...>::value};

/** @brief A type trait to test if a type is default-constructible if the
 * allocator is std::allocator. */
template<typename Allocator, typename T>
struct is_maybe_default_insertible: is_maybe_insertible<Allocator, T> {};
template<typename Allocator, typename T>
constexpr bool is_maybe_default_insertible_v{
  is_maybe_default_insertible<Allocator, T>::value};

/** @brief A type trait to test if a type is copy-constructible if the
 * allocator is std::allocator. */
template<typename Allocator, typename T>
struct is_maybe_copy_insertible: is_maybe_insertible<Allocator, T, T const&> {};
template<typename Allocator, typename T>
constexpr bool is_maybe_copy_insertible_v{
  is_maybe_copy_insertible<Allocator, T>::value};

/** @brief A type trait to test if a type is move-constructible if the
 * allocator is std::allocator. */
template<typename Allocator, typename T>
struct is_maybe_move_insertible: is_maybe_insertible<Allocator, T, T&&> {};
template<typename Allocator, typename T>
constexpr bool is_maybe_move_insertible_v{
  is_maybe_move_insertible<Allocator, T>::value};

/** @brief A type trait to test if a type can be constructed with the given
 * arguments without throwing exceptions if the allocator is std::allocator.
 *
 * Unfortunately, due to the deficiencies of C++ design, it is impossible to
 * test whether a particular element type is insertible without throwing
 * exceptions, since there's no obligation for allocator_traits functions to not
 * throw. However, testing for non-throwing insertibility is useful. Therefore,
 * in the most common case when the allocator is std::allocator, this delegates
 * to testing for non-throwing constructibility, but for custom allocators we
 * have no choice but to always return true. */
template<typename Allocator, typename T, typename... Args>
struct is_maybe_nothrow_insertible: std::true_type {};
template<typename U, typename T, typename... Args>
struct is_maybe_nothrow_insertible<std::allocator<U>, T, Args...>:
  std::is_nothrow_constructible<T, Args...> {};
template<typename Allocator, typename T, typename... Args>
constexpr bool is_maybe_nothrow_insertible_v{
  is_maybe_nothrow_insertible<Allocator, T, Args...>::value};

/** @brief A type trait to test if a type can be default-constructed without
 * throwing exceptions if the allocator is std::allocator. */
template<typename Allocator, typename T>
struct is_maybe_nothrow_default_insertible:
  is_maybe_nothrow_insertible<Allocator, T> {};
template<typename Allocator, typename T>
constexpr bool is_maybe_nothrow_default_insertible_v{
  is_maybe_nothrow_default_insertible<Allocator, T>::value};

/** @brief A type trait to test if a type can be copy-constructed without
 * throwing exceptions if the allocator is std::allocator. */
template<typename Allocator, typename T>
struct is_maybe_nothrow_copy_insertible:
  is_maybe_nothrow_insertible<Allocator, T, T const&> {};
template<typename Allocator, typename T>
constexpr bool is_maybe_nothrow_copy_insertible_v{
  is_maybe_nothrow_copy_insertible<Allocator, T>::value};

/** @brief A type trait to test if a type can be move-constructed without
 * throwing exceptions if the allocator is std::allocator. */
template<typename Allocator, typename T>
struct is_maybe_nothrow_move_insertible:
  is_maybe_nothrow_insertible<Allocator, T, T&&> {};
template<typename Allocator, typename T>
constexpr bool is_maybe_nothrow_move_insertible_v{
  is_maybe_nothrow_move_insertible<Allocator, T>::value};

/** @brief A type trait to test if a type can be destroyed without throwing
 * exceptions if the allocator is std::allocator. */
template<typename Allocator, typename T>
struct is_maybe_nothrow_eraseable: std::true_type {};
template<typename U, typename T>
struct is_maybe_nothrow_eraseable<std::allocator<U>, T>:
  std::is_nothrow_destructible<T> {};
template<typename Allocator, typename T>
constexpr bool is_maybe_nothrow_eraseable_v{
  is_maybe_nothrow_eraseable<Allocator, T>::value};

} // util

#endif
