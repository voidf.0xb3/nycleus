#ifndef UTIL_SAFE_INT_HPP_INCLUDED_C56MXUOE5HN1V4U1PBLL2HL3P
#define UTIL_SAFE_INT_HPP_INCLUDED_C56MXUOE5HN1V4U1PBLL2HL3P

#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <gsl/gsl>
#include <cassert>
#include <compare>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <optional>
#include <ostream>
#include <stdexcept>
#include <type_traits>
#include <utility>

#ifdef __cpp_lib_saturation_arithmetic
  #include <numeric>
#endif

namespace util {

/** @brief A concept for an integral type that is actually an integer type (and not a Boolean
 * or character type). */
template<typename T>
concept true_integer = std::integral<T>
  && !util::one_of<T, bool, char, wchar_t, char8_t, char16_t, char32_t>;

/** @brief A concept for a true integer type that is unsigned. */
template<typename T>
concept true_unsigned_integer = true_integer<T> && std::unsigned_integral<T>;

/** @brief A concept for a true integer type that is signed. */
template<typename T>
concept true_signed_integer = true_integer<T> && std::signed_integral<T>;

/** @brief A class wrapping an integer value and providing a safer API.
 *
 * The operations do not experience integer promotion. They do not allow overflow and check for
 * its absence using assertions, with alternative APIs for handling overflow differently.
 * Only lossless conversions can be done implicitly; lossy conversions have various options for
 * handling overflow. */
template<true_integer T>
class safe_int;

/** @brief A concept for types that are specializations of safe_int. */
template<typename T>
concept safe_integer = util::specialization_of<T, safe_int>;

/** @brief A concept for specializations of safe_int based on unsigned integer types. */
template<typename T>
concept safe_unsigned_integer = safe_integer<T>
  && std::unsigned_integral<typename T::base_type>;

/** @brief A concept for specializations of safe_int based on signed integer types. */
template<typename T>
concept safe_signed_integer = safe_integer<T>
  && std::signed_integral<typename T::base_type>;

using uint8_t = safe_int<std::uint8_t>;
using uint16_t = safe_int<std::uint16_t>;
using uint32_t = safe_int<std::uint32_t>;
using uint64_t = safe_int<std::uint64_t>;
using uintmax_t = safe_int<std::uintmax_t>;
using uintptr_t = safe_int<std::uintptr_t>;
using int8_t = safe_int<std::int8_t>;
using int16_t = safe_int<std::int16_t>;
using int32_t = safe_int<std::int32_t>;
using int64_t = safe_int<std::int64_t>;
using intmax_t = safe_int<std::intmax_t>;
using intptr_t = safe_int<std::intptr_t>;

using size_t = safe_int<std::size_t>;
using ptrdiff_t = safe_int<std::ptrdiff_t>;

template<true_integer T>
class safe_int {
private:
  T value_;

  /** @brief A tag type for the private constructors. */
  struct exact_init {};

  /** @brief Constructs a value wrapping the given raw integer. */
  constexpr explicit safe_int(exact_init, std::same_as<T> auto value) noexcept: value_{value} {
    static_assert(sizeof(safe_int) == sizeof(T));
  }

  /** @brief Constructs an uninitialized value. */
  constexpr explicit safe_int(exact_init) noexcept {}

public:
  using base_type = T;

  /** @brief Implicit conversion from a raw integer with a value known at compile time.
   *
   * The value can be of any integer type, and it is checked at compile time that the value
   * fits in the type. For creating instances from values not known at compile time, use the
   * wrap function. */
  consteval safe_int(true_integer auto value) noexcept: value_{static_cast<T>(value)} {
    static_assert(sizeof(safe_int) == sizeof(T));
    if(!std::in_range<T>(value)) {
      // Trigger a compile-time error
      unreachable();
    }
  }

  /** @brief Creates a safe integer from a raw integer not known at compile time.
   *
   * The input must be of the exact type being wrapped. Implicit conversions from other types
   * are disallowed. */
  static constexpr safe_int wrap(std::same_as<T> auto value) noexcept {
    return safe_int{exact_init{}, value};
  }

  /** @brief Wraps an uninitialized value.
   *
   * Reading an uninitialized value is undefined behavior. Before the object can be used, it
   * needs to be assigned a non-uninitialized value. */
  [[nodiscard]] static safe_int uninitialized() noexcept {
    return safe_int{exact_init{}};
  }

  /** @brief Returns the underlying raw integer value. */
  constexpr T unwrap() const noexcept { return value_; }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Addition

  /** @brief Adds the given number to this one, returing nothing in case of overflow. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_add(safe_int other) const noexcept
  requires std::unsigned_integral<T>
  {
    if(std::numeric_limits<T>::max() - unwrap() < other.unwrap()) {
      return std::nullopt;
    } else {
      return wrap(static_cast<T>(unwrap() + other.unwrap()));
    }
  }

  /** @brief Adds the given number to this one, returing nothing in case of overflow. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_add(safe_int other) const noexcept
  requires std::signed_integral<T>
  {
    if(
      other > 0 && std::numeric_limits<T>::max() - other.unwrap() < unwrap()
      || other < 0 && std::numeric_limits<T>::min() - other.unwrap() > unwrap()
    ) {
      return std::nullopt;
    } else {
      return wrap(static_cast<T>(unwrap() + other.unwrap()));
    }
  }

  /** @brief Adds two numbers without overflow.
   *
   * If there is overflow, the behavior is undefined; this is checked via an assertion. */
  [[nodiscard]] friend constexpr safe_int operator+(safe_int a, safe_int b) noexcept {
    auto const result{a.checked_add(b)};
    assert(result);
    return *result;
  }

  /** @brief Adds the given number to this one, wrapping on overflow.
   *
   * This works for both signed and unsigned numbers. */
  [[nodiscard]] constexpr safe_int wrapping_add(safe_int other) const noexcept {
    return wrap(static_cast<T>(static_cast<std::make_unsigned_t<T>>(unwrap())
      + static_cast<std::make_unsigned_t<T>>(other.unwrap())));
  }

  /** @brief Adds the given number to this one, throwing an exception on overflow.
   * @throws std::overflow_error
   * @throws std::bad_alloc if there is no memory for the exception object. */
  [[nodiscard]] constexpr safe_int throwing_add(safe_int other) const {
    if(auto const result{this->checked_add(other)}) {
      return *result;
    }
    throw std::overflow_error{"util::safe_int::throwing_add"};
  }

  #ifdef __cpp_lib_saturation_arithmetic
    /** @brief Adds the given number to this one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_add(safe_int other) const noexcept {
      return std::add_sat<T>(unwrap(), other.unwrap());
    }
  #else
    /** @brief Adds the given number to this one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_add(safe_int other) const noexcept
    requires std::unsigned_integral<T>
    {
      if(auto const result{this->checked_add(other)}) {
        return *result;
      }
      return wrap(std::numeric_limits<T>::max());
    }

    /** @brief Adds the given number to this one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_add(safe_int other) const noexcept
    requires std::signed_integral<T>
    {
      if(auto const result{this->checked_add(other)}) {
        return *result;
      }
      // Overflow can only happen if both operands have the same sign, and then the sign
      // determines the direction of overflow.
      return wrap(other > 0 ? std::numeric_limits<T>::max() : std::numeric_limits<T>::min());
    }
  #endif

  /** @brief Adds the given number to this one, as if by the `+` operator, and assigns the
   * result to this number. */
  constexpr safe_int& operator+=(safe_int other) noexcept {
    return *this = *this + other;
  }

  /** @brief Adds 1 to the number, as if by the `+` operator.
   * @returns A reference to this number. */
  constexpr safe_int& operator++() noexcept {
    return *this += 1;
  }

  /** @brief Adds 1 to the number, as if by the `+` operator.
   * @returns The old value of the number. */
  constexpr safe_int operator++(int) noexcept {
    safe_int const result{*this};
    ++*this;
    return result;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Subtraction

  /** @brief A difference type to make the value `weakly_incrementable`. */
  using difference_type = std::make_signed_t<T>;

  /** @brief Subtracts the given number from this one, returing nothing in case of overflow. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_sub(safe_int other) const noexcept
  requires std::unsigned_integral<T>
  {
    if(unwrap() < other.unwrap()) {
      return std::nullopt;
    } else {
      return wrap(static_cast<T>(unwrap() - other.unwrap()));
    }
  }

  /** @brief Subtracts the given number from this one, returing nothing in case of overflow. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_sub(safe_int other) const noexcept
  requires std::signed_integral<T>
  {
    if(
      other > 0 && std::numeric_limits<T>::min() + other.unwrap() > unwrap()
      || other < 0 && std::numeric_limits<T>::max() + other.unwrap() < unwrap()
    ) {
      return std::nullopt;
    } else {
      return wrap(static_cast<T>(unwrap() - other.unwrap()));
    }
  }

  /** @brief Subtracts two numbers without overflow.
   *
   * If there is overflow, the behavior is undefined; this is checked via an assertion. */
  [[nodiscard]] friend constexpr safe_int operator-(safe_int a, safe_int b) noexcept {
    auto const result{a.checked_sub(b)};
    assert(result);
    return *result;
  }

  /** @brief Subtracts the given number from this one, wrapping on overflow.
   *
   * This works for both signed and unsigned numbers. */
  [[nodiscard]] constexpr safe_int wrapping_sub(safe_int other) const noexcept {
    return wrap(static_cast<T>(static_cast<std::make_unsigned_t<T>>(unwrap())
      - static_cast<std::make_unsigned_t<T>>(other.unwrap())));
  }

  /** @brief Subtracts the given number from this one, throwing an exception on overflow.
   * @throws std::overflow_error
   * @throws std::bad_alloc If there is no memory for the exception object. */
  [[nodiscard]] constexpr safe_int throwing_sub(safe_int other) const {
    if(auto const result{this->checked_sub(other)}) {
      return *result;
    }
    throw std::overflow_error{"util::safe_int::throwing_sub"};
  }

  #ifdef __cpp_lib_saturation_arithmetic
    [[nodiscard]] constexpr safe_int saturating_sub(safe_int other) const noexcept {
      return std::sub_sat<T>(unwrap(), other.unwrap());
    }
  #else
    /** @brief Subtracts the given number from this one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_sub(safe_int other) const noexcept
    requires std::unsigned_integral<T>
    {
      if(auto const result{this->checked_sub(other)}) {
        return *result;
      }
      return 0;
    }

    /** @brief Subtracts the given number from this one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_sub(safe_int other) const noexcept
    requires std::signed_integral<T>
    {
      if(auto const result{this->checked_sub(other)}) {
        return *result;
      }
      // Overflow can only happen if both operands have different signs, and then the signs
      // determine the direction of overflow.
      return wrap(other > 0 ? std::numeric_limits<T>::min() : std::numeric_limits<T>::max());
    }
  #endif

  /** @brief Subtracts the given number from this one, as if by the `-` operator, and assigns
   * the result to this number. */
  constexpr safe_int& operator-=(safe_int other) noexcept {
    return *this = *this - other;
  }

  /** @brief Subtracts 1 from the number, as if by the `-` operator.
   * @returns A reference to this number. */
  constexpr safe_int& operator--() noexcept {
    return *this -= 1;
  }

  /** @brief Subtracts 1 from the number, as if by the `-` operator.
   * @returns The old value of the number. */
  constexpr safe_int operator--(int) const noexcept {
    safe_int const result{*this};
    --*this;
    return result;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Multiplication

  /** @brief Multiplies this number by the given one, returing nothing in case of overflow. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_mul(safe_int other) const noexcept
  requires std::unsigned_integral<T>
  {
    // Dividing the maximum by *this gives the coefficient of the greatest multiple of *this
    // that does not exceed the maximum. This coefficient is then the maximum allowed value of
    // other (else the resulting multiple of *this exceeds the maximum).
    if(unwrap() != 0 && std::numeric_limits<T>::max() / unwrap() < other.unwrap()) {
      return std::nullopt;
    } else {
      return wrap(static_cast<T>(unwrap() * other.unwrap()));
    }
  }

  /** @brief Multiplies this number by the given one, returing nothing in case of overflow. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_mul(safe_int other) const noexcept
  requires std::signed_integral<T>
  {
    if(
         *this > 0 && other > 0 && std::numeric_limits<T>::max() / unwrap() < other.unwrap()
      || *this > 0 && other < 0 && std::numeric_limits<T>::min() / unwrap() > other.unwrap()
      || *this < 0 && other > 0 && std::numeric_limits<T>::min() / unwrap() < other.unwrap()
      || *this < 0 && other < 0 && std::numeric_limits<T>::max() / unwrap() > other.unwrap()
    ) {
      return std::nullopt;
    } else {
      return wrap(static_cast<T>(unwrap() * other.unwrap()));
    }
  }

  /** @brief Multiplies two numbers without overflow.
   *
   * If there is overflow, the behavior is undefined; this is checked via an assertion. */
  [[nodiscard]] constexpr friend safe_int operator*(safe_int a, safe_int b) noexcept {
    auto const result{a.checked_mul(b)};
    assert(result);
    return *result;
  }

  /** @brief Multiplies this number by the given one, wrapping on overflow.
   *
   * This works for both signed and unsigned numbers. */
  [[nodiscard]] constexpr safe_int wrapping_mul(safe_int other) const noexcept {
    return wrap(static_cast<T>(static_cast<std::make_unsigned_t<T>>(unwrap())
      * static_cast<std::make_unsigned_t<T>>(other.unwrap())));
  }

  /** @brief Multiplies this number by the given one, throwing an exception on overflow.
   * @throws std::overflow_error
   * @throws std::bad_alloc if there is no memory for the exception object. */
  [[nodiscard]] constexpr safe_int throwing_mul(safe_int other) const {
    if(auto const result{this->checked_mul(other)}) {
      return *result;
    }
    throw std::overflow_error{"util::safe_int::throwing_mul"};
  }

  #ifdef __cpp_lib_saturation_arithmetic
    /** @brief Multiplies this number by the given one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_mul(safe_int other) const noexcept {
      return std::mul_sat<T>(unwrap(), other.unwrap());
    }
  #else
    /** @brief Multiplies this number by the given one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_mul(safe_int other) const noexcept
    requires std::unsigned_integral<T>
    {
      if(auto const result{this->checked_mul(other)}) {
        return *result;
      }
      return wrap(std::numeric_limits<T>::max());
    }

    /** @brief Multiplies this number by the given one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_mul(safe_int other) const noexcept
    requires std::signed_integral<T>
    {
      if(auto const result{this->checked_mul(other)}) {
        return *result;
      }
      return wrap((*this > 0) == (other > 0) ? std::numeric_limits<T>::max()
        : std::numeric_limits<T>::min());
    }
  #endif

  /** @brief Multiplies this number by the given one, as if by the `*` operator, and assigns
   * the result to this number. */
  constexpr safe_int& operator*=(safe_int other) noexcept {
    return *this = *this * other;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Division

  /** @brief Divides this number by the given one.
   *
   * Unsigned division can never overflow, so this will never return an empty value. But this
   * function still exists to match signed API. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_div(safe_int other) const noexcept
  requires std::unsigned_integral<T>
  {
    assert(other != 0);
    return wrap(static_cast<T>(unwrap() / other.unwrap()));
  }

  /** @brief Divides this number by the given one, returing nothing in case of overflow.
   *
   * Overflow only happens when the smallest (most negative) number is divided by -1. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_div(safe_int other) const noexcept
  requires std::signed_integral<T>
  {
    assert(other != 0);
    if(*this == std::numeric_limits<T>::min() && other == -1) {
      return std::nullopt;
    } else {
      return wrap(static_cast<T>(unwrap() / other.unwrap()));
    }
  }

  /** @brief Divides two numbers without overflow.
   *
   * If there is overflow, the behavior is undefined; this is checked via an assertion.
   *
   * Overflow only happens with signed integers, when the smallest (most negative) number is
   * divided by -1. */
  [[nodiscard]] friend constexpr safe_int operator/(safe_int a, safe_int b) noexcept {
    assert(b != 0);
    auto const result{a.checked_div(b)};
    assert(result);
    return *result;
  }

  /** @brief Divides this number by the given one.
   *
   * Unsigned division can never overflow, so this is exactly equivalent to all other unsigned
   * division functions. But this function still exists to match signed API. */
  [[nodiscard]] constexpr safe_int wrapping_div(safe_int other) const noexcept
  requires std::unsigned_integral<T>
  {
    assert(other != 0);
    return *this / other;
  }

  /** @brief Divides this number by the given one, wrapping on overflow.
   *
   * Overflow only happens when the smallest (most negative) number is divided by -1.
   * Mathematically, this produces the value that is one greater than the greatest (most
   * positive) number; wrapping, in this case, means returning the smallest (most negative)
   * number. */
  [[nodiscard]] constexpr safe_int wrapping_div(safe_int other) const noexcept
  requires std::signed_integral<T>
  {
    assert(other != 0);
    if(*this == std::numeric_limits<T>::min() && other == -1) {
      return wrap(std::numeric_limits<T>::min());
    } else {
      return wrap(static_cast<T>(unwrap() / other.unwrap()));
    }
  }

  /** @brief Divides this number by the given one.
   *
   * Unsigned division can never overflow, so this won't ever actually throw an exception. But
   * this function still exists to match signed API. */
  [[nodiscard]] constexpr safe_int throwing_div(safe_int other) const noexcept
  requires std::unsigned_integral<T>
  {
    assert(other != 0);
    return *this / other;
  }

  /** @brief Divides this number by the given one, throwing an exception on overflow.
   *
   * Overflow only happens when the smallest (most negative) number is divided by -1.
   *
   * @throws std::overflow_error
   * @throws std::bad_alloc if there is no memory for the exception object. */
  [[nodiscard]] constexpr safe_int throwing_div(safe_int other) const
  requires std::signed_integral<T>
  {
    assert(other != 0);
    if(auto const result{this->checked_div(other)}) {
      return *result;
    }
    throw std::overflow_error{"util::safe_int::throwing_div"};
  }

  #ifdef __cpp_lib_saturation_arithmetic
    /** @brief Divides this number by the given one, saturating on overflow. */
    [[nodiscard]] constexpr safe_int saturating_div(safe_int other) const noexcept {
      assert(other != 0);
      return std::div_sat<T>(unwrap(), other.unwrap());
    }
  #else
    /** @brief Divides this number by the given one.
     *
     * Unsigned division can never overflow, so this is exactly equivalent to all other
     * unsigned division functions. But this function still exists to match signed API. */
    [[nodiscard]] constexpr safe_int saturating_div(safe_int other) const noexcept
    requires std::unsigned_integral<T>
    {
      assert(other != 0);
      return *this / other;
    }

    /** @brief Divides this number by the given one, saturating on overflow.
     *
     * Overflow only happens when the smallest (most negative) number is divided by -1.
     * Mathematically, this produces the value that is one greater than the greatest (most
     * positive) number; saturation, in this case, means returning the greatest (most positive)
     * number (one less than the true result). */
    [[nodiscard]] constexpr safe_int saturating_div(safe_int other) const noexcept
    requires std::signed_integral<T>
    {
      assert(other != 0);
      if(*this == std::numeric_limits<T>::min() && other == -1) {
        return wrap(std::numeric_limits<T>::max());
      } else {
        return wrap(static_cast<T>(unwrap() / other.unwrap()));
      }
    }
  #endif

  /** @brief Divides this number by the given one, as if by the `/` operator, and assigns the
   * result to this number. */
  constexpr safe_int& operator/=(safe_int other) noexcept {
    assert(other != 0);
    return *this = *this / other;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Remainder

  /** @brief Computes the remainder from division of two numbers.
   *
   * The rules for signed remainders are the same as for the built-in remainder operator. */
  [[nodiscard]] friend constexpr safe_int operator%(safe_int a, safe_int b) noexcept {
    assert(b != 0);
    return wrap(static_cast<T>(a.unwrap() % b.unwrap()));
  }

  /** @brief Computes the remainder from the division of this number by the given one, as if by
   * the `%` operator, and assigns the result to this number. */
  constexpr safe_int& operator%=(safe_int other) noexcept {
    assert(other != 0);
    return *this = *this % other;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Negation

  /** @brief Changes the sign of this number, returning nothing in case of overflow.
   *
   * Overflow only happens for the smallest (most negative) number. */
  [[nodiscard]] constexpr std::optional<safe_int> checked_neg() const noexcept
  requires std::signed_integral<T>
  {
    if(*this == std::numeric_limits<T>::min()) {
      return std::nullopt;
    } else {
      return wrap(static_cast<T>(-unwrap()));
    }
  }

  /** @brief Changes the sign of this number without overflow.
   *
   * If there is overflow, the behavior is undefined; this is checked via an assertion.
   *
   * Overflow only happens for the smallest (most negative) number. */
  [[nodiscard]] constexpr safe_int operator-() const noexcept
  requires std::signed_integral<T>
  {
    auto const result{checked_neg()};
    assert(result);
    return *result;
  }

  /** @brief Changes the sign of this number, wrapping on overflow.
   *
   * Overflow only happens for the smallest (most negative) number. Wrapping, in this case,
   * means returning the original input. */
  [[nodiscard]] constexpr safe_int wrapping_neg() const noexcept
  requires std::signed_integral<T>
  {
    return wrap(static_cast<T>(~(static_cast<std::make_unsigned_t<T>>(unwrap()) - 1)));
  }

  /** @brief Changes the sign of this number, throwing an exception on overflow.
   *
   * Overflow only happens for the smallest (most negative) number.
   *
   * @throws std::overflow_error
   * @throws std::bad_alloc if there is no memory for the exception object. */
  [[nodiscard]] constexpr safe_int throwing_neg() const
  requires std::signed_integral<T>
  {
    if(auto const result{checked_neg()}) {
      return *result;
    }
    throw std::overflow_error{"util::safe_int::throwing_neg"};
  }

  /** @brief Changes the sign of the number, saturating on overflow.
   *
   * Overflow only happens for the smallest (most negative) number. Wrapping, in this case,
   * means returning the greatest (most positive) number. */
  [[nodiscard]] constexpr safe_int saturating_neg() const noexcept
  requires std::signed_integral<T>
  {
    #ifdef __cpp_lib_saturation_arithmetic
      return wrap(std::sub_sat<T>(0, unwrap()));
    #else
      if(auto const result{checked_neg()}) {
        return *result;
      }
      return wrap(std::numeric_limits<T>::max());
    #endif
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Bitwise operators

  [[nodiscard]] friend constexpr safe_int operator|(safe_int a, safe_int b) noexcept {
    return wrap(static_cast<T>(a.unwrap() | b.unwrap()));
  }

  constexpr safe_int& operator|=(safe_int other) noexcept {
    return *this = *this | other;
  }

  [[nodiscard]] friend constexpr safe_int operator&(safe_int a, safe_int b) noexcept {
    return wrap(static_cast<T>(a.unwrap() & b.unwrap()));
  }

  constexpr safe_int& operator&=(safe_int other) noexcept {
    return *this = *this & other;
  }

  [[nodiscard]] friend constexpr safe_int operator^(safe_int a, safe_int b) noexcept {
    return wrap(static_cast<T>(a.unwrap() ^ b.unwrap()));
  }

  constexpr safe_int& operator^=(safe_int other) noexcept {
    return *this = *this ^ other;
  }

  [[nodiscard]] constexpr safe_int operator<<(size_t offset) const noexcept {
    assert(offset < sizeof(T) * 8);
    return wrap(static_cast<T>(unwrap() << offset.unwrap()));
  }

  constexpr safe_int& operator<<=(size_t offset) noexcept {
    assert(offset < sizeof(T) * 8);
    return *this = *this << offset;
  }

  [[nodiscard]] constexpr safe_int operator>>(size_t offset) const noexcept {
    assert(offset < sizeof(T) * 8);
    return wrap(static_cast<T>(unwrap() >> offset.unwrap()));
  }

  constexpr safe_int& operator>>=(size_t offset) noexcept {
    assert(offset < sizeof(T) * 8);
    return *this = *this >> offset;
  }

  [[nodiscard]] constexpr safe_int operator~() const noexcept {
    return wrap(static_cast<T>(~unwrap()));
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Comparison

  [[nodiscard]] constexpr friend bool operator==(safe_int a, safe_int b) noexcept {
    return a.unwrap() == b.unwrap();
  }

  [[nodiscard]] constexpr friend std::strong_ordering operator<=>(
    safe_int a,
    safe_int b
  ) noexcept {
    return a.unwrap() <=> b.unwrap();
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Conversion

  /** @brief Lossless implicit conversion. */
  template<true_integer U>
  requires (sizeof(U) > sizeof(T)) && (std::unsigned_integral<T> || std::signed_integral<U>)
  [[nodiscard]] constexpr operator safe_int<U>() const noexcept {
    return safe_int<U>::wrap(static_cast<U>(unwrap()));
  }

  /** @brief Converts the number to the safe version of the given type, returning nothing if
   * the value does not fit. */
  template<true_integer U>
  [[nodiscard]] constexpr std::optional<safe_int<U>> checked_cast() const noexcept {
    if(std::in_range<U>(unwrap())) {
      return safe_int<U>::wrap(static_cast<U>(unwrap()));
    } else {
      return std::nullopt;
    }
  }

  /** @brief A checked_cast that names a safe_int as its destination. */
  template<safe_integer U>
  [[nodiscard]] constexpr std::optional<U> checked_cast() const noexcept {
    return this->template checked_cast<typename U::base_type>();
  }

  /** @brief Converts the number to the safe version of the given type, assuming it fits.
   *
   * If the value does not fit, the behavior is undefined; this is checked via an assertion. */
  template<true_integer U>
  [[nodiscard]] constexpr safe_int<U> cast() const noexcept {
    auto const result{this->checked_cast<U>()};
    assert(result);
    return *result;
  }

  /** @brief A cast that names a safe_int as its destination. */
  template<safe_integer U>
  [[nodiscard]] constexpr U cast() const noexcept {
    return this->template cast<typename U::base_type>();
  }

  /** @brief Converts the number to the safe version of the given type, throwing an exception
   * if it doesn't fit.
   * @throws std::overflow_error
   * @throws std::bad_alloc if there is no memory for the exception object. */
  template<true_integer U>
  [[nodiscard]] constexpr safe_int<U> throwing_cast() const
  noexcept(sizeof(U) > sizeof(T) && (std::unsigned_integral<T> || std::signed_integral<U>))
  {
    if(auto const result{this->checked_cast<U>()}) {
      return *result;
    }
    throw std::overflow_error{"util::safe_int::throwing_cast"};
  }

  /** @brief A throwing_cast that names a safe_int as its destination. */
  template<safe_integer U>
  [[nodiscard]] constexpr U throwing_cast() const
  noexcept(
    sizeof(typename U::base_type) > sizeof(T)
    && (std::unsigned_integral<T> || std::signed_integral<typename U::base_type>)
  )
  {
    return this->template throwing_cast<typename U::base_type>();
  }

  /** @brief Converts the number to the safe version of the given type, trimming or wrapping if
   * the value doesn't fit.
   *
   * This produces the same result as a built-in implicit narrowing conversion, but is invoked
   * explicitly. */
  template<true_integer U>
  [[nodiscard]] constexpr safe_int<U> lossy_cast() const noexcept {
    return safe_int<U>::wrap(gsl::narrow_cast<U>(unwrap()));
  }

  /** @brief A lossy_cast that names a safe_int as its destination. */
  template<safe_integer U>
  [[nodiscard]] constexpr U lossy_cast() const noexcept {
    return this->template lossy_cast<typename U::base_type>();
  }

  /** @brief Converts the number to the safe version of the given type, saturating if the value
   * doesn't fit. */
  template<true_integer U>
  [[nodiscard]] constexpr safe_int<U> saturating_cast() const noexcept {
    #ifdef __cpp_lib_saturation_arithmetic
      return std::saturate_cast<U>(unwrap());
    #else
      if(auto const result{this->checked_cast<U>()}) {
        return *result;
      }
      return safe_int<U>::wrap(*this > 0 ? std::numeric_limits<U>::max()
        : std::numeric_limits<U>::min());
    #endif
  }

  /** @brief A saturating_cast that names a safe_int as its destination. */
  template<safe_integer U>
  [[nodiscard]] constexpr U saturating_cast() const noexcept {
    return this->template saturating_cast<typename U::base_type>();
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // MARK: Output

  friend std::ostream& operator<<(std::ostream& os, safe_int value) noexcept {
    return os << value.unwrap();
  }
};

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Wrapping

template<true_integer T>
[[nodiscard]] constexpr safe_int<T> wrap(T value) noexcept {
  return safe_int<T>::wrap(value);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Saturating arithmetic

/** @brief An implementation of std::add_sat from C++26. */
template<true_integer T>
[[nodiscard]] constexpr T add_sat(T a, T b) noexcept {
  #ifdef __cpp_lib_saturation_arithmetic
    return std::add_sat<T>(a, b);
  #else
    return safe_int<T>::wrap(a).saturaring_add(safe_int<T>::wrap(b)).unwrap();
  #endif
}

/** @brief An implementation of std::sub_sat from C++26. */
template<true_integer T>
[[nodiscard]] constexpr T sub_sat(T a, T b) noexcept {
  #ifdef __cpp_lib_saturation_arithmetic
    return std::sub_sat<T>(a, b);
  #else
    return safe_int<T>::wrap(a).saturaring_sub(safe_int<T>::wrap(b)).unwrap();
  #endif
}

/** @brief An implementation of std::mul_sat from C++26. */
template<true_integer T>
[[nodiscard]] constexpr T mul_sat(T a, T b) noexcept {
  #ifdef __cpp_lib_saturation_arithmetic
    return std::mul_sat<T>(a, b);
  #else
    return safe_int<T>::wrap(a).saturaring_mul(safe_int<T>::wrap(b)).unwrap();
  #endif
}

/** @brief An implementation of std::div_sat from C++26. */
template<true_integer T>
[[nodiscard]] constexpr T div_sat(T a, T b) noexcept {
  #ifdef __cpp_lib_saturation_arithmetic
    return std::div_sat<T>(a, b);
  #else
    return safe_int<T>::wrap(a).saturaring_div(safe_int<T>::wrap(b)).unwrap();
  #endif
}

/** @brief An implementation of std::saturate_cast from C++26. */
template<true_integer T, true_integer U>
[[nodiscard]] constexpr T saturate_cast(U value) noexcept {
  #ifdef __cpp_lib_saturation_arithmetic
    return std::saturate_cast<T>(value);)
  #else
    return safe_int<U>::wrap(value).template saturating_cast<T>().unwrap();
  #endif
}

} // util

#endif
