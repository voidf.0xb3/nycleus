#ifndef UTIL_NUMS_HPP_INCLUDED_12HYHP3S9YHCXAF8P8UE5MXNG
#define UTIL_NUMS_HPP_INCLUDED_12HYHP3S9YHCXAF8P8UE5MXNG

#include "util/safe_int.hpp"
#include <array>
#include <bit>
#include <cassert>
#include <concepts>
#include <cstdint>
#include <cstddef>
#include <string>
#include <type_traits>

namespace util {

/** @brief Converts a signed integer into its unsigned counterpart.
 *
 * If the number does not fit, the behavior is undefined; this is checked via an assertion. */
template<safe_signed_integer T>
[[nodiscard]] constexpr auto make_unsigned(T t) noexcept {
  return t.template cast<std::make_unsigned_t<typename T::base_type>>();
}

/** @brief Converts a signed integer into its unsigned counterpart.
 *
 * If the number does not fit, the behavior is undefined; this is checked via an assertion. */
template<true_signed_integer T>
[[nodiscard]] constexpr auto make_unsigned(T t) noexcept {
  return util::make_unsigned(safe_int<T>::wrap(t)).unwrap();
}

/** @brief A utility to extract a byte at a given index from an integer.
 *
 * The byte indexes are zero-based and little-endian. */
template<safe_integer T>
[[nodiscard]] constexpr uint8_t nth_byte(size_t b, T x) noexcept {
  assert(b < sizeof(x));
  auto const ux{x.template lossy_cast<std::make_unsigned_t<typename T::base_type>>()};
  return (ux >> (b * 8)).template lossy_cast<uint8_t>();
}

/** @brief A utility to extract a byte at a given index from an integer.
 *
 * The byte indexes are zero-based and little-endian. */
template<true_integer T>
[[nodiscard]] constexpr std::uint8_t nth_byte(std::size_t b, T x) noexcept {
  return util::nth_byte(size_t::wrap(b), safe_int<T>::wrap(x)).unwrap();
}

namespace detail_::num_decimal_digits {

/** @brief Naively computes the number of digits in a non-zero number.
 *
 * This is never used for actually computing the number of digits. This is used just to
 * precompute the numbers of digits for powers of two to fill the guesses array. */
[[nodiscard]] consteval uint8_t naive(uintmax_t value) noexcept {
  assert(value != 0);
  uint8_t result{0};
  while(value != 0) {
    ++result;
    value /= 10;
  }
  return result;
}

/** @brief Precomputes the guesses array. */
[[nodiscard]] consteval std::array<std::uint8_t, sizeof(uintmax_t) * 8 + 1>
precompute_guesses() noexcept {
  std::array<std::uint8_t, sizeof(uintmax_t) * 8 + 1> result;
  result[0] = 1;
  uintmax_t pow2{1};

  size_t i{0};
  for(;;) {
    result[(i + 1).unwrap()] = num_decimal_digits::naive(pow2).unwrap();
    ++i;
    if(i == sizeof(uintmax_t) * 8) {
      break;
    }
    pow2 <<= 1;
  }

  return result;
}

/** @brief Initial guesses for the number of digits based on the bit width of the number.
 *
 * This array contains, at a given index <var>i</var>, the smallest possible number of decimal
 * digits in an integer with <var>i</var> significant bits. For <var>i</var> > 0, this is the
 * same as the number of decimal digits in 2<sup><var>i</var> − 1</sup>, which is the smallest
 * number with <var>i</var> significant bits. The <var>i</var> = 0 case is special: the only
 * integer with 0 significant bits is 0, and it has 1 decimal digit. */
inline constexpr auto guesses{precompute_guesses()};

/** @brief Precomputes the powers of 10 array. */
[[nodiscard]] consteval std::array<std::uintmax_t, guesses[sizeof(uintmax_t) * 8] + 1>
precompute_pow10() noexcept {
  std::array<std::uintmax_t, guesses[sizeof(uintmax_t) * 8] + 1> result;

  result[0] = 1;
  for(size_t i{1}; i <= guesses[sizeof(uintmax_t) * 8]; ++i) {
    result[i.unwrap()] = result[(i - 1).unwrap()] * 10;
  }

  return result;
}

/** @brief The powers of 10 array.
 *
 * These are all the powers of 10 that fit in the biggest integer type. Comparing numbers to
 * powers of 10 is used to determine the number of digits in the value. */
inline constexpr auto pow10{precompute_pow10()};

} // detail_::num_decimal_digits

/** @brief Determines the number of decimal digits in the given unsigned integer. */
template<safe_unsigned_integer T>
requires (sizeof(T) <= 8)
[[nodiscard]] constexpr uint8_t num_decimal_digits(T t) noexcept {
  uint8_t const guess{uint8_t::wrap(
    detail_::num_decimal_digits::guesses[std::bit_width(t.unwrap())])};
  return t >= uintmax_t::wrap(detail_::num_decimal_digits::pow10[guess.unwrap()])
    ? guess + 1 : guess;
}

/** @brief Determines the number of decimal digits in the given unsigned integer. */
template<true_unsigned_integer T>
requires (sizeof(T) <= 8)
[[nodiscard]] constexpr std::uint8_t num_decimal_digits(T t) noexcept {
  return util::num_decimal_digits(safe_int<T>::wrap(t)).unwrap();
}

/** @brief Determines the number of decimal digits in the given signed integer.
 *
 * If the integer is negative, the minus sign is counted as another digit. */
template<safe_signed_integer T>
requires (sizeof(T) <= 8)
[[nodiscard]] constexpr uint8_t num_decimal_digits(T t) noexcept {
  if(t < 0) {
    // Note that if `t` is negative, then `-t` is positive, unless `t` is the smallest possible
    // value. In that case, `-t == t`, and casting it to the unsigned type will still give the
    // mathematically correct result for `-t`.
    return 1 + util::num_decimal_digits(t.wrapping_neg()
      .template lossy_cast<std::make_unsigned_t<typename T::base_type>>());
  } else {
    return util::num_decimal_digits(util::make_unsigned(t));
  }
}

/** @brief Determines the number of decimal digits in the given signed integer.
 *
 * If the integer is negative, the minus sign is counted as another digit. */
template<true_signed_integer T>
requires (sizeof(T) <= 8)
[[nodiscard]] constexpr std::uint8_t num_decimal_digits(T t) noexcept {
  return util::num_decimal_digits(safe_int<T>::wrap(t)).unwrap();
}

/** @brief Returns a UTF-8 string with a textual representaton of the given number.
 * @throws std::bad_alloc
 * @throws std::length_error */
template<safe_integer T>
requires (sizeof(T) <= 8)
[[nodiscard]] std::u8string to_u8string(T t) {
  std::u8string result;
  result.resize(util::num_decimal_digits(t).unwrap());

  T value{t};
  if constexpr(safe_signed_integer<T>) {
    if(t < 0) {
      t = -t;
    }
  }

  auto it{result.end()};
  do {
    --it;
    *it = u8'0' + (value % 10).unwrap();
    value /= 10;
  } while(it != result.begin());

  if(t < 0) {
    result.front() = u8'-';
  }

  return result;
}

/** @brief Returns a UTF-8 string with a textual representaton of the given number.
 * @throws std::bad_alloc
 * @throws std::length_error */
template<true_integer T>
requires (sizeof(T) <= 8)
[[nodiscard]] std::u8string to_u8string(T t) {
  return util::to_u8string(safe_int<T>::wrap(t));
}

/** @brief Divides the two unsigned integers, rounding up the result. */
template<safe_unsigned_integer T>
[[nodiscard]] constexpr T div_ceil(T a, std::type_identity_t<T> b) noexcept {
  assert(b != 0);
  return a / b + (a % b == 0 ? T{0} : T{1});
}

/** @brief Divides the two unsigned integers, rounding up the result. */
template<true_unsigned_integer T>
[[nodiscard]] constexpr T div_ceil(T a, std::type_identity_t<T> b) noexcept {
  return util::div_ceil(safe_int<T>::wrap(a), safe_int<T>::wrap(b)).unwrap();
}

/** @brief Returns the smallest multiple of the second argument, which must be a
 * power of two, that is not less than the first argument. */
template<safe_unsigned_integer T>
[[nodiscard]] constexpr T next_multiple(T a, std::type_identity_t<T> b) noexcept {
  assert(std::has_single_bit(b.unwrap()));
  return (a + (b - 1)) & ~(b - 1);
}

/** @brief Returns the smallest multiple of the second argument, which must be a
 * power of two, that is not less than the first argument. */
template<true_unsigned_integer T>
[[nodiscard]] constexpr T next_multiple(T a, std::type_identity_t<T> b) noexcept {
  return util::next_multiple(safe_int<T>::wrap(a), safe_int<T>::wrap(b)).unwrap();
}

} // util

#endif
