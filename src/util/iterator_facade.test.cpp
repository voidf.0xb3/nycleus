#include <boost/test/unit_test.hpp>

#include "util/safe_int.hpp"
#include "util/iterator_facade.hpp"
#include <concepts>
#include <cstdint>
#include <iterator>
#include <ranges>

namespace {

class counting_iterator_core {
private:
  util::int8_t counter_{0};

public:
  counting_iterator_core() = default;
  explicit counting_iterator_core(util::int8_t counter) noexcept: counter_{counter} {}

protected:
  [[nodiscard]] util::int8_t dereference() const noexcept {
    return counter_;
  }

  void advance(std::int8_t off) noexcept {
    counter_ += util::wrap(off);
  }

  [[nodiscard]] std::int8_t distance_to(counting_iterator_core other) const noexcept {
    return (other.counter_ - counter_).unwrap();
  }

  [[nodiscard]] std::int8_t distance_to(std::default_sentinel_t) const noexcept {
    return (7 - counter_).unwrap();
  }
};

using counting_iterator = util::iterator_facade<counting_iterator_core>;

static_assert(std::random_access_iterator<counting_iterator>);
static_assert(!std::contiguous_iterator<counting_iterator>);
static_assert(std::same_as<std::iter_value_t<counting_iterator>, util::int8_t>);
static_assert(std::same_as<std::iter_reference_t<counting_iterator>, util::int8_t>);
static_assert(std::same_as<std::iter_difference_t<counting_iterator>, std::int8_t>);
static_assert(std::same_as<std::iter_rvalue_reference_t<counting_iterator>, util::int8_t>);

class sp_counting_iterator_core {
private:
  util::int8_t counter_{0};

public:
  sp_counting_iterator_core() = default;
  explicit sp_counting_iterator_core(util::int8_t counter) noexcept: counter_{counter} {}

protected:
  static constexpr bool is_single_pass{true};

  [[nodiscard]] util::int8_t dereference() const noexcept {
    return counter_;
  }

  void advance(std::int8_t off) noexcept {
    counter_ += util::wrap(off);
  }

  [[nodiscard]] std::int8_t distance_to(sp_counting_iterator_core other) const noexcept {
    return (other.counter_ - counter_).unwrap();
  }
};

using sp_counting_iterator = util::iterator_facade<sp_counting_iterator_core>;

static_assert(std::input_iterator<sp_counting_iterator>);
static_assert(!std::forward_iterator<sp_counting_iterator>);
static_assert(std::same_as<std::iter_value_t<sp_counting_iterator>, util::int8_t>);
static_assert(std::same_as<std::iter_reference_t<sp_counting_iterator>, util::int8_t>);
static_assert(std::same_as<std::iter_difference_t<sp_counting_iterator>, std::int8_t>);
static_assert(std::same_as<std::iter_rvalue_reference_t<sp_counting_iterator>, util::int8_t>);

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(iterator_facade)

BOOST_AUTO_TEST_CASE(multipass) {
  auto const result{std::ranges::subrange{counting_iterator{0}, counting_iterator{10}}};
  util::int8_t const values[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  BOOST_TEST(result == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(singlepass) {
  auto const result{std::ranges::subrange{sp_counting_iterator{0}, sp_counting_iterator{10}}};
  util::int8_t const values[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  BOOST_TEST(result == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(sentinels) {
  auto const result{std::ranges::subrange{counting_iterator{0}, std::default_sentinel}};
  util::int8_t const values[]{0, 1, 2, 3, 4, 5, 6};
  BOOST_TEST(result == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
