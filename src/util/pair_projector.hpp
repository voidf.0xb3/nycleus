#ifndef NYCLEUS_UTIL_PAIR_PROJECTOR_HPP_INCLUDED_WWLV6L7XW5ZWKISWMFS8Z3BYG
#define NYCLEUS_UTIL_PAIR_PROJECTOR_HPP_INCLUDED_WWLV6L7XW5ZWKISWMFS8Z3BYG

#include <utility>

namespace util {

struct pair_projector_first {
  template<typename T, typename U>
  [[nodiscard]] T& operator()(std::pair<T, U>& p) const noexcept {
    return p.first;
  }

  template<typename T, typename U>
  [[nodiscard]] T const& operator()(std::pair<T, U> const& p) const noexcept {
    return p.first;
  }

  template<typename T, typename U>
  [[nodiscard]] T&& operator()(std::pair<T, U>&& p) const noexcept {
    return p.first;
  }

  template<typename T, typename U>
  [[nodiscard]] T const&& operator()(std::pair<T, U> const&& p) const noexcept {
    return p.first;
  }
};

struct pair_projector_second {
  template<typename T, typename U>
  [[nodiscard]] U& operator()(std::pair<T, U>& p) const noexcept {
    return p.second;
  }

  template<typename T, typename U>
  [[nodiscard]] U const& operator()(std::pair<T, U> const& p) const noexcept {
    return p.second;
  }

  template<typename T, typename U>
  [[nodiscard]] U&& operator()(std::pair<T, U>&& p) const noexcept {
    return p.second;
  }

  template<typename T, typename U>
  [[nodiscard]] U const&& operator()(std::pair<T, U> const&& p) const noexcept {
    return p.second;
  }
};

} // util

#endif
