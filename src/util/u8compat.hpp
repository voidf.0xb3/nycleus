#ifndef UTIL_U8COMPAT_HPP_INCLUDED_S81JQV0JX2B72PMMZ5X229H1H
#define UTIL_U8COMPAT_HPP_INCLUDED_S81JQV0JX2B72PMMZ5X229H1H

#include <gsl/gsl>
#include <cstddef>
#include <exception>
#include <string>
#include <string_view>

namespace util {

using u8zstring = gsl::basic_zstring<char8_t>;
using cu8zstring = gsl::basic_zstring<char8_t const>;

/** @brief Returns a pointer to the same string, but represented as chars. */
[[nodiscard]] inline gsl::czstring as_char(cu8zstring str) noexcept {
  return reinterpret_cast<gsl::czstring>(str);
}

/** @brief Returns a pointer to the same string, but represented as chars. */
[[nodiscard]] inline gsl::zstring as_char(u8zstring str) noexcept {
  return reinterpret_cast<gsl::zstring>(str);
}

/** @brief Returns a view to the same string, but represented as chars. */
[[nodiscard]] inline std::string_view as_char(std::u8string_view view)
    noexcept {
  return {util::as_char(view.data()), view.size()};
}

/** @brief Returns a copy of the same string, but represented as chars.
 * @throws std::bad_alloc
 * @throws std::length_error */
[[nodiscard]] inline std::string as_char_s(std::u8string_view view) {
  return std::string{util::as_char(view)};
}

/** @brief Returns a copy of the same string, but represented as u8chars.
 * @throws std::bad_alloc
 * @throws std::length_error */
std::u8string as_u8char_s(std::string_view str);

namespace u8compat_literals {

/** @brief A UTF-8 string literal that produces a pointer to a null-terminated
 * string in the form of chars. */
[[nodiscard]] inline gsl::czstring operator""_ch(cu8zstring str, std::size_t)
    noexcept {
  return util::as_char(str);
}

/** @brief A UTF-8 character literal that produces a UTF-8 code unit as a char.
 */
[[nodiscard]] constexpr char operator""_ch(char8_t ch) noexcept {
  return static_cast<char>(ch);
}

/** @brief A UTF-8 string literal that produces a std::string.
 * @throws std::bad_alloc
 * @throws std::length_error */
[[nodiscard]] inline std::string operator""_chs(
  cu8zstring str,
  std::size_t size
) {
  return std::string{util::as_char(str), size};
}

/** @brief A UTF-8 string literal that produces a std::string_view. */
[[nodiscard]] inline std::string_view operator""_chsv(
  cu8zstring str,
  std::size_t size
) noexcept {
  return std::string_view{util::as_char(str), size};
}

} // u8compat_literals

} // util

#endif
