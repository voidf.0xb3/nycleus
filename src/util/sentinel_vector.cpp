#include "util/sentinel_vector.hpp"
#include <stdexcept>

util::sentinel_vector_length_error::sentinel_vector_length_error():
  std::runtime_error{"util::sentinel_vector_length_error"} {}
