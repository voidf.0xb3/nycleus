#ifndef UTIL_NOT_NULL_PTR_HPP_INCLUDED_2PAC9IXCPVQZ5ANM2VMOXJ3L9
#define UTIL_NOT_NULL_PTR_HPP_INCLUDED_2PAC9IXCPVQZ5ANM2VMOXJ3L9

#include <cassert>
#include <concepts>
#include <cstddef>
#include <memory>

namespace util {

/** @brief A wrapper for a pointer that asserts that the pointer is not null. */
template<typename T>
class non_null_ptr {
private:
  T* ptr_;

public:
  constexpr explicit non_null_ptr(T* ptr) noexcept: ptr_{ptr} {
    assert(ptr);
  }

  explicit non_null_ptr(std::nullptr_t) = delete;

  template<typename U>
  requires std::convertible_to<U*, T*>
  constexpr non_null_ptr(non_null_ptr<U> other) noexcept: ptr_{other.get()} {}

  constexpr T* get() const noexcept {
    return ptr_;
  }

  constexpr operator T*() const noexcept {
    return ptr_;
  }

  constexpr T& operator*() const noexcept {
    return *ptr_;
  }

  constexpr T* operator->() const noexcept {
    return ptr_;
  }
};

} // util

template<typename T>
struct std::hash<::util::non_null_ptr<T>> {
  ::std::size_t operator()(::util::non_null_ptr<T> ptr) const noexcept {
    return ::std::hash<T*>{}(ptr.get());
  }
};

#endif
