#include "util/bigint.hpp"
#include "util/repeat_view.hpp"
#include "util/nums.hpp"
#include "util/overflow.hpp"
#include "util/unbounded_view.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <bit>
#include <cassert>
#include <compare>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <iterator>
#include <ostream>
#include <ranges>
#include <string>
#include <string_view>
#include <utility>

// Low-level access ////////////////////////////////////////////////////////////

std::size_t util::bigint::significant_limbs() const noexcept {
  return util::asserted_cast<std::size_t>(std::find_if(limbs_.crbegin(),
    limbs_.crend(), [](limb x) noexcept {
      return x != 0;
    }).base() - limbs_.cbegin());
}

std::size_t util::bigint::size() const noexcept {
  std::size_t result{significant_limbs() * sizeof(limb)};
  if(result == 0) {
    return 0;
  }

  limb const last_limb{limbs_[result / sizeof(limb) - 1]};
  assert(last_limb != 0);

  while(nth_byte((result - 1) % sizeof(limb), last_limb) == 0) {
    --result;
  }

  return result;
}

void util::bigint::reserve(std::size_t count) {
  limbs_.reserve(count / sizeof(limb) + (count % sizeof(limb) == 0 ? 0 : 1));
}

void util::bigint::shrink_to_fit() {
  limbs_.erase_after(limbs_.begin()
    + static_cast<std::ptrdiff_t>(div_ceil(size(), sizeof(limb))));
  limbs_.shrink_to_fit();
}

std::uint8_t util::bigint::operator[](std::size_t off) const noexcept {
  std::size_t const limb_off{off / sizeof(limb)};
  std::size_t const sublimb_off{off % sizeof(limb)};

  if(limb_off >= limbs_.size()) {
    return 0;
  } else {
    return nth_byte(sublimb_off, limbs_[limb_off]);
  }
}

util::bigint::lnz_t util::bigint::lowest_non_zero() const noexcept {
  if(!neg_) {
    // If the number is not negative, this computation is unnecessary and should
    // not be performed.
    return lnz_t{0};
  }

  auto const it{std::ranges::find_if(util::unbounded_view{limbs_.cbegin()},
    [](limb x) noexcept { return x != 0; })};
  return lnz_t{util::asserted_cast<std::size_t>(it - limbs_.cbegin())};
}

std::uint8_t util::bigint::twos_compl_at(std::size_t off) const noexcept {
  if(!neg_) {
    return operator[](off);
  }

  std::size_t const limb_off{off / sizeof(limb)};
  std::size_t const sublimb_off{off % sizeof(limb)};

  if(limb_off >= limbs_.size()) {
    return 0xFF;
  }

  std::size_t const lnz{util::asserted_cast<std::size_t>(
    std::find_if(limbs_.cbegin(),
    limbs_.cbegin() + static_cast<std::ptrdiff_t>(limb_off),
    [](limb x) noexcept { return x != 0; }) - limbs_.cbegin())};

  if(limb_off < lnz) {
    return 0;
  } else {
    limb const result_limb{~limbs_[limb_off] + (limb_off == lnz ? 1 : 0)};
    return nth_byte(sublimb_off, result_limb);
  }
}

std::uint8_t util::bigint::twos_compl_at(std::size_t off, lnz_t lnz_opaque)
    const noexcept {
  if(!neg_) {
    return operator[](off);
  }

  std::size_t const limb_off{off / sizeof(limb)};
  std::size_t const sublimb_off{off % sizeof(limb)};

  if(limb_off >= limbs_.size()) {
    return 0xFF;
  }

  std::size_t const lnz{lnz_opaque.value_};
  assert(lnz == lowest_non_zero().value_);

  if(limb_off < lnz) {
    return 0;
  } else {
    limb const result_limb{~limbs_[limb_off] + (limb_off == lnz ? 1 : 0)};
    return nth_byte(sublimb_off, result_limb);
  }
}

// Comparison //////////////////////////////////////////////////////////////////

bool util::operator==(util::bigint const& a, util::bigint const& b) noexcept {
  if(a.neg_ != b.neg_) {
    return false;
  }

  std::size_t const common_size{std::min(a.limbs_.size(), b.limbs_.size())};
  if(!std::equal(a.limbs_.cbegin(),
      a.limbs_.cbegin() + static_cast<std::ptrdiff_t>(common_size),
      b.limbs_.cbegin())) {
    return false;
  }

  if(a.limbs_.size() > b.limbs_.size()) {
    return std::equal(
      a.limbs_.cbegin() + static_cast<std::ptrdiff_t>(common_size),
      a.limbs_.cend(),
      util::repeat(bigint::limb{0}).begin()
    );
  } else {
    return std::equal(
      b.limbs_.cbegin() + static_cast<std::ptrdiff_t>(common_size),
      b.limbs_.cend(),
      util::repeat(bigint::limb{0}).begin()
    );
  }
}

std::strong_ordering util::operator<=>(util::bigint const& a,
    util::bigint const& b) noexcept {
  if(a.limbs_.empty() && b.limbs_.empty()) {
    return std::strong_ordering::equivalent;
  }

  if(a.neg_ != b.neg_) {
    return a.neg_ ? std::strong_ordering::less : std::strong_ordering::greater;
  }

  std::size_t idx{std::max(a.limbs_.size(), b.limbs_.size())};
  do {
    --idx;
    bigint::limb const a_limb{idx < a.limbs_.size() ? a.limbs_[idx] : 0};
    bigint::limb const b_limb{idx < b.limbs_.size() ? b.limbs_[idx] : 0};

    if(auto const cmp{a_limb <=> b_limb}; cmp != 0) {
      if(a.neg_) {
        return cmp < 0 ? std::strong_ordering::greater
          : std::strong_ordering::less;
      } else {
        return cmp;
      }
    }
  } while(idx > 0);
  return std::strong_ordering::equivalent;
}

// Additive operations /////////////////////////////////////////////////////////

void util::bigint::add_mags(util::bigint const& a, util::bigint const& b) {
  assert(this != &a);
  assert(this != &b);
  assert(a.limbs_.size() < limbs_.max_size());
  assert(b.limbs_.size() < limbs_.max_size());
  limbs_.clear();
  limbs_.reserve(std::max(a.limbs_.size(), b.limbs_.size()) + 1);

  std::size_t const common_size{std::min(a.limbs_.size(), b.limbs_.size())};
  bool carry{false};

  for(std::size_t i{0}; i < common_size; ++i) {
    two_limbs const new_limbs{static_cast<two_limbs>(a.limbs_[i])
      + b.limbs_[i] + (carry ? 1 : 0)};
    carry = (new_limbs >> (sizeof(limb) * 8)) != 0;
    limbs_.push_back(gsl::narrow_cast<limb>(new_limbs));
  }

  auto const& longer_limbs{a.limbs_.size() > b.limbs_.size()
    ? a.limbs_ : b.limbs_};
  {
    std::size_t i{common_size};

    if(carry) {
      // Pass the carry through all the extra all-one limbs
      while(i < longer_limbs.size() && ~longer_limbs[i] == 0) {
        limbs_.push_back(0);
        ++i;
      }

      // If we passed the carry out of the longer vector, extend the length
      if(i == longer_limbs.size()) {
        limbs_.push_back(1);
        return;
      }

      // Extinguish the carry with the last limb into which it was passed
      limbs_.push_back(longer_limbs[i++] + 1);
    }

    limbs_.append(longer_limbs.cbegin() + static_cast<std::ptrdiff_t>(i),
      longer_limbs.cend());
  }
}

void util::bigint::add_mag(util::bigint const& other) {
  assert(limbs_.size() < limbs_.max_size());
  assert(other.limbs_.size() < limbs_.max_size());
  limbs_.reserve(std::max(limbs_.size(), other.limbs_.size()) + 1);

  // This algorithm is just a simple loop over all the limbs in increasing order
  // of significance, so it will work fine if the other operand is the same as
  // this one.

  std::size_t const common_size{std::min(limbs_.size(), other.limbs_.size())};
  bool carry{false};

  for(std::size_t i{0}; i < common_size; ++i) {
    two_limbs const new_limbs{static_cast<two_limbs>(limbs_[i])
      + other.limbs_[i] + (carry ? 1 : 0)};
    carry = (new_limbs >> (sizeof(limb) * 8)) != 0;
    limbs_[i] = gsl::narrow_cast<limb>(new_limbs);
  }

  if(limbs_.size() > other.limbs_.size()) {
    if(carry) {
      std::size_t i{common_size};

      // Pass the carry through all the extra all-one limbs
      while(i < limbs_.size() && ~limbs_[i] == 0) {
        limbs_[i] = 0;
        ++i;
      }

      // If we passed the carry out of the longer vector, extend the length
      if(i == limbs_.size()) {
        limbs_.push_back(1);
        return;
      }

      // Extinguish the carry with the last limb into which it was passed
      limbs_[i] += 1;
    }
  } else {
    std::size_t i{common_size};

    if(carry) {
      // Pass the carry through all the extra all-one limbs
      while(i < other.limbs_.size() && ~other.limbs_[i] == 0) {
        limbs_.push_back(0);
        ++i;
      }

      // If we passed the carry out of the longer vector, extend the length
      if(i == other.limbs_.size()) {
        limbs_.push_back(1);
        return;
      }

      // Extinguish the carry with the last limb into which it was passed
      limbs_.push_back(other.limbs_[i++] + 1);
    }

    limbs_.append(other.limbs_.cbegin() + static_cast<std::ptrdiff_t>(i),
      other.limbs_.cend());
  }
}

void util::bigint::invert_mag() noexcept {
  auto first_non_zero{std::find_if(limbs_.begin(), limbs_.end(),
    [](limb x) noexcept { return x != 0; })};
  assert(first_non_zero != limbs_.end());

  *first_non_zero = ~*first_non_zero + 1;
  ++first_non_zero;
  std::transform(first_non_zero, limbs_.end(), first_non_zero, std::bit_not{});
}

void util::bigint::sub_mags(util::bigint const& a, util::bigint const& b) {
  assert(this != &a);
  assert(this != &b);
  assert(a.limbs_.size() < limbs_.max_size());
  assert(b.limbs_.size() < limbs_.max_size());
  limbs_.clear();
  limbs_.reserve(std::max(a.limbs_.size(), b.limbs_.size()) + 1);

  std::size_t const common_size{std::min(a.limbs_.size(), b.limbs_.size())};
  bool borrow{false};
  bool zero{true};

  for(std::size_t i{0}; i < common_size; ++i) {
    if(zero && a.limbs_[i] != b.limbs_[i]) {
      zero = false;
    }
    two_limbs const new_limbs{static_cast<two_limbs>(a.limbs_[i])
      - b.limbs_[i] - (borrow ? 1 : 0)};
    borrow = (new_limbs >> (sizeof(limb) * 8)) != 0;
    limbs_.push_back(gsl::narrow_cast<limb>(new_limbs));
  }

  if(a.limbs_.size() > b.limbs_.size()) {
    std::size_t i{common_size};

    if(borrow) {
      assert(!zero);

      // Pass the borrow through all the extra all-zero limbs
      while(i < a.limbs_.size() && a.limbs_[i] == 0) {
        limbs_.push_back(~static_cast<limb>(0));
        ++i;
      }

      // If we passed the borrow out of the longer vector, invert the magnitude.
      if(i == a.limbs_.size()) {
        invert_mag();
        neg_ = !neg_;
        return;
      }

      // Extinguish the borrow with the last limb into which it was passed
      limbs_.push_back(a.limbs_[i++] - 1);
    }

    while(i < a.limbs_.size()) {
      if(zero && a.limbs_[i] != 0) {
        zero = false;
      }
      limbs_.push_back(a.limbs_[i++]);
    }

    // If we didn't pass the borrow out of the longer vector, the result may be
    // zero. In that case, set the sign to false (regardless of what is used to
    // be).
    if(zero) {
      neg_ = false;
      limbs_.clear();
    }
  } else {
    std::size_t i{common_size};

    if(!borrow) {
      // Pass the non-borrow through all the extra all-zero limbs
      while(i < b.limbs_.size() && b.limbs_[i] == 0) {
        limbs_.push_back(0);
        ++i;
      }

      // If we passed the non-borrow out of the longer vector, we're done.
      if(i == b.limbs_.size()) {
        // If we pass the non-borrow out of the longer vector, the result may be
        // zero. In that case, set the sign to false (regardless of what is used
        // to be).
        if(zero) {
          neg_ = false;
          limbs_.clear();
        }
        return;
      }

      // Extinguish the non-borrow with the last limb into which it was passed
      limbs_.push_back(~b.limbs_[i++] + 1);
    }

    // Since we have a borrow at this point, the already-constructed magnitude
    // is definitely not zero. So it's safe to invert it.
    invert_mag();
    neg_ = !neg_;
    // Conceptually, the remaining limbs should be appended to the result with
    // inversion, and then inverted again by the above call to `invert_mag`. So
    // we skip the double inversion by calling `invert_mag` first, and then
    // appending these limbs unchanged.
    limbs_.append(b.limbs_.cbegin() + static_cast<std::ptrdiff_t>(i),
      b.limbs_.cend());
  }
}

void util::bigint::sub_mag(util::bigint const& other) {
  assert(limbs_.size() < limbs_.max_size());
  assert(other.limbs_.size() < limbs_.max_size());
  limbs_.reserve(std::max(limbs_.size(), other.limbs_.size()) + 1);

  if(this == &other) {
    limbs_.clear();
    neg_ = false;
    return;
  }

  std::size_t const common_size{std::min(limbs_.size(), other.limbs_.size())};
  bool borrow{false};
  bool zero{true};

  for(std::size_t i{0}; i < common_size; ++i) {
    if(zero && limbs_[i] != other.limbs_[i]) {
      zero = false;
    }
    two_limbs const new_limbs{static_cast<two_limbs>(limbs_[i])
      - other.limbs_[i] - (borrow ? 1 : 0)};
    borrow = (new_limbs >> (sizeof(limb) * 8)) != 0;
    limbs_[i] = gsl::narrow_cast<limb>(new_limbs);
  }

  if(limbs_.size() > other.limbs_.size()) {
    std::size_t i{common_size};

    if(borrow) {
      assert(!zero);

      // Pass the borrow through all the extra all-zero limbs
      while(i < limbs_.size() && limbs_[i] == 0) {
        limbs_[i] = ~static_cast<limb>(0);
        ++i;
      }

      // If we passed the borrow out of the longer vector, invert the magnitude.
      if(i == limbs_.size()) {
        invert_mag();
        neg_ = !neg_;
        return;
      }

      // Extinguish the borrow with the last limb into which it was passed
      limbs_[i++] -= 1;
    }

    // If we didn't pass the borrow out of the longer vector, the result may be
    // zero. In that case, set the sign to false (regardless of what is used to
    // be).
    if(zero && std::all_of(limbs_.cbegin() + static_cast<std::ptrdiff_t>(i),
        limbs_.cend(), [](limb x) noexcept { return x == 0; })) {
      neg_ = false;
      limbs_.clear();
    }
  } else {
    std::size_t i{common_size};

    if(!borrow) {
      // Pass the non-borrow through all the extra all-zero limbs
      while(i < other.limbs_.size() && other.limbs_[i] == 0) {
        limbs_.push_back(0);
        ++i;
      }

      // If we passed the non-borrow out of the longer vector, we're done.
      if(i == other.limbs_.size()) {
        // If we pass the non-borrow out of the longer vector, the result may be
        // zero. In that case, set the sign to false (regardless of what is used
        // to be).
        if(zero) {
          neg_ = false;
          limbs_.clear();
        }
        return;
      }

      // Extinguish the non-borrow with the last limb into which it was passed
      limbs_.push_back(~other.limbs_[i++] + 1);
    }

    // Since we have a borrow at this point, the already-constructed magnitude
    // is definitely not zero. So it's safe to invert it.
    invert_mag();
    neg_ = !neg_;
    // Conceptually, the remaining limbs should be appended to the result with
    // inversion, and then inverted again by the above call to `invert_mag`. So
    // we skip the double inversion by calling `invert_mag` first, and then
    // appending these limbs unchanged.
    limbs_.append(other.limbs_.cbegin() + static_cast<std::ptrdiff_t>(i),
      other.limbs_.cend());
  }
}

util::bigint& util::bigint::operator+=(util::bigint const& other) {
  if(neg_ == other.neg_) {
    add_mag(other);
  } else {
    sub_mag(other);
  }
  return *this;
}

util::bigint util::operator+(util::bigint const& a, util::bigint const& b) {
  bigint result;
  if(a.neg_ == b.neg_) {
    result.neg_ = a.neg_;
    result.add_mags(a, b);
  } else {
    result.neg_ = a.neg_;
    result.sub_mags(a, b);
  }
  return result;
}

void util::bigint::negate() noexcept {
  if(*this != 0) {
    neg_ = !neg_;
  } else {
    // We know this is a zero, so let's make life easier for ourselves later
    limbs_.clear();
  }
}

util::bigint& util::bigint::operator-=(util::bigint const& other) {
  if(this == &other) {
    // This is just a short-circuit; the underlying algorithms will work fine
    // without this.
    return *this = 0;
  }

  if(neg_ != other.neg_) {
    add_mag(other);
  } else {
    sub_mag(other);
  }
  return *this;
}

util::bigint util::operator-(util::bigint const& a, util::bigint const& b) {
  bigint result;
  if(a.neg_ != b.neg_) {
    result.neg_ = a.neg_;
    result.add_mags(a, b);
  } else {
    result.neg_ = a.neg_;
    result.sub_mags(a, b);
  }
  return result;
}

// Multiplication //////////////////////////////////////////////////////////////

util::bigint& util::bigint::operator*=(util::bigint const& other) {
  if(limbs_.empty()) {
    return *this;
  }
  if(other.limbs_.empty()) {
    limbs_.clear();
    neg_ = false;
    return *this;
  }
  if(this == &other) {
    bigint result{*this * other};
    return *this = std::move(result);
  }

  // This is simple long multiplication, not any kind of fancy fast
  // multiplication algorithm.

  bool a_zero{true};
  std::size_t const old_size{limbs_.size()};
  std::size_t i{old_size};
  do {
    --i;
    limb const cur_limb{limbs_[i]};
    limbs_[i] = 0;
    if(cur_limb == 0) {
      continue;
    }
    a_zero = false;

    assert(i + 1 <= limbs_.max_size());
    assert(other.limbs_.size() <= limbs_.max_size() - (i + 1));
    limbs_.reserve(i + 1 + other.limbs_.size());

    limb carry{0};
    bool b_zero{true};
    for(std::size_t j{0}; j < other.limbs_.size(); ++j) {
      if(other.limbs_[j] != 0) {
        b_zero = false;
      }

      two_limbs const new_limbs{static_cast<two_limbs>(cur_limb)
        * other.limbs_[j] + carry};
      carry = new_limbs >> (sizeof(limb) * 8);
      assert(carry < ~static_cast<limb>(0) - 1);

      if(i + j < limbs_.size()) {
        two_limbs const add_limbs{gsl::narrow_cast<limb>(new_limbs)
          + static_cast<two_limbs>(limbs_[i + j])};

        limb const add_carry{asserted_cast_to_limb(add_limbs >> (sizeof(limb) * 8))};
        assert(add_carry == 0 || (add_carry == 1 && carry < ~static_cast<limb>(0)));
        carry += add_carry;

        limbs_[i + j] = gsl::narrow_cast<limb>(add_limbs);
      } else {
        assert(i + j == limbs_.size());
        limbs_.push_back(gsl::narrow_cast<limb>(new_limbs));
      }
    }

    if(b_zero) {
      // Now that we've gone through the entire `other` and seen only zeros, we
      // know that it's zero.
      limbs_.clear();
      neg_ = false;
      return *this;
    }

    if(carry != 0) {
      if(limbs_.size() == i + other.limbs_.size()) {
        limbs_.push_back(carry);
      } else {
        // Add the carry to the appropriate limb
        two_limbs const new_limbs{static_cast<two_limbs>(carry)
          + limbs_[i + other.limbs_.size()]};
        limbs_[i + other.limbs_.size()] = gsl::narrow_cast<limb>(new_limbs);

        // The addition may have resulted in another (additive) carry, so pass
        // that carry forward as necessary...
        if(new_limbs >> (sizeof(limb) * 8) != 0) {
          auto const first_past{limbs_.begin()
            + static_cast<std::ptrdiff_t>(i + other.limbs_.size() + 1)};
          auto const first_non_zero{std::find_if(first_past, limbs_.end(),
            [](limb x) noexcept { return x != ~static_cast<limb>(0); })};
          std::fill(first_past, first_non_zero, 0);
          if(first_non_zero == limbs_.end()) {
            limbs_.push_back(1);
          } else {
            *first_non_zero += 1;
          }
        }
      }
    }
  } while(i > 0);

  if(a_zero) {
    assert(!neg_);
    limbs_.clear();
    return *this;
  }

  neg_ = (neg_ != other.neg_);

  assert(limbs_.size() <= old_size + other.limbs_.size());
  return *this;
}

util::bigint util::operator*(util::bigint const& a, util::bigint const& b) {
  bigint result;
  if(a.limbs_.empty() || b.limbs_.empty()) {
    return result;
  }

  // This is simple long multiplication, not any kind of fancy fast
  // multiplication algorithm.

  bool a_zero{true};
  for(std::size_t i{0}; i < a.limbs_.size(); ++i) {
    if(a.limbs_[i] == 0) {
      continue;
    }
    a_zero = false;

    assert(a.limbs_.size() <= result.limbs_.max_size());
    assert(b.limbs_.size() <= result.limbs_.max_size() - a.limbs_.size());
    result.limbs_.reserve(a.limbs_.size() + b.limbs_.size());

    if(result.limbs_.size() < i) {
      auto const view{util::repeat(bigint::limb{0}, i - result.limbs_.size())};
      result.limbs_.append(view.begin(), view.end());
    }

    bigint::limb carry{0};
    bool b_zero{true};
    for(std::size_t j{0}; j < b.limbs_.size(); ++j) {
      if(b.limbs_[j] != 0) {
        b_zero = false;
      }

      bigint::two_limbs const new_limbs{
        static_cast<bigint::two_limbs>(a.limbs_[i]) * b.limbs_[j] + carry};
      carry = new_limbs >> (sizeof(bigint::limb) * 8);
      assert(carry < ~static_cast<bigint::limb>(0) - 1);

      if(i + j < result.limbs_.size()) {
        bigint::two_limbs const add_limbs{static_cast<bigint::limb>(new_limbs)
          + static_cast<bigint::two_limbs>(result.limbs_[i + j])};

        bigint::limb const add_carry{bigint::asserted_cast_to_limb(
          add_limbs >> (sizeof(bigint::limb) * 8))};
        assert(add_carry == 0 || (add_carry == 1 && carry < ~static_cast<bigint::limb>(0)));
        carry += add_carry;

        result.limbs_[i + j] = gsl::narrow_cast<bigint::limb>(add_limbs);
      } else {
        assert(i + j == result.limbs_.size());
        result.limbs_.push_back(gsl::narrow_cast<bigint::limb>(new_limbs));
      }
    }

    if(b_zero) {
      // Now that we've gone through the entire `b` and seen only zeros, we know
      // that it's zero.
      result.limbs_.clear();
      return result;
    }

    if(carry != 0) {
      assert(result.limbs_.size() == i + b.limbs_.size());
      result.limbs_.push_back(static_cast<bigint::limb>(carry));
    }
  }

  if(a_zero) {
    assert(result.limbs_.empty());
    return result;
  }

  result.neg_ = (a.neg_ != b.neg_);

  assert(result.limbs_.size() <= a.limbs_.size() + b.limbs_.size());
  return result;
}

// Division and remainder //////////////////////////////////////////////////////

void util::bigint::divide_by_limb(util::bigint::limb divisor, bool divisor_neg,
    util::bigint* quotient) {
  assert(quotient != this);
  assert(divisor != 0);

  std::size_t const dividend_limbs{significant_limbs()};
  if(dividend_limbs == 0) {
    if(quotient) {
      *quotient = 0;
    }
    return;
  }

  bool quotient_is_zero{true};

  if(quotient) {
    quotient->limbs_.resize(dividend_limbs);
  }

  // TODO: Division via multiplicative inverse

  std::size_t cur_limb{dividend_limbs - 1};
  if(quotient) {
    limb const quotient_limb{limbs_[cur_limb] / divisor};
    quotient->limbs_[cur_limb] = quotient_limb;
    if(quotient_limb != 0) {
      quotient_is_zero = false;
    }
  }
  limbs_[cur_limb] %= divisor;

  while(cur_limb-- > 0) {
    two_limbs const cur_dividend{(static_cast<two_limbs>(limbs_[cur_limb + 1])
      << (sizeof(limb) * 8)) | limbs_[cur_limb]};
    limbs_[cur_limb + 1] = 0;
    limbs_[cur_limb] = asserted_cast_to_limb(cur_dividend % divisor);
    if(quotient) {
      limb const quotient_limb{asserted_cast_to_limb(cur_dividend / divisor)};
      quotient->limbs_[cur_limb] = quotient_limb;
      if(quotient_limb != 0) {
        quotient_is_zero = false;
      }
    }
  }
  limbs_.erase_after(limbs_.cbegin() + 1);

  // The sign of the quotient is determined by regular multiplication rules.
  if(quotient) {
    if(quotient_is_zero) {
      quotient->limbs_.clear();
      quotient->neg_ = false;
    } else {
      quotient->neg_ = (neg_ != divisor_neg);
    }
  }

  // Since we round towards zero, the sign of the remainder is the same as the
  // sign of the dividend, unless the remainder is zero.
  if(limbs_[0] == 0) {
    limbs_.clear();
    neg_ = false;
  }
}

void util::bigint::divide(util::bigint const& divisor, util::bigint* quotient) {
  assert(quotient != this);
  assert(quotient != &divisor);
  assert(divisor != 0);
  if(this == &divisor) {
    *this = 0;
    if(quotient) {
      *quotient = 1;
    }
    return;
  }

  // This is just a dumb implementation of long division.
  //
  // The long division algorithm, as typically taught, is surprisingly
  // underspecified: it relies on the ability to perform a division in a
  // particular case where the quotient is known to be one digit long (i. e.
  // less than the base), without providing any guidance to doing so. If the
  // base is small (say, ten), this can be done simply by trial and error. But
  // in our case that is not possible.
  //
  // Thus, the following algorithm was adopted, inspired by the blog post at
  // <https://wbhart.blogspot.com/2010/10/bsdnt-divrem-discussion.html>.
  //
  // To perform the division in the required particular case, we calculate a
  // guess for the quotient, chosen so that the real quotient can never be less
  // than the guess, but can be greater than the guess by no more than 2. Then
  // we subtract the divisor multiplied by the quotient from the dividend to
  // test the guess. If the result is greater than the divisor, we keep
  // subtracting the divisor until that is no longer the case.
  //
  // To calculate the guess, we take the most significant N bits of the divisor,
  // where N is the bit width of a single limb, and the corresponding prefix of
  // the dividend, and perform the division on them. As proven below, this will
  // never be less than the real quotient, but can exceed the quotient by no
  // more than two. So we subtract two to obtain our guess.
  //
  // (If the divisor has fewer than N significant bits, then it fits into a
  // single limb, so we don't need to concern ourselves with any of this; we can
  // simply use the built-in division operator.)
  //
  // Let A be the dividend, B be the divisor, X be our prefix of the dividend,
  // Y be our prefix of the divisor, M be the length of the cut-off tails of the
  // operands. In the following, division is interpreted as mathematical
  // division, and rounding is specified explicitly.
  //
  // First, consider the fact that if ⌊X / Y⌋ = Z, this means that
  // Z * Y <= X < (Z + 1) * Y. We need to determine how far off Z can be from
  // the real quotient. If the tails of A and B that we cut off consist of
  // zeros, then multiplying all sides of these inequalities by 2^M gives
  // Z * Y * 2^M <= X * 2^M < (Z + 1) * Y * 2^M, or Z * B <= A < (Z + 1) * B, or
  // ⌊A / B⌋ = Z.
  //
  // The real quotient is the greatest if the dividend is extended with ones and
  // the divisor is extended with zeros, i. e. A = X * 2^M + (2^M - 1),
  // B = Y * 2^M. Then the second inequality becomes:
  // X * 2^M + (2^M - 1) < (Z + 1) * Y * 2^M. It still holds, because the right
  // side is a multiple of 2^M, and X * 2^M is less than it, so every number
  // greater than X * 2^M up to the next multiple of 2^M is also less than the
  // right side.
  //
  // The real quotient is the least if the dividend is extended with zeros and
  // the divisor is extended with ones, i. e. A = X * 2^M,
  // B = Y * 2^M + (2^M - 1). Since B < (Y + 1) * 2^M, we can instead consider
  // how small the value ⌊A / ((Y + 1) * 2^M)⌋ = ⌊X / (Y + 1)⌋ can be; the value
  // of ⌊A / B⌋ cannot be less. We want to prove that
  // ⌊X / (Y + 1)⌋ >= ⌊X / Y⌋ - 2.
  //
  // First, note that Y >= 2^(N-1), since it consists of N significant bits.
  // This implies 2^N * Y / (Y + 1) >= 2^N - 2. Since the right side is an
  // integer, the left side can be rounded down while maintaining the
  // inequality: ⌊2^N * Y / (Y + 1)⌋ >= 2^N - 2. Therefore, if X = 2^N * Y
  // (which isn't allowed in the particular case of division that we're focusing
  // on, but consider it anyway), then ⌊X / (Y + 1)⌋ >= ⌊X / Y⌋ - 2.
  //
  // Now, suppose that the condition holds for a particular X that is a multiple
  // of Y, and consider X' = X - Y. Then ⌊X' / Y⌋ = ⌊X / Y⌋ - 1, so the left
  // side is allowed to decrease only by 1. But there can only be one multiple
  // of Y + 1, say W, such that X >= W > X', since X and X' differ only by Y.
  // Thus, the left side will only decrease by 1, and the inequality will hold.
  // Thus, the inequality holds for all dividends that are multiples of Y.
  //
  // Finally, suppose that X and X' are adjacent multiples of Y, and consider
  // all dividends between them. Since ⌊(X - 1) / Y⌋ = ⌊X / Y⌋ - 1, the decrease
  // by 1 occurs on the very first step, going from X to X'. So the at most one
  // decrease of ⌊X / (Y + 1)⌋ is allowed to occur at any point in between X and
  // X' without breaking the inequality.
  //
  // Thus, the inequality holds for all X and Y, and the choice of the guess and
  // the algorithm are valid.

  std::size_t const divisor_limbs{divisor.significant_limbs()};
  assert(divisor_limbs != 0);
  if(divisor_limbs == 1) {
    this->divide_by_limb(divisor.limbs_[0], divisor.neg_, quotient);
    return;
  }

  std::size_t const dividend_limbs{significant_limbs()};
  if(divisor_limbs > dividend_limbs) {
    if(quotient) {
      *quotient = 0;
    }
    return;
  }

  std::size_t align{dividend_limbs - divisor_limbs};
  if(quotient) {
    quotient->limbs_.resize(align + 1);
  }
  bool quotient_is_zero{true};

  std::size_t const divisor_scale{static_cast<std::uint8_t>(
    std::countl_zero(divisor.limbs_[divisor_limbs - 1]))};
  limb const divisor_for_guess{
    (divisor.limbs_[divisor_limbs - 1] << divisor_scale)
    | (divisor.limbs_[divisor_limbs - 2]
      >> (sizeof(limb) * 8 - divisor_scale))};

  two_limbs dividend_for_guess{
    (static_cast<two_limbs>(limbs_[align + divisor_limbs - 1]) << divisor_scale)
    | (limbs_[align + divisor_limbs - 2]
      >> (sizeof(limb) * 8 - divisor_scale))};

  for(;;) {
    limb const first_guess{asserted_cast_to_limb(dividend_for_guess / divisor_for_guess)};
    limb guess{first_guess < 2 ? 0 : (first_guess - 2)};

    if(first_guess != 0) {
      // Multiply the divisor by guess and subtract the result from the
      // dividend's prefix aligned with the divisor.
      if(guess != 0) {
        limb carry{0};
        auto const extend_carry{[](limb x) noexcept {
          return x | (x == 0 ? 0 : ~static_cast<two_limbs>(0)
            << (sizeof(limb) * 8));
        }};

        for(std::size_t i{0}; i < divisor_limbs; ++i) {
          two_limbs const mul_result{
            static_cast<two_limbs>(divisor.limbs_[i]) * guess};

          // After the subtraction, the high limb, which produces the new carry,
          // contains the borrow from the subtraction and also the additive
          // inverse of the multiplication's carry, so the old carry has to be
          // added here (not subtracted).
          //
          // When modeling subtraction using the addition of an additive
          // inverse, we're going the other way around the integer ring. This
          // means that the high limb will be greater by one than the correct
          // result (which we would have obtained by subtraction). So we need to
          // pad the carry with ones, not zeros, to subtract that extra one.
          // However, zero is its own additive inverse, and adding it is the
          // same as subtracting it, so we should pad with zeros as usual.
          two_limbs const sub_result{(limbs_[align + i] - mul_result)
            + extend_carry(carry)};

          carry = asserted_cast_to_limb(sub_result >> (sizeof(limb) * 8));
          limbs_[align + i] = gsl::narrow_cast<limb>(sub_result);
        }

        // The prefix may be one limb longer than the aligned divisor
        if(align + divisor_limbs < dividend_limbs) {
          two_limbs const sub_result{limbs_[align + divisor_limbs]
            + extend_carry(carry)};
          carry = asserted_cast_to_limb(sub_result >> (sizeof(limb) * 8));
          limbs_[align + divisor_limbs] = gsl::narrow_cast<limb>(sub_result);
        }

        assert(carry == 0);
      }

      auto const prefix_is_less{[&]() noexcept {
        if(align + divisor_limbs < dividend_limbs
            && limbs_[align + divisor_limbs] != 0) {
          return false;
        }

        std::size_t i{divisor_limbs};
        while(i-- > 0) {
          if(limbs_[align + i] < divisor.limbs_[i]) {
            return true;
          } else if(limbs_[align + i] > divisor.limbs_[i]) {
            return false;
          }
        }
        return false;
      }};

      auto const subtract_divisor{[&]() noexcept {
        bool borrow{false};

        for(std::size_t i{0}; i < divisor_limbs; ++i) {
          two_limbs const sub_result{static_cast<two_limbs>(limbs_[align + i])
            - divisor.limbs_[i] - (borrow ? 1 : 0)};
          borrow = (sub_result >> (sizeof(limb) * 8)) != 0;
          limbs_[align + i] = gsl::narrow_cast<limb>(sub_result);
        }

        // The prefix may be one limb longer than the aligned divisor
        if(borrow) {
          assert(align + divisor_limbs < dividend_limbs);
          assert(limbs_[align + divisor_limbs] != 0);
          --limbs_[align + divisor_limbs];
        }
      }};

      if(first_guess != 0 && !prefix_is_less()) {
        subtract_divisor();
        ++guess;

        if(first_guess != 1 && !prefix_is_less()) {
          subtract_divisor();
          ++guess;
        }
      }

      assert(prefix_is_less());
    }

    if(quotient) {
      quotient->limbs_[align] = guess;
      if(guess != 0) {
        quotient_is_zero = false;
      }
    }

    if(align == 0) {
      break;
    }
    --align;

    dividend_for_guess
      = (static_cast<two_limbs>(limbs_[align + divisor_limbs])
        << (divisor_scale + sizeof(limb) * 8))
      | (static_cast<two_limbs>(
        limbs_[align + divisor_limbs - 1]) << divisor_scale)
      | (limbs_[align + divisor_limbs - 2]
        >> (sizeof(limb) * 8 - divisor_scale));
  }
  limbs_.erase_after(limbs_.cbegin()
    + static_cast<std::ptrdiff_t>(divisor_limbs));

  // The sign of the quotient is determined by regular multiplication rules.
  if(quotient) {
    if(quotient_is_zero) {
      quotient->limbs_.clear();
      quotient->neg_ = false;
    } else {
      quotient->neg_ = (neg_ != divisor.neg_);
    }
  }

  // Since we round towards zero, the sign of the remainder is the same as the
  // sign of the dividend, unless the remainder is zero.
  if(std::all_of(limbs_.cbegin(), limbs_.cend(),
      [](limb x) noexcept { return x == 0; })) {
    limbs_.clear();
    neg_ = false;
  }
}

// Bitwise operations //////////////////////////////////////////////////////////

void util::bigint::mag_decrement() noexcept {
  auto cur{limbs_.begin()};
  assert(cur != limbs_.end());
  while(*cur == 0) {
    *cur++ = ~static_cast<limb>(0);
    assert(cur != limbs_.end());
  }
  *cur -= 1;
}

util::bigint util::bigint::operator~() const & {
  if(neg_) {
    bigint result{-*this};
    result.mag_decrement();
    return result;
  } else {
    bigint result{*this + 1};
    result.neg_ = true;
    return result;
  }
}

util::bigint& util::bigint::operator&=(util::bigint const& other) {
  if(this == &other) {
    return *this;
  }

  std::size_t const common_size{std::min(limbs_.size(), other.limbs_.size())};
  limbs_.reserve(other.limbs_.size());

  auto this_it{this->limbs_begin()};
  auto other_it{other.limbs_begin()};
  bool flip_result{false};

  for(std::size_t i{0}; i < common_size; ++i) {
    bigint::limb const result_limb{*this_it++ & *other_it++};
    limbs_[i] = flip_result ? ~result_limb :
      neg_ && other.neg_ ? ~result_limb + 1 : result_limb;
    if(result_limb != 0 && neg_ && other.neg_) {
      flip_result = true;
    }
  }

  if(neg_ && other.limbs_.size() > limbs_.size()) {
    for(std::size_t i{common_size}; i < other.limbs_.size(); ++i) {
      bigint::limb const result_limb{*other_it++};
      limbs_.push_back(flip_result ? ~result_limb :
        neg_ && other.neg_ ? ~result_limb + 1 : result_limb);
      if(result_limb != 0 && neg_ && other.neg_) {
        flip_result = true;
      }
    }
  } else if(other.limbs_.size() < limbs_.size()) {
    if(other.neg_) {
      for(std::size_t i{common_size}; i < limbs_.size(); ++i) {
        bigint::limb const result_limb{*this_it++};
        limbs_[i] = flip_result ? ~result_limb :
          neg_ && other.neg_ ? ~result_limb + 1 : result_limb;
        if(result_limb != 0 && neg_ && other.neg_) {
          flip_result = true;
        }
      }
    } else {
      limbs_.erase_after(limbs_.cbegin()
        + static_cast<std::ptrdiff_t>(common_size));
    }
  }

  neg_ = neg_ && other.neg_;
  return *this;
}

util::bigint util::operator&(util::bigint const& a, util::bigint const& b) {
  if(&a == &b) {
    return a;
  }

  bigint result;
  std::size_t const longer_size{std::max(a.limbs_.size(), b.limbs_.size())};
  std::size_t const common_size{std::min(a.limbs_.size(), b.limbs_.size())};
  result.limbs_.reserve(std::max(a.limbs_.size(), b.limbs_.size()));

  auto a_it{a.limbs_begin()};
  auto b_it{b.limbs_begin()};
  bool flip_result{false};

  for(std::size_t i{0}; i < common_size; ++i) {
    bigint::limb const result_limb{*a_it++ & *b_it++};
    result.limbs_.push_back(flip_result ? ~result_limb :
      a.neg_ && b.neg_ ? ~result_limb + 1 : result_limb);
    if(result_limb != 0 && a.neg_ && b.neg_) {
      flip_result = true;
    }
  }

  if(a.limbs_.size() > b.limbs_.size() ? b.neg_ : a.neg_) {
    auto& longer_it{a.limbs_.size() > b.limbs_.size() ? a_it : b_it};

    for(std::size_t i{common_size}; i < longer_size; ++i) {
      bigint::limb const result_limb{*longer_it++};
      result.limbs_.push_back(flip_result ? ~result_limb :
        a.neg_ && b.neg_ ? ~result_limb + 1 : result_limb);
      if(result_limb != 0 && a.neg_ && b.neg_) {
        flip_result = true;
      }
    }
  }

  result.neg_ = a.neg_ && b.neg_;
  return result;
}

util::bigint& util::bigint::operator|=(util::bigint const& other) {
  if(this == &other) {
    return *this;
  }

  std::size_t const common_size{std::min(limbs_.size(), other.limbs_.size())};
  limbs_.reserve(other.limbs_.size());

  auto this_it{this->limbs_begin()};
  auto other_it{other.limbs_begin()};
  bool flip_result{false};

  for(std::size_t i{0}; i < common_size; ++i) {
    bigint::limb const result_limb{*this_it++ | *other_it++};
    limbs_[i] = flip_result ? ~result_limb :
      neg_ || other.neg_ ? ~result_limb + 1 : result_limb;
    if(result_limb != 0 && (neg_ || other.neg_)) {
      flip_result = true;
    }
  }

  // If the shorter operand's sign limb is ~0, then OR'ing it into another limb
  // will produce ~0. But in this case, the result is negative, so it will
  // actually store either 0, if `flip_result` is true, or 1 otherwise. However,
  // the only way `flip_result` can be false at this point is if all the result
  // limbs we have just stored are zeros, which is only possible if all the
  // input limbs were zeros, including all the limbs of the shorter operand.
  // But it is impossible, since the shorter operand is negative. So in this
  // case, all the limbs we will store in the destination are zeros, so we can
  // either skip this step, if the destination is longer, or trim the
  // destination, if it is shorter.

  if(other.limbs_.size() > limbs_.size()) {
    if(neg_) {
      assert(flip_result);
    } else {
      for(std::size_t i{common_size}; i < other.limbs_.size(); ++i) {
        bigint::limb const result_limb{*other_it++};
        limbs_.push_back(flip_result ? ~result_limb :
          neg_ || other.neg_ ? ~result_limb + 1 : result_limb);
        if(result_limb != 0 && (neg_ || other.neg_)) {
          flip_result = true;
        }
      }
    }
  } else {
    if(other.neg_) {
      assert(flip_result);
      limbs_.erase_after(limbs_.cbegin()
        + static_cast<std::ptrdiff_t>(common_size));
    } else {
      for(std::size_t i{common_size}; i < limbs_.size(); ++i) {
        bigint::limb const result_limb{*this_it++};
        limbs_[i] = flip_result ? ~result_limb :
          neg_ || other.neg_ ? ~result_limb + 1 : result_limb;
        if(result_limb != 0 && (neg_ || other.neg_)) {
          flip_result = true;
        }
      }
    }
  }

  neg_ = neg_ || other.neg_;
  return *this;
}

util::bigint util::operator|(util::bigint const& a, util::bigint const& b) {
  if(&a == &b) {
    return a;
  }

  bigint result;
  std::size_t const longer_size{std::max(a.limbs_.size(), b.limbs_.size())};
  std::size_t const common_size{std::min(a.limbs_.size(), b.limbs_.size())};
  result.limbs_.reserve(std::max(a.limbs_.size(), b.limbs_.size()));

  auto a_it{a.limbs_begin()};
  auto b_it{b.limbs_begin()};
  bool flip_result{false};

  for(std::size_t i{0}; i < common_size; ++i) {
    bigint::limb const result_limb{*a_it++ | *b_it++};
    result.limbs_.push_back(flip_result ? ~result_limb :
      a.neg_ || b.neg_ ? ~result_limb + 1 : result_limb);
    if(result_limb != 0 && (a.neg_ || b.neg_)) {
      flip_result = true;
    }
  }

  // If the shorter operand's sign limb is ~0, then OR'ing it into another limb
  // will produce ~0. But in this case, the result is negative, so it will
  // actually store either 0, if `flip_result` is true, or 1 otherwise. However,
  // the only way `flip_result` can be false at this point is if all the result
  // limbs we have just stored are zeros, which is only possible if all the
  // input limbs were zeros, including all the limbs of the shorter operand.
  // But it is impossible, since the shorter operand is negative. So in this
  // case, all the limbs we will store in the destination are zeros, so we can
  // skip this step.

  auto& longer_it{a.limbs_.size() > b.limbs_.size() ? a_it : b_it};

  if(a.limbs_.size() > b.limbs_.size() ? !b.neg_ : !a.neg_) {
    for(std::size_t i{common_size}; i < longer_size; ++i) {
      bigint::limb const result_limb{*longer_it++};
      result.limbs_.push_back(flip_result ? ~result_limb :
        a.neg_ || b.neg_ ? ~result_limb + 1 : result_limb);
      if(result_limb != 0 && (a.neg_ || b.neg_)) {
        flip_result = true;
      }
    }
  } else {
    assert(flip_result);
  }

  result.neg_ = a.neg_ || b.neg_;
  return result;
}

util::bigint& util::bigint::operator^=(util::bigint const& other) {
  if(this == &other) {
    return *this = 0;
  }

  std::size_t const common_size{std::min(limbs_.size(), other.limbs_.size())};
  limbs_.reserve(other.limbs_.size());

  auto this_it{this->limbs_begin()};
  auto other_it{other.limbs_begin()};
  bool flip_result{false};

  for(std::size_t i{0}; i < common_size; ++i) {
    bigint::limb const result_limb{*this_it++ ^ *other_it++};
    limbs_[i] = flip_result ? ~result_limb :
      (neg_ != other.neg_) ? ~result_limb + 1 : result_limb;
    if(result_limb != 0 && neg_ != other.neg_) {
      flip_result = true;
    }
  }

  if(other.limbs_.size() > limbs_.size()) {
    bigint::limb const sign_limb{neg_ ? ~static_cast<bigint::limb>(0) : 0};

    for(std::size_t i{common_size}; i < other.limbs_.size(); ++i) {
      bigint::limb const result_limb{*other_it++ ^ sign_limb};
      limbs_.push_back(flip_result ? ~result_limb :
        (neg_ != other.neg_) ? ~result_limb + 1 : result_limb);
      if(result_limb != 0 && neg_ != other.neg_) {
        flip_result = true;
      }
    }
  } else {
    bigint::limb const sign_limb{other.neg_
      ? ~static_cast<bigint::limb>(0) : 0};

    for(std::size_t i{common_size}; i < limbs_.size(); ++i) {
      bigint::limb const result_limb{*this_it++ ^ sign_limb};
      limbs_[i] = flip_result ? ~result_limb :
        (neg_ != other.neg_) ? ~result_limb + 1 : result_limb;
      if(result_limb != 0 && neg_ != other.neg_) {
        flip_result = true;
      }
    }
  }

  neg_ = (neg_ != other.neg_);
  return *this;
}

util::bigint util::operator^(util::bigint const& a, util::bigint const& b) {
  if(&a == &b) {
    return 0;
  }

  bigint result;
  std::size_t const longer_size{std::max(a.limbs_.size(), b.limbs_.size())};
  std::size_t const common_size{std::min(a.limbs_.size(), b.limbs_.size())};
  result.limbs_.reserve(std::max(a.limbs_.size(), b.limbs_.size()));

  auto a_it{a.limbs_begin()};
  auto b_it{b.limbs_begin()};
  bool flip_result{false};

  for(std::size_t i{0}; i < common_size; ++i) {
    bigint::limb const result_limb{*a_it++ ^ *b_it++};
    result.limbs_.push_back(flip_result ? ~result_limb :
      (a.neg_ != b.neg_) ? ~result_limb + 1 : result_limb);
    if(result_limb != 0 && a.neg_ != b.neg_) {
      flip_result = true;
    }
  }

  auto& longer_it{a.limbs_.size() > b.limbs_.size() ? a_it : b_it};
  bigint::limb const sign_limb{
    (a.limbs_.size() > b.limbs_.size() ? b.neg_ : a.neg_)
    ? ~static_cast<bigint::limb>(0) : 0};

  for(std::size_t i{common_size}; i < longer_size; ++i) {
    bigint::limb const result_limb{*longer_it++ ^ sign_limb};
    result.limbs_.push_back(flip_result ? ~result_limb :
      (a.neg_ != b.neg_) ? ~result_limb + 1 : result_limb);
    if(result_limb != 0 && a.neg_ != b.neg_) {
      flip_result = true;
    }
  }

  result.neg_ = (a.neg_ != b.neg_);
  return result;
}

util::bigint& util::bigint::operator<<=(std::size_t off) {
  if(limbs_.empty()) {
    return *this;
  }

  // It is enough to just shift extra zero bits from the right. If the number is
  // negative, then all bits up to and including the least significant bit that
  // is equal to 1 are not inverted in the two's complement representation; this
  // includes the zero bits that are shifted in, so the implementation is
  // correct.

  std::size_t const old_size{limbs_.size()};
  std::size_t const limb_off{off / (sizeof(bigint::limb) * 8)};
  std::size_t const sublimb_off{off % (sizeof(bigint::limb) * 8)};

  assert(limbs_.max_size() - old_size >= limb_off + (sublimb_off == 0 ? 0 : 1));
  limbs_.reserve(old_size + limb_off + (sublimb_off == 0 ? 0 : 1));

  // Add extra limbs

  if(old_size <= limb_off) {
    while(limbs_.size() < limb_off) {
      limbs_.push_back(0);
    }

    limbs_.push_back(limbs_[0] << sublimb_off);
  }

  for(std::size_t i{limbs_.size() - limb_off}; i < old_size; ++i) {
    limbs_.push_back((limbs_[i] << sublimb_off) | (sublimb_off == 0 ? 0
      : (limbs_[i - 1] >> (sizeof(bigint::limb) * 8 - sublimb_off))));
  }

  if(sublimb_off != 0) {
    limbs_.push_back(limbs_[old_size - 1]
      >> (sizeof(bigint::limb) * 8 - sublimb_off));
  }

  // Change existing limbs

  for(std::size_t i{old_size - 1}; i > limb_off; --i) {
    limbs_[i] = (limbs_[i - limb_off] << sublimb_off) | (sublimb_off == 0 ? 0
      : (limbs_[i - limb_off - 1] >> (sizeof(bigint::limb) * 8 - sublimb_off)));
  }

  if(limb_off < old_size) {
    limbs_[limb_off] = limbs_[0] << sublimb_off;
  }

  std::fill(limbs_.begin(), limbs_.begin()
    + static_cast<std::ptrdiff_t>(std::min(limb_off, old_size)), 0);

  return *this;
}

util::bigint util::operator<<(util::bigint const& num, std::size_t off) {
  if(num.limbs_.empty()) {
    return 0;
  }

  // It is enough to just shift extra zero bits from the right. If the number is
  // negative, then all bits up to and including the least significant bit that
  // is equal to 1 are not inverted in the two's complement representation; this
  // includes the zero bits that are shifted in, so the implementation is
  // correct.

  std::size_t const limb_off{off / (sizeof(bigint::limb) * 8)};
  std::size_t const sublimb_off{off % (sizeof(bigint::limb) * 8)};

  bigint result;
  assert(result.limbs_.max_size() >= num.limbs_.size());
  assert(result.limbs_.max_size() - num.limbs_.size() >= limb_off
    + (sublimb_off == 0 ? 0 : 1));
  result.limbs_.reserve(num.limbs_.size() + limb_off
    + (sublimb_off == 0 ? 0 : 1));
  result.neg_ = num.neg_;

  result.limbs_.assign(limb_off, 0);
  result.limbs_.push_back(num.limbs_[0] << sublimb_off);

  for(std::size_t i{1}; i < num.limbs_.size(); ++i) {
    result.limbs_.push_back((num.limbs_[i] << sublimb_off) | (sublimb_off == 0
      ? 0 : (num.limbs_[i - 1] >> (sizeof(bigint::limb) * 8 - sublimb_off))));
  }

  if(sublimb_off != 0) {
    result.limbs_.push_back(num.limbs_.back()
      >> (sizeof(bigint::limb) * 8 - sublimb_off));
  }

  return result;
}

util::bigint& util::bigint::operator>>=(std::size_t off) noexcept {
  std::size_t const limb_off{off / (sizeof(bigint::limb) * 8)};
  std::size_t const sublimb_off{off % (sizeof(bigint::limb) * 8)};

  if(limbs_.size() <= limb_off) {
    if(neg_) {
      return *this = -1;
    } else {
      return *this = 0;
    }
  }

  // When right-shifting a negative number, the behavior should be as if we did
  // the following:
  //
  // 1. Convert the magnitude to the two's complement representation:
  //   1.1. Subtract 1 from the magnitude.
  //   1.2. Perform a bitwise inversion of the magnitude.
  // 2. Perform the right-shift on the two's complement representation.
  // 3. Convert the two's complement representation back to the magnitude:
  //   3.1. Perform a bitwise inversion of the magnitude.
  //   3.2. Add 1 to the magnitude.
  //
  // Since the actual right-shift is sandwiched between the two bitwise
  // inversions, they can simply be elided. Thus, if the number is negative, we
  // simply need to subtract 1 from the magnitude before the shift and add 1
  // after the shift.

  if(neg_) {
    mag_decrement();
  }

  for(std::size_t i{limb_off}; i < limbs_.size() - 1; ++i) {
    limbs_[i - limb_off] = (limbs_[i] >> sublimb_off) | (sublimb_off == 0 ? 0
      : (limbs_[i + 1] << (sizeof(limb) * 8 - sublimb_off)));
  }
  limbs_[limbs_.size() - 1 - limb_off] = limbs_.back() >> sublimb_off;
  limbs_.erase_after(limbs_.cend() - static_cast<std::ptrdiff_t>(limb_off));

  if(neg_) {
    this->add_mag(1);
  }

  return *this;
}

util::bigint util::operator>>(util::bigint const& num, std::size_t off) {
  std::size_t const limb_off{off / (sizeof(bigint::limb) * 8)};
  std::size_t const sublimb_off{off % (sizeof(bigint::limb) * 8)};

  if(num.limbs_.size() <= limb_off) {
    if(num.neg_) {
      return -1;
    } else {
      return 0;
    }
  }

  bigint result;
  result.limbs_.reserve(num.limbs_.size() - limb_off);

  // Since we can't modify the input in place, in the event that the input is
  // negative, we generate the result of subtracting 1 from the magnitude on the
  // fly and use the result when shifting.

  bool found_non_zero{false};
  std::size_t input_index{0};
  auto const get_next_limb{[&]() noexcept {
    assert(input_index < num.limbs_.size());
    bigint::limb result{num.limbs_[input_index++]};
    if(num.neg_ && !found_non_zero) {
      if(result-- != 0) {
        found_non_zero = true;
      }
    }
    return result;
  }};

  for(std::size_t i{0}; i < limb_off; ++i) {
    get_next_limb();
  }
  bigint::limb last_limb{get_next_limb()};

  for(std::size_t i{limb_off}; i < num.limbs_.size() - 1; ++i) {
    bigint::limb const next_limb{get_next_limb()};
    result.limbs_.push_back((last_limb >> sublimb_off) | (sublimb_off == 0 ? 0
      : (next_limb << (sizeof(bigint::limb) * 8 - sublimb_off))));
    last_limb = next_limb;
  }
  result.limbs_.push_back(num.limbs_.back() >> sublimb_off);

  if(num.neg_) {
    result.add_mag(1);
  }

  result.neg_ = num.neg_;
  return result;
}

// String conversion ///////////////////////////////////////////////////////////

template std::string util::bigint::to_basic_string<char,
  std::char_traits<char>, decltype(util::bigint::digits_upper),
  std::string_view&&>(util::bigint const&,
  decltype(bigint::digits_upper)&&, std::string_view&&) const;
template std::wstring util::bigint::to_basic_string<wchar_t,
  std::char_traits<wchar_t>, decltype(util::bigint::digits_upper_w),
  std::wstring_view&&>(util::bigint const&,
  decltype(bigint::digits_upper_w)&&, std::wstring_view&&) const;
template std::u16string util::bigint::to_basic_string<char16_t,
  std::char_traits<char16_t>, decltype(util::bigint::digits_upper_u16),
  std::u16string_view&&>(util::bigint const&,
  decltype(bigint::digits_upper_u16)&&, std::u16string_view&&) const;
template std::u32string util::bigint::to_basic_string<char32_t,
  std::char_traits<char32_t>, decltype(util::bigint::digits_upper_u32),
  std::u32string_view&&>(util::bigint const&,
  decltype(bigint::digits_upper_u32)&&, std::u32string_view&&) const;

template std::ostream& util::operator<<(std::ostream&, util::bigint const&);
template std::wostream& util::operator<<(std::wostream&, util::bigint const&);

template std::istream& util::operator>>(std::istream&, util::bigint&);
template std::wistream& util::operator>>(std::wistream&, util::bigint&);
