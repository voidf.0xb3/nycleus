#include "util/stackful_coro.hpp"
#include "util/overflow.hpp"
#include <cassert>
#include <cstddef>
#include <exception>
#include <limits>
#include <new>
#include <stdexcept>

namespace coro = util::stackful_coro::detail_;

void coro::tail_call_buffer::create_buffer(std::size_t size) {
  assert(!ptr_);

  // This will never be called while suspended, so don't bother preserving the
  // suspended bit.
  assert(!suspended());

  try {
    ptr_ = new std::byte[throwing_add(size, sizeof(buffer_header))];
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }
  new(ptr_) buffer_header{size, nullptr};

  assert(ptr_);
  assert(!suspended());
}

void coro::tail_call_buffer::destroy_buffer() noexcept {
  assert(ptr_);

  // This will never be called while suspended, so don't bother preserving the
  // suspended bit.
  assert(!suspended());

  assert(!get_holder());

  delete[] ptr_.get_ptr();
  ptr_ = nullptr;

  assert(!ptr_);
  assert(!suspended());
}

bool coro::tail_call_buffer::has_callback() const noexcept {
  if(!ptr_) {
    return false;
  }
  if(get_holder()) {
    return true;
  }
  return false;
}

void coro::tail_call_buffer::invoke(util::stackful_coro::context ctx) noexcept {
  assert(has_callback());
  get_holder()->invoke(ctx);
  get_holder()->~tail_call_holder_base();
  get_header().holder_ptr = nullptr;
}

coro::tail_call_buffer::~tail_call_buffer() noexcept {
  assert(!has_callback());
  if(ptr_) {
    destroy_buffer();
  }
}

std::byte* coro::processor_base::new_chunk(std::size_t size) {
  std::byte* const chunk{new std::byte[size]};
  new(chunk) chunk_header{size, nullptr, nullptr, initial_offset};
  return chunk;
}

coro::processor_base::chunk_header*
    coro::processor_base::get_chunk_header(std::byte* chunk) noexcept {
  return std::launder(reinterpret_cast<chunk_header*>(chunk));
}

coro::processor_base::processor_base():
  cur_chunk_{new_chunk(initial_chunk_size)} {}

coro::processor_base::~processor_base() noexcept {
  if(top_frame_) {
    top_frame_.destroy();
  }

  std::byte* chunk{get_chunk_header(cur_chunk_)->next};
  while(chunk) {
    std::byte* old_chunk{chunk};
    chunk = get_chunk_header(chunk)->next;
    delete[] old_chunk;
  }

  chunk = get_chunk_header(cur_chunk_)->prev;
  while(chunk) {
    std::byte* old_chunk{chunk};
    chunk = get_chunk_header(chunk)->prev;
    delete[] old_chunk;
  }

  delete[] cur_chunk_;
}

std::byte* coro::processor_base::push_frame(std::size_t size) {
  // We want to allocate more than the requested size, in order to fit the
  // preserved value of the old offset and to maintain alignment. So we need
  // to add sizeof(std::size_t) and round up to next alignment point.

  try {
    size = throwing_add(size, sizeof(std::size_t));
  } catch(std::overflow_error const&) {
    std::throw_with_nested(std::bad_alloc{});
  }

  if(std::numeric_limits<std::size_t>::max() - size
      < __STDCPP_DEFAULT_NEW_ALIGNMENT__) {
    throw std::bad_alloc{};
  }
  size = (size + __STDCPP_DEFAULT_NEW_ALIGNMENT__ - 1)
    & ~(__STDCPP_DEFAULT_NEW_ALIGNMENT__ - 1);

  if(std::numeric_limits<std::size_t>::max() - initial_offset < size) {
    throw std::bad_alloc{};
  }

  std::size_t offset{get_chunk_header(cur_chunk_)->top_offset};

  assert(offset % __STDCPP_DEFAULT_NEW_ALIGNMENT__ == 0);
  assert(offset >= initial_offset);
  assert(offset <= get_chunk_header(cur_chunk_)->size);

  if(size > get_chunk_header(cur_chunk_)->size - offset) {
    // We're out of space in this chunk.
    std::size_t last_chunk_size{get_chunk_header(cur_chunk_)->size};

    if(get_chunk_header(cur_chunk_)->top_offset == initial_offset) {
      // If this is the first chunk, and it is empty, keep destroying chunks
      // until we find one that is big enough.
      do {
        // Drop one chunk
        assert(!get_chunk_header(cur_chunk_)->prev);
        std::byte* const old_chunk{cur_chunk_};
        cur_chunk_ = get_chunk_header(cur_chunk_)->next;
        delete[] old_chunk;

        if(!cur_chunk_) {
          break;
        }

        // Fix up the new first chunk
        assert(get_chunk_header(cur_chunk_)->top_offset == initial_offset);
        get_chunk_header(cur_chunk_)->prev = nullptr;
        last_chunk_size = get_chunk_header(cur_chunk_)->size;
      } while(size > get_chunk_header(cur_chunk_)->size - initial_offset);

      if(!cur_chunk_) {
        std::size_t new_chunk_size{
          saturating_mul(last_chunk_size, growth_factor)};
        if(size + initial_offset > new_chunk_size) {
          new_chunk_size = size + initial_offset;
        }
        cur_chunk_ = new_chunk(new_chunk_size);
      }
    } else {
      // We're in the middle of the chunk list.

      // In the (extremely unlikely) event that a bunch of further chunks are
      // all too small for our next frame, just drop them entirely.
      while(get_chunk_header(cur_chunk_)->next
          && get_chunk_header(get_chunk_header(cur_chunk_)->next)->size
          - initial_offset < size) {
        std::byte* const next_chunk{get_chunk_header(cur_chunk_)->next};
        assert(get_chunk_header(next_chunk)->top_offset == initial_offset);
        last_chunk_size = get_chunk_header(next_chunk)->size;
        get_chunk_header(cur_chunk_)->next = get_chunk_header(next_chunk)->next;
        delete[] next_chunk;
      }

      if(get_chunk_header(cur_chunk_)->next) {
        // We found a chunk that is big enough, use that
        cur_chunk_ = get_chunk_header(cur_chunk_)->next;
        assert(get_chunk_header(cur_chunk_)->top_offset == initial_offset);
        assert(get_chunk_header(cur_chunk_)->size - initial_offset >= size);
      } else {
        // We need a new chunk
        std::size_t new_chunk_size{
          saturating_mul(last_chunk_size, growth_factor)};
        if(size + initial_offset > new_chunk_size) {
          new_chunk_size = size + initial_offset;
        }

        std::byte* const chunk{new_chunk(new_chunk_size)};
        get_chunk_header(cur_chunk_)->next = chunk;
        get_chunk_header(chunk)->prev = cur_chunk_;
        cur_chunk_ = chunk;
      }
    }

    offset = initial_offset;
  }

  new(cur_chunk_ + offset + size - sizeof(std::size_t)) std::size_t{offset};
  get_chunk_header(cur_chunk_)->top_offset += size;
  frame_allocated_ = true;
  return cur_chunk_ + offset;
}

void coro::processor_base::pop_frame() noexcept {
  std::size_t offset{get_chunk_header(cur_chunk_)->top_offset};

  assert(offset % __STDCPP_DEFAULT_NEW_ALIGNMENT__ == 0);
  assert(offset > initial_offset);
  assert(offset <= get_chunk_header(cur_chunk_)->size);

  offset -= sizeof(std::size_t);
  offset = *std::launder(reinterpret_cast<std::size_t*>(cur_chunk_ + offset));

  assert(offset % __STDCPP_DEFAULT_NEW_ALIGNMENT__ == 0);
  assert(offset >= initial_offset);
  assert(offset < get_chunk_header(cur_chunk_)->top_offset);

  get_chunk_header(cur_chunk_)->top_offset = offset;

  if(offset == initial_offset) {
    std::byte* const prev_chunk{get_chunk_header(cur_chunk_)->prev};
    if(prev_chunk) {
      cur_chunk_ = prev_chunk;
      assert(get_chunk_header(cur_chunk_)->top_offset > initial_offset);
    }
  }
}

void coro::processor_base::loop() noexcept {
  tail_call_.set_suspended(false);
  while(!tail_call_.suspended() && top_frame_) {
    top_frame_.resume();
    if(tail_call_.has_callback()) {
      tail_call_.invoke(context{*this});
    }
  }
}
