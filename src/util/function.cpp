#include "util/function.hpp"
#include <gsl/gsl>

gsl::czstring util::function_target_exception::what() const noexcept {
  return "util::function_target_exception";
}
