#include <boost/test/unit_test.hpp>

#include "util/range_adaptor_closure.hpp"
#include <concepts>
#include <cstdint>
#include <ranges>
#include <type_traits>
#include <utility>
#include <vector>

namespace {

class times_two_t: public util::range_adaptor_closure<times_two_t> {
public:
  template<typename R>
  requires std::ranges::viewable_range<R&&>
    && std::same_as<std::uint8_t, std::ranges::range_value_t<R&&>>
    && std::is_move_constructible_v<std::remove_cvref_t<R>>
  [[nodiscard]] constexpr auto operator()(R&& r) const
      noexcept(std::is_nothrow_move_constructible_v<R>) {
    return std::forward<R>(r)
      | std::views::transform([](std::uint8_t x) noexcept -> std::uint8_t {
        return x * 2;
      });
  }
};

constexpr times_two_t times_two;

class plus_hundred_t: public util::range_adaptor_closure<plus_hundred_t> {
public:
  template<typename R>
  requires std::ranges::viewable_range<R&&>
    && std::same_as<std::uint8_t, std::ranges::range_value_t<R&&>>
    && std::is_move_constructible_v<std::remove_cvref_t<R>>
  [[nodiscard]] constexpr auto operator()(R&& r) const
      noexcept(std::is_nothrow_move_constructible_v<R>) {
    return std::forward<R>(r)
      | std::views::transform([](std::uint8_t x) noexcept -> std::uint8_t {
        return x + 100;
      });
  }
};

constexpr plus_hundred_t plus_hundred;

struct range_adaptor_closure_test_fixture {
  std::vector<std::uint8_t> vec{0, 1, 2};
};

}

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_FIXTURE_TEST_SUITE(range_adaptor_closure,
  range_adaptor_closure_test_fixture)

BOOST_AUTO_TEST_CASE(call_syntax) {
  uint8_t const values[]{0, 2, 4};
  auto result{times_two(vec)};
  BOOST_TEST(result == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pipe_syntax) {
  uint8_t const values[]{0, 2, 4};
  auto result{vec | times_two};
  BOOST_TEST(result == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(chain_syntax) {
  uint8_t const values[]{100, 102, 104};
  auto result{vec | times_two | plus_hundred};
  BOOST_TEST(result == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(chain_object) {
  uint8_t const values[]{100, 102, 104};
  auto result{vec | (times_two | plus_hundred)};
  BOOST_TEST(result == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(chain_object_post) {
  uint8_t const values[]{200, 204, 208};
  auto result{vec | (times_two | plus_hundred) | times_two};
  BOOST_TEST(result == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
