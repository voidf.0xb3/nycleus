#include <boost/test/unit_test.hpp>

#include "util/sentinel_vector.hpp"
#include "single_pass_view.hpp"
#include <algorithm>
#include <cstdint>
#include <stdexcept>
#include <utility>

namespace {

struct sentinel_vector_fixture {
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec{0U, 1U, 2U, 3U};
};

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_FIXTURE_TEST_SUITE(sentinel_vector, sentinel_vector_fixture)

// Iteration ///////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(iteration)

BOOST_AUTO_TEST_CASE(forward) {
  auto it{vec.begin()};
  auto iend{vec.end()};
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == 0U);
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == 1U);
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == 2U);
  --it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == 1U);
  it += 2;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == 3U);
  ++it;
  BOOST_TEST((it == iend));
}

BOOST_AUTO_TEST_CASE(reverse) {
  std::uint64_t arr[]{3U, 2U, 1U, 0U};
  BOOST_TEST(std::equal(vec.rbegin(), vec.rend(),
    std::begin(arr), std::end(arr)));
}

BOOST_AUTO_TEST_CASE(interop) {
  BOOST_TEST((vec.begin() + vec.size() == vec.cend()));
  BOOST_TEST((vec.cbegin() + vec.size() == vec.end()));
}

BOOST_AUTO_TEST_CASE(indexing_read) {
  auto it{vec.begin()};
  BOOST_TEST(it[1] == 1U);
}

BOOST_AUTO_TEST_CASE(indexing_write) {
  auto it{vec.begin()};
  it[1] = 179U;
  std::uint64_t values[]{0U, 179U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // iteration

// Construction ////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(construct)

BOOST_AUTO_TEST_CASE(copy) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U> other{vec};
  BOOST_TEST(vec == other, boost::test_tools::per_element());
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(move) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U> other{std::move(vec)};
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U>
    new_vec{std::begin(values), std::end(values)};
  BOOST_TEST(new_vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_unchecked) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U>
    new_vec{std::begin(values), std::end(values), util::unchecked_t{}};
  BOOST_TEST(new_vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_overlong) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  BOOST_CHECK_THROW((util::sentinel_vector<std::uint64_t, 3, 1000000U>{
    std::begin(values), std::end(values)}), util::sentinel_vector_length_error);
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  auto view{values | tests::single_pass};
  util::sentinel_vector<std::uint64_t, 8, 1000000U> new_vec{
    view.begin(), view.end()};
  BOOST_TEST(new_vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill) {
  util::sentinel_vector<std::uint64_t, 16, 1000000> new_vec(5, 179U);
  std::uint64_t arr[]{179U, 179U, 179U, 179U, 179U};
  BOOST_TEST(new_vec == arr, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // construct

// Swap ////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_CASE(swap) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  std::uint64_t values_other[]{100U, 101U, 102U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U> other{100U, 101U, 102U};
  vec.swap(other);
  BOOST_TEST(other == values, boost::test_tools::per_element());
  BOOST_TEST(vec == values_other, boost::test_tools::per_element());
}

// Assignment //////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(assign)

BOOST_AUTO_TEST_CASE(op_copy) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U> other{};
  other = vec;
  BOOST_TEST(vec == other, boost::test_tools::per_element());
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(self_assign) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  vec = vec;
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_move) {
  std::uint64_t values[4]{0U, 1U, 2U, 3U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U> other{};
  other = std::move(vec);
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_il) {
  vec = {100U, 101U, 102U};
  std::uint64_t arr[]{100U, 101U, 102U};
  BOOST_TEST(vec == arr, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_shrink) {
  std::uint64_t values[]{100U, 101U, 102U};
  vec.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_equal) {
  std::uint64_t values[]{100U, 101U, 102U, 103U};
  vec.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_extend) {
  std::uint64_t values[]{100U, 101U, 102U, 103U, 104U};
  vec.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_unchecked) {
  std::uint64_t values[]{100U, 101U, 102U};
  vec.assign(std::begin(values), std::end(values), util::unchecked_t{});
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_overlong) {
  std::uint64_t values[]{100U, 101U, 102U, 103U, 104U, 105U, 106U, 107U, 108U,
    109U};
  BOOST_CHECK_THROW(vec.assign(std::begin(values), std::end(values)),
    util::sentinel_vector_length_error);
}

BOOST_AUTO_TEST_CASE(il_extend) {
  vec.assign({100U, 101U, 102U, 103U, 104U});
  std::uint64_t values[]{100U, 101U, 102U, 103U, 104U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_extend) {
  vec.assign(5, 100U);
  std::uint64_t values[]{100U, 100U, 100U, 100U, 100U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // assign

// Indexing ////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(indexing)

BOOST_AUTO_TEST_CASE(at_unchecked) {
  BOOST_TEST(vec[2] == 2U);
}

BOOST_AUTO_TEST_CASE(at) {
  BOOST_TEST(vec.at(2) == 2U);
}

BOOST_AUTO_TEST_CASE(at_out_of_range) {
  BOOST_CHECK_THROW(static_cast<void>(vec.at(80)), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(front) {
  BOOST_TEST(vec.front() == 0U);
}

BOOST_AUTO_TEST_CASE(front_assign) {
  vec.front() = 179U;
  std::uint64_t values[]{179U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back) {
  BOOST_TEST(vec.back() == 3U);
}

BOOST_AUTO_TEST_CASE(back_assign) {
  vec.back() = 179U;
  std::uint64_t values[]{0U, 1U, 2U, 179U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // indexing

// Comparison //////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(compare)

BOOST_AUTO_TEST_CASE(equal) {
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec2{0U, 1U, 2U, 3U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec3{100U, 101U, 102U,
    103U};
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec4{101U, 102U, 103U};
  BOOST_TEST(vec == vec2);
  BOOST_TEST(vec != vec3);
  BOOST_TEST(vec != vec4);
}

BOOST_AUTO_TEST_SUITE_END() // compare

// Emplacement /////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(emplace)

BOOST_AUTO_TEST_CASE(into) {
  auto it{vec.emplace(vec.cbegin() + 1, 100U)};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(into_endlineup) {
  auto it{vec.emplace(vec.cend_common() - 1, 100U)};
  BOOST_TEST((it == vec.end_common() - 2));
  std::uint64_t values[]{0U, 1U, 2U, 100U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(into_atend) {
  auto it{vec.emplace(vec.cend(), 100U)};
  BOOST_TEST((it == vec.end_common() - 1));
  std::uint64_t values[]{0U, 1U, 2U, 3U, 100U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back) {
  std::uint64_t& el{vec.emplace_back(100U)};
  BOOST_TEST((&el == &vec.back()));
  std::uint64_t values[]{0U, 1U, 2U, 3U, 100U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(front) {
  std::uint64_t& el{vec.emplace_front(100U)};
  BOOST_TEST((&el == &vec.front()));
  std::uint64_t values[]{100U, 0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // emplace

// Insertion ///////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(insert)

BOOST_AUTO_TEST_CASE(one) {
  auto it{vec.insert(vec.cbegin() + 1, 100U)};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_endlineup) {
  auto it{vec.insert(vec.cend_common() - 1, 100U)};
  BOOST_TEST((it == vec.end_common() - 2));
  std::uint64_t values[]{0U, 1U, 2U, 100U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_atend) {
  auto it{vec.insert(vec.cend(), 100U)};
  BOOST_TEST((it == vec.end_common() - 1));
  std::uint64_t values[]{0U, 1U, 2U, 3U, 100U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_empty) {
  std::uint64_t arr[]{0U};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::begin(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endahead) {
  std::uint64_t arr[]{100U, 101U};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endlineup) {
  std::uint64_t arr[]{100U, 101U, 102U};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endoverlap) {
  std::uint64_t arr[]{100U, 101U, 102U, 103U};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 103U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_unchecked) {
  std::uint64_t arr[]{100U, 101U, 102U};
  auto it{vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr),
    util::unchecked_t{})};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_overlong) {
  std::uint64_t arr[]{100U, 101U, 102U, 103U, 104U, 105U, 106U};
  BOOST_CHECK_THROW(vec.insert(vec.begin() + 1, std::begin(arr), std::end(arr)),
    util::sentinel_vector_length_error);
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  std::uint64_t arr[]{100U, 101U, 102U, 103U};
  auto view{arr | tests::single_pass};
  auto it{vec.insert(vec.begin() + 1, view.begin(), view.end())};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 103U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_endoverlap) {
  auto it{vec.insert(vec.begin() + 1, {100U, 101U, 102U, 103U})};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 103U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_endoverlap) {
  auto it{vec.insert(vec.begin() + 1, 4, 100U)};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 100U, 100U, 100U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(prepend_empty) {
  std::uint64_t arr[]{0U};
  vec.prepend(std::begin(arr), std::begin(arr));
  std::uint64_t values[]{0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(prepend) {
  std::uint64_t arr[]{100U, 101U};
  vec.prepend(std::begin(arr), std::end(arr));
  std::uint64_t values[]{100U, 101U, 0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(prepend_single_pass) {
  std::uint64_t arr[]{100U, 101U, 102U, 103U};
  auto view{arr | tests::single_pass};
  vec.prepend(view.begin(), view.end());
  std::uint64_t values[]{100U, 101U, 102U, 103U, 0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front) {
  vec.push_front(255U);
  std::uint64_t values[]{255U, 0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front_one) {
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec_one{0U};
  vec_one.push_front(255U);
  std::uint64_t values[]{255U, 0U};
  BOOST_TEST(vec_one == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front_empty) {
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec_empty{};
  vec_empty.push_front(255U);
  std::uint64_t values[]{255U};
  BOOST_TEST(vec_empty == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_empty) {
  std::uint64_t arr[]{0U};
  vec.append(std::begin(arr), std::begin(arr));
  std::uint64_t values[]{0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append) {
  std::uint64_t arr[]{100U, 101U};
  vec.append(std::begin(arr), std::end(arr));
  std::uint64_t values[]{0U, 1U, 2U, 3U, 100U, 101U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_single_pass) {
  std::uint64_t arr[]{100U, 101U, 102U, 103U};
  auto view{arr | tests::single_pass};
  vec.append(view.begin(), view.end());
  std::uint64_t values[]{0U, 1U, 2U, 3U, 100U, 101U, 102U, 103U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back) {
  vec.push_back(255U);
  std::uint64_t values[]{0U, 1U, 2U, 3U, 255U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back_empty) {
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec_empty{};
  vec_empty.push_back(255U);
  std::uint64_t values[]{255U};
  BOOST_TEST(vec_empty == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // insert

// Erasure /////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(erase)

BOOST_AUTO_TEST_CASE(one) {
  auto it{vec.erase(vec.cbegin() + 1)};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_atend) {
  auto it{vec.erase(vec.cend_common() - 1)};
  BOOST_TEST((it == vec.end()));
  std::uint64_t values[]{0U, 1U, 2U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endahead) {
  auto it{vec.erase(vec.cbegin() + 1, vec.cbegin() + 3)};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endlineup) {
  auto it{vec.erase(vec.cbegin() + 1, vec.cend())};
  BOOST_TEST((it == vec.end()));
  std::uint64_t values[]{0U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(clear) {
  vec.clear();
  BOOST_TEST(vec.size() == 0);
  BOOST_TEST(vec.empty());
}

BOOST_AUTO_TEST_CASE(clear_empty) {
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec_empty{};
  vec_empty.clear();
  BOOST_TEST(vec_empty.size() == 0);
  BOOST_TEST(vec_empty.empty());
}

BOOST_AUTO_TEST_CASE(before_empty) {
  vec.erase_before(vec.cbegin());
  std::uint64_t values[]{0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(before) {
  vec.erase_before(vec.cbegin() + 2);
  std::uint64_t values[]{2, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pop_front) {
  vec.pop_front();
  std::uint64_t values[]{1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pop_front_one) {
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec_one{0U};
  vec_one.pop_front();
  BOOST_TEST(vec_one.size() == 0);
  BOOST_TEST(vec_one.empty());
}

BOOST_AUTO_TEST_CASE(after_empty) {
  vec.erase_after(vec.cend());
  std::uint64_t values[]{0U, 1U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(after) {
  vec.erase_after(vec.cbegin() + 2);
  std::uint64_t values[]{0U, 1U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pop_back) {
  vec.pop_back();
  std::uint64_t values[]{0U, 1U, 2U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pop_back_one) {
  util::sentinel_vector<std::uint64_t, 8, 1000000U> vec_one{0U};
  vec_one.pop_back();
  BOOST_TEST(vec_one.size() == 0);
  BOOST_TEST(vec_one.empty());
}

BOOST_AUTO_TEST_SUITE_END() // erase

// Resizing ////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(resize)

BOOST_AUTO_TEST_CASE(shrink) {
  vec.resize(2);
  std::uint64_t values[]{0U, 1U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(expand_default) {
  vec.resize(7);
  std::uint64_t values[]{0U, 1U, 2U, 3U, 0U, 0U, 0U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(expand_copy) {
  vec.resize(7, 100U);
  std::uint64_t values[]{0U, 1U, 2U, 3U, 100U, 100U, 100U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // resize

// Replacement /////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(replace)

BOOST_AUTO_TEST_CASE(range_empty) {
  std::uint64_t repl[]{0U};
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 3,
    std::begin(repl), std::begin(repl))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_empty_atend) {
  std::uint64_t repl[]{0U};
  auto it{vec.replace(vec.cbegin() + 2, vec.cend(),
    std::begin(repl), std::begin(repl))};
  BOOST_TEST((it == vec.end()));
  std::uint64_t values[]{0U, 1U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_shrink) {
  std::uint64_t repl[]{100U};
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 3,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_shrink_atend) {
  std::uint64_t repl[]{100U};
  auto it{vec.replace(vec.cbegin() + 2, vec.cend(),
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec.begin() + 2));
  std::uint64_t values[]{0U, 1U, 100U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_equal) {
  std::uint64_t repl[]{100U, 101U};
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 3,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_equal_atend) {
  std::uint64_t repl[]{100U, 101U};
  auto it{vec.replace(vec.cbegin() + 2, vec.cend(),
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec.begin() + 2));
  std::uint64_t values[]{0U, 1U, 100U, 101U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endahead) {
  std::uint64_t repl[]{100U, 101U};
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endlineup) {
  std::uint64_t repl[]{100U, 101U, 102U};
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_endoverlap) {
  std::uint64_t repl[]{100U, 101U, 102U, 103U};
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 103U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_unchecked) {
  std::uint64_t repl[]{100U, 101U, 102U, 103U};
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 2,
    std::begin(repl), std::end(repl), util::unchecked_t{})};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 103U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_overlong) {
  std::uint64_t repl[]{100U, 101U, 102U, 103U, 104U, 105U, 106U, 107U};
  BOOST_CHECK_THROW(vec.replace(vec.cbegin() + 1, vec.cbegin() + 3,
    std::begin(repl), std::end(repl)), util::sentinel_vector_length_error);
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  std::uint64_t repl[]{100U, 101U, 102U, 103U};
  auto view{repl | tests::single_pass};
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 2,
    view.begin(), view.end())};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 103U, 2U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_endoverlap) {
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 3,
    {100U, 101U, 102U, 103U})};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 101U, 102U, 103U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_endoverlap) {
  auto it{vec.replace(vec.cbegin() + 1, vec.cbegin() + 3, 4, 100U)};
  BOOST_TEST((it == vec.begin() + 1));
  std::uint64_t values[]{0U, 100U, 100U, 100U, 100U, 3U};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // replace

BOOST_AUTO_TEST_SUITE_END() // sentinel_vector
BOOST_AUTO_TEST_SUITE_END() // test_util
