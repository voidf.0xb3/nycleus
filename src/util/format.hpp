#ifndef UTIL_FORMAT_HPP_QDFPS6X0UIFQZEY8UUSZIM5W9
#define UTIL_FORMAT_HPP_QDFPS6X0UIFQZEY8UUSZIM5W9

#include "util/fixed_string.hpp"
#include "util/overflow.hpp"
#include "util/nums.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <exception>
#include <iterator>
#include <new>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>

namespace util {

/** @brief A mechanism for customizing the formatting of various types.
 *
 * Given a type `T` (after stripping references and cv-qualifiers) and the
 * format style `S`, this metafunction should produce a formatter type. The
 * formatter type, provided that the argument is formattable with the given
 * style, is expected to contain static member functions called `format` which
 * is called with two arguments: the output iterator and a const lvalue
 * reference to the argument to be formatted. The function is expected to format
 * the argument, write it via the iterator, and return the new iterator value.
 */
template<typename T, fixed_string S>
struct formatter {};
template<typename T, fixed_string S>
using formatter_t = formatter<T, S>::type;

/** @brief The formatter to use if no format style is specified.
 *
 * By default, this falls back to using an empty string as a formatter. */
template<typename T>
struct default_formatter: formatter<T, u8""> {};
template<typename T>
using default_formatter_t = default_formatter<T>::type;

// Format string parsing ///////////////////////////////////////////////////////

namespace detail_ {

/** @brief A formating operation that just produces the given string.
 *
 * Parts of the format string between format codes are represented by these
 * operations. */
template<fixed_string S>
struct format_literal {
  template<std::output_iterator<char8_t> Out>
  static constexpr void format(Out& out, auto&&...)
  noexcept(noexcept(*out++ = char8_t{}))
  {
    for(char8_t const ch: fixed_string_sv<S>) {
      *out++ = ch;
    }
  }
};

/** @brief A formatting operation that formats the argument with the given index
 * with the given style string.
 *
 * Format codes with explicitly specified styles are represented by these
 * operations. */
template<fixed_string S, std::size_t I>
struct format_code {
  template<std::output_iterator<char8_t> Out, typename... Args>
  requires (sizeof...(Args) > I)
  static constexpr void format(Out& out, Args&&... args)
  noexcept(noexcept(out = formatter_t<
    std::remove_cvref_t<std::tuple_element_t<I,
      std::tuple<std::remove_cvref_t<Args> const&...>>>,
    S
  >::format(
    std::move(out),
    std::get<I>(std::tuple<std::remove_cvref_t<Args> const&...>{
      std::forward<Args>(args)...})
  )))
  {
    using tuple_t = std::tuple<std::remove_cvref_t<Args> const&...>;
    out = formatter_t<
      std::remove_cvref_t<std::tuple_element_t<I, tuple_t>>,
      S
    >::format(
      std::move(out),
      std::get<I>(tuple_t{std::forward<Args>(args)...})
    );
  }
};

/** @brief A formatting operation that formats the argument with the given index
 * with the default formatter.
 *
 * Format codes without explicitly specified styles are represented by these
 * operations. */
template<std::size_t I>
struct format_default_code {
  template<std::output_iterator<char8_t> Out, typename... Args>
  requires (sizeof...(Args) > I)
  static constexpr void format(Out& out, Args&&... args)
  noexcept(noexcept(out = default_formatter_t<
    std::remove_cvref_t<std::tuple_element_t<I,
      std::tuple<std::remove_cvref_t<Args> const&...>>>
  >::format(
    std::move(out),
    std::get<I>(std::tuple<std::remove_cvref_t<Args> const&...>{
      std::forward<Args>(args)...})
  )))
  {
    using tuple_t = std::tuple<std::remove_cvref_t<Args> const&...>;
    out = default_formatter_t<
      std::remove_cvref_t<std::tuple_element_t<I, tuple_t>>
    >::format(
      std::move(out),
      std::get<I>(tuple_t{std::forward<Args>(args)...})
    );
  }
};

/** @brief The result of parsing a format string.
 *
 * This is parametrized by a sequence of operations. The operations are executed
 * in sequence to produce the output. */
template<typename... Ops>
struct parsed_format_string {
  template<std::output_iterator<char8_t> Out, typename... Args>
  static constexpr Out format(Out out, Args&&... args)
  noexcept((... && noexcept(Ops::format(out, std::forward<Args>(args)...))))
  {
    (Ops::format(out, std::forward<Args>(args)...), ...);
    return out;
  }
};

/** @brief A compile-time function to compute the sizes of strings returned by
 * `parse_format_string_impl`.
 * @returns - The size of the literal part;
 *          - The size of the style string, or 0 if there is none;
 *          - The size of the rest of the format string, or 0 if there is none.
 */
template<fixed_string S>
[[nodiscard]] consteval std::tuple<
  std::size_t,
  std::size_t,
  std::size_t
> parse_format_string_impl_sizes() noexcept {
  std::size_t input_index{0};
  std::size_t literal_size{0};
  while(input_index < S.size()) {
    if(S[input_index] == u8'{') {
      assert(input_index + 1 < S.size());
      if(S[input_index + 1] == u8'{') {
        ++literal_size;
        input_index += 2;
      } else {
        ++input_index;
        break;
      }
    } else if(S[input_index] == u8'}') {
      assert(input_index + 1 < S.size());
      assert(S[input_index + 1] == u8'}');
      ++literal_size;
      input_index += 2;
    } else {
      ++literal_size;
      ++input_index;
    }
  }

  if(input_index == S.size()) {
    return {literal_size, 0, 0};
  }

  if(S[input_index] != u8':' && S[input_index] != u8'}') {
    while(S[input_index] != u8':' && S[input_index] != u8'}') {
      ++input_index;
      assert(input_index < S.size());
    }
    assert(S[input_index] == u8':' || S[input_index] == u8'}');
  }

  std::size_t code_size{0};
  if(S[input_index] == u8':') {
    ++input_index;
    assert(input_index < S.size());

    for(;;) {
      if(S[input_index] == u8'}') {
        ++input_index;
        break;
      } else {
        ++code_size;
        ++input_index;
      }
    }
  } else {
    ++input_index;
  }

  return {literal_size, code_size, S.size() - input_index};
}

/** @brief A compile-time function to partially parse the given format string.
 * @returns - The literal string from the start of the format string up to the
 *            first format code, or the end of the string if there is none.
 *          - If there are no format codes, an empty optional. Otherwise:
 *            - The argument index specified in the first format code, if any;
 *            - The style string specified in the first format code, if any;
 *            - The rest of the format string after the first format code, to be
 *              passed to another invocation of the format string. */
template<fixed_string S>
[[nodiscard]] consteval std::pair<
  fixed_string<std::get<0>(parse_format_string_impl_sizes<S>())>,
  std::optional<std::tuple<
    std::optional<std::size_t>,
    std::optional<fixed_string<
      std::get<1>(parse_format_string_impl_sizes<S>())>>,
    fixed_string<std::get<2>(parse_format_string_impl_sizes<S>())>
  >>
> parse_format_string_impl() noexcept {
  constexpr auto sizes{parse_format_string_impl_sizes<S>()};
  fixed_string<std::get<0>(sizes)> literal;

  std::size_t input_index{0};
  std::size_t literal_index{0};
  while(input_index < S.size()) {
    if(S[input_index] == u8'{') {
      assert(input_index + 1 < S.size());
      if(S[input_index + 1] == u8'{') {
        assert(literal_index < literal.size());
        literal.buf[literal_index++] = u8'{';
        input_index += 2;
      } else {
        ++input_index;
        break;
      }
    } else if(S[input_index] == u8'}') {
      assert(input_index + 1 < S.size());
      assert(S[input_index + 1] == u8'}');
      assert(literal_index < literal.size());
      literal.buf[literal_index++] = u8'}';
      input_index += 2;
    } else {
      assert(literal_index < literal.size());
      literal.buf[literal_index++] = S[input_index++];
    }
  }
  assert(literal_index == literal.size());

  if(input_index == S.size()) {
    assert(std::get<1>(sizes) == 0);
    assert(std::get<2>(sizes) == 0);
    return {literal, std::nullopt};
  }

  std::optional<std::size_t> arg_index;
  if(S[input_index] != u8':' && S[input_index] != u8'}') {
    arg_index = 0;
    while(S[input_index] != u8':' && S[input_index] != u8'}') {
      assert(u8'0' <= S[input_index]);
      assert(S[input_index] <= u8'9');
      arg_index = util::asserted_add<std::size_t>(
        util::asserted_mul<std::size_t>(*arg_index, 10u),
        static_cast<std::uint8_t>(S[input_index] - u8'0')
      );
      ++input_index;
      assert(input_index < S.size());
    }
    assert(S[input_index] == u8':' || S[input_index] == u8'}');
  }

  std::optional<fixed_string<std::get<1>(sizes)>> code;
  if(S[input_index] == u8':') {
    code.emplace();
    ++input_index;
    assert(input_index < S.size());

    std::size_t code_index{0};
    for(;;) {
      if(S[input_index] == u8'}') {
        ++input_index;
        break;
      } else {
        assert(code_index < code->size());
        code->buf[code_index++] = S[input_index++];
        assert(input_index < S.size());
      }
    }
    assert(code_index == code->size());
  } else {
    ++input_index;
  }

  fixed_string<std::get<2>(sizes)> rest;

  assert(S.size() - input_index == rest.size());
  std::size_t rest_index{0};
  while(input_index < S.size()) {
    rest.buf[rest_index++] = S[input_index++];
  }

  return {literal, std::tuple{arg_index, code, rest}};
}

/** @brief Prepends the given operation to the given parsed format string. */
template<typename Op, typename Parsed>
struct parsed_format_string_prepend;
template<typename Op, typename Parsed>
using parsed_format_string_prepend_t
  = parsed_format_string_prepend<Op, Parsed>::type;
template<typename Op, typename... Ops>
struct parsed_format_string_prepend<Op, parsed_format_string<Ops...>> {
  using type = parsed_format_string<Op, Ops...>;
};

/** @brief Parses the given format string, expecting only format codes with
 * implicitly specified indexes. */
template<fixed_string S, std::size_t NextIndex>
struct parse_format_string_implicit;
template<fixed_string S, std::size_t NextIndex>
using parse_format_string_implicit_t
  = parse_format_string_implicit<S, NextIndex>::type;
template<fixed_string S, std::size_t NextIndex>
requires (!parse_format_string_impl<S>().second.has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string_implicit<S, NextIndex> {
  using type = parsed_format_string<
    format_literal<parse_format_string_impl<S>().first>
  >;
};
template<fixed_string S, std::size_t NextIndex>
requires (!parse_format_string_impl<S>().second.has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string_implicit<S, NextIndex> {
  using type = parsed_format_string<>;
};
template<fixed_string S, std::size_t NextIndex>
requires (parse_format_string_impl<S>().second.has_value())
  && (!std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string_implicit<S, NextIndex> {
  using type = parsed_format_string_prepend_t<
    format_literal<parse_format_string_impl<S>().first>,
    parsed_format_string_prepend_t<
      format_code<*std::get<1>(*parse_format_string_impl<S>().second),
        NextIndex>,
      parse_format_string_implicit_t<std::get<2>(
        *parse_format_string_impl<S>().second), NextIndex + 1>
    >
  >;
};
template<fixed_string S, std::size_t NextIndex>
requires (parse_format_string_impl<S>().second.has_value())
  && (!std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (!std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string_implicit<S, NextIndex> {
  using type = parsed_format_string_prepend_t<
    format_literal<parse_format_string_impl<S>().first>,
    parsed_format_string_prepend_t<
      format_default_code<NextIndex>,
      parse_format_string_implicit_t<std::get<2>(
        *parse_format_string_impl<S>().second), NextIndex + 1>
    >
  >;
};
template<fixed_string S, std::size_t NextIndex>
requires (parse_format_string_impl<S>().second.has_value())
  && (!std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string_implicit<S, NextIndex> {
  using type = parsed_format_string_prepend_t<
    format_code<*std::get<1>(*parse_format_string_impl<S>().second), NextIndex>,
    parse_format_string_implicit_t<std::get<2>(
      *parse_format_string_impl<S>().second), NextIndex + 1>
  >;
};
template<fixed_string S, std::size_t NextIndex>
requires (parse_format_string_impl<S>().second.has_value())
  && (!std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (!std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string_implicit<S, NextIndex> {
  using type = parsed_format_string_prepend_t<
    format_default_code<NextIndex>,
    parse_format_string_implicit_t<std::get<2>(
      *parse_format_string_impl<S>().second), NextIndex + 1>
  >;
};

/** @brief Parses the given format string, expecting only format codes with
 * explicitly specified indexes. */
template<fixed_string S>
struct parse_format_string_explicit;
template<fixed_string S>
using parse_format_string_explicit_t
  = parse_format_string_explicit<S>::type;
template<fixed_string S>
requires (!parse_format_string_impl<S>().second.has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string_explicit<S> {
  using type = parsed_format_string<
    format_literal<parse_format_string_impl<S>().first>
  >;
};
template<fixed_string S>
requires (!parse_format_string_impl<S>().second.has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string_explicit<S> {
  using type = parsed_format_string<>;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string_explicit<S> {
  using type = parsed_format_string_prepend_t<
    format_literal<parse_format_string_impl<S>().first>,
    parsed_format_string_prepend_t<
      format_code<*std::get<1>(*parse_format_string_impl<S>().second),
        *std::get<0>(*parse_format_string_impl<S>().second)>,
      parse_format_string_explicit_t<std::get<2>(
        *parse_format_string_impl<S>().second)>
    >
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (!std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string_explicit<S> {
  using type = parsed_format_string_prepend_t<
    format_literal<parse_format_string_impl<S>().first>,
    parsed_format_string_prepend_t<
      format_default_code<*std::get<0>(*parse_format_string_impl<S>().second)>,
      parse_format_string_explicit_t<std::get<2>(
        *parse_format_string_impl<S>().second)>
    >
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string_explicit<S> {
  using type = parsed_format_string_prepend_t<
    format_code<*std::get<1>(*parse_format_string_impl<S>().second),
      *std::get<0>(*parse_format_string_impl<S>().second)>,
    parse_format_string_explicit_t<std::get<2>(
      *parse_format_string_impl<S>().second)>
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (!std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string_explicit<S> {
  using type = parsed_format_string_prepend_t<
    format_default_code<*std::get<0>(*parse_format_string_impl<S>().second)>,
    parse_format_string_explicit_t<std::get<2>(
      *parse_format_string_impl<S>().second)>
  >;
};

/** @brief Parses the given format string; either implicitly or explicitly
 * specified indexes are allowed. */
template<fixed_string S>
struct parse_format_string;
template<fixed_string S>
using parse_format_string_t = parse_format_string<S>::type;
template<fixed_string S>
requires (!parse_format_string_impl<S>().second.has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string<S> {
  using type = parsed_format_string<
    format_literal<parse_format_string_impl<S>().first>
  >;
};
template<fixed_string S>
requires (!parse_format_string_impl<S>().second.has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string<S> {
  using type = parsed_format_string<>;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (!std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string<S> {
  using type = parsed_format_string_prepend_t<
    format_literal<parse_format_string_impl<S>().first>,
    parsed_format_string_prepend_t<
      format_code<*std::get<1>(*parse_format_string_impl<S>().second), 0>,
      parse_format_string_implicit_t<std::get<2>(
        *parse_format_string_impl<S>().second), 1>
    >
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (!std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (!std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string<S> {
  using type = parsed_format_string_prepend_t<
    format_literal<parse_format_string_impl<S>().first>,
    parsed_format_string_prepend_t<
      format_default_code<0>,
      parse_format_string_implicit_t<std::get<2>(
        *parse_format_string_impl<S>().second), 1>
    >
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (!std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string<S> {
  using type = parsed_format_string_prepend_t<
    format_code<*std::get<1>(*parse_format_string_impl<S>().second), 0>,
    parse_format_string_implicit_t<std::get<2>(
      *parse_format_string_impl<S>().second), 1>
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (!std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (!std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string<S> {
  using type = parsed_format_string_prepend_t<
    format_default_code<0>,
    parse_format_string_implicit_t<std::get<2>(
      *parse_format_string_impl<S>().second), 1>
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string<S> {
  using type = parsed_format_string_prepend_t<
    format_literal<parse_format_string_impl<S>().first>,
    parsed_format_string_prepend_t<
      format_code<*std::get<1>(*parse_format_string_impl<S>().second),
        *std::get<0>(*parse_format_string_impl<S>().second)>,
      parse_format_string_explicit_t<std::get<2>(
        *parse_format_string_impl<S>().second)>
    >
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (!std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() != 0)
struct parse_format_string<S> {
  using type = parsed_format_string_prepend_t<
    format_literal<parse_format_string_impl<S>().first>,
    parsed_format_string_prepend_t<
      format_default_code<*std::get<0>(*parse_format_string_impl<S>().second)>,
      parse_format_string_explicit_t<std::get<2>(
        *parse_format_string_impl<S>().second)>
    >
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string<S> {
  using type = parsed_format_string_prepend_t<
    format_code<*std::get<1>(*parse_format_string_impl<S>().second),
      *std::get<0>(*parse_format_string_impl<S>().second)>,
    parse_format_string_explicit_t<std::get<2>(
      *parse_format_string_impl<S>().second)>
  >;
};
template<fixed_string S>
requires (parse_format_string_impl<S>().second.has_value())
  && (std::get<0>(*parse_format_string_impl<S>().second).has_value())
  && (!std::get<1>(*parse_format_string_impl<S>().second).has_value())
  && (parse_format_string_impl<S>().first.size() == 0)
struct parse_format_string<S> {
  using type = parsed_format_string_prepend_t<
    format_default_code<*std::get<0>(*parse_format_string_impl<S>().second)>,
    parse_format_string_explicit_t<std::get<2>(
      *parse_format_string_impl<S>().second)>
  >;
};

} // detail_

// Public API //////////////////////////////////////////////////////////////////

/** @brief The basic entrypoint for the formatting API. Formats the given
 * arguments according to the given format string and writes them out via the
 * given output iterator.
 *
 * A format string is a sequence of format units. Each format unit is one of:
 * - A UTF-8 code unit that is not '{' or '}'. This outputs the same code unit
 *   to the output.
 * - Two code units: '{{' or '}}'. A duplicated curly brace outputs a single
 *   instance of that same kind of curly brace.
 * - A format code, starting with '{', ending with '}', and containing no curly
 *   braces inside it. It formats and outputs one of the arguments as specified
 *   below.
 *
 * The format code, apart from the curly braces, consists of two parts: the
 * argument index and the format style. Both parts are optional. The argument
 * index is a sequence of one or more Basic Latin digits, comprising a decimal
 * encoding of a 0-based index of an argument to be formatted. The format style
 * consists of a colon followed by a sequence of code units descibing the manner
 * in which the argument is to be formatted.
 *
 * All format codes in a format string must either have indexes or lack indexes.
 * If they have indexes, a format code's index specifies the argument to be
 * formatted. If they lack indexes, the format codes take their arguments in the
 * same order in which the format codes appear in the format string. It is
 * permitted to have arguments that do not printed by any format codes.
 *
 * The exact output produced by a format code is determined based on the
 * argument type, argument value, and the format style (if any). The
 * determination is made using the `formatter` and `default_formatter` class
 * templates. See their documentation for more information. Those class
 * templates can be specialized to customize formatting.
 *
 * This function throws whichever exceptions are thrown by writing through the
 * iterator and whichever exceptions are thrown by the argument formatters. */
template<
  fixed_string S,
  std::output_iterator<char8_t> Out,
  typename... Args
>
constexpr Out format_to(Out out, Args&&... args)
noexcept(noexcept(detail_::parse_format_string_t<S>::format(std::move(out),
  std::forward<Args>(args)...)))
{
  return detail_::parse_format_string_t<S>::format(std::move(out),
    std::forward<Args>(args)...);
}

/** @brief A special output iterator that counts the number of UTF-8 code units
 * fed into it.
 *
 * This is used by `formatted_size`. If the size of the formatted output can be
 * calculated more efficiently without actually producing the text, a formatter
 * can provide an overload for this specific iterator. */
class output_counting_iterator {
private:
  std::size_t size_{0};

  template<fixed_string S, typename... Args>
  friend constexpr std::size_t formatted_size(Args&&...);

  [[nodiscard]] constexpr std::size_t get_size() const noexcept {
    return size_;
  }

public:
  using difference_type = std::ptrdiff_t;

  constexpr output_counting_iterator() noexcept = default;

  /** @brief Increases the counter by the given amount.
   * @throws std::bad_alloc In case of overflow. */
  constexpr void add(std::size_t n) {
    try {
      size_ = util::throwing_add<std::size_t>(size_, n);
    } catch(std::overflow_error const&) {
      std::throw_with_nested(std::bad_alloc{});
    }
  }

  // NOLINTBEGIN(cppcoreguidelines-c-copy-assignment-signature)
  // NOLINTBEGIN(misc-unconventional-assign-operator)
  /** @throws std::bad_alloc In case of overflow. */
  constexpr void operator=(char8_t) { add(1); }
  // NOLINTEND(misc-unconventional-assign-operator)
  // NOLINTEND(cppcoreguidelines-c-copy-assignment-signature)

  constexpr output_counting_iterator& operator*() noexcept { return *this; }

  constexpr output_counting_iterator& operator++() noexcept { return *this; }

  constexpr output_counting_iterator& operator++(int) noexcept { return *this; }
};

/** @brief Calculates the amount of space necessary for the formatted output.
 *
 * This runs the formatter with `output_counting_iterator` as the iterator. If
 * the formatted size of an argument can be computed more efficiently without
 * producing actual text, the formatter can provide an overload for that
 * iterator.
 *
 * This function can also throw whatever is thrown by the argument formatters.
 *
 * @throws std::bad_alloc In case of overflow. */
template<fixed_string S, typename... Args>
constexpr std::size_t formatted_size(Args&&... args) {
  return util::format_to<S>(output_counting_iterator{},
    std::forward<Args>(args)...).get_size();
}

/** @brief Formats the given arguments, returning a string.
 *
 * This first computes the output size, via `formatted_size`, then allocates the
 * memory, then does the actual formatting into that memory.
 *
 * This function can also throw whatever is thrown by the argument formatters.
 *
 * @throws std::bad_alloc
 * @throws std::length_error */
template<fixed_string S, typename... Args>
[[nodiscard]] constexpr std::u8string format(Args&&... args) {
  std::size_t const size{util::formatted_size<S>(std::forward<Args>(args)...)};
  std::u8string result;
  result.reserve(size);
  util::format_to<S>(std::back_inserter(result), std::forward<Args>(args)...);
  assert(result.size() == size);
  return result;
}

// Formatters //////////////////////////////////////////////////////////////////

template<>
struct formatter<char8_t, u8""> {
  struct type {
    static constexpr auto format(
      std::output_iterator<char8_t> auto out,
      char8_t ch
    ) noexcept(noexcept(*out++ = ch)) {
      *out++ = ch;
      return out;
    }
  };
};

template<>
struct formatter<std::u8string_view, u8""> {
  struct type {
    static constexpr auto format(
      std::output_iterator<char8_t> auto out,
      std::u8string_view str
    ) noexcept(noexcept(*out++ = char8_t{})) {
      for(char8_t const ch: str) {
        *out++ = ch;
      }
      return out;
    }

    /** @throws std::bad_alloc */
    static constexpr output_counting_iterator format(
      output_counting_iterator out,
      std::u8string_view str
    ) {
      out.add(str.size());
      return out;
    }
  };
};

template<>
struct formatter<std::u8string, u8"">: formatter<std::u8string_view, u8""> {};

template<>
struct formatter<u8zstring, u8"">: formatter<std::u8string_view, u8""> {};

template<>
struct formatter<cu8zstring, u8"">: formatter<std::u8string_view, u8""> {};

template<std::size_t N>
struct formatter<char8_t[N], u8"">: formatter<std::u8string_view, u8""> {};

template<std::size_t N>
struct formatter<char8_t const[N], u8"">:
  formatter<std::u8string_view, u8""> {};

template<std::integral T>
requires (!std::same_as<T, bool>)
struct formatter<T, u8""> {
  struct type {
    static constexpr auto format(
      std::output_iterator<char8_t> auto out,
      T arg
    ) {
      return std::ranges::copy(to_u8string(arg), std::move(out)).out;
    }

    /** @throws std::bad_alloc */
    static constexpr output_counting_iterator format(
      output_counting_iterator out,
      T arg
    ) {
      out.add(num_decimal_digits(arg));
      return out;
    }
  };
};

// Joining ranges //////////////////////////////////////////////////////////////

/** @brief A special helper type that can be passed to formatting functions to
 * format elements of a range, optionally interspersed with a separator.
 *
 * The format style provided for this argument is used as the format style for
 * formatting each range element. */
template<
  std::ranges::forward_range Range,
  std::ranges::forward_range Separator
>
requires std::same_as<std::ranges::range_value_t<Separator>, char8_t>
struct format_join {
  Range&& range;
  Separator&& separator;

  explicit format_join(
    Range&& r,
    Separator&& s = std::u8string_view{}
  ) noexcept:
    range{std::forward<Range>(r)},
    separator{std::forward<Separator>(s)} {}

  format_join(format_join const& other) noexcept:
    range{std::forward<Range>(other.range)},
    separator{std::forward<Separator>(other.separator)} {}

  format_join& operator=(format_join const&) = delete;

  ~format_join() noexcept = default;
};

template<
  std::ranges::forward_range Range,
  std::ranges::forward_range Separator
>
requires std::same_as<std::ranges::range_value_t<Separator>, char8_t>
format_join(Range&&, Separator&&) -> format_join<Range, Separator>;

template<std::ranges::forward_range Range>
format_join(Range&&) -> format_join<Range, std::u8string_view&&>;

namespace detail_ {

template<
  std::ranges::forward_range Range,
  std::ranges::forward_range Separator,
  typename ElFormatter
>
struct format_join_formatter {
  static constexpr auto format(
    std::output_iterator<char8_t> auto out,
    format_join<Range, Separator> join
  ) noexcept(
    noexcept(*out++ = char8_t{})
    && noexcept(out = ElFormatter::format(std::move(out),
      std::declval<std::ranges::range_reference_t<Range>>()))
  ) {
    bool first{true};
    for(auto&& el: join.range) {
      if(first) {
        first = false;
      } else {
        for(char8_t const ch: join.separator) {
          *out++ = ch;
        }
      }
      out = ElFormatter::format(std::move(out), el);
    }
    return out;
  }
};

} // detail_

template<
  std::ranges::forward_range Range,
  std::ranges::forward_range Separator,
  fixed_string S
>
struct formatter<format_join<Range, Separator>, S> {
  using type = detail_::format_join_formatter<Range, Separator,
    formatter_t<std::ranges::range_value_t<Range>, S>>;
};

template<
  std::ranges::forward_range Range,
  std::ranges::forward_range Separator
>
struct default_formatter<format_join<Range, Separator>> {
  using type = detail_::format_join_formatter<Range, Separator,
    default_formatter_t<std::ranges::range_value_t<Range>>>;
};

} // util

#endif
