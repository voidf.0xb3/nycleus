#include <boost/test/unit_test.hpp>

#include "util/small_vector.hpp"
#include "single_pass_view.hpp"
#include <algorithm>
#include <cstdint>
#include <stdexcept>
#include <utility>

namespace {

struct small_vector_fixture {
  util::small_vector<std::string, 8> vec_short{"0", "1", "2", "3"};
  util::small_vector<std::string, 8> vec_long{"0", "1", "2", "3", "4", "5",
    "6", "7", "8", "9"};

  small_vector_fixture() { vec_long.reserve(16); }
};

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_FIXTURE_TEST_SUITE(small_vector, small_vector_fixture)

// Iteration ///////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(iteration)

BOOST_AUTO_TEST_CASE(forward_short) {
  auto it{vec_short.begin()};
  auto iend{vec_short.end()};
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "0");
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "1");
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "2");
  --it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "1");
  it += 2;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "3");
  ++it;
  BOOST_TEST((it == iend));
}

BOOST_AUTO_TEST_CASE(forward_long) {
  auto it{vec_long.begin()};
  auto iend{vec_long.end()};
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "0");
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "1");
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "2");
  --it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "1");
  it += 2;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(*it == "3");
  ++it;
}

BOOST_AUTO_TEST_CASE(reverse_short) {
  std::string arr[]{"3", "2", "1", "0"};
  BOOST_TEST(std::equal(vec_short.rbegin(), vec_short.rend(),
    std::begin(arr), std::end(arr)));
}

BOOST_AUTO_TEST_CASE(reverse_long) {
  std::string arr[]{"9", "8", "7", "6", "5", "4", "3", "2", "1", "0"};
  BOOST_TEST(std::equal(vec_long.rbegin(), vec_long.rend(),
    std::begin(arr), std::end(arr)));
}

BOOST_AUTO_TEST_CASE(interop) {
  BOOST_TEST((vec_short.begin() + vec_short.size() == vec_short.cend()));
  BOOST_TEST((vec_short.cbegin() + vec_short.size() == vec_short.end()));
  BOOST_TEST((vec_long.begin() + vec_long.size() == vec_long.cend()));
  BOOST_TEST((vec_long.cbegin() + vec_long.size() == vec_long.end()));
}

BOOST_AUTO_TEST_CASE(indexing_read_short) {
  auto it{vec_short.begin()};
  BOOST_TEST(it[1] == "1");
}

BOOST_AUTO_TEST_CASE(indexing_read_long) {
  auto it{vec_long.begin()};
  BOOST_TEST(it[1] == "1");
}

BOOST_AUTO_TEST_CASE(indexing_write_short) {
  auto it{vec_short.begin()};
  it[1] = "179";
  std::string values[]{"0", "179", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(indexing_write_long) {
  auto it{vec_long.begin()};
  it[1] = "179";
  std::string values[]{"0", "179", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // iteration

// Construction ////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(construct)

BOOST_AUTO_TEST_CASE(copy_short) {
  std::string values[]{"0", "1", "2", "3"};
  util::small_vector<std::string, 8> other{vec_short};
  BOOST_TEST(vec_short == other, boost::test_tools::per_element());
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(copy_long) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  util::small_vector<std::string, 8> other{vec_long};
  BOOST_TEST(vec_long == other, boost::test_tools::per_element());
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(move_short) {
  std::string values[]{"0", "1", "2", "3"};
  util::small_vector<std::string, 8> other{std::move(vec_short)};
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(move_long) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  util::small_vector<std::string, 8> other{std::move(vec_long)};
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_short) {
  std::string values[]{"0", "1", "2", "3"};
  util::small_vector<std::string, 8>
    new_vec{std::begin(values), std::end(values)};
  BOOST_TEST(new_vec == values, boost::test_tools::per_element());
  BOOST_TEST(new_vec.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_long) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  util::small_vector<std::string, 8>
    new_vec{std::begin(values), std::end(values)};
  BOOST_TEST(new_vec == values, boost::test_tools::per_element());
  BOOST_TEST(new_vec.capacity() >= 10);
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  std::string values[]{"0", "1", "2", "3"};
  auto view{values | tests::single_pass};
  util::small_vector<std::string, 8> new_vec{view.begin(), view.end()};
  BOOST_TEST(new_vec == values, boost::test_tools::per_element());
  BOOST_TEST(new_vec.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_short) {
  util::small_vector<std::string, 8> new_vec(5, "179");
  std::string arr[]{"179", "179", "179", "179", "179"};
  BOOST_TEST(new_vec == arr, boost::test_tools::per_element());
  BOOST_TEST(new_vec.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_long) {
  util::small_vector<std::string, 8> new_vec(10, "179");
  std::string arr[]{"179", "179", "179", "179", "179", "179", "179", "179",
    "179", "179"};
  BOOST_TEST(new_vec == arr, boost::test_tools::per_element());
  BOOST_TEST(new_vec.capacity() >= 10);
}

BOOST_AUTO_TEST_SUITE_END() // construct

// Swap ////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(swap)

BOOST_AUTO_TEST_CASE(short_short) {
  std::string values[]{"0", "1", "2", "3"};
  std::string values_other[]{"100", "101", "102"};
  util::small_vector<std::string, 8> other{"100", "101", "102"};
  vec_short.swap(other);
  BOOST_TEST(other == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short == values_other, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(short_long) {
  std::string values[]{"0", "1", "2", "3"};
  std::string values_other[]{"100", "101", "102", "103", "104",
    "105", "106", "107", "108", "109"};
  util::small_vector<std::string, 8> other{"100", "101", "102", "103", "104",
    "105", "106", "107", "108", "109"};
  vec_short.swap(other);
  BOOST_TEST(other == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short == values_other, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(long_short) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  std::string values_other[]{"100", "101", "102"};
  util::small_vector<std::string, 8> other{"100", "101", "102"};
  vec_long.swap(other);
  BOOST_TEST(other == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long == values_other, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(long_long) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  std::string values_other[]{"100", "101", "102", "103", "104",
    "105", "106", "107", "108", "109"};
  util::small_vector<std::string, 8> other{"100", "101", "102", "103", "104",
    "105", "106", "107", "108", "109"};
  vec_long.swap(other);
  BOOST_TEST(other == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long == values_other, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // swap

// Assignment //////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(assign)

BOOST_AUTO_TEST_CASE(op_copy) {
  std::string values[]{"0", "1", "2", "3"};
  util::small_vector<std::string, 8> other{};
  other = vec_short;
  BOOST_TEST(vec_short == other, boost::test_tools::per_element());
  BOOST_TEST(other == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(self_assign_short) {
  std::string values[]{"0", "1", "2", "3"};
  vec_short = vec_short;
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(self_assign_long) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  vec_long = vec_long;
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_move_short_short) {
  util::small_vector<std::string, 8> vec{"100", "101", "102"};
  std::string values[]{"0", "1", "2", "3"};
  vec = std::move(vec_short);
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_move_short_long) {
  util::small_vector<std::string, 8> vec{"100", "101", "102"};
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  vec = std::move(vec_long);
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_move_long_short) {
  util::small_vector<std::string, 8> vec{"100", "101", "102", "103", "104",
    "105", "106", "107", "108", "109"};
  std::string values[]{"0", "1", "2", "3"};
  vec = std::move(vec_short);
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_move_long_long) {
  util::small_vector<std::string, 8> vec{"100", "101", "102", "103", "104",
    "105", "106", "107", "108", "109"};
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  vec = std::move(vec_long);
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_il) {
  vec_short = {"100", "101", "102"};
  std::string arr[]{"100", "101", "102"};
  BOOST_TEST(vec_short == arr, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_short_shrink) {
  std::string values[]{"100", "101", "102"};
  vec_short.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_equal) {
  std::string values[]{"100", "101", "102", "103"};
  vec_short.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_extend) {
  std::string values[]{"100", "101", "102", "103", "104"};
  vec_short.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_realloc) {
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109"};
  vec_short.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_long_shrink) {
  auto const old_capacity{vec_long.capacity()};
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108"};
  vec_long.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_equal) {
  auto const old_capacity{vec_long.capacity()};
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109"};
  vec_long.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_extend) {
  auto const old_capacity{vec_long.capacity()};
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109", "110"};
  vec_long.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_realloc) {
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109", "110", "111", "112", "113", "114", "115", "116"};
  vec_long.assign(std::begin(values), std::end(values));
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_short_shrink) {
  vec_short.assign(3, "100");
  std::string values[]{"100", "100", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_short_equal) {
  vec_short.assign(4, "100");
  std::string values[]{"100", "100", "100", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_short_expand) {
  vec_short.assign(5, "100");
  std::string values[]{"100", "100", "100", "100", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_short_realloc) {
  vec_short.assign(10, "100");
  std::string values[]{"100", "100", "100", "100", "100", "100", "100", "100",
    "100", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_long_shrink) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.assign(3, "100");
  std::string values[]{"100", "100", "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(fill_long_equal) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.assign(10, "100");
  std::string values[]{"100", "100", "100", "100", "100", "100", "100", "100",
    "100", "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(fill_long_expand) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.assign(11, "100");
  std::string values[]{"100", "100", "100", "100", "100", "100", "100", "100",
    "100", "100", "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(fill_long_realloc) {
  vec_long.assign(20, "100");
  std::string values[]{"100", "100", "100", "100", "100", "100", "100", "100",
    "100", "100", "100", "100", "100", "100", "100", "100", "100", "100", "100",
    "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_short_shrink) {
  vec_short.assign({"100", "101", "102"});
  std::string values[]{"100", "101", "102"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(il_short_equal) {
  vec_short.assign({"100", "101", "102", "103"});
  std::string values[]{"100", "101", "102", "103"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(il_short_expand) {
  vec_short.assign({"100", "101", "102", "103", "104"});
  std::string values[]{"100", "101", "102", "103", "104"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(il_short_realloc) {
  vec_short.assign({"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109"});
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_long_shrink) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.assign({"100", "101", "102"});
  std::string values[]{"100", "101", "102"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(il_long_equal) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.assign({"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109"});
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(il_long_expand) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.assign({"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109", "110"});
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109", "110"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(il_long_realloc) {
  vec_long.assign({"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118",
    "119"});
  std::string values[]{"100", "101", "102", "103", "104", "105", "106", "107",
    "108", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118",
    "119"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // assign

// Indexing ////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(indexing)

BOOST_AUTO_TEST_CASE(at_unchecked_short) {
  BOOST_TEST(vec_short[2] == "2");
}

BOOST_AUTO_TEST_CASE(at_unchecked_long) {
  BOOST_TEST(vec_long[2] == "2");
}

BOOST_AUTO_TEST_CASE(at_short) {
  BOOST_TEST(vec_short.at(2) == "2");
}

BOOST_AUTO_TEST_CASE(at_long) {
  BOOST_TEST(vec_long.at(2) == "2");
}

BOOST_AUTO_TEST_CASE(at_out_of_range_short) {
  BOOST_CHECK_THROW(static_cast<void>(vec_short.at(80)), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(at_out_of_range_long) {
  BOOST_CHECK_THROW(static_cast<void>(vec_long.at(80)), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(front_short) {
  BOOST_TEST(vec_short.front() == "0");
}

BOOST_AUTO_TEST_CASE(front_long) {
  BOOST_TEST(vec_long.front() == "0");
}

BOOST_AUTO_TEST_CASE(front_assign_short) {
  vec_short.front() = "179";
  std::string values[]{"179", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(front_assign_long) {
  vec_long.front() = "179";
  std::string values[]{"179", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back_short) {
  BOOST_TEST(vec_short.back() == "3");
}

BOOST_AUTO_TEST_CASE(back_long) {
  BOOST_TEST(vec_long.back() == "9");
}

BOOST_AUTO_TEST_CASE(back_assign_short) {
  vec_short.back() = "179";
  std::string values[]{"0", "1", "2", "179"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back_assign_long) {
  vec_long.back() = "179";
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "179"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // indexing

// Capacity management /////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(capacity)

BOOST_AUTO_TEST_CASE(reserve_short) {
  std::string values[]{"0", "1", "2", "3"};
  vec_short.reserve(40);
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() >= 40);
}

BOOST_AUTO_TEST_CASE(reserve_long) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  vec_long.reserve(40);
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() >= 40);
}

BOOST_AUTO_TEST_CASE(reserve_shrink) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  vec_long.reserve(40);
  vec_long.reserve(20);
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() >= 40);
}

BOOST_AUTO_TEST_CASE(reserve_toosmall) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  vec_long.reserve(5);
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() >= 10);
}

BOOST_AUTO_TEST_CASE(reserve_noop) {
  std::string values[]{"0", "1", "2", "3"};
  vec_short.reserve(5);
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(shrink_to_fit_short) {
  std::string values[]{"0", "1", "2", "3"};
  vec_short.reserve(40);
  vec_short.shrink_to_fit();
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(shrink_to_fit_long) {
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  vec_long.reserve(40);
  vec_long.shrink_to_fit();
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(shrink_to_fit_noop) {
  std::string values[]{"0", "1", "2", "3"};
  vec_short.shrink_to_fit();
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_SUITE_END() // capacity

// Comparison //////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(compare)

BOOST_AUTO_TEST_CASE(equal) {
  util::small_vector<std::string, 8> const vec2{"0", "1", "2", "3"};
  util::small_vector<std::string, 8> const vec3{"100", "101", "102", "103"};
  util::small_vector<std::string, 8> const vec4{"101", "102", "103"};
  BOOST_TEST((vec_short == vec2));
  BOOST_TEST((vec_short != vec3));
  BOOST_TEST((vec_short != vec4));
}

BOOST_AUTO_TEST_CASE(lex) {
  util::small_vector<std::uint64_t, 8> const vec_a{0, 1, 2, 3};
  util::small_vector<std::uint64_t, 8> const vec_b{0, 1, 3};
  util::small_vector<std::uint64_t, 8> const vec_c{0, 1, 3, 4};

  BOOST_TEST(!(vec_a < vec_a));
  BOOST_TEST((vec_a < vec_b));
  BOOST_TEST((vec_a < vec_c));
  BOOST_TEST(!(vec_b < vec_a));
  BOOST_TEST(!(vec_b < vec_b));
  BOOST_TEST((vec_b < vec_c));
  BOOST_TEST(!(vec_c < vec_a));
  BOOST_TEST(!(vec_c < vec_b));
  BOOST_TEST(!(vec_c < vec_c));

  BOOST_TEST((vec_a <= vec_a));
  BOOST_TEST((vec_a <= vec_b));
  BOOST_TEST((vec_a <= vec_c));
  BOOST_TEST(!(vec_b <= vec_a));
  BOOST_TEST((vec_b <= vec_b));
  BOOST_TEST((vec_b <= vec_c));
  BOOST_TEST(!(vec_c <= vec_a));
  BOOST_TEST(!(vec_c <= vec_b));
  BOOST_TEST((vec_c <= vec_c));

  BOOST_TEST(!(vec_a > vec_a));
  BOOST_TEST(!(vec_a > vec_b));
  BOOST_TEST(!(vec_a > vec_c));
  BOOST_TEST((vec_b > vec_a));
  BOOST_TEST(!(vec_b > vec_b));
  BOOST_TEST(!(vec_b > vec_c));
  BOOST_TEST((vec_c > vec_a));
  BOOST_TEST((vec_c > vec_b));
  BOOST_TEST(!(vec_c > vec_c));

  BOOST_TEST((vec_a >= vec_a));
  BOOST_TEST(!(vec_a >= vec_b));
  BOOST_TEST(!(vec_a >= vec_c));
  BOOST_TEST((vec_b >= vec_a));
  BOOST_TEST((vec_b >= vec_b));
  BOOST_TEST(!(vec_b >= vec_c));
  BOOST_TEST((vec_c >= vec_a));
  BOOST_TEST((vec_c >= vec_b));
  BOOST_TEST((vec_c >= vec_c));
}

BOOST_AUTO_TEST_SUITE_END() // compare

// Emplacement /////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(emplace)

BOOST_AUTO_TEST_CASE(into_short) {
  auto it{vec_short.emplace(vec_short.cbegin() + 1, "100")};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(into_short_endlineup) {
  auto it{vec_short.emplace(vec_short.cend() - 1, "100")};
  BOOST_TEST((it == vec_short.end() - 2));
  std::string values[]{"0", "1", "2", "100", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(into_short_atend) {
  auto it{vec_short.emplace(vec_short.cend(), "100")};
  BOOST_TEST((it == vec_short.end() - 1));
  std::string values[]{"0", "1", "2", "3", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(into_short_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  auto it{vec.emplace(vec.cbegin() + 1, "100")};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(into_long) {
  auto it{vec_long.emplace(vec_long.cbegin() + 1, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(into_long_endlineup) {
  auto it{vec_long.emplace(vec_long.cend() - 1, "100")};
  BOOST_TEST((it == vec_long.end() - 2));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "100", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(into_long_atend) {
  auto it{vec_long.emplace(vec_long.cend(), "100")};
  BOOST_TEST((it == vec_long.end() - 1));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(into_long_realloc) {
  vec_long.shrink_to_fit();
  auto it{vec_long.emplace(vec_long.cbegin() + 1, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back_short) {
  std::string& el{vec_short.emplace_back("100")};
  BOOST_TEST((&el == &vec_short.back()));
  std::string values[]{"0", "1", "2", "3", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(back_short_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  std::string& el{vec.emplace_back("100")};
  BOOST_TEST((&el == &vec.back()));
  std::string values[]{"0", "1", "2", "3", "100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back_long) {
  std::string& el{vec_long.emplace_back("100")};
  BOOST_TEST((&el == &vec_long.back()));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(back_long_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3", "4", "5"};
  std::string& el{vec.emplace_back("100")};
  BOOST_TEST((&el == &vec.back()));
  std::string values[]{"0", "1", "2", "3", "4", "5", "100"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(front_short) {
  std::string& el{vec_short.emplace_front("100")};
  BOOST_TEST((&el == &vec_short.front()));
  std::string values[]{"100", "0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(front_short_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  std::string& el{vec.emplace_front("100")};
  BOOST_TEST((&el == &vec.front()));
  std::string values[]{"100", "0", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(front_long) {
  std::string& el{vec_long.emplace_front("100")};
  BOOST_TEST((&el == &vec_long.front()));
  std::string values[]{"100", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(front_long_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3", "4", "5"};
  std::string& el{vec.emplace_front("100")};
  BOOST_TEST((&el == &vec.front()));
  std::string values[]{"100", "0", "1", "2", "3", "4", "5"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // emplace

// Insertion ///////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(insert)

BOOST_AUTO_TEST_CASE(one_short) {
  auto it{vec_short.insert(vec_short.cbegin() + 1, "100")};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(one_short_endlineup) {
  auto it{vec_short.insert(vec_short.cend() - 1, "100")};
  BOOST_TEST((it == vec_short.end() - 2));
  std::string values[]{"0", "1", "2", "100", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(one_short_atend) {
  auto it{vec_short.insert(vec_short.cend(), "100")};
  BOOST_TEST((it == vec_short.end() - 1));
  std::string values[]{"0", "1", "2", "3", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(one_short_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  auto it{vec.insert(vec.cbegin() + 1, "100")};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_long) {
  auto it{vec_long.insert(vec_long.cbegin() + 1, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_long_endlineup) {
  auto it{vec_long.insert(vec_long.cend() - 1, "100")};
  BOOST_TEST((it == vec_long.end() - 2));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "100", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_long_atend) {
  auto it{vec_long.insert(vec_long.cend(), "100")};
  BOOST_TEST((it == vec_long.end() - 1));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_long_realloc) {
  vec_long.shrink_to_fit();
  auto it{vec_long.insert(vec_long.cbegin() + 1, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_short_empty) {
  std::string arr[]{"0"};
  auto it{vec_short.insert(vec_short.cbegin() + 1,
    std::begin(arr), std::begin(arr))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_endahead) {
  std::string arr[]{"100", "101"};
  auto it{vec_short.insert(vec_short.cbegin() + 1,
    std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_endlineup) {
  std::string arr[]{"100", "101", "102"};
  auto it{vec_short.insert(vec_short.cbegin() + 1,
    std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_endoverlap) {
  std::string arr[]{"100", "101", "102", "103"};
  auto it{vec_short.insert(vec_short.cbegin() + 1,
    std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  std::string arr[]{"100", "101"};
  auto it{vec.insert(vec.cbegin() + 1, std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec.begin() + 1));
  std::string values[]{"0", "100", "101", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_short_atend) {
  std::string arr[]{"100", "101"};
  auto it{vec_short.insert(vec_short.cend(),
    std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_short.end() - 2));
  std::string values[]{"0", "1", "2", "3", "100", "101"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_long_endahead) {
  std::string arr[]{"100", "101"};
  auto it{vec_long.insert(vec_long.cbegin() + 1,
    std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "1", "2", "3", "4",
    "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_long_endlineup) {
  std::string arr[]{"100", "101", "102"};
  auto it{vec_long.insert(vec_long.cbegin() + 1,
    std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "1", "2", "3", "4",
    "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_long_endoverlap) {
  std::string arr[]{"100", "101", "102", "103"};
  auto it{vec_long.insert(vec_long.cbegin() + 1,
    std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "1", "2", "3", "4",
    "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_long_empty) {
  auto const old_capacity{vec_long.capacity()};
  std::string arr[]{"0"};
  auto it{vec_long.insert(vec_long.cbegin() + 1,
    std::begin(arr), std::begin(arr))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_realloc) {
  vec_long.shrink_to_fit();
  std::string arr[]{"100", "101"};
  auto it{vec_long.insert(vec_long.cbegin() + 1,
    std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "1", "2", "3", "4",
    "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_long_atend) {
  std::string arr[]{"100", "101"};
  auto it{vec_long.insert(vec_long.cend(), std::begin(arr), std::end(arr))};
  BOOST_TEST((it == vec_long.end() - 2));
  std::string values[]{"0", "1", "2", "3", "4",
    "5", "6", "7", "8", "9", "100", "101"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  std::string arr[]{"100", "101", "102"};
  auto view{arr | tests::single_pass};
  auto it{vec_short.insert(vec_short.cbegin() + 1, view.begin(), view.end())};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_short_endahead) {
  auto it{vec_short.insert(vec_short.begin() + 1, 2, "100")};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "100", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_short_endlineup) {
  auto it{vec_short.insert(vec_short.begin() + 1, 3, "100")};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "100", "100", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_short_endoverlap) {
  auto it{vec_short.insert(vec_short.begin() + 1, 4, "100")};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "100", "100", "100", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_short_realloc) {
  auto it{vec_short.insert(vec_short.begin() + 1, 5, "100")};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "100", "100", "100", "100", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_long_endahead) {
  auto it{vec_long.insert(vec_long.begin() + 1, 2, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "100", "1", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_long_endlineup) {
  auto it{vec_long.insert(vec_long.begin() + 1, 3, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "100", "100", "1", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_long_endoverlap) {
  auto it{vec_long.insert(vec_long.begin() + 1, 4, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "100", "100", "100", "1", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_long_realloc) {
  vec_long.shrink_to_fit();
  auto it{vec_long.insert(vec_long.begin() + 1, 5, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "100", "100", "100", "100", "1", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_short_endahead) {
  auto it{vec_short.insert(vec_short.cbegin() + 1, {"100", "101"})};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(il_short_endlineup) {
  auto it{vec_short.insert(vec_short.cbegin() + 1, {"100", "101", "102"})};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(il_short_endoverlap) {
  auto it{vec_short.insert(vec_short.cbegin() + 1,
    {"100", "101", "102", "103"})};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(il_short_realloc) {
  auto it{vec_short.insert(vec_short.cbegin() + 1,
    {"100", "101", "102", "103", "104"})};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "104", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_long_endahead) {
  auto it{vec_long.insert(vec_long.cbegin() + 1, {"100", "101"})};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "1", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_long_endlineup) {
  auto it{vec_long.insert(vec_long.cbegin() + 1, {"100", "101", "102"})};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "1", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_long_endoverlap) {
  auto it{vec_long.insert(vec_long.cbegin() + 1,
    {"100", "101", "102", "103"})};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "1", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(il_long_realloc) {
  auto it{vec_long.insert(vec_long.cbegin() + 1,
    {"100", "101", "102", "103", "104"})};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "104", "1", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front_short) {
  vec_short.push_front("255");
  std::string values[]{"255", "0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(prepend_empty) {
  std::string arr[]{"0"};
  vec_short.prepend(std::begin(arr), std::begin(arr));
  std::string values[]{"0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(prepend_short) {
  std::string arr[]{"100", "101"};
  vec_short.prepend(std::begin(arr), std::end(arr));
  std::string values[]{"100", "101", "0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(prepend_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  std::string arr[]{"100", "101"};
  vec.prepend(std::begin(arr), std::end(arr));
  std::string values[]{"100", "101", "0", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(prepend_long) {
  std::string arr[]{"100", "101"};
  vec_long.prepend(std::begin(arr), std::end(arr));
  std::string values[]{"100", "101", "0", "1", "2", "3", "4",
    "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(prepend_long_realloc) {
  vec_long.shrink_to_fit();
  std::string arr[]{"100", "101"};
  vec_long.prepend(std::begin(arr), std::end(arr));
  std::string values[]{"100", "101", "0", "1", "2", "3", "4",
    "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(prepend_single_pass) {
  std::string arr[]{"100", "101", "102"};
  auto view{arr | tests::single_pass};
  vec_short.prepend(view.begin(), view.end());
  std::string values[]{"100", "101", "102", "0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(push_front_short_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  vec.push_front("255");
  std::string values[]{"255", "0", "1", "2", "3"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front_long) {
  vec_long.push_front("255");
  std::string values[]{"255", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front_long_realloc) {
  vec_long.shrink_to_fit();
  vec_long.push_front("255");
  std::string values[]{"255", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front_one) {
  util::small_vector<std::string, 8> vec{"0"};
  vec.push_front("255");
  std::string values[]{"255", "0"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
  BOOST_TEST(vec.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(push_front_empty) {
  util::small_vector<std::string, 8> vec{};
  vec.push_front("255");
  std::string values[]{"255"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
  BOOST_TEST(vec.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(append_empty) {
  std::string arr[]{"0"};
  vec_short.append(std::begin(arr), std::begin(arr));
  std::string values[]{"0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(append_short) {
  std::string arr[]{"100", "101"};
  vec_short.append(std::begin(arr), std::end(arr));
  std::string values[]{"0", "1", "2", "3", "100", "101"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(append_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  std::string arr[]{"100", "101"};
  vec.append(std::begin(arr), std::end(arr));
  std::string values[]{"0", "1", "2", "3", "100", "101"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_long) {
  std::string arr[]{"100", "101"};
  vec_long.append(std::begin(arr), std::end(arr));
  std::string values[]{"0", "1", "2", "3", "4",
    "5", "6", "7", "8", "9", "100", "101"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_long_realloc) {
  vec_long.shrink_to_fit();
  std::string arr[]{"100", "101"};
  vec_long.append(std::begin(arr), std::end(arr));
  std::string values[]{"0", "1", "2", "3", "4",
    "5", "6", "7", "8", "9", "100", "101"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(append_single_pass) {
  std::string arr[]{"100", "101", "102"};
  auto view{arr | tests::single_pass};
  vec_short.append(view.begin(), view.end());
  std::string values[]{"0", "1", "2", "3", "100", "101", "102"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(push_back_short) {
  vec_short.push_back("255");
  std::string values[]{"0", "1", "2", "3", "255"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(push_back_short_realloc) {
  util::small_vector<std::string, 4> vec{"0", "1", "2", "3"};
  vec.push_back("255");
  std::string values[]{"0", "1", "2", "3", "255"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back_long) {
  vec_long.push_back("255");
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "255"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back_long_realloc) {
  vec_long.shrink_to_fit();
  vec_long.push_back("255");
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "255"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_back_empty) {
  util::small_vector<std::string, 8> vec{};
  vec.push_back("255");
  std::string values[]{"255"};
  BOOST_TEST(vec == values, boost::test_tools::per_element());
  BOOST_TEST(vec.capacity() == 8);
}

BOOST_AUTO_TEST_SUITE_END() // insert

// Erasure /////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(erase)

BOOST_AUTO_TEST_CASE(one_short) {
  auto it{vec_short.erase(vec_short.cbegin() + 1)};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(one_long) {
  auto const old_capacity{vec_long.capacity()};
  auto it{vec_long.erase(vec_long.cbegin() + 1)};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(one_atend_short) {
  auto it{vec_short.erase(vec_short.cend() - 1)};
  BOOST_TEST((it == vec_short.end()));
  std::string values[]{"0", "1", "2"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(one_atend_long) {
  auto const old_capacity{vec_long.capacity()};
  auto it{vec_long.erase(vec_long.cend() - 1)};
  BOOST_TEST((it == vec_long.end()));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_short_endahead) {
  auto it{vec_short.erase(vec_short.cbegin() + 1, vec_short.cbegin() + 3)};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_endlineup) {
  auto it{vec_short.erase(vec_short.cbegin() + 1, vec_short.cend())};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_long_endahead) {
  auto const old_capacity{vec_long.capacity()};
  auto it{vec_long.erase(vec_long.cbegin() + 1, vec_long.cbegin() + 3)};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_endlineup) {
  auto const old_capacity{vec_long.capacity()};
  auto it{vec_long.erase(vec_long.cbegin() + 1, vec_long.cend())};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(clear_short) {
  vec_short.clear();
  BOOST_TEST(vec_short.size() == 0);
  BOOST_TEST(vec_short.empty());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(clear_long) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.clear();
  BOOST_TEST(vec_long.size() == 0);
  BOOST_TEST(vec_long.empty());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(clear_empty) {
  util::small_vector<std::string, 8> vec{};
  vec.clear();
  BOOST_TEST(vec.size() == 0);
  BOOST_TEST(vec.empty());
  BOOST_TEST(vec.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(before_empty) {
  vec_short.erase_before(vec_short.cbegin());
  std::string values[]{"0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(before_short) {
  vec_short.erase_before(vec_short.cbegin() + 2);
  std::string values[]{"2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(before_long) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.erase_before(vec_long.cbegin() + 4);
  std::string values[]{"4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(pop_front_short) {
  vec_short.pop_front();
  std::string values[]{"1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(pop_front_long) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.pop_front();
  std::string values[]{"1", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(pop_front_one) {
  util::small_vector<std::string, 8> vec{"0"};
  vec.pop_front();
  BOOST_TEST(vec.size() == 0);
  BOOST_TEST(vec.empty());
  BOOST_TEST(vec.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(after_empty) {
  vec_short.erase_after(vec_short.cend());
  std::string values[]{"0", "1", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(after_short) {
  vec_short.erase_after(vec_short.cbegin() + 2);
  std::string values[]{"0", "1"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(after_long) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.erase_after(vec_long.cbegin() + 4);
  std::string values[]{"0", "1", "2", "3"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(pop_back_short) {
  vec_short.pop_back();
  std::string values[]{"0", "1", "2"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(pop_back_long) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.pop_back();
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "8"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(pop_back_one) {
  util::small_vector<std::string, 8> vec{"0"};
  vec.pop_back();
  BOOST_TEST(vec.size() == 0);
  BOOST_TEST(vec.empty());
  BOOST_TEST(vec.capacity() == 8);
}

BOOST_AUTO_TEST_SUITE_END() // erase

// Resizing ////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(resize)

BOOST_AUTO_TEST_CASE(short_shrink) {
  vec_short.resize(2);
  std::string values[]{"0", "1"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(short_expand_default) {
  vec_short.resize(7);
  std::string values[]{"0", "1", "2", "3", "", "", ""};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(short_expand_default_realloc) {
  vec_short.resize(9);
  std::string values[]{"0", "1", "2", "3", "", "", "", "", ""};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(short_expand_copy) {
  vec_short.resize(7, "100");
  std::string values[]{"0", "1", "2", "3", "100", "100", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(long_shrink) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.resize(2);
  std::string values[]{"0", "1"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(long_expand_default) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.resize(12);
  std::string values[]{"0", "1", "2", "3", "4", "5",
    "6", "7", "8", "9", "", ""};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(long_expand_default_realloc) {
  vec_long.shrink_to_fit();
  vec_long.resize(12);
  std::string values[]{"0", "1", "2", "3", "4", "5",
    "6", "7", "8", "9", "", ""};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(long_expand_copy) {
  auto const old_capacity{vec_long.capacity()};
  vec_long.resize(12, "100");
  std::string values[]{"0", "1", "2", "3", "4", "5",
    "6", "7", "8", "9", "100", "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_SUITE_END() // resize

// Replacement /////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(replace)

BOOST_AUTO_TEST_CASE(range_short_empty) {
  std::string repl[]{""};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 2,
    std::begin(repl), std::begin(repl))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_empty_atend) {
  std::string repl[]{""};
  auto it{vec_short.replace(vec_short.cbegin() + 3, vec_short.cend(),
    std::begin(repl), std::begin(repl))};
  BOOST_TEST((it == vec_short.begin() + 3));
  std::string values[]{"0", "1", "2"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_shrink) {
  std::string repl[]{"100"};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 3,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_shrink_atend) {
  std::string repl[]{"100"};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cend(),
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_equal) {
  std::string repl[]{"100", "101"};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 3,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_equal_atend) {
  std::string repl[]{"100", "101"};
  auto it{vec_short.replace(vec_short.cbegin() + 2, vec_short.cend(),
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_short.begin() + 2));
  std::string values[]{"0", "1", "100", "101"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_endahead) {
  std::string repl[]{"100", "101"};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_endlineup) {
  std::string repl[]{"100", "101", "102"};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_endoverlap) {
  std::string repl[]{"100", "101", "102", "103"};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(range_short_realloc) {
  std::string repl[]{"100", "101", "102", "103", "104", "105"};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "104", "105", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_long_empty) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{""};
  auto it{vec_long.replace(vec_long.cbegin() + 1, vec_long.cbegin() + 2,
    std::begin(repl), std::begin(repl))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "2", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_empty_atend) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{""};
  auto it{vec_long.replace(vec_long.cbegin() + 3, vec_long.cend(),
    std::begin(repl), std::begin(repl))};
  BOOST_TEST((it == vec_long.begin() + 3));
  std::string values[]{"0", "1", "2"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_shrink) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{"100"};
  auto it{vec_long.replace(vec_long.cbegin() + 1, vec_long.cbegin() + 3,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_shrink_atend) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{"100"};
  auto it{vec_long.replace(vec_long.cbegin() + 1, vec_long.cend(),
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_equal) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{"100", "101"};
  auto it{vec_long.replace(vec_long.cbegin() + 1, vec_long.cbegin() + 3,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "3", "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_equal_atend) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{"100", "101"};
  auto it{vec_long.replace(vec_long.cend() - 2, vec_long.cend(),
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_long.end() - 2));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "7", "100", "101"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_endahead) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{"100", "101"};
  auto it{vec_long.replace(vec_long.cbegin() + 1, vec_long.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "2", "3", "4", "5", "6", "7", "8",
    "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_endlineup) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{"100", "101", "102"};
  auto it{vec_long.replace(vec_long.cbegin() + 7, vec_long.cbegin() + 8,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_long.begin() + 7));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "100", "101", "102",
    "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_endoverlap) {
  auto const old_capacity{vec_long.capacity()};
  std::string repl[]{"100", "101", "102", "103"};
  auto it{vec_long.replace(vec_long.cbegin() + 7, vec_long.cbegin() + 8,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_long.begin() + 7));
  std::string values[]{"0", "1", "2", "3", "4", "5", "6", "100", "101", "102",
    "103", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(range_long_realloc) {
  std::string repl[]{"100", "101", "102", "103", "104", "105"};
  auto it{vec_long.replace(vec_long.cbegin() + 1, vec_long.cbegin() + 2,
    std::begin(repl), std::end(repl))};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "104", "105", "2", "3",
    "4", "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  std::string repl[]{"100", "101", "102", "103", "104", "105"};
  auto view{repl | tests::single_pass};
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 2,
    view.begin(), view.end())};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "102", "103", "104", "105", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fill_short_endahead) {
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 2,
    2, "100")};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "100", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(fill_long_endahead) {
  auto const old_capacity{vec_long.capacity()};
  auto it{vec_long.replace(vec_long.cbegin() + 1, vec_long.cbegin() + 2,
    2, "100")};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "100", "2", "3", "4",
    "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_CASE(il_short_endahead) {
  auto it{vec_short.replace(vec_short.cbegin() + 1, vec_short.cbegin() + 2,
    {"100", "101"})};
  BOOST_TEST((it == vec_short.begin() + 1));
  std::string values[]{"0", "100", "101", "2", "3"};
  BOOST_TEST(vec_short == values, boost::test_tools::per_element());
  BOOST_TEST(vec_short.capacity() == 8);
}

BOOST_AUTO_TEST_CASE(il_long_endahead) {
  auto const old_capacity{vec_long.capacity()};
  auto it{vec_long.replace(vec_long.cbegin() + 1, vec_long.cbegin() + 2,
    {"100", "101"})};
  BOOST_TEST((it == vec_long.begin() + 1));
  std::string values[]{"0", "100", "101", "2", "3", "4",
    "5", "6", "7", "8", "9"};
  BOOST_TEST(vec_long == values, boost::test_tools::per_element());
  BOOST_TEST(vec_long.capacity() == old_capacity);
}

BOOST_AUTO_TEST_SUITE_END() // replace

BOOST_AUTO_TEST_SUITE_END() // small_vector
BOOST_AUTO_TEST_SUITE_END() // test_util
