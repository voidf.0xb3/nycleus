#include "util/fixed_string.hpp"
#include "util/static_trie.hpp"
#include "util/u8compat.hpp"
#include <cstdint>
#include <initializer_list>
#include <ranges>
#include <span>
#include <utility>

using namespace std::string_view_literals;

// Since the static trie is, well, static, all of the following is done at
// compile time and does not require the use of Boost.Test.

namespace {

namespace t = util::static_trie;

// Basic tries /////////////////////////////////////////////////////////////////

enum class test_enum_1 { a, to, tea, ted, ten, i, in, inn, fail };

template<test_enum_1 Result>
struct produce_enum_1 {
  static constexpr test_enum_1 match() noexcept {
    return Result;
  }
};

struct fh_1 {
  static constexpr test_enum_1 fail() noexcept {
    return test_enum_1::fail;
  }
};
static_assert(t::fail_handler<fh_1, void>);

using trie_1 = t::trie_t<void, fh_1,
  t::entry<u8"a", produce_enum_1<test_enum_1::a>>,
  t::entry<u8"to", produce_enum_1<test_enum_1::to>>,
  t::entry<u8"tea", produce_enum_1<test_enum_1::tea>>,
  t::entry<u8"ted", produce_enum_1<test_enum_1::ted>>,
  t::entry<u8"ten", produce_enum_1<test_enum_1::ten>>,
  t::entry<u8"i", produce_enum_1<test_enum_1::i>>,
  t::entry<u8"in", produce_enum_1<test_enum_1::in>>,
  t::entry<u8"inn", produce_enum_1<test_enum_1::inn>>
>;

static_assert(trie_1::lookup(u8"a"sv) == test_enum_1::a);
static_assert(trie_1::lookup(u8"to"sv) == test_enum_1::to);
static_assert(trie_1::lookup(u8"tea"sv) == test_enum_1::tea);
static_assert(trie_1::lookup(u8"ted"sv) == test_enum_1::ted);
static_assert(trie_1::lookup(u8"ten"sv) == test_enum_1::ten);
static_assert(trie_1::lookup(u8"i"sv) == test_enum_1::i);
static_assert(trie_1::lookup(u8"in"sv) == test_enum_1::in);
static_assert(trie_1::lookup(u8"inn"sv) == test_enum_1::inn);

static_assert(trie_1::lookup(u8"toe"sv) == test_enum_1::fail);
static_assert(trie_1::lookup(u8"te"sv) == test_enum_1::fail);
static_assert(trie_1::lookup(u8"tech"sv) == test_enum_1::fail);
static_assert(trie_1::lookup(u8"xyz"sv) == test_enum_1::fail);

// Tries with collapsed nodes //////////////////////////////////////////////////

enum class test_enum_2 { long_string, long_chain, long_sequence, fail };

template<test_enum_2 Result>
struct produce_enum_2 {
  static constexpr test_enum_2 match() noexcept {
    return Result;
  }
};

struct fh_2 {
  static constexpr test_enum_2 fail() noexcept {
    return test_enum_2::fail;
  }
};
static_assert(t::fail_handler<fh_2, void>);

using trie_2 = t::trie_t<void, fh_2,
  t::entry<u8"long_string",
    produce_enum_2<test_enum_2::long_string>>,
  t::entry<u8"long_chain",
    produce_enum_2<test_enum_2::long_chain>>,
  t::entry<u8"long_sequence",
    produce_enum_2<test_enum_2::long_sequence>>
>;

static_assert(trie_2::lookup(u8"long_string"sv) == test_enum_2::long_string);
static_assert(trie_2::lookup(u8"long_chain"sv) == test_enum_2::long_chain);
static_assert(trie_2::lookup(u8"long_sequence"sv)
  == test_enum_2::long_sequence);

static_assert(trie_2::lookup(u8"long_thing"sv) == test_enum_2::fail);
static_assert(trie_2::lookup(u8"long_sleep"sv) == test_enum_2::fail);
static_assert(trie_2::lookup(u8"long_stringy"sv) == test_enum_2::fail);
static_assert(trie_2::lookup(u8"xyz"sv) == test_enum_2::fail);

// Tries with partial matches //////////////////////////////////////////////////

enum class test_enum_3 { apple, ban, banana, bandage, bank, fail };

template<test_enum_3 Result>
struct produce_enum_3 {
  static constexpr test_enum_3 match() noexcept {
    return Result;
  }

  static constexpr test_enum_3 partial_match() noexcept {
    return Result;
  }
};

struct fh_3 {
  static constexpr test_enum_3 fail() noexcept {
    return test_enum_3::fail;
  }
};
static_assert(t::fail_handler<fh_3, void>);

using trie_3 = t::trie_t<void, fh_3,
  t::entry<u8"apple", produce_enum_3<test_enum_3::apple>>,
  t::entry<u8"ban", produce_enum_3<test_enum_3::ban>>,
  t::entry<u8"banana", produce_enum_3<test_enum_3::banana>>,
  t::entry<u8"bandage", produce_enum_3<test_enum_3::bandage>>
>;

static_assert(trie_3::lookup(u8"ba"sv) == test_enum_3::fail);
static_assert(trie_3::lookup(u8"ban"sv) == test_enum_3::ban);
static_assert(trie_3::lookup(u8"bana"sv) == test_enum_3::banana);
static_assert(trie_3::lookup(u8"band"sv) == test_enum_3::bandage);
static_assert(trie_3::lookup(u8"foo"sv) == test_enum_3::fail);

// Tries with non-void state ///////////////////////////////////////////////////

template<std::uint64_t Value>
struct increase_value {
  static constexpr std::uint64_t match(std::uint64_t input) noexcept {
    return input + Value;
  }
};

struct fh_4 {
  static constexpr std::uint64_t fail(std::uint64_t) noexcept {
    return 0;
  }
};
static_assert(t::fail_handler<fh_4, std::uint64_t>);

using trie_4 = t::trie_t<std::uint64_t, fh_4,
  t::entry<u8"one hundred", increase_value<100>>,
  t::entry<u8"two hundred", increase_value<200>>,
  t::entry<u8"three hundred", increase_value<300>>
>;

static_assert(trie_4::lookup(79, u8"what?!"sv) == 0);
static_assert(trie_4::lookup(79, u8"one hundred"sv) == 179);
static_assert(trie_4::lookup(79, u8"two hundred"sv) == 279);
static_assert(trie_4::lookup(79, u8"three hundred"sv) == 379);

// Tries collecting partial match lists ////////////////////////////////////////

template<util::fixed_string Str>
struct produce_str {
  static constexpr std::u8string_view sv{util::fixed_string_buf<Str>};
  static constexpr std::span<std::u8string_view const> match() noexcept {
    return {&sv, 1};
  }
};

struct fh_5 {
  static constexpr std::span<std::u8string_view const> fail(
      std::span<std::u8string_view const> matches) noexcept {
    return matches;
  }
};

using trie_5 = t::trie_t<void, fh_5,
  t::entry<u8"a", produce_str<u8"a">>,
  t::entry<u8"to", produce_str<u8"to">>,
  t::entry<u8"tea", produce_str<u8"tea">>,
  t::entry<u8"ted", produce_str<u8"ted">>,
  t::entry<u8"ten", produce_str<u8"ten">>,
  t::entry<u8"i", produce_str<u8"i">>,
  t::entry<u8"in", produce_str<u8"in">>,
  t::entry<u8"inn", produce_str<u8"inn">>,
  t::entry<u8"banana", produce_str<u8"banana">>
>;

constexpr bool svspan_equal(std::span<std::u8string_view const> a,
    std::initializer_list<std::u8string_view> b) {
  if(a.size() != b.size()) {
    return false;
  }

  for(std::u8string_view const sv_b: b) {
    bool found{false};
    for(std::u8string_view const sv_a: a) {
      if(sv_a == sv_b) {
        found = true;
        break;
      }
    }
    if(!found) {
      return false;
    }
  }

  return true;
}

static_assert(svspan_equal(trie_5::lookup(u8"a"sv), {u8"a"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"to"sv), {u8"to"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"tea"sv), {u8"tea"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"ted"sv), {u8"ted"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"ten"sv), {u8"ten"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"i"sv), {u8"i"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"in"sv), {u8"in"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"inn"sv), {u8"inn"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"banana"sv), {u8"banana"sv}));

static_assert(svspan_equal(trie_5::lookup(u8"toe"sv), {}));
static_assert(svspan_equal(trie_5::lookup(u8"te"sv),
  {u8"tea"sv, u8"ted"sv, u8"ten"sv}));
static_assert(svspan_equal(trie_5::lookup(u8"tech"sv), {}));
static_assert(svspan_equal(trie_5::lookup(u8"xyz"sv), {}));
static_assert(svspan_equal(trie_5::lookup(u8"bana"sv), {u8"banana"sv}));

// Tries with prefix matching //////////////////////////////////////////////////

enum class test_enum_6 { foo, bar, fail };

template<test_enum_6 Result>
struct produce_enum_6 {
  static constexpr test_enum_6 match() noexcept {
    return Result;
  }

  static constexpr test_enum_6 prefix_match(util::cu8zstring) noexcept {
    return Result;
  }
};

struct fh_6 {
  static constexpr test_enum_6 fail() noexcept {
    return test_enum_6::fail;
  }
};
static_assert(t::fail_handler<fh_6, void>);

using trie_6 = t::trie_t<void, fh_6,
  t::entry<u8"foo", produce_enum_6<test_enum_6::foo>>,
  t::entry<u8"bar", produce_enum_6<test_enum_6::bar>>
>;

static_assert(trie_6::lookup(u8"foo"sv) == test_enum_6::foo);
static_assert(trie_6::lookup(u8"bar"sv) == test_enum_6::bar);
static_assert(trie_6::lookup(u8"barracks"sv) == test_enum_6::bar);
static_assert(trie_6::lookup(u8"xyz"sv) == test_enum_6::fail);

} // (anonymous)
