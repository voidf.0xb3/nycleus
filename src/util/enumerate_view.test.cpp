#include <boost/test/unit_test.hpp>

#include "util/enumerate_view.hpp"
#include <algorithm>
#include <concepts>
#include <iterator>
#include <ranges>
#include <string>
#include <vector>

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_AUTO_TEST_SUITE(enumerate_view)

BOOST_AUTO_TEST_CASE(enumerate_view) {
  std::vector<std::string> const strs{"one", "two", "three"};

  auto const view{strs | util::enumerate};
  using view_t = decltype(view);
  static_assert(std::ranges::random_access_range<view_t>);
  using diff_t = std::ranges::range_difference_t<view_t>;
  static_assert(std::same_as<diff_t,
    std::ranges::range_difference_t<std::vector<std::string>>>);
  static_assert(std::same_as<std::ranges::range_value_t<view_t>,
    util::fancy_tuple<diff_t, std::string>>);
  static_assert(std::same_as<std::ranges::range_reference_t<view_t>,
    util::fancy_tuple<diff_t, std::string const&>>);
  static_assert(std::ranges::common_range<view_t>);

  std::vector<util::fancy_tuple<diff_t, std::string>> result;
  std::ranges::copy(view, std::back_inserter(result));

  util::fancy_tuple<diff_t, std::string> test[]{
    {0, "one"}, {1, "two"}, {2, "three"}};
  BOOST_TEST(std::ranges::equal(result, test));
}

BOOST_AUTO_TEST_SUITE_END() // enumerate_view
BOOST_AUTO_TEST_SUITE_END() // test_util
