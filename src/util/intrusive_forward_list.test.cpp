#include <boost/test/unit_test.hpp>

#include "util/intrusive_forward_list.hpp"
#include "single_pass_view.hpp"
#include <gsl/gsl>
#include <algorithm>
#include <cstdint>
#include <ranges>
#include <ostream>

namespace {

struct element: public util::intrusive_forward_list_hook {
  std::uint64_t value;

  element(std::uint64_t value) noexcept: value{value} {}

  [[nodiscard]] bool operator==(element const& other) const noexcept {
    return value == other.value;
  }

};

[[maybe_unused]] std::ostream& operator<<(std::ostream& os, element const& el) {
  return os << '{' << el.value << '}';
}

void dispose(element& el) noexcept {
  delete &el;
}

struct disposer {
  std::uint64_t& counter;

  void operator()(element& el) noexcept {
    ++counter;
    dispose(el);
  }
};

class test_disposer_exception: public std::exception {
public:
  [[nodiscard]] virtual gsl::czstring what() const noexcept override {
    return "test_disposer_exception";
  }
};

struct throwing_disposer {
  std::uint64_t& counter;
  std::uint64_t trigger;

  void operator()(element& el) {
    ++counter;
    dispose(el);

    if(counter >= trigger) {
      throw test_disposer_exception{};
    }
  }
};

struct iflist_fixture {
  util::intrusive_forward_list<element> l;
  util::intrusive_forward_list<element> l2;

  iflist_fixture() {
    try {
      l.push_front(*new element{3});
      l.push_front(*new element{2});
      l.push_front(*new element{1});
      l.push_front(*new element{0});

      l2.push_front(*new element{102});
      l2.push_front(*new element{101});
      l2.push_front(*new element{100});
    } catch(...) {
      l.clear_and_dispose(dispose);
      l2.clear_and_dispose(dispose);
      throw;
    }
  }

  iflist_fixture(iflist_fixture const&) = delete;
  iflist_fixture& operator=(iflist_fixture const&) = delete;

  ~iflist_fixture() noexcept {
    l.clear_and_dispose(dispose);
    l2.clear_and_dispose(dispose);
  }
};

struct erase_iflist_fixture: iflist_fixture {
  element values[4]{0, 1, 2, 3};
  util::intrusive_forward_list<element> l3;

  erase_iflist_fixture() noexcept {
    l3.assign(std::begin(values), std::end(values));
  }
};

} // (anonymous)

BOOST_AUTO_TEST_SUITE(test_util)
BOOST_FIXTURE_TEST_SUITE(intrusive_forward_list, iflist_fixture)

// Iteration ///////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(iteration)

BOOST_AUTO_TEST_CASE(forward) {
  auto it{l.begin()};
  auto iend{l.end()};
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(it->value == 0);
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(it->value == 1);
  ++it;
  BOOST_TEST_REQUIRE((it != iend));
  BOOST_TEST(it->value == 2);
  ++it;
  ++it;
  BOOST_TEST((it == iend));
}

BOOST_AUTO_TEST_CASE(interop) {
  BOOST_TEST((std::next(l.begin(), 4) == l.cend()));
  BOOST_TEST((std::next(l.cbegin(), 4) == l.end()));
}

BOOST_AUTO_TEST_SUITE_END() // iteration

// Construction ////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(construct)

BOOST_AUTO_TEST_CASE(move) {
  element values[]{0, 1, 2, 3};
  util::intrusive_forward_list<element> other{std::move(l)};
  BOOST_TEST(other == values, boost::test_tools::per_element());
  other.clear_and_dispose(dispose);
}

BOOST_AUTO_TEST_CASE(range) {
  element values[]{0, 1, 2, 3};
  util::intrusive_forward_list<element> new_list{std::begin(values),
    std::end(values)};
  BOOST_TEST(new_list == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  element values[]{0, 1, 2, 3};
  auto view{values | tests::single_pass};
  util::intrusive_forward_list<element> new_list{view.begin(), view.end()};
  BOOST_TEST(new_list == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // construct

// Assignment //////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(assign)

BOOST_AUTO_TEST_CASE(swap) {
  element values1[]{0, 1, 2, 3};
  element values2[]{100, 101, 102};
  l.swap(l2);
  BOOST_TEST(l2 == values1, boost::test_tools::per_element());
  BOOST_TEST(l == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(op_move) {
  element values1[]{0, 1, 2, 3};
  element values2[]{100, 101, 102};
  l2 = std::move(l);
  BOOST_TEST(l2 == values1, boost::test_tools::per_element());
  BOOST_TEST(l == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range) {
  util::intrusive_forward_list<element> list;

  element values1[]{0, 1, 2, 3};
  list.assign(std::begin(values1), std::end(values1));
  BOOST_TEST(list == values1, boost::test_tools::per_element());

  element values2[]{100, 101, 102};
  list.assign(std::begin(values2), std::end(values2));
  BOOST_TEST(list == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_single_pass) {
  util::intrusive_forward_list<element> list;

  element values1[]{0, 1, 2, 3};
  auto view1{values1 | tests::single_pass};
  list.assign(view1.begin(), view1.end());
  BOOST_TEST(list == values1, boost::test_tools::per_element());

  element values2[]{100, 101, 102};
  auto view2{values2 | tests::single_pass};
  list.assign(view2.begin(), view2.end());
  BOOST_TEST(list == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(dispose) {
  std::uint64_t counter{0};
  element* values[]{new element{100}, new element{101}, new element{102}};
  auto view{values | std::views::transform(
    [](element* el) noexcept -> element& { return *el; })};
  l.dispose_and_assign(disposer{counter}, std::begin(view), std::end(view));
  BOOST_TEST(l == view, boost::test_tools::per_element());
  BOOST_TEST(counter == 4);
}

BOOST_AUTO_TEST_SUITE_END() // assign

// Element access //////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(access)

BOOST_AUTO_TEST_CASE(front) {
  BOOST_TEST(l.front() == element{0});
}

BOOST_AUTO_TEST_CASE(front_assign) {
  l.front().value = 179;
  element values[]{179, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // access

// Insertion ///////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(insert)

BOOST_AUTO_TEST_CASE(one) {
  auto it{l.insert_after(l.begin(), *new element{179})};
  BOOST_TEST((it == std::next(l.cbegin())));
  element values[]{0, 179, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_at_begin) {
  auto it{l.insert_after(l.before_begin(), *new element{179})};
  BOOST_TEST((it == l.cbegin()));
  element values[]{179, 0, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_at_end) {
  auto it{l.insert_after(std::next(l.begin(), 3), *new element{179})};
  BOOST_TEST((it == std::next(l.begin(), 4)));
  element values[]{0, 1, 2, 3, 179};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_when_empty) {
  element values[]{0};
  util::intrusive_forward_list<element> list;
  list.insert_after(list.before_begin(), values[0]);
  BOOST_TEST(list == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_empty) {
  element arr[]{0};
  auto it{l.insert_after(l.begin(), std::begin(arr), std::begin(arr))};
  BOOST_TEST((it == l.begin()));
  element values[]{0, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range) {
  element* arr[]{new element{100}, new element{101}};
  auto view{arr | std::views::transform(
    [](element* el) noexcept -> element& { return *el; })};
  auto it{l.insert_after(l.begin(), std::begin(view), std::end(view))};
  BOOST_TEST((it == std::next(l.begin(), 2)));
  element values[]{0, 100, 101, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_at_begin) {
  element* arr[]{new element{100}, new element{101}};
  auto view{arr | std::views::transform(
    [](element* el) noexcept -> element& { return *el; })};
  auto it{l.insert_after(l.before_begin(), std::begin(view), std::end(view))};
  BOOST_TEST((it == std::next(l.cbegin())));
  element values[]{100, 101, 0, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_at_end) {
  element* arr[]{new element{100}, new element{101}};
  auto view{arr | std::views::transform(
    [](element* el) noexcept -> element& { return *el; })};
  auto it{l.insert_after(std::next(l.begin(), 3),
    std::begin(view), std::end(view))};
  BOOST_TEST((it == std::next(l.cbegin(), 5)));
  element values[]{0, 1, 2, 3, 100, 101};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_when_empty) {
  element arr[]{100, 101};
  util::intrusive_forward_list<element> list;
  auto it{list.insert_after(list.before_begin(), std::begin(arr),
    std::end(arr))};
  BOOST_TEST((it == std::next(list.cbegin())));
  BOOST_TEST(list == arr, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front) {
  l.push_front(*new element{179});
  element values[]{179, 0, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(push_front_when_empty) {
  element values[]{0};
  util::intrusive_forward_list<element> list;
  list.push_front(values[0]);
  BOOST_TEST(list == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // insert

// Splicing ////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(splice)

BOOST_AUTO_TEST_CASE(simple) {
  l2.splice_after(l2.begin(), l.begin(), std::next(l.begin(), 3));
  element values1[]{0, 3};
  element values2[]{100, 1, 2, 101, 102};
  BOOST_TEST(l == values1, boost::test_tools::per_element());
  BOOST_TEST(l2 == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(from_begin) {
  l2.splice_after(l2.begin(), l.before_begin(), std::next(l.begin(), 3));
  element values1[]{3};
  element values2[]{100, 0, 1, 2, 101, 102};
  BOOST_TEST(l == values1, boost::test_tools::per_element());
  BOOST_TEST(l2 == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(from_end) {
  l2.splice_after(l2.begin(), l.begin(), l.end());
  element values1[]{0};
  element values2[]{100, 1, 2, 3, 101, 102};
  BOOST_TEST(l == values1, boost::test_tools::per_element());
  BOOST_TEST(l2 == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(from_all) {
  l2.splice_after(l2.begin(), l.before_begin(), l.end());
  element values2[]{100, 0, 1, 2, 3, 101, 102};
  BOOST_TEST(l.empty());
  BOOST_TEST(l2 == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(from_empty) {
  l2.splice_after(l2.begin(), l.begin(), std::next(l.begin(), 1));
  element values1[]{0, 1, 2, 3};
  element values2[]{100, 101, 102};
  BOOST_TEST(l == values1, boost::test_tools::per_element());
  BOOST_TEST(l2 == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(to_begin) {
  l2.splice_after(l2.before_begin(), l.begin(), std::next(l.begin(), 3));
  element values1[]{0, 3};
  element values2[]{1, 2, 100, 101, 102};
  BOOST_TEST(l == values1, boost::test_tools::per_element());
  BOOST_TEST(l2 == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(to_end) {
  l2.splice_after(std::next(l2.begin(), 2), l.begin(), std::next(l.begin(), 3));
  element values1[]{0, 3};
  element values2[]{100, 101, 102, 1, 2};
  BOOST_TEST(l == values1, boost::test_tools::per_element());
  BOOST_TEST(l2 == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(self) {
  l.splice_after(l.begin(), std::next(l.begin(), 1), std::next(l.begin(), 3));
  element values[]{0, 2, 1, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(list) {
  l2.splice_after(l2.begin(), l);
  element values2[]{100, 0, 1, 2, 3, 101, 102};
  BOOST_TEST(l.empty());
  BOOST_TEST(l2 == values2, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // splice

// Erasure /////////////////////////////////////////////////////////////////////

BOOST_FIXTURE_TEST_SUITE(erase, erase_iflist_fixture)

BOOST_AUTO_TEST_CASE(one) {
  auto it{l3.erase_after(l3.begin())};
  BOOST_TEST((it == std::next(l3.cbegin())));
  element other_values[]{0, 2, 3};
  BOOST_TEST(l3 == other_values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_at_begin) {
  auto it{l3.erase_after(l3.before_begin())};
  BOOST_TEST((it == l3.cbegin()));
  element other_values[]{1, 2, 3};
  BOOST_TEST(l3 == other_values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_at_end) {
  auto it{l3.erase_after(std::next(l3.begin(), 2))};
  BOOST_TEST((it == l3.cend()));
  element other_values[]{0, 1, 2};
  BOOST_TEST(l3 == other_values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_when_one) {
  element el{100};
  util::intrusive_forward_list<element> list;
  list.push_front(el);

  auto it{list.erase_after(list.before_begin())};
  BOOST_TEST((it == list.cbegin()));

  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
}

BOOST_AUTO_TEST_CASE(range) {
  auto it{l3.erase_after(l3.begin(), std::next(l3.begin(), 3))};
  BOOST_TEST((it == std::next(l3.begin())));
  element other_values[]{0, 3};
  BOOST_TEST(l3 == other_values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_empty) {
  auto it{l3.erase_after(l3.begin(), std::next(l3.begin(), 1))};
  BOOST_TEST((it == std::next(l3.begin())));
  element other_values[]{0, 1, 2, 3};
  BOOST_TEST(l3 == other_values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_at_begin) {
  auto it{l3.erase_after(l3.before_begin(), std::next(l3.begin(), 2))};
  BOOST_TEST((it == l3.begin()));
  element other_values[]{2, 3};
  BOOST_TEST(l3 == other_values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_at_end) {
  auto it{l3.erase_after(std::next(l3.begin(), 1), l3.end())};
  BOOST_TEST((it == l3.end()));
  element other_values[]{0, 1};
  BOOST_TEST(l3 == other_values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(range_all) {
  auto it{l3.erase_after(l3.before_begin(), l3.end())};
  BOOST_TEST((it == l3.end()));
  BOOST_TEST(l3.empty());
  BOOST_TEST((l3.begin() == l3.end()));
}

BOOST_AUTO_TEST_CASE(range_when_empty) {
  util::intrusive_forward_list<element> list;
  auto it{list.erase_after(list.before_begin(), list.end())};
  BOOST_TEST((it == list.end()));
  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
}

BOOST_AUTO_TEST_CASE(clear) {
  l3.clear();
  BOOST_TEST(l3.empty());
  BOOST_TEST((l3.begin() == l3.end()));
}

BOOST_AUTO_TEST_CASE(clear_when_empty) {
  util::intrusive_forward_list<element> list;
  list.clear();
  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
}

BOOST_AUTO_TEST_CASE(pop_front) {
  l3.pop_front();
  element other_values[]{1, 2, 3};
  BOOST_TEST(l3 == other_values, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pop_front_when_one) {
  element el{100};
  util::intrusive_forward_list<element> list;
  list.push_front(el);

  list.pop_front();

  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
}

BOOST_AUTO_TEST_SUITE_END() // erase

// Erasure with disposal ///////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(erase_and_dispose)

BOOST_AUTO_TEST_CASE(one) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(l.begin(), disposer{counter})};
  BOOST_TEST((it == std::next(l.cbegin())));
  element values[]{0, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(one_at_begin) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(l.before_begin(), disposer{counter})};
  BOOST_TEST((it == l.cbegin()));
  element values[]{1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(one_at_end) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(std::next(l.begin(), 2),
    disposer{counter})};
  BOOST_TEST((it == l.cend()));
  element values[]{0, 1, 2};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(one_when_one) {
  std::uint64_t counter{0};

  util::intrusive_forward_list<element> list;
  list.push_front(*new element{100});

  auto it{list.erase_and_dispose_after(list.before_begin(), disposer{counter})};
  BOOST_TEST((it == list.cbegin()));

  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(range) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(l.begin(), std::next(l.begin(), 3),
    disposer{counter})};
  BOOST_TEST((it == std::next(l.begin())));
  element values[]{0, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 2);
}

BOOST_AUTO_TEST_CASE(range_empty) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(l.begin(), std::next(l.begin(), 1),
    disposer{counter})};
  BOOST_TEST((it == std::next(l.begin())));
  element values[]{0, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_CASE(range_at_begin) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(l.before_begin(), std::next(l.begin(), 2),
    disposer{counter})};
  BOOST_TEST((it == l.begin()));
  element values[]{2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 2);
}

BOOST_AUTO_TEST_CASE(range_at_end) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(std::next(l.begin(), 1), l.end(),
    disposer{counter})};
  BOOST_TEST((it == l.end()));
  element values[]{0, 1};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 2);
}

BOOST_AUTO_TEST_CASE(range_all) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(l.before_begin(), l.end(),
    disposer{counter})};
  BOOST_TEST((it == l.end()));
  BOOST_TEST(l.empty());
  BOOST_TEST((l.begin() == l.end()));
  BOOST_TEST(counter == 4);
}

BOOST_AUTO_TEST_CASE(range_when_empty) {
  std::uint64_t counter{0};
  util::intrusive_forward_list<element> list;
  auto it{list.erase_and_dispose_after(list.before_begin(), list.end(),
    disposer{counter})};
  BOOST_TEST((it == list.end()));
  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_CASE(clear) {
  std::uint64_t counter{0};
  l.clear_and_dispose(disposer{counter});
  BOOST_TEST(l.empty());
  BOOST_TEST((l.begin() == l.end()));
  BOOST_TEST(counter == 4);
}

BOOST_AUTO_TEST_CASE(clear_when_empty) {
  std::uint64_t counter{0};
  util::intrusive_forward_list<element> list;
  list.clear_and_dispose(disposer{counter});
  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_CASE(pop_front) {
  std::uint64_t counter{0};
  l.pop_front_and_dispose(disposer{counter});
  element values[]{1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(pop_front_when_one) {
  std::uint64_t counter{0};

  util::intrusive_forward_list<element> list;
  list.push_front(*new element{100});

  list.pop_front_and_dispose(disposer{counter});

  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_SUITE_END() // erase_and_dispose

// Throwing disposers //////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(disposer_throw)

BOOST_AUTO_TEST_CASE(dispose_and_assign) {
  std::uint64_t counter{0};
  element* values[]{new element{100}, new element{101}, new element{102}};
  auto view{values | std::views::transform(
    [](element* el) noexcept -> element& { return *el; })};

  BOOST_CHECK_THROW(l.dispose_and_assign(throwing_disposer{counter, 2},
    std::begin(view), std::end(view)), test_disposer_exception);

  element values1[]{0, 1};
  element values2[]{0, 2};
  element values3[]{0, 3};
  element values4[]{1, 2};
  element values5[]{1, 3};
  element values6[]{2, 3};
  BOOST_TEST((std::ranges::equal(l, values1) || std::ranges::equal(l, values2)
    || std::ranges::equal(l, values3) || std::ranges::equal(l, values4)
    || std::ranges::equal(l, values5) || std::ranges::equal(l, values6)));
  BOOST_TEST(counter == 2);

  for(element* el: values) {
    delete el;
  }
}

BOOST_AUTO_TEST_CASE(one) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.erase_and_dispose_after(l.begin(),
    throwing_disposer{counter, 1}), test_disposer_exception);
  element values[]{0, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(one_at_begin) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.erase_and_dispose_after(l.before_begin(),
    throwing_disposer{counter, 1}), test_disposer_exception);
  element values[]{1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(one_at_end) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.erase_and_dispose_after(std::next(l.begin(), 2),
    throwing_disposer{counter, 1}), test_disposer_exception);
  element values[]{0, 1, 2};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(one_when_one) {
  std::uint64_t counter{0};

  util::intrusive_forward_list<element> list;
  list.push_front(*new element{100});

  BOOST_CHECK_THROW(list.erase_and_dispose_after(list.before_begin(),
    throwing_disposer{counter, 1}), test_disposer_exception);

  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(range) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.erase_and_dispose_after(l.begin(),
    std::next(l.begin(), 3), throwing_disposer{counter, 1}),
    test_disposer_exception);
  element values1[]{0, 2, 3};
  element values2[]{0, 1, 3};
  BOOST_TEST((std::ranges::equal(l, values1)
    || std::ranges::equal(l, values2)));
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(range_empty) {
  std::uint64_t counter{0};
  auto it{l.erase_and_dispose_after(l.begin(), std::next(l.begin(), 1),
    throwing_disposer{counter, 0})};
  BOOST_TEST((it == std::next(l.begin())));
  element values[]{0, 1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_CASE(range_at_begin) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.erase_and_dispose_after(l.before_begin(),
    std::next(l.begin(), 2),
    throwing_disposer{counter, 1}), test_disposer_exception);
  element values1[]{1, 2, 3};
  element values2[]{0, 2, 3};
  BOOST_TEST((std::ranges::equal(l, values1)
    || std::ranges::equal(l, values2)));
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(range_at_end) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.erase_and_dispose_after(std::next(l.begin(), 1), l.end(),
    throwing_disposer{counter, 1}), test_disposer_exception);
  element values1[]{0, 1, 2};
  element values2[]{0, 1, 3};
  BOOST_TEST((std::ranges::equal(l, values1)
    || std::ranges::equal(l, values2)));
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(range_all) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.erase_and_dispose_after(l.before_begin(), l.end(),
    throwing_disposer{counter, 2}), test_disposer_exception);
  element values1[]{0, 1};
  element values2[]{0, 2};
  element values3[]{0, 3};
  element values4[]{1, 2};
  element values5[]{1, 3};
  element values6[]{2, 3};
  BOOST_TEST((std::ranges::equal(l, values1) || std::ranges::equal(l, values2)
    || std::ranges::equal(l, values3) || std::ranges::equal(l, values4)
    || std::ranges::equal(l, values5) || std::ranges::equal(l, values6)));
  BOOST_TEST(counter == 2);
}

BOOST_AUTO_TEST_CASE(range_when_empty) {
  std::uint64_t counter{0};
  util::intrusive_forward_list<element> list;
  auto it{list.erase_and_dispose_after(list.before_begin(), list.end(),
    throwing_disposer{counter, 0})};
  BOOST_TEST((it == list.end()));
  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_CASE(clear) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.clear_and_dispose(throwing_disposer{counter, 2}),
    test_disposer_exception);
  element values1[]{0, 1};
  element values2[]{0, 2};
  element values3[]{0, 3};
  element values4[]{1, 2};
  element values5[]{1, 3};
  element values6[]{2, 3};
  BOOST_TEST((std::ranges::equal(l, values1) || std::ranges::equal(l, values2)
    || std::ranges::equal(l, values3) || std::ranges::equal(l, values4)
    || std::ranges::equal(l, values5) || std::ranges::equal(l, values6)));
  BOOST_TEST(counter == 2);
}

BOOST_AUTO_TEST_CASE(clear_when_empty) {
  std::uint64_t counter{0};
  util::intrusive_forward_list<element> list;
  list.clear_and_dispose(throwing_disposer{counter, 0});
  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
  BOOST_TEST(counter == 0);
}

BOOST_AUTO_TEST_CASE(pop_front) {
  std::uint64_t counter{0};
  BOOST_CHECK_THROW(l.pop_front_and_dispose(throwing_disposer{counter, 1}),
    test_disposer_exception);
  element values[]{1, 2, 3};
  BOOST_TEST(l == values, boost::test_tools::per_element());
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_CASE(pop_front_when_one) {
  std::uint64_t counter{0};

  util::intrusive_forward_list<element> list;
  list.push_front(*new element{100});

  BOOST_CHECK_THROW(list.pop_front_and_dispose(throwing_disposer{counter, 1}),
    test_disposer_exception);

  BOOST_TEST(list.empty());
  BOOST_TEST((list.begin() == list.end()));
  BOOST_TEST(counter == 1);
}

BOOST_AUTO_TEST_SUITE_END() // disposer_throw

BOOST_AUTO_TEST_SUITE_END() // intrusive_forward_list
BOOST_AUTO_TEST_SUITE_END() // test_util
