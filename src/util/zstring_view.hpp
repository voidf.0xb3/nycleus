#ifndef UTIL_ZSTRING_VIEW_HPP_INCLUDED_LZ0ZXSU175FRDE366RUH5GHBI
#define UTIL_ZSTRING_VIEW_HPP_INCLUDED_LZ0ZXSU175FRDE366RUH5GHBI

#include "util/type_traits.hpp"
#include <gsl/gsl>
#include <cstddef>
#include <ranges>

namespace util {

/** @brief A character type that can be used with a zstring_view. */
template<typename T>
concept zstring_view_char
  = util::one_of<T, char, wchar_t, char8_t, char16_t, char32_t>;

/** @brief A utility to present a C-style zero-terminated string as a C++ view,
 * for use with range APIs. */
template<zstring_view_char Char>
class basic_zstring_view:
    public std::ranges::view_interface<basic_zstring_view<Char>> {
private:
  gsl::basic_zstring<Char const> str_;

  struct sentinel {
    [[nodiscard]] friend constexpr bool operator==(
      gsl::basic_zstring<Char const> it,
      sentinel
    ) noexcept {
      return *it == 0;
    }
  };

public:
  constexpr basic_zstring_view() = default;

  constexpr explicit basic_zstring_view(gsl::basic_zstring<Char const> str)
    noexcept: str_{str} {}

  [[nodiscard]] constexpr gsl::basic_zstring<Char const> begin() const
      noexcept {
    return str_;
  }

  [[nodiscard]] constexpr sentinel end() const noexcept {
    return {};
  }
};

template<zstring_view_char Char, std::size_t N>
basic_zstring_view(Char const (&)[N]) -> basic_zstring_view<Char>;

using zstring_view = basic_zstring_view<char>;
using wzstring_view = basic_zstring_view<wchar_t>;
using u8zstring_view = basic_zstring_view<char8_t>;
using u16zstring_view = basic_zstring_view<char16_t>;
using u32zstring_view = basic_zstring_view<char32_t>;

} // util

#endif
