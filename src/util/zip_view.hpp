#ifndef UTIL_ZIP_VIEW_HPP_INCLUDED_0YVL0AXB0IW0JS2R0WJML7BXJ
#define UTIL_ZIP_VIEW_HPP_INCLUDED_0YVL0AXB0IW0JS2R0WJML7BXJ

#include "util/fancy_tuple.hpp"
#include "util/iterator_facade.hpp"
#include "util/type_traits.hpp"
#include <algorithm>
#include <concepts>
#include <initializer_list>
#include <iterator>
#include <ranges>
#include <tuple>
#include <type_traits>
#include <utility>

namespace util {

template<std::ranges::input_range... Views>
requires (... && std::ranges::view<Views>) && (sizeof...(Views) > 0)
class zip_view;

/** @brief An implementation of std::zip_view from C++23.
 *
 * Unlike the C++23 version, this uses fancy_tuple as the value and reference
 * type instead of std::tuple, since this view relies on the
 * std::basic_common_reference and certain constructors that exist only in
 * C++23. */
template<std::ranges::input_range... Views>
requires (... && std::ranges::view<Views>) && (sizeof...(Views) > 0)
class zip_view: public std::ranges::view_interface<zip_view<Views...>> {
private:
  std::tuple<Views...> views_;

  static constexpr bool needs_non_const_ops{(...
    || (!std::ranges::range<Views const>
    || !std::same_as<std::ranges::iterator_t<Views>,
    std::ranges::iterator_t<Views const>>
    || !std::same_as<std::ranges::sentinel_t<Views>,
    std::ranges::sentinel_t<Views const>>))};

  template<bool Const>
  class sentinel;

  template<bool Const>
  class iterator_core {
  private:
    std::tuple<std::ranges::iterator_t<
      maybe_const<Const, Views>>...> its_;

    template<bool C>
    friend class iterator_core;

    template<bool C>
    friend class zip_view::sentinel;

    friend class zip_view;
    explicit constexpr iterator_core(
      std::ranges::iterator_t<maybe_const<Const, Views>>... its
    ) noexcept(
      (... && std::is_nothrow_move_constructible_v<
        std::ranges::iterator_t<maybe_const<Const, Views>>>)
    ): its_{std::move(its)...} {}

  public:
    constexpr iterator_core() = default;

    constexpr iterator_core(iterator_core<!Const> const& other)
      noexcept((... && (std::is_nothrow_convertible_v<
      std::ranges::iterator_t<Views>, std::ranges::iterator_t<Views const>>
      && std::is_nothrow_move_constructible_v<
      std::ranges::iterator_t<Views const>>)))
      requires (Const && ...
      && std::convertible_to<std::ranges::iterator_t<Views>,
      std::ranges::iterator_t<Views const>>):
      its_{std::apply([](std::ranges::iterator_t<Views> const&... other_its)
          constexpr noexcept((...
          && (std::is_nothrow_convertible_v<std::ranges::iterator_t<Views>,
          std::ranges::iterator_t<Views const>>
          && std::is_nothrow_move_constructible_v<
          std::ranges::iterator_t<Views const>>))) {
        return std::tuple<std::ranges::iterator_t<Views const>...>{
          other_its...};
      }, other.its_)} {}

  protected:
    using value_type = fancy_tuple<std::ranges::range_value_t<
      maybe_const<Const, Views>>...>;

    using difference_type = std::common_type_t<
      std::ranges::range_difference_t<maybe_const<Const, Views>>...>;

    static constexpr bool is_single_pass{!(...
      && std::ranges::forward_range<maybe_const<Const, Views>>)};

    [[nodiscard]] constexpr auto dereference() const noexcept((...
        && (noexcept(*std::declval<std::ranges::iterator_t<
        maybe_const<Const, Views>> const&>())
        && noexcept(std::is_nothrow_move_constructible_v<
        std::ranges::range_reference_t<
        maybe_const<Const, Views>>>)))) {
      return std::apply([](std::ranges::iterator_t<
          maybe_const<Const, Views>> const&... its)
          constexpr noexcept((... && (noexcept(*its)
          && noexcept(std::is_nothrow_move_constructible_v<
          std::ranges::range_reference_t<
          maybe_const<Const, Views>>>)))) {
        return fancy_tuple<std::ranges::range_reference_t<
          maybe_const<Const, Views>>...>{*its...};
      }, its_);
    }

    constexpr void increment() noexcept((...
        && noexcept(++std::declval<std::ranges::iterator_t<
        maybe_const<Const, Views>>&>()))) {
      std::apply([](std::ranges::iterator_t<
          maybe_const<Const, Views>>&... its)
          constexpr noexcept((... && noexcept(++its))) {
        (static_cast<void>(++its), ...);
      }, its_);
    }

    [[nodiscard]] constexpr bool equal_to(iterator_core const& other) const
        noexcept((... && is_nothrow_equality_comparable_v<
          std::ranges::iterator_t<maybe_const<Const, Views>>>))
        requires (... && std::equality_comparable<std::ranges::iterator_t<
          maybe_const<Const, Views>>>) {
      if constexpr((... && std::ranges::bidirectional_range<
          maybe_const<Const, Views>>)) {
        return its_ == other.its_;
      } else {
        return std::apply([&](std::ranges::iterator_t<
            maybe_const<Const, Views>> const&... its)
            constexpr noexcept((... && is_nothrow_equality_comparable_v<
            std::ranges::iterator_t<maybe_const<Const, Views>>>)) {
          return std::apply([&](std::ranges::iterator_t<
              maybe_const<Const, Views>> const&... other_its)
              constexpr noexcept((... && is_nothrow_equality_comparable_v<
              std::ranges::iterator_t<maybe_const<Const, Views>>>)) {
            return (... || (its == other_its));
          }, other.its_);
        }, its_);
      }
    }

    constexpr void decrement() noexcept((...
        && noexcept(--std::declval<std::ranges::iterator_t<
        maybe_const<Const, Views>>&>())))
        requires (... && std::ranges::bidirectional_range<
        maybe_const<Const, Views>>) {
      std::apply([](std::ranges::iterator_t<
          maybe_const<Const, Views>>&... its)
          constexpr noexcept((... && noexcept(--its))) {
        (static_cast<void>(--its), ...);
      }, its_);
    }

    [[nodiscard]] constexpr difference_type distance_to(
      iterator_core const& other
    ) const noexcept((... && noexcept(
      std::declval<std::ranges::iterator_t<
      maybe_const<Const, Views>>>()
      - std::declval<std::ranges::iterator_t<
      maybe_const<Const, Views>>>()
    )))
    requires (... && std::sized_sentinel_for<
      std::ranges::iterator_t<maybe_const<Const, Views>>,
      std::ranges::iterator_t<maybe_const<Const, Views>>>)
    {
      return std::apply([&](std::ranges::iterator_t<
          maybe_const<Const, Views>> const&... its)
          constexpr noexcept((... && noexcept(its - its))) {
        return std::apply([&](std::ranges::iterator_t<
            maybe_const<Const, Views>> const&... other_its)
            constexpr noexcept((... && noexcept(other_its - its))) {
          // We return the difference with the smallest absolute value. We store
          // absolute values with the minus sign, because the negative half of
          // the two's complement space fits one extra value.
          std::initializer_list<difference_type> diffs{other_its - its...};
          difference_type result{*diffs.begin()};
          difference_type neg_result{result > 0 ? -result : result};
          for(auto const diff: diffs | std::views::drop(1)) {
            auto const neg_diff{diff > 0 ? -diff : diff};
            if(neg_diff > neg_result) {
              result = diff;
              neg_result = neg_diff;
            }
          }
          return result;
        }, other.its_);
      }, its_);
    }

    constexpr void advance(difference_type offset) noexcept((... && noexcept(
        std::declval<std::ranges::iterator_t<
        maybe_const<Const, Views>>&>() += offset)))
        requires (... && std::ranges::random_access_range<
        maybe_const<Const, Views>>) {
      std::apply([&](std::ranges::iterator_t<
          maybe_const<Const, Views>>&... its) constexpr
          noexcept((... && noexcept(its += offset))) {
        (static_cast<void>(its += offset), ...);
      }, its_);
    }

    [[nodiscard]] constexpr auto at(difference_type offset) const noexcept((...
        && (noexcept(std::declval<std::ranges::iterator_t<
        maybe_const<Const, Views>> const&>()[offset])
        && noexcept(std::is_nothrow_move_constructible_v<
        std::ranges::range_reference_t<
        maybe_const<Const, Views>>>)))) {
      return std::apply([&](std::ranges::iterator_t<
          maybe_const<Const, Views>> const&... its)
          constexpr noexcept((... && (noexcept(its[offset])
          && noexcept(std::is_nothrow_move_constructible_v<
          std::ranges::range_reference_t<
          maybe_const<Const, Views>>>)))) {
        return fancy_tuple<std::ranges::range_reference_t<
          maybe_const<Const, Views>>...>{its[offset]...};
      }, its_);
    }

    [[nodiscard]] friend constexpr auto iter_move(iterator_core const& it)
        noexcept((... && (noexcept(std::ranges::iter_move(std::declval<
        std::ranges::iterator_t<maybe_const<Const, Views>>>()))
        && std::is_nothrow_move_constructible_v<
        std::ranges::range_rvalue_reference_t<
        maybe_const<Const, Views>>>))) {
      return std::apply([](std::ranges::iterator_t<
          maybe_const<Const, Views>> const&... its) constexpr
          noexcept((... && (noexcept(std::ranges::iter_move(its))
          && std::is_nothrow_move_constructible_v<
          std::ranges::range_rvalue_reference_t<
          maybe_const<Const, Views>>>))) {
        return fancy_tuple<std::ranges::range_rvalue_reference_t<
          maybe_const<Const, Views>>...>{
          std::ranges::iter_move(its)...};
      }, it.its_);
    }

    friend constexpr void iter_swap(
      iterator_core const& a,
      iterator_core const& b
    )
    noexcept((... && noexcept(std::ranges::iter_swap(
      std::declval<std::ranges::iterator_t<
      maybe_const<Const, Views> const&>>(),
      std::declval<std::ranges::iterator_t<
      maybe_const<Const, Views> const&>>()))))
    requires (... && std::indirectly_swappable<
      std::ranges::iterator_t<maybe_const<Const, Views>>>)
    {
      return std::apply([&](std::ranges::iterator_t<
          maybe_const<Const, Views>> const&... a_its) constexpr
          noexcept((... && noexcept(std::ranges::iter_swap(
          std::declval<std::ranges::iterator_t<
          maybe_const<Const, Views> const&>>(),
          std::declval<std::ranges::iterator_t<
          maybe_const<Const, Views> const&>>())))) {
        return std::apply([&](std::ranges::iterator_t<
            maybe_const<Const, Views>> const&... b_its) constexpr
            noexcept((... && noexcept(std::ranges::iter_swap(a_its, b_its)))) {
          (static_cast<void>(std::ranges::iter_swap(a_its, b_its)), ...);
        }, b.its_);
      }, a.its_);
    }
  };

  template<bool Const>
  using iterator = util::iterator_facade<iterator_core<Const>>;

  template<bool Const>
  class sentinel {
  private:
    std::tuple<std::ranges::sentinel_t<
      maybe_const<Const, Views>>...> iends_;

    friend class zip_view;
    explicit sentinel(
      std::ranges::sentinel_t<maybe_const<Const, Views>>... iends)
      noexcept((... && std::is_nothrow_move_constructible_v<
      std::ranges::sentinel_t<maybe_const<Const, Views>>>)):
      iends_{std::move(iends)...} {}

  public:
    sentinel() = default;

    constexpr sentinel(sentinel<false> const& other)
      noexcept((... && (std::is_nothrow_convertible_v<
      std::ranges::sentinel_t<Views>, std::ranges::sentinel_t<Views const>>
      && std::is_nothrow_move_constructible_v<
      std::ranges::sentinel_t<Views const>>)))
      requires (Const && ...
      && std::convertible_to<std::ranges::sentinel_t<Views>,
      std::ranges::sentinel_t<Views const>>):
      iends_{std::apply([](std::ranges::sentinel_t<Views> const&... other_iends)
          constexpr noexcept((...
          && (std::is_nothrow_convertible_v<std::ranges::sentinel_t<Views>,
          std::ranges::sentinel_t<Views const>>
          && std::is_nothrow_move_constructible_v<
          std::ranges::sentinel_t<Views const>>))) {
        return std::tuple<std::ranges::sentinel_t<Views const>...>{other_iends...};
      }, other.iends_)} {}
  };

  template<bool ItConst, bool SentConst>
  [[nodiscard]] static constexpr bool equal(
    iterator<ItConst> const& a,
    sentinel<SentConst> const& b
  ) noexcept((... && noexcept(std::declval<std::ranges::iterator_t<
      maybe_const<ItConst, Views>> const&>() == std::declval<
      std::ranges::sentinel_t<maybe_const<ItConst, Views>> const&>())))
      requires (... && std::sentinel_for<std::ranges::sentinel_t<
      maybe_const<ItConst, Views>>, std::ranges::iterator_t<
      maybe_const<ItConst, Views>>>) {
    return std::apply([&](std::ranges::iterator_t<
        maybe_const<ItConst, Views>>
        const&... its) constexpr noexcept((... && noexcept(std::declval<
        std::ranges::iterator_t<maybe_const<ItConst, Views>> const&>()
        == std::declval<std::ranges::sentinel_t<
        maybe_const<ItConst, Views>> const&>()))) {
      return std::apply([&](std::ranges::sentinel_t<
          maybe_const<SentConst, Views>>
          const&... iends) constexpr noexcept((... && noexcept(its == iends))) {
        return (... || (its == iends));
      }, b.iends_);
    }, a.its_);
  }

  template<bool ItConst, bool SentConst>
  [[nodiscard]] friend constexpr bool operator==(
    iterator<ItConst> const& a,
    sentinel<SentConst> const& b
  ) noexcept(noexcept(zip_view::equal<ItConst, SentConst>(a, b)))
      requires requires { zip_view::equal<ItConst, SentConst>(a, b); } {
    return zip_view::equal<ItConst, SentConst>(a, b);
  }

  template<bool ItConst, bool SentConst>
  [[nodiscard]] static constexpr auto distance(
    iterator<ItConst> const& a,
    sentinel<SentConst> const& b
  ) noexcept((... && noexcept(std::declval<std::ranges::iterator_t<
      maybe_const<SentConst, Views>> const&>()
      - std::declval<std::ranges::sentinel_t<
      maybe_const<SentConst, Views>> const&>())))
      requires (... && std::sized_sentinel_for<std::ranges::sentinel_t<
      maybe_const<SentConst, Views>>,
      std::ranges::iterator_t<maybe_const<ItConst, Views>>>) {
    using result_t = std::common_type_t<std::ranges::range_difference_t<
      maybe_const<SentConst, Views>>...>;
    return std::apply([&](std::ranges::iterator_t<
        maybe_const<ItConst, Views>> const&... its) constexpr
        noexcept((... && noexcept(its - std::declval<std::ranges::sentinel_t<
        maybe_const<SentConst, Views>> const&>()))) {
      return std::apply([&](std::ranges::sentinel_t<
          maybe_const<SentConst, Views>> const&... iends) constexpr
          noexcept((... && noexcept(its - iends))) {
        // We return the difference with the smallest absolute value. We store
        // absolute values with the minus sign, because the negative half of
        // the two's complement space fits one extra value.
        std::initializer_list<result_t> diffs{its - iends...};
        result_t result{diffs[0]};
        result_t neg_result{result > 0 ? -result : result};
        for(auto const diff: diffs | std::views::drop(1)) {
          auto const neg_diff{diff > 0 ? -diff : diff};
          if(neg_diff > neg_result) {
            result = diff;
            neg_result = neg_diff;
          }
        }
        return result;
      }, b.iends_);
    }, a.its_);
  }

  template<bool ItConst, bool SentConst>
  [[nodiscard]] static constexpr auto distance(
    sentinel<SentConst> const& a,
    iterator<ItConst> const& b
  ) noexcept((... && noexcept(std::declval<std::ranges::iterator_t<
      maybe_const<SentConst, Views>> const&>()
      - std::declval<std::ranges::sentinel_t<
      maybe_const<SentConst, Views>> const&>())))
      requires (... && std::sized_sentinel_for<std::ranges::sentinel_t<
      maybe_const<SentConst, Views>>,
      std::ranges::iterator_t<maybe_const<ItConst, Views>>>) {
    return -(b - a);
  }

  template<bool ItConst, bool SentConst>
  [[nodiscard]] friend constexpr auto operator-(
    iterator<ItConst> const& a,
    sentinel<SentConst> const& b
  ) noexcept(noexcept(zip_view::distance<ItConst, SentConst>(a, b)))
      requires requires { zip_view::distance<ItConst, SentConst>(a, b); } {
    return zip_view::distance<ItConst, SentConst>(a, b);
  }

  template<bool ItConst, bool SentConst>
  [[nodiscard]] friend constexpr auto operator-(
    sentinel<SentConst> const& a,
    iterator<ItConst> const& b
  ) noexcept(noexcept(zip_view::distance<ItConst, SentConst>(a, b)))
      requires requires { zip_view::distance<ItConst, SentConst>(a, b); } {
    return zip_view::distance<ItConst, SentConst>(a, b);
  }

public:
  constexpr zip_view() = default;

  constexpr zip_view(Views... views)
    noexcept((... && std::is_nothrow_move_constructible_v<Views>)):
    views_{std::move(views)...} {}

  [[nodiscard]] constexpr iterator<false> begin() noexcept((...
        && (noexcept(std::ranges::begin(std::declval<Views&>())
        && std::is_nothrow_move_constructible_v<
        std::ranges::iterator_t<Views>>))))
        requires needs_non_const_ops {
    return std::apply([](Views&... views) constexpr noexcept((...
        && (noexcept(std::ranges::begin(views)
        && std::is_nothrow_move_constructible_v<
        std::ranges::iterator_t<Views>>)))) {
      return iterator<false>{std::ranges::begin(views)...};
    }, views_);
  }

  [[nodiscard]] constexpr iterator<true> begin() const noexcept((...
        && (noexcept(std::ranges::begin(std::declval<Views const&>()))
        && std::is_nothrow_move_constructible_v<
        std::ranges::iterator_t<Views const>>)))
        requires (... && std::ranges::range<Views const>) {
    return std::apply([](Views const&... views) constexpr noexcept((...
        && (noexcept(std::ranges::begin(views))
        && std::is_nothrow_move_constructible_v<
        std::ranges::iterator_t<Views const>>))) {
      return iterator<true>{std::ranges::begin(views)...};
    }, views_);
  }

private:
  template<bool Const>
  [[nodiscard]] static constexpr bool end_noexcept() noexcept {
    if constexpr((...
        && (std::ranges::random_access_range<maybe_const<Const, Views>>
        && std::ranges::sized_range<maybe_const<Const, Views>>))) {
      return (... && (
        noexcept(std::ranges::begin(std::declval<
          maybe_const<Const, Views>&>()))
        && noexcept(std::ranges::size(std::declval<
          maybe_const<Const, Views>&>()))
        && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<
          maybe_const<Const, Views>>>
      ));
    } else if constexpr((...
        && std::ranges::common_range<maybe_const<Const, Views>>)
        && (!(... && std::ranges::bidirectional_range<
          maybe_const<Const, Views>>)
        || sizeof...(Views) == 1)) {
      return (... && (
        noexcept(std::ranges::end(std::declval<
          maybe_const<Const, Views>&>()))
        && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<
          maybe_const<Const, Views>>>
      ));
    } else {
      return (... && (
        noexcept(std::ranges::end(std::declval<
          maybe_const<Const, Views>&>()))
        && std::is_nothrow_move_constructible_v<std::ranges::sentinel_t<
          maybe_const<Const, Views>>>
      ));
    }
  }

public:
  [[nodiscard]] constexpr auto end() const noexcept(end_noexcept<false>())
      requires needs_non_const_ops {
    if constexpr((... && (std::ranges::random_access_range<Views const>
        && std::ranges::sized_range<Views const>))) {
      return std::apply([&](Views const&... views)
          constexpr noexcept(end_noexcept<false>()) {
        return iterator<false>{(std::ranges::begin(views) + size())...};
      }, views_);
    } else if constexpr((... && std::ranges::common_range<Views const>)
        && (!(... && std::ranges::bidirectional_range<Views const>)
        || sizeof...(Views) == 1)) {
      return std::apply([&](Views const&... views)
          constexpr noexcept(end_noexcept<false>()) {
        return iterator<false>{std::ranges::end(views)...};
      }, views_);
    } else {
      return std::apply([](Views const&... views)
          constexpr noexcept(end_noexcept<false>()) {
        return sentinel<false>{std::ranges::end(views)...};
      }, views_);
    }
  }

  [[nodiscard]] constexpr auto end() const noexcept(end_noexcept<true>())
      requires (... && std::ranges::range<Views const>) {
    if constexpr((... && (std::ranges::random_access_range<Views const>
        && std::ranges::sized_range<Views const>))) {
      return std::apply([&](Views const&... views)
          constexpr noexcept(end_noexcept<true>()) {
        return iterator<true>{(std::ranges::begin(views) + size())...};
      }, views_);
    } else if constexpr((... && std::ranges::common_range<Views const>)
        && (!(... && std::ranges::bidirectional_range<Views const>)
        || sizeof...(Views) == 1)) {
      return std::apply([&](Views const&... views)
          constexpr noexcept(end_noexcept<true>()) {
        return iterator<true>{std::ranges::end(views)...};
      }, views_);
    } else {
      return std::apply([](Views const&... views)
          constexpr noexcept(end_noexcept<true>()) {
        return sentinel<true>{std::ranges::end(views)...};
      }, views_);
    }
  }

  [[nodiscard]] constexpr auto size()
      noexcept((... && noexcept(std::ranges::size(std::declval<Views&>()))))
      requires (... && std::ranges::sized_range<Views>) {
    return std::apply([](Views&... views) constexpr
        noexcept((... && noexcept(std::ranges::size(views)))) {
      using result_t
        = std::common_type_t<decltype(std::ranges::size(views))...>;
      return std::ranges::min({result_t{std::ranges::size(views)}...});
    }, views_);
  }

  [[nodiscard]] constexpr auto size() const
      noexcept((... && noexcept(std::ranges::size(std::declval<Views const&>()))))
      requires (... && std::ranges::sized_range<Views const>) {
    return std::apply([](Views const&... views) constexpr
        noexcept((... && noexcept(std::ranges::size(views)))) {
      using result_t = std::common_type_t<decltype(std::ranges::size(views))...>;
      return std::ranges::min({result_t{std::ranges::size(views)}...});
    }, views_);
  }
};

template<typename... Ranges>
zip_view(Ranges&&...) -> zip_view<std::views::all_t<Ranges>...>;

} // util

template<typename... Views>
constexpr bool std::ranges::enable_borrowed_range<::util::zip_view<Views...>>{
  (... && ::std::ranges::enable_borrowed_range<Views>)};

namespace util {

namespace detail_ {

struct zip_t {
  constexpr auto operator()() const noexcept {
    return std::views::empty<std::tuple<>>;
  }

  template<std::ranges::viewable_range... Ranges>
  requires (sizeof...(Ranges) > 0)
  constexpr auto operator()(Ranges&&... ranges) const noexcept((...
      && std::is_nothrow_move_constructible_v<std::views::all_t<Ranges>>)) {
    return zip_view<std::views::all_t<Ranges>...>{
      std::forward<Ranges>(ranges)...};
  }
};

} // detail_

inline constexpr detail_::zip_t zip;

} // util

#endif
