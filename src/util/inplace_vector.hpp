#ifndef UTIL_INPLACE_VECTOR_HPP_INCLUDED_MR0Q1AQ926VK8B4WA0ALQ7HDQ
#define UTIL_INPLACE_VECTOR_HPP_INCLUDED_MR0Q1AQ926VK8B4WA0ALQ7HDQ

#include "util/iterator_facade.hpp"
#include "util/overflow.hpp"
#include "util/repeat_view.hpp"
#include "util/type_traits.hpp"
#include "util/util.hpp"
#include <algorithm>
#include <cassert>
#include <compare>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <initializer_list>
#include <iterator>
#include <limits>
#include <memory>
#include <new>
#include <ranges>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace util {

template<typename T>
concept inplace_vector_element = std::is_move_constructible_v<T>
  && std::is_move_assignable_v<T> && std::is_nothrow_destructible_v<T>;

namespace detail_ {

template<inplace_vector_element T>
class inplace_vector_empty_core {
public:
  [[nodiscard]] constexpr T* data() noexcept { return nullptr; }
  [[nodiscard]] constexpr T const* data() const noexcept { return nullptr; }
  [[nodiscard]] static constexpr std::size_t size() noexcept { return 0; }
  static constexpr void set_size(std::size_t size) noexcept { assert(size == 0); }
};

template<std::size_t N>
using inplace_vector_internal_size_type =
  std::conditional_t<N < (std::uint64_t{1} << 8), std::uint8_t,
  std::conditional_t<N < (std::uint64_t{1} << 16), std::uint16_t,
  std::conditional_t<N < (std::uint64_t{1} << 32), std::uint32_t, std::uint64_t>>>;

template<inplace_vector_element T, std::size_t N>
class inplace_vector_trivial_core {
  static_assert(std::is_trivial_v<T>);
  static_assert(N != 0);

private:
  inplace_vector_internal_size_type<N> size_{0};
  T buf_[N];

public:
  [[nodiscard]] constexpr T* data() noexcept {
    return buf_;
  }
  [[nodiscard]] constexpr T const* data() const noexcept {
    return buf_;
  }
  [[nodiscard]] constexpr std::size_t size() const noexcept { return size_; }
  constexpr void set_size(std::size_t size) noexcept {
    assert(size <= N);
    size_ = static_cast<inplace_vector_internal_size_type<N>>(size);
  }
};

template<inplace_vector_element T, std::size_t N>
class inplace_vector_nontrivial_core {
  static_assert(N != 0);

private:
  inplace_vector_internal_size_type<N> size_{0};

  alignas(T) std::byte buf_[sizeof(T) * N];

public:
  [[nodiscard]] T* data() noexcept {
    return std::launder(reinterpret_cast<T*>(buf_));
  }
  [[nodiscard]] T const* data() const noexcept {
    return std::launder(reinterpret_cast<T const*>(buf_));
  }
  [[nodiscard]] std::size_t size() const noexcept { return size_; }
  void set_size(std::size_t size) noexcept {
    assert(size <= N);
    size_ = static_cast<inplace_vector_internal_size_type<N>>(size);
  }
};

template<inplace_vector_element T, std::size_t N>
using inplace_vector_core = std::conditional_t<N == 0,
  inplace_vector_empty_core<T>,
  std::conditional_t<std::is_trivial_v<T>,
    inplace_vector_trivial_core<T, N>,
    inplace_vector_nontrivial_core<T, N>
  >>;

} // detail_

// Public interface ///////////////////////////////////////////////////////////////////////////

/** @brief An implementation of std::inplace_vector from C++26.
 *
 * This vector has a fixed capacity that is allocated entirely within the vector object. All
 * C++26 APIs are available, except for a range-based constructor, since that relies upon the
 * std::from_range tag, which is only available in C++23. */
template<inplace_vector_element T, std::size_t N>
class inplace_vector {
  static_assert(N <= std::numeric_limits<std::size_t>::max() / sizeof(T),
    "util::inplace_vector's capacity too large");

public:
  using value_type = T;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using reference = T&;
  using const_reference = T const&;
  using pointer = T*;
  using const_pointer = T const*;

private:
  [[no_unique_address]] detail_::inplace_vector_core<T, N> core_;

  // Construction & destruction ///////////////////////////////////////////////////////////////
public:
  /** @brief Constructs an empty vector. */
  constexpr inplace_vector() noexcept = default;

  /** @brief Constructs a vector with the given number of default-constructed elements.
   *
   * May also throw whatever is thrown by the element's default or copy constructor.
   *
   * @throws std::bad_alloc If the requested size exceeds the vector's capacity. */
  constexpr explicit inplace_vector(std::size_t count)
  requires std::is_default_constructible_v<T>: inplace_vector() {
    while(size() < count) {
      unchecked_emplace_back();
    }
  }

  /** @brief Constructs a vector with the given number of copies of the given value.
   *
   * May also throw whatever is thrown by the element's copy constructor.
   *
   * @throws std::bad_alloc If the requested size exceeds the vector's capacity. */
  constexpr inplace_vector(std::size_t count, T const& value): inplace_vector() {
    this->append_range(util::repeat(value, count));
  }

  /** @brief Constructs a vector with the values from the given range.
   *
   * May also throw whatever is thrown by operations on the iterators or the element's
   * constructor.
   *
   * @throws std::bad_alloc If the requested size exceeds the vector's capacity. */
  template<std::input_iterator Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T>
  constexpr inplace_vector(Iterator it, Iterator iend): inplace_vector() {
    this->append_range(std::ranges::subrange{std::move(it), std::move(iend)});
  }

  // TODO: In C++26, add an inplace_vector constructor from a range

  /** @brief Copy-contructs the vector. */
  constexpr inplace_vector(inplace_vector const& other)
  noexcept(std::is_nothrow_copy_constructible_v<T>)
  requires
    std::is_copy_constructible_v<T>
    && (N != 0)
    && (!std::is_trivially_copy_constructible_v<T>):
  inplace_vector(other.cbegin(), other.cend()) {}

  /** @brief Copy-contructs the vector. */
  constexpr inplace_vector(inplace_vector const& other)
  noexcept(std::is_nothrow_copy_constructible_v<T>)
  requires
    std::is_copy_constructible_v<T>
    && (N == 0
    || std::is_trivially_copy_constructible_v<T>)
  = default;

  /** @brief Move-contructs the vector.
   *
   * If the elements are not trivially move-constructible, the moved-from vector is in a valid
   * but unspecified state. */
  constexpr inplace_vector(inplace_vector&& other)
  noexcept(std::is_nothrow_move_constructible_v<T>):
  inplace_vector(std::move_iterator{other.begin()}, std::move_iterator{other.end()}) {}

  /** @brief Move-contructs the vector.
   *
   * If the elements are trivially move-constructible, the moved-from vector is unchanged. */
  constexpr inplace_vector(inplace_vector&& other)
  noexcept(std::is_nothrow_move_constructible_v<T>)
  requires (N == 0) || std::is_trivially_move_constructible_v<T>
  = default;

  /** @brief Constructs a vector with the values from the given initializer list. */
  constexpr inplace_vector(std::initializer_list<T> il)
  noexcept(N == 0 || std::is_nothrow_copy_constructible_v<T>)
  requires std::is_copy_constructible_v<T>:
  inplace_vector(il.begin(), il.end()) {}

  constexpr ~inplace_vector() noexcept { clear(); }
  constexpr ~inplace_vector() noexcept
  requires (N == 0) || std::is_trivially_destructible_v<T>
  = default;

  // Assignment ///////////////////////////////////////////////////////////////////////////////

  /** @brief Copy-assigns the vector.
   *
   * If the element is not trivially copy-constructible, trivially copy-assignable, and
   * trivially destructible, then this function has a basic exception safety guarantee.
   *
   * All iterators are invalidated. */
  constexpr inplace_vector& operator=(inplace_vector const& other)
  noexcept(
    N == 0
    || std::is_nothrow_copy_constructible_v<T>
    && std::is_nothrow_copy_assignable_v<T>
  )
  requires
    std::is_copy_constructible_v<T>
    && std::is_copy_assignable_v<T>
    && (N != 0)
    && (!std::is_trivially_copy_constructible_v<T>)
    && (!std::is_trivially_copy_assignable_v<T>)
  {
    if(this != &other) {
      this->assign_range(other);
    }
    return *this;
  }

  /** @brief Copy-assigns the vector. */
  constexpr inplace_vector& operator=(inplace_vector const& other) noexcept
  requires
    (N == 0)
    || std::is_trivially_copy_constructible_v<T>
    && std::is_trivially_copy_assignable_v<T>
  = default;

  /** @brief Move-assigns the vector.
   *
   * If the element is not trivially move-constructible, trivially move-assignable, and
   * trivially destructible, then this function has a basic exception safety guarantee, and the
   * other vector is left in a valid but unspecified state.
   *
   * All iterators are invalidated. */
  constexpr inplace_vector& operator=(inplace_vector&& other)
  noexcept(
    N == 0
    || std::is_nothrow_move_constructible_v<T>
    && std::is_nothrow_move_assignable_v<T>
  )
  {
    if(this != &other) {
      this->assign(std::move_iterator{other.begin()}, std::move_iterator(other.end()));
    }
    return *this;
  }

  /** @brief Move-assigns the vector.
   *
   * If the element is trivially move-constructible, trivially move-assignable, and trivially
   * destructible, then the other vector is unmodified.
   *
   * All iterators are invalidated. */
  constexpr inplace_vector& operator=(inplace_vector&& other) noexcept
  requires
    (N == 0)
    || std::is_trivially_move_constructible_v<T>
    && std::is_trivially_move_assignable_v<T>
    && std::is_trivially_destructible_v<T>
  = default;

  /** @brief Replaces the vector's content with the content of the given initializer list.
   *
   * May also throw whatever is thrown by the element's copy construction or assignment.
   *
   * If the vector's capacity is exceeded, strong exception safety guarantee. Otherwise, basic
   * exception safety guarantee.
   *
   * All iterators are invalidated.
   *
   * @throws std::bad_alloc If the requested size exceeds the vector's capacity. */
  constexpr inplace_vector& operator=(std::initializer_list<T> il)
  requires std::is_copy_constructible_v<T>
  {
    this->assign_range(il);
    return *this;
  }

  /** @brief Replaces the vector's content with the given number of copies of the given value.
   *
   * May also throw whatever is thrown by the element's copy construction or assignment.
   *
   * If the vector's capacity is exceeded, strong exception safety guarantee. Otherwise, basic
   * exception safety guarantee.
   *
   * All iterators are invalidated.
   *
   * @throws std::bad_alloc If the requested size exceeds the vector's capacity. */
  constexpr void assign(std::size_t count, T const& value)
  requires std::is_copy_constructible_v<T>
  {
    this->assign_range(util::repeat(value, count));
  }

  /** @brief Replaces the vector's content with the values from the given range.
   *
   * May also throw whatever is thrown by operations on the iterator or the element's copy
   * construction or assignment.
   *
   * If the vector's capacity is exceeded, strong exception safety guarantee. Otherwise, basic
   * exception safety guarantee.
   *
   * All iterators are invalidated.
   *
   * @throws std::bad_alloc If the requested size exceeds the vector's capacity. */
  template<std::input_iterator Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T>
  constexpr void assign(Iterator it, Iterator iend) {
    this->assign_range(std::ranges::subrange{std::move(it), std::move(iend)});
  }

  /** @brief Replaces the vector's content with the content of the given initializer list.
   *
   * May also throw whatever is thrown by the element's copy construction or assignment.
   *
   * If the vector's capacity is exceeded, strong exception safety guarantee. Otherwise, basic
   * exception safety guarantee.
   *
   * All iterators are invalidated.
   *
   * @throws std::bad_alloc If the requested size exceeds the vector's capacity. */
  constexpr void assign(std::initializer_list<T> il)
  requires std::is_copy_constructible_v<T>
  {
    this->assign_range(il);
  }

  /** @brief Replaces the vector's content with the values from the given range.
   *
   * May also throw whatever is thrown by range operations or by assignment or construction of
   * the elements.
   *
   * If the range is a sized range and the vector's capacity is exceeded, strong exception
   * safety guarantee. Otherwise, basic exception safety guarantee.
   *
   * All iterators are invalidated.
   *
   * @throws std::bad_alloc If the range is bigger than the vector's capacity. */
  template<std::ranges::input_range Range>
  requires
    (N == 0)
    || std::assignable_from<T&, std::ranges::range_reference_t<Range>>
    && std::convertible_to<std::ranges::range_reference_t<Range>, T>
  constexpr void assign_range(Range&& range) {
    if constexpr(std::ranges::sized_range<Range>) {
      auto const range_size_raw{std::ranges::size(range)};
      if(range_size_raw > N) {
        throw std::bad_alloc{};
      }
      std::size_t const range_size{util::asserted_cast<std::size_t>(range_size_raw)};

      if(!std::is_constant_evaluated()) {
        if constexpr(
          std::is_trivially_copyable_v<T>
          && std::ranges::contiguous_range<Range>
          && std::same_as<std::ranges::range_value_t<Range>, T>
        ) {
          std::memcpy(data(), std::ranges::data(range), range_size * sizeof(T));
          core_.set_size(range_size);
          return;
        }
      }
    }

    std::size_t index{0};
    auto it{std::ranges::begin(range)};
    auto const iend{std::ranges::end(range)};
    while(it != iend && index < size()) {
      (*this)[index] = *it;
      ++it;
      ++index;
    }
    if(index < size()) {
      this->erase(cbegin() + index, cend());
    } else {
      while(it != iend) {
        if constexpr(std::ranges::sized_range<Range>) {
          this->unchecked_push_back(*it);
        } else {
          this->push_back(*it);
        }
        ++it;
      }
    }
  }

  // Swap /////////////////////////////////////////////////////////////////////////////////////

  /** @brief Swaps the two vectors. */
  constexpr void swap(inplace_vector& other)
  noexcept(N == 0 || std::is_nothrow_swappable_v<T> && std::is_nothrow_move_constructible_v<T>)
  {
    if constexpr(std::is_trivially_copyable_v<T>) {
      auto temp{*this};
      *this = std::move(other);
      other = std::move(temp);
    } else {
      if(size() < other.size()) {
        other.swap(*this);
      }
      for(auto const i: std::views::iota(std::size_t{0}, other.size())) {
        using std::swap;
        swap((*this)[i], other[i]);
      }
      std::size_t const old_other_size{other.size()};
      for(auto const i: std::views::iota(old_other_size, size())) {
        other.unchecked_push_back((*this)[i]);
      }
      this->erase(cbegin() + old_other_size, cend());
    }
  }

  // Element access ///////////////////////////////////////////////////////////////////////////

  /** @brief Returns a reference to the element at the given index.
   * @throws std::out_of_range If the index is out of range.
   * @throws std::bad_alloc If the std::out_of_range exception object could not be
   *                        constructed. */
  [[nodiscard]] constexpr T& at(std::size_t index) {
    if(index >= size()) {
      throw std::out_of_range{"util::inplace_vector::at: index out of range"};
    }
    return operator[](index);
  }

  /** @brief Returns a reference to the element at the given index.
   * @throws std::out_of_range If the index is out of range.
   * @throws std::bad_alloc If the std::out_of_range exception object could not be
   *                        constructed. */
  [[nodiscard]] constexpr T const& at(std::size_t index) const {
    if(index >= size()) {
      throw std::out_of_range{"util::inplace_vector::at: index out of range"};
    }
    return operator[](index);
  }

  /** @brief Returns a reference to the element at the given index.
   *
   * If the index is out of range, the behavior is undefined. */
  [[nodiscard]] constexpr T& operator[](std::size_t index) noexcept {
    assert(index < size());
    return core_.data()[index];
  }

  /** @brief Returns a reference to the element at the given index.
   *
   * If the index is out of range, the behavior is undefined. */
  [[nodiscard]] constexpr T const& operator[](std::size_t index) const noexcept {
    assert(index < size());
    return core_.data()[index];
  }

  /** @brief Returns a reference to the first element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] constexpr T& front() noexcept {
    assert(!empty());
    return operator[](0);
  }

  /** @brief Returns a reference to the first element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] constexpr T const& front() const noexcept {
    assert(!empty());
    return operator[](0);
  }

  /** @brief Returns a reference to the last element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] constexpr T& back() noexcept {
    assert(!empty());
    return operator[](size() - 1);
  }

  /** @brief Returns a reference to the last element.
   *
   * If the vector is empty, the behavior is undefined. */
  [[nodiscard]] constexpr T const& back() const noexcept {
    assert(!empty());
    return operator[](size() - 1);
  }

  /** @brief Returns a pointer to the underlying storage. */
  [[nodiscard]] constexpr T* data() noexcept {
    return core_.data();
  }

  /** @brief Returns a pointer to the underlying storage. */
  [[nodiscard]] constexpr T const* data() const noexcept {
    return core_.data();
  }

  // Iterators ////////////////////////////////////////////////////////////////////////////////
private:
  class const_iterator_core;

  class iterator_core {
  private:
    T* ptr_;

    friend class inplace_vector;
    friend class const_iterator_core;
    constexpr explicit iterator_core(T* ptr) noexcept: ptr_{ptr} {}

  public:
    constexpr iterator_core() noexcept = default;

  protected:
    static constexpr bool is_contiguous{true};

    [[nodiscard]] constexpr T& dereference() const noexcept {
      return *ptr_;
    }

    constexpr void advance(std::ptrdiff_t off) noexcept {
      ptr_ += off;
    }

    [[nodiscard]] constexpr std::ptrdiff_t distance_to(iterator_core other) const noexcept {
      return other.ptr_ - ptr_;
    }
  };

public:
  using iterator = util::iterator_facade<iterator_core>;

private:
  class const_iterator_core {
  private:
    T const* ptr_;

    friend class inplace_vector;
    constexpr explicit const_iterator_core(T const* ptr) noexcept: ptr_{ptr} {}

  public:
    constexpr const_iterator_core() noexcept = default;
    constexpr const_iterator_core(iterator other) noexcept: ptr_{other.ptr_} {}

  protected:
    static constexpr bool is_contiguous{true};

    [[nodiscard]] constexpr T const& dereference() const noexcept {
      return *ptr_;
    }

    constexpr void advance(std::ptrdiff_t off) noexcept {
      ptr_ += off;
    }

    [[nodiscard]] constexpr std::ptrdiff_t distance_to(
      const_iterator_core other
    ) const noexcept {
      return other.ptr_ - ptr_;
    }
  };

public:
  using const_iterator = util::iterator_facade<const_iterator_core>;

  [[nodiscard]] constexpr iterator begin() noexcept {
    return iterator{data()};
  }
  [[nodiscard]] constexpr const_iterator begin() const noexcept {
    return const_iterator{data()};
  }
  [[nodiscard]] constexpr const_iterator cbegin() const noexcept { return begin(); }

  [[nodiscard]] constexpr iterator end() noexcept {
    return iterator{data() + size()};
  }
  [[nodiscard]] constexpr const_iterator end() const noexcept {
    return const_iterator{data() + size()};
  }
  [[nodiscard]] constexpr const_iterator cend() const noexcept { return end(); }

  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  [[nodiscard]] constexpr reverse_iterator rbegin() noexcept {
    return reverse_iterator{end()};
  }
  [[nodiscard]] constexpr const_reverse_iterator rbegin() const noexcept {
    return const_reverse_iterator{end()};
  }
  [[nodiscard]] constexpr const_reverse_iterator crbegin() const noexcept { return rbegin(); }

  [[nodiscard]] constexpr reverse_iterator rend() noexcept {
    return reverse_iterator{begin()};
  }
  [[nodiscard]] constexpr const_reverse_iterator rend() const noexcept {
    return const_reverse_iterator{begin()};
  }
  [[nodiscard]] constexpr const_reverse_iterator crend() const noexcept { return rend(); }

  // Size & capacity //////////////////////////////////////////////////////////////////////////
public:
  /** @brief Checks if the vector is empty. */
  [[nodiscard]] constexpr bool empty() const noexcept { return size() == 0; }

  /** @brief Returns the vector's size. */
  [[nodiscard]] constexpr std::size_t size() const noexcept { return core_.size(); }

  /** @brief Returns the vector's maximum size (the second template parameter).
   *
   * This exists for compatibility with std::vector's API. */
  [[nodiscard]] static constexpr std::size_t max_size() noexcept { return N; }

  /** @brief Returns the vector's capacity (the second template parameter).
   *
   * This exists for compatibility with std::vector's API. */
  [[nodiscard]] static constexpr std::size_t capacity() noexcept { return N; }

  /** @brief Throws std::bad_alloc if the argument is greater than the capacity; otherwise,
   * does nothing.
   *
   * This exists for compatibility with std::vector's API.
   *
   * @throws std::bad_alloc If the argument is greater than the capacity. */
  static constexpr void reserve(std::size_t new_capacity) {
    if(new_capacity > N) {
      throw std::bad_alloc{};
    }
  }

  /** @brief Does nothing.
   *
   * This exists for compatibility with std::vector's API. */
  static constexpr void shrink_to_fit() noexcept {}

  // Emplacement //////////////////////////////////////////////////////////////////////////////

  /** @brief Constructs a new element immediately before the given one.
   *
   * May also throw whatever is thrown by the element's constructor, or by the move
   * construction, move assignment, or swap of various elements.
   *
   * If the element is inserted at the end or if the vector is full, there is a strong
   * exception safety guarantee. Otherwise, let N be the position where the element is to be
   * inserted; if an exception is thrown, the vector's length is guaranteed to be at least N
   * and the first N elements are unmodified.
   *
   * Iterators at or after the given position are invalidated.
   *
   * @returns An iterator to the newly inserted element.
   * @throws std::bad_alloc If the vector is full. */
  template<typename... Args>
  requires std::is_constructible_v<T, Args...>
  constexpr iterator emplace(const_iterator pos, Args&&... args) {
    this->emplace_back(std::forward<Args>(args)...);
    auto const pos_mut{begin() + (pos - cbegin())};
    std::rotate(pos_mut, end() - 1, end());
    return pos_mut;
  }

  /** @brief Constructs a new element at the end of the vector.
   *
   * May also throw whatever is thrown by the element's constructor.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A reference to the newly constructed element.
   * @throws std::bad_alloc If the vector is full. */
  template<typename... Args>
  requires std::is_constructible_v<T, Args...>
  constexpr T& emplace_back(Args&&... args)
  {
    if(size() == N) {
      throw std::bad_alloc{};
    } else {
      return this->unchecked_emplace_back(std::forward<Args>(args)...);
    }
  }

  /** @brief Constructs a new element at the end of the vector.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A pointer to the newly constructed element, or a null pointer if the vector is
   *          full. */
  template<typename... Args>
  requires std::is_constructible_v<T, Args...>
  constexpr T* try_emplace_back(Args&&... args)
  noexcept(N == 0 || std::is_nothrow_constructible_v<T, Args...>)
  {
    if(size() == N) {
      return nullptr;
    } else {
      return std::addressof(this->unchecked_emplace_back(std::forward<Args>(args)...));
    }
  }

  /** @brief Constructs a new element at the end of the vector.
   *
   * If the vector is full, the behavior is undefined.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A reference to the newly constructed element. */
  template<typename... Args>
  requires std::is_constructible_v<T, Args...>
  constexpr T& unchecked_emplace_back(Args&&... args)
  noexcept(N == 0 || std::is_nothrow_constructible_v<T, Args...>)
  {
    assert(size() < N);
    T& result{*std::construct_at(data() + size(), std::forward<Args>(args)...)};
    core_.set_size(core_.size() + 1);
    return result;
  }

  // Insertion ////////////////////////////////////////////////////////////////////////////////

  /** @brief Copy-constructs a new element immediately before the given one.
   *
   * May also throw whatever is thrown by the element's constructor, or by the move
   * construction, move assignment, or swap of various elements.
   *
   * If the element is inserted at the end or if the vector is full, there is a strong
   * exception safety guarantee. Otherwise, let N be the position where the element is to be
   * inserted; if an exception is thrown, the vector's length is guaranteed to be at least N
   * and the first N elements are unmodified.
   *
   * Iterators at or after the given position are invalidated.
   *
   * @returns An iterator to the newly inserted element.
   * @throws std::bad_alloc If the vector is full. */
  constexpr iterator insert(const_iterator pos, T const& value)
  requires std::is_copy_constructible_v<T>
  {
    return this->emplace(pos, value);
  }

  /** @brief Move-constructs a new element immediately before the given one.
   *
   * May also throw whatever is thrown by the element's constructor, or by the move
   * construction, move assignment or swap of various elements.
   *
   * If the element is inserted at the end or if the vector is full, there is a strong
   * exception safety guarantee. Otherwise, let N be the position where the element is to be
   * inserted; if an exception is thrown, the vector's length is guaranteed to be at least N
   * and the first N elements are unmodified.
   *
   * Iterators at or after the given position are invalidated.
   *
   * @returns An iterator to the newly inserted element.
   * @throws std::bad_alloc If the vector is full. */
  constexpr iterator insert(const_iterator pos, T&& value)
  requires std::is_copy_constructible_v<T>
  {
    return this->emplace(pos, std::move(value));
  }

  /** @brief Inserts the given number of copies of the given value immediately before the given
   * element.
   *
   * May also throw whatever is thrown by the element's constructor, or by the move
   * construction, move assignment or swap of various elements.
   *
   * If the vector's capacity is exceeded, strong exception guarantee. Otherwise, let N be the
   * position where the elements are to be inserted; if an exception is thrown, the vector's
   * length is guaranteed to be at least N and the first N elements are unmodified.
   *
   * Iterators at or after the given position are invalidated.
   *
   * @returns An iterator to the start of the newly inserted elements, or, if the range is
   *          empty, to the start of the part of the vector that follows the insertion point.
   * @throws std::bad_alloc If there are too many elements to fit the vector. */
  constexpr iterator insert(const_iterator pos, std::size_t count, T const& value) {
    return this->insert_range(pos, util::repeat(value, count));
  }

  /** @brief Inserts into the vector the elements from the given range immediately before the
   * given one.
   *
   * May also throw whatever is thrown by range or iterator operations or element construction,
   * move-construction, move-assignment, or swap of the elements.
   *
   * If the range is a sized range and the vector's capacity is exceeded, strong exception
   * guarantee. Otherwise, let N be the position where the elements are to be inserted; if an
   * exception is thrown, the vector's length is guaranteed to be at least N and the first N
   * elements are unmodified.
   *
   * Iterators at or after the given position are invalidated.
   *
   * @returns An iterator to the start of the newly inserted elements, or, if the range is
   *          empty, to the start of the part of the vector that follows the insertion point.
   * @throws std::bad_alloc If there are too many elements to fit the vector. */
  template<std::input_iterator Iterator>
  requires std::convertible_to<std::iter_reference_t<Iterator>, T>
  constexpr iterator insert(const_iterator pos, Iterator it, Iterator iend) {
    return this->insert_range(pos, std::ranges::subrange{std::move(it), std::move(iend)});
  }

  /** @brief Inserts into the vector the elements from the given initializer list immediately
   * before the given one.
   *
   * May also throw whatever is thrown by the element's constructor, or by the move
   * construction, move assignment or swap of various elements.
   *
   * If the vector's capacity is exceeded, strong exception guarantee. Otherwise, let N be the
   * position where the elements are to be inserted; if an exception is thrown, the vector's
   * length is guaranteed to be at least N and the first N elements are unmodified.
   *
   * Iterators at or after the given position are invalidated.
   *
   * @returns An iterator to the start of the newly inserted elements, or, if the range is
   *          empty, to the start of the part of the vector that follows the insertion point.
   * @throws std::bad_alloc If there are too many elements to fit the vector. */
  constexpr iterator insert(const_iterator pos, std::initializer_list<T> il) {
    return this->insert_range(pos, il);
  }

  /** @brief Inserts into the vector the elements from the given range immediately before the
   * given one.
   *
   * May also throw whatever is thrown by range or iterator operations or element construction,
   * move-construction, move-assignment, or swap of the elements.
   *
   * If the range is a sized range and the vector's capacity is exceeded, strong exception
   * guarantee. Otherwise, let N be the position where the elements are to be inserted; if an
   * exception is thrown, the vector's length is guaranteed to be at least N and the first N
   * elements are unmodified.
   *
   * Iterators at or after the given position are invalidated.
   *
   * @returns An iterator to the start of the newly inserted elements, or, if the range is
   *          empty, to the start of the part of the vector that follows the insertion point.
   * @throws std::bad_alloc If there are too many elements to fit the vector. */
  template<std::ranges::input_range Range>
  requires std::convertible_to<std::ranges::range_reference_t<Range>, T>
  constexpr iterator insert_range(const_iterator pos, Range&& range) {
    auto const old_end{end()};
    this->append_range(std::forward<Range>(range));
    auto const pos_mut{begin() + (pos - cbegin())};
    std::rotate(pos_mut, old_end, end());
    return pos_mut;
  }

  /** @brief Copy-constructs the given value at the end of the vector.
   *
   * May also throw whatever is thrown by the element's copy-construction.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A reference to the newly added element.
   * @throws std::bad_alloc If the vector is full. */
  constexpr T& push_back(T const& value)
  requires std::is_copy_constructible_v<T>
  {
    return this->emplace_back(value);
  }

  /** @brief Move-constructs the given value at the end of the vector.
   *
   * May also throw whatever is thrown by the element's move-construction.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A reference to the newly added element.
   * @throws std::bad_alloc If the vector is full. */
  constexpr T& push_back(T&& value) {
    return this->emplace_back(std::move(value));
  }

  /** @brief Copy-constructs a new element at the end of the vector.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A pointer to the newly constructed element, or a null pointer if the vector is
   *          full. */
  constexpr T* try_push_back(T const& value)
  noexcept(N == 0 || std::is_nothrow_copy_constructible_v<T>)
  requires std::is_copy_constructible_v<T>
  {
    return this->try_emplace_back(value);
  }

  /** @brief Move-constructs a new element at the end of the vector.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A pointer to the newly constructed element, or a null pointer if the vector is
   *          full. */
  constexpr T* try_push_back(T&& value)
  noexcept(N == 0 || std::is_nothrow_move_constructible_v<T>)
  {
    return this->try_emplace_back(std::move(value));
  }

  /** @brief Copy-constructs a new element at the end of the vector.
   *
   * If the vector is full, the behavior is undefined.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A reference to the newly constructed element. */
  constexpr T& unchecked_push_back(T const& value)
  noexcept(N == 0 || std::is_nothrow_copy_constructible_v<T>)
  requires std::is_copy_constructible_v<T>
  {
    assert(size() < N);
    return this->unchecked_emplace_back(value);
  }

  /** @brief Move-constructs a new element at the end of the vector.
   *
   * If the vector is full, the behavior is undefined.
   *
   * Has a strong exception safety guarantee.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns A reference to the newly constructed element. */
  constexpr T& unchecked_push_back(T&& value)
  noexcept(N == 0 || std::is_nothrow_move_constructible_v<T>)
  {
    assert(size() < N);
    return this->unchecked_emplace_back(std::move(value));
  }

  /** @brief Appends to the vector the elements from the given range.
   *
   * May also throw whatever is thrown by range or iterator operations or element construction.
   *
   * If the range is a sized range and the vector's capacity is exceeded, strong exception
   * guarantee. Otherwise, if an exception is thrown, only the elements prior to the throwing
   * one are appended.
   *
   * Past-the-end iterators are invalidated.
   *
   * @throws std::bad_alloc If there are too many elements to fit the vector. */
  template<std::ranges::input_range Range>
  requires std::convertible_to<std::ranges::range_reference_t<Range>, T>
  constexpr void append_range(Range&& range) {
    if constexpr(std::ranges::sized_range<Range>) {
      auto const range_size_raw{std::ranges::size(range)};
      if(range_size_raw > N || size() > N - range_size_raw) {
        throw std::bad_alloc{};
      }
      std::size_t range_size{util::asserted_cast<std::size_t>(range_size_raw)};

      if(!std::is_constant_evaluated()) {
        if constexpr(
          std::is_trivially_copyable_v<T>
          && std::ranges::contiguous_range<Range>
          && std::same_as<std::ranges::range_value_t<Range>, T>
        ) {
          std::memcpy(data() + size(), std::ranges::cdata(range), range_size * sizeof(T));
          core_.set_size(size() + range_size);
          return;
        }
      }
    }

    for(auto&& el: range) {
      this->push_back(std::forward<decltype(el)>(el));
    }
  }

  /** @brief Appends to the vector elements from the given range until the vector is full or
   * the range is exhausted.
   *
   * If an exception is thrown, only the elements prior to the throwing one are appended.
   *
   * Past-the-end iterators are invalidated.
   *
   * @returns An iterator into the argument range to the first element not inserted into the
   *          vector, or a past-the-end iterator if all elements were inserted. */
  template<std::ranges::input_range Range>
  requires std::convertible_to<std::ranges::range_reference_t<Range>, T>
  constexpr std::ranges::borrowed_iterator_t<Range> try_append_range(Range&& range)
  noexcept(
    noexcept(std::ranges::begin(range))
    && noexcept(std::ranges::end(range))
    && noexcept(std::declval<std::ranges::iterator_t<Range>&>()
      != std::declval<std::ranges::sentinel_t<Range> const&>())
    && noexcept(*std::declval<std::ranges::iterator_t<Range>&>())
    && noexcept(++std::declval<std::ranges::iterator_t<Range>&>())
    && std::is_nothrow_constructible_v<T, std::ranges::range_reference_t<Range>>
  )
  {
    if(!std::is_constant_evaluated()) {
      if constexpr(
        std::is_trivially_copyable_v<T>
        && std::ranges::contiguous_range<Range>
        && std::same_as<std::ranges::range_value_t<Range>, T>
        && std::ranges::sized_range<Range>
      ) {
        std::size_t const copied_size{util::asserted_cast<std::size_t>(
          std::min<std::common_type_t<std::ranges::range_size_t<Range>, std::size_t>>(
            std::ranges::size(range),
            N - size()
          )
        )};
        std::memcpy(data() + size(), std::ranges::data(range), copied_size * sizeof(T));
        core_.set_size(size() + copied_size);
        return std::ranges::begin(range) + copied_size;
      }
    }

    auto it{std::ranges::begin(range)};
    if(size() == N) {
      return it;
    }
    auto const iend{std::ranges::end(range)};
    while(size() < N && it != iend) {
      this->unchecked_push_back(*it);
      ++it;
    }
    return it;
  }

  // Erasure //////////////////////////////////////////////////////////////////////////////////

  /** @brief Destroys all elements.
   *
   * Invalidates all iterators. */
  constexpr void clear() noexcept {
    this->erase(cbegin(), cend());
  }

  /** @brief Destroys the given element
   *
   * May also throw whatever is thrown by the element's constructor, or by the move
   * construction or move assignment of various elements.
   *
   * Invalidates all iterators up to and including `pos`.
   *
   * @returns An iterator to the new start of the part of the vector after the erased element.
   */
  constexpr iterator erase(const_iterator pos)
  noexcept(
    N == 0
    || std::is_nothrow_move_constructible_v<T>
    && std::is_nothrow_move_assignable_v<T>
    && std::is_nothrow_swappable_v<T>
  )
  {
    return this->erase(pos, pos + 1);
  }

  /** @brief Destroys all the elements starting with `pos_first` and ending right before
   * `pos_last`.
   *
   * May also throw whatever is thrown by the element's constructor, or by the move
   * construction or move assignment of various elements.
   *
   * Invalidates all iterators up to and including pos_first.
   *
   * @returns An iterator to the new start of the part of the vector after the erased range. */
  constexpr iterator erase(const_iterator pos_first, const_iterator pos_last)
  noexcept(
    N == 0
    || std::is_nothrow_move_constructible_v<T>
    && std::is_nothrow_move_assignable_v<T>
    && std::is_nothrow_swappable_v<T>
  )
  {
    auto const pos_first_mut{begin() + (pos_first - cbegin())};
    auto const pos_last_mut{begin() + (pos_last - cbegin())};
    auto const new_size{util::asserted_cast<std::size_t>(
      std::rotate(pos_first_mut, pos_last_mut, end()) - begin())};
    if constexpr(!std::is_trivially_destructible_v<T>) {
      for(auto const i: std::views::iota(new_size, size())) {
        std::destroy_at(data() + i);
      }
    }
    core_.set_size(new_size);
    return pos_first_mut;
  }

  /** @brief Destroys the last element.
   *
   * If the vector is empty, the behavior is undefined.
   *
   * Invalidates iterators to the last element and past-the-end iterators. */
  constexpr void pop_back() noexcept {
    assert(!empty());
    core_.set_size(size() - 1);
    std::destroy_at(data() + size());
  }

  // Resizing /////////////////////////////////////////////////////////////////////////////////

  /** @brief Changes the vector's size to the specified one.
   *
   * If the new size is less than the old one, elements at the end are erased. If the new size
   * is greater than the old size, default-constructed elements are inserted at the end.
   *
   * May also throw whatever is thrown by the element's default construction.
   *
   * Strong exception safety guarantee.
   *
   * The past-the-end iterator is invalidated. If the size is decreased, iterators to erased
   * elements are also invalidated.
   *
   * @throws std::bad_alloc If the new size exceeds the vector capacity. */
  constexpr void resize(std::size_t new_size)
  requires std::is_default_constructible_v<T>
  {
    if(size() > new_size) {
      this->erase(cbegin() + new_size, cend());
    } else {
      if(N < new_size) {
        throw std::bad_alloc{};
      }
      std::size_t const old_size{size()};
      try {
        while(size() < new_size) {
          unchecked_emplace_back();
        }
      } catch(...) {
        resize(old_size);
        throw;
      }
    }
  }

  /** @brief Changes the vector's size to the specified one.
   *
   * If the new size is less than the old one, elements at the end are erased. If the new size
   * is greater than the old size, copies of the specified value are inserted at the end.
   *
   * May also throw whatever is thrown by the element's copy construction.
   *
   * Strong exception safety guarantee.
   *
   * The past-the-end iterator is invalidated. If the size is decreased, iterators to erased
   * elements are also invalidated.
   *
   * @throws std::bad_alloc If the new size exceeds the vector capacity. */
  constexpr void resize(std::size_t new_size, T const& value)
  requires std::is_copy_constructible_v<T>
  {
    if(size() > new_size) {
      this->erase(cbegin() + new_size, cend());
    } else if(size() < new_size) {
      std::size_t const old_size{size()};
      try {
        this->insert(cend(), new_size - size(), value);
      } catch(...) {
        this->resize(old_size);
        throw;
      }
    }
  }
};

// Swap (standalone function) /////////////////////////////////////////////////////////////////

template<inplace_vector_element T, std::size_t N>
constexpr void swap(
  inplace_vector<T, N>& a,
  inplace_vector<T, N>& b
)
noexcept(N == 0 || std::is_nothrow_swappable_v<T> && std::is_nothrow_move_constructible_v<T>)
{
  a.swap(b);
}

// Erasure (standalone functions) /////////////////////////////////////////////////////////////

/** @brief Deletes all elements from the given vector that compare equal to the given one. */
template<inplace_vector_element T, std::size_t N, std::equality_comparable_with<T> U = T>
constexpr std::size_t erase(inplace_vector<T, N>& vec, U const& value)
noexcept(
  N == 0
  || std::is_nothrow_move_assignable_v<T>
  && util::is_nothrow_equality_comparable_with_v<T, U>
)
{
  auto const new_end{std::ranges::remove(vec, value)};
  auto const num_erased{util::asserted_cast<std::size_t>(vec.cend() - new_end)};
  vec.erase(new_end, vec.cend());
  return num_erased;
}

/** @brief Deletes all elements from the given vector that satisfy the given predicate. */
template<inplace_vector_element T, std::size_t N, typename Pred>
constexpr std::size_t erase_if(inplace_vector<T, N>& vec, Pred pred)
noexcept(
  N == 0
  || std::is_nothrow_move_assignable_v<T>
  && noexcept(pred(std::declval<T&>()))
  && noexcept(pred(std::declval<T const&>()))
  && noexcept(pred(std::declval<T&&>()))
  && noexcept(pred(std::declval<T const&&>()))
)
requires requires(T& t, T const& tc) {
  { pred(t) } -> std::convertible_to<bool>;
  { pred(tc) } -> std::convertible_to<bool>;
  { pred(std::move(t)) } -> std::convertible_to<bool>;
  { pred(std::move(tc)) } -> std::convertible_to<bool>;
}
{
  auto const new_end{std::remove_if(vec.begin(), vec.end(), std::move(pred))};
  auto const num_erased{vec.cend() - new_end};
  vec.erase(new_end, vec.cend());
  return num_erased;
}

// Comparison /////////////////////////////////////////////////////////////////////////////////

/** @brief Compares the vectors for equality. */
template<inplace_vector_element T, std::size_t N>
requires std::equality_comparable<T>
[[nodiscard]] constexpr bool operator==(
  inplace_vector<T, N> const& a,
  inplace_vector<T, N> const& b
)
noexcept(N == 0 || util::is_nothrow_equality_comparable_v<T>)
{
  return std::ranges::equal(a, b);
}

namespace detail_ {

template<inplace_vector_element T, std::size_t N>
requires std::three_way_comparable<T> || requires(T const& t) { t < t; }
[[nodiscard]] constexpr bool inplace_vector_three_way_cmp_noexcept() noexcept {
  if(N == 0) {
    return true;
  }
  if constexpr(std::three_way_comparable<T>) {
    return noexcept(std::declval<T const&>() <=> std::declval<T const&>());
  } else {
    return noexcept(std::declval<T const&>() < std::declval<T const&>());
  }
}

} // detail_

/** @brief Compares the vectors lexicographically. */
template<inplace_vector_element T, std::size_t N>
requires std::three_way_comparable<T> || requires(T const& t) { t < t; }
[[nodiscard]] constexpr auto operator<=>(
  inplace_vector<T, N> const& a,
  inplace_vector<T, N> const& b
)
noexcept(detail_::inplace_vector_three_way_cmp_noexcept<T, N>())
{
  return std::lexicographical_compare_three_way(a.cbegin(), a.cend(), b.cbegin(), b.cend(),
    [](T const& a, T const& b)
        noexcept(detail_::inplace_vector_three_way_cmp_noexcept<T, N>()) {
      if constexpr(std::three_way_comparable<T>) {
        return a <=> b;
      } else {
        if(a < b) {
          return std::weak_ordering::less;
        } else if(b < a) {
          return std::weak_ordering::greater;
        } else {
          return std::weak_ordering::equivalent;
        }
      }
    });
}

} // util

#endif
