#include "driver.hpp"
#include "nycleus/diagnostic_logger.hpp"
#include "nycleus/entity.hpp"
#include "nycleus/entity_bag.hpp"
#include "nycleus/global_name.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/lexer.hpp"
#include "nycleus/parser.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/task_runner.hpp"
#include "nycleus/token.hpp"
#include "nycleus/type_utils.hpp"
#include "nycleus_llvm/codegen.hpp"
#include "nycleus_llvm/target.hpp"
#include "unicode/encoding.hpp"
#include "unicode/windows.hpp"
#include "util/cow.hpp"
#include "util/downcast.hpp"
#include "util/generator.hpp"
#include "util/nums.hpp"
#include "util/thread_pool.hpp"
#include "util/util.hpp"
#include "diagnostic_print.hpp"
#include "opt.hpp"
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/raw_os_ostream.h>
#include <cassert>
#include <cstdint>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <ios>
#include <istream>
#include <memory>
#include <new>
#include <optional>
#include <ostream>
#include <ranges>
#include <stdexcept>
#include <stop_token>
#include <string>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

#ifdef _WIN32
  #include "util/ranges.hpp"
#else
  #include "util/u8compat.hpp"
  using namespace util::u8compat_literals;
#endif

namespace {

/** @throws std::bad_alloc
 * @throws std::length_error
 * @throws std::filesystem::filesystem_error
 * @throws std::ios_base::failure */
std::unique_ptr<std::ostream> codegen_stream(
  std::filesystem::path const& base,
  nycleus::global_name const& name
) {
  std::u8string filename;
  if(name.is_initializer) {
    filename += '.';
  }
  filename += name.name;
  for(auto const& nested_part: name.nested_parts) {
    filename += '.';
    std::visit(util::overloaded{
      [&](nycleus::global_name::in_function const& p) {
        filename += util::to_u8string(p.index);
      },

      [&](nycleus::global_name::in_class const& p) {
        filename += p.name;
      },
    }, nested_part);
  }

  #ifdef _WIN32
    auto encoded_filename{filename
      | unicode::utf8_decode(unicode::eh_assume_valid{})
      | unicode::utf16_encode<wchar_t>() | util::range_to<std::wstring>()};
  #else
    std::string encoded_filename{util::as_char(filename)};
  #endif

  std::filesystem::create_directories(base);

  auto stream{std::make_unique<std::ofstream>()};
  stream->open(base / (encoded_filename +
    #ifdef _WIN32
      L".ll"
    #else
      u8".ll"_ch
    #endif
    ), std::ios_base::trunc | std::ios_base::out | std::ios_base::binary);

  try {
    stream->exceptions(std::ios_base::failbit | std::ios_base::badbit);
  } catch(std::bad_alloc const&) {
    throw;
  } catch(std::length_error const&) {
    throw;
  } catch(std::ios_base::failure const&) {
    throw;
  } catch(...) {
    std::throw_with_nested(std::ios_base::failure{"codegen failure"});
  }

  return stream;
}

} // (anonymous)

bool run_compiler(
  options_config_t const& opts,
  diag_message_printer& diag_printer,
  nycleus_llvm::target& tgt
) {
  nycleus::diagnostic_logger dt{opts.get<opt::error_limit>()};
  nycleus::type_registry tr;

  // Since we always do a full rebuild, erase the output directory before
  // beginning.
  // TODO: Incremental builds
  std::filesystem::remove_all(*opts.get<opt::temp_dir>());

  std::uintptr_t const jobs{opts.get<opt::jobs>()
    ? *opts.get<opt::jobs>() : default_num_jobs()};

  nycleus::entity_bag bag;

  bool too_many_errors{false};
  try {
     {
      std::vector<nycleus::entity_bag> bags(jobs);

      util::run_on_thread_pool(
        bags,
        opts.get_unnamed(),
        [&dt](
          std::stop_token,
          nycleus::entity_bag& bag,
          std::basic_string_view<unicode::native_char> file
        ) {
          std::filesystem::path path{file};
          std::ifstream f;
          f.exceptions(std::ios_base::failbit | std::ios_base::badbit);
          f.open(path, std::ios_base::in | std::ios_base::binary);

          auto token_range{[](
            std::istream& f,
            std::filesystem::path const& path
          ) -> util::generator<nycleus::token, nycleus::token,
              std::allocator<void>> {
            auto decoded_view{
              std::ranges::subrange{
                std::istreambuf_iterator<char>{f},
                std::istreambuf_iterator<char>{}
              } | unicode::decode(
                nycleus::utf8_counted<char>{std::optional{util::cow{path}}},
                unicode::eh_throw{})
            };

            co_yield util::elements_of{std::ranges::ref_view{decoded_view}
              | nycleus::lex()};

            co_yield nycleus::token{nycleus::tok::eof{},
              std::move(decoded_view).get_state().finish()};
          }(f, path)};

          nycleus::parse(
            token_range,
            [&](
              std::u8string name,
              nycleus::hlir_ptr<nycleus::hlir::def> def
            ) {
              bag.process_global_def(std::move(def), {std::move(name)}, dt);
            },
            dt
          );
        }
      );

      bag = nycleus::entity_bag::merge(bags);
    }

    {
      nycleus::task_runner runner{bag};
      util::run_in_threads(
        std::views::iota(std::uintptr_t{0}, jobs),
        [&runner, &dt, &bag, &tgt, &tr](
          std::stop_token stop,
          std::uintptr_t thread_index
        ) {
          // We need to report duplicate entities, but this can be done in
          // parallel with task_runner. So just do it on one of the worker
          // threads before starting task_runner.
          if(thread_index == 0) {
            bag.report_duplicates(dt);
          }

          runner.run(dt, bag, tgt, tr, std::move(stop));
        }
      );
    }

    if(!dt.has_errors()) {
      std::filesystem::path base{*opts.get<opt::temp_dir>()};
      util::run_on_thread_pool(
        std::views::iota(std::uintptr_t{0}, jobs),
        bag.entities(),
        [&tgt, &base](
          std::stop_token,
          std::uintptr_t,
          nycleus::named_entity& entity
        ) {
          switch(entity.get_entity_kind()) {
            case nycleus::named_entity::entity_kind_t::function: {
              auto& fn{util::downcast<nycleus::function&>(entity)};

              try {
                llvm::LLVMContext llvm_ctx;
                std::unique_ptr<llvm::Module> mod{
                  nycleus_llvm::codegen_function(llvm_ctx, tgt, fn)};

                auto const os{codegen_stream(base, fn.name)};
                assert(os);
                llvm::raw_os_ostream raw_os{*os};
                mod->print(raw_os, nullptr);
              } catch(std::bad_alloc const&) {
                throw;
              } catch(std::length_error const&) {
                throw;
              } catch(std::ios_base::failure const&) {
                throw;
              } catch(nycleus_llvm::codegen_error const&) {
                throw;
              } catch(...) {
                std::throw_with_nested(nycleus_llvm::codegen_error{});
              }

              break;
            }

            case nycleus::named_entity::entity_kind_t::global_var: {
              auto& var{util::downcast<nycleus::global_var&>(entity)};

              try {
                llvm::LLVMContext llvm_ctx;
                std::unique_ptr<llvm::Module> mod{
                  nycleus_llvm::codegen_global_var(llvm_ctx, tgt, var)};

                auto const os{codegen_stream(base, var.name)};
                assert(os);
                llvm::raw_os_ostream raw_os{*os};
                mod->print(raw_os, nullptr);
              } catch(std::bad_alloc const&) {
                throw;
              } catch(std::length_error const&) {
                throw;
              } catch(std::ios_base::failure const&) {
                throw;
              } catch(nycleus_llvm::codegen_error const&) {
                throw;
              } catch(...) {
                std::throw_with_nested(nycleus_llvm::codegen_error{});
              }

              break;
            }

            case nycleus::named_entity::entity_kind_t::class_type:
              break;
          }
        }
      );
    }
  } catch(nycleus::too_many_errors const&) {
    assert(dt.has_errors());
    too_many_errors = true;
  }

  bool const has_errors{dt.has_errors()};

  diag_printer.print(std::move(dt).extract_diags(), too_many_errors);

  return !has_errors;
}
