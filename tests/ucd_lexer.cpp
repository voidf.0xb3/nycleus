#include <boost/test/unit_test.hpp>

#include "unicode/normal.hpp"
#include "unicode/props.hpp"
#include <algorithm>
#include <cstdint>
#include <ranges>

#include "unicode/ucd/decimal_list.hpp"
#include "unicode/ucd/decomposing_list.hpp"
#include "unicode/ucd/non_starter_list.hpp"
#include "unicode/ucd/white_space_list.hpp"

BOOST_AUTO_TEST_SUITE(ucd_tests)
BOOST_AUTO_TEST_SUITE(lexer)

// This file contains tests that verify various assumptions about the Unicode
// data that must be held in order for the lexer code to be valid. The following
// discussion uses notation from the Nycleus specification, particularly the
// `lex` and `tokenize` functions.
//
// The lexer operates simply as follows: at each point, it extracts the longest
// prefix of the remaining file that counts as a token, turns it into a token,
// and continues after the consumed prefix. (For the purposes of this
// discussion, comments form special pseudo-tokens, as do individual whitespace
// code points.) The token kind is usually determined by examining the first
// code point of the remaining part of the input and checking which of the
// following classes it belongs to:
// - code points with the White_Space property (the whitespace class) form
//   whitespace pseudo-tokens;
// - U+0023 `#` NUMBER SIGN begins a comment pseudo-token;
// - U+0022 `"` QUOTATION MARK begins a string literal token;
// - U+0027 `'` APOSTROPHE begins a character literal token;
// - U+0024 `$` DOLLAR SIGN begins a word token of the identifier kind;
// - U+005F `_` LOW LINE begins a word token of the extension kind;
// - code points with the XID_Start property (the XID_Start class) begin word
//   tokens of the unprefixed kind;
// - code points with the Numeric_Type property having the value Decimal (the
//   decimal class) begin numeric literal tokens;
// - various Basic Latin punctuation tokens begin punctuation tokens (the
//   punctuation class; this excludes U+002E `.` FULL STOP);
// - U+002E `.` FULL STOP is ambiguous: if it is followed by a decimal digit, it
//   begins a numeric literal, otherwise it begins a punctuation token;
// - all other code points are in the invalid class and trigger a lexing error.
//
// In order for the `lex` function to be well-defined, all of the above classes
// must be disjoint. Also, in order to resolve ambiguities for tokens starting
// with `.`, decimal digits must be disjoint from anything that could continue
// the punctuation token after a `.`; but since such a code point could only be
// another `.`, this already holds true by virtue of the above assumptions.
//
// The following tests verify that the above classes are disjoint:

BOOST_AUTO_TEST_CASE(white_space_disjoint) {
  for(char32_t const ch: unicode::ucd::white_space_cps) {
    BOOST_TEST((ch != U'!'));
    BOOST_TEST((ch != U'"'));
    BOOST_TEST((ch != U'#'));
    BOOST_TEST((ch != U'$'));
    BOOST_TEST((ch != U'%'));
    BOOST_TEST((ch != U'&'));
    BOOST_TEST((ch != U'\''));
    BOOST_TEST((ch != U'('));
    BOOST_TEST((ch != U')'));
    BOOST_TEST((ch != U'*'));
    BOOST_TEST((ch != U'+'));
    BOOST_TEST((ch != U','));
    BOOST_TEST((ch != U'-'));
    BOOST_TEST((ch != U'.'));
    BOOST_TEST((ch != U'/'));
    BOOST_TEST((ch != U':'));
    BOOST_TEST((ch != U';'));
    BOOST_TEST((ch != U'<'));
    BOOST_TEST((ch != U'='));
    BOOST_TEST((ch != U'>'));
    BOOST_TEST((ch != U'?'));
    BOOST_TEST((ch != U'@'));
    BOOST_TEST((ch != U'['));
    BOOST_TEST((ch != U']'));
    BOOST_TEST((ch != U'^'));
    BOOST_TEST((ch != U'_'));
    BOOST_TEST((ch != U'{'));
    BOOST_TEST((ch != U'|'));
    BOOST_TEST((ch != U'}'));
    BOOST_TEST((ch != U'~'));
    BOOST_TEST(!unicode::xid_start(ch));
    BOOST_TEST(unicode::decimal_value(ch) == unicode::no_decimal_value);
  }
}

BOOST_AUTO_TEST_CASE(decimal_disjoint) {
  for(char32_t const ch_base: unicode::ucd::decimal_cps) {
    for(auto const i: std::views::iota(static_cast<std::uint32_t>(0),
        static_cast<std::uint32_t>(10))) {
      char32_t const ch{ch_base + i};
      BOOST_TEST((ch != U'!'));
      BOOST_TEST((ch != U'"'));
      BOOST_TEST((ch != U'#'));
      BOOST_TEST((ch != U'$'));
      BOOST_TEST((ch != U'%'));
      BOOST_TEST((ch != U'&'));
      BOOST_TEST((ch != U'\''));
      BOOST_TEST((ch != U'('));
      BOOST_TEST((ch != U')'));
      BOOST_TEST((ch != U'*'));
      BOOST_TEST((ch != U'+'));
      BOOST_TEST((ch != U','));
      BOOST_TEST((ch != U'-'));
      BOOST_TEST((ch != U'.'));
      BOOST_TEST((ch != U'/'));
      BOOST_TEST((ch != U':'));
      BOOST_TEST((ch != U';'));
      BOOST_TEST((ch != U'<'));
      BOOST_TEST((ch != U'='));
      BOOST_TEST((ch != U'>'));
      BOOST_TEST((ch != U'?'));
      BOOST_TEST((ch != U'@'));
      BOOST_TEST((ch != U'['));
      BOOST_TEST((ch != U']'));
      BOOST_TEST((ch != U'^'));
      BOOST_TEST((ch != U'_'));
      BOOST_TEST((ch != U'{'));
      BOOST_TEST((ch != U'|'));
      BOOST_TEST((ch != U'}'));
      BOOST_TEST((ch != U'~'));
      BOOST_TEST(!unicode::xid_start(ch));
    }
  }
}

// The Nycleus specification says that source files must be converted to NFC
// before lexing takes place. We, however, lex unnormalized files, and then, if
// necessary, normalize the extracted tokens. So we need to prove that the
// result will be equivalent. For that, we make a number of assumptions, which
// are tested below.

// 1. The following code points are starters, do not have canonical
//    decompositions, and do not appear in canonical decompositions of other
//    code points:
//    - Basic Latin code points, except Latin letters;
//    - White_Space code points;
//    - decimal digits;
//    - code points involved in mandatory line breaks;
//    - U+200C ZERO WIDTH NON-JOINER;
//    - U+200D ZERO WIDTH JOINER;
//    - U+202A LEFT-TO-RIGHT EMBEDDING;
//    - U+202B RIGHT-TO-LEFT EMBEDDING;
//    - U+202C POP DIRECTIONAL FORMATTING;
//    - U+202D LEFT-TO-RIGHT OVERRIDE;
//    - U+202E RIGHT-TO-LEFT OVERRIDE;
//    - U+2066 LEFT-TO-RIGHT ISOLATE;
//    - U+2067 RIGHT-TO-LEFT ISOLATE;
//    - U+2068 FIRST STRONG ISOLATE;
//    - U+2069 POP DIRECTIONAL ISOLATE.
//    With the following exceptions:
//    - U+2000 EN QUAD decomposes to U+2002 EN SPACE;
//    - U+2001 EM QUAD decomposes to U+2003 EM SPACE;
//    - U+2260 NOT EQUAL TO decomposes to U+003D EQUALS SIGN, U+0338 COMBINING
//      LONG SOLIDUS OVERLAY;
//    - U+226E NOT LESS-THAN decomposes to U+003C LESS-THAN SIGN, U+0338
//      COMBINING LONG SOLIDUS OVERLAY;
//    - U+226F NOT GREATER-THAN decomposes to U+003E GREATER-THAN SIGN, U+0338
//      COMBINING LONG SOLIDUS OVERLAY.
//
// Note that the above assumption is also tested for bidirectional formatting
// codes. Those are subject to certain restrictions within comments and string
// and character literals. The specification imposes those restrictions on the
// normalized form, but we check the restrictions on the unnormalized form; the
// assumption ensures that that is valid.

BOOST_AUTO_TEST_CASE(inert_cps) {
  auto check_inert_1{[](char32_t ch) {
    BOOST_TEST(unicode::ccc(ch) == unicode::ccc_t{0});

    auto const decomp{unicode::detail_::canon_decompose(ch)};
    BOOST_TEST(decomp.size() == 1);
    BOOST_TEST((decomp.at(0) == ch));
  }};

  for(char32_t const ch: std::views::iota(U'\u0000', U'\u0080')) {
    if(U'A' <= ch && ch <= U'Z' || U'a' <= ch && ch <= U'z') {
      continue;
    }
    check_inert_1(ch);
  }
  for(char32_t const ch: unicode::ucd::white_space_cps) {
    if(ch == U'\u2000' || ch == U'\u2001') {
      continue;
    }
    check_inert_1(ch);
  }
  for(char32_t const base_ch: unicode::ucd::decimal_cps) {
    for(char32_t const ch: std::views::iota(base_ch, base_ch + 10)) {
      check_inert_1(ch);
    }
  }
  check_inert_1(U'\u0085');
  check_inert_1(U'\u200C');
  check_inert_1(U'\u200D');
  check_inert_1(U'\u2028');
  check_inert_1(U'\u2029');
  check_inert_1(U'\u202A');
  check_inert_1(U'\u202B');
  check_inert_1(U'\u202C');
  check_inert_1(U'\u202D');
  check_inert_1(U'\u202E');
  check_inert_1(U'\u2066');
  check_inert_1(U'\u2067');
  check_inert_1(U'\u2068');
  check_inert_1(U'\u2069');

  for(char32_t const ch: unicode::ucd::decomposing_cps) {
    if(  ch == U'\u2000'
      || ch == U'\u2001'
      || ch == U'\u2260'
      || ch == U'\u226E'
      || ch == U'\u226F'
    ) {
      continue;
    }
    auto const decomp{unicode::detail_::canon_decompose(ch)};
    for(char32_t const decomp_ch: decomp) {
      BOOST_TEST((ch > U'\u0080'
        || (U'A' <= ch && ch <= U'Z') || (U'a' <= ch && ch <= U'z')));
      BOOST_TEST(!unicode::white_space(decomp_ch));
      BOOST_TEST(unicode::decimal_value(decomp_ch)
        == unicode::no_decimal_value);
      BOOST_TEST((ch != U'\u0085'));
      BOOST_TEST((ch != U'\u200C'));
      BOOST_TEST((ch != U'\u200D'));
      BOOST_TEST((ch != U'\u202A'));
      BOOST_TEST((ch != U'\u202B'));
      BOOST_TEST((ch != U'\u202C'));
      BOOST_TEST((ch != U'\u202D'));
      BOOST_TEST((ch != U'\u202E'));
      BOOST_TEST((ch != U'\u2066'));
      BOOST_TEST((ch != U'\u2067'));
      BOOST_TEST((ch != U'\u2068'));
      BOOST_TEST((ch != U'\u2069'));
      BOOST_TEST((ch != U'\u2028'));
      BOOST_TEST((ch != U'\u2029'));
    }
  }
}

// 2. All non-starters are in the invalid class, i. e., they cannot begin
//    tokens.

BOOST_AUTO_TEST_CASE(non_starters_are_invalid) {
  for(char32_t const ch: unicode::ucd::non_starter_cps) {
    BOOST_TEST((ch != U'!'));
    BOOST_TEST((ch != U'"'));
    BOOST_TEST((ch != U'#'));
    BOOST_TEST((ch != U'$'));
    BOOST_TEST((ch != U'%'));
    BOOST_TEST((ch != U'&'));
    BOOST_TEST((ch != U'\''));
    BOOST_TEST((ch != U'('));
    BOOST_TEST((ch != U')'));
    BOOST_TEST((ch != U'*'));
    BOOST_TEST((ch != U'+'));
    BOOST_TEST((ch != U','));
    BOOST_TEST((ch != U'-'));
    BOOST_TEST((ch != U'.'));
    BOOST_TEST((ch != U'/'));
    BOOST_TEST((ch != U':'));
    BOOST_TEST((ch != U';'));
    BOOST_TEST((ch != U'<'));
    BOOST_TEST((ch != U'='));
    BOOST_TEST((ch != U'>'));
    BOOST_TEST((ch != U'?'));
    BOOST_TEST((ch != U'@'));
    BOOST_TEST((ch != U'['));
    BOOST_TEST((ch != U']'));
    BOOST_TEST((ch != U'^'));
    BOOST_TEST((ch != U'_'));
    BOOST_TEST((ch != U'{'));
    BOOST_TEST((ch != U'|'));
    BOOST_TEST((ch != U'}'));
    BOOST_TEST((ch != U'~'));
    BOOST_TEST(!unicode::white_space(ch));
    BOOST_TEST(!unicode::xid_start(ch));
    BOOST_TEST(unicode::decimal_value(ch) == unicode::no_decimal_value);
  }
}

// 3. All non-starters have the XID_Continue property.

BOOST_AUTO_TEST_CASE(xid_non_starters) {
  for(char32_t const ch: unicode::ucd::non_starter_cps) {
    BOOST_TEST(unicode::xid_continue(ch));
  }
}

// 4. If a XID_Start code point has a canonical decomposition, it starts with a
//    XID_Start code point.

BOOST_AUTO_TEST_CASE(xid_start_decomp) {
  for(char32_t const ch: unicode::ucd::decomposing_cps) {
    if(!unicode::xid_start(ch)) {
      continue;
    }
    auto const decomp{unicode::detail_::canon_decompose(ch)};
    BOOST_TEST(unicode::xid_start(decomp.at(0)));
  }
}

// 5. If a XID_Continue code point has a canonical decomposition, it consists
//    solely of XID_Continue code points.

BOOST_AUTO_TEST_CASE(xid_continue_decomp) {
  for(char32_t const ch: unicode::ucd::decomposing_cps) {
    if(!unicode::xid_continue(ch)) {
      continue;
    }
    auto const decomp{unicode::detail_::canon_decompose(ch)};
    for(char32_t const decomp_ch: decomp) {
      BOOST_TEST(unicode::xid_continue(decomp_ch));
    }
  }
}

// 6. If a code point has a canonical decomposition that starts with a XID_Start
//    code point, the original code point is also a XID_Start code point.

BOOST_AUTO_TEST_CASE(xid_start_inverse_decomp) {
  for(char32_t const ch: unicode::ucd::decomposing_cps) {
    auto const decomp{unicode::detail_::canon_decompose(ch)};
    if(unicode::xid_start(decomp.at(0))) {
      BOOST_TEST(unicode::xid_start(ch));
    }
  }
}

// 7. If a code point has a canonical decomposition that starts with a
//    XID_Continue code point, the original code point is also a XID_Continue
//    code point. (Together with assumption 5, this implies that all other code
//    points in the decomposition are also XID_Continue code points.)

BOOST_AUTO_TEST_CASE(xid_continue_inverse_decomp) {
  for(char32_t const ch: unicode::ucd::decomposing_cps) {
    auto const decomp{unicode::detail_::canon_decompose(ch)};
    if(unicode::xid_continue(decomp.at(0))) {
      BOOST_TEST(unicode::xid_continue(ch));
    }
  }
}

// 8. If a code point has a non-trivial canonical decomposition, it cannot
//    consist solely of Basic Latin letters, except that U+212A KELVIN SIGN
//    decomposes to U+004B LATIN CAPITAL LETTER K.

BOOST_AUTO_TEST_CASE(basic_latin_inverse_decomp) {
  for(char32_t const ch: unicode::ucd::decomposing_cps) {
    if(ch == U'\u212A') {
      continue;
    }
    auto const decomp{unicode::detail_::canon_decompose(ch)};
    BOOST_TEST(!std::ranges::all_of(decomp, [](char32_t ch) noexcept {
      return U'A' <= ch && ch <= U'Z' || U'a' <= ch && ch <= U'z';
    }));
  }
}

////////////////////////////////////////////////////////////////////////////////
// Armed with the above assumptions, we now prove that our implementation of the
// lexer is valid in accordance with the specification. For a more precise
// statement, we need to slightly adjust our definition of `lex` and `tokenize`.
//
// First, `tokenize` is now defined as a partial function from 𝕌* to Tok*; all
// inputs previously in its domain now map to a one-token string. Furthermore,
// inputs consisiting of a single whitespace code point, as well as inputs that
// are comments, map to an empty token string. (While it is technically context-
// dependent whether a string is a comment, UAX #14 makes the context limited
// in such a way that mandatory line breaks can be recognized within this
// model.) This allows us to handle discarding whitespace and comments
// analogously to extracting tokens.
//
// Second, the definition of `lex` is adjusted to deal with unnormalized inputs.
// We now define it as follows:
// lex(|u|, uvw) := tokenize(NFC(v)) lex(|uv|, uvw),
// where u,v,w ∈ 𝕌*, |vw| > 0, and v is the longest prefix of vw such that
// v ∈ dom(tokenize); also, lex(|w|, w) := ε, where w ∈ 𝕌*.
//
// For this definition to be valid, it is necessary that v ∈ dom(tokenize)
// implies NFC(v) ∈ dom(tokenize). This is proven below (well-formedness);
// unfortunately, some numeric literal tokens can be exceptions. (There aren't
// actually any exceptions with the current version of Unicode, but there do not
// appear to be any guarantees that that will always remain true, so the
// possibility needs to be handled.) To account for this, we recognize that
// numeric literals are processed in two steps: first we collect code points
// that match a certain formal grammar, then extract some additional structure
// from the collected code points. So whenever v ∈ dom(tokenize) but
// NFC(v) ∉ dom(tokenize), we extract that additional structure from NFC(v) and
// treat the resulting numeric literal token as if it were tokenize(NFC(v)). (We
// don't actually add it to the domain of `tokenize`, so it is not recognized as
// a token if encountered in the actual input. This is consistent with how the
// code is written.) It can be shown that any such token is an invalid numeric
// literal.
//
// Third, the sets of string and character literals are expanded as follows. A
// string literal starts with a `"` code point and ends with the next `"` code
// point that is not immediately preceded by a run consisting of an odd number
// of `\` code points. A character literal is the same thing, but with `'` code
// points instead of `"` code points.
//
// Clearly, this syntax forms a superset of valid string/character literal
// tokens prescribed in the specification. All string literal tokens under this
// new syntax that are not valid under the specfication are considered
// representations of a special invalid string literal token, and likewise for
// character literals.
//
// We shall also refer to the `<`, `=`, `>` as non-negated comparison code
// points and to the `≮`, `≠`, `≯` as negated comparison code points.
//
// Under these new definitions, we wish to prove that for any u ∈ 𝕌*, it holds
// that lex(0, u) = lex(0, NFC(u)). (This equality is to be understood in the
// strong sense: either both sides are undefined, or they are both defined and
// equal.) Unfortunately, that is not true. So instead, we prove below a weaker
// statement (equivalence), namely, that one of the following must be true:
// - Each of the lex(0, u) and lex(0, NFC(u)) is either undefined or has a value
//   that contains an invalid numeric literal.
// - Both lex(0, u) and lex(0, NFC(u)) are defined and have the same value.
// This is denoted lex(0, u) ≈ lex(0, NFC(u)). We allow changing the output if
// it contains invalid numeric literals, as long as the changed output still has
// invalid numeric literals, since in this situation the program is ill-formed
// anyway. We also allow changing an invalid input into an input that produces
// invalid numeric literals or vice versa, for the same reason.
//
// The above expansion of the classes of string and character literals affects
// the proof as follows. By definition of string and character literals, such a
// literal has no proper prefixes that are valid tokens and is not itself a
// proper prefix of any valid token. Therefore, the only effect of expanding
// these token classes is that some inputs that used to be invalid now produce
// output that contains invalid string or character literals; previously valid
// inputs remain valid and produce the same outputs. Thus, the statement we seek
// to prove with expanded string and character literal classes is stronger than
// the corresponding statement with non-expanded ones (which is what we need to
// prove).
//
// Conversion to NFC consists of three steps: full decomposition, the Canoncial
// Reordering Algorithm (CRA), and the Canonical Composition Algorithm (CCA).
// Thus: NFC(u) = CCA(CRA(fullDecomp(u))). We define decomp: 𝕌* → 𝕌* as a
// function that replaces each decomposable code point with its canonical
// decomposition. Then `fullDecomp` consists of repeatedly applying `decomp`
// until we arrive at a fixed point.
//
// To prove well-formedness, we now seek to prove the three steps:
// 1. v ∈ dom(tokenize) implies decomp(v) ∈ dom(tokenize);
// 2. v ∈ dom(tokenize) implies CRA(v) ∈ dom(tokenize);
// 3. v ∈ dom(tokenize) implies CCA(v) ∈ dom(tokenize).
// The above three steps do not hold for certain (invalid) numeric literals;
// those cases are handled separately below.
//
// To prove equivalence, we also seek to prove the three steps (also understood
// in the strong sense as above, and using the `≈` notation from above):
// 1. lex(0, u) ≈ lex(0, decomp(u));
// 2. lex(0, u) ≈ lex(0, CRA(u));
// 3. lex(0, u) ≈ lex(0, CCA(u)).
//
// To prove each step, we seek to prove the following: given u,v,w ∈ 𝕌*,
// |v| > 0, t ∈ Tok*, |t| ≤ 1, lex(|u|, uvw) ≈ t lex(|uv|, uvw), f is one of
// decomp, CRA, CCA, and f(uvw) = f(u)f(vw), it is the case that
// f(vw) = f(v)f(w) and lex(|f(u)|, f(uvw)) ≈ t lex(|f(u)f(v)|, f(uvw)). That
// follows (via induction by |vw|) from the following two claims:
// 1. If v is the longest prefix of vw that is a valid token, then
//    f(vw) = f(v)f(w) and f(v) is the longest prefix of f(vw) that is a valid
//    token.
// 2. If vw has no prefix that is a valid token, then f(vw) has no prefix that
//    is a valid token.
// These claims are not always true; any exceptions are handled separately
// below.
//
////////////////////////////////////////////////////////////////////////////////
// STEP 1: DECOMPOSITION.
// Note that decomp(vw) = decomp(v)decomp(w) is always true.
//
// We now prove that v ∈ dom(tokenize) implies decomp(v) ∈ dom(tokenize)
// (well-formedness) by considering the string v, and simultaneously prove
// equivalence by considering the string vw from the two equivalence claims
// above.
//
// If the original first code point is in the whitespace class, it alone forms a
// whitespace pseudo-token. Per assumption 1, it does not decompose, unless it
// is U+2000 or U+2001, which decompose to U+2002 and U+2003, respectively,
// which are also whitespace code points and also form the same whitespace
// pseudo-token.
//
// If the original first code point is `#`, it starts a comment pseudo-token.
// The end of the pseudo-token is determined by examining patterns formed by the
// code points `#`, `+`, `*`, and those involved in mandatory line breaks. Per
// assumption 1, none of them are added or removed by decomposition.
//
// If the original first code point is `"` or `'`, it starts a string or a
// character literal. The end of the literal is determined by examining patterns
// formed by the code points `"`, `'`, and `\`. Per assumption 1, none of them
// are added or removed by decomposition.
//
// If the original first code point is `$` or `_`, it starts an identifier or
// extension word. The word consists of all immediately following code points
// that either have the XID_Continue property or are `$`; per assumption 5, all
// decomposition of XID_Continue code points consist of XID_Continue code
// points, and the `$` code point does not decompose. So an identifier or
// extension word token remains a token, thus proving well-formedness; for
// equivalence, we need to further note that the first code point that was not
// part of the extracted token before decomposition will not decompose to
// anything that starts with a `$` or `_`, per assumption 1, or to anything that
// starts with a XID_Continue code point, per assumption 7.
//
// If the original first code point is in the XID_Start class, it starts an
// unprefixed word. The word consists of all immediately following code points
// that either have the XID_Continue property or are `$`. The reasoning then
// proceeds as for idenitifier and extension words, except that it is also
// necessary to note that if the initial XID_Start code point decomposes, it
// will start with a XID_Start code point per assumption 4, and, since XID_Start
// is a subset of XID_Continue, it will consist of XID_Continue code points per
// assumption 5.
//
// If the original first code point is in the decimal class, or if it is a `.`
// followed by a decimal digit, it starts a numeric literal. The reasoning
// proceeds as for word tokens, except that we also need to take into account
// the context where the `+` and `-` code points are allowed. But if such
// context changed, then the original numeric literal was invalid, since a valid
// numeric literal must consist only of Basic Latin code points and decimal
// digits, none of which have decompositions (the latter per assumption 1); and
// the resulting numeric literal is also invalid, because none of the new code
// points are Basic Latin non-letters or decimal digits (per assumption 1), and
// decompositions must contain some code points other than Basic Latin letters
// (per assumption 8), unless it is the letter `K`, which cannot appear in valid
// numeric literals.
//
// If the original first code point is in the punctuation class, or if it is a
// `.` not followed by a decimal digit, it starts a punctuation token. Since
// none of the code points comprising punctuation tokens decompose,
// well-formedness is satisfied. However, if the original punctuation token was
// followed by a negated comparison code point, decompostion will add a
// non-negated comparison code point, which can alter the extracted punctuation
// token. But in that case, before decomposition, the extracted token is
// followed by a negated comparison code point, and after decomposition, the
// extracted token is followed by U+0338, all of which are in the invalid class;
// therefore, in both of these cases, the input is invalid.
//
// To prove the second part of equivalence, we need to note that decomposition
// of a code point in the invalid class either produces something that starts
// with a code point in the invalid class or, if the original code point is a
// negated comparison code point, it produces a non-negated comparison code
// point, which forms a punctuation token, followed by U+0338, which is also in
// the invalid class, so the input is still invalid.
//
////////////////////////////////////////////////////////////////////////////////
// STEP 2: CANONICAL REORDERING ALGORITHM.
// Note that the CRA consists of a stable sort of all runs of non-starter code
// points by their canonical combining class value. This implies that, given
// u,v ∈ 𝕌*, if u ends with a starter code point or v begins with a starter
// code point, then CRA(uv) = CRA(u)CRA(v).
//
// We need to prove that lex(0, u) ≈ lex(0, CRA(u)). If u begins with a
// non-starter, then so does CRA(u); per assumption 2, they both are in the
// invalid class, so both are undefined. We now limit consideration only to the
// cases where u begins with a starter.
//
// We now prove that v ∈ dom(tokenize) implies CRA(v) ∈ dom(tokenize)
// (well-formedness) by considering the string v, and simultaneously prove
// equivalence by considering the string vw from the two equivalence claims
// above.
//
// If the original first code point is in the whitespace class, it alone forms a
// whitespace pseudo-token. It is a starter per assumption 1, so both
// well-formedness and equivalence follow.
//
// If the original first code point is `#`, it starts a comment pseudo-token.
// The end of the pseudo-token is determined by examining patterns formed by the
// code points `#`, `+`, `*`, and those involved in mandatory line breaks. Per
// assumption 1, all of them are starters, so well-formedness follows. For
// equivalence, we also need to observe that the comment ends with either `#`
// (for multiline comments) or with a code point involved in a mandatory line
// break (for single-line comments), all of which are starters per assumption 1.
//
// If the original first code point is `"` or `'`, it starts a string or a
// character literal. The end of the literal is determined by examining patterns
// formed by the code points `"`, `'`, and `\`. Per assumption 1, all of them
// are starters, so well-formedness follows. For equivalence, we also need to
// observe that the literal ends with either `"` or `'`, which are both
// starters.
//
// If the original first code point is `$` or `_` or is in the XID_Start class,
// it starts a word token. The word consists of all immediately following code
// points that either have the XID_Continue property or are `$`. Reordering them
// clearly has no effect on the validity of the token, so well-formedness
// follows. For equivalence, we also need to observe that w begins with a
// starter; indeed, if it began with a non-starter, then per assumption 3 that
// non-starter would have the XID_Continue property, and therefore could be
// added to the string v to make a longer token (violating the definition of
// `lex`). This proves that the CRA will not change the first code point of w
// (thus ensuring that it is still not in XID_Continue) and also establishes the
// necessary condition for applying induction.
//
// If the original first code point is in the decimal class, or if it is a `.`
// followed by a decimal digit, it starts a numeric literal. The reasoning then
// proceeds as for word tokens, except that it is also necessary to point out
// that code points that determine the context where `+` and `-` are accepted
// are all starters (for decimal digits, this relies upon assumption 1), and are
// therefore unaffected by the CRA.
//
// If the original first code point is in the punctuation class, or if it is a
// `.` not followed by a decimal digit, it starts a punctuation token. Code
// points comprising punctuation tokens are all starters, so they are unaffected
// by the CRA. This directly implies well-formedness; this also implies that in
// the equivalence proof, the string v ends with a starter; and this also
// further implies that applying the CRA to the string w cannot extend the token
// (if its first code point is changed, it is changed to a non-starter, which
// cannot continue a punctuation token), which proves equivalence.
//
// For the second part of equivalence, we simply note that the first code point
// of v is, by assumption, a starter, and is therefore the first code point of
// CRA(v), which proves the desired claim.
//
////////////////////////////////////////////////////////////////////////////////
// STEP 3: CANONICAL COMPOSITION ALGORITHM.
// We now prove that v ∈ dom(tokenize) implies CCA(v) ∈ dom(tokenize)
// (well-formedness) by considering the string v, and simultaneously prove
// equivalence by considering the string vw from the two equivalence claims
// above.
//
// If the original first code point is in the whitespace class, it alone forms a
// whitespace pseudo-token. Per assumption 1, it is a starter and does not
// compose with anything, so both well-formedness and equivalence follow.
//
// If the original first code point is `#`, it starts a comment pseudo-token.
// The end of the pseudo-token is determined by examining patterns formed by the
// code points `#`, `+`, `*`, and those involved in mandatory line breaks. Per
// assumption 1, none of them compose with anything, so well-formedness follows.
// For equivalence, we also need to observe that the comment ends with either
// `#` (for multiline comments) or with a code point involved in a mandatory
// line break (for single-line comments), none of which compose with anything
// per assumption 1.
//
// If the original first code point is `"` or `'`, it starts a string or a
// character literal. The end of the literal is determined by examining patterns
// formed by the code points `"`, `'`, and `\`. Per assumption 1, none of them
// compose with anything, so well-formedness follows. For equivalence, we also
// need to observe that the literal ends with either `"` or `'`, none with which
// compose with anything per assumption 1.
//
// If the original first code point is `$` or `_`, it starts an identifier or
// extension word. The word consists of all immediately following code points
// that either have the XID_Continue property or are `$`. Per assumption 1, `$`
// and `_` do not compose, and per assumption 7, composition of XID_Continue
// code points produces a XID_Continue code point, which yields well-formedness.
//
// For equivalence, we need to consider which code points a XID_Continue code
// point can compose with. The CCA can perform a composition in two cases:
// - If a XID_Continue starter is immediately followed by a sequence of
//   non-starters, it can compose with one of those non-starters. Because
//   non-starters have the XID_Continue property per assumption 3, the removed
//   code point is still in the string v.
// - If a XID_Continue starter is immediately followed by another starter, it
//   can compose with that other starter. In this case, per assumptions 5 & 7,
//   the other starter also had the XID_Continue property, so it was also in the
//   string v.
// Therefore, any time a code point in v composes with another code point, that
// other code point was also in v, so CCA(vw) = CCA(v)CCA(w). To prove that
// CCA(w) does not start with a XID_Continue code point, we observe that
// otherwise w would begin with a XID_Continue code point per assumption 5.
//
// If the original first code point is in the XID_Start class, it starts an
// unprefixed word. The reasoning proceeds as for other word tokens, except that
// we also need to consider the possibility of the first code point composing.
// If that happens, the composition result also has the XID_Start property per
// assumption 6, so the token class remains the same, and the rest of the
// reasoning is the same as for identifier and extension words (since XID_Start
// is a subset of XID_Continue).
//
// If the original first code point is in the decimal class, or if it is a `.`
// followed by a decimal digit, it starts a numeric literal. The reasoning
// proceeds as for word tokens (`.` and decimal digits never compose with
// anything, per assumption 1), except that we also need to take into
// consideration the context in which `+` and `-` code points are allowed. But
// if such context changed, the resulting token must be an invalid numeric
// literal, since it contains code points that have decompositions (this uses
// assumption 1); and the original token must have been an invalid numeric
// literal, since it contained their decompositions, which (per
// assumptions 1 & 8) must have contained some code points other than Basic
// Latin or decimal digits.
//
// If the original first code point is in the punctuation class, or if it is a
// `.` not followed by a decimal digit, it starts a punctuation token. All code
// points comprising punctuation tokens are starters, and the only such code
// points that can compose are non-negated comparison code points, which can
// only compose with U+0338, a non-starter. So a punctuation token in isolation
// is unaffected by the CCA, giving us well-formedness; but if it ends with a
// non-negated comparison code point, it could compose with a later U+0338. But
// in that case, the string w must begin with a non-starter, thus making the
// original input invalid; and after the CCA, if |v| = 1, then CCA(vw) begins
// with a negated comparison code point, which is in the invalid class, or if
// |v| > 1, then CCA(vw) begins with a punctuation token followed by a negated
// comparison code point, so in either case, the input remains invalid after
// applying the CCA.
//
// For the second part of equivalence, we note that apart from code points from
// the invalid class, only XID_Start code points can form via composition, but,
// per assumption 6, that can only happen if v originally started with a
// XID_Start code point, which is not the case here.

BOOST_AUTO_TEST_SUITE_END() // lexer
BOOST_AUTO_TEST_SUITE_END() // ucd_tests
