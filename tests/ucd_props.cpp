#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "unicode/props.hpp"

#include "unicode/ucd/white_space_list.hpp"

namespace bdata = boost::unit_test::data;

BOOST_AUTO_TEST_SUITE(ucd_tests)
BOOST_AUTO_TEST_SUITE(props)

BOOST_DATA_TEST_CASE(white_space,
    bdata::make(unicode::ucd::white_space_cps), ch) {
  BOOST_TEST(unicode::white_space(ch));
}

BOOST_AUTO_TEST_SUITE_END() // props
BOOST_AUTO_TEST_SUITE_END() // ucd_tests
