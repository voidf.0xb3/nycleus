#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "unicode/casefold.hpp"
#include "unicode/normal.hpp"
#include <algorithm>
#include <iterator>
#include <ranges>
#include <string>

#include "unicode/ucd/decomposing_list.hpp"

namespace bdata = boost::unit_test::data;

BOOST_AUTO_TEST_SUITE(ucd_tests)
BOOST_AUTO_TEST_SUITE(casefold)

BOOST_DATA_TEST_CASE(decomp_preserves,
    bdata::make(unicode::ucd::decomposing_cps), cp) {
  // This tests for assumptions that are made in the implementation of
  // casefolding, when the first normalization step is omitted for chunks that
  // do not contain casefold-crazy code points.
  if(!unicode::detail_::is_casefold_crazy(cp)) {
    auto const decomp{unicode::detail_::canon_decompose(cp)};
    auto const casefolding{unicode::detail_::casefold_one(cp)};

    std::u32string decomp_casefolded;
    for(char32_t const ch: decomp) {
      std::ranges::copy(unicode::detail_::casefold_one(ch),
        std::back_inserter(decomp_casefolded));
    }

    BOOST_TEST(std::ranges::equal(
      casefolding | unicode::nfd,
      decomp_casefolded | unicode::nfd
    ));

    if(unicode::ccc(cp) != 0) {
      BOOST_TEST(std::ranges::equal(std::views::single(cp), casefolding));
    }
  }
}

BOOST_AUTO_TEST_SUITE_END() // casefold
BOOST_AUTO_TEST_SUITE_END() // ucd_tests
