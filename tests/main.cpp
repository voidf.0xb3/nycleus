#define BOOST_TEST_MODULE nycleus
#include <boost/test/unit_test.hpp>

#ifdef NDEBUG
  #error The test suite must be built in debug mode.
#endif
