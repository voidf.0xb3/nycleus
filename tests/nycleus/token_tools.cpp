#include "nycleus/token_tools.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/source_location_tools.hpp"
#include "nycleus/token.hpp"
#include "util/cow.hpp"
#include "util/type_traits.hpp"
#include "util/u8compat.hpp"
#include <algorithm>
#include <concepts>
#include <optional>
#include <ostream>
#include <variant>
#include <vector>

using namespace util::u8compat_literals;

// Token output ////////////////////////////////////////////////////////////////

namespace {

void print_escaped_string(std::ostream& os, std::u8string const& str) {
  for(char8_t const ch: str) {
    switch(ch) {
      case u8'"': os << u8"\""_ch; break;
      case u8'\\': os << u8"\\\\"_ch; break;
      case u8'\a': os << u8"\\a"_ch; break;
      case u8'\b': os << u8"\\b"_ch; break;
      case u8'\f': os << u8"\\f"_ch; break;
      case u8'\n': os << u8"\\n"_ch; break;
      case u8'\r': os << u8"\\r"_ch; break;
      case u8'\t': os << u8"\\t"_ch; break;
      case u8'\v': os << u8"\\v"_ch; break;
      default: os << static_cast<char>(ch); break;
    }
  }
}

void print_token(std::ostream& os, nycleus::tok::word word) {
  switch(word.sigil) {
    case nycleus::tok::word::sigil_t::none:       os << u8"word"_ch; break;
    case nycleus::tok::word::sigil_t::dollar:     os << u8"id"_ch; break;
    case nycleus::tok::word::sigil_t::underscore: os << u8"ext"_ch; break;
  }
  os << u8":\""_ch;
  print_escaped_string(os, *word.text);
  os << u8'"'_ch;
}

void print_token(std::ostream& os,
    nycleus::tok::int_literal int_literal) {
  os << u8"int_literal:"_ch << *int_literal.num;
  if(int_literal.type_suffix) {
    os << (int_literal.type_suffix->is_signed ? u8's'_ch : u8'u'_ch)
      << int_literal.type_suffix->width;
  }
}

void print_token(std::ostream& os, nycleus::tok::comment comment) {
  switch(comment.kind) {
    case nycleus::tok::comment::kind_t::single_line:
      os << u8"sl"_ch;
      break;
    case nycleus::tok::comment::kind_t::multiline_non_nestable:
      os << u8"ml"_ch;
      break;

    case nycleus::tok::comment::kind_t::multiline_nestable:
      os << u8"mlnest"_ch;
      break;
  }

  os << u8" comment:\""_ch;
  print_escaped_string(os, *comment.text);
  os << u8'"'_ch;
}

void print_token(std::ostream& os, nycleus::tok::string_literal str) {
  os << u8'"'_ch;
  print_escaped_string(os, *str.value);
  os << u8'"'_ch;
}

void print_token(std::ostream& os, nycleus::tok::char_literal ch) {
  os << u8'\''_ch;
  print_escaped_string(os, *ch.value);
  os << u8'\''_ch;
}

void print_token(std::ostream& os, nycleus::tok::excl) {
  os << u8'!'_ch;
}
void print_token(std::ostream& os, nycleus::tok::neq) {
  os << u8"!="_ch;
}
void print_token(std::ostream& os, nycleus::tok::percent) {
  os << u8'%'_ch;
}
void print_token(std::ostream& os, nycleus::tok::percent_assign) {
  os << u8"%="_ch;
}
void print_token(std::ostream& os, nycleus::tok::amp) {
  os << u8'&'_ch;
}
void print_token(std::ostream& os, nycleus::tok::logical_and tok) {
  os << u8"&& "_ch << tok.first_amp_loc << u8' '_ch << tok.second_amp_loc;
}
void print_token(std::ostream& os, nycleus::tok::amp_assign) {
  os << u8"&="_ch;
}
void print_token(std::ostream& os, nycleus::tok::lparen) {
  os << u8'('_ch;
}
void print_token(std::ostream& os, nycleus::tok::rparen) {
  os << u8')'_ch;
}
void print_token(std::ostream& os, nycleus::tok::star) {
  os << u8'*'_ch;
}
void print_token(std::ostream& os, nycleus::tok::star_assign) {
  os << u8"*="_ch;
}
void print_token(std::ostream& os, nycleus::tok::plus) {
  os << u8'+'_ch;
}
void print_token(std::ostream& os, nycleus::tok::incr) {
  os << u8"++"_ch;
}
void print_token(std::ostream& os, nycleus::tok::plus_assign) {
  os << u8"+="_ch;
}
void print_token(std::ostream& os, nycleus::tok::comma) {
  os << u8','_ch;
}
void print_token(std::ostream& os, nycleus::tok::minus) {
  os << u8'-'_ch;
}
void print_token(std::ostream& os, nycleus::tok::decr) {
  os << u8"--"_ch;
}
void print_token(std::ostream& os, nycleus::tok::minus_assign) {
  os << u8"-="_ch;
}
void print_token(std::ostream& os, nycleus::tok::arrow) {
  os << u8"->"_ch;
}
void print_token(std::ostream& os, nycleus::tok::point) {
  os << u8'.'_ch;
}
void print_token(std::ostream& os, nycleus::tok::double_point) {
  os << u8".."_ch;
}
void print_token(std::ostream& os, nycleus::tok::ellipsis) {
  os << u8"..."_ch;
}
void print_token(std::ostream& os, nycleus::tok::slash) {
  os << u8'/'_ch;
}
void print_token(std::ostream& os, nycleus::tok::slash_assign) {
  os << u8"/="_ch;
}
void print_token(std::ostream& os, nycleus::tok::colon) {
  os << u8':'_ch;
}
void print_token(std::ostream& os, nycleus::tok::semicolon) {
  os << u8';'_ch;
}
void print_token(std::ostream& os, nycleus::tok::lt) {
  os << u8'<'_ch;
}
void print_token(std::ostream& os, nycleus::tok::lrot) {
  os << u8"</"_ch;
}
void print_token(std::ostream& os, nycleus::tok::lrot_assign) {
  os << u8"</="_ch;
}
void print_token(std::ostream& os, nycleus::tok::lshift) {
  os << u8"<<"_ch;
}
void print_token(std::ostream& os, nycleus::tok::lshift_assign) {
  os << u8"<<="_ch;
}
void print_token(std::ostream& os, nycleus::tok::leq) {
  os << u8"<="_ch;
}
void print_token(std::ostream& os, nycleus::tok::three_way_cmp) {
  os << u8"<=>"_ch;
}
void print_token(std::ostream& os, nycleus::tok::assign) {
  os << u8'='_ch;
}
void print_token(std::ostream& os, nycleus::tok::eq) {
  os << u8"=="_ch;
}
void print_token(std::ostream& os, nycleus::tok::fat_arrow) {
  os << u8"=>"_ch;
}
void print_token(std::ostream& os, nycleus::tok::gt) {
  os << u8'>'_ch;
}
void print_token(std::ostream& os, nycleus::tok::rrot) {
  os << u8">/"_ch;
}
void print_token(std::ostream& os, nycleus::tok::rrot_assign) {
  os << u8">/="_ch;
}
void print_token(std::ostream& os, nycleus::tok::geq) {
  os << u8">="_ch;
}
void print_token(std::ostream& os, nycleus::tok::rshift) {
  os << u8">>"_ch;
}
void print_token(std::ostream& os, nycleus::tok::rshift_assign) {
  os << u8">>="_ch;
}
void print_token(std::ostream& os, nycleus::tok::question) {
  os << u8'?'_ch;
}
void print_token(std::ostream& os, nycleus::tok::at) {
  os << u8'@'_ch;
}
void print_token(std::ostream& os, nycleus::tok::lsquare) {
  os << u8'['_ch;
}
void print_token(std::ostream& os, nycleus::tok::rsquare) {
  os << u8']'_ch;
}
void print_token(std::ostream& os, nycleus::tok::bitwise_xor) {
  os << u8'^'_ch;
}
void print_token(std::ostream& os, nycleus::tok::bitwise_xor_assign) {
  os << u8"^="_ch;
}
void print_token(std::ostream& os, nycleus::tok::logical_xor) {
  os << u8"^^"_ch;
}
void print_token(std::ostream& os, nycleus::tok::lbrace) {
  os << u8'{'_ch;
}
void print_token(std::ostream& os, nycleus::tok::pipe) {
  os << u8'|'_ch;
}
void print_token(std::ostream& os, nycleus::tok::pipe_assign) {
  os << u8"|="_ch;
}
void print_token(std::ostream& os, nycleus::tok::subst) {
  os << u8"|>"_ch;
}
void print_token(std::ostream& os, nycleus::tok::logical_or) {
  os << u8"||"_ch;
}
void print_token(std::ostream& os, nycleus::tok::rbrace) {
  os << u8'}'_ch;
}
void print_token(std::ostream& os, nycleus::tok::tilde) {
  os << u8'~'_ch;
}

void print_token(std::ostream& os, nycleus::tok::error) {
  os << u8"error"_ch;
}
void print_token(std::ostream& os, nycleus::tok::eof) {
  os << u8"eof"_ch;
}

bool diags_have_errors(
  std::optional<util::cow<std::vector<nycleus::diagnostic_box>>> const& diags
) noexcept {
  if(!diags) {
    return false;
  } else {
    return std::ranges::any_of(**diags,
      [](nycleus::diagnostic_box const& diag) noexcept {
        return dynamic_cast<nycleus::error const*>(diag.get()) != nullptr;
      });
  }
}

} // (anonymous)

std::ostream& nycleus::operator<<(std::ostream& os, nycleus::token const& tok) {
  std::visit([&os](auto const& t) { print_token(os, t); }, tok.tok);
  os << u8' '_ch << tok.location;
  if(diags_have_errors(tok.diagnostics)) {
    os << u8" (errors)"_ch;
  }
  return os;
}

// Token comparison ////////////////////////////////////////////////////////////

namespace {

[[nodiscard]] bool compare_token_types(
  nycleus::tok::word const& a,
  nycleus::tok::word const& b
) noexcept {
  return *a.text == *b.text && a.sigil == b.sigil;
}

[[nodiscard]] bool compare_token_types(
  nycleus::tok::int_literal const& a,
  nycleus::tok::int_literal const& b
) noexcept {
  return *a.num == *b.num && (!a.type_suffix && !b.type_suffix
    || a.type_suffix && b.type_suffix
    && a.type_suffix->width == b.type_suffix->width
    && a.type_suffix->is_signed == b.type_suffix->is_signed);
}

[[nodiscard]] bool compare_token_types(
  nycleus::tok::comment const& a,
  nycleus::tok::comment const& b
) noexcept {
  return *a.text == *b.text && a.kind == b.kind;
}

[[nodiscard]] bool compare_token_types(
  nycleus::tok::string_literal const& a,
  nycleus::tok::string_literal const& b
) noexcept {
  return *a.value == *b.value;
}

[[nodiscard]] bool compare_token_types(
  nycleus::tok::char_literal const& a,
  nycleus::tok::char_literal const& b
) noexcept {
  return *a.value == *b.value;
}

template<typename T>
[[nodiscard]] bool compare_token_types(T const&, T const&) noexcept {
  return true;
}

} // (anonymous)

bool nycleus::operator==(
  nycleus::token const& a,
  nycleus::token const& b
) noexcept {
  return std::visit(
    [](auto const& a_tok, auto const& b_tok) {
      using a_type = std::remove_cvref_t<decltype(a_tok)>;
      using b_type = std::remove_cvref_t<decltype(b_tok)>;
      if constexpr(std::same_as<a_type, b_type>) {
        return compare_token_types(a_tok, b_tok);
      } else {
        return false;
      }
    }, a.tok, b.tok)
    && a.location == b.location;
}
