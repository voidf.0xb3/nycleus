#ifndef TESTS_NYCLEUS_DIAGNOSTIC_TOOLS_HPP_INCLUDED_29I50H53Q9QMHN37SEVGENYOC
#define TESTS_NYCLEUS_DIAGNOSTIC_TOOLS_HPP_INCLUDED_29I50H53Q9QMHN37SEVGENYOC

#include <array>
#include <concepts>
#include <cstddef>
#include <functional>
#include <memory>
#include <span>
#include <utility>

namespace nycleus {

struct diagnostic;
class diagnostic_logger;

[[nodiscard]] bool operator==(
  diagnostic const& a,
  diagnostic const& b
) noexcept;

} // nycleus

template<>
struct std::hash<::std::reference_wrapper<::nycleus::diagnostic const>> {
  [[nodiscard]] ::std::size_t operator()(
    ::std::reference_wrapper<::nycleus::diagnostic const>
  ) const noexcept;
};

namespace test::nycleus {

template<std::derived_from<::nycleus::diagnostic>... Ts>
[[nodiscard]] auto diags(std::unique_ptr<Ts>... diags) {
  return std::array<std::unique_ptr<::nycleus::diagnostic>, sizeof...(Ts)>{
    std::move(diags)...};
}

[[nodiscard]] bool diags_eq(
  ::nycleus::diagnostic_logger&,
  std::span<std::unique_ptr<::nycleus::diagnostic> const> test_diags
);

} // test::nycleus

#endif
