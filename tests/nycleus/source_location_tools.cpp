#include "nycleus/source_location_tools.hpp"
#include "nycleus/source_location.hpp"
#include "util/u8compat.hpp"
#include <boost/container_hash/hash.hpp>
#include <cstddef>
#include <filesystem>
#include <functional>
#include <ostream>

using namespace util::u8compat_literals;

std::ostream& nycleus::operator<<(std::ostream& os,
    nycleus::source_location src_loc) {
  if(src_loc.file) {
    os << **src_loc.file;
  } else {
    os << u8"<?>"_ch;
  }
  os << u8':'_ch << src_loc.first.line << u8':'_ch << src_loc.first.col
    << u8'-'_ch << src_loc.last.line << u8':'_ch << src_loc.last.col;
  return os;
}

::std::size_t std::hash<::nycleus::file_pos>::operator()(
  ::nycleus::file_pos pos
) const noexcept {
  ::std::size_t result{0};
  ::boost::hash_combine(result, pos.col);
  ::boost::hash_combine(result, pos.line);
  return result;
}

::std::size_t std::hash<::nycleus::source_location>::operator()(
  ::nycleus::source_location const& loc
) const noexcept {
  ::std::size_t result;
  if(!loc.file) {
    result = 0;
  } else {
    result = 1;
    ::boost::hash_combine(result, ::std::filesystem::hash_value(**loc.file));
  }
  ::boost::hash_combine(result, ::std::hash<::nycleus::file_pos>{}(loc.first));
  ::boost::hash_combine(result, ::std::hash<::nycleus::file_pos>{}(loc.last));
  return result;
}
