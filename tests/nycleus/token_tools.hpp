#ifndef TESTS_NYCLEUS_TOKEN_TOOLS_HPP_INCLUDED_Q8D61PDDFPCY5YZNFQIGWN0CJ
#define TESTS_NYCLEUS_TOKEN_TOOLS_HPP_INCLUDED_Q8D61PDDFPCY5YZNFQIGWN0CJ

#include "nycleus/token.hpp"
#include <iosfwd>

namespace nycleus {

std::ostream& operator<<(std::ostream&, token const&);
[[nodiscard]] bool operator==(token const&, token const&) noexcept;

} // (nycleus)

#endif
