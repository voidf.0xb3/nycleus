#ifndef TESTS_NYCLEUS_HLIR_TOOLS_HPP_INCLUDED_ZZL7NY9JCNCY3XECJ72HTYQBZ
#define TESTS_NYCLEUS_HLIR_TOOLS_HPP_INCLUDED_ZZL7NY9JCNCY3XECJ72HTYQBZ

#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include <iosfwd>
#include <memory>

namespace nycleus::hlir {

struct type;
struct expr;
struct stmt;
struct def;

} // nycleus::hlir

// HLIR cloning ////////////////////////////////////////////////////////////////

namespace test::nycleus {

[[nodiscard]] ::nycleus::hlir_ptr<::nycleus::hlir::type> clone_type(
  ::nycleus::hlir::type const*);
[[nodiscard]] ::nycleus::hlir_ptr<::nycleus::hlir::expr> clone_expr(
  ::nycleus::hlir::expr const*);
[[nodiscard]] ::nycleus::hlir_ptr<::nycleus::hlir::stmt> clone_stmt(
  ::nycleus::hlir::stmt const*);
[[nodiscard]] ::nycleus::hlir_ptr<::nycleus::hlir::def> clone_def(
  ::nycleus::hlir::def const*);

} // test::nycleus

// HLIR output /////////////////////////////////////////////////////////////////

namespace nycleus::hlir {

std::ostream& operator<<(std::ostream&, type const&);
std::ostream& operator<<(std::ostream&, expr const&);
std::ostream& operator<<(std::ostream&, stmt const&);
std::ostream& operator<<(std::ostream&, def const&);

// HLIR comparison /////////////////////////////////////////////////////////////

[[nodiscard]] bool operator==(type const&, type const&) noexcept;
[[nodiscard]] bool operator==(expr const&, expr const&) noexcept;
[[nodiscard]] bool operator==(stmt const&, stmt const&) noexcept;
[[nodiscard]] bool operator==(def const&, def const&) noexcept;

} // nycleus::hlir

#endif
