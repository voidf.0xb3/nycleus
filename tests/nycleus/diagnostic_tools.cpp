#include "nycleus/diagnostic_tools.hpp"
#include "nycleus/diagnostic.hpp"
#include "nycleus/diagnostic_logger.hpp"
#include "nycleus/source_location_tools.hpp"
#include "util/fancy_tuple.hpp"
#include "util/ranges.hpp"
#include "util/zip_view.hpp"
#include <boost/container_hash/hash.hpp>
#include <algorithm>
#include <concepts>
#include <cstddef>
#include <functional>
#include <memory>
#include <ranges>
#include <span>
#include <typeinfo>
#include <unordered_set>
#include <utility>

namespace diag = nycleus::diag;

////////////////////////////////////////////////////////////////////////////////
// Comparison //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

namespace {

template<typename T, typename U>
requires (!std::same_as<T, U>)
[[nodiscard]] bool diag_cmp_impl(T const&, U const&) noexcept { return false; }

[[nodiscard]] bool diag_cmp_impl(
  diag::error_with_location const& a,
  diag::error_with_location const& b
) noexcept {
  return a.loc == b.loc;
}

// Lexer errors ////////////////////////////////////////////////////////////////

[[nodiscard]] bool diag_cmp_impl(
  diag::bidi_code_in_string const& a,
  diag::bidi_code_in_string const& b
) noexcept {
  return a.loc == b.loc && a.code == b.code;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::comment_unmatched_bidi_code const& a,
  diag::comment_unmatched_bidi_code const& b
) noexcept {
  return a.loc == b.loc && a.code == b.code;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::num_literal_suffix_missing_width const& a,
  diag::num_literal_suffix_missing_width const& b
) noexcept {
  return a.loc == b.loc && a.prefix_letter == b.prefix_letter;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::unexpected_code_point const& a,
  diag::unexpected_code_point const& b
) noexcept {
  return a.loc == b.loc && a.code_point == b.code_point;
}

// Parser errors ///////////////////////////////////////////////////////////////

[[nodiscard]] bool diag_cmp_impl(
  diag::bad_comparison_chain const& a,
  diag::bad_comparison_chain const& b
) noexcept {
  return a.loc == b.loc && a.lt_loc == b.lt_loc && a.gt_loc == b.gt_loc
    && a.lt_strict == b.lt_strict && a.gt_strict == b.gt_strict
    && a.lt_before_gt == b.lt_before_gt;
}

// Semantic errors /////////////////////////////////////////////////////////////

[[nodiscard]] bool diag_cmp_impl(
  diag::addr_mut const& a,
  diag::addr_mut const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::assign_to_immut const& a,
  diag::assign_to_immut const& b
) noexcept {
  return a.stmt == b.stmt;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::assign_to_transient const& a,
  diag::assign_to_transient const& b
) noexcept {
  return a.stmt == b.stmt;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::call_expr_arg_count_mismatch const& a,
  diag::call_expr_arg_count_mismatch const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::ct_int_div_by_zero const& a,
  diag::ct_int_div_by_zero const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::ct_int_mod_by_zero const& a,
  diag::ct_int_mod_by_zero const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::ct_int_too_big const& a,
  diag::ct_int_too_big const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::dep_cycle const& a,
  diag::dep_cycle const& b
) noexcept {
  return a.cycle == b.cycle;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::duplicate_class_member const& a,
  diag::duplicate_class_member const& b
) noexcept {
  return a.name == b.name && a.locations == b.locations;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::duplicate_global_def const& a,
  diag::duplicate_global_def const& b
) noexcept {
  return a.name == b.name && a.locations == b.locations;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::duplicate_name_in_block const& a,
  diag::duplicate_name_in_block const& b
) noexcept {
  return a.name == b.name && a.locations == b.locations;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::duplicate_param_name const& a,
  diag::duplicate_param_name const& b
) noexcept {
  return a.name == b.name && a.locations == b.locations;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::int_literal_too_big const& a,
  diag::int_literal_too_big const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::int_literal_does_not_fit_type const& a,
  diag::int_literal_does_not_fit_type const& b
) noexcept {
  return a.expr == b.expr && a.width == b.width;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::local_var_wrong_function const& a,
  diag::local_var_wrong_function const& b
) noexcept {
  return a.expr == b.expr && a.var == b.var && a.fn == b.fn;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::member_access_bad_name const& a,
  diag::member_access_bad_name const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::name_is_not_type const& a,
  diag::name_is_not_type const& b
) noexcept {
  return a.name == b.name;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::name_is_not_value const& a,
  diag::name_is_not_value const& b
) noexcept {
  return a.name == b.name;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::name_undefined const& a,
  diag::name_undefined const& b
) noexcept {
  return a.name == b.name;
}

// Errors about types //////////////////////////////////////////////////////////

[[nodiscard]] bool diag_cmp_impl(
  diag::add_bad_types const& a,
  diag::add_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::add_void const& a,
  diag::add_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::addr_void const& a,
  diag::addr_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::assign_bad_types const& a,
  diag::assign_bad_types const& b
) noexcept {
  return a.stmt == b.stmt;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::assign_from_void const& a,
  diag::assign_from_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::assign_to_void const& a,
  diag::assign_to_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::band_bad_types const& a,
  diag::band_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::band_void const& a,
  diag::band_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::bnot_bad_type const& a,
  diag::bnot_bad_type const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::bnot_void const& a,
  diag::bnot_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::bor_bad_types const& a,
  diag::bor_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::bor_void const& a,
  diag::bor_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::bxor_bad_types const& a,
  diag::bxor_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::bxor_void const& a,
  diag::bxor_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::call_expr_arg_bad_type const& a,
  diag::call_expr_arg_bad_type const& b
) noexcept {
  return a.expr == b.expr && a.param_type == b.param_type && a.arg == b.arg;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::call_expr_arg_void const& a,
  diag::call_expr_arg_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::call_expr_callee_bad_type const& a,
  diag::call_expr_callee_bad_type const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::call_expr_callee_void const& a,
  diag::call_expr_callee_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::comparison_bad_types const& a,
  diag::comparison_bad_types const& b
) noexcept {
  return a.left == b.left && a.right == b.right;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::comparison_void const& a,
  diag::comparison_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::div_bad_types const& a,
  diag::div_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::div_void const& a,
  diag::div_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_return_bad_type const& a,
  diag::fn_return_bad_type const& b
) noexcept {
  return a.loc == b.loc && a.fn == b.fn;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_return_void const& a,
  diag::fn_return_void const& b
) noexcept {
  return a.loc == b.loc && a.fn == b.fn;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::deref_bad_type const& a,
  diag::deref_bad_type const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::deref_void const& a,
  diag::deref_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::local_var_init_bad_type const& a,
  diag::local_var_init_bad_type const& b
) noexcept {
  return a.stmt == b.stmt;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::local_var_init_void const& a,
  diag::local_var_init_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::logical_and_bad_types const& a,
  diag::logical_and_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::logical_and_void const& a,
  diag::logical_and_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::logical_or_bad_types const& a,
  diag::logical_or_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::logical_or_void const& a,
  diag::logical_or_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::logical_xor_bad_types const& a,
  diag::logical_xor_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::logical_xor_void const& a,
  diag::logical_xor_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::lrot_bad_types const& a,
  diag::lrot_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::lrot_void const& a,
  diag::lrot_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::lsh_bad_types const& a,
  diag::lsh_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::lsh_void const& a,
  diag::lsh_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::member_access_bad_type const& a,
  diag::member_access_bad_type const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::member_access_void const& a,
  diag::member_access_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::mod_bad_types const& a,
  diag::mod_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::mod_void const& a,
  diag::mod_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::mul_bad_types const& a,
  diag::mul_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::mul_void const& a,
  diag::mul_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::neg_bad_type const& a,
  diag::neg_bad_type const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::neg_void const& a,
  diag::neg_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::neq_bad_types const& a,
  diag::neq_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::neq_void const& a,
  diag::neq_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::not_bad_type const& a,
  diag::not_bad_type const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::not_void const& a,
  diag::not_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::rrot_bad_types const& a,
  diag::rrot_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::rrot_void const& a,
  diag::rrot_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::rsh_bad_types const& a,
  diag::rsh_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::rsh_void const& a,
  diag::rsh_void const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::sub_bad_types const& a,
  diag::sub_bad_types const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::sub_void const& a,
  diag::sub_void const& b
) noexcept {
  return a.expr == b.expr;
}

// Errors about unsupported features ///////////////////////////////////////////

[[nodiscard]] bool diag_cmp_impl(
  diag::class_field_initializers_unsupported const& a,
  diag::class_field_initializers_unsupported const& b
) noexcept {
  return a.init == b.init;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::methods_unsupported const& a,
  diag::methods_unsupported const& b
) noexcept {
  return a.def == b.def;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::persistent_class_arg_unsupported const& a,
  diag::persistent_class_arg_unsupported const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::persistent_class_init_unsupported const& a,
  diag::persistent_class_init_unsupported const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::persistent_class_return_unsupported const& a,
  diag::persistent_class_return_unsupported const& b
) noexcept {
  return a.expr == b.expr;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::uninitialized_variables_unsupported const& a,
  diag::uninitialized_variables_unsupported const& b
) noexcept {
  return a.def == b.def;
}

// Warnings ////////////////////////////////////////////////////////////////////

[[nodiscard]] bool diag_cmp_impl(
  diag::string_newline const& a,
  diag::string_newline const& b
) noexcept {
  return a.loc == b.loc && a.newline_code == b.newline_code;
}

} // (anonymous)

// Public API //////////////////////////////////////////////////////////////////

bool nycleus::operator==(
  nycleus::diagnostic const& a,
  nycleus::diagnostic const& b
) noexcept {
  return a.const_visit([&](auto const& a_concrete) noexcept {
    return b.const_visit([&](auto const& b_concrete) noexcept {
      return diag_cmp_impl(a_concrete, b_concrete);
    });
  });
}

////////////////////////////////////////////////////////////////////////////////
// Hashing /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

namespace {

template<typename T>
void add_hash(
  std::size_t& result,
  T const& value
) {
  boost::hash_combine(result, std::hash<T>{}(value));
}

void diag_hash_impl(
  std::size_t& result,
  diag::error_with_location const& d
) noexcept {
  add_hash(result, d.loc);
}

// Lexer errors ////////////////////////////////////////////////////////////////

void diag_hash_impl(
  std::size_t& result,
  diag::bidi_code_in_string const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.code);
}

void diag_hash_impl(
  std::size_t& result,
  diag::comment_unmatched_bidi_code const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.code);
}

void diag_hash_impl(
  std::size_t& result,
  diag::num_literal_suffix_missing_width
   const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.prefix_letter);
}

void diag_hash_impl(
  std::size_t& result,
  diag::unexpected_code_point const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.code_point);
}

// Parser errors ///////////////////////////////////////////////////////////////

void diag_hash_impl(
  std::size_t& result,
  diag::bad_comparison_chain const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.lt_loc);
  add_hash(result, d.gt_loc);
  add_hash(result, d.lt_strict);
  add_hash(result, d.gt_strict);
  add_hash(result, d.lt_before_gt);
}

// Semantic errors /////////////////////////////////////////////////////////////

void diag_hash_impl(
  std::size_t& result,
  diag::addr_mut const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::assign_to_immut const& d
) noexcept {
  add_hash(result, d.stmt.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::assign_to_transient const& d
) noexcept {
  add_hash(result, d.stmt.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::call_expr_arg_count_mismatch const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::ct_int_div_by_zero const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::ct_int_mod_by_zero const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::ct_int_too_big const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::dep_cycle const& d
) noexcept {
  add_hash(result, d.cycle.size());
  for(auto const& entry: d.cycle) {
    add_hash(result, entry);
  }
}

void diag_hash_impl(
  std::size_t& result,
  diag::duplicate_class_member const& d
) noexcept {
  add_hash(result, d.name);
  add_hash(result, d.locations.size());
  for(auto const& loc: d.locations) {
    add_hash(result, loc);
  }
}

void diag_hash_impl(
  std::size_t& result,
  diag::duplicate_global_def const& d
) noexcept {
  add_hash(result, d.name);
  add_hash(result, d.locations.size());
  for(auto const& loc: d.locations) {
    add_hash(result, loc);
  }
}

void diag_hash_impl(
  std::size_t& result,
  diag::duplicate_name_in_block const& d
) noexcept {
  add_hash(result, d.name);
  add_hash(result, d.locations.size());
  for(auto const& loc: d.locations) {
    add_hash(result, loc);
  }
}

void diag_hash_impl(
  std::size_t& result,
  diag::duplicate_param_name const& d
) noexcept {
  add_hash(result, d.name);
  add_hash(result, d.locations.size());
  for(auto const& loc: d.locations) {
    add_hash(result, loc);
  }
}

void diag_hash_impl(
  std::size_t& result,
  diag::int_literal_too_big const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::int_literal_does_not_fit_type const& d
) noexcept {
  add_hash(result, d.expr.get());
  add_hash(result, d.width);
}

void diag_hash_impl(
  std::size_t& result,
  diag::local_var_wrong_function const& d
) noexcept {
  add_hash(result, d.expr.get());
  add_hash(result, d.var.get());
  add_hash(result, d.fn.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::member_access_bad_name const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::name_is_not_type const& d
) noexcept {
  add_hash(result, d.name.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::name_is_not_value const& d
) noexcept {
  add_hash(result, d.name.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::name_undefined const& d
) noexcept {
  add_hash(result, d.name.get());
}

// Errors about types //////////////////////////////////////////////////////////

void diag_hash_impl(
  std::size_t& result,
  diag::add_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::add_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::addr_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::assign_bad_types const& d
) noexcept {
  add_hash(result, d.stmt.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::assign_from_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::assign_to_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::band_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::band_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::bnot_bad_type const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::bnot_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::bor_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::bor_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::bxor_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::bxor_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::call_expr_arg_bad_type const& d
) noexcept {
  add_hash(result, d.expr.get());
  add_hash(result, d.param_type.get());
  add_hash(result, d.arg.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::call_expr_arg_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::call_expr_callee_bad_type const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::call_expr_callee_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::comparison_bad_types const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.left.get());
  add_hash(result, d.right.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::comparison_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::div_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::div_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_return_bad_type const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.fn.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_return_void const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.fn.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::deref_bad_type const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::deref_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::local_var_init_bad_type const& d
) noexcept {
  add_hash(result, d.stmt.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::local_var_init_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::logical_and_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::logical_and_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::logical_or_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::logical_or_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::logical_xor_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::logical_xor_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::lrot_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::lrot_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::lsh_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::lsh_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::member_access_bad_type const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::member_access_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::mod_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::mod_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::mul_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::mul_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::neg_bad_type const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::neg_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::neq_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::neq_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::not_bad_type const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::not_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::rrot_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::rrot_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::rsh_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::rsh_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::sub_bad_types const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::sub_void const& d
) noexcept {
  add_hash(result, d.expr.get());
}

// Errors about unsupported features ///////////////////////////////////////////

void diag_hash_impl(
  std::size_t& result,
  diag::class_field_initializers_unsupported const& d
) noexcept {
  add_hash(result, d.init.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::methods_unsupported const& d
) noexcept {
  add_hash(result, d.def.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::persistent_class_arg_unsupported const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::persistent_class_init_unsupported const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::persistent_class_return_unsupported const& d
) noexcept {
  add_hash(result, d.expr.get());
}

void diag_hash_impl(
  std::size_t& result,
  diag::uninitialized_variables_unsupported const& d
) noexcept {
  add_hash(result, d.def.get());
}

// Warnings ////////////////////////////////////////////////////////////////////

void diag_hash_impl(
  std::size_t& result,
  diag::string_newline const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.newline_code);
}

} // (anonymous)

// Public API //////////////////////////////////////////////////////////////////

::std::size_t std::hash<::std::reference_wrapper<::nycleus::diagnostic const>>
    ::operator()(
  ::std::reference_wrapper<::nycleus::diagnostic const> d
) const noexcept {
  ::nycleus::diagnostic const& d_deref{d.get()};
  ::std::size_t result{typeid(d_deref).hash_code()};
  d.get().const_visit([&](auto const& derived) noexcept {
    diag_hash_impl(result, derived);
  });
  return result;
}

////////////////////////////////////////////////////////////////////////////////
// Other stuff /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool test::nycleus::diags_eq(
  ::nycleus::diagnostic_logger& dt,
  std::span<std::unique_ptr<::nycleus::diagnostic> const> test_diags
) {
  auto const diags{std::move(dt).extract_diags()};
  auto diags_transform{
    std::views::transform(&std::unique_ptr<::nycleus::diagnostic>::operator*)
    | util::range_to<std::unordered_set<
      std::reference_wrapper<::nycleus::diagnostic const>>>()};
  auto const diags_set{diags | diags_transform};
  auto const test_diags_set{test_diags | diags_transform};
  return diags_set == test_diags_set;
}
