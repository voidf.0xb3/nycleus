#include "nycleus/hlir_tools.hpp"
#include "nycleus/hlir.hpp"
#include "nycleus/hlir_ptr.hpp"
#include "nycleus/source_location.hpp"
#include "nycleus/source_location_tools.hpp"
#include "util/ranges.hpp"
#include "util/small_vector.hpp"
#include "util/u8compat.hpp"
#include "util/util.hpp"
#include "test_tools.hpp"
#include <algorithm>
#include <concepts>
#include <cstdint>
#include <memory>
#include <ostream>
#include <ranges>
#include <vector>

using namespace util::u8compat_literals;

////////////////////////////////////////////////////////////////////////////////
// HLIR cloning ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

namespace {

[[nodiscard]] nycleus::hlir_ptr<nycleus::hlir::block> clone_block(
  nycleus::hlir::block const& block
) {
  namespace h = nycleus::hlir;
  namespace t = test::nycleus;

  auto cloned_stmts{block.stmts | std::views::transform(
    [](nycleus::hlir_ptr<h::stmt> const& stmt) {
      return t::clone_stmt(stmt.get());
    })};
  return nycleus::make_hlir<h::block>(block.loc,
    std::vector(cloned_stmts.begin(), cloned_stmts.end()),
    t::clone_expr(block.trail.get()));
}

} // (anonymous)

::nycleus::hlir_ptr<::nycleus::hlir::type> test::nycleus::clone_type(
  ::nycleus::hlir::type const* type
) {
  namespace h = ::nycleus::hlir;
  namespace t = test::nycleus;
  using result_t = ::nycleus::hlir_ptr<h::type>;

  if(!type) {
    return nullptr;
  }

  return type->visit(util::overloaded{
    [](h::int_type const& type) -> result_t {
      return ::nycleus::make_hlir<h::int_type>(type.loc, type.width,
        type.is_signed);
    },

    [](h::bool_type const& type) -> result_t {
      return ::nycleus::make_hlir<h::bool_type>(type.loc);
    },

    [](h::fn_type const& type) -> result_t {
      std::vector<result_t> param_types;
      param_types.reserve(type.param_types.size());
      for(auto const& param: type.param_types) {
        param_types.push_back(t::clone_type(param.get()));
      }
      return ::nycleus::make_hlir<h::fn_type>(type.loc, type.param_types
        | std::views::transform([](result_t const& param)
        { return t::clone_type(param.get()); }) | util::range_to<std::vector>(),
        t::clone_type(type.return_type.get()));
    },

    [](h::ptr_type const& type) -> result_t {
      return ::nycleus::make_hlir<h::ptr_type>(type.loc,
        t::clone_type(type.pointee_type.get()), type.is_mutable);
    },

    [](h::named_type const& type) -> result_t {
      return ::nycleus::make_hlir<h::named_type>(type.loc, type.name);
    },

    [](h::class_ref_type const& type) -> result_t {
      return ::nycleus::make_hlir<h::class_ref_type>(type.loc, *type.cl);
    }
  });
}

::nycleus::hlir_ptr<::nycleus::hlir::expr> test::nycleus::clone_expr(
  ::nycleus::hlir::expr const* expr
) {
  namespace h = ::nycleus::hlir;
  namespace t = test::nycleus;
  using result_t = ::nycleus::hlir_ptr<h::expr>;

  if(!expr) {
    return nullptr;
  }

  return expr->visit(util::overloaded{
    [](h::int_literal const& expr) -> result_t {
      return ::nycleus::make_hlir<h::int_literal>(expr.loc, expr.value,
        expr.type_suffix);
    },

    [](h::bool_literal const& expr) -> result_t {
      return ::nycleus::make_hlir<h::bool_literal>(expr.loc, expr.value);
    },

    [](h::named_var_ref const& expr) -> result_t {
      return ::nycleus::make_hlir<h::named_var_ref>(expr.loc, expr.name);
    },

    [](h::global_var_ref const& expr) -> result_t {
      return ::nycleus::make_hlir<h::global_var_ref>(expr.loc,
        *expr.target);
    },

    [](h::local_var_ref const& expr) -> result_t {
      return ::nycleus::make_hlir<h::local_var_ref>(expr.loc,
        *expr.target);
    },

    [](h::function_ref const& expr) -> result_t {
      return ::nycleus::make_hlir<h::function_ref>(expr.loc, *expr.target);
    },

    [](h::cmp_expr const& expr) -> result_t {
      return ::nycleus::make_hlir<h::cmp_expr>(
        expr.loc,
        t::clone_expr(expr.first_op.get()),
        expr.rels | std::views::transform([](h::cmp_expr::rel const& rel) {
          return h::cmp_expr::rel{rel.kind, t::clone_expr(rel.op.get()),
            rel.loc};
        }) | util::range_to<util::small_vector<h::cmp_expr::rel, 1>>()
      );
    },

    []<std::derived_from<h::binary_op_expr> T>(T const& expr) -> result_t {
      return ::nycleus::make_hlir<T>(expr.loc,
        t::clone_expr(expr.left.get()), t::clone_expr(expr.right.get()));
    },

    []<std::derived_from<h::unary_op_expr> T>(T const& expr) -> result_t {
      return ::nycleus::make_hlir<T>(expr.loc,
        t::clone_expr(expr.op.get()));
    },

    [](h::addr_expr const& expr) -> result_t {
      return ::nycleus::make_hlir<h::addr_expr>(expr.loc,
        t::clone_expr(expr.op.get()), expr.is_mutable);
    },

    [](h::named_member_expr const& expr) -> result_t {
      return ::nycleus::make_hlir<h::named_member_expr>(expr.loc,
        t::clone_expr(expr.op.get()), expr.name);
    },

    [](h::indexed_member_expr const& expr) -> result_t {
      return ::nycleus::make_hlir<h::indexed_member_expr>(expr.loc,
        t::clone_expr(expr.op.get()), expr.index);
    },

    [](h::call_expr const& expr) -> result_t {
      return ::nycleus::make_hlir<h::call_expr>(expr.loc,
        t::clone_expr(expr.callee.get()), expr.args
        | std::views::transform([](h::call_expr::arg const& arg) {
          return h::call_expr::arg{t::clone_expr(arg.value.get())};
        }) | util::range_to<std::vector>());
    },

    [](h::block const& expr) -> result_t {
      return clone_block(expr);
    }
  });
}

::nycleus::hlir_ptr<::nycleus::hlir::stmt> test::nycleus::clone_stmt(
  ::nycleus::hlir::stmt const* stmt
) {
  namespace h = ::nycleus::hlir;
  namespace t = test::nycleus;
  using result_t = ::nycleus::hlir_ptr<h::stmt>;

  if(!stmt) {
    return nullptr;
  }

  return stmt->visit(util::overloaded{
    [](h::expr_stmt const& stmt) -> result_t {
      return ::nycleus::make_hlir<h::expr_stmt>(
          t::clone_expr(stmt.content.get()));
    },

    [](h::assign_stmt const& stmt) -> result_t {
      return ::nycleus::make_hlir<h::assign_stmt>(
        t::clone_expr(stmt.target.get()), t::clone_expr(stmt.value.get()));
    },

    [](h::def_stmt const& stmt) -> result_t {
      return ::nycleus::make_hlir<h::def_stmt>(stmt.name,
        t::clone_def(stmt.value.get()));
    },

    [](h::local_var_init_stmt const& stmt) -> result_t {
      return ::nycleus::make_hlir<h::local_var_init_stmt>(*stmt.target,
        t::clone_expr(stmt.value.get()));
    }
  });
}

::nycleus::hlir_ptr<::nycleus::hlir::def> test::nycleus::clone_def(
  ::nycleus::hlir::def const* def
) {
  namespace h = ::nycleus::hlir;
  namespace t = test::nycleus;
  using result_t = ::nycleus::hlir_ptr<h::def>;

  if(!def) {
    return nullptr;
  }

  return def->visit(util::overloaded{
    [](h::fn_def const& def) -> result_t {
      return ::nycleus::make_hlir<h::fn_def>(def.core_loc,
        def.params | std::views::transform(
          [](h::fn_def::param const& param) {
            return h::fn_def::param{param.name, t::clone_type(param.type.get()),
              param.core_loc};
          }
        ) | util::range_to<std::vector>(),
        t::clone_type(def.return_type.get()),
        def.body ? clone_block(*def.body) : nullptr);
    },

    [](h::var_def const& def) -> result_t {
      return ::nycleus::make_hlir<h::var_def>(def.core_loc,
        t::clone_type(def.type.get()), t::clone_expr(def.init.get()),
        def.mutable_spec);
    },

    [](h::class_def const& def) -> result_t {
      return ::nycleus::make_hlir<h::class_def>(def.core_loc,
        def.members | std::views::transform(
          [](std::pair<std::u8string,
              ::nycleus::hlir_ptr<h::def>> const& member) {
            return std::pair<std::u8string, ::nycleus::hlir_ptr<h::def>>{
              member.first, t::clone_def(member.second.get())};
          }
        ) | util::range_to<std::vector>()
      );
    }
  });
}

////////////////////////////////////////////////////////////////////////////////
// HLIR output /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

namespace {

using test::tabs;

void print_type(nycleus::hlir::type const&, std::ostream&,
  std::uint64_t indent);
void print_expr(nycleus::hlir::expr const&, std::ostream&,
  std::uint64_t indent);
void print_stmt(nycleus::hlir::stmt const&, std::ostream&,
  std::uint64_t indent);
void print_def(nycleus::hlir::def const&, std::ostream&,
  std::uint64_t indent);

} // (anonymous)

std::ostream& nycleus::hlir::operator<<(std::ostream& os,
    nycleus::hlir::type const& type) {
  print_type(type, os, 0);
  return os;
}

std::ostream& nycleus::hlir::operator<<(std::ostream& os,
    nycleus::hlir::expr const& expr) {
  print_expr(expr, os, 0);
  return os;
}

std::ostream& nycleus::hlir::operator<<(std::ostream& os,
    nycleus::hlir::stmt const& stmt) {
  print_stmt(stmt, os, 0);
  return os;
}

std::ostream& nycleus::hlir::operator<<(std::ostream& os,
    nycleus::hlir::def const& def) {
  print_def(def, os, 0);
  return os;
}

namespace {

void print_type(nycleus::hlir::type const& type, std::ostream& os,
    std::uint64_t indent) {
  type.visit(util::overloaded{
    [&](nycleus::hlir::int_type const& type) {
      os << tabs{indent} << (type.is_signed ? u8's'_ch : u8'u'_ch)
        << type.width << u8" @ "_ch << type.loc << u8'\n'_ch;
    },

    [&](nycleus::hlir::bool_type const&) {
      os << tabs{indent} << u8"bool @ "_ch << type.loc << u8'\n'_ch;
    },

    [&](nycleus::hlir::fn_type const& type) {
      os << tabs{indent} << u8"function reference @ "_ch << type.loc
        << u8'\n'_ch << tabs{indent + 1};
      if(type.return_type) {
        os << u8"return type:\n"_ch;
        print_type(*type.return_type, os, indent + 2);
      } else {
        os << u8"returns void\n"_ch;
      }
      os << tabs{indent + 1} << u8"param types:\n"_ch;
      for(auto const& param: type.param_types) {
        if(param) {
          print_type(*param, os, indent + 2);
        } else {
          os << tabs{indent + 2} << u8"(invalid param)\n"_ch;
        }
      }
    },

    [&](nycleus::hlir::ptr_type const& type) {
      os << tabs{indent};
      if(type.is_mutable) {
        os << u8"mutable "_ch;
      }
      os << u8"pointer type @ "_ch << type.loc << u8'\n'_ch;
      if(type.pointee_type) {
        print_type(*type.pointee_type, os, indent + 1);
      } else {
        os << tabs{indent + 1} << u8"(invalid pointee type)\n"_ch;
      }
    },

    [&](nycleus::hlir::named_type const& type) {
      os << tabs{indent} << u8"named type "_ch << util::as_char(type.name)
        << u8" @ "_ch << type.loc << u8'\n'_ch;
    },

    [&](nycleus::hlir::class_ref_type const& type) {
      os << tabs{indent} << u8"class "_ch << type.cl.get() << u8" @ "_ch
        << type.loc << u8'\n'_ch;
    }
  });
}

void print_expr(nycleus::hlir::expr const& expr, std::ostream& os,
    std::uint64_t indent) {
  expr.visit(util::overloaded{
    [&](nycleus::hlir::int_literal const& expr) {
      os << tabs{indent} << u8"int "_ch << expr.value;
      if(expr.type_suffix) {
        os << (expr.type_suffix->is_signed ? u8's'_ch : u8'u'_ch)
          << expr.type_suffix->width;
      }
      os << u8" @ "_ch << expr.loc << u8'\n'_ch;
    },

    [&](nycleus::hlir::bool_literal const& expr) {
      os << tabs{indent} << u8"bool "_ch
        << (expr.value ? u8"true"_ch : u8"false"_ch) << u8" @ "_ch << expr.loc
        << u8'\n'_ch;
    },

    [&](nycleus::hlir::global_var_ref const& expr) {
      os << tabs{indent} << u8"global var "_ch << expr.target.get()
        << u8" @ "_ch << expr.loc << u8'\n'_ch;
    },

    [&](nycleus::hlir::named_var_ref const& expr) {
      os << tabs{indent} << u8"named var "_ch << util::as_char(expr.name)
        << u8" @ "_ch << expr.loc << u8'\n'_ch;
    },

    [&](nycleus::hlir::local_var_ref const& expr) {
      os << tabs{indent} << u8"local var "_ch << expr.target.get()
        << u8" @ "_ch << expr.loc << u8'\n'_ch;
    },

    [&](nycleus::hlir::function_ref const& expr) {
      os << tabs{indent} << u8"function "_ch << expr.target.get()
        << u8" @ "_ch << expr.loc << u8'\n'_ch;
    },

    [&](nycleus::hlir::cmp_expr const& expr) {
      os << tabs{indent} << u8"cmp @ "_ch << expr.loc << u8'\n'_ch;
      if(expr.first_op) {
        print_expr(*expr.first_op, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid op)\n"_ch;
      }
      for(auto const& rel: expr.rels) {
        os << tabs{indent + 1};
        switch(rel.kind) {
          case nycleus::hlir::cmp_expr::rel_kind::eq: os << u8"=="_ch; break;
          case nycleus::hlir::cmp_expr::rel_kind::gt: os << u8'>'_ch; break;
          case nycleus::hlir::cmp_expr::rel_kind::ge: os << u8">="_ch; break;
          case nycleus::hlir::cmp_expr::rel_kind::lt: os << u8'<'_ch; break;
          case nycleus::hlir::cmp_expr::rel_kind::le: os << u8"<="_ch; break;
        }
        os << u8" @ "_ch << rel.loc << u8'\n'_ch;
        if(rel.op) {
          print_expr(*rel.op, os, indent + 1);
        } else {
          os << tabs{indent} << u8"  (invalid op)\n"_ch;
        }
      }
    },

    [&](nycleus::hlir::binary_op_expr const& expr) {
      os << tabs{indent};
      switch(expr.get_kind()) {
        case nycleus::hlir::expr::kind_t::or_expr:   os << u8"||"_ch; break;
        case nycleus::hlir::expr::kind_t::and_expr:  os << u8"&&"_ch; break;
        case nycleus::hlir::expr::kind_t::xor_expr:  os << u8"^^"_ch; break;
        case nycleus::hlir::expr::kind_t::neq_expr:  os << u8"!="_ch; break;
        case nycleus::hlir::expr::kind_t::add_expr:  os << u8'+'_ch; break;
        case nycleus::hlir::expr::kind_t::sub_expr:
          os << u8"- (binary)"_ch;
          break;
        case nycleus::hlir::expr::kind_t::mul_expr:  os << u8'*'_ch; break;
        case nycleus::hlir::expr::kind_t::div_expr:  os << u8'/'_ch; break;
        case nycleus::hlir::expr::kind_t::mod_expr:  os << u8'%'_ch; break;
        case nycleus::hlir::expr::kind_t::bor_expr:  os << u8'|'_ch; break;
        case nycleus::hlir::expr::kind_t::band_expr: os << u8'&'_ch; break;
        case nycleus::hlir::expr::kind_t::bxor_expr: os << u8'^'_ch; break;
        case nycleus::hlir::expr::kind_t::lsh_expr:  os << u8"<<"_ch; break;
        case nycleus::hlir::expr::kind_t::rsh_expr:  os << u8">>"_ch; break;
        case nycleus::hlir::expr::kind_t::lrot_expr: os << u8"</"_ch; break;
        case nycleus::hlir::expr::kind_t::rrot_expr: os << u8">/"_ch; break;
        default: util::unreachable();
      }
      os << u8" @ "_ch << expr.loc << u8'\n'_ch;
      if(expr.left) {
        print_expr(*expr.left, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid left)\n"_ch;
      }
      if(expr.right) {
        print_expr(*expr.right, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid right)\n"_ch;
      }
    },

    [&](nycleus::hlir::unary_op_expr const& expr) {
      os << tabs{indent};
      switch(expr.get_kind()) {
        case nycleus::hlir::expr::kind_t::neg_expr:
          os << u8"- (unary)"_ch;
          break;
        case nycleus::hlir::expr::kind_t::bnot_expr:  os << u8'~'_ch; break;
        case nycleus::hlir::expr::kind_t::not_expr:   os << u8'!'_ch; break;
        case nycleus::hlir::expr::kind_t::deref_expr: os << u8'*'_ch; break;
        default: util::unreachable();
      }
      os << u8" @ "_ch << expr.loc << u8'\n'_ch;
      if(expr.op) {
        print_expr(*expr.op, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid op)\n"_ch;
      }
    },

    [&](nycleus::hlir::addr_expr const& expr) {
      os << tabs{indent} << u8'&'_ch;
      if(expr.is_mutable) {
        os << u8"mut"_ch;
      }
      os << u8" @ "_ch << expr.loc << u8'\n'_ch;
      if(expr.op) {
        print_expr(*expr.op, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid op)"_ch;
      }
    },

    [&](nycleus::hlir::named_member_expr const& expr) {
      os << tabs{indent} << u8"named member: "_ch << util::as_char(expr.name)
        << u8" @ "_ch << expr.loc << u8'\n'_ch;
      if(expr.op) {
        print_expr(*expr.op, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid op)\n"_ch;
      }
    },

    [&](nycleus::hlir::indexed_member_expr const& expr) {
      os << tabs{indent} << u8"indexed member: "_ch << expr.index
        << u8" @ "_ch << expr.loc << u8'\n'_ch;
      if(expr.op) {
        print_expr(*expr.op, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid op)\n"_ch;
      }
    },

    [&](nycleus::hlir::call_expr const& expr) {
      os << tabs{indent} << u8"call expr @ "_ch << expr.loc << u8'\n'_ch
        << tabs{indent} << u8"  callee:\n"_ch;
      if(expr.callee) {
        print_expr(*expr.callee, os, indent + 2);
      } else {
        os << tabs{indent} << u8"    (invalid callee)\n"_ch;
      }

      os << tabs{indent} << u8"  args:\n"_ch;
      for(auto const& arg: expr.args) {
        if(arg.value) {
          print_expr(*arg.value, os, indent + 1);
        } else {
          os << tabs{indent} << u8"  (invalid arg)\n"_ch;
        }
      }
    },

    [&](nycleus::hlir::block const& expr) {
      os << tabs{indent} << u8"block @ "_ch << expr.loc << u8'\n'_ch;

      for(auto const& el: expr.stmts) {
        if(el) {
          print_stmt(*el, os, indent + 1);
        } else {
          os << tabs{indent} << u8"  (invalid stmt)\n"_ch;
        }
      }

      if(expr.trail) {
        os << tabs{indent} << u8"  trail:\n"_ch;
        print_expr(*expr.trail, os, indent + 2);
      }
    }
  });
}

void print_stmt(nycleus::hlir::stmt const& stmt, std::ostream& os,
    std::uint64_t indent) {
  stmt.visit(util::overloaded{
    [&](nycleus::hlir::expr_stmt const& stmt) {
      if(stmt.content) {
        print_expr(*stmt.content, os, indent);
      } else {
        os << tabs{indent} << u8"(invalid expr)\n"_ch;
      }
    },

    [&](nycleus::hlir::assign_stmt const& stmt) {
      os << tabs{indent} << u8"assign:\n"_ch << tabs{indent}
        << u8"  target:\n"_ch;
      if(stmt.target) {
        print_expr(*stmt.target, os, indent + 2);
      } else {
        os << tabs{indent} << u8"    (invalid target)\n"_ch;
      }
      os << tabs{indent} << u8"  value:\n"_ch;
      if(stmt.value) {
        print_expr(*stmt.target, os, indent + 2);
      } else {
        os << tabs{indent} << u8"    (invalid value)\n"_ch;
      }
    },

    [&](nycleus::hlir::def_stmt const& stmt) {
      os << tabs{indent} << u8"def "_ch << util::as_char(stmt.name)
        << u8'\n'_ch;
      if(stmt.value) {
        print_def(*stmt.value, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid definition)\n"_ch;
      }
    },

    [&](nycleus::hlir::local_var_init_stmt const& stmt) {
      os << tabs{indent} << u8"init local var "_ch << stmt.target
        << u8" with:\n"_ch;
      if(stmt.value) {
        print_expr(*stmt.value, os, indent + 1);
      } else {
        os << tabs{indent} << u8"  (invalid value)\n"_ch;
      }
    }
  });
}

void print_def(nycleus::hlir::def const& def, std::ostream& os,
    std::uint64_t indent) {
  def.visit(util::overloaded{
    [&](nycleus::hlir::fn_def const& def) {
      os << tabs{indent} << u8"function definition @ "_ch << def.core_loc
        << u8'\n'_ch << tabs{indent} << u8"  params:\n"_ch;
      for(auto const& param: def.params) {
        os << tabs{indent} << u8"    "_ch;
        if(param.name) {
          os << util::as_char(*param.name);
        } else {
          os << u8"(anonymous parameter)"_ch;
        }
        os << u8" @ "_ch << param.core_loc << u8":\n"_ch;
        assert(param.type);
        print_type(*param.type, os, indent + 3);
      }

      if(def.return_type) {
        os << tabs{indent} << u8"  return type:\n"_ch;
        print_type(*def.return_type, os, indent + 2);
      }

      os << tabs{indent} << u8"  body:\n"_ch;
      if(def.body) {
        print_expr(*def.body, os, indent + 2);
      } else {
        os << tabs{indent} << u8"    (invalid body)\n"_ch;
      }
    },

    [&](nycleus::hlir::var_def const& def) {
      os << tabs{indent};

      switch(def.mutable_spec) {
        case nycleus::hlir::var_def::mutable_spec_t::none:
          break;

        case nycleus::hlir::var_def::mutable_spec_t::mut:
          os << u8"mutable "_ch;
          break;

        case nycleus::hlir::var_def::mutable_spec_t::constant:
          os << u8"constant "_ch;
          break;
      }

      os << u8"variable definition @ "_ch << def.core_loc << u8'\n'_ch
        << tabs{indent} << u8"  type:\n"_ch;
      if(def.type) {
        print_type(*def.type, os, indent + 2);
      } else {
        os << tabs{indent} << u8"    (invalid type)\n"_ch;
      }

      os << tabs{indent} << u8"  init:\n"_ch;
      if(def.init) {
        print_expr(*def.init, os, indent + 2);
      } else {
        os << tabs{indent} << u8"    (no initializer)\n"_ch;
      }
    },

    [&](nycleus::hlir::class_def const& def) {
      os << tabs{indent} << u8"class definition @ "_ch << def.core_loc
        << u8'\n'_ch;
      for(auto const& [name, member]: def.members) {
        os << tabs{indent + 1} << util::as_char(name) << u8":\n"_ch;
        if(member) {
          print_def(*member, os, indent + 2);
        } else {
          os << tabs{indent} << u8"    (invalid member)\n"_ch;
        }
      }
    }
  });
}

} // (anonymous)

////////////////////////////////////////////////////////////////////////////////
// HLIR comparison /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Type comparison /////////////////////////////////////////////////////////////

namespace {

[[nodiscard]] bool compare_type(auto const&, auto const&) noexcept {
  return false;
}

[[nodiscard]] bool compare_type(
  nycleus::hlir::int_type const& a,
  nycleus::hlir::int_type const& b
) noexcept {
  return a.width == b.width && a.is_signed == b.is_signed;
}

[[nodiscard]] bool compare_type(
  nycleus::hlir::bool_type const&,
  nycleus::hlir::bool_type const&
) noexcept {
  return true;
}

[[nodiscard]] bool compare_type(
  nycleus::hlir::fn_type const& a,
  nycleus::hlir::fn_type const& b
) noexcept {
  return (!a.return_type && !b.return_type
    || a.return_type && b.return_type && *a.return_type == *b.return_type)
    && std::ranges::equal(a.param_types, b.param_types,
      [](::nycleus::hlir_ptr<::nycleus::hlir::type> const& a,
          ::nycleus::hlir_ptr<::nycleus::hlir::type> const& b) noexcept {
        return !a && !b || a && b && *a == *b;
      });
}

[[nodiscard]] bool compare_type(
  nycleus::hlir::ptr_type const& a,
  nycleus::hlir::ptr_type const& b
) noexcept {
  return (!a.pointee_type && !b.pointee_type
    || a.pointee_type && b.pointee_type && *a.pointee_type == *b.pointee_type)
    && a.is_mutable == b.is_mutable;
}

[[nodiscard]] bool compare_type(
  nycleus::hlir::named_type const& a,
  nycleus::hlir::named_type const& b
) noexcept {
  return a.name == b.name;
}

[[nodiscard]] bool compare_type(
  nycleus::hlir::class_ref_type const& a,
  nycleus::hlir::class_ref_type const& b
) noexcept {
  return a.cl == b.cl;
}

} // (anonymous)

bool nycleus::hlir::operator==(nycleus::hlir::type const& a,
    nycleus::hlir::type const& b) noexcept {
  return a.loc == b.loc && a.visit([&](auto const& a_der) noexcept {
    return b.visit([&](auto const& b_der) noexcept {
      return compare_type(a_der, b_der);
    });
  });
}

// Expression comparison ///////////////////////////////////////////////////////

namespace {

[[nodiscard]] bool compare_expr(auto const&, auto const&) noexcept {
  return false;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::int_literal const& a,
  nycleus::hlir::int_literal const& b
) noexcept {
  return a.value == b.value
    && (!a.type_suffix && !b.type_suffix || a.type_suffix && b.type_suffix
      && a.type_suffix->width == b.type_suffix->width
      && a.type_suffix->is_signed == b.type_suffix->is_signed);
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::bool_literal const& a,
  nycleus::hlir::bool_literal const& b
) noexcept {
  return a.value == b.value;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::named_var_ref const& a,
  nycleus::hlir::named_var_ref const& b
) noexcept {
  return a.name == b.name;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::global_var_ref const& a,
  nycleus::hlir::global_var_ref const& b
) noexcept {
  return a.target == b.target;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::local_var_ref const& a,
  nycleus::hlir::local_var_ref const& b
) noexcept {
  return a.target == b.target;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::function_ref const& a,
  nycleus::hlir::function_ref const& b
) noexcept {
  return a.target == b.target;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::cmp_expr const& a,
  nycleus::hlir::cmp_expr const& b
) noexcept {
  return (!a.first_op && !b.first_op
      || a.first_op && b.first_op && *a.first_op == *b.first_op)
    && std::ranges::equal(a.rels, b.rels,
      [](
        ::nycleus::hlir::cmp_expr::rel const& a,
        ::nycleus::hlir::cmp_expr::rel const& b
      ) noexcept {
        return a.kind == b.kind && a.loc == b.loc
          && (!a.op && !b.op || a.op && b.op && *a.op == *b.op);
      }
    );
}

[[nodiscard]] bool compare_expr(
  std::derived_from<nycleus::hlir::binary_op_expr> auto const& a,
  std::derived_from<nycleus::hlir::binary_op_expr> auto const& b
) noexcept {
  return (!a.left && !b.left || a.left && b.left && *a.left == *b.left)
    && (!a.right && !b.right || a.right && b.right && *a.right == *b.right);
}

[[nodiscard]] bool compare_expr(
  std::derived_from<nycleus::hlir::unary_op_expr> auto const& a,
  std::derived_from<nycleus::hlir::unary_op_expr> auto const& b
) noexcept {
  return !a.op && !b.op || a.op && b.op && *a.op == *b.op;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::addr_expr const& a,
  nycleus::hlir::addr_expr const& b
) noexcept {
  return (!a.op && !b.op || a.op && b.op && *a.op == *b.op)
    && a.is_mutable == b.is_mutable;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::named_member_expr const& a,
  nycleus::hlir::named_member_expr const& b
) noexcept {
  return (!a.op && !b.op || a.op && b.op && *a.op == *b.op)
    && a.name == b.name;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::indexed_member_expr const& a,
  nycleus::hlir::indexed_member_expr const& b
) noexcept {
  return (!a.op && !b.op || a.op && b.op && *a.op == *b.op)
    && a.index == b.index;
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::call_expr const& a,
  nycleus::hlir::call_expr const& b
) noexcept {
  return (!a.callee && !b.callee || a.callee && b.callee
      && *a.callee == *b.callee)
    && std::ranges::equal(a.args, b.args,
      [](::nycleus::hlir::call_expr::arg const& a,
          ::nycleus::hlir::call_expr::arg const& b) noexcept {
        return !a.value && !b.value
          || a.value && b.value && *a.value == *b.value;
      }
    );
}

[[nodiscard]] bool compare_expr(
  nycleus::hlir::block const& a,
  nycleus::hlir::block const& b
) noexcept {
  return std::ranges::equal(a.stmts, b.stmts,
    [](::nycleus::hlir_ptr<::nycleus::hlir::stmt> const& a,
        ::nycleus::hlir_ptr<::nycleus::hlir::stmt> const& b) noexcept {
      return !a && !b || a && b && *a == *b;
    }
  ) && (!a.trail && !b.trail || a.trail && b.trail
    && *a.trail == *b.trail);
}

} // (anonymous)

bool nycleus::hlir::operator==(nycleus::hlir::expr const& a,
    nycleus::hlir::expr const& b) noexcept {
  return a.loc == b.loc && a.visit([&](auto const& a_der) noexcept {
    return b.visit([&](auto const& b_der) noexcept {
      return compare_expr(a_der, b_der);
    });
  });
}

// Statement comparison ////////////////////////////////////////////////////////

namespace {

[[nodiscard]] bool compare_stmt(auto const&, auto const&) noexcept {
  return false;
}

[[nodiscard]] bool compare_stmt(
  nycleus::hlir::expr_stmt const& a,
  nycleus::hlir::expr_stmt const& b
) {
  return !a.content && !b.content || a.content && b.content
    && *a.content == *b.content;
}

[[nodiscard]] bool compare_stmt(
  nycleus::hlir::assign_stmt const& a,
  nycleus::hlir::assign_stmt const& b
) noexcept {
  return (!a.target && !b.target || a.target && b.target
      && *a.target == *b.target)
    && (!a.value && !b.value || a.value && b.value && *a.value == *b.value);
}

[[nodiscard]] bool compare_stmt(
  nycleus::hlir::def_stmt const& a,
  nycleus::hlir::def_stmt const& b
) noexcept {
  return a.name == b.name
    && (!a.value && !b.value || a.value && b.value && *a.value == *b.value);
}

[[nodiscard]] bool compare_stmt(
  nycleus::hlir::local_var_init_stmt const& a,
  nycleus::hlir::local_var_init_stmt const& b
) noexcept {
  return a.target == b.target
    && (!a.value && !b.value || a.value && b.value && *a.value == *b.value);
}

} // (anonymous)

bool nycleus::hlir::operator==(nycleus::hlir::stmt const& a,
    nycleus::hlir::stmt const& b) noexcept {
  return a.visit([&](auto const& a_der) noexcept {
    return b.visit([&](auto const& b_der) noexcept {
      return compare_stmt(a_der, b_der);
    });
  });
}

// Definition comparison ///////////////////////////////////////////////////////

namespace {

[[nodiscard]] bool compare_def(auto const&, auto const&) noexcept {
  return false;
}

[[nodiscard]] bool compare_def(
  nycleus::hlir::fn_def const& a,
  nycleus::hlir::fn_def const& b
) noexcept {
  return (!a.return_type && !b.return_type || a.return_type && b.return_type
      && *a.return_type == *b.return_type)
    && (!a.body && !b.body || a.body && b.body && *a.body == *b.body)
    && std::ranges::equal(a.params, b.params,
      [](nycleus::hlir::fn_def::param const& a,
          nycleus::hlir::fn_def::param const& b) noexcept {
        return a.name == b.name
          && (!a.type && !b.type || a.type && b.type && *a.type == *b.type)
          && a.core_loc == b.core_loc;
      }
    );
}

[[nodiscard]] bool compare_def(
  nycleus::hlir::var_def const& a,
  nycleus::hlir::var_def const& b
) noexcept {
  return (!a.type && !b.type || a.type && b.type && *a.type == *b.type)
    && (!a.init && !b.init || a.init && b.init && *a.init == *b.init)
    && a.mutable_spec == b.mutable_spec;
}

[[nodiscard]] bool compare_def(
  nycleus::hlir::class_def const& a,
  nycleus::hlir::class_def const& b
) noexcept {
  return std::ranges::equal(a.members, b.members,
    [](
      std::pair<std::u8string,
        ::nycleus::hlir_ptr<nycleus::hlir::def>> const& a,
      std::pair<std::u8string,
        ::nycleus::hlir_ptr<nycleus::hlir::def>> const& b
    ) noexcept {
      return a.first == b.first && (!a.second && !b.second
        || a.second && b.second && *a.second == *b.second);
    });
}

} // (anonymous)

bool nycleus::hlir::operator==(
  nycleus::hlir::def const& a,
  nycleus::hlir::def const& b
) noexcept {
  return a.core_loc == b.core_loc && a.visit([&](auto const& a_der) noexcept {
    return b.visit([&](auto const& b_der) noexcept {
      return compare_def(a_der, b_der);
    });
  });
}
