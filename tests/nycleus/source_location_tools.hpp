#ifndef TESTS_NYCLEUS_SOURCE_LOCATION_TOOLS_HPP_INCLUDED_\
OQZ5P8QUH80VBRJ7S9MBSS56Z
#define TESTS_NYCLEUS_SOURCE_LOCATION_TOOLS_HPP_INCLUDED_\
OQZ5P8QUH80VBRJ7S9MBSS56Z

#include "nycleus/source_location.hpp"
#include <cstddef>
#include <functional>
#include <iosfwd>

namespace nycleus {

std::ostream& operator<<(std::ostream&, source_location);

} // nycleus

template<>
struct std::hash<::nycleus::file_pos> {
  [[nodiscard]] ::std::size_t operator()(::nycleus::file_pos) const noexcept;
};

template<>
struct std::hash<::nycleus::source_location> {
  [[nodiscard]] ::std::size_t operator()(::nycleus::source_location const&)
    const noexcept;
};

#endif
