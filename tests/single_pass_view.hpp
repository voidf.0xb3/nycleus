#ifndef TESTS_SINGLE_PASS_VIEW_HPP_INCLUDED_42SPCU5SCLDLEAWQTNXBJ1W3P
#define TESTS_SINGLE_PASS_VIEW_HPP_INCLUDED_42SPCU5SCLDLEAWQTNXBJ1W3P

#include "util/iterator_facade.hpp"
#include "util/range_adaptor_closure.hpp"
#include <iterator>
#include <ranges>
#include <type_traits>
#include <utility>

namespace tests {

/** @brief A range adaptor that turns the underlying range into a single-pass range, that is to
 * say, an input range that is not a forward range. */
template<std::ranges::view V>
requires std::ranges::input_range<V>
class single_pass_view: public std::ranges::view_interface<single_pass_view<V>> {
private:
  V view_;

  template<bool Const>
  class iterator_core;

  class sentinel {
  private:
    [[no_unique_address]] std::ranges::sentinel_t<V> iend_;

    template<bool Const>
    friend class iterator_core;
    friend class single_pass_view;

    constexpr explicit sentinel(std::ranges::sentinel_t<V> iend)
      noexcept(std::is_nothrow_move_constructible_v<std::ranges::sentinel_t<V>>):
      iend_{std::move(iend)} {}

  public:
    constexpr sentinel() = default;
  };

  template<bool Const>
  class iterator_core {
  private:
    using base_view = std::conditional_t<Const, V const, V>;
    using base_iterator = std::ranges::iterator_t<base_view>;

    base_iterator it_;

    friend class single_pass_view;
    explicit constexpr iterator_core(base_iterator it)
      noexcept(std::is_nothrow_move_constructible_v<base_iterator>): it_{std::move(it)} {}

  public:
    constexpr iterator_core() = default;

    [[nodiscard]] constexpr base_iterator const& base() const& noexcept {
      return it_;
    }

    [[nodiscard]] constexpr base_iterator base() &&
        noexcept(std::is_nothrow_move_constructible_v<base_iterator>) {
      return std::move(it_);
    }

    constexpr iterator_core(iterator_core<!Const> other)
      noexcept(std::is_nothrow_constructible_v<base_iterator, std::ranges::iterator_t<V>&&>)
      requires Const && std::convertible_to<std::ranges::iterator_t<V>, base_iterator>:
      it_{std::move(other).base()} {}

  protected:
    static constexpr bool is_single_pass{true};

    using value_type = std::iter_value_t<base_iterator>;

    using difference_type = std::iter_difference_t<base_iterator>;

    [[nodiscard]] constexpr decltype(auto) dereference() const
        noexcept(noexcept(*it_)) {
      return *it_;
    }

    constexpr void increment() noexcept(noexcept(++it_)) {
      ++it_;
    }

    [[nodiscard]] constexpr bool equal_to(iterator_core const& other) const
        noexcept(noexcept(it_ == other.it_))
        requires std::sentinel_for<base_iterator, base_iterator> {
      return it_ == other.it_;
    }

    [[nodiscard]] constexpr bool equal_to(sentinel const& iend) const
        noexcept(noexcept(it_ == iend.iend_)) {
      return it_ == iend.iend_;
    }

    [[nodiscard]] friend constexpr auto iter_move(iterator_core const& it)
        noexcept(noexcept(std::ranges::iter_move(it.it_))) {
      return std::ranges::iter_move(it.it_);
    }
  };

  template<bool Const>
  using iterator = util::iterator_facade<iterator_core<Const>>;

public:
  constexpr single_pass_view() = default;

  constexpr explicit single_pass_view(V view)
    noexcept(std::is_nothrow_move_constructible_v<V>): view_{std::move(view)} {}

  [[nodiscard]] constexpr V const& base() const& noexcept {
    return view_;
  }

  [[nodiscard]] constexpr V&& base() && noexcept(std::is_nothrow_move_constructible_v<V>) {
    return std::move(view_);
  }

  [[nodiscard]] constexpr auto begin()
  noexcept(
    noexcept(std::ranges::begin(view_))
    && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<V>>
  ) requires (
    !std::ranges::range<V const>
    || !std::same_as<std::ranges::iterator_t<V>,
      std::ranges::iterator_t<V const>>
    || !std::same_as<std::ranges::sentinel_t<V>,
      std::ranges::sentinel_t<V const>>
  ) {
    return iterator<false>{std::ranges::begin(view_)};
  }

  [[nodiscard]] constexpr auto begin() const
  noexcept(
    noexcept(std::ranges::begin(view_))
    && std::is_nothrow_move_constructible_v<std::ranges::iterator_t<V const>>
  ) requires std::ranges::input_range<V const> {
    return iterator<true>{std::ranges::begin(view_)};
  }

  [[nodiscard]] constexpr auto end()
  noexcept(
    noexcept(std::ranges::end(view_))
    && (
      !std::ranges::common_range<V>
      || std::is_nothrow_move_constructible_v<std::ranges::iterator_t<V>>
    )
  )
  requires (
    !std::ranges::range<V const>
    || !std::same_as<std::ranges::iterator_t<V>,
      std::ranges::iterator_t<V const>>
    || !std::same_as<std::ranges::sentinel_t<V>,
      std::ranges::sentinel_t<V const>>
  ) {
    if constexpr(std::ranges::common_range<V>) {
      return iterator<false>{std::ranges::end(view_)};
    } else {
      return std::ranges::end(view_) && std::is_nothrow_move_constructible_v<V>;
    }
  }

  [[nodiscard]] constexpr auto end() const
  noexcept(
    noexcept(std::ranges::end(view_))
    && (
      !std::ranges::common_range<V const>
      || std::is_nothrow_move_constructible_v<
        std::ranges::iterator_t<V const>>
    )
  ) requires std::ranges::input_range<V const> {
    if constexpr(std::ranges::common_range<V const>) {
      return iterator<true>{std::ranges::end(view_)};
    } else {
      return sentinel{std::ranges::end(view_)};
    }
  }

  [[nodiscard]] constexpr auto size()
  noexcept(noexcept(std::ranges::size(view_)))
  requires std::ranges::sized_range<V>
  {
    return std::ranges::size(view_);
  }

  [[nodiscard]] constexpr auto size() const
  noexcept(noexcept(std::ranges::size(view_)))
  requires std::ranges::sized_range<V const>
  {
    return std::ranges::size(view_);
  }
};

template<typename R>
single_pass_view(R&&) -> single_pass_view<std::views::all_t<R>>;

} // tests

template<::std::ranges::view V>
requires ::std::ranges::input_range<V>
constexpr bool std::ranges::enable_borrowed_range<::tests::single_pass_view<V>>{
  ::std::ranges::enable_borrowed_range<V>};

namespace tests {

namespace detail_ {

struct single_pass_t: public util::range_adaptor_closure<single_pass_t> {
  template<std::ranges::viewable_range R>
  [[nodiscard]] constexpr auto operator()(R&& range) const noexcept(noexcept(
      single_pass_view{std::views::all_t<R>{std::forward<R>(range)}})) {
    return single_pass_view{std::views::all_t<R>{std::forward<R>(range)}};
  }
};

} // detail_

inline constexpr detail_::single_pass_t single_pass;

} // tests

#endif
