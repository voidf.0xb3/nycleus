#define BOOST_TEST_MODULE ucd
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#ifdef NDEBUG
  #error The test suite must be built in debug mode.
#endif

#include "unicode/encoding.hpp"
#include "unicode/normal.hpp"
#include "util/u8compat.hpp"
#include "single_pass_view.hpp"
#include <algorithm>
#include <iterator>
#include <ranges>
#include <string>

#include "unicode/ucd/ucd_test_data.hpp"

namespace bdata = boost::unit_test::data;

////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(ucd_tests)
BOOST_AUTO_TEST_SUITE(nfc)

BOOST_DATA_TEST_CASE(input,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  std::u32string nfc_of_input;
  std::ranges::copy(input | tests::single_pass | unicode::nfc,
    std::back_inserter(nfc_of_input));
  BOOST_TEST((nfc_of_input == nfc));

  std::u32string nfc_of_nfc;
  std::ranges::copy(nfc | tests::single_pass | unicode::nfc,
    std::back_inserter(nfc_of_nfc));
  BOOST_TEST((nfc_of_nfc == nfc));

  std::u32string nfc_of_nfd;
  std::ranges::copy(nfd | tests::single_pass | unicode::nfc,
    std::back_inserter(nfc_of_nfd));
  BOOST_TEST((nfc_of_nfd == nfc));
}

BOOST_AUTO_TEST_CASE(input_singles) {
  for(auto const run: unicode::ucd::single_cps) {
    for(char32_t const ch: std::views::iota(run.first.unwrap(), run.second.unwrap())) {
      std::u32string input{1, ch};

      std::u32string nfc;
      std::ranges::copy(input | tests::single_pass | unicode::nfc,
        std::back_inserter(nfc));
      BOOST_TEST((nfc == input));
    }
  }
}

BOOST_DATA_TEST_CASE(forward,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  std::u32string nfc_of_input;
  std::ranges::copy(input | unicode::nfc, std::back_inserter(nfc_of_input));
  BOOST_TEST((nfc_of_input == nfc));

  std::u32string nfc_of_nfc;
  std::ranges::copy(input | unicode::nfc, std::back_inserter(nfc_of_nfc));
  BOOST_TEST((nfc_of_nfc == nfc));

  std::u32string nfc_of_nfd;
  std::ranges::copy(input | unicode::nfc, std::back_inserter(nfc_of_nfd));
  BOOST_TEST((nfc_of_nfd == nfc));
}

BOOST_AUTO_TEST_CASE(forward_singles) {
  for(auto const run: unicode::ucd::single_cps) {
    for(char32_t const ch: std::views::iota(run.first.unwrap(), run.second.unwrap())) {
      std::u32string input{1, ch};

      std::u32string nfc;
      std::ranges::copy(input | unicode::nfc, std::back_inserter(nfc));
      BOOST_TEST((nfc == input));
    }
  }
}

BOOST_DATA_TEST_CASE(eager_input,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  std::u32string const nfc_of_input{unicode::to_nfc(input
    | tests::single_pass)};
  BOOST_TEST((nfc_of_input == nfc));

  std::u32string const nfc_of_nfc{unicode::to_nfc(nfc | tests::single_pass)};
  BOOST_TEST((nfc_of_nfc == nfc));

  std::u32string const nfc_of_nfd{unicode::to_nfc(nfd | tests::single_pass)};
  BOOST_TEST((nfc_of_nfd == nfc));
}

BOOST_AUTO_TEST_CASE(eager_input_singles) {
  for(auto const run: unicode::ucd::single_cps) {
    for(char32_t const ch: std::views::iota(run.first.unwrap(), run.second.unwrap())) {
      std::u32string const input{1, ch};
      std::u32string const nfc{unicode::to_nfc(input | tests::single_pass)};
      BOOST_TEST((nfc == input));
    }
  }
}

BOOST_DATA_TEST_CASE(eager_forward,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  std::u32string const nfc_of_input{unicode::to_nfc(input)};
  BOOST_TEST((nfc_of_input == nfc));

  std::u32string const nfc_of_nfc{unicode::to_nfc(nfc)};
  BOOST_TEST((nfc_of_nfc == nfc));

  std::u32string const nfc_of_nfd{unicode::to_nfc(nfd)};
  BOOST_TEST((nfc_of_nfd == nfc));
}

BOOST_AUTO_TEST_CASE(eager_forward_singles) {
  for(auto const run: unicode::ucd::single_cps) {
    for(char32_t const ch: std::views::iota(run.first.unwrap(), run.second.unwrap())) {
      std::u32string const input{1, ch};
      std::u32string const nfc{unicode::to_nfc(input)};
      BOOST_TEST((nfc == input));
    }
  }
}

BOOST_DATA_TEST_CASE(check_input,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  if(input != nfc) {
    BOOST_TEST(!unicode::is_nfc(input | tests::single_pass));
  }

  BOOST_TEST(unicode::is_nfc(nfc | tests::single_pass));

  if(nfd != nfc) {
    BOOST_TEST(!unicode::is_nfc(nfd | tests::single_pass));
  }
}

BOOST_DATA_TEST_CASE(check_forward,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  if(input != nfc) {
    BOOST_TEST(!unicode::is_nfc(input));
  }

  BOOST_TEST(unicode::is_nfc(nfc));

  if(nfd != nfc) {
    BOOST_TEST(!unicode::is_nfc(nfd));
  }
}

BOOST_AUTO_TEST_SUITE_END() // nfc

////////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE(nfd)
BOOST_DATA_TEST_CASE(input,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  std::u32string nfd_of_input;
  std::ranges::copy(input | tests::single_pass | unicode::nfd,
    std::back_inserter(nfd_of_input));
  BOOST_TEST((nfd_of_input == nfd));

  std::u32string nfd_of_nfc;
  std::ranges::copy(input | tests::single_pass | unicode::nfd,
    std::back_inserter(nfd_of_nfc));
  BOOST_TEST((nfd_of_nfc == nfd));

  std::u32string nfd_of_nfd;
  std::ranges::copy(input | tests::single_pass | unicode::nfd,
    std::back_inserter(nfd_of_nfd));
  BOOST_TEST((nfd_of_nfd == nfd));
}

BOOST_AUTO_TEST_CASE(input_singles) {
  for(auto const run: unicode::ucd::single_cps) {
    for(char32_t const ch: std::views::iota(run.first.unwrap(), run.second.unwrap())) {
      std::u32string input{1, ch};

      std::u32string nfd;
      std::ranges::copy(input | tests::single_pass | unicode::nfd,
        std::back_inserter(nfd));
      BOOST_TEST((nfd == input));
    }
  }
}
BOOST_DATA_TEST_CASE(forward,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  std::u32string nfd_of_input;
  std::ranges::copy(input | unicode::nfd, std::back_inserter(nfd_of_input));
  BOOST_TEST((nfd_of_input == nfd));

  std::u32string nfd_of_nfc;
  std::ranges::copy(input | unicode::nfd, std::back_inserter(nfd_of_nfc));
  BOOST_TEST((nfd_of_nfc == nfd));

  std::u32string nfd_of_nfd;
  std::ranges::copy(input | unicode::nfd, std::back_inserter(nfd_of_nfd));
  BOOST_TEST((nfd_of_nfd == nfd));
}

BOOST_AUTO_TEST_CASE(forward_singles) {
  for(auto const run: unicode::ucd::single_cps) {
    for(char32_t const ch: std::views::iota(run.first.unwrap(), run.second.unwrap())) {
      std::u32string input{1, ch};

      std::u32string nfd;
      std::ranges::copy(input | unicode::nfd, std::back_inserter(nfd));
      BOOST_TEST((nfd == input));
    }
  }
}

BOOST_DATA_TEST_CASE(eager_input,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  std::u32string const nfd_of_input{unicode::to_nfd(input
    | tests::single_pass)};
  BOOST_TEST((nfd_of_input == nfd));

  std::u32string const nfd_of_nfc{unicode::to_nfd(nfc | tests::single_pass)};
  BOOST_TEST((nfd_of_nfc == nfd));

  std::u32string const nfd_of_nfd{unicode::to_nfd(nfd | tests::single_pass)};
  BOOST_TEST((nfd_of_nfd == nfd));
}

BOOST_AUTO_TEST_CASE(eager_input_singles) {
  for(auto const run: unicode::ucd::single_cps) {
    for(char32_t const ch: std::views::iota(run.first.unwrap(), run.second.unwrap())) {
      std::u32string const input{1, ch};
      std::u32string const nfd{unicode::to_nfd(input | tests::single_pass)};
      BOOST_TEST((nfd == input));
    }
  }
}

BOOST_DATA_TEST_CASE(eager_forward,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  std::u32string const nfd_of_input{unicode::to_nfd(input)};
  BOOST_TEST((nfd_of_input == nfd));

  std::u32string const nfd_of_nfc{unicode::to_nfd(nfc)};
  BOOST_TEST((nfd_of_nfc == nfd));

  std::u32string const nfd_of_nfd{unicode::to_nfd(nfd)};
  BOOST_TEST((nfd_of_nfd == nfd));
}

BOOST_AUTO_TEST_CASE(eager_forward_singles) {
  for(auto const run: unicode::ucd::single_cps) {
    for(char32_t const ch: std::views::iota(run.first.unwrap(), run.second.unwrap())) {
      std::u32string const input{1, ch};
      std::u32string const nfd{unicode::to_nfd(input)};
      BOOST_TEST((nfd == input));
    }
  }
}

BOOST_DATA_TEST_CASE(check_input,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  if(input != nfd) {
    BOOST_TEST(!unicode::is_nfd(input | tests::single_pass));
  }

  if(nfc != nfd) {
    BOOST_TEST(!unicode::is_nfd(nfc | tests::single_pass));
  }

  BOOST_TEST(unicode::is_nfd(nfd | tests::single_pass));
}

BOOST_DATA_TEST_CASE(check_forward,
    bdata::make(unicode::ucd::normal_test_inputs)
    ^ bdata::make(unicode::ucd::normal_test_nfc)
    ^ bdata::make(unicode::ucd::normal_test_nfd), input_c, nfc_c, nfd_c) {
  std::u8string const u8input{util::as_u8char_s(input_c)};
  std::u32string input;
  std::ranges::copy(u8input | unicode::utf8_decode(),
    std::back_inserter(input));

  std::u8string const u8nfc{util::as_u8char_s(nfc_c)};
  std::u32string nfc;
  std::ranges::copy(u8nfc | unicode::utf8_decode(), std::back_inserter(nfc));

  std::u8string const u8nfd{util::as_u8char_s(nfd_c)};
  std::u32string nfd;
  std::ranges::copy(u8nfd | unicode::utf8_decode(), std::back_inserter(nfd));

  if(input != nfd) {
    BOOST_TEST(!unicode::is_nfd(input));
  }

  if(nfc != nfd) {
    BOOST_TEST(!unicode::is_nfd(nfc));
  }

  BOOST_TEST(unicode::is_nfd(nfd));
}

BOOST_AUTO_TEST_SUITE_END() // nfd
BOOST_AUTO_TEST_SUITE_END() // ucd_tests
