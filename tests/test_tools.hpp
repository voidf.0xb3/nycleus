#ifndef TESTS_TEST_TOOLS_HPP_INCLUDED_DH5VHWJ0SVSZWX7PZM97I6RNE
#define TESTS_TEST_TOOLS_HPP_INCLUDED_DH5VHWJ0SVSZWX7PZM97I6RNE

#include "util/small_vector.hpp"
#include "util/u8compat.hpp"
#include <cstddef>
#include <ostream>
#include <utility>
#include <vector>

namespace test {

struct tabs {
  std::size_t value;
};

inline std::ostream& operator<<(std::ostream& os, tabs indent) {
  using namespace util::u8compat_literals;
  for(std::size_t i{0}; i < indent.value; ++i) {
    os << u8"  "_ch;
  }
  return os;
}

template<typename T, typename... Args>
[[nodiscard]] std::vector<T> make_vector(Args&&... args) {
  std::vector<T> vec{};
  (vec.push_back(std::forward<Args>(args)), ...);
  return vec;
}

template<typename T, std::size_t I, typename... Args>
[[nodiscard]] util::small_vector<T, I> make_small_vector(Args&&... args) {
  util::small_vector<T, I> vec{};
  (vec.push_back(std::forward<Args>(args)), ...);
  return vec;
}

} // test

#endif
