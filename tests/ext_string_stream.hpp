#ifndef TESTS_EXT_STRING_STREAM_HPP_INCLUDED_R87U0B0KP7VLVDW9XAK7DZE0F
#define TESTS_EXT_STRING_STREAM_HPP_INCLUDED_R87U0B0KP7VLVDW9XAK7DZE0F

#include <istream>
#include <memory>
#include <streambuf>
#include <string>

namespace test {

/** @brief A stream buffer that uses an externally supplied std::basic_string
 * object as a buffer. */
template<typename T, typename Traits = std::char_traits<T>,
  typename Allocator = std::allocator<T>>
class basic_ext_string_streambuf: public std::basic_streambuf<T, Traits> {
public:
  using string_type = std::basic_string<T, Traits, Allocator>;

private:
  string_type& buf_;

  using base_type = std::basic_streambuf<T, Traits>;

public:
  explicit basic_ext_string_streambuf(string_type& buf): buf_{buf} {
    base_type::setg(&*buf_.begin(), &*buf_.begin(), &*buf_.end());
    base_type::setp(&*buf_.end(), &*buf_.end());
  }

  virtual typename base_type::int_type overflow(
      typename base_type::int_type ch = Traits::eof()) override {
    auto const get_offset{base_type::gptr() - base_type::eback()};

    if(Traits::eq_int_type(ch, Traits::eof())) {
      buf_.push_back(0);
      base_type::setp(&*buf_.end() - 1, &*buf_.end());
    } else {
      buf_.push_back(Traits::to_char_type(ch));
      base_type::setp(&*buf_.end(), &*buf_.end());
    }

    base_type::setg(&*buf_.begin(), &*buf_.begin() + get_offset, &*buf_.end());

    return Traits::not_eof(ch);
  }
};

extern template class basic_ext_string_streambuf<char>;

using ext_string_streambuf = basic_ext_string_streambuf<char>;
using ext_wstring_streambuf = basic_ext_string_streambuf<wchar_t>;

/** @brief A stream that uses an externally supplied std::basic_string object as
 * a buffer. */
template<typename T, typename Traits = std::char_traits<T>,
  typename Allocator = std::allocator<T>>
class basic_ext_string_stream:
    virtual private basic_ext_string_streambuf<T, Traits, Allocator>,
    public std::basic_iostream<T, Traits> {
private:
  using string_type = std::basic_string<T, Traits, Allocator>;
  using streambuf_type = basic_ext_string_streambuf<T, Traits, Allocator>;

public:
  explicit basic_ext_string_stream(string_type& buf): streambuf_type{buf},
    std::basic_ios<T, Traits>{},
    std::basic_iostream<T, Traits>{static_cast<streambuf_type*>(this)} {}
};

extern template class basic_ext_string_stream<char>;

using ext_string_stream = basic_ext_string_stream<char>;
using ext_wstring_stream = basic_ext_string_stream<wchar_t>;

} // test

#endif
