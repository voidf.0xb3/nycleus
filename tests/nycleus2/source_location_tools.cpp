#include "nycleus2/source_location_tools.hpp"
#include "nycleus2/source_location.hpp"
#include "nycleus2/util.hpp"
#include "util/u8compat.hpp"
#include <boost/container_hash/hash.hpp>
#include <cstddef>
#include <filesystem>
#include <functional>

using namespace util::u8compat_literals;

std::ostream& nycleus2::operator<<(
  std::ostream& os,
  nycleus2::source_metric offset
) {
  return os << u8'{'_ch << offset.line << u8", "_ch << offset.col << u8'}'_ch;
}

std::ostream& nycleus2::operator<<(
  std::ostream& os,
  nycleus2::source_metric_range range
) {
  return os << u8'{'_ch << range.start << u8", "_ch << range.end << u8'}'_ch;
}

::std::size_t std::hash<::nycleus2::source_metric>::operator()(
  ::nycleus2::source_metric const& loc
) const noexcept {
  ::std::size_t result{::std::hash<::nycleus2::src_pos_t::base_type>{}(loc.line.unwrap())};
  ::boost::hash_combine(result,
    ::std::hash<::nycleus2::src_pos_t::base_type>{}(loc.col.unwrap()));
  return result;
}

::std::size_t std::hash<::nycleus2::source_metric_range>::operator()(
  ::nycleus2::source_metric_range const& range
) const noexcept {
  ::std::size_t result{::std::hash<::nycleus2::source_metric>{}(range.start)};
  ::boost::hash_combine(result, ::std::hash<::nycleus2::source_metric>{}(range.end));
  return result;
}
