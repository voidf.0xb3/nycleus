#ifndef TESTS_SOURCE_LOCATION_TOOLS_HPP_INCLUDED_O9RNSZPTW71ADEY24GDB52UCM
#define TESTS_SOURCE_LOCATION_TOOLS_HPP_INCLUDED_O9RNSZPTW71ADEY24GDB52UCM

#include <cstddef>
#include <functional>
#include <iosfwd>

namespace nycleus2 {

struct source_metric;
struct source_metric_range;

std::ostream& operator<<(std::ostream&, source_metric);

std::ostream& operator<<(std::ostream&, source_metric_range);

} // nycleus2

template<>
struct std::hash<::nycleus2::source_metric> {
  [[nodiscard]] ::std::size_t operator()(
    ::nycleus2::source_metric const&
  ) const noexcept;
};

template<>
struct std::hash<::nycleus2::source_metric_range> {
  [[nodiscard]] ::std::size_t operator()(
    ::nycleus2::source_metric_range const&
  ) const noexcept;
};

#endif
