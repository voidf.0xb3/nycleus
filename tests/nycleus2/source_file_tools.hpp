#ifndef TESTS_SOURCE_FILE_TOOLS_HPP_INCLUDED_9BBNAHDX0K8ZPP7176DLKWSQK
#define TESTS_SOURCE_FILE_TOOLS_HPP_INCLUDED_9BBNAHDX0K8ZPP7176DLKWSQK

#include "nycleus2/source_file.hpp"
#include "nycleus2/source_location.hpp"
#include <filesystem>
#include <iosfwd>
#include <memory>
#include <span>
#include <string>
#include <vector>

namespace nycleus2 {

std::ostream& operator<<(std::ostream&, parsed_file::top_level_def const&);

[[nodiscard]] bool operator==(
  parsed_file::top_level_def const&,
  parsed_file::top_level_def const&
) noexcept;

} // nycleus2

namespace test::nycleus2 {

struct source_file_access {
  using token_boundary = ::nycleus2::source_file::token_boundary;

  struct proto_node {
    std::u8string data;
    std::vector<token_boundary> tokens;
    bool starts_in_token;
  };

  static ::nycleus2::source_file create(
    std::filesystem::path name,
    std::span<proto_node const>,
    std::unique_ptr<::nycleus2::token_diagnostics>,
    ::nycleus2::metric_mode mode = ::nycleus2::metric_mode{::nycleus2::metric_encoding::utf8,
      ::nycleus2::metric_line_term::full}
  );

  static void compare(
    ::nycleus2::source_file const&,
    std::span<proto_node const> expected_nodes
  ) noexcept;

  static std::unique_ptr<::nycleus2::token_diagnostics> const& get_diags(
    ::nycleus2::source_file const& file
  ) noexcept {
    return file.diags_;
  }
};

} // test::nycleus2

#endif
