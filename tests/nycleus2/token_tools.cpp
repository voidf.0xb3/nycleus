#include "nycleus2/token_tools.hpp"
#include "nycleus2/token.hpp"
#include "nycleus2/source_location_tools.hpp"
#include "nycleus2/util.hpp"
#include "util/u8compat.hpp"
#include <ostream>
#include <variant>

using namespace util::u8compat_literals;

namespace {

void print_token(std::ostream& os, nycleus2::tok::word const& tok) {
  os << u8"word: "_ch;
  switch(tok.sigil) {
    using enum nycleus2::tok::word::sigil_t;
    case none: break;
    case dollar: os << u8'$'_ch; break;
    case underscore: os << u8'_'_ch; break;
  }
  os << util::as_char(tok.text);
}

void print_token(std::ostream& os, nycleus2::tok::int_literal const& tok) {
  os << u8"int literal: "_ch << tok.value;
  if(tok.type_suffix) {
    os << (tok.type_suffix->is_signed ? u8's'_ch : u8'u'_ch)
      << tok.type_suffix->width;
  }
}

void print_token(std::ostream& os, nycleus2::tok::string_literal const& tok) {
  os << u8"string literal: \""_ch;
  for(char8_t const ch: tok.value) {
    if(ch == u8'"' || ch == u8'\\') {
      os << u8'\\'_ch;
    }
    os << static_cast<char>(ch);
  }
  os << u8'"'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::char_literal const& tok) {
  os << u8"char literal: '"_ch;
  for(char8_t const ch: tok.value) {
    if(ch == u8'\'' || ch == u8'\\') {
      os << u8'\\'_ch;
    }
    os << static_cast<char>(ch);
  }
  os << u8'\''_ch;
}

void print_token(std::ostream& os, nycleus2::tok::excl) {
  os << u8'!'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::neq) {
  os << u8"!="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::percent) {
  os << u8'%'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::percent_assign) {
  os << u8"%="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::amp) {
  os << u8'&'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::logical_and tok) {
  os << u8"&& "_ch << tok.midpoint;
}

void print_token(std::ostream& os, nycleus2::tok::amp_assign) {
  os << u8"&="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::lparen) {
  os << u8'('_ch;
}

void print_token(std::ostream& os, nycleus2::tok::rparen) {
  os << u8')'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::star) {
  os << u8'*'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::star_assign) {
  os << u8"*="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::plus) {
  os << u8'+'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::incr) {
  os << u8"++"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::plus_assign) {
  os << u8"+="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::comma) {
  os << u8','_ch;
}

void print_token(std::ostream& os, nycleus2::tok::minus) {
  os << u8'-'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::decr) {
  os << u8"--"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::minus_assign) {
  os << u8"-="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::arrow) {
  os << u8"->"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::point) {
  os << u8'.'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::double_point) {
  os << u8".."_ch;
}

void print_token(std::ostream& os, nycleus2::tok::ellipsis) {
  os << u8"..."_ch;
}

void print_token(std::ostream& os, nycleus2::tok::slash) {
  os << u8'/'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::slash_assign) {
  os << u8"/="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::colon) {
  os << u8':'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::semicolon) {
  os << u8';'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::lt) {
  os << u8'<'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::lrot) {
  os << u8"</"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::lrot_assign) {
  os << u8"</="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::lshift) {
  os << u8"<<"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::lshift_assign) {
  os << u8"<<="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::leq) {
  os << u8"<="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::three_way_cmp) {
  os << u8"<=>"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::assign) {
  os << u8'='_ch;
}

void print_token(std::ostream& os, nycleus2::tok::eq) {
  os << u8"=="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::fat_arrow) {
  os << u8"=>"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::gt) {
  os << u8'>'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::rrot) {
  os << u8">/"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::rrot_assign) {
  os << u8">/="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::geq) {
  os << u8">="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::rshift) {
  os << u8">>"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::rshift_assign) {
  os << u8">>="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::question) {
  os << u8'?'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::at) {
  os << u8'@'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::lsquare) {
  os << u8'['_ch;
}

void print_token(std::ostream& os, nycleus2::tok::rsquare) {
  os << u8']'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::bitwise_xor) {
  os << u8'^'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::bitwise_xor_assign) {
  os << u8"^="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::logical_xor) {
  os << u8"^^"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::lbrace) {
  os << u8'{'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::pipe) {
  os << u8'|'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::pipe_assign) {
  os << u8"|="_ch;
}

void print_token(std::ostream& os, nycleus2::tok::subst) {
  os << u8"|>"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::logical_or) {
  os << u8"||"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::rbrace) {
  os << u8'}'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::tilde) {
  os << u8'~'_ch;
}

void print_token(std::ostream& os, nycleus2::tok::comment) {
  os << u8"comment"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::error) {
  os << u8"error token"_ch;
}

void print_token(std::ostream& os, nycleus2::tok::eof) {
  os << u8"eof token"_ch;
}

} // (anonymous)

std::ostream& nycleus2::operator<<(
  std::ostream& os,
  nycleus2::token const& tok
) {
  std::visit([&](auto const& tok) { print_token(os, tok); }, tok);
  return os;
}

std::ostream& nycleus2::operator<<(
  std::ostream& os,
  token_source::first_gap_result const& first_gap_result
) {
  return os
    << u8"first_gap_result{.new_offset = "_ch << first_gap_result.new_metric
    << u8", .old_offset = "_ch << first_gap_result.old_metric
    << u8", .max_reuse = "_ch << first_gap_result.max_reuse << u8'}'_ch;
}

std::ostream& nycleus2::operator<<(
  std::ostream& os,
  token_source::next_token_result const& next_token_result
) {
  return os
    << u8"next_token_result{.token = "_ch << next_token_result.next_token
    << u8", .token_new_offset = "_ch << next_token_result.token_new_metric
    << u8", .gap_new_offset = "_ch << next_token_result.gap_new_metric
    << u8", .old_offset = "_ch << next_token_result.old_metric
    << u8", .max_reuse = "_ch << next_token_result.max_reuse << u8'}'_ch;
}

std::ostream& nycleus2::operator<<(
  std::ostream& os,
  token_source::reuse_result const& reuse_result
) {
  return os
    << u8"reuse_result{.gap_new_offset = "_ch << reuse_result.gap_new_metric
    << u8", .gap_old_offset = "_ch << reuse_result.gap_old_metric
    << u8", .max_reuse = "_ch << reuse_result.max_reuse << u8'}'_ch;
}
