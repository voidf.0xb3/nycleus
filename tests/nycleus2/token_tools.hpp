#ifndef TESTS_NYCLEUS2_TOKEN_TOOLS_HPP_INCLUDED_28V831WB2QU8JOAIQT9DSOSH9
#define TESTS_NYCLEUS2_TOKEN_TOOLS_HPP_INCLUDED_28V831WB2QU8JOAIQT9DSOSH9

#include "nycleus2/token.hpp"
#include <iosfwd>

namespace nycleus2 {

std::ostream& operator<<(std::ostream&, token const&);

std::ostream& operator<<(std::ostream&, token_source::first_gap_result const&);

std::ostream& operator<<(std::ostream&, token_source::next_token_result const&);

std::ostream& operator<<(std::ostream&, token_source::reuse_result const&);

} // nycleus

#endif
