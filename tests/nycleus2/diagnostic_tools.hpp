#ifndef TESTS_DIAGNOSTIC_TOOLS_HPP_INCLUDED_9IWX7UTCQ2X7X04X8WC2SRBIB
#define TESTS_DIAGNOSTIC_TOOLS_HPP_INCLUDED_9IWX7UTCQ2X7X04X8WC2SRBIB

#include <cstddef>
#include <functional>
#include <memory>
#include <span>
#include <utility>
#include <vector>

namespace nycleus2 {

struct diagnostic;

[[nodiscard]] bool operator==(
  diagnostic const& a,
  diagnostic const& b
) noexcept;

} // nycleus2

template<>
struct std::hash<::std::reference_wrapper<::nycleus2::diagnostic const>> {
  [[nodiscard]] ::std::size_t operator()(
    ::std::reference_wrapper<::nycleus2::diagnostic const>
  ) const noexcept;
};

namespace test::nycleus2 {

template<std::derived_from<::nycleus2::diagnostic>... Ts>
[[nodiscard]] auto diags(std::unique_ptr<Ts>... ds) {
  std::vector<std::unique_ptr<::nycleus2::diagnostic>> result;
  result.reserve(sizeof...(Ts));
  (result.push_back(std::move(ds)), ...);
  return result;
}

[[nodiscard]] bool diagseq(
  std::span<std::unique_ptr<::nycleus2::diagnostic> const>,
  std::span<std::unique_ptr<::nycleus2::diagnostic> const>
);

} // test::nycleus2

#endif
