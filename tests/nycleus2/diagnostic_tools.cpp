#include "nycleus2/diagnostic_tools.hpp"
#include "nycleus2/diagnostic.hpp"
#include "nycleus2/source_location_tools.hpp"
#include "util/ranges.hpp"
#include <boost/container_hash/hash.hpp>
#include <concepts>
#include <cstddef>
#include <functional>
#include <memory>
#include <ranges>
#include <span>
#include <typeinfo>
#include <unordered_set>

namespace diag = nycleus2::diag;

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: COMPARISON
///////////////////////////////////////////////////////////////////////////////////////////////

namespace {

template<typename T, typename U>
requires (!std::same_as<T, U>)
[[nodiscard]] bool diag_cmp_impl(T const&, U const&) noexcept {
  return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Lexer errors

[[nodiscard]] bool diag_cmp_impl(
  diag::bidi_code_in_string const& a,
  diag::bidi_code_in_string const& b
) noexcept {
  return a.code == b.code && a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::comment_unmatched_bidi_code const& a,
  diag::comment_unmatched_bidi_code const& b
) noexcept {
  return a.code == b.code && a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::empty_id const& a,
  diag::empty_id const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::empty_ext const& a,
  diag::empty_ext const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::invalid_escape_code const& a,
  diag::invalid_escape_code const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::num_literal_base_prefix_and_suffix const& a,
  diag::num_literal_base_prefix_and_suffix const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::num_literal_invalid const& a,
  diag::num_literal_invalid const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::num_literal_suffix_missing_width const& a,
  diag::num_literal_suffix_missing_width const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::num_literal_suffix_too_wide const& a,
  diag::num_literal_suffix_too_wide const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::num_literal_suffix_zero_width const& a,
  diag::num_literal_suffix_zero_width const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::unexpected_code_point const& a,
  diag::unexpected_code_point const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::unexpected_zwj const& a,
  diag::unexpected_zwj const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::unexpected_zwnj const& a,
  diag::unexpected_zwnj const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::unterminated_char const& a,
  diag::unterminated_char const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::unterminated_comment const& a,
  diag::unterminated_comment const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::unterminated_string const& a,
  diag::unterminated_string const& b
) noexcept {
  return a.loc == b.loc;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Parser errors

[[nodiscard]] bool diag_cmp_impl(
  diag::assign_semicolon_expected const& a,
  diag::assign_semicolon_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::block_rbrace_expected const& a,
  diag::block_rbrace_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::class_def_body_expected const& a,
  diag::class_def_body_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::class_def_name_expected const& a,
  diag::class_def_name_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::class_def_rbrace_expected const& a,
  diag::class_def_rbrace_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::expr_expected const& a,
  diag::expr_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::expr_rparen_expected const& a,
  diag::expr_rparen_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_def_body_expected const& a,
  diag::fn_def_body_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_def_lparen_expected const& a,
  diag::fn_def_lparen_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_def_name_expected const& a,
  diag::fn_def_name_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_def_param_name_expected const& a,
  diag::fn_def_param_name_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_def_param_next_expected const& a,
  diag::fn_def_param_next_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_def_param_type_expected const& a,
  diag::fn_def_param_type_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_type_lparen_expected const& a,
  diag::fn_type_lparen_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_type_param_name_expected const& a,
  diag::fn_type_param_name_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_type_param_next_expected const& a,
  diag::fn_type_param_next_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_type_rparen_expected const& a,
  diag::fn_type_rparen_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::fn_type_param_type_expected const& a,
  diag::fn_type_param_type_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::int_type_width_is_zero const& a,
  diag::int_type_width_is_zero const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::int_type_width_too_big const& a,
  diag::int_type_width_too_big const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::stmt_expected const& a,
  diag::stmt_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::top_level_def_expected const& a,
  diag::top_level_def_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::type_name_expected const& a,
  diag::type_name_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::var_def_init_expected const& a,
  diag::var_def_init_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::var_def_name_expected const& a,
  diag::var_def_name_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::var_def_semicolon_expected const& a,
  diag::var_def_semicolon_expected const& b
) noexcept {
  return a.loc == b.loc;
}

[[nodiscard]] bool diag_cmp_impl(
  diag::var_def_type_expected const& a,
  diag::var_def_type_expected const& b
) noexcept {
  return a.loc == b.loc;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Errors about unsupported features

[[nodiscard]] bool diag_cmp_impl(
  diag::fp_literals_unsupported const& a,
  diag::fp_literals_unsupported const& b
) noexcept {
  return a.loc == b.loc;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Warnings

[[nodiscard]] bool diag_cmp_impl(
  diag::string_newline const& a,
  diag::string_newline const& b
) noexcept {
  return a.newline_code == b.newline_code && a.loc == b.loc;
}

} // (anonymous)

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Public API

bool nycleus2::operator==(
  nycleus2::diagnostic const& a,
  nycleus2::diagnostic const& b
) noexcept {
  return a.visit([&](auto const& a) {
    return b.visit([&](auto const& b) {
      return diag_cmp_impl(a, b);
    });
  });
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: HASHING
///////////////////////////////////////////////////////////////////////////////////////////////

namespace {

template<typename T>
void add_hash(
  std::size_t& result,
  T const& value
) {
  boost::hash_combine(result, std::hash<T>{}(value));
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Lexer errors

void diag_hash_impl(
  std::size_t& result,
  diag::bidi_code_in_string const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.code);
}

void diag_hash_impl(
  std::size_t& result,
  diag::comment_unmatched_bidi_code const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.code);
}

void diag_hash_impl(
  std::size_t& result,
  diag::empty_id const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::empty_ext const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::invalid_escape_code const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::num_literal_base_prefix_and_suffix const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::num_literal_invalid const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::num_literal_suffix_missing_width const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.prefix_letter);
}

void diag_hash_impl(
  std::size_t& result,
  diag::num_literal_suffix_too_wide const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::num_literal_suffix_zero_width const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::unexpected_code_point const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.code_point);
}

void diag_hash_impl(
  std::size_t& result,
  diag::unexpected_zwj const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::unexpected_zwnj const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::unterminated_char const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::unterminated_comment const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::unterminated_string const& d
) noexcept {
  add_hash(result, d.loc);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Parser errors

void diag_hash_impl(
  std::size_t& result,
  diag::assign_semicolon_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::block_rbrace_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::class_def_body_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::class_def_name_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::class_def_rbrace_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::expr_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::expr_rparen_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_def_body_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_def_lparen_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_def_name_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_def_param_name_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_def_param_next_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_def_param_type_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_type_lparen_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_type_param_name_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_type_param_next_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_type_rparen_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::fn_type_param_type_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::int_type_width_is_zero const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::int_type_width_too_big const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::stmt_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::top_level_def_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::type_name_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::var_def_init_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::var_def_name_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::var_def_semicolon_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

void diag_hash_impl(
  std::size_t& result,
  diag::var_def_type_expected const& d
) noexcept {
  add_hash(result, d.loc);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Errors about unsupported features

void diag_hash_impl(
  std::size_t& result,
  diag::fp_literals_unsupported const& d
) noexcept {
  add_hash(result, d.loc);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Warnings

void diag_hash_impl(
  std::size_t& result,
  diag::string_newline const& d
) noexcept {
  add_hash(result, d.loc);
  add_hash(result, d.newline_code);
}

} // (anonymous)

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Public API

::std::size_t
std::hash<::std::reference_wrapper<::nycleus2::diagnostic const>>::operator()(
  ::std::reference_wrapper<::nycleus2::diagnostic const> d
) const noexcept {
  ::std::size_t result{typeid(d).hash_code()};
  d.get().const_visit([&](auto const& d) noexcept {
    diag_hash_impl(result, d);
  });
  return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: SET COMPARISON
///////////////////////////////////////////////////////////////////////////////////////////////

bool test::nycleus2::diagseq(
  std::span<std::unique_ptr<::nycleus2::diagnostic> const> a,
  std::span<std::unique_ptr<::nycleus2::diagnostic> const> b
) {
  auto const diags_set{a
    | std::views::transform(&std::unique_ptr<::nycleus2::diagnostic>::operator*)
    | util::range_to<std::unordered_set<
      std::reference_wrapper<::nycleus2::diagnostic const>>>()};
  auto const expected_diags_set{b
    | std::views::transform(&std::unique_ptr<::nycleus2::diagnostic>::operator*)
    | util::range_to<std::unordered_set<
      std::reference_wrapper<::nycleus2::diagnostic const>>>()};
  return diags_set == expected_diags_set;
}
