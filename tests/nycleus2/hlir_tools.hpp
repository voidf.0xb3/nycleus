#ifndef TESTS_NYCLEUS2_HLIR_TOOLS_HPP_INCLUDED_0MGXI4D482XNRB6KY4W1Q2XU8
#define TESTS_NYCLEUS2_HLIR_TOOLS_HPP_INCLUDED_0MGXI4D482XNRB6KY4W1Q2XU8

#include <iosfwd>
#include <memory>

namespace nycleus2::hlir {

struct type;
struct expr;
struct stmt;
struct def;

} // nycleus2::hlir

// HLIR output /////////////////////////////////////////////////////////////////

namespace nycleus2::hlir {

std::ostream& operator<<(std::ostream&, std::unique_ptr<type> const&);
std::ostream& operator<<(std::ostream&, std::unique_ptr<expr> const&);
std::ostream& operator<<(std::ostream&, std::unique_ptr<stmt> const&);
std::ostream& operator<<(std::ostream&, std::unique_ptr<def> const&);

// HLIR comparison /////////////////////////////////////////////////////////////

[[nodiscard]] bool operator==(type const&, type const&) noexcept;
[[nodiscard]] bool operator==(expr const&, expr const&) noexcept;
[[nodiscard]] bool operator==(stmt const&, stmt const&) noexcept;
[[nodiscard]] bool operator==(def const&, def const&) noexcept;

} // nycleus2::hlir

#endif
