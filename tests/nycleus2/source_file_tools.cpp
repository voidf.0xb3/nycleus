#include "nycleus2/source_file_tools.hpp"
#include "nycleus2/diagnostic_tools.hpp"
#include "nycleus2/hlir_tools.hpp"
#include "nycleus2/source_file.hpp"
#include "nycleus2/source_location.hpp"
#include "nycleus2/source_location_tools.hpp"
#include "nycleus2/util.hpp"
#include "unicode/encoding.hpp"
#include "util/overflow.hpp"
#include "util/ranges.hpp"
#include "util/u8compat.hpp"
#include "util/unbounded_view.hpp"
#include "util/util.hpp"
#include "util/zip_view.hpp"
#include <boost/test/unit_test.hpp>
#include <algorithm>
#include <cassert>
#include <filesystem>
#include <memory>
#include <ostream>
#include <ranges>
#include <span>
#include <utility>
#include <vector>

using namespace util::u8compat_literals;

std::ostream& nycleus2::operator<<(
  std::ostream& os,
  nycleus2::parsed_file::top_level_def const& def
) {
  os << u8"top-level def\npreceding gap = "_ch << def.preceding_gap << u8"\npanic seq = {"_ch;

  bool first{true};
  for(auto const& metric: def.panic_seq.metrics) {
    if(first) {
      first = false;
    } else {
      os << u8", "_ch;
    }
    os << metric;
  }

  os << u8"}\n"_ch << def.def;

  return os;
}

bool nycleus2::operator==(
  nycleus2::parsed_file::top_level_def const& a,
  nycleus2::parsed_file::top_level_def const& b
) noexcept {
  return a.preceding_gap == b.preceding_gap
    && (!a.def && !b.def || a.def && b.def && *a.def == *b.def)
    && a.panic_seq == b.panic_seq
    && test::nycleus2::diagseq(a.diags, b.diags);
}

::nycleus2::source_file test::nycleus2::source_file_access::create(
  std::filesystem::path name,
  std::span<test::nycleus2::source_file_access::proto_node const> nodes,
  std::unique_ptr<::nycleus2::token_diagnostics> initial_diags,
  ::nycleus2::metric_mode mode
) {
  ::nycleus2::source_file src{std::move(name), mode};

  src.diags_ = std::move(initial_diags);

  if(nodes.empty()) {
    src.validate();
    return src;
  }

  std::vector<::nycleus2::source_file::node> real_nodes;
  real_nodes.reserve(util::asserted_cast<
    std::vector<::nycleus2::source_file::node>::size_type>(nodes.size()));

  bool seen_cr{false};
  for(auto const& node: nodes) {
    ::nycleus2::source_file::node new_node{
      .metric = {0, 0},
      .data = node.data | util::range_to<decltype(new_node.data)>(),
      .tokens = node.tokens,
      .starts_in_token = node.starts_in_token
    };

    assert(unicode::is_valid_utf8(node.data));

    auto it{new_node.data.cbegin()};
    while(it != new_node.data.cend()) {
      char32_t const ch{unicode::utf8<>::decode(it,
        util::unreachable_sentinel<decltype(it)>{}, unicode::eh_assume_valid<>{})};
      if(::nycleus2::is_line_terminator(ch, mode)) {
        new_node.metric = new_node.metric.add({1, 0});
      } else if(ch == U'\r') {
        if(it == new_node.data.cend() || *it == u8'\n') {
          new_node.metric = new_node.metric.add({0, ::nycleus2::num_columns(ch, mode)});
        } else {
          new_node.metric = new_node.metric.add({1, 0});
        }
      } else {
        new_node.metric = new_node.metric.add({0, ::nycleus2::num_columns(ch, mode)});
      }
    }

    if(seen_cr && node.data.front() != u8'\n') {
      ++real_nodes.back().metric.line;
      real_nodes.back().metric.col = 0;
    }

    seen_cr = node.data.back() == u8'\r';

    real_nodes.push_back(std::move(new_node));
  }
  if(seen_cr) {
    ++real_nodes.back().metric.line;
    real_nodes.back().metric.col = 0;
  }

  src.nodes_ = std::move(real_nodes);

  src.validate();
  return src;
}

namespace {

struct token_boundary_copy {
  nycleus2::src_pos_t offset;
  bool is_lookahead;

  [[maybe_unused]] friend bool operator==(token_boundary_copy a,
    token_boundary_copy b) noexcept = default;
};

[[maybe_unused]] std::ostream& operator<<(
  std::ostream& os,
  token_boundary_copy tok
) {
  os << tok.offset;
  if(tok.is_lookahead) {
    os << u8" L"_ch;
  }
  return os;
}

} // (anonymous)

void test::nycleus2::source_file_access::compare(
  ::nycleus2::source_file const& file,
  std::span<test::nycleus2::source_file_access::proto_node const> exp_nodes
) noexcept {
  BOOST_TEST(file.nodes_.size() == exp_nodes.size());

  for(auto const& [obs_node, exp_node]: util::zip(file.nodes_, exp_nodes)) {
    BOOST_TEST(std::ranges::equal(obs_node.data, exp_node.data));

    BOOST_TEST(obs_node.starts_in_token == exp_node.starts_in_token);

    auto make_tb_copy{[](::nycleus2::source_file::token_boundary tb) noexcept {
      return token_boundary_copy{
        .offset = tb.offset,
        .is_lookahead = tb.is_lookahead
      };
    }};
    auto obs_tokens{obs_node.tokens | std::views::transform(make_tb_copy)};
    auto exp_tokens{exp_node.tokens | std::views::transform(make_tb_copy)};
    BOOST_TEST(obs_tokens == exp_tokens, boost::test_tools::per_element());
  }
}
