#include "nycleus2/hlir_tools.hpp"
#include "nycleus2/diagnostic_tools.hpp"
#include "nycleus2/hlir.hpp"
#include "nycleus2/source_location_tools.hpp"
#include "util/u8compat.hpp"
#include "util/util.hpp"
#include "test_tools.hpp"
#include <ostream>
#include <memory>

using namespace nycleus2;
using namespace util::u8compat_literals;

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: HLIR OUTPUT
///////////////////////////////////////////////////////////////////////////////////////////////

using test::tabs;

namespace {

void print_type(std::unique_ptr<hlir::type> const&, std::ostream&, std::uint64_t indent);
void print_expr(std::unique_ptr<hlir::expr> const&, std::ostream&, std::uint64_t indent);
void print_stmt(std::unique_ptr<hlir::stmt> const&, std::ostream&, std::uint64_t indent);
void print_def(std::unique_ptr<hlir::def> const&, std::ostream&, std::uint64_t indent);

} // (anonymous)

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Public API

std::ostream& hlir::operator<<(std::ostream& os, std::unique_ptr<hlir::type> const& type) {
  print_type(type, os, 0);
  return os;
}

std::ostream& hlir::operator<<(std::ostream& os, std::unique_ptr<hlir::expr> const& expr) {
  print_expr(expr, os, 0);
  return os;
}

std::ostream& hlir::operator<<(std::ostream& os, std::unique_ptr<hlir::stmt> const& stmt) {
  print_stmt(stmt, os, 0);
  return os;
}

std::ostream& hlir::operator<<(std::ostream& os, std::unique_ptr<hlir::def> const& def) {
  print_def(def, os, 0);
  return os;
}

namespace {

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Type printing

void print_type(
  std::unique_ptr<hlir::type> const& type,
  std::ostream& os,
  std::uint64_t indent
) {
  os << tabs{indent};
  if(!type) {
    os << u8"(tainted)\n"_ch;
    return;
  }
  os << type->loc << u8' '_ch;
  type->visit(util::overloaded{
    [&](hlir::int_type const& type) {
      os << (type.is_signed ? u8's'_ch : u8'u'_ch) << type.width << u8'\n'_ch;
    },

    [&](hlir::bool_type const&) {
      os << u8"bool\n"_ch;
    },

    [&](hlir::fn_type const& type) {
      os << u8"function reference\n"_ch << tabs{indent + 1};
      if(type.return_type) {
        os << u8"return type:\n"_ch;
        print_type(*type.return_type, os, indent + 2);
      } else {
        os << u8"returns void\n"_ch;
      }
      os << tabs{indent + 1} << u8"param types:\n"_ch;
      for(auto const& param: type.param_types) {
        print_type(param, os, indent + 2);
      }
    },

    [&](hlir::ptr_type const& type) {
      if(type.is_mutable) {
        os << u8"mutable "_ch;
      }
      os << u8"pointer type\n"_ch;
      print_type(type.pointee_type, os, indent + 1);
    },

    [&](hlir::id_type const& type) {
      os << u8"id type "_ch << util::as_char(type.name) << u8'\n'_ch;
    }
  });
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Expression printing

void print_expr(
  std::unique_ptr<hlir::expr> const& expr,
  std::ostream& os,
  std::uint64_t indent
) {
  os << tabs{indent};
  if(!expr) {
    os << u8"(tainted)\n"_ch;
    return;
  }
  os << expr->loc << u8' '_ch;
  expr->visit(util::overloaded{
    [&](hlir::int_literal const& expr) {
      os << u8"int "_ch << expr.value;
      if(expr.type_suffix) {
        os << (expr.type_suffix->is_signed ? u8's'_ch : u8'u'_ch) << expr.type_suffix->width;
      }
      os << u8'\n'_ch;
    },

    [&](hlir::bool_literal const& expr) {
      os << u8"bool "_ch << (expr.value ? u8"true"_ch : u8"false"_ch) << u8'\n'_ch;
    },

    [&](hlir::id_expr const& expr) {
      os << u8"id "_ch << util::as_char(expr.name) << u8'\n'_ch;
    },

    [&](hlir::cmp_expr const& expr) {
      os << u8"cmp\n"_ch;
      print_expr(expr.first_op, os, indent + 1);
      for(auto const& rel: expr.rels) {
        os << tabs{indent + 1};
        switch(rel.kind) {
          case hlir::cmp_expr::rel_kind_t::eq: os << u8"=="_ch; break;
          case hlir::cmp_expr::rel_kind_t::gt: os << u8'>'_ch; break;
          case hlir::cmp_expr::rel_kind_t::ge: os << u8">="_ch; break;
          case hlir::cmp_expr::rel_kind_t::lt: os << u8'<'_ch; break;
          case hlir::cmp_expr::rel_kind_t::le: os << u8"<="_ch; break;
        }
        os << u8'\n'_ch;
        print_expr(rel.right_op, os, indent + 1);
      }
    },

    [&](hlir::binary_op_expr const& expr) {
      switch(expr.get_kind()) {
        case hlir::expr::kind_t::or_expr:   os << u8"||"_ch; break;
        case hlir::expr::kind_t::and_expr:  os << u8"&&"_ch; break;
        case hlir::expr::kind_t::xor_expr:  os << u8"^^"_ch; break;
        case hlir::expr::kind_t::neq_expr:  os << u8"!="_ch; break;
        case hlir::expr::kind_t::add_expr:  os << u8'+'_ch; break;
        case hlir::expr::kind_t::sub_expr:  os << u8"- (binary)"_ch; break;
        case hlir::expr::kind_t::mul_expr:  os << u8'*'_ch; break;
        case hlir::expr::kind_t::div_expr:  os << u8'/'_ch; break;
        case hlir::expr::kind_t::mod_expr:  os << u8'%'_ch; break;
        case hlir::expr::kind_t::bor_expr:  os << u8'|'_ch; break;
        case hlir::expr::kind_t::band_expr: os << u8'&'_ch; break;
        case hlir::expr::kind_t::bxor_expr: os << u8'^'_ch; break;
        case hlir::expr::kind_t::lsh_expr:  os << u8"<<"_ch; break;
        case hlir::expr::kind_t::rsh_expr:  os << u8">>"_ch; break;
        case hlir::expr::kind_t::lrot_expr: os << u8"</"_ch; break;
        case hlir::expr::kind_t::rrot_expr: os << u8">/"_ch; break;
        default: util::unreachable();
      }
      os << u8'\n'_ch;
      print_expr(expr.left_op, os, indent + 1);
      print_expr(expr.right_op, os, indent + 1);
    },

    [&](hlir::unary_op_expr const& expr) {
      switch(expr.get_kind()) {
        case hlir::expr::kind_t::neg_expr:    os << u8"- (unary)"_ch; break;
        case hlir::expr::kind_t::bnot_expr:   os << u8'~'_ch; break;
        case hlir::expr::kind_t::not_expr:    os << u8'!'_ch; break;
        case hlir::expr::kind_t::deref_expr:  os << u8'*'_ch; break;
        case hlir::expr::kind_t::parens_expr: os << u8"()"_ch; break;
        default: util::unreachable();
      }
      os << u8'\n'_ch;
      print_expr(expr.op, os, indent + 1);
    },

    [&](hlir::addr_expr const& expr) {
      os << u8'&'_ch;
      if(expr.is_mutable) {
        os << u8"mut"_ch;
      }
      os << u8'\n'_ch;
      print_expr(expr.op, os, indent + 1);
    },

    [&](hlir::member_expr const& expr) {
      os << u8"member: "_ch << util::as_char(expr.name) << u8'\n'_ch;
      print_expr(expr.op, os, indent + 1);
    },

    [&](hlir::call_expr const& expr) {
      os << u8"call expr\n"_ch << tabs{indent} << u8"  callee:\n"_ch;
      print_expr(expr.callee, os, indent + 2);

      os << u8"  args:\n"_ch;
      for(auto const& arg: expr.args) {
        print_expr(arg.value, os, indent + 1);
      }
    },

    [&](hlir::block const& expr) {
      os << u8"block\n"_ch;

      for(auto const& el: expr.stmts) {
        print_stmt(el, os, indent + 1);
      }

      if(expr.trail) {
        os << tabs{indent} << u8"  trail:\n"_ch;
        print_expr(*expr.trail, os, indent + 2);
      }
    }
  });
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Statement printing

void print_stmt(
  std::unique_ptr<hlir::stmt> const& stmt,
  std::ostream& os,
  std::uint64_t indent
) {
  os << tabs{indent};
  if(!stmt) {
    os << u8"(tainted)\n"_ch;
    return;
  }
  os << stmt->loc << u8' '_ch;
  stmt->visit(util::overloaded{
    [&](hlir::expr_stmt const& stmt) {
      os << u8"expr\n"_ch;
      print_expr(stmt.body, os, indent + 1);
    },

    [&](hlir::assign_stmt const& stmt) {
      os << u8"assign:\n"_ch << tabs{indent} << u8"  target:\n"_ch;
      print_expr(stmt.target, os, indent + 2);
      os << tabs{indent} << u8"  value:\n"_ch;
      print_expr(stmt.value, os, indent + 2);
    },

    [&](hlir::def_stmt const& stmt) {
      os << u8"def\n"_ch;
      print_def(stmt.body, os, indent + 1);
    },
  });
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Definition printing

void print_def(
  std::unique_ptr<hlir::def> const& def,
  std::ostream& os,
  std::uint64_t indent
) {
  os << tabs{indent};
  if(!def) {
    os << u8"(tainted)\n"_ch;
    return;
  }
  os << def->loc << u8' '_ch;
  if(def->name) {
    os << util::as_char(*def->name);
  } else {
    os << u8"(tainted name)"_ch;
  }
  os << u8": "_ch;
  def->visit(util::overloaded{
    [&](hlir::fn_def const& def) {
      os << u8"function definition\n"_ch << tabs{indent} << u8"  params:\n"_ch;
      for(auto const& param: def.params) {
        os << tabs{indent} << u8"    "_ch;
        if(param.name) {
          os << util::as_char(*param.name);
        } else {
          os << u8"(anonymous parameter)"_ch;
        }
        os << u8":\n"_ch;
        print_type(param.type, os, indent + 3);
      }

      if(def.return_type) {
        os << tabs{indent} << u8"  return type:\n"_ch;
        print_type(*def.return_type, os, indent + 2);
      }

      os << tabs{indent} << u8"  body:\n"_ch;
      print_expr(def.body, os, indent + 2);
    },

    [&](hlir::var_def const& def) {
      switch(def.mutable_spec) {
        case hlir::var_def::mutable_spec_t::none:
          break;

        case hlir::var_def::mutable_spec_t::mut:
          os << u8"mutable "_ch;
          break;

        case hlir::var_def::mutable_spec_t::constant:
          os << u8"constant "_ch;
          break;
      }

      os << u8"variable definition\n"_ch << tabs{indent} << u8"  type:\n"_ch;
      print_type(def.type, os, indent + 2);

      os << tabs{indent + 1};
      if(def.init) {
        os << u8"init:\n"_ch;
        print_expr(*def.init, os, indent + 2);
      } else {
        os << u8"no init\n"_ch;
      }
    },

    [&](hlir::class_def const& def) {
      os << u8"class definition\n"_ch;
      for(auto const& member: def.members) {
        print_def(member, os, indent + 1);
      }
    }
  });
}

} // (anonymous)

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: HLIR COMPARISON
///////////////////////////////////////////////////////////////////////////////////////////////

namespace {

template<typename T>
[[nodiscard]] bool ptreq(std::unique_ptr<T> const& a, std::unique_ptr<T> const& b) noexcept {
  return !a && !b || a && b && *a == *b;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Type comparison

[[nodiscard]] bool compare_type(auto const&, auto const&) noexcept {
  return false;
}

[[nodiscard]] bool compare_type(
  hlir::int_type const& a,
  hlir::int_type const& b
) noexcept {
  return a.width == b.width && a.is_signed == b.is_signed;
}

[[nodiscard]] bool compare_type(
  hlir::bool_type const&,
  hlir::bool_type const&
) noexcept {
  return true;
}

[[nodiscard]] bool compare_type(
  hlir::fn_type const& a,
  hlir::fn_type const& b
) noexcept {
  return (!a.return_type && !b.return_type
      || a.return_type && b.return_type && ptreq(*a.return_type, *b.return_type))
    && std::ranges::equal(a.param_types, b.param_types, [](
      std::unique_ptr<hlir::type> const& a,
      std::unique_ptr<hlir::type> const& b
    ) noexcept {
      return ptreq(a, b);
    });
}

[[nodiscard]] bool compare_type(
  hlir::ptr_type const& a,
  hlir::ptr_type const& b
) noexcept {
  return ptreq(a.pointee_type, b.pointee_type) && a.is_mutable == b.is_mutable;
}

[[nodiscard]] bool compare_type(
  hlir::id_type const& a,
  hlir::id_type const& b
) noexcept {
  return a.name == b.name;
}

} // (anonymous)

bool hlir::operator==(
  hlir::type const& a,
  hlir::type const& b
) noexcept {
  return a.loc == b.loc
    && test::nycleus2::diagseq(a.diags, b.diags)
    && a.visit([&](auto const& a_der) noexcept {
      return b.visit([&](auto const& b_der) noexcept {
        return compare_type(a_der, b_der);
      });
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Expression comparison

namespace {

[[nodiscard]] bool compare_expr(auto const&, auto const&) noexcept {
  return false;
}

[[nodiscard]] bool compare_expr(
  hlir::int_literal const& a,
  hlir::int_literal const& b
) noexcept {
  return a.value == b.value
    && (!a.type_suffix && !b.type_suffix || a.type_suffix && b.type_suffix
      && a.type_suffix->width == b.type_suffix->width
      && a.type_suffix->is_signed == b.type_suffix->is_signed);
}

[[nodiscard]] bool compare_expr(
  hlir::bool_literal const& a,
  hlir::bool_literal const& b
) noexcept {
  return a.value == b.value;
}

[[nodiscard]] bool compare_expr(
  hlir::id_expr const& a,
  hlir::id_expr const& b
) noexcept {
  return a.name == b.name;
}

[[nodiscard]] bool compare_expr(
  hlir::cmp_expr const& a,
  hlir::cmp_expr const& b
) noexcept {
  return ptreq(a.first_op, b.first_op)
    && std::ranges::equal(a.rels, b.rels,
      [](
        hlir::cmp_expr::rel const& a,
        hlir::cmp_expr::rel const& b
      ) noexcept {
        return a.kind == b.kind && ptreq(a.right_op, b.right_op);
      }
    );
}

[[nodiscard]] bool compare_expr(
  std::derived_from<hlir::binary_op_expr> auto const& a,
  std::derived_from<hlir::binary_op_expr> auto const& b
) noexcept {
  return ptreq(a.left_op, b.left_op) && ptreq(a.right_op, b.right_op);
}

[[nodiscard]] bool compare_expr(
  std::derived_from<hlir::unary_op_expr> auto const& a,
  std::derived_from<hlir::unary_op_expr> auto const& b
) noexcept {
  return ptreq(a.op, b.op);
}

[[nodiscard]] bool compare_expr(
  hlir::addr_expr const& a,
  hlir::addr_expr const& b
) noexcept {
  return ptreq(a.op, b.op) && a.is_mutable == b.is_mutable;
}

[[nodiscard]] bool compare_expr(
  hlir::member_expr const& a,
  hlir::member_expr const& b
) noexcept {
  return ptreq(a.op, b.op) && a.name == b.name;
}

[[nodiscard]] bool compare_expr(
  hlir::call_expr const& a,
  hlir::call_expr const& b
) noexcept {
  return ptreq(a.callee, b.callee) && std::ranges::equal(a.args, b.args, [](
    hlir::call_expr::arg const& a,
    hlir::call_expr::arg const& b
  ) noexcept {
    return ptreq(a.value, b.value);
  });
}

[[nodiscard]] bool compare_expr(
  hlir::block const& a,
  hlir::block const& b
) noexcept {
  return std::ranges::equal(a.stmts, b.stmts, [](
    std::unique_ptr<hlir::stmt> const& a,
    std::unique_ptr<hlir::stmt> const& b
  ) noexcept {
    return ptreq(a, b);
  }) && (!a.trail && !b.trail || a.trail && b.trail && ptreq(*a.trail, *b.trail));
}

} // (anonymous)

bool hlir::operator==(
  hlir::expr const& a,
  hlir::expr const& b
) noexcept {
  return a.loc == b.loc
    && test::nycleus2::diagseq(a.diags, b.diags)
    && a.visit([&](auto const& a_der) noexcept {
      return b.visit([&](auto const& b_der) noexcept {
        return compare_expr(a_der, b_der);
      });
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Statement comparison

namespace {

[[nodiscard]] bool compare_stmt(auto const&, auto const&) noexcept {
  return false;
}

[[nodiscard]] bool compare_stmt(
  hlir::expr_stmt const& a,
  hlir::expr_stmt const& b
) {
  return ptreq(a.body, b.body);
}

[[nodiscard]] bool compare_stmt(
  hlir::assign_stmt const& a,
  hlir::assign_stmt const& b
) noexcept {
  return ptreq(a.target, b.target) && ptreq(a.value, b.value);
}

[[nodiscard]] bool compare_stmt(
  hlir::def_stmt const& a,
  hlir::def_stmt const& b
) noexcept {
  return ptreq(a.body, b.body);
}

} // (anonymous)

bool hlir::operator==(
  hlir::stmt const& a,
  hlir::stmt const& b
) noexcept {
  return a.loc == b.loc
    && test::nycleus2::diagseq(a.diags, b.diags)
    && a.visit([&](auto const& a_der) noexcept {
      return b.visit([&](auto const& b_der) noexcept {
        return compare_stmt(a_der, b_der);
      });
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////
// MARK: Definition comparison

namespace {

[[nodiscard]] bool compare_def(auto const&, auto const&) noexcept {
  return false;
}

[[nodiscard]] bool compare_def(
  hlir::fn_def const& a,
  hlir::fn_def const& b
) noexcept {
  return
    (!a.return_type && !b.return_type
      || a.return_type && b.return_type && ptreq(*a.return_type, *b.return_type))
    && ptreq(a.body, b.body)
    && std::ranges::equal(a.params, b.params, [](
      hlir::fn_def::param const& a,
      hlir::fn_def::param const& b
    ) noexcept {
      return a.name == b.name && ptreq(a.type, b.type);
    });
}

[[nodiscard]] bool compare_def(
  hlir::var_def const& a,
  hlir::var_def const& b
) noexcept {
  return ptreq(a.type, b.type)
    && (!a.init && !b.init || a.init && b.init && ptreq(*a.init, *b.init))
    && a.mutable_spec == b.mutable_spec;
}

[[nodiscard]] bool compare_def(
  hlir::class_def const& a,
  hlir::class_def const& b
) noexcept {
  return std::ranges::equal(a.members, b.members, [](
    std::unique_ptr<hlir::def> const& a,
    std::unique_ptr<hlir::def> const& b
  ) noexcept {
    return ptreq(a, b);
  });
}

} // (anonymous)

bool hlir::operator==(
  hlir::def const& a,
  hlir::def const& b
) noexcept {
  return a.loc == b.loc
    && a.name == b.name
    && test::nycleus2::diagseq(a.diags, b.diags)
    && a.visit([&](auto const& a_der) noexcept {
      return b.visit([&](auto const& b_der) noexcept {
        return compare_def(a_der, b_der);
      });
    });
}
